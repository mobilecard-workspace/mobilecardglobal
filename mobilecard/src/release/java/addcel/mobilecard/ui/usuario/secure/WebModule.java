package addcel.mobilecard.ui.usuario.secure;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.domain.secure.WebInteractor;
import addcel.mobilecard.domain.secure.WebInteractorImpl;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * ADDCEL on 15/12/16.
 */

@Module
public final class WebModule {
    private final WebActivity activity;
    private final String USER_AGENT;

    public WebModule(WebActivity activity) {
        this.activity = activity;
        this.USER_AGENT =
                "User-Agent:Mozilla/5.0 (Linux; Android 4.4.2; SM-T230 Build/KOT49H) AppleWebKit/537.36"
                        + "(KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Safari/537.36";
    }

    @Provides
    @PerActivity
    Bundle provideExtras() {
        return Objects.requireNonNull(activity.getIntent().getExtras());
    }

    @Provides
    @PerActivity
    @Named("titulo")
    String provideTitulo(Bundle extras) {
        return extras.getString("titulo");
    }

    @Provides
    @PerActivity
    @Named("startUrl")
    String provideStartUrl(Bundle extras) {
        return extras.getString("startUrl");
    }

    @Provides
    @PerActivity
    @Named("formEndpoint")
    String provideFormEndpoint(Bundle extras) {
        return extras.getString("formEndpoint");
    }

    @Provides
    @PerActivity
    @Named("finishEndpoint")
    String provideFinishEndpoint(Bundle extras) {
        return extras.getString("finishEndpoint");
    }

    @Provides
    @PerActivity
    @Named("errorEndpoint")
    String provideError(Bundle extras) {
        return extras.getString("errorEndpoint");
    }

    @Provides
    @PerActivity
    @Named("currentUrl")
    String provideCurrent(
            @Named("startUrl") String inicioUrl) {
        return inicioUrl;
    }

    @Provides
    @PerActivity
    @Named("token")
    String provideToken(Bundle extras) {
        return extras.getString("token");
    }

    @Provides
    @PerActivity
    @Nullable
    CardEntity provideCard(Bundle extras) {
        return extras.getParcelable("card");
    }

    @Provides
    @PerActivity
    AlertDialog provideWarningDialog() {
        final DetachableClickListener okWrapper =
                DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        activity.goToMainMenu();
                    }
                });

        final DetachableClickListener cancelWrapper =
                DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        final AlertDialog dialog =
                new AlertDialog.Builder(activity).setTitle(android.R.string.dialog_alert_title)
                        .setMessage(R.string.txt_pago_warning)
                        .setPositiveButton(android.R.string.yes, okWrapper)
                        .setNegativeButton(android.R.string.cancel, cancelWrapper)
                        .create();

        okWrapper.clearOnDetach(dialog);
        cancelWrapper.clearOnDetach(dialog);

        return dialog;
    }

    @Provides
    @PerActivity
    byte[] provideRequest(Bundle extras) {
        return extras.getByteArray("data");
    }

    @Provides
    @PerActivity
    CookieManager provideManager() {
        return CookieManager.getInstance();
    }

    @Provides
    @PerActivity
    WebInteractor provideInteractor(OkHttpClient client,
                                    @Nullable CardEntity cardEntity) {
        return new WebInteractorImpl(client, cardEntity);
    }

    @Provides
    @PerActivity
    WebContract.Presenter providePresenter(WebInteractor interactor,
                                           @Named("formEndpoint") String formEndpoint) {
        return new WebPresenter(interactor, formEndpoint, activity);
    }

    @Provides
    @PerActivity
    WebViewClient provideWebClient(WebContract.Presenter presenter) {
        return new SecureWebViewClient(presenter, activity);
    }

    @SuppressLint({"SetJavaScriptEnabled", "ClickableViewAccessibility"})
    @Provides
    @PerActivity
    WebView provideBrowser(WebViewClient client, CookieManager manager) {
        final WebView browser = new WebView(activity);
        browser.setWebViewClient(client);
        browser.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        browser.requestFocus(View.FOCUS_DOWN);
        browser.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!v.hasFocus()) {
                        v.requestFocus();
                    }
                }
                return false;
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager.setAcceptThirdPartyCookies(browser, true);
            manager.removeAllCookies(null);
            browser.clearCache(true);
        } else {
            manager.removeAllCookie();
        }

        browser.getSettings().setUserAgentString(USER_AGENT);

        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        return browser;
    }
}
