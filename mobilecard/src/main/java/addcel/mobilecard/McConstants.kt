package addcel.mobilecard

import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 2019-12-18.
 */
class McConstants {

    companion object {

        const val USUARIO_STRING = "USUARIO"
        const val NEGOCIO_STRING = "NEGOCIO"
        const val COMERCIO_STRING = "COMERCIO"
        const val USE_CASE_USUARIO = 1
        const val USE_CASE_NEGOCIO = 2

        const val PAIS_ID_MX = 1
        const val PAIS_ID_CO = 2
        const val PAIS_ID_USA = 3
        const val PAIS_ID_PE = 4

        val PAIS_MX = PaisResponse.PaisEntity(PAIS_ID_MX, "MX", "MEXICO", "MEXICO")
        val PAIS_CO = PaisResponse.PaisEntity(PAIS_ID_CO, "CO", "COLOMBIA", "COLOMBIA")
        val PAIS_USA = PaisResponse.PaisEntity(PAIS_ID_USA, "USA", "USA", "USA")
        val PAIS_PE = PaisResponse.PaisEntity(PAIS_ID_PE, "PE", "PERU", "PERU")
        val PAISES = listOf(PAIS_MX, PAIS_CO, PAIS_USA, PAIS_PE)

        const val TEL_MX = "018009255001"
        const val TEL_CO = "+5715800822"
        //const val TEL_CO = "018005184868"
        const val TEL_US = "+12134230411"
        const val TEL_PE = "080080102"

        val LOCALE_MX = Locale("es", "MX")
        val LOCALE_COL = Locale("es", "CO")
        val LOCALE_USA: Locale = Locale.US
        val LOCALE_PE = Locale("es", "PE")

        val MX_CURR_FORMAT: NumberFormat = NumberFormat.getCurrencyInstance(LOCALE_MX)
        val CO_CURR_FORMAT: NumberFormat = NumberFormat.getCurrencyInstance(LOCALE_COL)
        val USA_CURR_FORMAT: NumberFormat = NumberFormat.getCurrencyInstance(LOCALE_USA)
        val PE_CURR_FORMAT: NumberFormat = NumberFormat.getCurrencyInstance(LOCALE_PE)

        const val WHATSAPP_MX = "https://wa.me/525552510001"
        const val WHATSAPP_CO = "https://wa.me/5715800822"
        const val WHATSAPP_USA = "https://wa.me/12134230411"
        const val WHATSAPP_PE = "https://wa.me/5116449082"
        const val WHATSAPP_PACKAGE = "com.whatsapp"

        const val DEFAULT_FEE_PCT = 0.05

        //POSIBLES STATUS DE USUARIO MOBILECARD
        const val USER_STATUS_BLOQUEADO = 0
        const val USER_STATUS_ACTIVO = 1
        const val USER_STATUS_FRAUDE = 3
        const val USER_STATUS_PASS_RESET = 98
        const val USER_STATUS_EMAIL_VERIFICATION = 99
        const val USER_STATUS_SMS_VERIFICATION = 100

        //POSIBLES STATUS DE JUMIO
        const val JUMIO_STATUS_NOT_ENROLLED = "0"
        const val JUMIO_STATUS_ENROLLED = "1"
        const val JUMIO_STATUS_PROCESSING = "2"
        const val JUMIO_STATUS_REJECTED = "3"

        fun getNumberFormat(idPais: Int): NumberFormat {
            return when (idPais) {
                PAIS_ID_MX -> MX_CURR_FORMAT
                PAIS_ID_CO -> CO_CURR_FORMAT
                PAIS_ID_USA -> USA_CURR_FORMAT
                PAIS_ID_PE -> PE_CURR_FORMAT
                else -> USA_CURR_FORMAT
            }
        }

    }
}