package addcel.mobilecard;

/**
 * ADDCEL on 2/28/19.
 */
public final class McRegexPatterns {
    public static final String TEBCA = "^(?=\\d{16}$)(483039)\\d+";
    public static final String PREVIVALE = "^(?=\\d{16}$)(506450)\\d+";
    public static final String PREVIVALE_OR_EMPTY = "(^(?=\\d{16}$)(506450)\\d+)?";
    public static final String TRANSFER_CLABE = "^\\d{18}$";
    public static final String TRANSFER_ACCOUNT = "^\\d{10,11}$";
    public static final String DECIMAL_AMOUNT = "\\d*\\.?\\d+";
    public static final String CURRENCY_AMOUNT = "(^\\d{1,3}(\\.?\\d{3})*(,\\d{2})?$)|(^\\d{1,3}(,?\\d{3})*(\\.\\d{2})?$)";
    public static final String MX_ZIP_CODE = "^[0-9]{5}$";
    public static final String RFC_OR_EMPTY =
            "([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?)?";
    public static final String CURP =
            "[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]";
    public static final String SSN =
            "^(?!219099999|078051120)(?!666|000|9\\d{2})\\d{3}(?!00)\\d{2}(?!0{4})\\d{4}$";

    public static final String PATTERN_DNI = "/^\\d{8}(?:[-\\s]\\d{4})?$/";
    public static final String PASSWORD = "^.{8,12}$";

    private McRegexPatterns() {
    }
}
