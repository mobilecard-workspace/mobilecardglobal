package addcel.mobilecard.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * ADDCEL on 22/09/16.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerView {
}
