package addcel.mobilecard.di.module;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

import com.google.gson.Gson;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.CatalogoAPI;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.transacto.TransactoService;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.data.net.viamericas.ViamericasService;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import timber.log.Timber;

@Module
public class NetModule {

    private static final int CONN_TIMEOUT = 10;
    private static final int READ_TIMEOUT = 40;
    private static final int WRITE_TIMEOUT = 80;

    private static final int LONG_CONN_TIMEOUT = 20;
    private static final int LONG_READ_TIMEOUT = 80;
    private static final int LONG_WRITE_TIMEOUT = 160;

    private final String baseUrl;


    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    String getBaseUrl() {
        return baseUrl;
    }

    @Provides
    @Singleton
    @Named("baseUrl")
    String provideBaseUrl() {
        return baseUrl;
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    ConnectivityManager providesConnectivityManager(Application application) {
        return (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor providesInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(message -> {
            message = message.replaceAll("(?:\"img_ine\":\")(.*?)(?:\",)", "");
            message = message.replaceAll("(?:\"img_domicilio\":\")(.*?)(?:\",)", "");
            Timber.d(message);
        });
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {

        final CertificatePinner pinner = new CertificatePinner.Builder()
                .add("www.mobilecard.mx", "sha256/Vjs8r4z+80wjNcr1YKepWQboSIRi63WsWXhIMN+eWys=", "sha256/Ko8tivDrEjiY90yGasP6ZpBU4jwXvHqVvQI0GS3GNdA=")
                .add("*.spykemobile.net", "sha256/Ko8tivDrEjiY90yGasP6ZpBU4jwXvHqVvQI0GS3GNdA=", "sha256/VjLZe/p3W/PJnd6lL8JVNBCGQBZynFLdZSTIqcO0SJ8=")
                .add("aptopayments.com", "sha256/++MBgDH5WGvL9Bcn5Be30cRcL0f5O+NyoXuWtQdX1aI=", "sha256/KwccWaCgrnaw6tsrrSO61FgLacNgG2MMLq8GE6+oP5I=", "sha256/FfFKxFycfaIz00eRZOgTf+Ne4POK6FgYPwhBDqgqxLQ=")
                .add("vault.ux.8583.io", "sha256/Vjs8r4z+80wjNcr1YKepWQboSIRi63WsWXhIMN+eWys=")
                .build();

        final OkHttpClient.Builder builder = new OkHttpClient.Builder().certificatePinner(pinner)
                .connectTimeout(CONN_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(interceptor).build();
        }

        return builder.build();
    }

    @Provides
    @Singleton
    @Named("longClient")
    OkHttpClient provideCacheClient(OkHttpClient client,
                                    Cache cache) {
        return client.newBuilder()
                .cache(cache)
                .connectTimeout(LONG_CONN_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(LONG_READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(LONG_WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    OkHttp3Downloader provideDownloader(
            @Named("longClient") OkHttpClient client) {
        return new OkHttp3Downloader(client);
    }

    @Provides
    @Singleton
    Picasso providePicasso(Application application,
                           OkHttp3Downloader downloader) {
        return new Picasso.Builder(application).downloader(downloader).build();
    }

    @Provides
    @Singleton
    Retrofit provideMcRetrofit(Gson gson, OkHttpClient client) {
        return new Retrofit.Builder().client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }

    @Provides
    @Singleton
    @Named("cacheRetrofit")
    Retrofit provideCacheRetrofit(
            @Named("longClient") OkHttpClient client, Retrofit retrofit) {
        return retrofit.newBuilder().client(client).baseUrl(BuildConfig.BASE_URL).build();
    }

    @Provides
    @Singleton
    @Named("ScalarRetrofit")
    Retrofit provideScalarRetrofit(
            OkHttpClient client) {
        return new Retrofit.Builder().client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }

    @Provides
    @Singleton
    UsuariosService providesMobilecardService(Retrofit retrofit) {
        return retrofit.create(UsuariosService.class);
    }

    @Provides
    @Singleton
    CatalogoService providesCatalogoService(
            @Named("cacheRetrofit") Retrofit retrofit) {
        return retrofit.create(CatalogoService.class);
    }

    @Provides
    @Singleton
    TransactoService providesTransactoService(
            @Named("ScalarRetrofit") Retrofit retrofit) {
        return retrofit.create(TransactoService.class);
    }

    @Provides
    @Singleton
    WalletAPI providesWalletAPI(@Named("cacheRetrofit") Retrofit retrofit) {
        return WalletAPI.Companion.get(retrofit);
    }

    @Provides
    @Singleton
    CatalogoAPI provideCatalogoAPI(@Named("cacheRetrofit") Retrofit retrofit) {
        return CatalogoAPI.Companion.get(retrofit);
    }

    @Provides
    @Singleton
    ViamericasService provideVmService(Retrofit retrofit) {
        return ViamericasService.Companion.get(retrofit);
    }
}
