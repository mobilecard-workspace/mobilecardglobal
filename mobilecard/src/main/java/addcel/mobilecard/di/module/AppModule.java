package addcel.mobilecard.di.module;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import androidx.room.Room;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.squareup.otto.Bus;

import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.db.favoritos.database.McDatabaseRx;
import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.local.shared.usuario.UserSession;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.UiEventBus;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    SessionOperations providesSession(SharedPreferences preferences, Gson gson) {
        return new UserSession(preferences, gson);
    }

    @Provides
    @Singleton
    StateSession provideState(SharedPreferences preferences) {
        return StateSession.Companion.build(preferences);
    }

    @Singleton
    @Provides
    McDatabaseRx providesRoomDatabase() {
        return Room.databaseBuilder(mApplication, McDatabaseRx.class, "mc-db")
                // .allowMainThreadQueries()
                .fallbackToDestructiveMigration().build();
    }

    @Singleton
    @Provides
    FavoritoDaoRx providesProductDao(McDatabaseRx demoDatabase) {
        return demoDatabase.favoritoDao();
    }

    @Provides
    @Singleton
    Resources providesResources() {
        return mApplication.getResources();
    }

    @Provides
    @Singleton
    NumberFormat provideCurrencyFormat() {
        return NumberFormat.getCurrencyInstance(Locale.US);
    }

    @Provides
    @Singleton
    Bus providesBus() {
        return UiEventBus.getInstance();
    }

    @Provides
    @Singleton
    @Named("iconMap")
    Map<Integer, Integer> provideServicioLabelMap() {
        final Map<Integer, Integer> map = new LinkedHashMap<>();
        map.put(1, R.drawable.cfe);
        map.put(3, R.drawable.sky);
        map.put(4, R.drawable.aguas_monterrey);
        map.put(5, R.drawable.sapao);
        map.put(6, R.drawable.multimedios);
        map.put(7, R.drawable.maxcom);
        map.put(8, R.drawable.telenor);
        map.put(9, R.drawable.gas_natural);
        map.put(10, R.drawable.dish);
        map.put(11, R.drawable.aguakan);
        map.put(12, R.drawable.siapa);
        map.put(13, R.drawable.multimedios);
        map.put(14, R.drawable.axtel);
        map.put(15, R.drawable.global_card);
        map.put(16, R.drawable.jumapa);
        map.put(17, R.drawable.adt);
        map.put(18, R.drawable.cmapas);
        map.put(19, R.drawable.payment);
        map.put(20, R.drawable.cdmx); // TODO falta icono blanco
        map.put(21, R.drawable.edomex);
        map.put(23, R.drawable.citi);
        map.put(24, R.drawable.michoacan);
        map.put(39, R.drawable.jalisco);
        map.put(45, R.drawable.izzi);
        map.put(46, R.drawable.interjet);
        map.put(47, R.drawable.megacable);
        map.put(48, R.drawable.telmex);
        map.put(49, R.drawable.izzi);
        map.put(50, R.drawable.megacable);
        map.put(51, R.drawable.telcel);
        map.put(52, R.drawable.att); // NEXTEL
        map.put(53, R.drawable.aguas_saltillo);
        map.put(54, R.drawable.jad);
        map.put(55, R.drawable.jmas);
        map.put(56, R.drawable.transpais);
        map.put(57, R.drawable.rednovo);
        map.put(58, R.drawable.infonavit);
        map.put(59, R.drawable.copsis);
        map.put(60, R.drawable.scapsatm); // TODO falta icono blanco
        return map;
    }

    @Provides
    @Singleton
    @Named("recargaMap")
    Map<Integer, Integer> provideRecargaLabelMap() {
        final Map<Integer, Integer> map = new LinkedHashMap<>();
        map.put(1, R.drawable.telcel);
        map.put(2, R.drawable.movistar);
        map.put(3, R.drawable.att);
        map.put(4, R.drawable.nextel);
        map.put(5, R.drawable.att);
        map.put(6, R.drawable.idmexico);
        map.put(7, R.drawable.todito);
        map.put(8, R.drawable.pase);
        return map;
    }

    @Provides
    @Singleton
    ViewDataAdapter<TextInputLayout, String> provideTilAdapter() {
        return new ViewDataAdapter<TextInputLayout, String>() {
            @Override
            public String getData(TextInputLayout view) {
                return AndroidUtils.getText(view.getEditText());
            }
        };
    }
}
