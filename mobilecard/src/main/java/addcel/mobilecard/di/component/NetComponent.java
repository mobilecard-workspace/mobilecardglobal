package addcel.mobilecard.di.component;

import javax.inject.Singleton;

import addcel.mobilecard.di.module.AppModule;
import addcel.mobilecard.di.module.NetModule;
import addcel.mobilecard.domain.push.McFirebaseMessagingService;
import addcel.mobilecard.ui.keyboard.KeyboardService;
import addcel.mobilecard.ui.keyboard.beneficiarios.KeyboardBeneficiariosSelectModule;
import addcel.mobilecard.ui.keyboard.beneficiarios.KeyboardBeneficiariosSelectSubcomponent;
import addcel.mobilecard.ui.keyboard.confirm.KeyboardConfirmModule;
import addcel.mobilecard.ui.keyboard.confirm.KeyboardConfirmSubcomponent;
import addcel.mobilecard.ui.keyboard.monto.KeyboardMontoModule;
import addcel.mobilecard.ui.keyboard.monto.KeyboardMontoSubcomponent;
import addcel.mobilecard.ui.keyboard.secure.KeyboardWebModule;
import addcel.mobilecard.ui.keyboard.secure.KeyboardWebSubcomponent;
import addcel.mobilecard.ui.keyboard.wallet.KeyboardWalletModule;
import addcel.mobilecard.ui.keyboard.wallet.KeyboardWalletSubcomponent;
import addcel.mobilecard.ui.launch.LaunchModule;
import addcel.mobilecard.ui.launch.LaunchSubcomponent;
import addcel.mobilecard.ui.login.negocio.LoginNegocioModule;
import addcel.mobilecard.ui.login.negocio.LoginNegocioSubcomponent;
import addcel.mobilecard.ui.login.pais.LoginPaisModule;
import addcel.mobilecard.ui.login.pais.LoginPaisSubcomponent;
import addcel.mobilecard.ui.login.tipo.LoginTipoModule;
import addcel.mobilecard.ui.login.tipo.LoginTipoSubcomponent;
import addcel.mobilecard.ui.login.usuario.LoginUsuarioModule;
import addcel.mobilecard.ui.login.usuario.LoginUsuarioSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroModule;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.create.qr.NegocioCobroCreateQrModule;
import addcel.mobilecard.ui.negocio.mx.cobro.create.qr.NegocioCobroCreateQrSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.menu.NegocioCobroMenuModule;
import addcel.mobilecard.ui.negocio.mx.cobro.menu.NegocioCobroMenuSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.scan.NegocioCobroQrScanModule;
import addcel.mobilecard.ui.negocio.mx.cobro.scan.NegocioCobroQrScanSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.secure.NegocioCobroSecureModule;
import addcel.mobilecard.ui.negocio.mx.cobro.secure.NegocioCobroSecureSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.card.NegocioCobroUserCardCardModule;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.card.NegocioCobroUserCardCardSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.form.NegocioCobroUserCardFormModule;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.form.NegocioCobroUserCardFormSubcomponent;
import addcel.mobilecard.ui.negocio.mx.historial.NegocioHistorialModule;
import addcel.mobilecard.ui.negocio.mx.historial.NegocioHistorialSubcomponent;
import addcel.mobilecard.ui.negocio.mx.password.NegocioPasswordModule;
import addcel.mobilecard.ui.negocio.mx.password.NegocioPasswordSubcomponent;
import addcel.mobilecard.ui.negocio.mx.registro.domicilio.NegocioRegistroDomicilioModule;
import addcel.mobilecard.ui.negocio.mx.registro.domicilio.NegocioRegistroDomicilioSubcomponent;
import addcel.mobilecard.ui.negocio.mx.registro.tarjeta.NegocioRegistroTarjetaModule;
import addcel.mobilecard.ui.negocio.mx.registro.tarjeta.NegocioRegistroTarjetaSubcomponent;
import addcel.mobilecard.ui.negocio.mx.wallet.NegocioWalletModule;
import addcel.mobilecard.ui.negocio.mx.wallet.NegocioWalletSubcomponent;
import addcel.mobilecard.ui.negocio.mx.wallet.card.NegocioWalletCardModule;
import addcel.mobilecard.ui.negocio.mx.wallet.card.NegocioWalletCardSubcomponent;
import addcel.mobilecard.ui.negocio.mx.wallet.registro.NegocioWalletRegistroModule;
import addcel.mobilecard.ui.negocio.mx.wallet.registro.NegocioWalletRegistroSubcomponent;
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletModule;
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletSubcomponent;
import addcel.mobilecard.ui.negocio.mx.wallet.update.account.form.NegocioMxWalletAccountFormModule;
import addcel.mobilecard.ui.negocio.mx.wallet.update.account.form.NegocioMxWalletAccountFormSubcomponent;
import addcel.mobilecard.ui.negocio.mx.wallet.update.activation.NegocioMxWalletActivationModule;
import addcel.mobilecard.ui.negocio.mx.wallet.update.activation.NegocioMxWalletActivationSubcomponent;
import addcel.mobilecard.ui.negocio.pe.generaqr.qr.NegocioPeGeneraQrQrModule;
import addcel.mobilecard.ui.negocio.pe.generaqr.qr.NegocioPeGeneraQrQrSubcomponent;
import addcel.mobilecard.ui.negocio.pe.id.NegocioPeIdQRActivity;
import addcel.mobilecard.ui.negocio.pe.menu.NegocioMenuPeActivity;
import addcel.mobilecard.ui.negocio.pe.registro.second.NegocioRegistroPe2Module;
import addcel.mobilecard.ui.negocio.pe.registro.second.NegocioRegistroPe2Subcomponent;
import addcel.mobilecard.ui.negocio.pe.registro.third.NegocioRegistroPe3Module;
import addcel.mobilecard.ui.negocio.pe.registro.third.NegocioRegistroPe3Subcomponent;
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.tarjeta.NegocioPeTarjetaPresenteTarjetaModule;
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.tarjeta.NegocioPeTarjetaPresenteTarjetaSubcomponent;
import addcel.mobilecard.ui.negocio.pe.wallet.NegocioPeWalletModule;
import addcel.mobilecard.ui.negocio.pe.wallet.NegocioPeWalletSubcomponent;
import addcel.mobilecard.ui.negocio.pe.wallet.account.form.NegocioPeWalletAccountFormModule;
import addcel.mobilecard.ui.negocio.pe.wallet.account.form.NegocioPeWalletAccountFormSubcomponent;
import addcel.mobilecard.ui.negocio.pe.wallet.activation.NegocioPeWalletActivationModule;
import addcel.mobilecard.ui.negocio.pe.wallet.activation.NegocioPeWalletActivationSubcomponent;
import addcel.mobilecard.ui.password.reset.ResetPasswordModule;
import addcel.mobilecard.ui.password.reset.ResetPasswordSubcomponent;
import addcel.mobilecard.ui.sms.SmsModule;
import addcel.mobilecard.ui.sms.SmsSubcomponent;
import addcel.mobilecard.ui.usuario.IncludeMenuActivity;
import addcel.mobilecard.ui.usuario.aci.AciModule;
import addcel.mobilecard.ui.usuario.aci.AciSubcomponent;
import addcel.mobilecard.ui.usuario.aci.confirmation.AciConfirmationModule;
import addcel.mobilecard.ui.usuario.aci.confirmation.AciConfirmationSubcomponent;
import addcel.mobilecard.ui.usuario.aci.processing.AciProcessingModule;
import addcel.mobilecard.ui.usuario.aci.processing.AciProcessingSubcomponent;
import addcel.mobilecard.ui.usuario.apto.cards.activation.CardActivationActivity;
import addcel.mobilecard.ui.usuario.apto.cards.created.CardCreatedActivity;
import addcel.mobilecard.ui.usuario.apto.user.address.AptoUserAddressActivity;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureModule;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureSubcomponent;
import addcel.mobilecard.ui.usuario.blackstone.carriers.BSCarrierModule;
import addcel.mobilecard.ui.usuario.blackstone.carriers.BSCarrierSubcomponent;
import addcel.mobilecard.ui.usuario.blackstone.confirm.BSConfirmModule;
import addcel.mobilecard.ui.usuario.blackstone.confirm.BSConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.blackstone.monto.BSMontoModule;
import addcel.mobilecard.ui.usuario.blackstone.monto.BSMontoSubcomponent;
import addcel.mobilecard.ui.usuario.blackstone.products.BSProductModule;
import addcel.mobilecard.ui.usuario.blackstone.products.BSProductSubcomponent;
import addcel.mobilecard.ui.usuario.blackstone.result.BSResultModule;
import addcel.mobilecard.ui.usuario.blackstone.result.BSResultSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.compra.MmProductosConfirmModule;
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.compra.MmProductosConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.lista.MmProductosListModule;
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.lista.MmProductosListSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.categorias.MmCategoriasModule;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.categorias.MmCategoriasSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.compra.MmServiciosConfirmModule;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.compra.MmServiciosConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.monto.MmServicioMontoModule;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.monto.MmServicioMontoSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search.MmServicioSearchModule;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search.MmServicioSearchSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.ColombiaRecargaPaqueteDatosSucomponent;
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.ColombiaRecargaPaquetesDatosModule;
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.confirm.ColombiaRecargaPaquetesConfirmModule;
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.confirm.ColombiaRecargaPaquetesConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.seleccion.ColombiaRecargaPaquetesSeleccionModule;
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.seleccion.ColombiaRecargaPaquetesSeleccionSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.recargas.operadores.ColombiaRecargaOperadorModule;
import addcel.mobilecard.ui.usuario.colombia.recargas.operadores.ColombiaRecargaOperadorSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.recargas.tae.confirm.ColombiaRecargaTaeConfirmModule;
import addcel.mobilecard.ui.usuario.colombia.recargas.tae.confirm.ColombiaRecargaTaeConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.recargas.tae.seleccion.ColombiaRecargaTaeSeleccionModule;
import addcel.mobilecard.ui.usuario.colombia.recargas.tae.seleccion.ColombiaRecargaTaeSeleccionSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.result.ColombiaResultModule;
import addcel.mobilecard.ui.usuario.colombia.result.ColombiaResultSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.soad.info.ColombiaSoatInfoModule;
import addcel.mobilecard.ui.usuario.colombia.soad.info.ColombiaSoatInfoSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.soad.processing.ColombiaSoadProcessingModule;
import addcel.mobilecard.ui.usuario.colombia.soad.processing.ColombiaSoadProcessingSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.confirm.ViarapidaConfirmModule;
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.confirm.ViarapidaConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.seleccion.ViarapidaSeleccionModule;
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.seleccion.ViarapidaSeleccionSubcomponent;
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.tag.ViarapidaTagModule;
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.tag.ViarapidaTagSubcomponent;
import addcel.mobilecard.ui.usuario.edocuenta.EdocuentaActivity;
import addcel.mobilecard.ui.usuario.edocuenta.list.EdocuentaListModule;
import addcel.mobilecard.ui.usuario.edocuenta.list.EdocuentaListSubcomponent;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add.MxTransferBeneficiarioAddModule;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add.MxTransferBeneficiarioAddSubcomponent;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select.MxTransferBeneficiarioSelectModule;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select.MxTransferBeneficiarioSelectSubcomponent;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.update.MxTransferBeneficiarioUpdateModule;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.update.MxTransferBeneficiarioUpdateSubcomponent;
import addcel.mobilecard.ui.usuario.endtoend.processing.MxTransferProcessingModule;
import addcel.mobilecard.ui.usuario.endtoend.processing.MxTransferProcessingSubcomponent;
import addcel.mobilecard.ui.usuario.ingo.IngoModule;
import addcel.mobilecard.ui.usuario.ingo.IngoSubcomponent;
import addcel.mobilecard.ui.usuario.ingo.processing.IngoProcessingModule;
import addcel.mobilecard.ui.usuario.ingo.processing.IngoProcessingSubcomponent;
import addcel.mobilecard.ui.usuario.ingo.registro.IngoRegistroModule;
import addcel.mobilecard.ui.usuario.ingo.registro.IngoRegistroSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.recargas.CarrierSelectionModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.CarrierSelectionSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.recargas.list.RecargaListModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.list.RecargaListSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.confirm.RecargaConfirmModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.confirm.RecargaConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.RecargaModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.RecargaSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.confirm.PeajeConfirmModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.confirm.PeajeConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.confirm.MobileTagConfirmModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.confirm.MobileTagConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.select.MobileTagSelectModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.select.MobileTagSelectSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select.PeajeNewModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select.PeajeNewSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.servicios.categorias.CategoriaListModule;
import addcel.mobilecard.ui.usuario.legacy.servicios.categorias.CategoriaListSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.servicios.confirm.ServicioConfirmModule;
import addcel.mobilecard.ui.usuario.legacy.servicios.confirm.ServicioConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.servicios.consulta.ServicioConsultaModule;
import addcel.mobilecard.ui.usuario.legacy.servicios.consulta.ServicioConsultaSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.servicios.directo.ServicioModule;
import addcel.mobilecard.ui.usuario.legacy.servicios.directo.ServicioSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.servicios.list.ServicioSelectionModule;
import addcel.mobilecard.ui.usuario.legacy.servicios.list.ServicioSelectionSubcomponent;
import addcel.mobilecard.ui.usuario.main.MainModule;
import addcel.mobilecard.ui.usuario.main.MainSubcomponent;
import addcel.mobilecard.ui.usuario.main.contacto.ContactoModule;
import addcel.mobilecard.ui.usuario.main.contacto.ContactoSubcomponent;
import addcel.mobilecard.ui.usuario.main.favoritos.FavoritosModule;
import addcel.mobilecard.ui.usuario.main.favoritos.FavoritosSubcomponent;
import addcel.mobilecard.ui.usuario.main.menu.Menu2Module;
import addcel.mobilecard.ui.usuario.main.menu.Menu2Subcomponent;
import addcel.mobilecard.ui.usuario.main.pais.recarga.RecargaPaisModule;
import addcel.mobilecard.ui.usuario.main.pais.recarga.RecargaPaisSubcomponent;
import addcel.mobilecard.ui.usuario.main.pais.servicio.PaisModule;
import addcel.mobilecard.ui.usuario.main.pais.servicio.PaisSubcomponent;
import addcel.mobilecard.ui.usuario.menu.BottomMenuModule;
import addcel.mobilecard.ui.usuario.menu.BottomMenuSubcomponent;
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardModule;
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardSubcomponent;
import addcel.mobilecard.ui.usuario.mymobilecard.display.MyMobilecardDisplayModule;
import addcel.mobilecard.ui.usuario.mymobilecard.display.MyMobilecardDisplaySubcomponent;
import addcel.mobilecard.ui.usuario.mymobilecard.previvale.activate.MyMobilecardPrevivaleActivateModule;
import addcel.mobilecard.ui.usuario.mymobilecard.previvale.activate.MyMobilecardPrevivaleActivateSubcomponent;
import addcel.mobilecard.ui.usuario.mymobilecard.previvale.historial.PrevivaleHistorialModule;
import addcel.mobilecard.ui.usuario.mymobilecard.previvale.historial.PrevivaleHistorialSubcomponent;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormModule;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormSubcomponent;
import addcel.mobilecard.ui.usuario.password.PasswordModule;
import addcel.mobilecard.ui.usuario.password.PasswordSubcomponent;
import addcel.mobilecard.ui.usuario.perfil.Perfil2Module;
import addcel.mobilecard.ui.usuario.perfil.Perfil2Subcomponent;
import addcel.mobilecard.ui.usuario.perfil.photo.PerfilPhotoActivity;
import addcel.mobilecard.ui.usuario.registro.create.RegistroCreateModule;
import addcel.mobilecard.ui.usuario.registro.create.RegistroCreateSubcomponent;
import addcel.mobilecard.ui.usuario.registro.email.EmailRegistroModule;
import addcel.mobilecard.ui.usuario.registro.email.EmailRegistroSubcomponent;
import addcel.mobilecard.ui.usuario.registro.formapago.RegistroFormaPagoModule;
import addcel.mobilecard.ui.usuario.registro.formapago.RegistroFormaPagoSubcomponent;
import addcel.mobilecard.ui.usuario.registro.info.RegistroPersonalInfoModule;
import addcel.mobilecard.ui.usuario.registro.info.RegistroPersonalInfoSubcomponent;
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroModule;
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.manual.confirm.ScanManualConfirmModule;
import addcel.mobilecard.ui.usuario.scanpay.manual.confirm.ScanManualConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.manual.monto.ScanManualMontoModule;
import addcel.mobilecard.ui.usuario.scanpay.manual.monto.ScanManualMontoSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.manual.select.ScanManualSelectEstablecimientoModule;
import addcel.mobilecard.ui.usuario.scanpay.manual.select.ScanManualSelectEstablecimientoSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.menu.ScanMenuModule;
import addcel.mobilecard.ui.usuario.scanpay.menu.ScanMenuSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.qr.ScanQrModule;
import addcel.mobilecard.ui.usuario.scanpay.qr.ScanQrSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.qr.confirm.ScanQrConfirmModule;
import addcel.mobilecard.ui.usuario.scanpay.qr.confirm.ScanQrConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.qr.id.ScanQrIdModule;
import addcel.mobilecard.ui.usuario.scanpay.qr.id.ScanQrIdSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.qr.mx.monto.SpMxMontoQrKeyboardModule;
import addcel.mobilecard.ui.usuario.scanpay.qr.mx.monto.SpMxMontoQrKeyboardSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.qr.pe.qr.ScanPeQrMontoModule;
import addcel.mobilecard.ui.usuario.scanpay.qr.pe.qr.ScanPeQrMontoSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.result.qr.ScanResultQrModule;
import addcel.mobilecard.ui.usuario.scanpay.result.qr.ScanResultQrSubcomponent;
import addcel.mobilecard.ui.usuario.scanpay.secure.ScanSecureModule;
import addcel.mobilecard.ui.usuario.scanpay.secure.ScanSecureSubcomponent;
import addcel.mobilecard.ui.usuario.secure.WebModule;
import addcel.mobilecard.ui.usuario.secure.WebSubcomponent;
import addcel.mobilecard.ui.usuario.viamericas.VmActivity;
import addcel.mobilecard.ui.usuario.viamericas.confirmation.ViamericasConfirmModule;
import addcel.mobilecard.ui.usuario.viamericas.confirmation.ViamericasConfirmSubcomponent;
import addcel.mobilecard.ui.usuario.viamericas.fees.ViamericasFeeModule;
import addcel.mobilecard.ui.usuario.viamericas.fees.ViamericasFeeSubcomponent;
import addcel.mobilecard.ui.usuario.viamericas.order.ViamericasOrderModule;
import addcel.mobilecard.ui.usuario.viamericas.order.ViamericasOrderSubcomponent;
import addcel.mobilecard.ui.usuario.viamericas.recipient.create.ViamericasRecipientCreateModule;
import addcel.mobilecard.ui.usuario.viamericas.recipient.create.ViamericasRecipientCreateSubcomponent;
import addcel.mobilecard.ui.usuario.viamericas.recipient.select.ViamericasRecipientModule;
import addcel.mobilecard.ui.usuario.viamericas.recipient.select.ViamericasRecipientSubcomponent;
import addcel.mobilecard.ui.usuario.viamericas.result.ViamericasResultFragment;
import addcel.mobilecard.ui.usuario.viamericas.sender.ViamericasSenderModule;
import addcel.mobilecard.ui.usuario.viamericas.sender.ViamericasSenderSubcomponent;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import addcel.mobilecard.ui.usuario.wallet.cards.WalletCardsModule;
import addcel.mobilecard.ui.usuario.wallet.cards.WalletCardsSubcomponent;
import addcel.mobilecard.ui.usuario.wallet.create.WalletCreateModule;
import addcel.mobilecard.ui.usuario.wallet.create.WalletCreateSubcomponent;
import addcel.mobilecard.ui.usuario.wallet.create.pay.WalletCreatePayModule;
import addcel.mobilecard.ui.usuario.wallet.create.pay.WalletCreatePaySubcomponent;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectModule;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectSubcomponent;
import addcel.mobilecard.ui.usuario.wallet.select.add.WalletSelectAddModule;
import addcel.mobilecard.ui.usuario.wallet.select.add.WalletSelectAddSubcomponent;
import addcel.mobilecard.ui.usuario.wallet.select.custom.WalletSelectCustomModule;
import addcel.mobilecard.ui.usuario.wallet.select.custom.WalletSelectCustomSubcomponent;
import addcel.mobilecard.ui.usuario.wallet.update.WalletUpdateModule;
import addcel.mobilecard.ui.usuario.wallet.update.WalletUpdateSubcomponent;
import dagger.Component;

/**
 * ADDCEL on 26/10/16.
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {

    void inject(EdocuentaActivity activity);

    void inject(WalletContainerActivity activity);

    void inject(PagoContainerActivity activity);

    void inject(MxTransferActivity activity);

    void inject(VmActivity activity);

    void inject(KeyboardService service);

    void inject(IncludeMenuActivity activity);

    void inject(NegocioMenuPeActivity activity);

    void inject(NegocioPeIdQRActivity activity);

    void inject(PerfilPhotoActivity activity);

    void inject(ViamericasResultFragment fragment);

    void inject(McFirebaseMessagingService service);

    void inject(AptoUserAddressActivity activity);

    void inject(CardCreatedActivity activity);

    void inject(CardActivationActivity activationActivity);

    void inject(MyMcRoutingActivity activity);

    // LANZAMIENTO

    LaunchSubcomponent splashSubcomponent(LaunchModule module);

    // LOGIN
    LoginPaisSubcomponent paisSubcomponent(LoginPaisModule module);

    LoginTipoSubcomponent usuarioSubcomponent(LoginTipoModule module);

    LoginUsuarioSubcomponent loginUsuarioSubcomponent(LoginUsuarioModule module);

    LoginNegocioSubcomponent loginNegocioSubcomponent(LoginNegocioModule module);

    // USUARIO REGISTRO
    RegistroCreateSubcomponent registroCreateSubcomponent(RegistroCreateModule module);

    RegistroPersonalInfoSubcomponent registroPersonalInfoSubcomponent(
            RegistroPersonalInfoModule module);

    RegistroFormaPagoSubcomponent registroFormaPagoSubcomponent(RegistroFormaPagoModule module);

    //RESET
    ResetPasswordSubcomponent resetPasswordSubcomponent(ResetPasswordModule module);

    // HISTORIAL

    EdocuentaListSubcomponent edocuentaListSubcomponent(EdocuentaListModule module);

    // MY MC
    MyMobilecardSubcomponent myMobilecardSubcomponent(MyMobilecardModule module);

    MyMobilecardDisplaySubcomponent myMobilecardDisplaySubcomponent(MyMobilecardDisplayModule module);

    PrevivaleHistorialSubcomponent previvaleHistorialSubcomponent(PrevivaleHistorialModule module);

    MainSubcomponent mainSubcomponent(MainModule module);

    ContactoSubcomponent contactoSubcomponent(ContactoModule module);

    Menu2Subcomponent menu2Subcomponent(Menu2Module module);

    PaisSubcomponent paisSubcomponent(PaisModule module);

    FavoritosSubcomponent favoritosSubcomponent(FavoritosModule module);

    Perfil2Subcomponent perfil2Subcomponent(Perfil2Module module);

    WalletCardsSubcomponent walletCardsSubcomponent(WalletCardsModule module);

    WalletUpdateSubcomponent walletUpdateSubcomponent(WalletUpdateModule module);

    WalletCreateSubcomponent walletCreateSubcomponent(WalletCreateModule module);

    WalletCreatePaySubcomponent walletCreatePaySubcomponent(WalletCreatePayModule module);

    WalletSelectSubcomponent walletSelectSubcomponent(WalletSelectModule module);

    WalletSelectAddSubcomponent walletSelectAddSubcomponent(WalletSelectAddModule module);

    WalletSelectCustomSubcomponent walletSelectCustomSubcomponent(WalletSelectCustomModule module);

    TebcaFormSubcomponent tebcaFormSubcomponent(TebcaFormModule module);

    RecargaPaisSubcomponent recargaPaisSubcomponent(RecargaPaisModule module);

    RecargaListSubcomponent recargaListSubcomponent(RecargaListModule module);

    RecargaConfirmSubcomponent recargaConfirmFragment(RecargaConfirmModule module);

    PeajeConfirmSubcomponent peajeConfirmSubcomponent(PeajeConfirmModule module);

    CategoriaListSubcomponent categoriaListSubcomponent(CategoriaListModule module);

    ServicioConfirmSubcomponent servicioConfirmSubcomponent(ServicioConfirmModule module);

    CarrierSelectionSubcomponent carrierSelectionSubcomponent(CarrierSelectionModule module);

    ServicioSelectionSubcomponent servicioSelectionSubcomponent(ServicioSelectionModule module);

    ServicioSubcomponent servicioSubcomponent(ServicioModule module);

    ServicioConsultaSubcomponent servicioConsultaSubcomponent(ServicioConsultaModule module);

    RecargaSubcomponent recargaSubcomponent(RecargaModule module);

    MobileTagSelectSubcomponent mobileTagSelectSubcomponent(MobileTagSelectModule module);

    MobileTagConfirmSubcomponent mobileTagConfirmSubcomponent(MobileTagConfirmModule module);

    WebSubcomponent webSubcomponent(WebModule module);

    PasswordSubcomponent passwordSubcomponent(PasswordModule module);

    AciSubcomponent aciSubcomponent(AciModule module);

    AciProcessingSubcomponent aciProcessingSubcomponent(AciProcessingModule module);

    AciConfirmationSubcomponent aciConfirmationSubcomponent(AciConfirmationModule module);

    PeajeNewSubcomponent peajeNewSubcomponent(PeajeNewModule module);

    // H2H
    MxTransferBeneficiarioSelectSubcomponent mxTransferBeneficiarioSelectSubcomponent(
            MxTransferBeneficiarioSelectModule module);

    MxTransferBeneficiarioAddSubcomponent mxTransferBeneficiarioAddSubcomponent(
            MxTransferBeneficiarioAddModule module);

    MxTransferBeneficiarioUpdateSubcomponent mxTransferBeneficiarioUpdateSubcomponent(
            MxTransferBeneficiarioUpdateModule module);

    MxTransferProcessingSubcomponent mxTransferProcessingSubcomponent(
            MxTransferProcessingModule module);

    //Viamericas
    ViamericasSenderSubcomponent viamericasSenderSubcomponent(ViamericasSenderModule module);

    ViamericasOrderSubcomponent viamericasOrderSubcomponent(ViamericasOrderModule module);

    ViamericasRecipientSubcomponent viamericasRecipientSubcomponent(ViamericasRecipientModule module);

    ViamericasRecipientCreateSubcomponent viamericasRecipientCreateSubcomponent(ViamericasRecipientCreateModule module);

    ViamericasFeeSubcomponent viamericasFeeSubcomponent(ViamericasFeeModule module);

    ViamericasConfirmSubcomponent viamericasConfirmSubcomponent(ViamericasConfirmModule module);

    // Blackstone
    BSCarrierSubcomponent bsCarrierSubcomponent(BSCarrierModule module);

    BSProductSubcomponent bsProductSubcomponent(BSProductModule module);

    BSMontoSubcomponent bsMontoSubcomponent(BSMontoModule montoModule);

    BSConfirmSubcomponent bsConfirmSubcomponent(BSConfirmModule module);

    BSResultSubcomponent bsResultSubcomponent(BSResultModule module);

    // Ingo
    IngoSubcomponent ingSubcomponent(IngoModule module);

    IngoRegistroSubcomponent ingoRegistroSubcomponent(IngoRegistroModule module);

    IngoProcessingSubcomponent ingoProcessingSubcomponent(IngoProcessingModule module);

    // KEYBOARD
    KeyboardBeneficiariosSelectSubcomponent keyboardBeneficiariosSelectSubcomponent(
            KeyboardBeneficiariosSelectModule module);

    KeyboardMontoSubcomponent keyboardMontoSubcomponent(KeyboardMontoModule montoModule);

    KeyboardWalletSubcomponent keyboardWalletSubcomponent(KeyboardWalletModule module);

    KeyboardConfirmSubcomponent keyboardConfirmSubcomponent(KeyboardConfirmModule module);

    KeyboardWebSubcomponent keyboardWebSubcomponent(KeyboardWebModule module);

    // Scan&Pay
    ScanMenuSubcomponent scanMenuSubcomponent(ScanMenuModule module);

    ScanQrSubcomponent scanQrSubcomponent(ScanQrModule module);

    SpMxMontoQrKeyboardSubcomponent spMxMontoQrKeyboardSubcomponent(SpMxMontoQrKeyboardModule module);

    ScanQrConfirmSubcomponent scanQrConfirmSubcomponent(ScanQrConfirmModule module);

    ScanQrIdSubcomponent scanQrIdSubcomponent(ScanQrIdModule module);

    ScanManualSelectEstablecimientoSubcomponent scanManualSelectEstablecimientoSubcomponent(
            ScanManualSelectEstablecimientoModule module);

    ScanManualMontoSubcomponent scanManualMontoSubcomponent(ScanManualMontoModule montoModule);

    ScanManualConfirmSubcomponent scanManualConfirmSubcomponent(ScanManualConfirmModule module);

    ScanSecureSubcomponent scanSecureSubcomponent(ScanSecureModule module);

    ScanResultQrSubcomponent scanResultQrSubcomponent(ScanResultQrModule module);

    ScanPeQrMontoSubcomponent scanPeQrMontoSubcomponent(ScanPeQrMontoModule module);

    // Negocio

    NegocioRegistroDomicilioSubcomponent negocioRegistroDomicilioSubcomponent(NegocioRegistroDomicilioModule module);

    NegocioRegistroTarjetaSubcomponent negocioRegistroTarjetaSubcomponent(NegocioRegistroTarjetaModule module);


    NegocioCobroSubcomponent negocioCobroSubcomponent(NegocioCobroModule module);

    NegocioCobroMenuSubcomponent negocioCobroMenuSubcomponent(NegocioCobroMenuModule module);

    NegocioHistorialSubcomponent negocioHistorialSubcomponent(NegocioHistorialModule activity);

    NegocioPasswordSubcomponent negocioPasswordSubcomponent(NegocioPasswordModule module);

    NegocioWalletRegistroSubcomponent negocioWalletRegistroSubcomponent(
            NegocioWalletRegistroModule module);

    NegocioWalletSubcomponent negocioWalletSubcomponent(NegocioWalletModule module);

    NegocioWalletCardSubcomponent negocioWalletCardSubcomponent(NegocioWalletCardModule module);

    NegocioCobroCreateQrSubcomponent negocioCobroCreateQrSubcomponent(
            NegocioCobroCreateQrModule module);

    NegocioCobroUserCardFormSubcomponent negocioCobroUserCardFormSubcomponent(
            NegocioCobroUserCardFormModule module);

    NegocioCobroUserCardCardSubcomponent negocioCobroUserCardCardSubcomponent(
            NegocioCobroUserCardCardModule module);

    NegocioCobroSecureSubcomponent negocioCobroSecureSubcomponent(NegocioCobroSecureModule module);

    NegocioCobroQrScanSubcomponent negocioCobroQrScanSubcomponent(NegocioCobroQrScanModule module);

    // COLOMBIA

    ColombiaResultSubcomponent colombiaResultSubcomponent(ColombiaResultModule module);

    ColombiaRecargaOperadorSubcomponent colombiaRecargaOperadorSubcomponent(
            ColombiaRecargaOperadorModule module);

    ColombiaRecargaPaqueteDatosSucomponent colombiaRecargaPaqueteDatosSucomponent(
            ColombiaRecargaPaquetesDatosModule module);

    ColombiaRecargaPaquetesSeleccionSubcomponent colombiaRecargaPaquetesSeleccionSubcomponent(
            ColombiaRecargaPaquetesSeleccionModule module);

    ColombiaRecargaTaeSeleccionSubcomponent colombiaRecargaTaeSeleccionSubcomponent(
            ColombiaRecargaTaeSeleccionModule module);

    ColombiaRecargaTaeConfirmSubcomponent colombiaRecargaTaeConfirmSubcomponent(
            ColombiaRecargaTaeConfirmModule module);

    ColombiaRecargaPaquetesConfirmSubcomponent colombiaRecargaPaquetesConfirmSubcomponent(
            ColombiaRecargaPaquetesConfirmModule module);

    MmProductosListSubcomponent mmProductosListSubcomponent(MmProductosListModule module);

    MmProductosConfirmSubcomponent mmProductosConfirmSubcomponent(MmProductosConfirmModule module);

    MmCategoriasSubcomponent mmCategoriasSubcomponent(MmCategoriasModule module);

    MmServicioSearchSubcomponent mmServicioSearchSubcomponent(MmServicioSearchModule module);

    MmServicioMontoSubcomponent mmServicioMontoSubcomponent(MmServicioMontoModule module);

    MmServiciosConfirmSubcomponent mmServiciosConfirmSubcomponent(MmServiciosConfirmModule module);


    ViarapidaTagSubcomponent viarapidaTagSubcomponent(ViarapidaTagModule module);

    ViarapidaSeleccionSubcomponent viarapidaSeleccionSubcomponent(ViarapidaSeleccionModule module);

    ViarapidaConfirmSubcomponent viarapidaConfirmSubcomponent(ViarapidaConfirmModule module);

    ColombiaSoatInfoSubcomponent colombiaSoatInfoSubcomponent(ColombiaSoatInfoModule module);

    ColombiaSoadProcessingSubcomponent colombiaSoadProcessingSubcomponent(
            ColombiaSoadProcessingModule module);

    MyMobilecardPrevivaleActivateSubcomponent myMobilecardPrevivaleActivateSubcomponent(
            MyMobilecardPrevivaleActivateModule module);

    // SMS

    SmsSubcomponent smsSubcomponent(SmsModule module);

    SmsRegistroSubcomponent smsRegistroSubcomponent(SmsRegistroModule module);

    // EMAIL
    EmailRegistroSubcomponent emailRegistroSubcomponent(EmailRegistroModule module);

    // MENU
    BottomMenuSubcomponent bottomMenuSubcomponent(BottomMenuModule module);

    //NEGOCIO PERU
    NegocioRegistroPe2Subcomponent negocioRegistroPe2Subcomponent(NegocioRegistroPe2Module module);

    NegocioRegistroPe3Subcomponent negocioRegistroPe3Subcomponent(NegocioRegistroPe3Module module);

    NegocioPeTarjetaPresenteTarjetaSubcomponent negocioPeTarjetaPresenteSubcomponent(
            NegocioPeTarjetaPresenteTarjetaModule module);

    NegocioPeGeneraQrQrSubcomponent negocioPeGeneraQrQrSubcomponent(NegocioPeGeneraQrQrModule module);

    NegocioPeWalletSubcomponent negocioPeWalletSubcomponent(NegocioPeWalletModule module);

    NegocioPeWalletAccountFormSubcomponent negocioPeWalletAccountFormSubcomponent(
            NegocioPeWalletAccountFormModule module);

    NegocioPeWalletActivationSubcomponent negocioPeWalletActivationSubcomponent(
            NegocioPeWalletActivationModule module);

    NegocioMxWalletSubcomponent negocioMxWalletSubcomponent(NegocioMxWalletModule module);

    NegocioMxWalletActivationSubcomponent negocioMxWalletActivationSubcomponent(
            NegocioMxWalletActivationModule module);

    NegocioMxWalletAccountFormSubcomponent negocioMxWalletAccountFormSubcomponent(
            NegocioMxWalletAccountFormModule module);

    BillPocketSecureSubcomponent billPocketSecureSubcomponent(BillPocketSecureModule module);
}
