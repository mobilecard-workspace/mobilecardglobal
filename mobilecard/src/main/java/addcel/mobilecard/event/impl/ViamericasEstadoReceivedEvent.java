package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.VmStateModel;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 27/03/17.
 */

final class ViamericasEstadoReceivedEvent implements BusEvent {
    private final List<VmStateModel> estados;

    public ViamericasEstadoReceivedEvent(List<VmStateModel> estados) {
        this.estados = estados;
    }

    public List<VmStateModel> getEstados() {
        return estados;
    }
}
