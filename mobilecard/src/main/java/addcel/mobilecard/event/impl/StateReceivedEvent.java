package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 28/03/17.
 */
public class StateReceivedEvent implements BusEvent {
    private final List<EstadoResponse.EstadoEntity> estados;

    public StateReceivedEvent(List<EstadoResponse.EstadoEntity> estados) {
        this.estados = estados;
    }

    public List<EstadoResponse.EstadoEntity> getEstados() {
        return estados;
    }
}
