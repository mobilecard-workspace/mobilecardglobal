package addcel.mobilecard.event.impl;

import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 18/04/17.
 */
class PerfilUpdateEvent implements BusEvent {
    private final int adapterPos;
    private final String value;

    public PerfilUpdateEvent(int adapterPos, String value) {
        this.adapterPos = adapterPos;
        this.value = value;
    }

    public int getAdapterPos() {
        return adapterPos;
    }

    public String getValue() {
        return value;
    }
}
