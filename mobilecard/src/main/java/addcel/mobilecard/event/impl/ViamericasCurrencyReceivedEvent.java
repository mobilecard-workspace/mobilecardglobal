package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.CurrencyModel;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 30/03/17.
 */

final class ViamericasCurrencyReceivedEvent implements BusEvent {
    private final List<CurrencyModel> currencies;

    public ViamericasCurrencyReceivedEvent(List<CurrencyModel> currencies) {
        this.currencies = currencies;
    }

    public List<CurrencyModel> getCurrencies() {
        return currencies;
    }
}
