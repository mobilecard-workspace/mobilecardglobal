package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.VmZipModel;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 29/03/17.
 */

class ViamericasZipReceivedEvent implements BusEvent {
    private final List<VmZipModel> zipCodes;

    public ViamericasZipReceivedEvent(List<VmZipModel> zipCodes) {
        this.zipCodes = zipCodes;
    }

    public List<VmZipModel> getZipCodes() {
        return zipCodes;
    }
}
