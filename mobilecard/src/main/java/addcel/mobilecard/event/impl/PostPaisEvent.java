package addcel.mobilecard.event.impl;

import com.google.common.base.MoreObjects;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 21/04/17.
 */
public class PostPaisEvent implements BusEvent {
    private final int idPais;
    private final boolean withUiElements;
    private final boolean hasIngo;

    public PostPaisEvent(int idPais, boolean withUiElements, boolean hasIngo) {
        this.idPais = idPais;
        this.withUiElements = withUiElements;
        this.hasIngo = hasIngo;
    }

    public int getIdPais() {
        return idPais;
    }

    public boolean isWithUiElements() {
        return withUiElements;
    }

    public boolean isHasIngo() {
        return hasIngo;
    }

    @NotNull
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("idPais", idPais)
                .add("withUiElements", withUiElements)
                .add("hasIngo", false)
                .toString();
    }
}
