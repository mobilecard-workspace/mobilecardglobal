package addcel.mobilecard.event.impl;

import addcel.mobilecard.data.net.viamericas.model.RecipientResponse;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 30/03/17.
 */

final class ViamericasRecipientRegisteredEvent implements BusEvent {
    private final RecipientResponse recipientResponse;

    public ViamericasRecipientRegisteredEvent(RecipientResponse recipientResponse) {
        this.recipientResponse = recipientResponse;
    }

    public RecipientResponse getRecipientResponse() {
        return recipientResponse;
    }
}
