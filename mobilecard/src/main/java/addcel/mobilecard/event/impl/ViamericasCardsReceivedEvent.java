package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.OrderResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 28/03/17.
 */

public final class ViamericasCardsReceivedEvent extends CardsReceivedEvent {
    private final List<CardEntity> cards;
    private final OrderResponse order;
    private final boolean withDialog;

    public ViamericasCardsReceivedEvent(List<CardEntity> cards, OrderResponse order,
                                        boolean withDialog) {
        super(cards, withDialog);
        this.cards = cards;
        this.order = order;
        this.withDialog = withDialog;
    }

    public List<CardEntity> getCards() {
        return cards;
    }

    public OrderResponse getOrder() {
        return order;
    }

    public boolean isWithDialog() {
        return withDialog;
    }
}
