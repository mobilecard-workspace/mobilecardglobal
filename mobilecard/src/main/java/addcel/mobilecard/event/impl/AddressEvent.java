package addcel.mobilecard.event.impl;

import android.location.Address;

import java.util.List;

import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 10/07/17.
 */
final class AddressEvent implements BusEvent {
    private final List<Address> addresses;

    public AddressEvent(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return addresses;
    }
}
