package addcel.mobilecard.event.impl;

import addcel.mobilecard.data.net.transacto.entity.SaldoEntity;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 19/04/17.
 */
public class SaldoConsultaEvent implements BusEvent {
    private final int idPais;
    private final SaldoEntity model;

    public SaldoConsultaEvent(int idPais, SaldoEntity model) {
        this.idPais = idPais;
        this.model = model;
    }

    public int getIdPais() {
        return idPais;
    }

    public SaldoEntity getSaldoResponse() {
        return model;
    }
}
