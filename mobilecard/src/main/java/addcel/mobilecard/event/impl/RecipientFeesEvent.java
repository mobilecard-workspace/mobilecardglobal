package addcel.mobilecard.event.impl;

import addcel.mobilecard.data.net.viamericas.model.OrderFeeModel;
import addcel.mobilecard.data.net.viamericas.model.RecipientResponse;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 12/05/17.
 */

class RecipientFeesEvent implements BusEvent {
    private final RecipientResponse recipient;
    private final OrderFeeModel fees;
    private final boolean successful;

    public RecipientFeesEvent(RecipientResponse recipient, OrderFeeModel fees, boolean successful) {
        this.recipient = recipient;
        this.fees = fees;
        this.successful = successful;
    }

    public RecipientResponse getRecipient() {
        return recipient;
    }

    public OrderFeeModel getFees() {
        return fees;
    }

    public boolean isSuccessful() {
        return successful;
    }
}
