package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.VmCityModel;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 29/03/17.
 */

class ViamericasCitiesReceivedEvent implements BusEvent {
    private final List<VmCityModel> cities;

    public ViamericasCitiesReceivedEvent(List<VmCityModel> cities) {
        this.cities = cities;
    }

    public List<VmCityModel> getCities() {
        return cities;
    }
}
