package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.VmCountryModel;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 29/03/17.
 */

final class ViamericasCountriesSenderReceivedEvent implements BusEvent {
    private final List<VmCountryModel> countries;

    public ViamericasCountriesSenderReceivedEvent(List<VmCountryModel> countries) {
        this.countries = countries;
    }

    public List<VmCountryModel> getCountries() {
        return countries;
    }
}
