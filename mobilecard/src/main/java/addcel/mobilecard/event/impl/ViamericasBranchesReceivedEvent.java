package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.LocationModel;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 30/03/17.
 */

final class ViamericasBranchesReceivedEvent implements BusEvent {
    private final List<LocationModel> branches;

    public ViamericasBranchesReceivedEvent(List<LocationModel> branches) {
        this.branches = branches;
    }

    public List<LocationModel> getBranches() {
        return branches;
    }
}
