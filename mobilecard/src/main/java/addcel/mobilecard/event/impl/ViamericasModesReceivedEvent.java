package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.PaymentModeModel;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 30/03/17.
 */

final class ViamericasModesReceivedEvent implements BusEvent {
    private final List<PaymentModeModel> modes;

    public ViamericasModesReceivedEvent(List<PaymentModeModel> modes) {
        this.modes = modes;
    }

    public List<PaymentModeModel> getModes() {
        return modes;
    }
}
