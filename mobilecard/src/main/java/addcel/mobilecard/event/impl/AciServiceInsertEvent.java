package addcel.mobilecard.event.impl;

import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 24/03/17.
 */
public class AciServiceInsertEvent implements BusEvent {
    public static final int ITEM_LOAD_FINISHED = -1000;
    public static final int ITEM_LOAD_ERROR = -2000;
    private final int pos;

    public AciServiceInsertEvent(int pos) {
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }
}
