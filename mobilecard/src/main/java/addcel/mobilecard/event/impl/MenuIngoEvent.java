package addcel.mobilecard.event.impl;

import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 08/06/17.
 */
class MenuIngoEvent implements BusEvent {
    private final int idPais;
    private final long idUsuario;
    private final String msg;

    public MenuIngoEvent(int idPais, long idUsuario, String msg) {
        this.idPais = idPais;
        this.idUsuario = idUsuario;
        this.msg = msg;
    }

    public int getIdPais() {
        return idPais;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public String getMsg() {
        return msg;
    }
}
