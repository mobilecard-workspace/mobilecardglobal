package addcel.mobilecard.event.impl;

import addcel.mobilecard.data.net.viamericas.model.ExchangeRateResponse;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 09/05/17.
 */

final class ExchangeRateEvent implements BusEvent {
    private final ExchangeRateResponse exchangeRate;
    private final boolean success;

    public ExchangeRateEvent(ExchangeRateResponse exchangeRate, boolean success) {
        this.exchangeRate = exchangeRate;
        this.success = success;
    }

    public ExchangeRateResponse getExchangeRate() {
        return exchangeRate;
    }

    public boolean isSuccess() {
        return success;
    }
}
