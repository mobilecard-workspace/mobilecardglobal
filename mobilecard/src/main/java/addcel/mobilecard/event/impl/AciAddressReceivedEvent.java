package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 29/03/17.
 */
public final class AciAddressReceivedEvent implements BusEvent {
    private final List<Address> addresses;

    public AciAddressReceivedEvent(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return addresses;
    }
}
