package addcel.mobilecard.event.impl;

import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 28/03/17.
 */
public final class AciNoDataEvent implements BusEvent {
    private final ServiceModel model;
    private final CardEntity formaPago;
    private final String cuenta;
    private final double monto;

    public AciNoDataEvent(ServiceModel model, CardEntity formaPago, String cuenta, double monto) {
        this.model = model;
        this.formaPago = formaPago;
        this.cuenta = cuenta;
        this.monto = monto;
    }

    public ServiceModel getModel() {
        return model;
    }

    public CardEntity getFormaPago() {
        return formaPago;
    }

    public String getCuenta() {
        return cuenta;
    }

    public double getMonto() {
        return monto;
    }
}
