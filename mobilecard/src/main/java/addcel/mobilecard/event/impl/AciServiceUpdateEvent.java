package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 23/03/17.
 */
public class AciServiceUpdateEvent implements BusEvent {
    private final List<ServiceModel> models;

    public AciServiceUpdateEvent(List<ServiceModel> models) {
        this.models = models;
    }

    public List<ServiceModel> getModels() {
        return models;
    }
}
