package addcel.mobilecard.event.impl;

import addcel.mobilecard.data.net.transacto.entity.SaldoEntity;
import addcel.mobilecard.data.net.transacto.entity.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 20/04/17.
 */

public class PostAntadConsultaTokenEvent implements BusEvent {
    private final TokenEntity token;
    private final String referencia;
    private final SaldoEntity response;
    private final CardEntity formaPago;

    public PostAntadConsultaTokenEvent(TokenEntity token, String referencia, SaldoEntity response,
                                       CardEntity formaPago) {
        this.token = token;
        this.referencia = referencia;
        this.response = response;
        this.formaPago = formaPago;
    }

    public TokenEntity getToken() {
        return token;
    }

    public String getReferencia() {
        return referencia;
    }

    public SaldoEntity getResponse() {
        return response;
    }

    public CardEntity getFormaPago() {
        return formaPago;
    }
}
