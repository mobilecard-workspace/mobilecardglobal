package addcel.mobilecard.event.impl;

import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 30/05/17.
 */
class MenuActivationEvent implements BusEvent {
    private final String msg;

    public MenuActivationEvent(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
