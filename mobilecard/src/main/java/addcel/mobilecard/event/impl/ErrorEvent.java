package addcel.mobilecard.event.impl;

import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 20/04/17.
 */
public class ErrorEvent implements BusEvent {
    private final int errorRes;
    private final String errorMsg;
    private final boolean destroyView;

    public ErrorEvent(int errorRes, String errorMsg, boolean destroyView) {
        this.errorRes = errorRes;
        this.errorMsg = errorMsg;
        this.destroyView = destroyView;
    }

    public int getErrorRes() {
        return errorRes;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public boolean isDestroyView() {
        return destroyView;
    }
}
