package addcel.mobilecard.event.impl;

import addcel.mobilecard.data.net.viamericas.model.SenderResponse;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 27/03/17.
 */

final class ViamericasSenderRegisteredEvent implements BusEvent {
    private final SenderResponse response;

    public ViamericasSenderRegisteredEvent(SenderResponse response) {
        this.response = response;
    }

    public SenderResponse getResponse() {
        return response;
    }
}
