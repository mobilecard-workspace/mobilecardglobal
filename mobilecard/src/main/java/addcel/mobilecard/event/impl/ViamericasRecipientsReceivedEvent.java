package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.viamericas.model.RecipientResponse;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 30/03/17.
 */

final class ViamericasRecipientsReceivedEvent implements BusEvent {
    private final List<RecipientResponse> recipients;

    public ViamericasRecipientsReceivedEvent(List<RecipientResponse> recipients) {
        this.recipients = recipients;
    }

    public List<RecipientResponse> getRecipients() {
        return recipients;
    }
}
