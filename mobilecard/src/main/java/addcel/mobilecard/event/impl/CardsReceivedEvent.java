package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 28/03/17.
 */
class CardsReceivedEvent implements BusEvent {
    private final List<CardEntity> cards;
    private final boolean withDialog;

    CardsReceivedEvent(List<CardEntity> cards, boolean withDialog) {
        this.cards = cards;
        this.withDialog = withDialog;
    }

    public List<CardEntity> getCards() {
        return cards;
    }

    public boolean isWithDialog() {
        return withDialog;
    }
}
