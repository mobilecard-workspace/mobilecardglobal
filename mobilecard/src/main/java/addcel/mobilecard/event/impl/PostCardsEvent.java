package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 20/04/17.
 */
class PostCardsEvent implements BusEvent {
    private final List<CardEntity> cards;
    private final boolean withUiElements;

    public PostCardsEvent(List<CardEntity> cards, boolean withUiElements) {
        this.cards = cards;
        this.withUiElements = withUiElements;
    }

    public List<CardEntity> getCards() {
        return cards;
    }

    public boolean isWithUiElements() {
        return withUiElements;
    }
}
