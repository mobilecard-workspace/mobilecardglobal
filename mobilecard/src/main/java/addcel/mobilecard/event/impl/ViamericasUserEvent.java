package addcel.mobilecard.event.impl;

import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 10/05/17.
 */

class ViamericasUserEvent implements BusEvent {
    private final int idSender;
    private final boolean success;

    public ViamericasUserEvent(int idSender, boolean success) {
        this.idSender = idSender;
        this.success = success;
    }

    public int getIdUsuario() {
        return idSender;
    }

    public boolean isSuccess() {
        return success;
    }
}
