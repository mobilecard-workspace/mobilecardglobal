package addcel.mobilecard.event.impl;

import java.util.List;

import addcel.mobilecard.data.net.aci.model.Ciudad;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 28/03/17.
 */
final class CityReceivedEvent implements BusEvent {
    private final List<Ciudad> ciudades;

    public CityReceivedEvent(List<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }

    public List<Ciudad> getCiudades() {
        return ciudades;
    }
}
