package addcel.mobilecard.event;

/**
 * ADDCEL on 29/12/16.
 */
final class CardUpdateEvent {
    private final String cardNumber;

    public CardUpdateEvent(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }
}
