package addcel.mobilecard.domain.email

import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.net.registro.EmailStatusEntity
import addcel.mobilecard.data.net.registro.RegistroAPI
import addcel.mobilecard.data.net.registro.TipoVerificacion
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-07-24.
 */
interface EmailRegistroInteractor {

    fun clearDisposables()

    fun sendVerificationLink(
            idApp: Int, idPais: Int, idioma: String, imei: String, phone: String,
            email: String, callback: InteractorCallback<EmailStatusEntity>
    )

    fun checkIfEmailVerified(
            idApp: Int, idPais: Int, idioma: String, id: Long,
            callback: InteractorCallback<EmailStatusEntity>
    )

    fun fetchEmail(): String

    fun storeEmail(email: String)
}

class EmailRegistroInteractorImpl(
        val api: RegistroAPI, val state: StateSession, val useCase: Int,
        val disposables: CompositeDisposable
) : EmailRegistroInteractor {
    override fun storeEmail(email: String) {
        state.setCapturedEmail(email)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun fetchEmail(): String {
        return state.getCapturedEmail()
    }

    override fun sendVerificationLink(
            idApp: Int, idPais: Int, idioma: String, imei: String,
            phone: String, email: String, callback: InteractorCallback<EmailStatusEntity>
    ) {
        val tipo = if (useCase == 1) TipoVerificacion.USUARIO else TipoVerificacion.COMERCIO
        disposables.add(
                api.sendVerificationLink(idApp, idPais, idioma, tipo, email, imei, phone).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                        { result -> callback.onSuccess(result) }, { t ->
                    t.printStackTrace()
                    callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        )
    }

    override fun checkIfEmailVerified(
            idApp: Int, idPais: Int, idioma: String, id: Long,
            callback: InteractorCallback<EmailStatusEntity>
    ) {
        val tipo = if (useCase == 1) TipoVerificacion.USUARIO else TipoVerificacion.COMERCIO
        disposables.add(api.checkIfEmailVerified(idApp, idPais, idioma, tipo, id).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { result -> callback.onSuccess(result) }, { t ->
            t.printStackTrace()
            callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }
}