package addcel.mobilecard.domain.previvale.activate;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Credentials;

/**
 * ADDCEL on 11/16/18.
 */
public class PrevivaleActivateInteractorImpl implements PrevivaleActivateInteractor {
    private final WalletAPI api;
    private final CatalogoService catalogo;
    private final Usuario usuario;
    private final CompositeDisposable cDisposable;

    public PrevivaleActivateInteractorImpl(WalletAPI api, CatalogoService catalogo, Usuario usuario) {
        this.api = api;
        this.catalogo = catalogo;
        this.usuario = usuario;
        this.cDisposable = new CompositeDisposable();
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return cDisposable;
    }

    @Override
    public Usuario getUsuario() {
        return usuario;
    }

    @Override
    public void getEstados(int idApp, String idioma,
                           InteractorCallback<List<EstadoResponse.EstadoEntity>> callback) {
        cDisposable.add(catalogo.getEstados(idApp, usuario.getIdPais(), idioma)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(estadoResponse -> {
                    if (estadoResponse.getIdError() == 0) {
                        callback.onSuccess(estadoResponse.getEstados());
                    } else {
                        callback.onError(estadoResponse.getMensajeError());
                    }
                }, throwable -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError()))));
    }

    @SuppressLint("CheckResult")
    @Override
    public void activate(int idApp, String idioma, PrevivaleUserRequest pvUser,
                         InteractorCallback<CardEntity> callback) {

        String credentials = Credentials.basic("mobilecardmx", "82007eb4238205c75ebcdefc06a01311");

        cDisposable.add(
                api.getPrevivaleCard(idApp, usuario.getIdPais(), idioma, WalletAPI.Companion.USUARIO,
                        usuario.getIdeUsuario(), pvUser)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(mobileCardCardResponse -> {
                            if (mobileCardCardResponse.getIdError() == 0) {
                                callback.onSuccess(mobileCardCardResponse.getCard());
                            } else {
                                callback.onError(mobileCardCardResponse.getMensajeError());
                            }
                        }, throwable -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError()))));
    }
}
