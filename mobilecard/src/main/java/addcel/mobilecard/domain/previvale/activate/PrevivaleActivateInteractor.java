package addcel.mobilecard.domain.previvale.activate;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;
import addcel.mobilecard.domain.InteractorCallback;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 11/16/18.
 */
public interface PrevivaleActivateInteractor {

    CompositeDisposable getCompositeDisposable();

    Usuario getUsuario();

    void getEstados(int idApp, String idioma,
                    InteractorCallback<List<EstadoResponse.EstadoEntity>> callback);

    void activate(int idApp, String idioma, PrevivaleUserRequest pvUser,
                  InteractorCallback<CardEntity> callback);
}
