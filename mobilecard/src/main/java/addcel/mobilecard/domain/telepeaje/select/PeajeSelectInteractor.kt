package addcel.mobilecard.domain.telepeaje.select

import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.telepeaje.TagsApi
import addcel.mobilecard.data.net.telepeaje.model.Tag
import addcel.mobilecard.data.net.telepeaje.model.TagsResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.MapUtils
import addcel.mobilecard.utils.StringUtil
import com.google.common.base.Strings
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 3/12/19.
 */
class PeajeSelectInteractor(
        val api: TagsApi, val dao: FavoritoDaoRx,
        val session: SessionOperations, private val providerLookup: Map<Int, Int>,
        val disposables: CompositeDisposable
) {

    fun clearDisposables() {
        disposables.clear()
    }

    fun getPeajeProvider(idServicio: Int): Int {
        return MapUtils.getOrDefault(providerLookup, idServicio, 4)
    }

    fun getIdPais(): Int {
        return session.usuario.idPais
    }

    fun list(idApp: Int, idioma: String, idServicio: Int, callback: InteractorCallback<List<Tag>>) {
        disposables.add(api.list(
                idApp, idioma, session.usuario.ideUsuario,
                MapUtils.getOrDefault(providerLookup, idServicio, 4)
        ).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
            if (r.idError == 0) callback.onSuccess(r.tags) else callback.onError(r.mensajeError)
        }, { t -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError())) })
        )
    }

    fun add(
            idApp: Int, idioma: String, idServicio: Int, tag: Tag,
            callback: InteractorCallback<TagsResponse>
    ) {
        disposables.add(api.insert(
                idApp, idioma, session.usuario.ideUsuario,
                MapUtils.getOrDefault(providerLookup, idServicio, 4), tag
        ).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
            if (r.idError == 0) callback.onSuccess(r) else callback.onError(r.mensajeError)
        }, { t -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError())) })
        )
    }

    fun delete(
            idApp: Int, idioma: String, idServicio: Int, tagNumber: String,
            callback: InteractorCallback<TagsResponse>
    ) {
        disposables.delete(api.delete(
                idApp, idioma, session.usuario.ideUsuario,
                MapUtils.getOrDefault(providerLookup, idServicio, 4), tagNumber
        ).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
            if (r.idError == 0) callback.onSuccess(r) else callback.onError(r.mensajeError)
        }, { t -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError())) })
        )
    }

    fun addToFav(favorito: Favorito, callback: InteractorCallback<Long>) {
        disposables.add(
                Single.fromCallable { dao.insert(favorito) }.subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe(
                        { l -> if (l > 0) callback.onSuccess(l) else callback.onError("") },
                        { t -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError())) })
        )
    }

    fun checkBalance(idApp: Int, idioma: String, tag: Tag, callback: InteractorCallback<Double>) {
        disposables.add(api.checkBalance(
                idApp,
                idioma,
                session.usuario.ideUsuario,
                tag
        ).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
            if (r.idError == 0L) callback.onSuccess(r.balance) else callback.onError(r.mensajeError)
        }, { t -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError())) })
        )
    }
}