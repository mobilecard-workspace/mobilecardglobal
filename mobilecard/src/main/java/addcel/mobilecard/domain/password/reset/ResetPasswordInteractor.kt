package addcel.mobilecard.domain.password.reset

import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.usuarios.UsuariosService
import addcel.mobilecard.data.net.usuarios.model.McResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-07-19.
 */
interface ResetPasswordInteractor {

    fun clearDisposables()

    fun reset(
            idApp: Int, idPais: Int, idioma: String, email: String,
            callback: InteractorCallback<McResponse>
    )
}

class ResetPasswordUsuarioInteractorImpl(
        val api: UsuariosService,
        private val compDisposable: CompositeDisposable
) : ResetPasswordInteractor {
    override fun clearDisposables() {
        compDisposable.clear()
    }

    override fun reset(
            idApp: Int, idPais: Int, idioma: String, email: String,
            callback: InteractorCallback<McResponse>
    ) {
        compDisposable.add(
                api.resetPassword(idApp, idioma, email).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r -> callback.onSuccess(r) }, { t ->
                    t.printStackTrace()
                    callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t))
                })
        )
    }
}

class ResetPasswordNegocioInteractorImpl(
        val api: NegocioAPI,
        private val compDisposable: CompositeDisposable
) : ResetPasswordInteractor {
    override fun clearDisposables() {
        compDisposable.clear()
    }

    override fun reset(
            idApp: Int, idPais: Int, idioma: String, email: String,
            callback: InteractorCallback<McResponse>
    ) {
        compDisposable.add(
                api.resetPasswordAdmin(idApp, idioma, email).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).map { r ->
                    McResponse(r.idError, r.mensajeError)
                }.subscribe({ r -> callback.onSuccess(r) }, { t ->
                    t.printStackTrace()
                    callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t))
                })
        )
    }
}