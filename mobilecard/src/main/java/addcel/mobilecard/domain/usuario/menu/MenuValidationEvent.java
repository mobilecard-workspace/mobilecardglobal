package addcel.mobilecard.domain.usuario.menu;

import addcel.mobilecard.data.net.usuarios.model.UserValidation;

/**
 * ADDCEL on 12/5/18.
 */
public final class MenuValidationEvent {
    private final int useCase;
    private final UserValidation validation;

    public MenuValidationEvent(int useCase, UserValidation validation) {
        this.useCase = useCase;
        this.validation = validation;
    }

    public int getUseCase() {
        return useCase;
    }

    public UserValidation getValidation() {
        return validation;
    }
}
