package addcel.mobilecard.domain.usuario.perfil;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;
import com.squareup.otto.Bus;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.PhotoAPI;
import addcel.mobilecard.data.net.usuarios.PhotoRequest;
import addcel.mobilecard.data.net.usuarios.PhotoResponse;
import addcel.mobilecard.data.net.usuarios.PhotoResponseEvent;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.data.net.usuarios.model.DataUpdateResponse;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.UserValidation;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.login.UsuarioStatus;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 12/5/18.
 */
public class PerfilInteractorImpl implements PerfilInteractor {
    private final UsuariosService userApi;
    private final CatalogoService catalogApi;
    private final PhotoAPI photoAPI;
    private final Bus bus;
    private final SessionOperations session;
    private final CompositeDisposable disposables;

    public PerfilInteractorImpl(UsuariosService userApi, CatalogoService catalogApi,
                                PhotoAPI photoAPI, Bus bus, SessionOperations session) {
        this.userApi = userApi;
        this.catalogApi = catalogApi;
        this.photoAPI = photoAPI;
        this.bus = bus;
        this.session = session;
        disposables = new CompositeDisposable();
    }

    @Override
    public long getIdUsuario() {
        return session.getUsuario().getIdeUsuario();
    }

    @Override
    public String getNombre() {
        return session.getUsuario().getUsrNombre();
    }

    @Override
    public String getApellido() {
        return session.getUsuario().getUsrApellido();
    }

    @Override
    public String getEmail() {
        return session.getUsuario().getEMail();
    }

    @Override
    public String getPassword() {
        return session.getUsuario().getUsrPwd();
    }

    @Override
    public int getPais() {
        return session.getUsuario().getIdPais();
    }

    @Override
    public String getCelular() {
        return session.getUsuario().getUsrTelefono();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getPaises(int idApp, String idioma,
                          InteractorCallback<List<PaisResponse.PaisEntity>> callback) {
        disposables.add(catalogApi.getPaises(idApp, idioma)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSuccess,
                        throwable -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError()))));
    }

    @Override
    public Observable<DataUpdateResponse> getUpdateInfoObservable(int idApp, String idioma,
                                                                  String name, String value) {
        long idUsuario = session.getUsuario().getIdeUsuario();
        return userApi.updateRx(idApp, idioma, idUsuario, name, value)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @SuppressLint("CheckResult")
    @Override
    public void updateInfo(int idApp, String idioma, List<Observable<DataUpdateResponse>> observables,
                           InteractorCallback<UserValidation> callback) {
        Observable.merge(observables).subscribeWith(new Observer<DataUpdateResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposables.add(d);
            }

            @Override
            public void onNext(DataUpdateResponse dataUpdateResponse) {
                if (dataUpdateResponse.getIdError() != 0) {
                    callback.onError(dataUpdateResponse.getMensajeError());
                }
            }

            @Override
            public void onError(Throwable e) {
                callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError()));
            }

            @Override
            public void onComplete() {
                validateAndUpdate(idApp, idioma, callback);
            }
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void updatePassword(int idApp, String idioma, String enNewPass,
                               InteractorCallback<McResponse> callback) {
        long idUsuario = session.getUsuario().getIdeUsuario();
        String enCurrPass = session.getUsuario().getUsrPwd();
        disposables.add(userApi.updatePassword(idApp, idioma, idUsuario, enCurrPass, enNewPass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mcResponse -> {
                    if (mcResponse.getIdError() == 0) {
                        updatePassInRepo(enNewPass);
                        callback.onSuccess(mcResponse);
                    } else {
                        callback.onError(mcResponse.getMensajeError());
                    }
                }, throwable -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError()))));
    }

    @SuppressLint("CheckResult")
    @Override
    public void validateAndUpdate(int idApp, String idioma,
                                  InteractorCallback<UserValidation> callback) {
        long idUsuario = session.getUsuario().getIdeUsuario();
        disposables.add(userApi.verifyUser(idApp, idioma, idUsuario)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(validation -> {
                    if (validation.getIdError() == 0) {
                        update(validation, callback);
                    } else {
                        callback.onError(validation.getMensajeError());
                    }
                }, throwable -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError()))));
    }

    @Override
    public void updateImage(int idApp, String idioma, @NotNull String imgB64) {
        final String method =
                Strings.isNullOrEmpty(session.getUsuario().getImg()) ? PhotoAPI.ADD : PhotoAPI.UPDATE;

        final PhotoRequest body = new PhotoRequest(getIdUsuario(), imgB64);

        disposables.add(
                photoAPI.loadPhoto(idApp, session.getUsuario().getIdPais(), idioma, method, body)
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.computation())
                        .subscribe(photoResponse -> {
                            updateImgInRepo(imgB64);
                            bus.post(new PhotoResponseEvent(photoResponse));
                        }, throwable -> bus.post(new PhotoResponseEvent(new PhotoResponse(-999, "")))));
    }

    @Override
    public void loadImageAsBitmap() {
        String img = session.getUsuario().getImg();
        if (!Strings.isNullOrEmpty(img)) {
            disposables.add(
                    Single.fromCallable(() -> StringUtil.base64ToByteArray(session.getUsuario().getImg()))
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(bus::post, Throwable::printStackTrace));
        }
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    private void updateImgInRepo(String img) {
        Usuario usuario = session.getUsuario();
        usuario.setImg(img);
        session.setUsuario(usuario);
        session.setUsuarioLogged(Boolean.TRUE);
    }

    private void updatePassInRepo(String enNewPass) {
        Usuario usuario = session.getUsuario();
        usuario.setUsrPwd(enNewPass);
        session.setUsuario(usuario);
        session.setUsuarioLogged(Boolean.TRUE);
    }

    private void update(UserValidation validation,
                        InteractorCallback<UserValidation> validationInteractorCallback) {
        switch (validation.getIdUsrStatus()) {
            case UsuarioStatus.ACTIVO:
            case UsuarioStatus.EMAIL_VERIFICATION:
            case UsuarioStatus.SMS_VERIFICATION:
                Usuario tempUser = session.getUsuario();
                tempUser.setUsrNombre(validation.getUsrNombre());
                tempUser.setUsrApellido(validation.getUsrApellido());
                tempUser.setIdPais(validation.getIdPais());
                tempUser.setIdUsrStatus(validation.getIdUsrStatus());
                session.setUsuario(tempUser);
                session.setUsuarioLogged(Boolean.TRUE);
                validationInteractorCallback.onSuccess(validation);
                break;
            default:
                session.setUsuario(null);
                session.setUsuarioLogged(Boolean.FALSE);
                validationInteractorCallback.onError(validation.getMensajeError());
                break;
        }
    }
}
