package addcel.mobilecard.domain.usuario.menu;

/**
 * ADDCEL on 12/10/18.
 */
public final class MenuLocationEvent {
    private final int useCase;
    private final boolean hasLocation;

    public MenuLocationEvent(int useCase, boolean hasLocation) {
        this.useCase = useCase;
        this.hasLocation = hasLocation;
    }

    public int getUseCase() {
        return useCase;
    }

    public boolean isHasLocation() {
        return hasLocation;
    }
}
