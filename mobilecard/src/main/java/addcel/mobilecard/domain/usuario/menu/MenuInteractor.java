package addcel.mobilecard.domain.usuario.menu;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.PushResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 12/5/18.
 */
public interface MenuInteractor {

    void clearDisposables();

    Usuario getUsuario();

    int getIdPais();

    long getIdUsuario();

    String getUsuarioName();

    String getUsuarioApellidos();

    String getUsuarioProfilePic();

    String getEmail();

    void checkIfIngoActive(InteractorCallback<IngoAliveResponse> callback);

    void verifyUser(int useCase, int idApp, String idioma,
                    InteractorCallback<MenuValidationEvent> callback);

    void updateReference(int idApp, String idioma, int idPais, String reference,
                         InteractorCallback<McResponse> callback);

    void resendEmailAuth(int idApp, String idioma, InteractorCallback<DefaultResponse> callback);

    void getRecargasMx(int useCase, int idApp, String idioma,
                       InteractorCallback<List<CatalogoResponse.RecargaEntity>> callback);

    void getCategoriasMx(int useCase, int idApp, String idioma,
                         InteractorCallback<List<CatalogoResponse.CategoriaEntity>> callback);

    void getCards(int useCase, int idApp, String idioma,
                  InteractorCallback<List<CardEntity>> callback);

    void getTelefonicaPaises(int idApp, String idioma,
                             InteractorCallback<List<PaisResponse.PaisEntity>> callback);

    void logout(InteractorCallback<Integer> callback);

    void postCountryUpdateEvent(boolean hastIngo);

    void loadProfile();

    boolean isMenuLaunched();

    void setMenuLaunched(boolean first);

    void updateToken(int idApp, String idioma, String token);

    void deleteToken(int idApp, @NotNull String idioma, @NotNull String token, @NotNull InteractorCallback<PushResponse> callback);
}
