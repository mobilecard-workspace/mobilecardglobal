package addcel.mobilecard.domain.usuario.menu;

/**
 * ADDCEL on 12/5/18.
 */
public final class MenuUseCase {
    public static final int PROFILE = 1;
    public static final int EDO_CUENTA = 2;
    public static final int WALLET = 3;
    public static final int FAVS = 4;
    public static final int MY_MC = 5;
    public static final int MX_TRANSFERS = 6;
    public static final int MX_RECARGAS = 7;
    public static final int MX_SERVICIOS = 8;
    public static final int SCAN_PAY = 9;
    public static final int COL_SERVICIOS = 10;
    public static final int COL_RECARGAS = 11;
    public static final int CHEQUES = 12;
    public static final int US_SERVICIOS = 13;
    public static final int US_RECARGAS = 14;

    private MenuUseCase() {
    }
}
