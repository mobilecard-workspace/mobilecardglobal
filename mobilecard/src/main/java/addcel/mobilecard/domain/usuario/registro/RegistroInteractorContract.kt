package addcel.mobilecard.domain.usuario.registro

import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.usuarios.UsuariosService
import addcel.mobilecard.data.net.usuarios.model.McResponse
import addcel.mobilecard.data.net.usuarios.model.RegistroRequest
import addcel.mobilecard.data.net.usuarios.model.RegistroResponse
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-06-26.
 */
interface RegistroInteractor {

    fun clearDisposables()

    fun getTerms(
            tipoTerm: String, idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<String>
    )

    fun saveUser(
            idApp: Int, idioma: String, request: RegistroRequest,
            callback: InteractorCallback<RegistroResponse>
    )

    fun createUser(
            idApp: Int, idioma: String, request: RegistroRequest, callback: InteractorCallback<Usuario>
    )

    fun loginUser(
            usuario: Usuario, callback: InteractorCallback<McResponse>
    )

    fun updateReference(
            idApp: Int, idioma: String, idPais: Int, login: String, reference: String,
            callback: InteractorCallback<McResponse>
    )

    fun setFirstUse()
}

class RegistroInteractorImpl(
        val api: UsuariosService, val repository: SessionOperations, val state: StateSession,
        val disposables: CompositeDisposable
) : RegistroInteractor {
    override fun setFirstUse() {
        state.setMenuLaunched(false)
        state.setWalletLaunched(false)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getTerms(
            tipoTerm: String, idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<String>
    ) {
        val client = StringUtil.sha256("mobilecardandroid")
        disposables.add(
                api.getTermsParam(idApp, client, tipoTerm, idPais, idioma).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    if (r.idError == 0) callback.onSuccess(r.terminos)
                    else callback.onError(r.mensajeError)
                }, { t ->
                    t.printStackTrace()
                    callback.onError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        )
    }

    override fun saveUser(
            idApp: Int, idioma: String, request: RegistroRequest,
            callback: InteractorCallback<RegistroResponse>
    ) {

    }

    override fun createUser(
            idApp: Int,
            idioma: String,
            request: RegistroRequest,
            callback: InteractorCallback<Usuario>
    ) {
        disposables.add(
                api.create(idApp, idioma, request).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({
                    callback.onSuccess(it)
                }, {
                    it.printStackTrace()
                    callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        )
    }

    override fun loginUser(usuario: Usuario, callback: InteractorCallback<McResponse>) {
        repository.usuario = Usuario.sanitize(usuario)
        repository.isUsuarioLogged = true
        callback.onSuccess(McResponse(0, "Bienvenid@ a MobileCard"))
    }

    override fun updateReference(
            idApp: Int, idioma: String, idPais: Int, login: String,
            reference: String, callback: InteractorCallback<McResponse>
    ) {
        disposables.add(api.updateReference(idApp, idPais, idioma, login, reference).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { r -> callback.onSuccess(r) }, { throwable ->
            throwable.printStackTrace()
            callback.onError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }
}