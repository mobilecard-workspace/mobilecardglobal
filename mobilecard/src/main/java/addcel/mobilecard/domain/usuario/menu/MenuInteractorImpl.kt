package addcel.mobilecard.domain.usuario.menu

import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest
import addcel.mobilecard.data.net.ingo.IngoAliveAPI
import addcel.mobilecard.data.net.ingo.IngoAliveResponse
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse
import addcel.mobilecard.data.net.usuarios.PhotoNotificationEvent
import addcel.mobilecard.data.net.usuarios.UsuariosService
import addcel.mobilecard.data.net.usuarios.model.*
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.login.UsuarioStatus
import addcel.mobilecard.event.impl.PostPaisEvent
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.annotation.SuppressLint
import com.google.common.base.Strings
import com.google.common.collect.Collections2
import com.google.common.collect.Lists
import com.squareup.otto.Bus
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

/**
 * ADDCEL on 12/5/18.
 */
class MenuInteractorImpl(
        private val userApi: UsuariosService,
        private val catalogApi: CatalogoService,
        private val ingoAliveAPI: IngoAliveAPI,
        private val cardsApi: WalletAPI,
        private val session: SessionOperations,
        private val state: StateSession,
        private val bus: Bus
) : MenuInteractor {
    override fun getUsuario(): Usuario {
        return session.usuario
    }

    private val disposables = CompositeDisposable()

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getIdPais(): Int {
        return session.usuario.idPais
    }

    override fun getIdUsuario(): Long {
        return session.usuario.ideUsuario
    }

    override fun getUsuarioName(): String {
        return Strings.nullToEmpty(session.usuario.usrNombre)
    }

    override fun getUsuarioApellidos(): String {
        return Strings.nullToEmpty(session.usuario.usrApellido)
    }

    override fun getUsuarioProfilePic(): String {
        return session.usuario.img
    }

    override fun getEmail(): String {
        return session.usuario.eMail
    }

    @SuppressLint("CheckResult")
    override fun checkIfIngoActive(callback: InteractorCallback<IngoAliveResponse>) {
        disposables.add(
                ingoAliveAPI.systemAvailability()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ ingoAliveResponse ->
                            if (ingoAliveResponse.serviceAvailable) {
                                callback.onSuccess(ingoAliveResponse)
                            } else {
                                callback.onError(ingoAliveResponse.message)
                            }
                        }, { throwable -> callback.onError(StringUtil.getNetworkError()) })
        )
    }

    @SuppressLint("CheckResult")
    override fun verifyUser(
            useCase: Int, idApp: Int, idioma: String,
            callback: InteractorCallback<MenuValidationEvent>
    ) {
        disposables.add(
                userApi.verifyUser(idApp, idioma, session.usuario.ideUsuario)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            val validation = UserValidation.Builder(it).setIdUSuario(
                                    session.usuario.ideUsuario
                            ).build()
                            if (validation.idError == 0) {
                                update(useCase, validation, callback)
                            } else {
                                callback.onError(validation.mensajeError)
                            }
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    @SuppressLint("CheckResult")
    override fun updateReference(
            idApp: Int, idioma: String, idPais: Int, reference: String,
            callback: InteractorCallback<McResponse>
    ) {
        disposables.add(
                userApi.updateReference(idApp, idPais, idioma, session.usuario.eMail, reference)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) {
                                callback.onSuccess(it)
                            } else {
                                callback.onError(it.mensajeError)
                            }
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    @SuppressLint("CheckResult")
    override fun resendEmailAuth(
            idApp: Int, idioma: String,
            callback: InteractorCallback<DefaultResponse>
    ) {
        disposables.add(
                userApi.activateUser(idApp, idioma, session.usuario.ideUsuario)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) {
                                callback.onSuccess(
                                        DefaultResponse(it.idError, it.mensajeError)
                                )
                            } else {
                                callback.onError(it.mensajeError)
                            }
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    @SuppressLint("CheckResult")
    override fun getRecargasMx(
            useCase: Int, idApp: Int, idioma: String,
            callback: InteractorCallback<List<CatalogoResponse.RecargaEntity>>
    ) {
        disposables.add(
                catalogApi.getRecargas(
                        idApp, session.usuario.idPais, idioma,
                        ServiciosRequest.empty()
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) {
                                callback.onSuccess(processRecargaList(it.recargas))
                            } else {
                                callback.onError(it.mensajeError)
                            }
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    @SuppressLint("CheckResult")
    override fun getCategoriasMx(
            useCase: Int, idApp: Int, idioma: String,
            callback: InteractorCallback<List<CatalogoResponse.CategoriaEntity>>
    ) {
        disposables.add(
                catalogApi.getServiciosCategorias(idApp, idPais, idioma, ServiciosRequest.empty())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) {
                                callback.onSuccess(it.categorias)
                            } else {
                                callback.onError(it.mensajeError)
                            }
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    @SuppressLint("CheckResult")
    override fun getCards(
            useCase: Int, idApp: Int, idioma: String,
            callback: InteractorCallback<List<CardEntity>>
    ) {
        disposables.add(
                cardsApi.getTarjetas(idApp, session.usuario.ideUsuario, idioma)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) {
                                callback.onSuccess(it.tarjetas)
                            } else {
                                callback.onError(it.mensajeError)
                            }
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    @SuppressLint("CheckResult")
    override fun getTelefonicaPaises(
            idApp: Int, idioma: String,
            callback: InteractorCallback<List<PaisResponse.PaisEntity>>
    ) {
        disposables.add(
                catalogApi.getPaisesTelefonica(idApp, session.usuario.idPais, idioma)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) {
                                callback.onSuccess(it.paises)
                            } else {
                                callback.onError(it.mensajeError)
                            }
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    override fun logout(callback: InteractorCallback<Int>) {
        session.logout()
        callback.onSuccess(0)
    }

    override fun postCountryUpdateEvent(hasIngo: Boolean) {
        bus.post(PostPaisEvent(idPais, java.lang.Boolean.TRUE, hasIngo))
    }

    @SuppressLint("CheckResult")
    override fun loadProfile() {
        val img = session.usuario.img
        if (!Strings.isNullOrEmpty(img)) {
            disposables.add(Single.fromCallable { session.usuario.img }
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ bus.post(PhotoNotificationEvent(it)) },
                            { it.printStackTrace() })
            )
        }
    }

    override fun isMenuLaunched(): Boolean {
        return state.isMenuLaunched()
    }

    override fun setMenuLaunched(menuLaunched: Boolean) {
        state.setMenuLaunched(menuLaunched)
    }

    override fun deleteToken(
            idApp: Int,
            idioma: String,
            token: String,
            callback: InteractorCallback<PushResponse>
    ) {

        val request = RemovePushTokenRequest(session.usuario.ideUsuario, "USUARIO", token)

        disposables.add(
                userApi.deleteToken(UsuariosService.PUSH_AUTH, request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    Timber.d(it.toString())
                                    callback.onSuccess(it)
                                }, {
                            Timber.e(it)
                            callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        }
                        )
        )
    }

    override fun updateToken(idApp: Int, idioma: String, token: String) {
        val request = PushTokenRequest(
                idApp,
                session.usuario.idPais,
                session.usuario.ideUsuario,
                idioma,
                "USUARIO",
                token
        )

        disposables.add(
                userApi.saveToken(UsuariosService.PUSH_AUTH, request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { (_, message) -> Timber.d(message) }, { Timber.e(it) })
        )
    }

    private fun update(
            useCase: Int, validation: UserValidation,
            callback: InteractorCallback<MenuValidationEvent>
    ) {
        val tempUser = session.usuario
        when (validation.idUsrStatus) {
            UsuarioStatus.ACTIVO, UsuarioStatus.EMAIL_VERIFICATION, UsuarioStatus.SMS_VERIFICATION -> {
                tempUser.usrNombre = validation.usrNombre
                tempUser.usrApellido = validation.usrApellido
                tempUser.idPais = validation.idPais!!
                tempUser.idUsrStatus = validation.idUsrStatus!!
                tempUser.usrJumio = validation.usrJumio
                tempUser.scanReference = validation.scanReference
                session.usuario = tempUser
                session.isUsuarioLogged = java.lang.Boolean.TRUE
                callback.onSuccess(MenuValidationEvent(useCase, validation))
            }
            else -> callback.onError(validation.mensajeError)
        }
    }

    private fun processRecargaList(
            src: List<CatalogoResponse.RecargaEntity>
    ): ArrayList<CatalogoResponse.RecargaEntity> {
        return if (idPais == PaisResponse.PaisEntity.US) {
            Lists.newArrayList(
                    Collections2.filter<CatalogoResponse.RecargaEntity>(src) { input ->
                        Objects.requireNonNull<CatalogoResponse.RecargaEntity>(
                                input
                        ).id != 2
                    })
        } else Lists.newArrayList(src)
    }
}
