package addcel.mobilecard.domain.usuario.perfil;

/**
 * ADDCEL on 12/5/18.
 */
final class PerfilUseCase {
    public static final int NOMBRE = 1;
    public static final int APELLIDOS = 2;
    public static final int PAIS = 3;
    public static final int PASSWORD = 4;

    private PerfilUseCase() {
    }
}
