package addcel.mobilecard.domain.usuario.bottom

import addcel.mobilecard.data.net.usuarios.UsuariosService
import addcel.mobilecard.data.net.usuarios.model.UserValidation
import addcel.mobilecard.utils.ErrorUtil
import com.squareup.otto.Bus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-06-14.
 */

enum class UseCase {
    HOME, WALLET, FAVORITOS, HISTORIAL, PAISES, CONTACTO
}

data class MenuEvent(val result: UserValidation, val useCase: UseCase)

interface BottomMenuInteractor {

    fun clearDisposables()

    fun verify(idApp: Int, idioma: String, useCase: UseCase)
}

class BottomMenuInteractorImpl(
        val api: UsuariosService, val bus: Bus, val idUsuario: Long,
        val disposables: CompositeDisposable
) : BottomMenuInteractor {
    override fun clearDisposables() {
        disposables.clear()
    }

    override fun verify(idApp: Int, idioma: String, useCase: UseCase) {
        disposables.add(
                api.verifyUser(
                        idApp,
                        idioma,
                        idUsuario
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ v ->
                    bus.post(MenuEvent(v, useCase))
                }, {
                    bus.post(
                            MenuEvent(
                                    UserValidation(-9999, ErrorUtil.getFormattedHttpErrorMsg(it)),
                                    useCase
                            )
                    )
                })
        )
    }
}

