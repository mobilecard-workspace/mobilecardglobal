package addcel.mobilecard.domain.usuario.perfil;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.model.DataUpdateResponse;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.UserValidation;
import addcel.mobilecard.domain.InteractorCallback;
import io.reactivex.Observable;

/**
 * ADDCEL on 12/5/18.
 */
public interface PerfilInteractor {

    long getIdUsuario();

    String getNombre();

    String getApellido();

    String getEmail();

    String getPassword();

    int getPais();

    String getCelular();

    void getPaises(int idApp, String idioma,
                   InteractorCallback<List<PaisResponse.PaisEntity>> callback);

    Observable<DataUpdateResponse> getUpdateInfoObservable(int idApp, String idioma, String name,
                                                           String value);

    void updateInfo(int idApp, String idioma, List<Observable<DataUpdateResponse>> observables,
                    InteractorCallback<UserValidation> callback);

    void updatePassword(int idApp, String idioma, String enNewPass,
                        InteractorCallback<McResponse> callback);

    void validateAndUpdate(int idApp, String idioma, InteractorCallback<UserValidation> callback);

    void updateImage(int idApp, String idioma, @NotNull String imgB64);

    void loadImageAsBitmap();

    void clearDisposables();
}
