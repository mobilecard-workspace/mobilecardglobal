package addcel.mobilecard.domain.aci.list;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.stream.JsonReader;
import com.squareup.otto.Bus;

import java.io.Reader;
import java.util.List;

import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.event.impl.AciServiceInsertEvent;
import addcel.mobilecard.event.impl.AciServiceUpdateEvent;
import addcel.mobilecard.utils.JsonUtil;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 1/24/19.
 */
public class AciListInteractorImpl implements AciListInteractor {
    private final AciService api;
    private final Bus bus;
    private final List<ServiceModel> items;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public AciListInteractorImpl(AciService api, Bus bus) {
        this.api = api;
        this.bus = bus;
        this.items = Lists.newArrayList();
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @Override
    public Bus getBus() {
        return bus;
    }

    @SuppressLint("CheckResult")
    @Override
    public void downloadItems() {
        api.get()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMap(responseBody -> AciListInteractorImpl.this.models(responseBody.charStream()))
                .subscribe(items::add,
                        throwable -> bus.post(new AciServiceInsertEvent(AciServiceInsertEvent.ITEM_LOAD_ERROR)),
                        () -> bus.post(new AciServiceInsertEvent(AciServiceInsertEvent.ITEM_LOAD_FINISHED)),
                        compositeDisposable::add);
    }

    @Override
    public void filterItems(String pattern) {
        if (Strings.isNullOrEmpty(pattern)) {
            bus.post(new AciServiceUpdateEvent(items));
        } else {
            filterItemsImpl(pattern);
        }
    }

    private void filterItemsImpl(String pattern) {
        List<ServiceModel> temp = Lists.newArrayList();
        Observable.just(items)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap((Function<List<ServiceModel>, Observable<ServiceModel>>) Observable::fromIterable)
                .filter(
                        serviceModel -> serviceModel.getName().toLowerCase().contains(pattern.toLowerCase()))
                .subscribe(new Observer<ServiceModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(ServiceModel serviceModel) {
                        temp.add(serviceModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        bus.post(new AciServiceUpdateEvent(temp));
                    }
                });
    }

    @Override
    public List<ServiceModel> getItems() {
        return items;
    }

    @Override
    public void clearItems() {
        items.clear();
    }

    private Observable<ServiceModel> models(Reader charStream) {
        return Observable.create(emitter -> {
            try {
                JsonReader reader = new JsonReader(charStream);
                reader.beginArray();
                while (reader.hasNext()) {
                    ServiceModel message = JsonUtil.fromJson(reader, ServiceModel.class);
                    emitter.onNext(message);
                }
                reader.endArray();
                reader.close();
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }
}
