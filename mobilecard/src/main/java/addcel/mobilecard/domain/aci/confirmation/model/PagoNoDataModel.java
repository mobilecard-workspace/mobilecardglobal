package addcel.mobilecard.domain.aci.confirmation.model;

/**
 * ADDCEL on 1/24/19.
 */
public final class PagoNoDataModel {
  /*
  private final int idTarjeta;
  private final String codigo; //cvv2
  private final String direccionUsuario; //direccion
  private final String ciudadUsuario;
  private final int estadoUsuario;
  private final String cpUsuario;
  private final double monto;
  private final String referencia;
  private final String nombreServicio;
  private final String direccionServicio;
  private final String ciudadServicio;
  private final String idServicio;
  private final String software;
  private final String wkey;
  private final String imei;
  private final String modelo;
   */

    private String idioma;
    private int idTarjeta;
    private String codigo; // cvv2
    private String direccionUsuario; // direccion
    private String ciudadUsuario;
    private int estadoUsuario;
    private String cpUsuario;
    private double monto;
    private String referencia;
    private String nombreServicio;
    private String direccionServicio;
    private String ciudadServicio;
    private String idServicio;
    private String software;
    private String wkey;
    private String imei;
    private String modelo;

    public PagoNoDataModel() {
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(int idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDireccionUsuario() {
        return direccionUsuario;
    }

    public void setDireccionUsuario(String direccionUsuario) {
        this.direccionUsuario = direccionUsuario;
    }

    public String getCiudadUsuario() {
        return ciudadUsuario;
    }

    public void setCiudadUsuario(String ciudadUsuario) {
        this.ciudadUsuario = ciudadUsuario;
    }

    public int getEstadoUsuario() {
        return estadoUsuario;
    }

    public void setEstadoUsuario(int estadoUsuario) {
        this.estadoUsuario = estadoUsuario;
    }

    public String getCpUsuario() {
        return cpUsuario;
    }

    public void setCpUsuario(String cpUsuario) {
        this.cpUsuario = cpUsuario;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    public String getDireccionServicio() {
        return direccionServicio;
    }

    public void setDireccionServicio(String direccionServicio) {
        this.direccionServicio = direccionServicio;
    }

    public String getCiudadServicio() {
        return ciudadServicio;
    }

    public void setCiudadServicio(String ciudadServicio) {
        this.ciudadServicio = ciudadServicio;
    }

    public String getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(String idServicio) {
        this.idServicio = idServicio;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getWkey() {
        return wkey;
    }

    public void setWkey(String wkey) {
        this.wkey = wkey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}
