package addcel.mobilecard.domain.aci.confirmation;

import android.annotation.SuppressLint;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.data.net.aci.model.NoDataPagoRequest;
import addcel.mobilecard.data.net.aci.model.PagoRequest;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.aci.confirmation.model.PagoModel;
import addcel.mobilecard.domain.aci.confirmation.model.PagoNoDataModel;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 1/24/19.
 */
public class AciConfirmationInteractorImpl implements AciConfirmationInteractor {
    private final AciService service;
    private final SessionOperations session;
    private final CompositeDisposable cDisposable = new CompositeDisposable();

    public AciConfirmationInteractorImpl(AciService service, SessionOperations session) {
        this.service = service;
        this.session = session;
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return cDisposable;
    }

    @SuppressLint("CheckResult")
    @Override
    public void pagar(PagoModel model, InteractorCallback<McResponse> callback) {
        PagoRequest request =
                new PagoRequest.Builder(model.getIdioma(), session.getUsuario().getIdeUsuario(),
                        model.getIdTarjeta()).setCiudadServicio(model.getCiudadServicio())
                        .setDireccionServicio(model.getDireccionServicio())
                        .setIdServicio(model.getIdServicio())
                        .setImei(model.getImei())
                        .setModelo(model.getModelo())
                        .setMonto(model.getMonto())
                        .setNombreServicio(model.getNombreServicio())
                        .setReferencia(model.getReferencia())
                        .setSoftware(model.getSoftware())
                        .setWkey(model.getImei())
                        .setLat(session.getMinimalLocationData().getLat())
                        .setLon(session.getMinimalLocationData().getLon())
                        .build();

        cDisposable.add(service.payment(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mcResponse -> {
                    if (mcResponse.getIdError() == 0) {
                        callback.onSuccess(mcResponse);
                    } else {
                        callback.onError(mcResponse.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError())));
    }

    @SuppressLint("CheckResult")
    @Override
    public void pagarSinDatos(PagoNoDataModel model, InteractorCallback<McResponse> callback) {
        NoDataPagoRequest request =
                new NoDataPagoRequest.Builder(model.getIdioma(), session.getUsuario().getIdeUsuario(),
                        model.getIdTarjeta()).setCiudadServicio(model.getCiudadServicio())
                        .setCiudadUsuario(model.getCiudadUsuario())
                        .setCodigo(model.getCodigo())
                        .setCpUsuario(model.getCpUsuario())
                        .setDireccionServicio(model.getDireccionServicio())
                        .setDireccionUsuario(model.getDireccionUsuario())
                        .setEstadoUsuario(model.getEstadoUsuario())
                        .setIdServicio(model.getIdServicio())
                        .setImei(model.getImei())
                        .setModelo(model.getModelo())
                        .setMonto(model.getMonto())
                        .setNombreServicio(model.getNombreServicio())
                        .setReferencia(model.getReferencia())
                        .setSoftware(model.getSoftware())
                        .setWkey(model.getImei())
                        .build();

        cDisposable.add(service.paymentNotData(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mcResponse -> {
                    if (mcResponse.getIdError() == 0) {
                        callback.onSuccess(mcResponse);
                    } else {
                        callback.onError(mcResponse.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError())));
    }
}
