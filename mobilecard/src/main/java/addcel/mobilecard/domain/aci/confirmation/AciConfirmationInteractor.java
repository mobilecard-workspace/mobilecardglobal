package addcel.mobilecard.domain.aci.confirmation;

import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.aci.confirmation.model.PagoModel;
import addcel.mobilecard.domain.aci.confirmation.model.PagoNoDataModel;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 1/24/19.
 */
public interface AciConfirmationInteractor {

    CompositeDisposable getCompositeDisposable();

    void pagar(PagoModel model, InteractorCallback<McResponse> callback);

    void pagarSinDatos(PagoNoDataModel model, InteractorCallback<McResponse> callback);
}
