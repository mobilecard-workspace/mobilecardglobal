package addcel.mobilecard.domain.aci.list;

import com.squareup.otto.Bus;

import java.util.List;

import addcel.mobilecard.data.net.aci.model.ServiceModel;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 1/24/19.
 */
public interface AciListInteractor {

    CompositeDisposable getCompositeDisposable();

    Bus getBus();

    void downloadItems();

    void filterItems(String pattern);

    List<ServiceModel> getItems();

    void clearItems();
}
