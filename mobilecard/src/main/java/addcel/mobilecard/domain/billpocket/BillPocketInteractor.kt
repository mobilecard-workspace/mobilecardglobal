package addcel.mobilecard.domain.billpocket

import addcel.mobilecard.data.net.pago.PagoEntity
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.data.net.token.TokenBaseResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.domain.InteractorCallback

/**
 * ADDCEL on 3/7/19.
 */
interface BillPocketInteractor {

    fun clearDisposables()

    fun getIdUsuario(): Long

    fun getIdPais(): Int

    fun getLat(): Double

    fun getLon(): Double

    fun getJumioRef(): String

    fun getJumioStat(): String

    fun checkWhiteList(idApp: Int, idioma: String, callback: InteractorCallback<TokenBaseResponse>)

    fun getToken(
            idApp: Int, idioma: String, profile: String, auth: String,
            callback: InteractorCallback<TokenEntity>
    )

    fun pay(
            idApp: Int, idioma: String, token: TokenEntity, request: PagoEntity,
            callback: InteractorCallback<PagoResponse>
    )
}