package addcel.mobilecard.domain.billpocket

import addcel.mobilecard.data.net.pago.BPApi
import addcel.mobilecard.data.net.pago.PagoEntity
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.data.net.token.TokenBaseResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.utils.ConnectionUtil
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class BillPocketInteractorImpl(
        val api: BPApi, val tokenizer: TokenizerAPI, val usuario: Usuario,
        val location: McLocationData, val disposables: CompositeDisposable
) : BillPocketInteractor {

    override fun checkWhiteList(
            idApp: Int,
            idioma: String,
            callback: InteractorCallback<TokenBaseResponse>
    ) {
        disposables.add(
                tokenizer.isOnWhiteList(idApp, usuario.idPais, idioma, usuario.ideUsuario)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            callback.onSuccess(it)
                        }, {
                            callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })
        )
    }

    override fun getJumioRef(): String {
        return usuario.scanReference
    }

    override fun getJumioStat(): String {
        return usuario.usrJumio
    }

    override fun getIdUsuario(): Long {
        return usuario.ideUsuario
    }

    override fun getIdPais(): Int {
        return usuario.idPais
    }

    override fun getLat(): Double {
        return location.lat
    }

    override fun getLon(): Double {
        return location.lon
    }

    override fun pay(
            idApp: Int, idioma: String, token: TokenEntity, request: PagoEntity,
            callback: InteractorCallback<PagoResponse>
    ) {

        disposables.add(ConnectionUtil.checkInternetConnectionSingle().flatMap {
            api.executePagoBP(token.token, idApp, getIdPais(), idioma, token.accountId, request)
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { bpResponse ->
                    callback.onSuccess(bpResponse)
                }, {
            callback.onError(StringUtil.getNetworkError())
        })
        )
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getToken(
            idApp: Int, idioma: String, profile: String, auth: String,
            callback: InteractorCallback<TokenEntity>
    ) {
        disposables.add(
                tokenizer.getToken(
                        auth,
                        profile,
                        idApp,
                        usuario.idPais,
                        idioma
                ).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
                    when (t.code) {
                        0 -> callback.onSuccess(t)
                        else -> callback.onError(t.message)
                    }
                }, { callback.onError(StringUtil.getNetworkError()) })
        )
    }
}