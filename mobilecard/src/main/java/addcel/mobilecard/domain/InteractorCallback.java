package addcel.mobilecard.domain;

import org.jetbrains.annotations.NotNull;

public interface InteractorCallback<T> {
    void onSuccess(@NotNull T result);

    void onError(@NotNull String error);
}
