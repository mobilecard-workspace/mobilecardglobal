package addcel.mobilecard.domain.tae

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.domain.InteractorCallback

/**
 * ADDCEL on 2019-06-15.
 */
interface TaeInteractor {
    fun getProductos(
            idApp: Int, idioma: String, idCategoria: Int,
            callback: InteractorCallback<List<CatalogoResponse.RecargaEntity>>
    )
}

