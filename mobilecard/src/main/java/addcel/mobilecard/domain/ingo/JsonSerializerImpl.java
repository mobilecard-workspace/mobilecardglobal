package addcel.mobilecard.domain.ingo;

import com.google.gson.Gson;
import com.ingomoney.ingosdk.android.manager.JsonSerializer;

/**
 * ADDCEL on 25/10/16.
 */
public class JsonSerializerImpl implements JsonSerializer {

    private final Gson gson;

    public JsonSerializerImpl(Gson gson) {
        this.gson = gson;
    }

    @Override
    public String serializeJsonObject(Object o) {
        return gson.toJson(o);
    }
}
