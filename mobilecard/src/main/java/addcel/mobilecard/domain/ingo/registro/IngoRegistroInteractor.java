package addcel.mobilecard.domain.ingo.registro;

import java.util.List;

import addcel.mobilecard.data.net.ingo.model.IngoEnrollmentRequest;
import addcel.mobilecard.data.net.ingo.model.IngoEstadoResponse;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 1/14/19.
 */
public interface IngoRegistroInteractor {

    Usuario getUsuario();

    void getEstados(int idApp, String idioma,
                    InteractorCallback<List<IngoEstadoResponse.EstadoEntity>> callback);

    void enroll(int idApp, String idioma, IngoEnrollmentRequest request,
                InteractorCallback<IngoUserData> callback);
}
