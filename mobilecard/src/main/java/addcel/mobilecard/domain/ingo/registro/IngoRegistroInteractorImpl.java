package addcel.mobilecard.domain.ingo.registro;

import android.annotation.SuppressLint;

import java.util.List;

import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.data.net.ingo.model.IngoEnrollmentRequest;
import addcel.mobilecard.data.net.ingo.model.IngoEstadoResponse;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 1/14/19.
 */
public class IngoRegistroInteractorImpl implements IngoRegistroInteractor {
    private final IngoService service;
    private final Usuario usuario;

    public IngoRegistroInteractorImpl(IngoService service, Usuario usuario) {
        this.service = service;
        this.usuario = usuario;
    }

    @Override
    public Usuario getUsuario() {
        return usuario;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getEstados(int idApp, String idioma,
                           InteractorCallback<List<IngoEstadoResponse.EstadoEntity>> callback) {
        service.getEstados(idApp, idioma, usuario.getIdPais())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ingoEstadoResponse -> {
                    if (ingoEstadoResponse.getIdError() == 0) {
                        callback.onSuccess(ingoEstadoResponse.getEstados());
                    } else {
                        callback.onError(ingoEstadoResponse.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError()));
    }

    @SuppressLint("CheckResult")
    @Override
    public void enroll(int idApp, String idioma, IngoEnrollmentRequest request,
                       InteractorCallback<IngoUserData> callback) {
        service.enrollCustomer(idApp, idioma, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ingoUserData -> {
                    if (ingoUserData.getErrorCode() == 0) {
                        callback.onSuccess(ingoUserData);
                    } else {
                        callback.onError(ingoUserData.getErrorMessage());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError()));
    }
}
