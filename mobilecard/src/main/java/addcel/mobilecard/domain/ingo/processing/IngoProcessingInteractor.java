package addcel.mobilecard.domain.ingo.processing;

import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.ingo.model.IngoCardRequest;
import addcel.mobilecard.data.net.ingo.model.IngoCardResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 1/14/19.
 */
public interface IngoProcessingInteractor {

    Usuario getUsuario();

    void clearDisposables();

    void addOrUpdateCard(int idApp, String idioma, IngoCardRequest request,
                         InteractorCallback<IngoCardResponse> callback);

    void checkIfIngoActive(InteractorCallback<IngoAliveResponse> interactorCallback);
}
