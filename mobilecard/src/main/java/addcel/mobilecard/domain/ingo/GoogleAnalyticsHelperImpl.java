package addcel.mobilecard.domain.ingo;

import android.app.Activity;
import android.content.Context;

import com.ingomoney.ingosdk.android.manager.GoogleAnalyticsHelper;

/**
 * ADDCEL on 25/10/16.
 */
public class GoogleAnalyticsHelperImpl implements GoogleAnalyticsHelper {
    public GoogleAnalyticsHelperImpl() {
    }

    @Override
    public void trackActivityStart(Activity activity) {
    }

    @Override
    public void trackActivityStop(Activity activity) {
    }

    @Override
    public void attemptedPromo(Context context) {
    }

    @Override
    public void promoSuccess(Context context) {
    }

    @Override
    public void promoFailure(Context context) {
    }

    @Override
    public void feeTypeSwitched(Context context) {
    }

    @Override
    public void declinedTermsAndConditions(Context context) {
    }

    @Override
    public void declinedPrivacyPolicy(Context context) {
    }

    @Override
    public void retakeCheckImage(Context context) {
    }

    @Override
    public void negativeExperience(Context context) {
    }

    @Override
    public void positiveExperience(Context context) {
    }
}
