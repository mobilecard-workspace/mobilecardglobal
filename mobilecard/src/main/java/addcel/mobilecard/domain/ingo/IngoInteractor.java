package addcel.mobilecard.domain.ingo;

import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 1/14/19.
 */
public interface IngoInteractor {
    String INGO_AUTH_U = "guest";
    String INGO_AUTH_P = "guest";

    IngoService getService();

    IngoAliveAPI getAliveApi();

    void checkIngoData(int idApp, String idioma, String deviceId,
                       InteractorCallback<IngoUserData> callback);

    void checkIngoAlive(InteractorCallback<IngoAliveResponse> callback);
}
