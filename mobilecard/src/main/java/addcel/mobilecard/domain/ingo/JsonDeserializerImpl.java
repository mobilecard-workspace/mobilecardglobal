package addcel.mobilecard.domain.ingo;

import com.google.gson.Gson;
import com.ingomoney.ingosdk.android.manager.JsonDeserializer;

/**
 * ADDCEL on 25/10/16.
 */
public class JsonDeserializerImpl implements JsonDeserializer {
    private final Gson gson;

    public JsonDeserializerImpl(Gson gson) {
        this.gson = gson;
    }

    @Override
    public <T> T deserializeJsonIntoType(String s, Class<T> aClass) {
        return gson.fromJson(s, aClass);
    }
}
