package addcel.mobilecard.domain.ingo;

import android.annotation.SuppressLint;

import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.data.net.ingo.model.IngoLoginRequest;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Credentials;

/**
 * ADDCEL on 1/14/19.
 */
public class IngoInteractorImpl implements IngoInteractor {
    private final IngoService service;
    private final IngoAliveAPI ingoAliveAPI;
    private final long idUsuario;

    public IngoInteractorImpl(IngoService service, IngoAliveAPI ingoAliveAPI, long idUsuario) {
        this.service = service;
        this.idUsuario = idUsuario;
        this.ingoAliveAPI = ingoAliveAPI;
    }

    @Override
    public IngoService getService() {
        return service;
    }

    @Override
    public IngoAliveAPI getAliveApi() {
        return ingoAliveAPI;
    }

    @SuppressLint("CheckResult")
    @Override
    public void checkIngoData(int idApp, String idioma, String deviceId,
                              InteractorCallback<IngoUserData> callback) {

        String cred = Credentials.basic(IngoInteractor.INGO_AUTH_U, IngoInteractor.INGO_AUTH_P);

        IngoLoginRequest request =
                new IngoLoginRequest.Builder().withDeviceId(deviceId).withIdUsuario(idUsuario).build();

        final Disposable disposable = service.getSessionData(cred, idApp, idioma, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSuccess,
                        throwable -> callback.onError(StringUtil.getNetworkError()));
    }

    @Override
    public void checkIngoAlive(InteractorCallback<IngoAliveResponse> callback) {
        final Disposable disposable = ingoAliveAPI.systemAvailability()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSuccess, throwable -> callback.onError(ErrorUtil.Companion.getFormattedHttpErrorMsg(throwable)));
    }
}
