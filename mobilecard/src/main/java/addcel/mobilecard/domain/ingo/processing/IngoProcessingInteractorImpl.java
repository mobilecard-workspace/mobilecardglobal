package addcel.mobilecard.domain.ingo.processing;

import android.annotation.SuppressLint;

import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.data.net.ingo.model.IngoCardRequest;
import addcel.mobilecard.data.net.ingo.model.IngoCardResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.ErrorUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 1/14/19.
 */
public class IngoProcessingInteractorImpl implements IngoProcessingInteractor {
    private final IngoService service;
    private final IngoAliveAPI aliveAPI;
    private final Usuario usuario;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public IngoProcessingInteractorImpl(IngoService service, IngoAliveAPI aliveAPI, Usuario usuario) {
        this.service = service;
        this.aliveAPI = aliveAPI;
        this.usuario = usuario;
    }

    @Override
    public Usuario getUsuario() {
        return usuario;
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void addOrUpdateCard(int idApp, String idioma, IngoCardRequest request,
                                InteractorCallback<IngoCardResponse> callback) {
        disposables.add(service.addOrUpdateCard(idApp, idioma, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ingoCardResponse -> {
                    if (ingoCardResponse.getErrorCode() == 0) {
                        callback.onSuccess(ingoCardResponse);
                    } else {
                        callback.onError(ingoCardResponse.getErrorMessage());
                    }
                }, throwable -> {
                    callback.onError(ErrorUtil.Companion.getFormattedHttpErrorMsg(throwable));
                }));
    }

    @Override
    public void checkIfIngoActive(InteractorCallback<IngoAliveResponse> interactorCallback) {
        disposables.add(aliveAPI.systemAvailability()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(interactorCallback::onSuccess, throwable -> interactorCallback.onError(
                        ErrorUtil.Companion.getFormattedHttpErrorMsg(throwable))));
    }
}
