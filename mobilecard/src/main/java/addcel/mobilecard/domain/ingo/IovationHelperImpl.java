package addcel.mobilecard.domain.ingo;

import android.content.Context;

import com.ingomoney.ingosdk.android.manager.IovationHelper;

import addcel.mobilecard.utils.DeviceUtil;

/**
 * ADDCEL on 25/10/16.
 */
public class IovationHelperImpl implements IovationHelper {

    @Override
    public String generateIovationBlackbox(Context context) {
        return DeviceUtil.Companion.getDeviceId(context);
    }
}
