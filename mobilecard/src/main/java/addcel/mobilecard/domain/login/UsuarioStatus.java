package addcel.mobilecard.domain.login;

/**
 * ADDCEL on 11/28/18.
 */
public final class UsuarioStatus {
    public static final int BLOQUEADO = 0;
    public static final int ACTIVO = 1;
    public static final int FRAUDE = 3;
    public static final int PASS_RESET = 98;
    public static final int EMAIL_VERIFICATION = 99;
    public static final int SMS_VERIFICATION = 100;

    private UsuarioStatus() {
    }
}
