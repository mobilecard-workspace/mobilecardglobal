package addcel.mobilecard.domain.login.usuario;

import android.annotation.SuppressLint;

import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.data.net.usuarios.model.LoginRequest;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.login.UsuarioStatus;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 11/28/18.
 */
public class LoginUsuarioInteractorImpl implements LoginUsuarioInteractor {
    private final UsuariosService service;
    private final SessionOperations session;
    private final StateSession state;
    private final CompositeDisposable disposables;

    public LoginUsuarioInteractorImpl(UsuariosService service, SessionOperations session,
                                      StateSession state) {
        this.service = service;
        this.session = session;
        this.state = state;
        this.disposables = new CompositeDisposable();
    }

    @Override
    public boolean isFirstLaunch() {
        return !state.isMenuLaunched();
    }

    @SuppressLint("CheckResult")
    @Override
    public void login(int idApp, int idPais, String idioma, LoginRequest request,
                      InteractorCallback<Usuario> callback) {
        session.logout();
        disposables.add(service.login(idApp, idPais, idioma, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(usuario -> {
                    if (usuario.getIdError() == 0) {
                        storeUsuario(usuario, request.getUsrPwd(), callback);
                    } else {
                        callback.onError(usuario.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError())));
    }

    @Override
    public void storeUsuario(Usuario usuario, String enPassword,
                             InteractorCallback<Usuario> callback) {

        switch (usuario.getIdUsrStatus()) {
            case UsuarioStatus.ACTIVO:
            case UsuarioStatus.EMAIL_VERIFICATION:
            case UsuarioStatus.SMS_VERIFICATION:
                usuario.setUsrPwd(enPassword);
                session.setUsuario(usuario);
                session.setUsuarioLogged(Boolean.TRUE);
                callback.onSuccess(usuario);
                break;
            default:
                callback.onError(usuario.getMensajeError());
                break;
        }
    }

    @SuppressLint("CheckResult")
    @Override
    public void reset(int idApp, int idPais, String idioma, String loginOrEmail,
                      InteractorCallback<McResponse> callback) {
        disposables.add(service.resetPassword(idApp, idioma, loginOrEmail)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mcResponse -> {
                    if (mcResponse.getIdError() == 0) {
                        callback.onSuccess(mcResponse);
                    } else {
                        callback.onError(mcResponse.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError())));
    }

    @Override
    public void updateReference(int idApp, int idPais, String idioma, String loginOrEmail,
                                String reference, InteractorCallback<McResponse> callback) {
        disposables.add(service.updateReference(idApp, idPais, idioma, loginOrEmail, reference)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mcResponse -> {
                    if (mcResponse.getIdError() == 0) {
                        callback.onSuccess(mcResponse);
                    } else {
                        callback.onError(mcResponse.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError())));
    }

    @Override
    public void logout() {
        session.logout();
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }
}
