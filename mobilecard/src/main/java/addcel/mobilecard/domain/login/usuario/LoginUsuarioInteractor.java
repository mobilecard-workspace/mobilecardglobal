package addcel.mobilecard.domain.login.usuario;

import addcel.mobilecard.data.net.usuarios.model.LoginRequest;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 11/28/18.
 */
public interface LoginUsuarioInteractor {

    boolean isFirstLaunch();

    void login(int idApp, int idPais, String idioma, LoginRequest request,
               InteractorCallback<Usuario> callback);

    void storeUsuario(Usuario usuario, String enPassword, InteractorCallback<Usuario> callback);

    void reset(int idApp, int idPais, String idioma, String loginOrEmail,
               InteractorCallback<McResponse> callback);

    void updateReference(int idApp, int idPais, String idioma, String loginOrEmail, String reference,
                         InteractorCallback<McResponse> callback);

    void logout();

    void clearDisposables();
}
