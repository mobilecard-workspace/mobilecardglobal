package addcel.mobilecard.domain.pago;

import android.annotation.SuppressLint;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.pago.PagoApi;
import addcel.mobilecard.data.net.transacto.entity.PagoEntity;
import addcel.mobilecard.data.net.transacto.entity.TokenEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import mx.mobilecard.crypto.AddcelCrypto;

public class PagoInteractorImpl implements PagoInteractor {

    private final PagoApi api;

    public PagoInteractorImpl(PagoApi api) {
        this.api = api;
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadIntialUrl(String url, TokenEntity token, PagoEntity pagoEntity,
                              InteractorCallback<String> callback) {
        api.loadInitialModelFromJson(token.getToken(), BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(), token.getAccountId(),
                AddcelCrypto.encryptSensitive(JsonUtil.toJson(pagoEntity)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSuccess,
                        throwable -> callback.onError(StringUtil.getNetworkError()));
    }
}
