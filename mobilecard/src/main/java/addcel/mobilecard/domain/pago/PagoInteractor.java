package addcel.mobilecard.domain.pago;

import addcel.mobilecard.data.net.transacto.entity.PagoEntity;
import addcel.mobilecard.data.net.transacto.entity.TokenEntity;
import addcel.mobilecard.domain.InteractorCallback;

interface PagoInteractor {

    void loadIntialUrl(String url, TokenEntity token, PagoEntity pagoEntity,
                       InteractorCallback<String> callback);
}
