package addcel.mobilecard.domain.scanpay.manual.select;

import java.util.List;

import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;

/**
 * ADDCEL on 1/28/19.
 */
public final class ScanManualSelectEstablecimientoFilterEvent {
    private final List<LcpfEstablecimiento> establecimientos;

    public ScanManualSelectEstablecimientoFilterEvent(List<LcpfEstablecimiento> establecimientos) {
        this.establecimientos = establecimientos;
    }

    public List<LcpfEstablecimiento> getEstablecimientos() {
        return establecimientos;
    }
}
