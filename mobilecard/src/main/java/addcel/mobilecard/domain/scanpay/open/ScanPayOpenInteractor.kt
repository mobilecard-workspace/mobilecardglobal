package addcel.mobilecard.domain.scanpay.open

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenBaseResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 4/22/19.
 */
class ScanPayOpenInteractor(
        val spApi: SPApi, val tokenApi: TokenizerAPI,
        val session: SessionOperations, val disposables: CompositeDisposable
) {

    fun clearDisposables() {
        disposables.clear()
    }

    fun getScanReference(): String {
        return session.usuario.scanReference
    }

    fun getIdPais(tipoNegocio: String): Int {
        return if (tipoNegocio == SPApi.TIPO_NEGOCIO) session.negocio.idPais else session.usuario.idPais
    }

    fun getIdUsuario(): Long {
        return session.usuario.ideUsuario
    }

    fun getIdNegocio(): Int {
        return session.negocio.idEstablecimiento
    }

    fun getLocation(): McLocationData {
        return session.minimalLocationData
    }

    fun isOnWhiteList(
            idApp: Int,
            idioma: String,
            idPais: Int,
            callback: InteractorCallback<TokenBaseResponse>
    ) {
        disposables.add(
                tokenApi.isOnWhiteList(idApp, idPais, idioma, session.usuario.ideUsuario)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { callback.onSuccess(it) },
                                { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) }
                        )
        )
    }

    fun getToken(
            idApp: Int, idioma: String, idPais: Int, auth: String, profile: String,
            callback: InteractorCallback<TokenEntity>
    ) {
        disposables.add(
                tokenApi.getToken(auth, profile, idApp, idPais, idioma).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
                    if (t.code == 0) {
                        callback.onSuccess(t)
                    } else {
                        callback.onError(t.message)
                    }
                }, { t ->
                    callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t))
                })
        )
    }

    fun pagoBP(
            idApp: Int, idioma: String, token: String, tipoUsuario: String, accountId: String,
            pago: SPPagoEntity, callback: InteractorCallback<SPReceiptEntity>
    ) {
        disposables.add(
                spApi.pagoBP(
                        token, idApp, getIdPais(tipoUsuario), idioma, tipoUsuario, accountId,
                        pago
                ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                        { t -> callback.onSuccess(t) }, { t ->
                    callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t))
                })
        )
    }

    fun pagoBPById(
            idApp: Int, idioma: String, token: String, accountId: String, pago: SPPagoEntity,
            callback: InteractorCallback<SPReceiptEntity>
    ) {
        disposables.add(
                spApi.pagoBPById(
                        token, idApp, session.usuario.idPais, idioma, accountId, pago.idUser,
                        pago.idCard, pago.idBitacora
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ t ->
                    callback.onSuccess(t)
                }, { t -> callback.onError(StringUtil.getNetworkError()) })
        )
    }
}