package addcel.mobilecard.domain.scanpay.manual.select;

import com.squareup.otto.Bus;

import java.util.List;

import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 1/28/19.
 */
public interface ScanManualSelectEstablecimientoInteractor {
    Bus getBus();

    CompositeDisposable getCompositeDisposable();

    void downloadItems(int idApp, String idioma);

    void filterItems(String pattern);

    List<LcpfEstablecimiento> getItems();

    void clearItems();
}
