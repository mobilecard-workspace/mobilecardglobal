package addcel.mobilecard.domain.scanpay.manual.select;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.stream.JsonReader;
import com.squareup.otto.Bus;

import java.io.Reader;
import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimientoRequest;
import addcel.mobilecard.utils.JsonUtil;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

/**
 * ADDCEL on 1/28/19.
 */
public class ScanManualSelectEstablecimientoInteractorImpl
        implements ScanManualSelectEstablecimientoInteractor {
    private final ScanPayService api;
    private final SessionOperations session;
    private final Bus bus;
    private final List<LcpfEstablecimiento> items;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ScanManualSelectEstablecimientoInteractorImpl(ScanPayService api,
                                                         SessionOperations session, Bus bus) {
        this.api = api;
        this.session = session;
        this.bus = bus;
        this.items = Lists.newArrayList();
    }

    @Override
    public Bus getBus() {
        return bus;
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @SuppressLint("CheckResult")
    @Override
    public void downloadItems(int idApp, String idioma) {

        LcpfEstablecimientoRequest request =
                new LcpfEstablecimientoRequest.Builder().withLat(session.getMinimalLocationData().getLat())
                        .withLon(session.getMinimalLocationData().getLon())
                        .withUbicacionActual(session.getMinimalLocationData().getCurrAddress())
                        .build();

        RequestBody body =
                RequestBody.create(MediaType.parse("application/json"), JsonUtil.toJson(request));

        api.getEstablecimientos(idApp, session.getUsuario().getIdPais(), idioma, body)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMap(responseBody -> models(responseBody.charStream()))
                .subscribe(items::add, throwable -> bus.post(
                        new ScanManualSelectEstablecimientoErrorEvent(R.string.error_default)),
                        () -> bus.post(new ScanManualSelectEstablecimientoDownloadEvent()),
                        compositeDisposable::add);
    }

    @Override
    public void filterItems(String pattern) {
        if (Strings.isNullOrEmpty(pattern)) {
            bus.post(new ScanManualSelectEstablecimientoFilterEvent(items));
        } else {
            List<LcpfEstablecimiento> temp = Lists.newArrayList();
            Observable.just(items)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMap(
                            (Function<List<LcpfEstablecimiento>, Observable<LcpfEstablecimiento>>) Observable::fromIterable)
                    .filter(
                            serviceModel -> serviceModel.getAlias().toLowerCase().contains(pattern.toLowerCase()))
                    .subscribe(new Observer<LcpfEstablecimiento>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(LcpfEstablecimiento establecimiento) {
                            temp.add(establecimiento);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            bus.post(new ScanManualSelectEstablecimientoFilterEvent(temp));
                        }
                    });
        }
    }

    @Override
    public List<LcpfEstablecimiento> getItems() {
        return items;
    }

    @Override
    public void clearItems() {
        items.clear();
    }

    private Observable<LcpfEstablecimiento> models(Reader charStream) {
        return Observable.create(emitter -> {
            try {
                JsonReader reader = new JsonReader(charStream);
                reader.beginObject();
                Timber.d("Objeto iniciado");
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    Timber.d(name);
                    if (name.equals("idError")) reader.nextInt();
                    if (name.equals("mensajeError")) reader.nextString();
                    if (name.equals("accounts")) {
                        reader.beginArray();
                        Timber.d("Array iniciado");
                        while (reader.hasNext()) {
                            LcpfEstablecimiento message = JsonUtil.fromJson(reader, LcpfEstablecimiento.class);
                            emitter.onNext(message);
                        }
                        reader.endArray();
                    }
                }
                reader.endObject();
                reader.close();
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }
}
