package addcel.mobilecard.domain.scanpay.manual.select;

import androidx.annotation.StringRes;

/**
 * ADDCEL on 1/28/19.
 */
public final class ScanManualSelectEstablecimientoErrorEvent {
    private final int idError;

    public ScanManualSelectEstablecimientoErrorEvent(@StringRes int idError) {
        this.idError = idError;
    }

    public int getIdError() {
        return idError;
    }
}
