package addcel.mobilecard.domain.scanpay.secure

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.ScanPayService
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.ui.login.tipo.LoginTipoContract
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * ADDCEL on 3/26/19.
 */
class ScanPaySecureInteractor(
        val api: ScanPayService, val session: SessionOperations,
        val disposables: CompositeDisposable
) {

    fun clearDisposable() {
        disposables.clear()
    }

    fun getIdPais(): Int {
        return session.usuario.idPais
    }

    fun payment(
            idApp: Int, idPais: Int, idioma: String, tipoUsuario: LoginTipoContract.Tipo,
            request: SPPagoEntity, callback: InteractorCallback<String>
    ) {
        disposables.add(
                api.payment(
                        idApp,
                        idPais,
                        idioma,
                        tipoUsuario.name.toLowerCase(Locale.getDefault()),
                        request.vigencia,
                        request.amount,
                        request.msi,
                        request.tipoTarjeta,
                        request.idBitacora,
                        request.idCard,
                        request.concept,
                        request.propina,
                        request.lon,
                        request.firma,
                        request.idUser,
                        request.ct,
                        request.referenciaNeg,
                        request.comision,
                        request.establecimientoId,
                        request.tarjeta,
                        request.lat
                ).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                        { s -> callback.onSuccess(s) },
                        { t -> callback.onError(StringUtil.getNetworkError()) })
        )
    }

    fun paymentById(
            idApp: Int, idPais: Int, idioma: String, request: SPPagoEntity,
            callback: InteractorCallback<String>
    ) {
        disposables.add(
                api.paymentById(
                        idApp, idPais, idioma, request.vigencia, request.amount, request.msi,
                        request.tipoTarjeta, request.idBitacora, request.idCard, request.concept,
                        request.propina, request.lon, request.firma, request.idUser, request.ct,
                        request.referenciaNeg, request.comision, request.establecimientoId, request.tarjeta,
                        request.lat
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ s -> callback.onSuccess(s) },
                        { t -> callback.onError(StringUtil.getNetworkError()) })
        )
    }
}