package addcel.mobilecard.domain.peru

import addcel.mobilecard.data.net.tebca.CommerceAccountsAPI
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-08-28.
 */
interface NegocioPeWalletInteractor {

    fun clearDisposables()

    fun getAccountInfo(
            idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<CommerceAccountsResponse>
    )

    fun requestCard(
            idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<CommerceAccountsResponse>
    )

    fun blockCard(
            idApp: Int, idPais: Int, idioma: String, isBlocked: Int,
            callback: InteractorCallback<CommerceAccountsResponse>
    )

    fun favCard(
            idApp: Int, idPais: Int, idioma: String, type: Int,
            callback: InteractorCallback<CommerceAccountsResponse>
    )

    fun replaceCard(
            idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<CommerceAccountsResponse>
    )
}

class NegocioPeWalletInteractorImpl(
        val accounts: CommerceAccountsAPI, val negocio: Int,
        val disposables: CompositeDisposable
) : NegocioPeWalletInteractor {
    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getAccountInfo(
            idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<CommerceAccountsResponse>
    ) {
        disposables.add(accounts.getCommerceAccounts(idApp, idPais, idioma, negocio).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { r -> callback.onSuccess(r) }, { t ->
            callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }

    override fun requestCard(
            idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<CommerceAccountsResponse>
    ) {
        disposables.add(accounts.applyForDigitalCard(idApp, idPais, idioma, negocio).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { r -> callback.onSuccess(r) }, { t ->
            callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }

    override fun blockCard(
            idApp: Int, idPais: Int, idioma: String, isBlocked: Int,
            callback: InteractorCallback<CommerceAccountsResponse>
    ) {
        disposables.add(accounts.blockCard(idApp, idPais, idioma, isBlocked, negocio).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { r -> callback.onSuccess(r) }, { t ->
            callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }

    override fun favCard(
            idApp: Int, idPais: Int, idioma: String, type: Int,
            callback: InteractorCallback<CommerceAccountsResponse>
    ) {
        disposables.add(accounts.setFavoriteAccount(
                idApp,
                idPais,
                idioma,
                negocio,
                type
        ).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { r -> callback.onSuccess(r) }, { t ->
            callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }

    override fun replaceCard(
            idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<CommerceAccountsResponse>
    ) {
        disposables.add(
                accounts.replaceCard(
                        idApp,
                        idPais,
                        idioma,
                        negocio
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r -> callback.onSuccess(r) }, { t ->
                    callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        )
    }
}