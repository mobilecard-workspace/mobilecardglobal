package addcel.mobilecard.domain.peru

import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.negocios.entity.BankResponse
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse
import addcel.mobilecard.data.net.negocios.entity.NegocioAltaRequest
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.data.net.usuarios.model.TerminosResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-07-11.
 */
interface PeruNegocioRegistroInteractor {

    fun clear()

    fun getBanks(idApp: Int, idioma: String, callback: InteractorCallback<BankResponse>)

    fun saveUser(
            idApp: Int, idioma: String, request: NegocioAltaRequest,
            callback: InteractorCallback<NegocioEntity>
    )

    fun getTerms(
            idApp: Int, method: String, idioma: String,
            callback: InteractorCallback<TerminosResponse>
    )

    fun updateReference(
            idApp: Int, idioma: String, login: String, scanReference: String,
            callback: InteractorCallback<DefaultResponse>
    )
}

class PeruNegocioRegistroInteractorImpl(val api: NegocioAPI, val disposables: CompositeDisposable) :
        PeruNegocioRegistroInteractor {

    override fun getBanks(idApp: Int, idioma: String, callback: InteractorCallback<BankResponse>) {
        disposables.add(
                api.getBanks(idApp, 4, idioma).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ callback.onSuccess(it) }, {
                    callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        )
    }

    override fun clear() {
        disposables.clear()
    }

    override fun saveUser(
            idApp: Int, idioma: String, request: NegocioAltaRequest,
            callback: InteractorCallback<NegocioEntity>
    ) {
        disposables.add(
                api.create(idApp, idioma, request).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ callback.onSuccess(it) }, {
                    callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        )
    }

    override fun updateReference(
            idApp: Int, idioma: String, login: String, scanReference: String,
            callback: InteractorCallback<DefaultResponse>
    ) {
        disposables.add(
                api.updateReference(idApp, 4, idioma, login, scanReference)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    if (it.idError == 0) callback.onSuccess(it)
                                    else
                                        callback.onError(it.mensajeError)
                                }, {
                            callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })
        )
    }

    override fun getTerms(
            idApp: Int, method: String, idioma: String,
            callback: InteractorCallback<TerminosResponse>
    ) {
        disposables.add(api.getLegalInfo(
                idApp, method, StringUtil.sha256("mobilecardandroid"), 1,
                idioma
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { callback.onSuccess(it) },
                        { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) }
                )
        )
    }
}