package addcel.mobilecard.domain.peru

import addcel.mobilecard.data.net.tebca.CommerceAccountsAPI
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.data.net.tebca.TebcaActivationRequest
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-08-30.
 */
interface NegocioPeWalletActivationInteractor {

    fun clearDisposables()

    fun activateCard(
            idApp: Int, idPais: Int, idioma: String, request: TebcaActivationRequest,
            callback: InteractorCallback<CommerceAccountsResponse>
    )
}

class NegocioPeWalletActivationInteractorImpl(
        val accounts: CommerceAccountsAPI,
        val disposables: CompositeDisposable
) : NegocioPeWalletActivationInteractor {
    override fun clearDisposables() {
        disposables.clear()
    }

    override fun activateCard(
            idApp: Int, idPais: Int, idioma: String,
            request: TebcaActivationRequest, callback: InteractorCallback<CommerceAccountsResponse>
    ) {
        disposables.add(accounts.activatePhysicalCard(idApp, idPais, idioma, request).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { r -> callback.onSuccess(r) }, { t ->
            callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }
}