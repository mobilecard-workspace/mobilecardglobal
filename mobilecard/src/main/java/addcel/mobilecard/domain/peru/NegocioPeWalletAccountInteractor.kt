package addcel.mobilecard.domain.peru

import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.negocios.entity.BankResponse
import addcel.mobilecard.data.net.tebca.CciRequest
import addcel.mobilecard.data.net.tebca.CommerceAccountsAPI
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-08-26.
 */
interface NegocioPeWalletAccountInteractor {
    fun clearDisposables()
    fun getBanks(
            idApp: Int,
            idPais: Int,
            idioma: String,
            callback: InteractorCallback<BankResponse>
    )

    fun addAccount(
            idApp: Int, idPais: Int, idioma: String, request: CciRequest,
            callback: InteractorCallback<CommerceAccountsResponse>
    )
}

class NegocioPeWalletAccountInteractorImpl(
        private val negocioApi: NegocioAPI,
        private val accounts: CommerceAccountsAPI, val disposables: CompositeDisposable
) :
        NegocioPeWalletAccountInteractor {

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getBanks(
            idApp: Int, idPais: Int, idioma: String,
            callback: InteractorCallback<BankResponse>
    ) {
        disposables.add(
                negocioApi.getBanks(idApp, idPais, idioma).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r -> callback.onSuccess(r) }, { t ->
                    callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        )
    }

    override fun addAccount(
            idApp: Int, idPais: Int, idioma: String, request: CciRequest,
            callback: InteractorCallback<CommerceAccountsResponse>
    ) {
        disposables.add(accounts.updatePersonalAccount(idApp, idPais, idioma, request).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { r -> callback.onSuccess(r) }, { t ->
            callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }
}