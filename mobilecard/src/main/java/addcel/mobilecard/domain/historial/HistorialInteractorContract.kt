package addcel.mobilecard.domain.historial

import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.TransactionResponse
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-06-12.
 */
interface HistorialInteractor {

    fun clearDisposables()

    fun addToDisposables(d: Disposable)

    fun getTransactions(
            idApp: Int,
            idioma: String,
            callback: InteractorCallback<TransactionResponse>
    )
}

class HistorialInteractorImpl(
        val api: WalletAPI, val usuario: Usuario,
        val disposables: CompositeDisposable
) : HistorialInteractor {
    override fun clearDisposables() {
        disposables.clear()
    }

    override fun addToDisposables(d: Disposable) {
        disposables.add(d)
    }

    override fun getTransactions(
            idApp: Int, idioma: String,
            callback: InteractorCallback<TransactionResponse>
    ) {
        disposables.add(
                api.transactions(
                        idApp,
                        usuario.idPais,
                        idioma,
                        usuario.ideUsuario
                ).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    if (r.idError == 0) callback.onSuccess(r) else callback.onError(r.mensajeError)

                }, { t ->
                    t.printStackTrace()
                    callback.onError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        )
    }
}
