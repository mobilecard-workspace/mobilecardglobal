package addcel.mobilecard.domain.wallet.tebca

import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.data.net.catalogo.DeptoEntity
import addcel.mobilecard.data.net.catalogo.DistritoEntity
import addcel.mobilecard.data.net.catalogo.ProvinciaEntity
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.CardResponse
import addcel.mobilecard.data.net.wallet.TebcaAltaRequest
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import com.google.common.collect.Lists
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-08-15.
 */
interface TebcaFormInteractor {

    fun clearDisposables()

    fun loadDepartamentos(
            idApp: Int,
            idioma: String,
            callback: InteractorCallback<List<DeptoEntity>>
    )

    fun loadProvincias(
            idApp: Int, idioma: String, depto: String,
            callback: InteractorCallback<List<ProvinciaEntity>>
    )

    fun loadDistritos(
            idApp: Int, idioma: String, provincia: String,
            callback: InteractorCallback<List<DistritoEntity>>
    )

    fun saveCard(
            idApp: Int, idioma: String, body: TebcaAltaRequest,
            callback: InteractorCallback<CardResponse>
    )
}

class TebcaFormInteractorImpl(
        val wallet: WalletAPI, val catalogo: CatalogoAPI, val disposables: CompositeDisposable
) : TebcaFormInteractor {

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun loadDepartamentos(
            idApp: Int, idioma: String,
            callback: InteractorCallback<List<DeptoEntity>>
    ) {
        disposables.add(
                catalogo.getDepartamentos(idApp, 4, StringUtil.getCurrentLanguage()).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                        { r -> callback.onSuccess(r) }, { t ->
                    callback.onError(
                            t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)
                    );callback.onSuccess(
                        Lists.newArrayList()
                )
                })
        )
    }

    override fun loadProvincias(
            idApp: Int, idioma: String, depto: String,
            callback: InteractorCallback<List<ProvinciaEntity>>
    ) {
        disposables.add(
                catalogo.getProvincias(idApp, 4, StringUtil.getCurrentLanguage(), depto).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                        { r -> callback.onSuccess(r) }, { t ->
                    callback.onError(
                            t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)
                    );callback.onSuccess(
                        Lists.newArrayList()
                )
                })
        )
    }

    override fun loadDistritos(
            idApp: Int, idioma: String, provincia: String,
            callback: InteractorCallback<List<DistritoEntity>>
    ) {
        disposables.add(
                catalogo.getDistritos(idApp, 4, StringUtil.getCurrentLanguage(), provincia).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                        { r -> callback.onSuccess(r) }, { t ->
                    callback.onError(
                            t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)
                    );callback.onSuccess(
                        Lists.newArrayList()
                )
                })
        )
    }

    override fun saveCard(
            idApp: Int, idioma: String, body: TebcaAltaRequest,
            callback: InteractorCallback<CardResponse>
    ) {
        disposables.add(
                wallet.saveTebcaCard(idApp, 4, idioma, body).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r -> callback.onSuccess(r) }, { t ->
                    callback.onError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        )
    }
}