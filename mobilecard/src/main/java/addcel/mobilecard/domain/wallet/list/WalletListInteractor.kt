package addcel.mobilecard.domain.wallet.list

import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.*
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-05-23.
 */
class WalletListInteractor(
        val api: WalletAPI, val usuario: Usuario,
        val disposables: CompositeDisposable
) {
    fun clearDisposables() {
        disposables.clear()
    }

    fun list(idApp: Int, idioma: String, callback: InteractorCallback<List<CardEntity>>) {
        disposables.add(
                api.getTarjetas(
                        idApp,
                        usuario.ideUsuario,
                        idioma
                ).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) callback.onSuccess(it.tarjetas)
                            else callback.onError(it.mensajeError)
                        }, {
                            it.printStackTrace()
                            callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })
        )
    }

    fun delete(
            idApp: Int,
            idioma: String,
            idCard: Int,
            callback: InteractorCallback<CardResponse>
    ) {
        disposables.add(api.deleteTarjeta(idApp, idCard, usuario.ideUsuario, idioma).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { callback.onSuccess(it) }, {
            it.printStackTrace()
            callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
        })
        )
    }

    fun fav(idApp: Int, idioma: String, idCard: Int, callback: InteractorCallback<CardResponse>) {

        val request = DefaultRequest(usuario.ideUsuario, idCard)

        disposables.add(api.setDefaultCard(idApp, usuario.idPais, idioma, request).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { callback.onSuccess(it) }, {
            it.printStackTrace()
            callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
        })
        )
    }

    fun listLastMovements(
            idApp: Int, idioma: String,
            callback: InteractorCallback<List<MovementEntity>>
    ) {
        disposables.add(api.getMovementsByUser(idApp, usuario.ideUsuario, idioma).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { callback.onSuccess(it.fromMonth) }, {
            it.printStackTrace()
            callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
        })
        )
    }
}