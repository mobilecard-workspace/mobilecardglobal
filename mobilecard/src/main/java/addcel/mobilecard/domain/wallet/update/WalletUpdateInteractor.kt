package addcel.mobilecard.domain.wallet.update

import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.CardRequest
import addcel.mobilecard.data.net.wallet.CardResponse
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-05-23.
 */
class WalletUpdateInteractor(
        val api: WalletAPI, val catalogoService: CatalogoService, val usuario: Usuario,
        val disposables: CompositeDisposable
) {
    fun clearDisposables() {
        disposables.clear()
    }

    fun getIdUsuario(): Long {
        return usuario.ideUsuario
    }

    fun getEstados(idApp: Int, idioma: String, callback: InteractorCallback<EstadoResponse>) {
        disposables.add(
                catalogoService.getEstados(idApp, usuario.idPais, idioma)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            callback.onSuccess(it)
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    fun update(idApp: Int, request: CardRequest, callback: InteractorCallback<CardResponse>) {
        disposables.add(
                api.updateTarjeta(idApp, request).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r -> callback.onSuccess(r) }, { t ->
                    t.printStackTrace()
                    callback.onError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        )
    }

    fun delete(
            idApp: Int,
            idioma: String,
            idCard: Int,
            callback: InteractorCallback<CardResponse>
    ) {
        disposables.add(api.deleteTarjeta(idApp, idCard, usuario.ideUsuario, idioma).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { r -> callback.onSuccess(r) }, { t ->
            t.printStackTrace()
            callback.onError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }
}