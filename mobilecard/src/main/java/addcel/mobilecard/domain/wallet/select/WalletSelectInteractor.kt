package addcel.mobilecard.domain.wallet.select

import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-05-22.
 */
class WalletSelectInteractor(
        val api: WalletAPI, val usuario: Usuario,
        val disposables: CompositeDisposable
) {

    fun clearDisposables() {
        disposables.clear()
    }

    fun list(idAplicacion: Int, idioma: String, callback: InteractorCallback<List<CardEntity>>) {
        disposables.add(
                api.getTarjetas(idAplicacion, usuario.ideUsuario, idioma).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    if (r.idError == 0) {
                        if (r.tarjetas.isEmpty()) callback.onError(r.mensajeError)
                        callback.onSuccess(r.tarjetas)
                    } else r.mensajeError.let { callback.onError(it) }
                }, { callback.onError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)) })
        )
    }

    fun list(
            idAplicacion: Int, filter: String, idioma: String,
            callback: InteractorCallback<List<CardEntity>>
    ) {
        disposables.add(
                api.getTarjetasWithFilter(idAplicacion, filter, usuario.ideUsuario, idioma).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    if (r.idError == 0) {
                        if (r.tarjetas.isEmpty()) callback.onError(r.mensajeError)
                        callback.onSuccess(r.tarjetas)
                    } else r.mensajeError.let { callback.onError(it) }
                }, { callback.onError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)) })
        )
    }
}