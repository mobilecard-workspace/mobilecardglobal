package addcel.mobilecard.domain.wallet.add

import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.CardRequest
import addcel.mobilecard.data.net.wallet.CardResponse
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-05-23.
 */
class WalletCreateInteractor(
        val api: WalletAPI, val catalogoService: CatalogoService, val usuario: Usuario,
        val disposables: CompositeDisposable
) {
    fun clearDisposables() {
        disposables.clear()
    }

    fun getIdUsuario(): Long {
        return usuario.ideUsuario
    }

    fun getUsuarioName(): String {
        return "${usuario.usrNombre} ${usuario.usrApellido} ${usuario.usrMaterno}"
    }

    fun getEstados(idApp: Int, idioma: String, callback: InteractorCallback<EstadoResponse>) {
        disposables.add(
                catalogoService.getEstados(idApp, usuario.idPais, idioma)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            callback.onSuccess(it)
                        }, { callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it)) })
        )
    }

    fun create(idApp: Int, request: CardRequest, callback: InteractorCallback<CardResponse>) {
        disposables.add(
                api.addTarjeta(idApp, request).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r -> callback.onSuccess(r) }, { t ->
                    t.printStackTrace()
                    callback.onError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        )
    }
}