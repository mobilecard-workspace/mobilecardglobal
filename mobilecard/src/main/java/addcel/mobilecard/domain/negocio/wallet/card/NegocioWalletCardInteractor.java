package addcel.mobilecard.domain.negocio.wallet.card;

import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 12/12/18.
 */
public interface NegocioWalletCardInteractor {
    CardEntity getCard();

    String getNombre();
}
