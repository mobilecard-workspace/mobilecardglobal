package addcel.mobilecard.domain.negocio.cobro.usercard.signature;

import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

import addcel.mobilecard.data.net.scanpay.SPApi;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 1/28/19.
 */
public class CobroSignatureInteractorImpl implements CobroSignatureInteractor {

    private final ScanPayOpenInteractor interactor;

    public CobroSignatureInteractorImpl(ScanPayOpenInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return interactor.getDisposables();
    }

    @Override
    public void getFirma(Bitmap bitmap, InteractorCallback<String> callback) {
        Single<String> single = Single.just(bitmap).map(bmp -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            return Base64.encodeToString(imageBytes, Base64.DEFAULT);
        });

        interactor.getDisposables()
                .add(single.subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(callback::onSuccess,
                                throwable -> callback.onError("Ocurrió un error al procesar firma")));
    }

    @Override
    public void getToken(int idApp, String idioma, String authorization, String profile,
                         InteractorCallback<TokenEntity> callback) {
        interactor.getToken(idApp, idioma, 1, authorization, profile, callback);
    }

    @Override
    public void pagoBP(int idApp, String idioma, TokenEntity token, SPPagoEntity pago,
                       InteractorCallback<SPReceiptEntity> callback) {
        interactor.pagoBP(idApp, idioma, token.getToken(), SPApi.TIPO_NEGOCIO, token.getAccountId(),
                pago, callback);
    }
}
