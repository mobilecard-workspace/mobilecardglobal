package addcel.mobilecard.domain.negocio.wallet;

import com.squareup.otto.Bus;

import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 12/11/18.
 */
public interface NegocioWalletInteractor {

    Bus getBus();

    NegocioEntity getNegocioInfo();

    void getEstados(int idApp, String idioma);

    void registrar(int idApp, String idioma, PrevivaleUserRequest user,
                   InteractorCallback<CardEntity> callback);

    void clearDisposables();
}
