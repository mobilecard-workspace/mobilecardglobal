package addcel.mobilecard.domain.negocio.wallet.card;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 12/12/18.
 */
public class NegocioWalletCardInteractorImpl implements NegocioWalletCardInteractor {
    private final SessionOperations session;
    private final CardEntity card;

    public NegocioWalletCardInteractorImpl(NegocioAPI api, SessionOperations session,
                                           CardEntity card) {
        this.session = session;
        this.card = card;
    }

    @Override
    public CardEntity getCard() {
        return card;
    }

    @Override
    public String getNombre() {
        NegocioEntity entity = session.getNegocio();
        return entity.getRepresentanteLegal();
    }
}
