package addcel.mobilecard.domain.negocio.cobro.menu;

import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 12/3/18.
 */
public interface NegocioCobroMenuInteractor {
    void verify(int idApp, String idioma, InteractorCallback<ValidationEntity> callback);

    void resendActivationLink(int idApp, String idioma, InteractorCallback<DefaultResponse> callback);

    NegocioEntity getNegocio();

    int getIdAccount();

    String getComercioName();

    void updateToken(int idApp, String idioma, String token);
}
