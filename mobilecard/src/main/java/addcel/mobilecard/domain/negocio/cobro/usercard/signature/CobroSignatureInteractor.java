package addcel.mobilecard.domain.negocio.cobro.usercard.signature;

import android.graphics.Bitmap;

import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.domain.InteractorCallback;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 1/28/19.
 */
public interface CobroSignatureInteractor {

    CompositeDisposable getCompositeDisposable();

    void getFirma(Bitmap bitmap, InteractorCallback<String> callback);

    void getToken(int idApp, String idioma, String authorization, String profile,
                  InteractorCallback<TokenEntity> callback);

    void pagoBP(int idApp, String idioma, TokenEntity token, SPPagoEntity pago,
                InteractorCallback<SPReceiptEntity> callback);
}
