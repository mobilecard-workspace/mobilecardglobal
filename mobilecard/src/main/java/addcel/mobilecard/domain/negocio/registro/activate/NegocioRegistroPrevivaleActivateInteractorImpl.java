package addcel.mobilecard.domain.negocio.registro.activate;

import android.annotation.SuppressLint;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 11/23/18.
 */
public class NegocioRegistroPrevivaleActivateInteractorImpl
        implements NegocioRegistroPrevivaleActivateInteractor {
    private final CatalogoService api;
    private final CompositeDisposable disposables;

    public NegocioRegistroPrevivaleActivateInteractorImpl(CatalogoService api) {
        this.api = api;
        disposables = new CompositeDisposable();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getEstados(int idApp, String idioma,
                           InteractorCallback<List<EstadoResponse.EstadoEntity>> callback) {
        disposables.add(api.getEstados(idApp, 1, idioma)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(estadoResponse -> {
                    if (estadoResponse.getIdError() == 0) {
                        callback.onSuccess(estadoResponse.getEstados());
                    } else {
                        callback.onError(estadoResponse.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError())));
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }
}
