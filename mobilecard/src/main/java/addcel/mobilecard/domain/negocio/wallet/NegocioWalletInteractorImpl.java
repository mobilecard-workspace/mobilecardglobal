package addcel.mobilecard.domain.negocio.wallet;

import android.annotation.SuppressLint;

import com.squareup.otto.Bus;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 12/11/18.
 */
public class NegocioWalletInteractorImpl implements NegocioWalletInteractor {
    private final WalletAPI api;
    private final CatalogoService catalogo;
    private final Bus bus;
    private final SessionOperations session;
    private final CompositeDisposable disposables;

    public NegocioWalletInteractorImpl(WalletAPI api, CatalogoService catalogo, Bus bus,
                                       SessionOperations session) {
        this.api = api;
        this.catalogo = catalogo;
        this.bus = bus;
        this.session = session;
        disposables = new CompositeDisposable();
    }

    @Override
    public Bus getBus() {
        return bus;
    }

    @Override
    public NegocioEntity getNegocioInfo() {
        return session.getNegocio();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getEstados(int idApp, String idioma) {
        disposables.add(catalogo.getEstados(idApp, 1, idioma)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(estadoResponse -> {
                    try {
                        bus.post(new NegocioWalletEstadoEvent(estadoResponse));
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }, throwable -> bus.post(new NegocioWalletEstadoEvent(
                        new EstadoResponse(-9999, StringUtil.getNetworkError())))));
    }

    @SuppressLint("CheckResult")
    @Override
    public void registrar(int idApp, String idioma, PrevivaleUserRequest user,
                          InteractorCallback<CardEntity> c) {
        disposables.add(api.getPrevivaleCard(idApp, 1, idioma, WalletAPI.Companion.NEGOCIO,
                session.getNegocio().getIdEstablecimiento(), user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mobileCardCardResponse -> {
                    if (mobileCardCardResponse.getIdError() == 0) {
                        c.onSuccess(mobileCardCardResponse.getCard());
                    } else {
                        c.onError(mobileCardCardResponse.getMensajeError());
                    }
                }, throwable -> c.onError(StringUtil.getNetworkError())));
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }
}
