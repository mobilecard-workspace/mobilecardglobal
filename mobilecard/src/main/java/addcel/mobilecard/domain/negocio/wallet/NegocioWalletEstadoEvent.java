package addcel.mobilecard.domain.negocio.wallet;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;

/**
 * ADDCEL on 1/30/19.
 */
public class NegocioWalletEstadoEvent {
    private final EstadoResponse data;

    public NegocioWalletEstadoEvent(EstadoResponse data) {
        this.data = data;
    }

    public EstadoResponse getData() {
        return data;
    }
}
