package addcel.mobilecard.domain.negocio.registro.activate;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 11/23/18.
 */
public interface NegocioRegistroPrevivaleActivateInteractor {
    void getEstados(int idApp, String idioma,
                    InteractorCallback<List<EstadoResponse.EstadoEntity>> callback);

    void clearDisposables();
}
