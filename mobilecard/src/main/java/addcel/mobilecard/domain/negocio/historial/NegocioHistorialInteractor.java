package addcel.mobilecard.domain.negocio.historial;

import java.util.List;

import addcel.mobilecard.data.net.scanpay.model.SPCuentaEntity;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 11/14/18.
 */
public interface NegocioHistorialInteractor {

    void clearDisposables();

    void getCuentas(int idApp, int idPais, String idioma, InteractorCallback<List<SPCuentaEntity>> callback);
}
