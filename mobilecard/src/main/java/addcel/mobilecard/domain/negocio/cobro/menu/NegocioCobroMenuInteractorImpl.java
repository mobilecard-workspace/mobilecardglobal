package addcel.mobilecard.domain.negocio.cobro.menu;

import android.annotation.SuppressLint;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity;
import addcel.mobilecard.data.net.usuarios.model.PushTokenRequest;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.login.UsuarioStatus;
import addcel.mobilecard.utils.ErrorUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * ADDCEL on 12/3/18.
 */
public class NegocioCobroMenuInteractorImpl implements NegocioCobroMenuInteractor {
    private final NegocioAPI api;
    private final CompositeDisposable disposables;
    private final SessionOperations session;

    public NegocioCobroMenuInteractorImpl(NegocioAPI api, SessionOperations session) {
        this.api = api;
        this.disposables = new CompositeDisposable();
        this.session = session;
    }

    @SuppressLint("CheckResult")
    @Override
    public void verify(int idApp, String idioma, InteractorCallback<ValidationEntity> callback) {
        disposables.add(api.verify(idApp, idioma, session.getNegocio().getIdEstablecimiento())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(validationEntity -> {
                    if (validationEntity.getIdError() == 0) {
                        updateNegocio(validationEntity, callback);
                    } else {
                        callback.onError(validationEntity.getMensajeError());
                    }
                }, throwable -> callback.onError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.NETWORK))));
    }

    @SuppressLint("CheckResult")
    @Override
    public void resendActivationLink(int idApp, String idioma,
                                     InteractorCallback<DefaultResponse> callback) {
        disposables.add(
                api.resendActivationLink(idApp, idioma, session.getNegocio().getIdEstablecimiento())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> {
                            if (response.getIdError() == 0) {
                                callback.onSuccess(response);
                            } else {
                                callback.onError(response.getMensajeError());
                            }
                        }, throwable -> callback.onError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.NETWORK))));
    }

    @Override
    public NegocioEntity getNegocio() {
        return session.getNegocio();
    }

    @Override
    public int getIdAccount() {
        return session.getNegocio().getIdAccount();
    }

    @Override
    public String getComercioName() {
        return session.getNegocio().getNombreEstablecimiento();
    }

    @Override
    public void updateToken(int idApp, String idioma, String token) {

        PushTokenRequest request = new PushTokenRequest(idApp, getNegocio().getIdPais(), getNegocio().getIdEstablecimiento(), idioma, "ESTABLECIMIENTO", token);

        disposables.add(api.saveToken(NegocioAPI.PUSH_AUTH, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pushResponse -> Timber.d(pushResponse.getMessage()), Timber::e));
    }

    private void updateNegocio(ValidationEntity entity,
                               InteractorCallback<ValidationEntity> callback) {
        switch (entity.getEstatus()) {
            case UsuarioStatus.ACTIVO:
            case UsuarioStatus.EMAIL_VERIFICATION:
            case UsuarioStatus.SMS_VERIFICATION:
                NegocioEntity.Builder b = new NegocioEntity.Builder(session.getNegocio());
                b.setIdUsrStatus(entity.getEstatus());
                session.setNegocio(b.build());
                session.setNegocioLogged(Boolean.TRUE);
                callback.onSuccess(entity);
                break;
            default:
                session.setNegocio(null);
                session.setNegocioLogged(Boolean.FALSE);
                callback.onError(entity.getMensajeError());
                break;
        }
    }
}
