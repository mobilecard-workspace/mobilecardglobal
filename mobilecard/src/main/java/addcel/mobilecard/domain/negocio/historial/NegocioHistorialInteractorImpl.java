package addcel.mobilecard.domain.negocio.historial;

import android.annotation.SuppressLint;

import java.util.List;

import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.data.net.scanpay.model.SPCuentaEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 11/14/18.
 */
public class NegocioHistorialInteractorImpl implements NegocioHistorialInteractor {

    private final ScanPayService service;
    private final NegocioEntity negocioEntity;
    private final CompositeDisposable disposable = new CompositeDisposable();

    public NegocioHistorialInteractorImpl(ScanPayService service, NegocioEntity negocioEntity) {
        this.service = service;
        this.negocioEntity = negocioEntity;
    }

    @Override
    public void clearDisposables() {
        disposable.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCuentas(int idApp, int idPais, String idioma,
                           InteractorCallback<List<SPCuentaEntity>> callback) {
        disposable.add(service.getBills(idApp, idioma, idPais, negocioEntity.getIdEstablecimiento())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSuccess,
                        throwable -> callback.onError(StringUtil.getNetworkError())));
    }
}
