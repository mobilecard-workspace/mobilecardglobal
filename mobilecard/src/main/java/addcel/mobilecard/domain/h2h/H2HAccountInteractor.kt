package addcel.mobilecard.domain.h2h

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.hth.*
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-05-13.
 */
class H2HAccountInteractor(
        val api: H2HApi, val session: SessionOperations,
        val disposables: CompositeDisposable
) {

    fun getIdPais(): Int {
        return session.usuario.idPais
    }

    fun getIdUsuario(): Long {
        return session.usuario.ideUsuario
    }

    fun getAccounts(idApp: Int, idioma: String, callback: InteractorCallback<AccountResponse>) {
        disposables.add(api.toAccount(idApp, session.usuario.ideUsuario, idioma).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
            if (r.idError == 0) callback.onSuccess(r) else callback.onError(r.mensajeError)
        }, { t -> callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t)) })
        )
    }

    fun deleteAccount(
            idApp: Int, idioma: String, idAccount: Int,
            callback: InteractorCallback<AccountResponse>
    ) {
        disposables.add(
                api.deleteAccount(idApp, session.usuario.ideUsuario, idioma, idAccount).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    if (r.idError == 0) callback.onSuccess(r) else callback.onError(r.mensajeError)
                }, { t -> callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t)) })
        )
    }

    fun updateAccount(
            idApp: Int, request: AccountUpdateRequest,
            callback: InteractorCallback<AccountResponse>
    ) {
        disposables.add(api.updateAccount(idApp, request).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
        ).subscribe({ r ->
            if (r.idError == 0) callback.onSuccess(r) else callback.onError(r.mensajeError)
        }, { t -> callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t)) })
        )
    }

    fun addAccount(
            idApp: Int, request: AccountRequest,
            callback: InteractorCallback<AccountResponse>
    ) {
        disposables.add(api.enqueueAccountSignUp(
                idApp,
                request
        ).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
        ).subscribe({ r ->
            if (r.idError == 0) callback.onSuccess(r) else callback.onError(r.mensajeError)
        }, { t -> callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t)) })
        )
    }

    fun fetchBanks(idApp: Int, callback: InteractorCallback<List<BankEntity>>) {
        disposables.add(api.bankCodes(idApp).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
        ).subscribe({ r ->
            if (r.idError == 0) callback.onSuccess(r.banks) else callback.onError(r.mensajeError)
        }, { t -> callback.onError(ErrorUtil.getFormattedHttpErrorMsg(t)) })
        )
    }
}