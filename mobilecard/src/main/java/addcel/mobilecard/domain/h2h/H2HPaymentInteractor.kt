package addcel.mobilecard.domain.h2h

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.hth.H2HApi
import addcel.mobilecard.data.net.hth.PaymentEntity
import addcel.mobilecard.data.net.hth.ReceiptEntity
import addcel.mobilecard.data.net.token.TokenBaseResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-05-13.
 */
open class H2HPaymentInteractor(
        val h2hApi: H2HApi, val tokenApi: TokenizerAPI,
        val session: SessionOperations, val disposables: CompositeDisposable
) {

    fun clearDisposables() {
        disposables.clear()
    }

    fun getScanReference(): String {
        return session.usuario.scanReference
    }

    fun getIdPais(): Int {
        return session.usuario.idPais
    }

    fun getIdUsuario(): Long {
        return session.usuario.ideUsuario
    }

    fun getLocation(): McLocationData {
        return session.minimalLocationData
    }

    fun checkWhiteList(
            idApp: Int,
            idPais: Int,
            idioma: String,
            idUsuario: Long,
            callback: InteractorCallback<TokenBaseResponse>
    ) {
        disposables.add(
                tokenApi.isOnWhiteList(idApp, idPais, idioma, idUsuario)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    callback.onSuccess(it)
                                }, {
                            callback.onError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        }
                        )
        )
    }

    fun getToken(
            idApp: Int, idPais: Int, idioma: String, auth: String, profile: String,
            callback: InteractorCallback<TokenEntity>
    ) {
        disposables.add(
                tokenApi.getToken(auth, profile, idApp, idPais, idioma).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ e ->
                    if (e.code == 0) callback.onSuccess(e) else callback.onError(e.message)
                }, { t ->
                    callback.onError(StringUtil.getNetworkError())
                })
        )
    }

    fun pagoBP(
            idApp: Int, idioma: String, idPais: Int, accountId: String, auth: String,
            request: PaymentEntity, callback: InteractorCallback<ReceiptEntity>
    ) {
        disposables.add(
                h2hApi.enqueuePayment(auth, idApp, idPais, idioma, accountId, request).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    callback.onSuccess(r)
                }, { t -> callback.onError(StringUtil.getNetworkError()) })
        )
    }
}