package addcel.mobilecard.domain.h2h

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.hth.H2HApi
import addcel.mobilecard.data.net.token.TokenizerAPI
import com.squareup.otto.Bus
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 2019-06-03.
 */
class KeyboardH2HPaymentInteractor(
        h2hApi: H2HApi, tokenApi: TokenizerAPI,
        session: SessionOperations, disposables: CompositeDisposable, val bus: Bus
) :
        H2HPaymentInteractor(h2hApi, tokenApi, session, disposables)