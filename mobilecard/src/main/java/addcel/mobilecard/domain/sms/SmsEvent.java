package addcel.mobilecard.domain.sms;

import com.google.common.base.MoreObjects;

/**
 * ADDCEL on 11/29/18.
 */
public final class SmsEvent {
    private final int useCase;
    private final String mensaje;

    private SmsEvent(Builder builder) {
        useCase = builder.useCase;
        mensaje = builder.mensaje;
    }

    public int getUseCase() {
        return useCase;
    }

    public String getMensaje() {
        return mensaje;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("useCase", useCase)
                .add("mensaje", mensaje)
                .toString();
    }

    public static final class Builder {
        private int useCase;
        private String mensaje;

        public Builder() {
        }

        public Builder(SmsEvent copy) {
            this.useCase = copy.getUseCase();
            this.mensaje = copy.getMensaje();
        }

        public Builder setUseCase(int useCase) {
            this.useCase = useCase;
            return this;
        }

        public Builder setMensaje(String mensaje) {
            this.mensaje = mensaje;
            return this;
        }

        public SmsEvent build() {
            return new SmsEvent(this);
        }
    }
}
