package addcel.mobilecard.domain.sms;

import android.annotation.SuppressLint;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.data.net.usuarios.model.SmsResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.ui.login.tipo.LoginTipoContract;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 11/29/18.
 */
public class SmsInteractorImpl implements SmsInteractor {

    private final UsuariosService api;
    private final SessionOperations session;
    private final int useCase;
    private final long id;
    private final int idPais;
    private final CompositeDisposable disposables;

    public SmsInteractorImpl(UsuariosService api, SessionOperations session, int useCase, long id,
                             int idPais) {
        this.api = api;
        this.session = session;
        this.useCase = useCase;
        this.id = id;
        this.idPais = idPais;
        this.disposables = new CompositeDisposable();
    }

    @Override
    public int getUseCase() {
        return useCase;
    }

    @Override
    public int getIdpais() {
        return idPais;
    }

    @SuppressLint("CheckResult")
    @Override
    public void validateCode(int idApp, String idioma, String code,
                             InteractorCallback<SmsEvent> callback) {
        LoginTipoContract.Tipo tipo = getTipo(useCase);
        disposables.add(api.validateSMS(idApp, idPais, idioma, tipo, id, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.getIdError() == 0) {
                        if (tipo.equals(LoginTipoContract.Tipo.NEGOCIO)) {
                            updateNegocio(response, callback);
                        } else {
                            updateUsuario(response, callback);
                        }
                    } else {
                        callback.onError(response.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError())));
    }

    @SuppressLint("CheckResult")
    @Override
    public void resendSms(int idApp, String idioma, InteractorCallback<DefaultResponse> callback) {
        disposables.add(api.resendSMS(idApp, idPais, idioma, getTipo(useCase), id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.getIdError() == 0) {
                        callback.onSuccess(response);
                    } else {
                        callback.onError(response.getMensajeError());
                    }
                }, throwable -> callback.onError(StringUtil.getNetworkError())));
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    private void updateUsuario(SmsResponse response, InteractorCallback<SmsEvent> callback) {
        if (useCase != SmsUseCase.U_REGISTRO) {
            Usuario mUsuario = session.getUsuario();
            mUsuario.setIdUsrStatus(response.getIdUsrStatus());
            session.setUsuario(mUsuario);
            session.setUsuarioLogged(Boolean.TRUE);
        }
        callback.onSuccess(
                new SmsEvent.Builder().setUseCase(useCase).setMensaje(response.getMensajeError()).build());
    }

    private void updateNegocio(SmsResponse response, InteractorCallback<SmsEvent> callback) {
        if (useCase != SmsUseCase.N_REGISTRO) {
            NegocioEntity.Builder b = new NegocioEntity.Builder(session.getNegocio());
            b.setIdUsrStatus(response.getIdUsrStatus());
            session.setNegocio(b.build());
            session.setNegocioLogged(Boolean.TRUE);
        }
        callback.onSuccess(
                new SmsEvent.Builder().setUseCase(useCase).setMensaje(response.getMensajeError()).build());
    }

    private LoginTipoContract.Tipo getTipo(int useCase) {
        switch (useCase) {
            case SmsUseCase.U_REGISTRO:
                return LoginTipoContract.Tipo.USUARIO;
            case SmsUseCase.U_SPLASH:
                return LoginTipoContract.Tipo.USUARIO;
            case SmsUseCase.U_LOGIN:
                return LoginTipoContract.Tipo.USUARIO;
            case SmsUseCase.U_MENU:
                return LoginTipoContract.Tipo.USUARIO;
            case SmsUseCase.N_REGISTRO:
                return LoginTipoContract.Tipo.NEGOCIO;
            case SmsUseCase.N_SPLASH:
                return LoginTipoContract.Tipo.NEGOCIO;
            case SmsUseCase.N_LOGIN:
                return LoginTipoContract.Tipo.NEGOCIO;
            case SmsUseCase.N_MENU:
                return LoginTipoContract.Tipo.NEGOCIO;
            default:
                return LoginTipoContract.Tipo.USUARIO;
        }
    }
}
