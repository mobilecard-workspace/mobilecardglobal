package addcel.mobilecard.domain.sms.registro

import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.net.registro.*
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.Retrofit2HttpErrorHandling
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-07-23.
 */
interface SmsRegistroInteractor {

    fun clearDisposables()

    fun fetchPhone(): String

    fun storePhone(phone: String)

    fun launchSms(
            idApp: Int, idPais: Int, idioma: String, phone: String, imei: String,
            callback: InteractorCallback<SmsActivationEntity>
    )

    fun verifySms(
            idApp: Int, idPais: Int, idioma: String, code: String, phone: String, imei: String,
            callback: InteractorCallback<SmsActivationEntity>
    )
}

class SmsRegistroInteractorImpl(
        val api: RegistroAPI, val state: StateSession, val useCase: Int,
        val disposables: CompositeDisposable
) : SmsRegistroInteractor {

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun fetchPhone(): String {
        return state.getCapturedPhone()
    }

    override fun storePhone(phone: String) {
        state.setCapturedPhone(phone)
    }

    override fun launchSms(
            idApp: Int, idPais: Int, idioma: String, phone: String, imei: String,
            callback: InteractorCallback<SmsActivationEntity>
    ) {

        val tipo = if (useCase == 1) TipoVerificacion.USUARIO else TipoVerificacion.COMERCIO

        val body = SmsActivationRequest(phone, imei, tipo)

        disposables.add(
                api.getSmsCode(idApp, idPais, idioma, body).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ result -> callback.onSuccess(result) },
                        { t ->
                            t.printStackTrace()
                            val errorRes = Retrofit2HttpErrorHandling.getBodyFromErrorBody(
                                    t,
                                    SmsActivationEntity::class.java
                            )
                            if (errorRes != null) callback.onError(errorRes.message)
                            else callback.onError(
                                    t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)
                            )
                        })
        )
    }

    override fun verifySms(
            idApp: Int, idPais: Int, idioma: String, code: String, phone: String,
            imei: String, callback: InteractorCallback<SmsActivationEntity>
    ) {

        val tipo = if (useCase == 1) TipoVerificacion.USUARIO else TipoVerificacion.COMERCIO

        disposables.add(
                api.validateSmsCode(
                        idApp, idPais, idioma,
                        SmsVerificationRequest(code, phone, imei, tipo)
                ).subscribeOn(Schedulers.io())
                        .observeOn(
                                AndroidSchedulers.mainThread()
                        ).subscribe({ callback.onSuccess(it) }, {
                            it.printStackTrace()
                            val errorRes =
                                    Retrofit2HttpErrorHandling.getBodyFromErrorBody(
                                            it,
                                            SmsActivationEntity::class.java
                                    )
                            if (errorRes != null) callback.onError(errorRes.message)
                            else callback.onError(
                                    ErrorUtil.getFormattedHttpErrorMsg(it)
                            )
                        })
        )
    }
}