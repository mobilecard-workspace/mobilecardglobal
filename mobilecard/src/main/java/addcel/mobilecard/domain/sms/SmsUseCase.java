package addcel.mobilecard.domain.sms;

/**
 * ADDCEL on 11/29/18.
 */
public final class SmsUseCase {
    public static final int U_REGISTRO = 1;
    public static final int N_REGISTRO = 2;
    public static final int U_LOGIN = 3;
    public static final int N_LOGIN = 4;
    public static final int U_SPLASH = 5;
    public static final int N_SPLASH = 6;
    public static final int U_MENU = 7;
    public static final int N_MENU = 8;

    private SmsUseCase() {
    }
}
