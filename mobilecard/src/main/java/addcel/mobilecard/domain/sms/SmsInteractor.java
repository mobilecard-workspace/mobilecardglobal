package addcel.mobilecard.domain.sms;

import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.domain.InteractorCallback;

/**
 * ADDCEL on 11/29/18.
 */
public interface SmsInteractor {

    int getUseCase();

    int getIdpais();

    void validateCode(int idApp, String idioma, String code, InteractorCallback<SmsEvent> callback);

    void resendSms(int idApp, String idioma, InteractorCallback<DefaultResponse> callback);

    void clearDisposables();
}
