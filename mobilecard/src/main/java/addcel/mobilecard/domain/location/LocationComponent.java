package addcel.mobilecard.domain.location;

import javax.inject.Singleton;

import addcel.mobilecard.di.module.AppModule;
import dagger.Component;

/**
 * ADDCEL on 24/01/18.
 */
@Singleton
@Component(modules = {AppModule.class, LocationModule.class})
public interface LocationComponent {
    void inject(LocationService service);
}
