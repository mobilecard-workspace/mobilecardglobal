package addcel.mobilecard.domain.location;

import android.location.Location;

/**
 * ADDCEL on 08/02/18.
 */
public final class McLocationData {
    private double lat;
    private double lon;
    private String currAddress;

    public McLocationData() {
        this(0, 0, "");
    }

    public McLocationData(double lat, double lon) {
        this(lat, lon, "");
    }

    public McLocationData(double lat, double lon, String currAddress) {
        this.lat = lat;
        this.lon = lon;
        this.currAddress = currAddress;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(long lon) {
        this.lon = lon;
    }

    public String getCurrAddress() {
        return currAddress;
    }

    public void setCurrAddress(String currAddress) {
        this.currAddress = currAddress;
    }

    public Location toAndroidLocation() {
        Location androidLocation = new Location("");
        androidLocation.setLatitude(lat);
        androidLocation.setLongitude(lon);
        return androidLocation;
    }
}
