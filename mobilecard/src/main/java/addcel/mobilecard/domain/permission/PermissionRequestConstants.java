package addcel.mobilecard.domain.permission;

/**
 * ADDCEL on 30/08/18.
 */
public final class PermissionRequestConstants {
    public static final int LOGIN = 1;
    public static final int MENU = 2;
    public static final int TAE = 3;
    public static final int PEAJE = 4;
    public static final int SERVICIOS = 5;
    public static final int ACI = 6;
    public static final int BLACKSTONE = 7;
    public static final int H2H = 8;
    public static final int LCPF = 9;
    public static final int INGO = 10;
    public static final int SCAN = 11;
    public static final int SECURE = 12;
    public static final int NEGOCIO = 13;
    public static final int NEGOCIO_ID = 14;
    public static final int NEGOCIO_DOM = 15;
    public static final int SHIFT = 16;
    public static final int PROFILE = 17;

    private PermissionRequestConstants() {
    }
}
