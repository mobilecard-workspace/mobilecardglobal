package addcel.mobilecard.domain.transacto

import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.transacto.TransactoService
import addcel.mobilecard.data.net.transacto.entity.ConsultaRequest
import addcel.mobilecard.data.net.transacto.entity.SaldoEntity
import addcel.mobilecard.data.net.transacto.entity.TipoCambioEntity
import addcel.mobilecard.data.net.usuarios.model.MobilecardContact
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import android.content.ContentResolver
import android.database.Cursor
import android.provider.ContactsContract
import android.text.TextUtils
import com.google.common.base.Strings
import com.google.common.collect.ImmutableSet
import com.google.common.collect.Lists
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.crypto.AddcelCrypto

/**
 * ADDCEL on 3/7/19.
 */
class TransactoInteractor(
        val api: TransactoService, val dao: FavoritoDaoRx,
        val session: SessionOperations, private val contentResolver: ContentResolver,
        val disposables: CompositeDisposable
) {

    private fun getContactCursor(): Cursor? {
        val selection: String =
                ContactsContract.Contacts.IN_VISIBLE_GROUP + " = 1" + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER + " = 1"
        val sortOrder: String = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC"

        return contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                selection, null, sortOrder
        )
    }

    fun clearDisposables() {
        disposables.clear()
    }

    fun getIdUsuario(): Long {
        return session.usuario.ideUsuario
    }

    fun getIdPais(): Int {
        return session.usuario.idPais
    }

    fun getLocation(): McLocationData {
        return session.minimalLocationData
    }

    fun fetchContacts(callback: InteractorCallback<List<MobilecardContact>>) {
        val cursor: Cursor? = getContactCursor()

        if (cursor != null) {
            disposables.add(contactsObservable(cursor).subscribeOn(Schedulers.io()).observeOn(
                    AndroidSchedulers.mainThread()
            ).map { ImmutableSet.copyOf(it).asList() }.subscribe(
                    { callback.onSuccess(it) }, {
                callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError()))
            })
            )
        } else {
            callback.onError(Strings.nullToEmpty("Contacts not available"))
        }
    }

    fun saveToFavs(favorito: Favorito, callback: InteractorCallback<Long>) {
        disposables.add(saveToFavSingle(favorito).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
        ).subscribe({
            callback.onSuccess(it)
        }, { callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError())) })
        )
    }

    fun getTipoCambio(idApp: Int, idioma: String, callback: InteractorCallback<Double>) {
        if (getIdPais() == 1) {
            callback.onSuccess(0.0)
        } else {
            disposables.add(api.call(idApp, idioma, TransactoService.DIVISA, "").subscribeOn(
                    Schedulers.io()
            ).observeOn(AndroidSchedulers.mainThread()).map { s ->
                JsonUtil.fromJson(
                        AddcelCrypto.decryptSensitive(s),
                        TipoCambioEntity::class.java
                )
            }.subscribe({ t -> callback.onSuccess(t.valorDolar) },
                    { t -> callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError())) })
            )
        }
    }

    fun consultaSaldo(
            idApp: Int, idioma: String, request: ConsultaRequest,
            callback: InteractorCallback<SaldoEntity>
    ) {
        disposables.add(api.call(
                idApp, idioma, TransactoService.SALDO,
                AddcelCrypto.encryptSensitive(JsonUtil.toJson(request))
        ).subscribeOn(
                Schedulers.io()
        ).observeOn(AndroidSchedulers.mainThread()).map { s ->
            JsonUtil.fromJson(AddcelCrypto.decryptSensitive(s), SaldoEntity::class.java)
        }.subscribe({ s ->
            if (s.idError == 0) callback.onSuccess(s) else callback.onError(s.mensajeError)
        }) { callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError())) })
    }

    private fun contactsObservable(cursor: Cursor): Observable<List<MobilecardContact>> {
        return Observable.fromCallable { fetchContacts(cursor) }
    }

    private fun fetchContacts(cursor: Cursor): List<MobilecardContact> {
        val contacts = Lists.newArrayList<MobilecardContact>()
        while (cursor.moveToNext()) {
            val contactName = cursor.getString(
                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
            )
            val phNumber = cursor.getString(
                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
            )
            if (!TextUtils.isEmpty(phNumber)) {
                contacts.add(MobilecardContact(contactName, phNumber))
            }
        }
        cursor.close()
        return contacts
    }

    private fun saveToFavSingle(favorito: Favorito): Single<Long> {
        return Single.fromCallable { dao.insert(favorito) }
    }
}