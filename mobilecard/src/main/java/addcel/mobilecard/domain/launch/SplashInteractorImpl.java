package addcel.mobilecard.domain.launch;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.base.Strings;

import java.util.Objects;

import addcel.mobilecard.McConstants;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.UserValidation;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 11/30/18.
 */
public class SplashInteractorImpl implements SplashInteractor {
    private final UsuariosService userApi;
    private final NegocioAPI negocioApi;
    private final SessionOperations sessionOperations;
    private final CompositeDisposable disposables;

    public SplashInteractorImpl(UsuariosService userApi, NegocioAPI negocioApi,
                                SessionOperations sessionOperations) {
        this.userApi = userApi;
        this.negocioApi = negocioApi;
        this.sessionOperations = sessionOperations;
        this.disposables = new CompositeDisposable();
    }

    @Nullable
    @Override
    public Usuario getUsuario() {
        return sessionOperations.getUsuario();
    }

    @Nullable
    @Override
    public NegocioEntity getNegocio() {
        return sessionOperations.getNegocio();
    }

    @SuppressLint("CheckResult")
    @Override
    public void verifyUser(int idApp, String idioma, boolean online,
                           InteractorCallback<UserValidation> callback) {
        if (online) {
            long idUsuario = Objects.requireNonNull(getUsuario()).getIdeUsuario();
            disposables.add(userApi.verifyUser(idApp, idioma, idUsuario)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(validation -> {
                        validation = new UserValidation.Builder(validation).setIdUSuario(
                                sessionOperations.getUsuario().getIdeUsuario()).build();
                        if (validation.getIdError() == 0) {
                            updateUser(validation, callback);
                        } else {
                            callback.onError(validation.getMensajeError());
                        }
                    }, throwable -> callback.onError(StringUtil.getNetworkError())));
        } else {
            evalUserOffline(Objects.requireNonNull(getUsuario()), callback);
        }
    }

    @SuppressLint("CheckResult")
    @Override
    public void verifyNegocio(int idApp, String idioma, boolean online,
                              InteractorCallback<ValidationEntity> callback) {
        if (online) {
            long id = Objects.requireNonNull(getNegocio()).getIdEstablecimiento();
            disposables.add(negocioApi.verify(idApp, idioma, id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(negocioEntity -> {
                        if (negocioEntity.getIdError() == 0) {
                            updateNegocio(negocioEntity, callback);
                        } else {
                            callback.onError(negocioEntity.getMensajeError());
                        }
                    }, throwable -> callback.onError(StringUtil.getNetworkError())));
        } else {
            evalNegocioOffline(Objects.requireNonNull(getNegocio()), callback);
        }
    }

    @Override
    public void checkIfIngoAvailable(InteractorCallback<Boolean> callback) {

    }

    @Override
    public void updateReference(int idApp, String idioma, int idPais, String email, String reference,
                                InteractorCallback<McResponse> callback) {
        disposables.add(userApi.updateReference(idApp, idPais, idioma, email, reference)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSuccess,
                        throwable -> callback.onError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.NETWORK))));
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @Override
    public void addToDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

    private void updateUser(UserValidation validation, InteractorCallback<UserValidation> callback) {
        switch (validation.getIdUsrStatus()) {
            case McConstants.USER_STATUS_ACTIVO:
            case McConstants.USER_STATUS_EMAIL_VERIFICATION:
            case McConstants.USER_STATUS_SMS_VERIFICATION:
                Usuario u = Objects.requireNonNull(getUsuario());
                u.setUsrNombre(validation.getUsrNombre());
                u.setUsrApellido(validation.getUsrApellido());
                u.setUsrTelefono(validation.getUsrTelefono());
                u.setIdUsrStatus(validation.getIdUsrStatus());
                u.setUsrJumio(validation.getUsrJumio());
                u.setScanReference(validation.getScanReference());
                u.setIdPais(validation.getIdPais());
                sessionOperations.setUsuario(u);
                sessionOperations.setUsuarioLogged(Boolean.TRUE);
                callback.onSuccess(validation);
                break;
            default:
                clearSession(validation.getMensajeError(), callback);
                break;
        }
    }

    private void updateNegocio(ValidationEntity negocioEntity,
                               InteractorCallback<ValidationEntity> callback) {
        switch (negocioEntity.getEstatus()) {
            case McConstants.USER_STATUS_ACTIVO:
            case McConstants.USER_STATUS_EMAIL_VERIFICATION:
            case McConstants.USER_STATUS_SMS_VERIFICATION:
                NegocioEntity.Builder b = new NegocioEntity.Builder(Objects.requireNonNull(getNegocio()));
                b.setIdUsrStatus(negocioEntity.getEstatus());
                b.setJumioStatus(negocioEntity.getJumioStatus());
                b.setScanReference(negocioEntity.getJumioReference());
                b.setIdPais(negocioEntity.getIdPais());
                sessionOperations.setNegocio(b.build());
                sessionOperations.setNegocioLogged(Boolean.TRUE);
                callback.onSuccess(negocioEntity);
                break;
            default:
                clearSession(negocioEntity.getMensajeError(), callback);
                break;
        }
    }

    private void evalUserOffline(@NonNull Usuario usuario,
                                 InteractorCallback<UserValidation> callback) {
        switch (usuario.getIdUsrStatus()) {
            case McConstants.USER_STATUS_ACTIVO:
            case McConstants.USER_STATUS_EMAIL_VERIFICATION:
            case McConstants.USER_STATUS_SMS_VERIFICATION:

                UserValidation validation = new UserValidation.Builder().setIdError(0)
                        .setIdPais(usuario.getIdPais())
                        .setIdUsrStatus(usuario.getIdUsrStatus())
                        .setUsrJumio(usuario.getUsrJumio())
                        .setScanReference(usuario.getScanReference())
                        .setIdUSuario(usuario.getIdeUsuario())
                        .setMensajeError(Strings.nullToEmpty(usuario.getMensajeError()))
                        .setUsrApellido(usuario.getUsrApellido())
                        .setUsrNombre(usuario.getUsrNombre())
                        .setUsrTelefono(usuario.getUsrTelefono())
                        .build();
                callback.onSuccess(validation);
                break;
            default:
                clearSession(usuario.getMensajeError(), callback);
                break;
        }
    }

    private void evalNegocioOffline(@NonNull NegocioEntity negocio,
                                    InteractorCallback<ValidationEntity> callback) {
        switch (negocio.getIdUsrStatus()) {
            case McConstants.USER_STATUS_ACTIVO:
            case McConstants.USER_STATUS_EMAIL_VERIFICATION:
            case McConstants.USER_STATUS_SMS_VERIFICATION:
                ValidationEntity entity = new ValidationEntity.Builder().setIdError(0)
                        .setId(negocio.getIdEstablecimiento())
                        .setEmailContacto(negocio.getEmail())
                        .setTelefonoContacto(negocio.getTelefono())
                        .setEstatus(negocio.getIdUsrStatus())
                        .setIdPais(negocio.getIdPais())
                        .setJumioReference(negocio.getScanReference())
                        .setJumioStatus(negocio.getJumioStatus())
                        .build();
                callback.onSuccess(entity);
                break;
            default:
                clearSession(negocio.getMensajeError(), callback);
                break;
        }
    }

    private <T> void clearSession(@Nullable String msg, InteractorCallback<T> callback) {
        sessionOperations.setUsuario(null);
        sessionOperations.setUsuarioLogged(Boolean.FALSE);
        sessionOperations.setNegocio(null);
        sessionOperations.setNegocioLogged(Boolean.FALSE);
        callback.onError(Strings.nullToEmpty(msg));
    }
}
