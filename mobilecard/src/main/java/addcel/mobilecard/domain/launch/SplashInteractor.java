package addcel.mobilecard.domain.launch;

import androidx.annotation.Nullable;

import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.UserValidation;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import io.reactivex.disposables.Disposable;

/**
 * ADDCEL on 11/30/18.
 */
public interface SplashInteractor {
    @Nullable
    Usuario getUsuario();

    @Nullable
    NegocioEntity getNegocio();

    void verifyUser(int idApp, String idioma, boolean onLine,
                    InteractorCallback<UserValidation> callback);

    void verifyNegocio(int idApp, String idioma, boolean onLine,
                       InteractorCallback<ValidationEntity> callback);

    void checkIfIngoAvailable(InteractorCallback<Boolean> callback);

    void updateReference(int idApp, String idioma, int idPais, String email, String reference,
                         InteractorCallback<McResponse> callback);

    void clearDisposables();

    void addToDisposable(Disposable disposable);
}
