package addcel.mobilecard.domain.secure;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.FranquiciaEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import mx.mobilecard.crypto.AddcelCrypto;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import timber.log.Timber;

public class WebInteractorImpl implements WebInteractor {

    private final OkHttpClient client;
    private final CardEntity cardEntity;
    private final CompositeDisposable compositeDisposable;

    public WebInteractorImpl(OkHttpClient client, @Nullable CardEntity cardEntity) {
        this.client = client;
        this.cardEntity = cardEntity;
        this.compositeDisposable = new CompositeDisposable();
    }

    @SuppressLint("CheckResult")
    @Override
    public void processUrl(String url, byte[] data, Map<String, String> headers,
                           InteractorCallback<String> callback) {
        Request.Builder builder = new Request.Builder().url(url)
                .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), data));

        for (Map.Entry<String, String> entry : headers.entrySet()) {
            builder.addHeader(entry.getKey(), entry.getValue());
        }

        Single<Response> single = Single.fromCallable(() -> client.newCall(builder.build()).execute())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        Disposable disposable = single.subscribe(response -> {
            final String htmlString = Objects.requireNonNull(response.body()).string();
            callback.onSuccess(htmlString);
        }, throwable -> callback.onError(ErrorUtil.Companion.getFormattedHttpErrorMsg(throwable)));
        compositeDisposable.add(disposable);
    }

    @Override
    public void processUrlGET(String url, byte[] data, Map<String, String> headers,
                              InteractorCallback<String> callback) {
        Request.Builder builder = new Request.Builder().url(url).get();

        for (Map.Entry<String, String> entry : headers.entrySet()) {
            builder.addHeader(entry.getKey(), entry.getValue());
        }

        Single<Response> single = Single.fromCallable(() -> client.newCall(builder.build()).execute())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        Disposable disposable = single.subscribe(response -> {
            final String htmlString = Objects.requireNonNull(response.body()).string();
            callback.onSuccess(htmlString);
        }, throwable -> callback.onError(StringUtil.getNetworkError()));
        compositeDisposable.add(disposable);
    }

    @Override
    public String getFormData() {
        if (cardEntity != null) {
            final String pan = Strings.nullToEmpty(AddcelCrypto.decryptHard(cardEntity.getPan()));
            final String type = getFranquiciaName(Objects.requireNonNull(cardEntity.getTipo()));
            final String ct = Strings.nullToEmpty(AddcelCrypto.decryptHard(cardEntity.getCodigo()));
            final String[] arr = CardEntity.Companion.getVigenciaAsArray(
                    AddcelCrypto.decryptHard(cardEntity.getVigencia()));
            Timber.d("Vigencia arr: %s", Arrays.toString(arr));
            final String mes = arr[0];
            final int indexAnio = (2000 + Integer.parseInt(arr[1]));

            return "document.getElementsByName('cc_name')[0].value='"
                    + cardEntity.getNombre()
                    + "';"
                    + "document.getElementsByName('cc_number')[0].value='"
                    + pan
                    + "';"
                    + "document.getElementsByName('cc_type')[0].value='"
                    + type
                    + "';"
                    + "document.getElementsByName('cc_cvv2')[0].value='"
                    + ct
                    + "';"
                    + "document.getElementsByName('_cc_expmonth')[0].value='"
                    + Strings.nullToEmpty(mes)
                    + "';"
                    + "document.getElementsByName('_cc_expyear')[0].value="
                    + indexAnio
                    + ";"

                    // SETTEAMOS A READONLY LOS CAMPOS DEL FORM DE PROCOM DESPUES DE LLENARLOS
                    + "document.getElementsByName('cc_name')[0].readOnly=true;"
                    + "document.getElementsByName('cc_number')[0].readOnly=true;"
                    + "document.getElementsByName('cc_type')[0].readOnly=true;"
                    // EL CAMPO DE CVV SOLO SE SETTEA COMO READONLY SI la tarjeta TIENE ASIGNADO EL CVV
                    + (!Strings.isNullOrEmpty(ct) ? "document.getElementsByName('cc_cvv2')[0].readOnly=true;"
                    : "")
                    + "document.getElementsByName('_cc_expmonth')[0].readOnly=true;"
                    + "document.getElementsByName('_cc_expyear')[0].readOnly=true;";
        } else {
            return "";
        }
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    private String getFranquiciaName(@NonNull FranquiciaEntity f) {
        Preconditions.checkNotNull(f);
        switch (f) {
            case MasterCard:
                return CONST_MASTER;
            case Carnet:
                return CONST_CARNET;
            default:
                return CONST_VISA;
        }
    }
}
