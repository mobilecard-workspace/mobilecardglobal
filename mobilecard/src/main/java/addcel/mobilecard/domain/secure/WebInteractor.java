package addcel.mobilecard.domain.secure;

import java.util.Map;

import addcel.mobilecard.domain.InteractorCallback;
import io.reactivex.disposables.CompositeDisposable;

public interface WebInteractor {

    String CONST_VISA = "Visa";
    String CONST_MASTER = "Mastercard";
    String CONST_CARNET = "Carnet";

    void processUrl(String url, byte[] data, Map<String, String> headers,
                    InteractorCallback<String> callback);

    void processUrlGET(String url, byte[] data, Map<String, String> headers,
                       InteractorCallback<String> callback);

    String getFormData();

    CompositeDisposable getCompositeDisposable();
}
