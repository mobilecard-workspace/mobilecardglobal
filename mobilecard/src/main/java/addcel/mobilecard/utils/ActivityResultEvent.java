package addcel.mobilecard.utils;

import android.content.Intent;

/**
 * ADDCEL on 20/12/16.
 */
public final class ActivityResultEvent {
    private final int requestCode;
    private final int resultCode;
    private final Intent data;

    public ActivityResultEvent(int requestCode, int resultCode, Intent data) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public int getResultCode() {
        return resultCode;
    }

    public Intent getData() {
        return data;
    }
}
