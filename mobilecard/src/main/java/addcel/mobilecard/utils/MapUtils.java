package addcel.mobilecard.utils;

import androidx.annotation.NonNull;

import com.google.common.base.Charsets;

import java.util.Map;

import timber.log.Timber;

/**
 * ADDCEL on 25/07/18.
 */
public final class MapUtils {
    private MapUtils() {
    }

    /**
     * @param map          - Mapa donde se ejecutar&aacute; el lookup con <b>key</b>
     * @param key          - Clave de b&uacute;squeda
     * @param defaultValue - Valor a retornar en caso de que la clave no se encuentre
     * @param <K>          Tipo de la clave
     * @param <V>          Tipo del valor
     * @return Valor <code>V</code> resultado del lookup o el default
     */
    public static synchronized <K, V> V getOrDefault(@NonNull Map<K, V> map, @NonNull K key,
                                                     @NonNull V defaultValue) {
        V v;
        return (((v = map.get(key)) != null) || map.containsKey(key)) ? v : defaultValue;
    }

    public static synchronized <K, V> byte[] mapToPostRequestArray(@NonNull Map<K, V> params) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<K, V> entry : params.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        // Timber.d("Con ultimo &: %s", sb.toString());

        sb.deleteCharAt(sb.length() - 1);

        Timber.d("Sin ultimo &: %s", sb.toString());

        return sb.toString().getBytes(Charsets.UTF_8);
    }
}
