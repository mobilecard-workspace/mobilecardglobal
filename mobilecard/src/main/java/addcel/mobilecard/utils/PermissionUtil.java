package addcel.mobilecard.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import timber.log.Timber;

/**
 * ADDCEL on 27/03/18.
 */
public final class PermissionUtil {

    private PermissionUtil() {
    }

    public static void checkPermission(Activity activity, String[] permissions, int requestCode,
                                       Callback callback) {
        Timber.d("Entro a checkPermission");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Timber.d("Menor a M, ejecutamos accion");
            callback.doOnPermissionGranted();
        } else {
            if (checkPermission(activity, permissions)) {
                Timber.d("Mayor o igual a M, tenemos permiso y ejecutamos accion");
                callback.doOnPermissionGranted();
            } else {
                Timber.d("Mayor o igual a M, no hay permiso, los solicitamos");
                ActivityCompat.requestPermissions(activity, permissions, requestCode);
            }
        }
    }

    public static void onRequestPermissionsResult(@NonNull int[] grantResults, Callback callback) {
        boolean granted = Boolean.TRUE;
        if (grantResults.length > 0) {
            for (int grant : grantResults) {
                if (grant == PackageManager.PERMISSION_DENIED) {
                    granted = Boolean.FALSE;
                    break;
                }
            }
            if (granted) {
                callback.doOnPermissionGranted();
            } else {
                callback.doOnPermissionDenied();
            }
        }
    }

    private static boolean checkPermission(Activity activity, String[] permissions) {
        boolean granted = Boolean.TRUE;
        for (String perm : permissions) {
            if (ContextCompat.checkSelfPermission(activity, perm) == PackageManager.PERMISSION_DENIED) {
                granted = Boolean.FALSE;
                break;
            }
        }
        return granted;
    }

    public interface Callback {
        void doOnPermissionGranted();

        void doOnPermissionDenied();
    }
}
