package addcel.mobilecard.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

/**
 * ADDCEL on 1/21/19.
 */
public final class JsonUtil {
    private static final Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();

    private JsonUtil() {
    }

    public static synchronized String toJson(Object o) {
        return gson.toJson(o);
    }

    public static synchronized <T> T fromJson(String json, Class<T> t) {
        return gson.fromJson(json, t);
    }

    public static synchronized <T> T fromJson(String json, Type type) {
        return gson.fromJson(json, type);
    }

    public static synchronized <T> T fromJson(JsonReader reader, Class<T> t) {
        return gson.fromJson(reader, t);
    }

    public static synchronized boolean isJson(String s) {
        try {
            new JSONObject(s);
        } catch (JSONException e) {
            try {
                new JSONArray(s);
            } catch (JSONException e1) {
                return false;
            }
        }
        return true;
    }
}
