package addcel.mobilecard.utils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/**
 * ADDCEL on 07/09/18.
 */
public final class FragmentUtil {
    private FragmentUtil() {
    }

    public static void addFragment(@NonNull FragmentManager fragmentManager, int container,
                                   Fragment fragment) {
        fragmentManager.beginTransaction()
                .add(container, fragment)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();
    }

    public static void addFragmentHidingCurrent(@NonNull FragmentManager fragmentManager,
                                                int container, Fragment currentFragment, Fragment newFragment) {
        fragmentManager.beginTransaction()
                .add(container, newFragment)
                .addToBackStack(null)
                .hide(currentFragment)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    public static void replaceFragment(@NonNull FragmentManager fragmentManager, int container,
                                       Fragment newFragment) {
        fragmentManager.beginTransaction()
                .replace(container, newFragment)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    public static void addFragmentRemovingCurrent(@NonNull FragmentManager fragmentManager,
                                                  int container, Fragment currentFragment, Fragment newFragment) {
        fragmentManager.beginTransaction()
                .add(container, newFragment)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .remove(currentFragment)
                .commit();
    }
}
