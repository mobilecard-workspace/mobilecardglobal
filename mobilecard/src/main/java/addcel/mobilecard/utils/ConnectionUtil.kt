package addcel.mobilecard.utils

import io.reactivex.Observable
import timber.log.Timber
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

/**
 * ADDCEL on 4/1/19.
 */
class ConnectionUtil {
    companion object {

        private fun checkInternetConnection(): Boolean {
            return try {
                val timeoutMs = 1500
                val sock = Socket()
                val sockaddr = InetSocketAddress("8.8.8.8", 53)

                sock.connect(sockaddr, timeoutMs)
                sock.close()

                true
            } catch (e: IOException) {
                Timber.e(e)
                false
            }
        }

        fun checkInternetConnectionSingle(): Observable<Boolean> {
            return Observable.fromCallable { checkInternetConnection() }
        }
    }
}