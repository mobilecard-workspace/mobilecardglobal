package addcel.mobilecard.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.io.ByteArrayOutputStream;
import java.util.Locale;
import java.util.regex.Pattern;

import addcel.mobilecard.McRegexPatterns;
import okio.ByteString;
import timber.log.Timber;

/**
 * ADDCEL on 26/07/18.
 */
public final class StringUtil {

    private static final String[] vocales = {
            "a", "e", "i", "o", "u",
    };
    private static final String[] letras = {
            "Á", "á", "É", "é", "Í", "í", "Ó", "ó", "Ú", "ú", "Ñ", "ñ"
    };
    private static final String[] reemplazo = {
            "A", "a", "E", "e", "I", "i", "O", "o", "U", "u", "N", "n"
    };
    private static final String ERROR_ES = "Ocurrió un error";
    private static final String ERROR_EN = "An error occured";
    private static final String SUCCESS_ES = "Operación completada con éxito";
    private static final String SUCCESS_EN = "Operation completed";

    private static final String ERROR_NET_ES =
            "No logramos procesar tu petición. Verifica tu conexión a Internet";
    private static final String ERROR_NET_EN =
            "Your request cannot be processed at this time. Please check your Internet connection";

    private StringUtil() {
    }

    public static String removeAcentos(String str) {
        return TextUtils.replace(str, letras, reemplazo).toString();
    }

    public static String removeAcentosMultiple(String str) {
        String[] words = TextUtils.split(str, "\\s+");
        StringBuilder sb = new StringBuilder();
        for (String s : words) {
            sb.append(removeAcentos(s)).append(" ");
        }
        return sb.toString().trim();
    }

    public static String findBetween(String src, String start, String end) {
        return src.substring(src.indexOf(start) + 1, src.indexOf(end));
    }

    public static double amountStringToDouble(String amStr) {
        String sanAmStr = Strings.nullToEmpty(amStr);
        if (Strings.isNullOrEmpty(sanAmStr)) return 0;
        return Double.parseDouble(sanAmStr);
    }

    public static String sha256(@NonNull String input) {
        Preconditions.checkNotNull(input, "Arg a hashear no puede ser NULL");
        return ByteString.encodeUtf8(input).sha256().hex();
    }

    public static String sha512(@NonNull String input) {
        Preconditions.checkNotNull(input, "Arg a hashear no puede ser NULL");
        return ByteString.encodeUtf8(input).sha512().base64();
    }

    public static boolean isStringNumberLargerThanValue(@Nullable String amountAsString,
                                                        double compareValue) {
        double amount =
                Strings.isNullOrEmpty(amountAsString) ? 0.0 : Double.parseDouble(amountAsString);
        return amount > compareValue;
    }

    public static boolean isDecimalAmount(@NonNull String amount) {
        return Pattern.matches(McRegexPatterns.DECIMAL_AMOUNT, amount);
    }

    public static boolean isDecimalAmount(@NonNull CharSequence amount) {
        return Pattern.matches(McRegexPatterns.DECIMAL_AMOUNT, amount);
    }

    /**
     * Tarjeta que sustituye los digitos correspondientes a los indices 4 - 11 con un caracter
     * cualquiera. Si un caracter en cuestion se encuentra antes de un indice multiplo de 4 agrega un
     * espacio a la representacion
     *
     * @param card        - El numero de tarjeta a enmascarar
     * @param placeholder - Caracter a utilizar como sustitucion
     * @return String representando la tarjeta enmascarada
     */
    public static String maskCard(String card, String placeholder) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(card) && TextUtils.isDigitsOnly(card));
        StringBuilder result = new StringBuilder();
        Timber.d("Longitud tarjeta: %d", card.length());
        for (int i = 0; i < card.length(); i++) {
            if (i >= 4 && i <= 11) {
                if ((i + 1) % 4 == 0) {
                    result.append(placeholder).append(" ");
                } else {
                    result.append(placeholder);
                }
            } else {
                if ((i + 1) % 4 == 0) {
                    result.append(card.charAt(i)).append(" ");
                } else {
                    result.append(card.charAt(i));
                }
            }
        }
        return result.toString();
    }

    public static String maskCard(String card) {
        return maskCard(card, "·");
    }

    public static String removeLast(@NonNull String str) {
        Preconditions.checkNotNull(str);
        return str.substring(0, str.length() - 1);
    }

    public static String getCurrentLanguage() {
        //String lang = Locale.getDefault().getLanguage().toLowerCase();
        //return Strings.isNullOrEmpty(lang) ? "en" : lang;
        return "es";
    }

    public static String byteArrayToBase64(@NonNull Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); // bm is the bitmap object
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
        // ByteString.of(b).base64();
    }

    public static Bitmap base64ToByteArray(@NonNull String str) {
        byte[] decodedString = Base64.decode(str, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static String generarRFC(String input) {

        StringBuilder resultado = new StringBuilder();
        String[] nombreSplit = input.split(" ");
        String nombre = nombreSplit[0];
        String apellidoPaterno = nombreSplit[1];
        String apellidoMaterno = nombreSplit[2];
        boolean vocalEncontrada = false;

        // Sacamos la primera letra del apellido paterno
        resultado.append(apellidoPaterno.charAt(0));
        // Sacamos la primera vocal del apellido paterno
        for (int i = 0; i < apellidoPaterno.length() && !vocalEncontrada; i++) {
            String letra = String.valueOf(apellidoPaterno.charAt(i));
            for (String vocal : vocales) {
                if (letra.compareTo(vocal) == 0) {
                    resultado.append(letra);
                    vocalEncontrada = true;
                    break;
                }
            }
        }

        // Sacamos la primera letra del apellido materno
        resultado.append(apellidoMaterno.charAt(0));
        resultado.append(nombre.charAt(0));

        return resultado.toString().toUpperCase(Locale.getDefault());
    }

    public static boolean isMobileCardPlastic(CharSequence pan) {
        return Pattern.matches(McRegexPatterns.PREVIVALE, pan);
    }

    public static boolean isMobileCardPlasticREGEX(CharSequence pan) {
        CharSequence sPan = Strings.nullToEmpty(pan.toString());
        return Pattern.matches(McRegexPatterns.PREVIVALE, sPan)
                && TextUtils.indexOf(sPan, "506450") == 0;
    }

    public static String getUltimos4DigitosTDC(@NonNull String tarjeta) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(tarjeta) && tarjeta.length() >= 4);
        final int start = tarjeta.length() - 4; // tomando en cuenta index 0
        final int end = tarjeta.length();
        return TextUtils.substring(tarjeta, start, end);
    }

    public static String trimPhone(String phone) {
        phone = phone.replace("-", "");
        if (phone.length() > 10) phone = phone.substring(phone.length() - 10);
        return phone;
    }

    public static String trimPhone(String phone, int desiredLen) {
        phone = phone.replace("-", "");
        if (phone.length() > desiredLen) phone = phone.substring(phone.length() - desiredLen);
        return phone;
    }

    public static String trimLoginIfEmail(String login) {
        if (Patterns.EMAIL_ADDRESS.matcher(login).matches()) return TextUtils.split(login, "@")[0];
        return login;
    }

    public static String textOrDefault(@NonNull String text, @NonNull String def) {
        return Strings.isNullOrEmpty(text) ? def : text;
    }

    public static String errorOrDefault(@NonNull String error, String idioma) {
        return idioma.equals("es") ? textOrDefault(error, ERROR_ES) : textOrDefault(error, ERROR_EN);
    }

    public static String getNetworkError() {
        return getCurrentLanguage().equals("es") ? ERROR_NET_ES : ERROR_NET_EN;
    }

    public static String successOrDefault(@NonNull String error, String idioma) {
        return idioma.equals("es") ? textOrDefault(error, SUCCESS_ES)
                : textOrDefault(error, SUCCESS_EN);
    }

    public static String fromSequence(@Nullable CharSequence sequence) {
        if (sequence == null) return "";
        return sequence.toString().trim();
    }

    public static String digest(String... data) {
        StringBuilder digest = new StringBuilder();
        for (String s : data) {
            digest.append(s);
        }
        return sha512(digest.toString());
    }

    public static String splitCard(String card, String divider) {
        StringBuilder split = new StringBuilder();

        for (int i = 0; i < card.length(); i++) {
            if (i > 0 && i % 4 == 0) split.append(divider);
            split.append(card.charAt(i));
        }
        return split.toString();
    }

    public static String parsePlaca(String placa, String divider) {

        if (placa.contains("-")) return placa;

        StringBuilder split = new StringBuilder();

        for (int i = 0; i < placa.length(); i++) {
            if (i > 0 && i % 3 == 0) split.append(divider);
            split.append(placa.charAt(i));
        }
        return split.toString();
    }
}
