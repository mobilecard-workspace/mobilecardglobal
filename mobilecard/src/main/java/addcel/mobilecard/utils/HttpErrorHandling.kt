package addcel.mobilecard.utils

import retrofit2.HttpException

/**
 * ADDCEL on 2019-07-25.
 */
class Retrofit2HttpErrorHandling {

    companion object {
        fun <T> getBodyFromErrorBody(throwable: Throwable, cls: Class<T>): T? {
            return if (throwable is HttpException) {
                val errorBody = throwable.response()?.errorBody()
                if (errorBody != null) {
                    JsonUtil.fromJson(errorBody.string(), cls)
                } else {
                    null
                }
            } else {
                null
            }
        }
    }
}