package addcel.mobilecard.utils

import addcel.mobilecard.McRegexPatterns
import addcel.mobilecard.R
import commons.validator.routines.CreditCardValidator
import java.util.regex.Pattern

/**
 * ADDCEL on 2019-10-10.
 */
class CardLoadingUtils {

    companion object {
        private const val VISA_FULL = R.drawable.full_visa
        private const val MASTER_FULL = R.drawable.full_mastercard
        private const val AMEX_FULL = R.drawable.full_amex
        private const val CARNET_FULL = R.drawable.full_mobilecard_h

        private const val VISA_SHORT = R.drawable.bg_visa
        private const val MASTER_SHORT = R.drawable.bg_mastercard
        private const val AMEX_SHORT = R.drawable.bg_amex
        private const val CARNET_SHORT = R.drawable.bg_mobilecard

        const val SHORT = 1
        const val FULL = 2

        fun getCardImg(pan: String, shortOrLongCard: Int): Int {

            if (Pattern.matches(
                            McRegexPatterns.PREVIVALE,
                            pan
                    )
            ) if (shortOrLongCard == SHORT) return CARNET_SHORT else CARNET_FULL

            if (CreditCardValidator.VISA_VALIDATOR.isValid(
                            pan
                    )
            ) if (shortOrLongCard == SHORT) return VISA_SHORT else VISA_FULL

            if (CreditCardValidator.MASTERCARD_VALIDATOR.isValid(
                            pan
                    )
            ) if (shortOrLongCard == SHORT) return MASTER_SHORT else MASTER_FULL

            if (CreditCardValidator.AMEX_VALIDATOR.isValid(
                            pan
                    )
            ) if (shortOrLongCard == SHORT) return AMEX_SHORT else AMEX_FULL

            return CARNET_SHORT
        }
    }
}