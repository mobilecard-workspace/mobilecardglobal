package addcel.mobilecard.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;

/**
 * ADDCEL on 3/29/19.
 */
public final class ListUtil {
    private ListUtil() {
    }

    public static synchronized <T> boolean isNullOrEmpty(@Nullable List<T> list) {
        return list == null || list.size() == 0;
    }

    public static synchronized <T> boolean notEmpty(@Nullable List<T> list) {
        return !isNullOrEmpty(list);
    }

    public static synchronized <T> List<T> nullToEmpty(@Nullable List<T> list) {
        return list == null ? Collections.emptyList() : list;
    }

    public static synchronized <T> T getElementAt(@NonNull List<T> list, int index) {
        if (isNullOrEmpty(list)) throw new IllegalArgumentException("List cannot be NULL or empty");
        return list.get(index);
    }

    public static synchronized <T> T getLastElement(@NonNull List<T> list) {
        return getElementAt(list, list.size() - 1);
    }

    public static synchronized <T> T getFirstElement(@NonNull List<T> list) {
        return getElementAt(list, 0);
    }
}
