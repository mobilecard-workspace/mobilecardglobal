package addcel.mobilecard.utils

/**
 * ADDCEL on 2020-01-21.
 */
class KListUtil {

    companion object {
        @Synchronized
        fun <T> isNullOrEmpty(list: List<T>?): Boolean {
            return list.isNullOrEmpty()
        }

        @Synchronized
        fun <T> notEmpty(list: List<T>?): Boolean {
            return !isNullOrEmpty(list)
        }

        @Synchronized
        fun <T> nullToEmpty(list: List<T>?): List<T>? {
            return list.orEmpty()
        }

        @Synchronized
        fun <T> getElementAt(list: List<T>, index: Int): T {
            require(!isNullOrEmpty(list)) { "List cannot be NULL or empty" }
            return list[index]
        }

        @Synchronized
        fun <T> getLastElement(list: List<T>): T {
            return list.last()
        }

        @Synchronized
        fun <T> getFirstElement(list: List<T>): T {
            return list.first()
        }
    }
}