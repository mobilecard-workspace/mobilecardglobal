package addcel.mobilecard.utils;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

public final class UiEventBus extends Bus {

    private static Bus instance;
    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public static Bus getInstance() {
        if (instance == null) instance = new UiEventBus();
        return instance;
    }

    @Override
    public void post(Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            mainThread.post(() -> post(event));
        }
    }
}
