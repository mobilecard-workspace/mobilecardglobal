package addcel.mobilecard.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * Esta clase agrupa métodos que reciben como parametros Objetos pertenecientes a al ecosistema
 * Android Eg: Context, Activity, Fragment, View, etc.
 */
public final class AndroidUtils {
    private AndroidUtils() {
    }

    public static synchronized boolean isLocationAvailable(@NonNull Context context)
            throws NullPointerException {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (manager == null) {
            throw new NullPointerException("Device does not support location");
        } else {
            boolean gpsEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean networkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            return gpsEnabled || networkEnabled;
        }
    }

    public static synchronized boolean isGooglePlayServicesAvailable(@NonNull Activity activity,
                                                                     @NonNull GoogleApiAvailability instance) {
        int status = instance.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (instance.isUserResolvableError(status)) {
                instance.getErrorDialog(activity, status, 2404).show();
            }
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public static String getText(@Nullable TextView view) {
        return Strings.nullToEmpty(Preconditions.checkNotNull(view).getText().toString()).trim();
    }

    public static void setText(@Nullable TextView view, int resId) {
        Preconditions.checkNotNull(view).setText(resId);
    }

    public static void setText(@Nullable TextView view, @NonNull CharSequence text) {
        Preconditions.checkNotNull(view).setText(text);
    }


    public static void setTextEmpty(@Nullable TextView view) {
        setText(view, "");
    }

    public static EditText getEditText(@NonNull TextInputLayout til) {
        return Preconditions.checkNotNull(til.getEditText());
    }

    public static <T> T getSelectedItem(@NonNull Spinner spinner, Class<T> cls) {
        return cls.cast(Preconditions.checkNotNull(spinner.getSelectedItem()));
    }

    public static <T extends View> T findViewById(@Nullable View view, @IdRes int id) {
        return Preconditions.checkNotNull(view).findViewById(id);
    }

    public static boolean validatePasswordWindow(@NonNull TextInputLayout passTil,
                                                 @NonNull TextInputLayout passConfTil, int errorPass, int errorConf) {
        final String pass = AndroidUtils.getText(passTil.getEditText());
        final String passConf = AndroidUtils.getText(passConfTil.getEditText());
        if (Strings.isNullOrEmpty(pass)) {
            passTil.setError(passTil.getResources().getText(errorPass));
            return false;
        } else if (TextUtils.getTrimmedLength(pass) < 8) {
            passTil.setError(passTil.getResources().getText(errorPass));
            return false;
        } else if (!TextUtils.equals(pass, passConf)) {
            passConfTil.setError(passConfTil.getResources().getText(errorConf));
            return false;
        } else {
            return true;
        }
    }

    public static void clearForm(TextView... views) {
        for (TextView v : views) {
            v.setText("");
        }
    }

    public static boolean enableButton(TextInputLayout... tils) {
        boolean enabled = true;
        for (TextInputLayout t : tils) {
            if (t.getError() != null && !TextUtils.isEmpty(AndroidUtils.getText(t.getEditText()))) {
                enabled = false;
                break;
            }
        }
        return enabled;
    }

    public static void setPic(ImageView image, String path) {
        int targetW = image.getWidth();
        int targetH = image.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        image.setImageBitmap(bitmap);
    }

    public static boolean appInstalledOrNot(Context context, String uri) {
        PackageManager packageManager = context.getPackageManager();
        boolean appInstalled;
        try {
            packageManager.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            appInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            appInstalled = false;
        }

        return appInstalled;
    }

    public static void showInformationDialog(@NonNull Context context, @StringRes int title, @StringRes int msg, @StringRes int okText) {
        final AlertDialog dialog = createInformationDialog(context, title, msg, okText);
        dialog.show();
    }

    public static AlertDialog createInformationDialog(@NonNull Context context, @StringRes int title, @StringRes int msg, @StringRes int okText) {
        return new AlertDialog.Builder(context).setTitle(title).setMessage(msg).setPositiveButton(okText, (dialog, which) -> {
            if (dialog != null) dialog.dismiss();
        }).create();
    }

    /*


        private fun appInstalledOrNot(uri: String): Boolean {
        val pm = packageManager
        return try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }
     */
}
