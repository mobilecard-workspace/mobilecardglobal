package addcel.mobilecard.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import androidx.annotation.NonNull;

/**
 * ADDCEL on 30/07/18.
 */
final class ScreenUtil {
    private ScreenUtil() {
    }

    private static synchronized DisplayMetrics getMetrics(@NonNull WindowManager windowManager) {
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    public static synchronized DisplayMetrics getMetrics(@NonNull Context context)
            throws NullPointerException {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (manager == null) throw new NullPointerException();
        return getMetrics(manager);
    }
}
