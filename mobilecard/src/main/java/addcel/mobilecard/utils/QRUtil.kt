package addcel.mobilecard.utils

import android.graphics.Bitmap
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.glxn.qrgen.android.QRCode

/**
 * ADDCEL on 2019-05-14.
 */
class QRUtil {

    companion object {

        fun generateQRCodeSingle(s: String, w: Int, h: Int, fg: Int, bg: Int): Single<Bitmap> {
            return Single.fromCallable {
                QRCode.from(s).withCharset(Charsets.UTF_8.displayName()).withColor(fg, bg)
                        .withSize(w, h)
                        .bitmap()
            }.subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
        }
    }
}