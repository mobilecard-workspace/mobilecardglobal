package addcel.mobilecard.utils

/**
 * ADDCEL on 2019-08-23.
 */

data class ConnectionEntity(
        val connected: Boolean = false, val isAvailable: Boolean = false,
        val isConnected: Boolean = false, val typeName: String = ""
)

