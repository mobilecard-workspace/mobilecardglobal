package addcel.mobilecard.utils;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import addcel.mobilecard.McRegexPatterns;
import commons.validator.routines.CreditCardValidator;
import timber.log.Timber;

/**
 * ADDCEL on 1/22/19.
 */
public final class CreditCardFormatUtil {
    private CreditCardFormatUtil() {
    }

    public static String maskCard(@NonNull String card, String placeholder) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(card) && TextUtils.isDigitsOnly(card));
        StringBuilder result = new StringBuilder();
        Timber.d("Longitud tarjeta: %d", card.length());
        for (int i = 0; i < card.length(); i++) {
            if (i >= 4 && i <= 11) {
                if ((i + 1) % 4 == 0) {
                    result.append(placeholder).append(" ");
                } else {
                    result.append(placeholder);
                }
            } else {
                if ((i + 1) % 4 == 0) {
                    result.append(card.charAt(i)).append(" ");
                } else {
                    result.append(card.charAt(i));
                }
            }
        }
        return result.toString();
    }

    public static String maskCard(String card) {
        return maskCard(card, "•");
    }

    // 51, 52, 53, 54, 55
    // Visa        4
    public static int getTipoTarjeta(@NonNull String tarjeta) {
        if (TextUtils.indexOf(tarjeta, "37") == 0 || TextUtils.indexOf(tarjeta, "34") == 0)
            return 3;
        if (TextUtils.indexOf(tarjeta, "51") == 0
                || TextUtils.indexOf(tarjeta, "52") == 0
                || TextUtils.indexOf(tarjeta, "53") == 0
                || TextUtils.indexOf(tarjeta, "54") == 0
                || TextUtils.indexOf(tarjeta, "51") == 0
                || TextUtils.indexOf(tarjeta, "52") == 0
                || TextUtils.indexOf(tarjeta, "55") == 0) {
            return 2;
        }
        if (TextUtils.indexOf(tarjeta, "4") == 0) return 1;
        return 0;
    }

    public static boolean isValidCard(String pan) {
        return CreditCardValidator.VISA_VALIDATOR.isValid(pan) ||
                CreditCardValidator.MASTERCARD_VALIDATOR.isValid(pan) ||
                CreditCardValidator.AMEX_VALIDATOR.isValid(pan) || pan.matches(McRegexPatterns.PREVIVALE);
    }
}
