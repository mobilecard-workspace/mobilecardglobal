package addcel.mobilecard.utils

import android.content.Context
import android.provider.Settings

/**
 * ADDCEL on 1/22/19.
 */
class DeviceUtil {
    companion object {
        @Synchronized
        fun getDeviceId(context: Context): String {
            return Settings.System.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        }
    }
}
