package addcel.mobilecard;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDexApplication;

import com.jakewharton.threetenabp.AndroidThreeTen;
import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.di.component.DaggerNetComponent;
import addcel.mobilecard.di.component.NetComponent;
import addcel.mobilecard.di.module.AppModule;
import addcel.mobilecard.di.module.NetModule;
import addcel.mobilecard.domain.location.DaggerLocationComponent;
import addcel.mobilecard.domain.location.LocationComponent;
import addcel.mobilecard.domain.location.LocationModule;
import addcel.mobilecard.validation.annotation.Celular;
import addcel.mobilecard.validation.annotation.Login;
import addcel.mobilecard.validation.annotation.PeajeTag;
import timber.log.Timber;

/**
 * ADDCEL on 22/08/16.
 */
public final class Mobilecard extends MultiDexApplication {

    private static Mobilecard instance;
    private AppModule appModule;
    private NetComponent netComponent;
    private LocationComponent locationComponent;

    public static Mobilecard get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }

        // Setteamos políticas para compartir screenshots desde la aplicación
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        // Registramos validaciones Saripaar
        Validator.registerAnnotation(Login.class);
        Validator.registerAnnotation(Celular.class);
        Validator.registerAnnotation(PeajeTag.class);

        // Iniciamos módulos Singleton Dagger2
        appModule = new AppModule(this);
        netComponent = DaggerNetComponent.builder()
                .appModule(appModule)
                .netModule(new NetModule(BuildConfig.BASE_URL))
                .build();

        instance = this;
    }

    public NetComponent getNetComponent() {
        return netComponent;
    }

    public LocationComponent getLocationComponent() {
        if (locationComponent == null) {
            locationComponent = DaggerLocationComponent.builder()
                    .appModule(appModule)
                    .locationModule(new LocationModule())
                    .build();
        }
        return locationComponent;
    }

    /**
     * A tree which logs important information for crash reporting.
     */
    private static class CrashReportingTree extends Timber.Tree {
        @SuppressLint("LogNotTimber")
        @Override
        protected void log(int priority, String tag, @NonNull String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) return;
            if (t != null) {
                if (priority == Log.ERROR) {
                    t.printStackTrace();
                } else if (priority == Log.WARN) {
                    t.printStackTrace();
                }
            }
        }
    }
}
