package addcel.mobilecard.ui;

import androidx.annotation.NonNull;

/**
 * ADDCEL on 3/14/19.
 */
public interface ScreenView {
    void showProgress();

    void hideProgress();

    void showError(@NonNull String msg);

    void showSuccess(@NonNull String msg);
}
