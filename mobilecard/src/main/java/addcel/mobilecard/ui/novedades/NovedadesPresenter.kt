package addcel.mobilecard.ui.novedades

import addcel.mobilecard.R

/**
 * ADDCEL on 2019-08-08.
 */
interface NovedadesPresenter {
    fun getCarroussel(idPais: Int, tipoUsuario: String): Array<Int>
}

class NovedadesPresenterImpl : NovedadesPresenter {
    override fun getCarroussel(idPais: Int, tipoUsuario: String): Array<Int> {
        if (tipoUsuario == "NEGOCIO") {
            return when (idPais) {
                1 -> getMexUserArray()
                2 -> getMexUserArray()
                3 -> getMexUserArray()
                4 -> getPerNegocioArray()
                else -> {
                    return getMexUserArray()
                }
            }
        } else {
            return when (idPais) {
                1 -> getMexUserArray()
                2 -> getMexUserArray()
                3 -> getMexUserArray()
                4 -> getPerNegocioArray()
                else -> {
                    return getMexUserArray()
                }
            }
        }
    }

    private fun getMexUserArray(): Array<Int> {
        return arrayOf(
                R.drawable.car_reg_mex_01, R.drawable.car_reg_mex_02, R.drawable.car_reg_mex_03,
                R.drawable.car_reg_mex_04, R.drawable.car_reg_mex_05
        )
    }

    private fun getPerNegocioArray(): Array<Int> {
        return arrayOf(
                R.drawable.car_reg_per_01,
                R.drawable.car_reg_per_02,
                R.drawable.car_reg_per_03
        )
    }
}