package addcel.mobilecard.ui.novedades

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletActivity
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletModel
import addcel.mobilecard.ui.negocio.pe.registro.persona.PePersonaFiscal
import addcel.mobilecard.ui.negocio.pe.wallet.NegocioPeWalletActivity
import addcel.mobilecard.ui.negocio.pe.wallet.NegocioPeWalletModel
import addcel.mobilecard.ui.usuario.main.tutorial.MainTutorialFragment.Companion.get
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingActivity
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingModel
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.viewpager.widget.ViewPager
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_limbo.*
import java.util.*

interface NovedadesView {
    fun clickMyMc()

    fun clickNoCard()

    fun clickGetCard()
}

@Parcelize
data class NovedadesModel(
        val idPais: Int, val tipoUsuario: String,
        val personaJuridica: Boolean = false
) : Parcelable

class NovedadesActivity : AppCompatActivity(), NovedadesView {

    companion object {
        const val REQUEST_CODE = 419

        const val DATA_USE_CASE = "use_case"

        const val USE_CASE_MOBILECARD = 1
        const val USE_CASE_GET_CARD = 2
        const val USE_CASE_NO_CARD = 3

        fun get(context: Context, model: NovedadesModel): Intent {
            return Intent(context, NovedadesActivity::class.java).putExtra("model", model)
        }


        fun processUsuario(
                activity: AppCompatActivity,
                useCase: Int,
                idUsuario: Long = 0,
                idPais: Int = 1
        ) {
            when (useCase) {
                USE_CASE_MOBILECARD -> processMobileCardUser(activity, idUsuario, idPais)
                USE_CASE_GET_CARD -> processCardUser(activity)
                USE_CASE_NO_CARD -> processNoCardUser(activity, idPais)
                else -> {
                }
            }
        }

        fun processNegocio(
                activity: AppCompatActivity,
                useCase: Int,
                id: Int,
                idPais: Int,
                persona: String = PePersonaFiscal.NATURAL.name.toLowerCase(Locale.getDefault())
        ) {
            when (useCase) {
                USE_CASE_MOBILECARD -> processMobileCardNegocio(activity, id, idPais, persona)
                else -> {
                }
            }
        }

        private fun processMobileCardUser(activity: AppCompatActivity, idUsuario: Long, idPais: Int) {
            val model = MyMcRoutingModel(idUsuario, idPais)
            val router = MyMcRoutingActivity.get(activity, model)
            activity.startActivityForResult(router, MyMcRoutingActivity.REQUEST_CODE)
        }

        private fun processCardUser(activity: AppCompatActivity) {
            activity.startActivity(WalletContainerActivity.get(activity))
        }

        private fun processNoCardUser(activity: AppCompatActivity, idPais: Int) {
            activity.supportFragmentManager.commit {
                add(R.id.frame_main, get(idPais))
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }

        private fun processMobileCardNegocio(
                activity: AppCompatActivity,
                id: Int,
                idPais: Int,
                persona: String
        ) {
            when (idPais) {
                PaisResponse.PaisEntity.MX -> activity.startActivity(
                        NegocioMxWalletActivity.get(
                                activity,
                                NegocioMxWalletModel(id, persona)
                        )
                )
                PaisResponse.PaisEntity.PE -> activity.startActivity(
                        NegocioPeWalletActivity.get(
                                activity,
                                NegocioPeWalletModel(id, persona)
                        )
                )
            }
        }
    }

    private lateinit var model: NovedadesModel
    private lateinit var carrViews: Array<Int>
    private lateinit var dots: Array<ImageView?>
    private lateinit var adapter: NovedadesTabAdapter

    private val presenter: NovedadesPresenter = NovedadesPresenterImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        setContentView(R.layout.activity_limbo)
        model = intent?.getParcelableExtra("model")!!
        carrViews = presenter.getCarroussel(model.idPais, model.tipoUsuario)
        dots = arrayOfNulls(carrViews.size)
        adapter = NovedadesTabAdapter(carrViews)
        pager_registro_carousel.adapter = adapter
        pager_registro_carousel.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                addBottomDots(this@NovedadesActivity, position)
            }
        })

        b_registro_result_card.setOnClickListener { clickGetCard() }
        b_registro_result_no_card.setOnClickListener { clickNoCard() }
        b_registro_result_mobilecard.setOnClickListener { clickMyMc() }

        addBottomDots(this, 0)

        if (model.tipoUsuario == "NEGOCIO") {
            b_registro_result_card.visibility = android.view.View.INVISIBLE
            if (model.personaJuridica) b_registro_result_mobilecard.visibility =
                    android.view.View.INVISIBLE
        }
    }

    override fun clickMyMc() {
        val intent = Intent().putExtra(DATA_USE_CASE, USE_CASE_MOBILECARD)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun clickNoCard() {
        val intent = Intent().putExtra(DATA_USE_CASE, USE_CASE_NO_CARD)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun clickGetCard() {
        val intent = Intent().putExtra(DATA_USE_CASE, USE_CASE_GET_CARD)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    fun addBottomDots(context: Context?, currentPage: Int) {
        val colorsActive = resources.obtainTypedArray(R.array.array_dot_registro_active)
        val colorsInactive = resources.obtainTypedArray(R.array.array_dot_registro_inactive)

        layout_registro_dots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = ImageView(context)
            dots[i]?.setImageResource(colorsInactive.getResourceId(currentPage, -1))
            layout_registro_dots.addView(dots[i])
        }

        colorsInactive.recycle()

        if (dots.isNotEmpty()) {
            dots[currentPage]?.setImageResource(colorsActive.getResourceId(currentPage, -1))
            colorsActive.recycle()
        }
    }
}
