package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select;

import android.annotation.SuppressLint;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.AccountResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.h2h.H2HAccountInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 08/11/17.
 */
class MxTransferBeneficiarioSelectPresenter
        implements MxTransferBeneficiarioSelectContract.Presenter {
    private static final String CERO = "$0.00";
    private final H2HAccountInteractor interactor;
    private final NumberFormat format;
    private final MxTransferBeneficiarioSelectContract.View view;

    MxTransferBeneficiarioSelectPresenter(H2HAccountInteractor interactor, NumberFormat format,
                                          MxTransferBeneficiarioSelectContract.View view) {
        this.interactor = interactor;
        this.format = format;
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getBeneficiarios() {
        view.showProgress();
        interactor.getAccounts(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<AccountResponse>() {
                    @Override
                    public void onSuccess(@NotNull AccountResponse result) {
                        view.hideProgress();
                        view.setBeneficiarios(result.getAccounts());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void deleteBeneficiario() {
        view.showProgress();
        interactor.deleteAccount(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                (int) view.getSelectedAccount().getId(), new InteractorCallback<AccountResponse>() {
                    @Override
                    public void onSuccess(@NotNull AccountResponse result) {
                        view.hideProgress();
                        view.setBeneficiarios(result.getAccounts());
                        view.showSuccess(result.getMensajeError());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public double getDoubleComision(String monto, AccountEntity account) {
        if (monto.equals("0") || account == null) return 0;
        return account.getComision_fija() + ((Double.valueOf(monto) + account.getComision_fija())
                * account.getComision_porcentaje());
    }

    @Override
    public String getComision(String monto, AccountEntity account) {
        if (monto.equals("0") || account == null) return CERO;
        return format.format(getDoubleComision(monto, account));
    }

    @Override
    public double getDoubleTotal(String monto, AccountEntity account) {
        if (monto.equals("0") || account == null) return 0;
        return Double.valueOf(monto) + getDoubleComision(monto, account);
    }

    @Override
    public String getTotal(String total, AccountEntity account) {
        if (total.equals("0") || account == null) return CERO;
        double comision = getDoubleComision(total, account);
        return format.format(Double.valueOf(total) + comision);
    }
}
