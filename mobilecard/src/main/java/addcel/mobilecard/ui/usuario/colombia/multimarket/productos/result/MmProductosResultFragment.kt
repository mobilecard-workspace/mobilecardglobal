package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.result

import addcel.mobilecard.McConstants
import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.MmConsultaResponse
import addcel.mobilecard.data.net.colombia.multimarket.MmPagoResponse
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.squareup.phrase.Phrase
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_generic_result.*
import mx.mobilecard.crypto.AddcelCrypto
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

/**
 * ADDCEL on 2019-12-19.
 */


@Parcelize
data class MmProductosResultViewModel(val result: MmPagoResponse, val card: CardEntity, val saldo: Double) : Parcelable

class MmProductosResultFragment : Fragment() {
    companion object {
        fun get(result: MmPagoResponse, card: CardEntity, saldo: Double): MmProductosResultFragment {
            val frag = MmProductosResultFragment()
            frag.arguments = BundleBuilder().putParcelable("model", MmProductosResultViewModel(result, card, saldo)).build()
            return frag
        }
    }


    private lateinit var pcActivity: ContainerActivity
    lateinit var model: MmProductosResultViewModel
    lateinit var permission: RxPermissions
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pcActivity = activity as ContainerActivity
        permission = RxPermissions(this)
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_generic_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pcActivity.setStreenStatus(ContainerScreenView.STATUS_FINISHED)
        b_generic_result_aceptar.setOnClickListener { pcActivity.onBackPressed() }
        b_generic_result_home.setOnClickListener { pcActivity.finish() }
        /*b_pago_screenshot_share.setOnClickListener {
            disposables.add(
                permission.request(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                    .subscribe {
                        if (it) {
                            if (isVisible) {
                                val intent =
                                    ScreenshotUtil.shareScreenshot(
                                        activity,
                                        getString(R.string.txt_transaction_share)
                                    )
                                shareScreenshot(intent)
                            } else {
                                if (isVisible) Toasty.error(
                                    this.activity!!,
                                    getString(R.string.txt_ingo_permission_retry)
                                ).show()
                            }
                        }
                    }
            )
        }*/

        title_generic_result.text = getTitleSpan(model.result)
        msg_generic_result.text = buildResultMsg(model.result)
        icon_generic_result.setImageResource(getStatusIcon(model.result.codigo))
        view_generic_result_aclaraciones.text = buildAclaraciones(model.result.codigo, McConstants.PAIS_ID_CO)

        if (model.result.codigo == 0) {
            pcActivity.setStreenStatus(ContainerScreenView.STATUS_FINISHED)
            b_generic_result_aceptar.visibility = View.GONE
        } else {
            pcActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
            b_generic_result_aceptar.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }


    private fun formatDateForMsg(millis: Long): String {
        val dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault())
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
        return formatter.format(dateTime)
    }

    private fun buildResultMsg(result: MmPagoResponse): CharSequence {
        val res = resources
        return if (result.codigo == 0) {
            Phrase.from(res, R.string.txt_confirmacion_pago_msg_mm)
                    .put("mensaje", result.mensaje ?: ErrorUtil.getErrorMsg(ErrorUtil.OPERATION))
                    .put("idbitacora", result.idTransaccion ?: "-")
                    .put("tdc", StringUtil.maskCard(AddcelCrypto.decryptHard(model.card.pan)))
                    .put("date", formatDateForMsg(Calendar.getInstance().timeInMillis)).format()
        } else {
            Phrase.from(res, R.string.txt_confirmacion_pago_error)
                    .put("error", result.codigo.toString())
                    .put("mensaje", result.mensaje
                            ?: ErrorUtil.getErrorMsg(ErrorUtil.OPERATION)).format()
        }
    }

    private fun getStatusIcon(code: Int): Int {
        return if (code == 0) R.drawable.ic_historial_success else R.drawable.ic_historial_error
    }

    private fun getStatusText(code: Int): String {
        return if (code == 0) "EXITOSA" else "FALLIDA"
    }

    private fun getStatusColor(status: Int): Int {
        return if (status == 0) R.color.colorAccept else R.color.colorDeny
    }

    private fun getTitleSpan(receipt: MmPagoResponse): CharSequence {
        val firstPart = SpannableString("OPERACIÓN ")
        val lastPart = SpannableString(getStatusText(receipt.codigo))

        firstPart.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.colorBlack)), 0,
                firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context!!, getStatusColor(receipt.codigo))), 0,
                lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, lastPart)
    }

    private fun buildAclaraciones(code: Int, idPais: Int): String {
        return if (code == 0) {
            "Servicio operado por Plataforma MobileCard. El pago quedará aplicado a tu servicio en las próximas horas.\n\n" +
                    "Para aclaraciones marque " + getPhoneByCountry(idPais)
        } else {
            "Para aclaraciones marque \n" + getPhoneByCountry(idPais)
        }
    }

    private fun getPhoneByCountry(idPais: Int): String {
        return when (idPais) {
            McConstants.PAIS_ID_MX -> McConstants.TEL_MX
            McConstants.PAIS_ID_CO -> McConstants.TEL_CO
            McConstants.PAIS_ID_USA -> McConstants.TEL_US
            McConstants.PAIS_ID_PE -> McConstants.TEL_MX
            else -> McConstants.TEL_US
        }
    }
}