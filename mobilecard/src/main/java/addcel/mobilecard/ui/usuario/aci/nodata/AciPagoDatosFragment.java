package addcel.mobilecard.ui.usuario.aci.nodata;

import android.Manifest;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.impl.StateReceivedEvent;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import addcel.mobilecard.ui.usuario.aci.confirmation.AciConfirmationFragment;
import addcel.mobilecard.ui.usuario.aci.result.AciResultFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * ADDCEL on 28/03/17.
 */
public final class AciPagoDatosFragment extends Fragment implements AciPagoDatosContract.View {

    private static final int USA_BASE_INDEX = 33;
    private final CompositeDisposable permDisposables = new CompositeDisposable();
    @BindView(R.id.spinner_aci_state)
    Spinner stateSpinner;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_aci_city)
    TextInputLayout cityTil;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_aci_zipcode)
    TextInputLayout zipCodeTil;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_aci_direccion)
    TextInputLayout direccionTil;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_aci_cvv)
    TextInputLayout cvvTil;
    @Inject
    Bus bus;
    @Inject
    ConnectivityManager connectivityManager;
    @Inject
    ArrayAdapter<EstadoResponse.EstadoEntity> estadoAdapter;
    @Inject
    Validator validator;
    @Inject
    CardEntity formaPago;
    @Inject
    AciPagoDatosContract.Presenter presenter;
    private AciActivity aciActivity;
    private Unbinder unbinder;
    private RxPermissions permissions;

    public AciPagoDatosFragment() {
    }

    public static AciPagoDatosFragment get(Bundle bundle) {
        final AciPagoDatosFragment fragment = new AciPagoDatosFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aciActivity = ((AciActivity) Preconditions.checkNotNull(getActivity()));
        aciActivity.getSubcomponent()
                .aciPagoDatosSubcomponent(new AciPagoDatosModule(this))
                .inject(this);
        permissions = new RxPermissions(this);
        bus.register(this);
        presenter.getEstados();
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_aci_pago_datos, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        aciActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        stateSpinner.setAdapter(estadoAdapter);
        final String city = presenter.getCiudad();
        final String zipCode = presenter.getZipCode();
        final String address = presenter.getAddress();
        final String code = presenter.getSecurityCode();

        if (!Strings.isNullOrEmpty(city)) {
            AndroidUtils.setText(cityTil.getEditText(), city);
            cityTil.setEnabled(false);
        }

        if (!Strings.isNullOrEmpty(zipCode)) {
            AndroidUtils.setText(zipCodeTil.getEditText(), zipCode);
            zipCodeTil.setEnabled(false);
        }

        if (!Strings.isNullOrEmpty(address)) {
            AndroidUtils.setText(direccionTil.getEditText(), address);
            direccionTil.setEnabled(false);
        }

        if (!Strings.isNullOrEmpty(code)) {
            AndroidUtils.setText(cvvTil.getEditText(), code);
            cvvTil.setEnabled(false);
        }

        if (estadoAdapter.getCount() == 0) {
            presenter.getEstados();
        } else {
            setEstadoValues();
        }
    }

    @Subscribe
    public void onEstadosReceived(StateReceivedEvent event) {
        Timber.d(event.getEstados().toString());
        estadoAdapter.addAll(event.getEstados());
        estadoAdapter.notifyDataSetChanged();
        setEstadoValues();
    }

    @OnClick(R.id.b_aci_pagar)
    void clickPagar() {
        ValidationUtils.clearViewErrors(cityTil, zipCodeTil, direccionTil, cvvTil);
        validator.validate();
    }

    @Subscribe
    public void onResult(McResponse response) {
        aciActivity.getFragmentManagerLazy()
                .beginTransaction()
                .replace(R.id.frame_aci, AciResultFragment.get(response))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        permDisposables.clear();
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onValidationSucceeded() {
        permDisposables.add(
                permissions.request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                        .subscribe(aBoolean -> {
                            if (aBoolean) launchConfirmation();
                        })
        );
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationUtils.onValidationFailed(aciActivity, errors);
    }

    private void setEstadoValues() {
        final int idEstado = presenter.getIdEstado();
        Timber.d("Id estado usuario: %d", idEstado);
        if (idEstado < 33) {
            stateSpinner.setSelection(idEstado);
        } else {
            stateSpinner.setSelection(idEstado - USA_BASE_INDEX);
            stateSpinner.setEnabled(false);
        }
    }

    private void launchConfirmation() {
        Objects.requireNonNull(getArguments())
                .putString("ciudad", AndroidUtils.getText(cityTil.getEditText()));
        getArguments().putString("cvv", AndroidUtils.getText(cvvTil.getEditText()));
        getArguments().putString("direccion", AndroidUtils.getText(direccionTil.getEditText()));
        getArguments().putParcelable("estado",
                estadoAdapter.getItem(stateSpinner.getSelectedItemPosition()));
        getArguments().putString("zipCode", AndroidUtils.getText(zipCodeTil.getEditText()));

        final AciConfirmationFragment fragment = AciConfirmationFragment.get(getArguments());
        aciActivity.getFragmentManagerLazy()
                .beginTransaction()
                .addToBackStack(null)
                .hide(this)
                .add(R.id.frame_aci, fragment)
                .commit();
    }

    @Override
    public void showProgress() {
        aciActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        aciActivity.hideProgress();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        aciActivity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        aciActivity.showSuccess(msg);
    }

    @Override
    public String getIMEI() {
        return DeviceUtil.Companion.getDeviceId(aciActivity);
    }
}
