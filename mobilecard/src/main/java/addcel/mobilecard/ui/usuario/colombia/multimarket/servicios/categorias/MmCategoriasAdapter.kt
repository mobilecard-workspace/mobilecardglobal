package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.categorias

import addcel.mobilecard.data.net.colombia.multimarket.Categoria
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import addcel.mobilecard.R
import android.view.LayoutInflater
import android.view.ViewGroup
import timber.log.Timber

/**
 * ADDCEL on 2019-12-19.
 */
class MmCategoriasAdapter(val data: MutableList<Categoria> = ArrayList()) : RecyclerView.Adapter<MmCategoriasAdapter.ViewHolder>() {

    fun update(args: List<Categoria>) {
        data.clear()
        data.addAll(args)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_mm_producto, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoria: Categoria? = data[position]
        if (categoria != null) {
            holder.text.text = categoria.nombre
        }
    }

    fun getItem(position: Int): Categoria {
        Timber.d("POSITION adapter: %d", position)
        return data[position]
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val text: TextView = v.findViewById(R.id.text_mm_item)
    }

}