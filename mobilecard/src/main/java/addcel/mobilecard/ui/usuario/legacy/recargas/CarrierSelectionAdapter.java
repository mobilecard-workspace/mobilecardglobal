package addcel.mobilecard.ui.usuario.legacy.recargas;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.utils.MapUtils;

/**
 * ADDCEL on 14/12/16.
 */
public final class CarrierSelectionAdapter
        extends RecyclerView.Adapter<CarrierSelectionAdapter.ViewHolder> {

    private final List<CatalogoResponse.RecargaEntity> models;
    private final Map<Integer, Integer> logos;

    CarrierSelectionAdapter(List<CatalogoResponse.RecargaEntity> carriers,
                            Map<Integer, Integer> logos) {
        this.models = carriers;
        this.logos = logos;
    }

    public void insert(int pos, CatalogoResponse.RecargaEntity model) {
        this.models.add(pos, model);
        notifyItemInserted(pos);
    }

    public void update(List<CatalogoResponse.RecargaEntity> models) {
        this.models.clear();
        this.models.addAll(models);
        notifyDataSetChanged();
    }

    public CatalogoResponse.RecargaEntity getItem(int pos) {
        return models.get(pos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_servicio, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CatalogoResponse.RecargaEntity recargaModel = models.get(position);
        holder.text.setText(recargaModel.toString());
        holder.logo.setImageResource(
                MapUtils.getOrDefault(logos, recargaModel.getId(), R.drawable.recargas));
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView text;
        private final ImageView logo;

        ViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text_servicio);
            logo = itemView.findViewById(R.id.img_servicio_logo);
        }
    }
}
