package addcel.mobilecard.ui.usuario.scanpay;

/**
 * ADDCEL on 21/08/18.
 */
interface ScanPayContract {
    interface View {

        void setFinished(boolean finished);

        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);
    }

    interface Presenter {
    }
}
