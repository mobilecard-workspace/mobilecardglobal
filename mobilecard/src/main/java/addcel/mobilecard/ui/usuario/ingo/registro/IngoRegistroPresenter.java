package addcel.mobilecard.ui.usuario.ingo.registro;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import org.jetbrains.annotations.NotNull;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.ingo.model.IngoEnrollmentRequest;
import addcel.mobilecard.data.net.ingo.model.IngoEstadoResponse;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.ingo.registro.IngoRegistroInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 18/07/18.
 */
class IngoRegistroPresenter implements IngoRegistroContract.Presenter {
    private final IngoRegistroInteractor interactor;
    private final DateTimeFormatter dateTimeFormatter;
    private final IngoRegistroContract.View view;

    IngoRegistroPresenter(IngoRegistroInteractor interactor, DateTimeFormatter dateTimeFormatter,
                          IngoRegistroContract.View view) {
        this.interactor = interactor;
        this.dateTimeFormatter = dateTimeFormatter;
        this.view = view;
    }

    @Override
    public String getEmail() {
        return Strings.nullToEmpty(interactor.getUsuario().getEMail());
    }

    @Override
    public String getNombres() {
        return Strings.nullToEmpty(interactor.getUsuario().getUsrNombre());
    }

    @Override
    public String getApellidos() {
        return Strings.nullToEmpty(interactor.getUsuario().getUsrApellido());
    }

    @Override
    public String getPhone() {
        return Strings.nullToEmpty(interactor.getUsuario().getUsrTelefono());
    }

    @Override
    public String getFormattedDateTime(LocalDateTime dobTime) {
        return dateTimeFormatter.format(dobTime);
    }

    @SuppressLint("CheckResult")
    @Override
    public void getEstados() {
        view.showProgress();
        interactor.getEstados(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<IngoEstadoResponse.EstadoEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<IngoEstadoResponse.EstadoEntity> result) {
                        view.hideProgress();
                        view.setEstados(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    /**
     * @Expose private String addressLine1; @Expose private String addressLine2; @Expose private
     * Boolean allowTexts; @Expose private String city; @Expose private String
     * countryOfOrigin; @Expose private String dateOfBirth; @Expose private String deviceId; @Expose
     * private String email; @Expose private String firstName; @Expose private String gender; @Expose
     * private String homeNumber; @Expose private Long idUsuario; @Expose private String
     * lastName; @Expose private String middleInitial; @Expose private String mobileNumber; @Expose
     * private String plataforma; @Expose private String ssn; @Expose private String state; @Expose
     * private String suffix; @Expose private String title; @Expose private String zip;
     */
    @SuppressLint("CheckResult")
    @Override
    public void enroll() {
        view.showProgress();
        IngoEnrollmentRequest request =
                new IngoEnrollmentRequest.Builder().withAddressLine1(view.getDireccion())
                        .withCity(view.getCiudad())
                        .withDateOfBirth(getFormattedDateTime(view.getDob()))
                        .withDeviceId(view.getImei())
                        .withEmail(interactor.getUsuario().getEMail())
                        .withFirstName(interactor.getUsuario().getUsrNombre())
                        .withIdUsuario(interactor.getUsuario().getIdeUsuario())
                        .withLastName(interactor.getUsuario().getUsrApellido())
                        .withMobileNumber(interactor.getUsuario().getUsrTelefono())
                        .withSsn(view.getSsn())
                        .withState(view.getEstado().getId())
                        .withZip(view.getZipCode())
                        .build();
        interactor.enroll(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request,
                new InteractorCallback<IngoUserData>() {
                    @Override
                    public void onSuccess(@NotNull IngoUserData result) {
                        view.hideProgress();
                        view.showSuccess(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }
}
