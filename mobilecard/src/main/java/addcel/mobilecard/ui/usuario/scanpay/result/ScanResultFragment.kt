package addcel.mobilecard.ui.usuario.scanpay.result

import addcel.mobilecard.McConstants
import addcel.mobilecard.R
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import addcel.mobilecard.ui.usuario.scanpay.result.qr.ScanResultQrFragment
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.FragmentUtil
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.format.DateFormat
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.common.base.Strings
import com.squareup.phrase.Phrase
import kotlinx.android.synthetic.main.screen_scanpay_result.*

/** ADDCEL on 11/09/18.  */
class ScanResultFragment : Fragment(), ScanResultContract.View {

    companion object {
        fun get(result: SPReceiptEntity, fromNegocio: Boolean): ScanResultFragment {
            return get(result, McConstants.PAIS_ID_MX, fromNegocio)
        }

        fun get(result: SPReceiptEntity, idPais: Int, fromNegocio: Boolean): ScanResultFragment {
            val args =
                    BundleBuilder().putParcelable("result", result)
                            .putInt("idPais", idPais)
                            .putBoolean("fromNegocio", fromNegocio)
                            .build()
            val fragment = ScanResultFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var presenter: ScanResultPresenter
    private var idPais: Int = McConstants.PAIS_ID_MX
    private var fromNegocio: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = ScanResultPresenter(arguments?.getParcelable("result")!!)
        idPais = arguments?.getInt("idPais", McConstants.PAIS_ID_MX)!!
        fromNegocio = arguments?.getBoolean("fromNegocio")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_scanpay_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b_scanpay_result_ok.setOnClickListener { clickOk() }
        b_scanpay_result_qr.setOnClickListener { clickQR() }
        checkIfFromScanOrNegocioAndSetFinished()
        setUIData()
    }

    override fun onResume() {
        super.onResume()
        checkIfFromScanOrNegocioAndSetFinished()
    }

    override fun setUIData() {
        img_scanpay_result.setImageResource(getStatusIcon(presenter.result.code))
        view_scanpay_result_title.text = getTitleSpan(presenter.result)
        view_scanpay_result_msg.text = buildResultMsg()
        if (presenter.result.code != 0 || fromNegocio) b_scanpay_result_qr.visibility = View.GONE
    }

    override fun clickQR() {
        if (isVisible) {
            val frame = if (fromNegocio) R.id.frame_negocio_cobro else R.id.frame_scanpay
            FragmentUtil.addFragmentHidingCurrent(
                    activity!!.supportFragmentManager, frame, this,
                    ScanResultQrFragment.get(presenter.result, fromNegocio)
            )
        }
    }

    override fun clickOk() {
        if (isVisible) {
            if (fromNegocio) {
                val ncActivity = activity as NegocioCobroActivity
                ncActivity.refreshHomeScreen(ncActivity.fragmentManagerLazy)
            } else {
                activity!!.onBackPressed()
            }
        }
    }

    private fun checkIfFromScanOrNegocioAndSetFinished() {
        if (fromNegocio) {
            if (presenter.result.code == 0) (activity as NegocioCobroActivity).setStreenStatus(ContainerScreenView.STATUS_FINISHED
            )
        } else {
            if (presenter.result.code == 0) (activity as ScanPayActivity).setFinished(true)
        }
    }

    private fun buildResultMsg(): CharSequence {
        val res = resources
        return if (presenter.result.code == 0) {
            Phrase.from(res, R.string.txt_confirmacion_pago_msg)
                    .put("mensaje", Strings.nullToEmpty(presenter.result.message))
                    .put("idbitacora", presenter.result.idTransaccion.toString())
                    .put("auth", Strings.nullToEmpty(presenter.result.authNumber))
                    .put("tdc", Strings.nullToEmpty(presenter.result.maskedPAN))
                    .put("date", DateFormat.format("dd-MM-yyyy HH:mm:ss", presenter.result.dateTime))
                    .put("monto", McConstants.getNumberFormat(idPais).format(presenter.result.amount)).format()
        } else {
            Phrase.from(res, R.string.txt_confirmacion_pago_error)
                    .put("error", presenter.result.code.toString())
                    .put("mensaje", Strings.nullToEmpty(presenter.result.message)).format()
        }
    }

    private fun getStatusIcon(status: Int): Int {
        return if (status == 0) R.drawable.ic_historial_success else R.drawable.ic_historial_error
    }

    private fun getStatusText(status: Int): String {
        return if (status == 0) "EXITOSA" else "FALLIDA"
    }

    private fun getStatusColor(status: Int): Int {
        return if (status == 0) R.color.colorAccept else R.color.colorDeny
    }

    private fun getTitleSpan(receipt: SPReceiptEntity): CharSequence {
        val firstPart = SpannableString("OPERACIÓN ")
        val lastPart = SpannableString(getStatusText(receipt.code))

        firstPart.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.colorBlack)), 0,
                firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context!!, getStatusColor(receipt.code))), 0,
                lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, lastPart)
    }
}
