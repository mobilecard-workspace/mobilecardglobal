package addcel.mobilecard.ui.usuario.scanpay.qr;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.zxing.Result;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.model.SPQrEntity;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.utils.FragmentUtil;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * ADDCEL on 21/08/18.
 */
public final class ScanQrFragment extends Fragment implements ScanQrContract.View {
    @Inject
    ScanPayActivity activity;
    @Inject
    ScanQrContract.Presenter presenter;
    private ZXingScannerView scanner;

    public static synchronized ScanQrFragment get() {
        return new ScanQrFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().scanQrSubcomponent(new ScanQrModule(this)).inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_scanpay_qr, container, false);
        scanner = view.findViewById(R.id.scanner_scanpay_qr);
        view.findViewById(R.id.b_scanpay_qr_id).setOnClickListener(view1 -> clickId());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setFinished(Boolean.FALSE);
    }

    @Override
    public void onResume() {
        super.onResume();
        scanner.setResultHandler(this);
        scanner.startCamera();
    }

    @Override
    public void onPause() {
        scanner.setResultHandler(null);
        scanner.stopCamera();
        super.onPause();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        if (isVisible()) scanner.resumeCameraPreview(this);
        activity.showError(msg);
    }

    @Override
    public void showSuccess(String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void onScanSuccess(SPQrEntity pagoEntity) {
        scanner.stopCamera();


        double amount = presenter.getIdPais() == PaisResponse.PaisEntity.MX ? pagoEntity.getAmountMxn() : pagoEntity.getAmountUsd();

        SPPagoEntity pEntity =
                new SPPagoEntity(amount, pagoEntity.getComision(), pagoEntity.getConcept(),
                        "", pagoEntity.getEstablecimientoId(), "",
                        pagoEntity.getIdBitacora(), 0, 0,
                        DeviceUtil.Companion.getDeviceId(activity), 0.0, 0.0,
                        "", 0, pagoEntity.getPropina(),
                        pagoEntity.getReferenciaNeg(), "", "",
                        "", "", "",
                        "", "", "", "");

        Bundle bundle = new BundleBuilder().putParcelable("pago", pEntity).build();

        FragmentUtil.replaceFragment(activity.getSupportFragmentManager(), R.id.frame_scanpay,
                WalletSelectFragment.get(WalletSelectFragment.SCANPAY_QR_MX, bundle));
    }

    @Override
    public void onIdReceived(@NotNull String id) {
    }

    @Override
    public void clickId() {
        scanner.stopCamera();

        FragmentUtil.replaceFragment(activity.getSupportFragmentManager(), R.id.frame_scanpay,
                WalletSelectFragment.get(WalletSelectFragment.SCANPAY_QR_ID_MX, new Bundle()));
    }

    @Override
    public void handleResult(Result result) {
        presenter.validateScanResult(result.getText());
        // scanner.resumeCameraPreview(this);
    }
}
