package addcel.mobilecard.ui.usuario.aci.processing;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import addcel.mobilecard.ui.usuario.aci.confirmation.AciConfirmationFragment;
import addcel.mobilecard.ui.usuario.aci.nodata.AciPagoDatosFragment;

/**
 * ADDCEL on 16/10/17.
 */
public class AciProcessingFragment extends Fragment implements AciProcessingContract.View {

    @Inject
    AciActivity aciActivity;
    @Inject
    AciProcessingContract.Presenter presenter;

    public AciProcessingFragment() {
    }

    public static AciProcessingFragment get(Bundle bundle) {
        AciProcessingFragment fragment = new AciProcessingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .aciProcessingSubcomponent(new AciProcessingModule(this))
                .inject(this);

        presenter.checkData();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void onHasData() {
        if (getArguments() != null) {
            getArguments().putBoolean("withData", true);
            final AciConfirmationFragment fragment = AciConfirmationFragment.get(getArguments());
            aciActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .add(R.id.frame_aci, fragment)
                    .commit();
        }
    }

    @Override
    public void onNoData() {
        if (getArguments() != null) {
            getArguments().putBoolean("withData", false);
            final AciPagoDatosFragment fragment = AciPagoDatosFragment.get(getArguments());
            aciActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .add(R.id.frame_aci, fragment)
                    .commit();
        }
    }

    @Override
    public void showProgress() {
        if (aciActivity != null) aciActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        if (aciActivity != null) aciActivity.hideProgress();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        aciActivity.getFragmentManagerLazy().beginTransaction().remove(this).commit();
        aciActivity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        aciActivity.showSuccess(msg);
    }
}
