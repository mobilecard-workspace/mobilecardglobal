package addcel.mobilecard.ui.usuario.blackstone.carriers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.blackstone.model.BSCarrierEntity;
import addcel.mobilecard.utils.ListUtil;

/**
 * ADDCEL on 09/07/18.
 */
public final class BSCarrierAdapter extends RecyclerView.Adapter<BSCarrierAdapter.ViewHolder> {
    private final List<BSCarrierEntity> mCarriers;

    BSCarrierAdapter() {
        this.mCarriers = Lists.newArrayList();
    }

    public void update(List<BSCarrierEntity> carriers) {
        if (ListUtil.notEmpty(mCarriers)) mCarriers.clear();
        this.mCarriers.addAll(carriers);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_servicio, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.text.setText(mCarriers.get(i).getCarrierName());
    }

    @Override
    public int getItemCount() {
        return mCarriers.size();
    }

    public BSCarrierEntity getItem(int pos) {
        return mCarriers.get(pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView text;
        private final ImageView logo;

        ViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text_servicio);
            logo = itemView.findViewById(R.id.img_servicio_logo);
            init();
        }

        private void init() {
            logo.setImageResource(R.drawable.recargas);
        }
    }
}
