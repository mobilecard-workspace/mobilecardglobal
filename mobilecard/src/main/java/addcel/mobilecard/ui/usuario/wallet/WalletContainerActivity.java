package addcel.mobilecard.ui.usuario.wallet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.common.collect.Lists;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.usuario.bottom.MenuEvent;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.ui.contacto.ContactoActivity;
import addcel.mobilecard.ui.contacto.ContactoModel;
import addcel.mobilecard.ui.usuario.edocuenta.EdocuentaActivity;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.ui.usuario.main.MainContract;
import addcel.mobilecard.ui.usuario.menu.MenuLayout;
import addcel.mobilecard.ui.usuario.wallet.cards.WalletCardsFragment;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.ListUtil;
import es.dmoral.toasty.Toasty;

public class WalletContainerActivity extends AppCompatActivity implements WalletContainerView {
    public Toolbar toolbar;
    public MenuLayout bottomMenu;
    @Inject
    Bus bus;
    @Inject
    SessionOperations session;
    boolean relaunchMenu;
    private List<CardEntity> models;
    private ProgressBar progressBar;

    public static synchronized Intent get(@NotNull Context context) {
        return get(context, false);
    }

    public static synchronized Intent get(@NotNull Context context, boolean relaunchMenu) {
        return new Intent(context, WalletContainerActivity.class).putExtras(
                new BundleBuilder().putBoolean("relaunchMenu", relaunchMenu).build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_container);
        toolbar = findViewById(R.id.toolbar_wallet_container);
        bottomMenu = findViewById(R.id.menu_wallet);
        setSupportActionBar(toolbar);
        try {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .add(R.id.container_wallet, WalletCardsFragment.get())
                    .commit();
            Mobilecard.get().getNetComponent().inject(this);
            relaunchMenu = getIntent().getExtras().getBoolean("relaunchMenu");
        } catch (Throwable t) {
            Toasty.info(this, Objects.requireNonNull(t.getLocalizedMessage())).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (relaunchMenu) {
            startActivity(MainActivity.get(this, false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
        bottomMenu.unblock();
    }

    @Override
    protected void onPause() {
        bottomMenu.block();
        bus.unregister(this);
        super.onPause();
    }

    public List<CardEntity> getModels() {
        if (models == null) {
            models = Lists.newArrayList();
        }
        return models;
    }

    public ProgressBar getProgressBar() {
        if (progressBar == null) {
            progressBar = findViewById(R.id.progress_wallet_container);
        }
        return progressBar;
    }

    @Override
    public void updateCards(List<CardEntity> models) {
        if (ListUtil.notEmpty(this.models)) this.models.clear();
        this.models.addAll(models);
    }

    @Override
    public void postWalletUpdateEvent(List<CardEntity> models) {
        bus.post(new WalletUpdateEvent(models));
    }

    @Subscribe
    @Override
    public void onMenuEventReceived(@NotNull MenuEvent event) {
        bottomMenu.unblock();
        final UseCase useCase = event.getUseCase();
        if (useCase.equals(UseCase.HOME)) {
            startActivity(MainActivity.get(this, MainContract.UseCase.INICIO, false));
        } else if (useCase.equals(UseCase.CONTACTO)) {
            startActivity(ContactoActivity.Companion.get(this, new ContactoModel(session.getUsuario().getIdeUsuario(), session.getUsuario().getEMail(), session.getUsuario().getIdPais())));
        } else if (useCase.equals(UseCase.HISTORIAL)) {
            startActivity(EdocuentaActivity.get(this));
        }
    }
}
