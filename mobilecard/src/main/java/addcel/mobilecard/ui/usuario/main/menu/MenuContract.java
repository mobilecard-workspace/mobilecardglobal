package addcel.mobilecard.ui.usuario.main.menu;

import android.graphics.Bitmap;

import addcel.mobilecard.event.impl.PostPaisEvent;

/**
 * ADDCEL on 04/10/17.
 */
public interface MenuContract {
    interface View {

        void setWelcomeName();

        void clickProfile();

        void clickModel(int pos);

        void showProgress();

        void hideProgress();

        void showError(String mensajeError);

        void showSuccess(String mensaje);

        void finish();

        void updatePais(PostPaisEvent event);

        void postProfile(Bitmap bitmap);
    }

    interface Presenter {
        void launchView(MenuModel model);

        void updateDrawer();

        String getNombre();

        int getIdPais();

        void activate();

        void showIngo();

        void setLaunched();

        void loadProfileDelegate();
    }
}
