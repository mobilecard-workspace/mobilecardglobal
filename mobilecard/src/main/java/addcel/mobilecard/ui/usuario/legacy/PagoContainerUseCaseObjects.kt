package addcel.mobilecard.ui.usuario.legacy

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * ADDCEL on 2019-06-24.
 */
@Parcelize
data class RecargaUseCaseObject(
        val recargas: List<CatalogoResponse.RecargaEntity>,
        val pais: PaisResponse.PaisEntity?
) : Parcelable

@Parcelize
data class CategoriaUseCaseObject(
        val categorias: List<CatalogoResponse.CategoriaEntity>
) : Parcelable

@Parcelize
data class TaeFavUseCaseObject(
        val recarga: CatalogoResponse.RecargaEntity,
        val montos: List<CatalogoResponse.MontoEntity>
) : Parcelable

@Parcelize
data class PeajeFavUseCaseObject(
        val recarga: CatalogoResponse.RecargaEntity,
        val montos: List<CatalogoResponse.MontoEntity>
) : Parcelable

@Parcelize
data class ServicioFavUseCaseObject(val servicio: ServicioResponse.ServicioEntity) :
        Parcelable