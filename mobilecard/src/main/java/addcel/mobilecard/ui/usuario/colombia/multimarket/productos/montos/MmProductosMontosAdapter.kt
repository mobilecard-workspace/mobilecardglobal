package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.montos

import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Valor
import addcel.mobilecard.ui.custom.adapter.SelectableAdapter
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.text.isDigitsOnly
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * ADDCEL on 2019-12-18.
 */
class MmProductosMontosAdapter(val data: MutableList<Valor> = ArrayList()) : SelectableAdapter<MmProductosMontosAdapter.ViewHolder>() {

    private val numberFormat = NumberFormat.getCurrencyInstance(Locale("es", "CO"))

    fun update(models: List<Valor>) {
        data.clear()
        data.addAll(models)
        notifyDataSetChanged()
    }

    fun getItem(pos: Int): Valor {
        return data[pos]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemLayoutView =
                LayoutInflater.from(parent.context).inflate(R.layout.view_monto, parent, false)
        return ViewHolder(itemLayoutView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val valor = data[position]
        holder.montoText.text = if (valor.valor.isDigitsOnly()) numberFormat.format(valor.valor.toInt())
        else valor.valor
        TextViewCompat.setAutoSizeTextTypeWithDefaults(holder.montoText, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM)
        if (isSelected(position)) {
            holder.montoText.setBackgroundColor(ContextCompat.getColor(holder.montoText.context, R.color.accent))
            holder.montoText.setTextColor(Color.WHITE)
        } else {
            holder.montoText.background = ContextCompat.getDrawable(holder.montoText.context, R.drawable.sh_rectangle)
            holder.montoText.setTextColor(Color.GRAY)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val montoText: TextView = view.findViewById(R.id.text_monto)
    }
}