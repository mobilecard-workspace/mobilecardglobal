package addcel.mobilecard.ui.usuario.scanpay.qr.id;

import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.Authenticable;

/**
 * ADDCEL on 27/08/18.
 */
public interface ScanQrIdContract {
    interface View extends Authenticable {
        String getScanPayId();

        String getMsi();

        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void clickValidate();

        void clickConfirmar();

        CardEntity getcard();

        void setData(SPPagoEntity pago);

        void setTestData(String... data);

        void launchGetProfile();

        void launchGetToken(String profile);

        void onTokenReceived(TokenEntity tokenEntity);

        void launchPago(TokenEntity token);

        void onPagoReceived(SPReceiptEntity result);
    }

    interface Presenter {

        void clearData();

        Object getData();

        void setData(Object object);

        boolean hasData();

        SPPagoEntity buildRequest();

        String getJumioRef();

        void checkWhiteList();

        void getToken(String profile);

        void pago(TokenEntity tokenEntity, SPPagoEntity request);
    }
}
