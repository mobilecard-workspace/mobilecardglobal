package addcel.mobilecard.ui.usuario.mymobilecard.display

import addcel.mobilecard.McConstants
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.cvv.MobilecardCvvTimerActivity
import addcel.mobilecard.ui.cvv.MobilecardCvvTimerModel
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity
import addcel.mobilecard.ui.usuario.mymobilecard.previvale.historial.PrevivaleHistorialFragment
import addcel.mobilecard.utils.AndroidUtils
import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.google.common.base.Strings
import com.squareup.phrase.Phrase
import kotlinx.android.synthetic.main.view_my_mobilecard_previvale_display.*
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 17/11/17.
 */

internal interface MyMobilecardDisplayContractView : ScreenView {
    val activationPhrase: Phrase

    fun clickSave()

    fun clickCvv()

    fun copyPanToClipboard(): Boolean

    fun configMovimientosButton()

    fun changeButtonsVisibility()
}



class MyMobilecardDisplayFragment : Fragment(), MyMobilecardDisplayContractView {

    @Inject
    internal lateinit var myMcActivity: MyMobilecardActivity
    @Inject
    internal lateinit var adapter: MyMobilecardDisplayTransactionAdapter
    @Inject
    internal lateinit var presenter: MyMobilecardDisplayPresenter

    override val activationPhrase: Phrase
        get() = if (isVisible) {
            Phrase.from(Objects.requireNonNull<Context>(context), R.string.txt_activation_msg)
        } else {
            Phrase.from("Llama al {phone} o haz clic en Activar Ahora")
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get()
                .netComponent
                .myMobilecardDisplaySubcomponent(MyMobilecardDisplayModule(this))
                .inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_my_mobilecard_previvale_display, container, false)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) myMcActivity.setAppToolbarTitle(R.string.txt_menu_mymobilecard)
    }

    private fun setClicks() {
        addClickListenerToActivateButton()
        card_container.setOnClickListener { clickCvv() }
        b_my_mc_pan.setOnClickListener { copyPanToClipboard() }
        b_my_mc_cvv.setOnClickListener { clickCvv() }
        view_mymobilecard_display_clabe.setOnClickListener { clickSave() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setClicks()
        myMcActivity.setStreenStatus(ContainerScreenView.STATUS_FINISHED)

        val ccv = presenter.buildCardView(view.context)

        card_container.addView(ccv)
        changeButtonsVisibility()

        AndroidUtils.setText(
                view_mymobilecard_display_balance,
                getString(R.string.txt_negocio_card_saldo, presenter.balance)
        )
        AndroidUtils.setText(
                view_mymobilecard_display_clabe,
                getString(R.string.txt_negocio_card_clabe, presenter.model.clabe)
        )

        b_mymc_info.setOnClickListener {
            AlertDialog.Builder(myMcActivity).setTitle("Aviso")
                    .setMessage(R.string.txt_mymc_info)
                    .setPositiveButton("Aceptar") { dialogInterface, _ -> dialogInterface.dismiss() }
                    .show()
        }

        configMovimientosButton()

        if (presenter.model.imgFull != null) {
            presenter.loadBmp(presenter.model.imgFull!!, ccv)
        }
    }

    private fun addClickListenerToActivateButton() {
        b_mymobilecard_display_activate.setOnClickListener { view ->
            val msgSequence: CharSequence
            msgSequence = try {
                presenter.buildPhoneMessage()
            } catch (t: Throwable) {
                Timber.e(t)
                Strings.nullToEmpty(presenter.model.phoneNumberActivation).trim { it <= ' ' }
            }

            val msg = View.inflate(view.context, R.layout.view_activation_msg, null)

            (AndroidUtils.findViewById<View>(msg, R.id.text_activation_msg) as TextView).text =
                    msgSequence
            AlertDialog.Builder(view.context).setTitle(R.string.txt_add_mc_title)
                    .setView(msg)
                    .setPositiveButton(R.string.txt_mymc_activate) { _, _ ->
                        // TODO Agregar lanzamiento de marcado de teléfono

                        if (presenter.model.phoneNumberActivation != null) {
                            val intent = Intent(
                                    Intent.ACTION_DIAL,
                                    Uri.parse("tel:" + presenter.model.phoneNumberActivation)
                            )
                            startActivity(intent)
                        }
                    }
                    .setNegativeButton(
                            android.R.string.cancel
                    ) { dialogInterface, _ -> dialogInterface.dismiss() }
                    .setCancelable(false)
                    .show()
        }
    }

    override fun clickSave() {
        val clipboard =
                myMcActivity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText(
                "MI CUENTA CLABE",
                //AndroidUtils.getText(view_mymobilecard_display_clabe)
                presenter.model.clabe
        )
        clipboard.setPrimaryClip(clip)
        Toast.makeText(myMcActivity, "CLABE copiada en portapapeles", Toast.LENGTH_SHORT).show()
    }

    override fun clickCvv() {
        val model = MobilecardCvvTimerModel(Strings.nullToEmpty(presenter.model.codigo))
        startActivity(MobilecardCvvTimerActivity.get(myMcActivity, model))
    }

    override fun copyPanToClipboard(): Boolean {
        val clipboard =
                myMcActivity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText(
                "MI CUENTA CLABE",
                AddcelCrypto.decryptHard(presenter.model.pan)
        )
        clipboard.setPrimaryClip(clip)
        Toast.makeText(
                myMcActivity,
                "Número de tarjeta copiado en portapapeles",
                Toast.LENGTH_SHORT
        ).show()
        return true
    }

    override fun configMovimientosButton() {
        if (myMcActivity.usuario.idPais == McConstants.PAIS_ID_MX) {
            b_mymobilecard_display_movimientos.setOnClickListener {
                myMcActivity.supportFragmentManager.commit {
                    add(R.id.frame_mymobilecard, PrevivaleHistorialFragment.get(myMcActivity.usuario.ideUsuario, presenter.model.pan!!))
                    hide(this@MyMobilecardDisplayFragment)
                    addToBackStack(null)
                    setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
                }
            }
        } else {
            b_mymobilecard_display_movimientos.visibility = View.GONE
        }
    }

    override fun changeButtonsVisibility() {

        val visibleStatus = if (myMcActivity.usuario.idPais == McConstants.PAIS_ID_MX) View.GONE else View.VISIBLE

        b_mymobilecard_display_plastico.visibility = visibleStatus
        b_mymobilecard_display_recargar.visibility = visibleStatus
    }

    override fun showSuccess(msg: String) {
        myMcActivity.showSuccess(msg)
    }

    override fun showError(msg: String) {
        myMcActivity.showError(msg)
    }

    override fun hideProgress() {
        myMcActivity.hideProgress()
    }

    override fun showProgress() {
        myMcActivity.showProgress()
    }

    companion object {

        fun get(model: CardEntity): MyMobilecardDisplayFragment {
            val bundle = Bundle()
            bundle.putParcelable("myMobilecard", model)
            val fragment = MyMobilecardDisplayFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
