package addcel.mobilecard.ui.usuario.colombia.soad.placa

import addcel.mobilecard.R
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.colombia.soad.info.ColombiaSoadInfoFragment
import addcel.mobilecard.ui.usuario.colombia.soad.info.ColombiaSoadInfoModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.goodiebag.pinview.Pinview
import kotlinx.android.synthetic.main.screen_soad_placa.*
import java.util.*

/**
 * ADDCEL on 9/30/19.
 */

interface ColombiaSoadPlacaView : ScreenView, Pinview.PinViewEventListener {
    fun launchInfoScreen()
}

class ColombiaSoadPlacaFragment : Fragment(), ColombiaSoadPlacaView {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_soad_placa, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        pinview_soad_placa.setPinViewEventListener(this)
        b_soad_placa_continuar.setOnClickListener { launchInfoScreen() }
    }

    override fun launchInfoScreen() {
        if (b_soad_placa_continuar.isChecked) {
            (activity as ContainerActivity).fragmentManagerLazy.commit {
                add(
                        R.id.frame_colombia_soad, ColombiaSoadInfoFragment.get(
                        ColombiaSoadInfoModel(pinview_soad_placa.value.toUpperCase(Locale.getDefault()))
                )
                )
                hide(this@ColombiaSoadPlacaFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        } else {
            showError(getString(R.string.error_soad_placa))
        }
    }

    override fun showProgress() {
        (activity as ContainerActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ContainerActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ContainerActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ContainerActivity).showSuccess(msg)
    }

    override fun onDataEntered(pinview: Pinview?, fromUser: Boolean) {
        b_soad_placa_continuar.isChecked = (pinview != null && pinview.value.length == 6)
    }
}