package addcel.mobilecard.ui.usuario.password;

import androidx.annotation.NonNull;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 22/12/16.
 */
public interface PasswordContract {

    interface View extends ScreenView, Validator.ValidationListener {
        void clickActualizar();

        void onPasswordChanged(@NonNull String msg);
    }

    interface Presenter {
        void clearDisposables();

        void setPassword(String oldPass, String newPass);

        void saveUsuario(String newPass, int newStatus);
    }
}
