package addcel.mobilecard.ui.usuario.scanpay.result.qr;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import addcel.mobilecard.utils.BundleBuilder;
import es.dmoral.toasty.Toasty;

/**
 * ADDCEL on 11/09/18.
 */
public class ScanResultQrFragment extends Fragment implements ScanResultQrContract.View {

    @Inject
    ScanResultQrContract.Presenter presenter;
    @Inject
    Activity activity;
    @Inject
    boolean fromNegocio;
    @Inject
    @Named("idPais")
    int idPais;

    private ImageView imgView;
    private TextView msgView;
    private ImageView qrView;

    public ScanResultQrFragment() {
    }

    public static synchronized ScanResultQrFragment get(SPReceiptEntity resultEntity,
                                                        boolean fromNegocio) {
        ScanResultQrFragment fragment = new ScanResultQrFragment();
        fragment.setArguments(new BundleBuilder().putParcelable("result", resultEntity)
                .putBoolean("fromNegocio", fromNegocio)
                .build());
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .scanResultQrSubcomponent(new ScanResultQrModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_lcpf_qrcode, container, false);
        TextView titleView = view.findViewById(R.id.view_qr_code_title);
        imgView = view.findViewById(R.id.h_qrcode);
        msgView = view.findViewById(R.id.view_qrcode_msg);
        qrView = view.findViewById(R.id.view_qrcode);
        view.findViewById(R.id.b_qrcode_share).setOnClickListener(v -> activity.finish());
        view.findViewById(R.id.b_qrcode_ok).setOnClickListener(v -> {
            startActivity(ScanPayActivity.get(view.getContext(), idPais));
            activity.finish();
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkIfScanOrNegocioAndSetFinished();
        imgView.setVisibility(View.GONE);
        Display display = Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        presenter.createQrCode(width - (width / 4), Color.BLACK, Color.TRANSPARENT);
        showSuccess(getString(R.string.txt_confirmacion_pago_msg));
    }

    @Override
    public void onDestroy() {
        presenter.clearCompositeDisposable();
        super.onDestroy();
    }

    @Override
    public void setQrCode(Bitmap bitmap) {
        qrView.setImageBitmap(bitmap);
    }

    @Override
    public void showError(@NonNull String msg) {
        Toasty.error(activity, msg).show();
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        msgView.setText(presenter.buildMsg(msg));
    }

    private void checkIfScanOrNegocioAndSetFinished() {
        if (fromNegocio) {
            ((NegocioCobroActivity) activity).setStreenStatus(ContainerScreenView.STATUS_FINISHED);
        } else {
            ((ScanPayActivity) activity).setFinished(Boolean.TRUE);
        }
    }
}
