package addcel.mobilecard.ui.usuario.blackstone.products;

import android.os.Bundle;

import java.util.Objects;

import addcel.mobilecard.data.net.blackstone.BlackstoneService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 09/07/18.
 */
@Module
public class BSProductModule {
    private final BSProductFragment fragment;

    BSProductModule(BSProductFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    BlackstoneActivity provideActivity() {
        return (BlackstoneActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Bundle provideArguments() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    BlackstoneService provideService(Retrofit retrofit) {
        return retrofit.create(BlackstoneService.class);
    }

    @PerFragment
    @Provides
    BSProductAdapter provideAdapter() {
        return new BSProductAdapter();
    }

    @PerFragment
    @Provides
    BSProductContract.Presenter providePresenter(BlackstoneService service,
                                                 Bundle bundle) {
        return new BSProductPresenter(service, bundle.getParcelable("carrier"), fragment);
    }
}
