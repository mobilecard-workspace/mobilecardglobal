package addcel.mobilecard.ui.usuario.mymobilecard.tebca

import addcel.mobilecard.R
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_tebca_form.*

/**
 * ADDCEL on 2019-08-12.
 */

@Parcelize
data class TebcaFormFragmentModel(val nombre: String, val paterno: String, val materno: String) :
        Parcelable

class TebcaFormFragment : Fragment() {

    companion object {
        fun get(model: TebcaFormFragmentModel): TebcaFormFragment {
            val args = BundleBuilder().putParcelable("model", model).build()
            val frag = TebcaFormFragment()
            frag.arguments = args
            return frag
        }
    }

    lateinit var model: TebcaFormFragmentModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_tebca_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AndroidUtils.setText(til_pe_negocio_registro_nombres.editText, model.nombre)
        AndroidUtils.setText(til_pe_negocio_registro_paterno.editText, model.paterno)
        AndroidUtils.setText(til_pe_negocio_registro_materno.editText, model.materno)

    }
}