package addcel.mobilecard.ui.usuario.main.pais.recarga;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.RecargaUseCaseObject;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * ADDCEL on 11/12/17.
 */
public class RecargaPaisFragment extends Fragment implements RecargaPaisContract.View {
    @BindView(R.id.recycler_pais)
    RecyclerView paisView;

    @Inject
    MainActivity mainActivity;
    @Inject
    RecargaPaisContract.Presenter presenter;
    @Inject
    RecargaPaisAdapter adapter;
    private Unbinder unbinder;

    public RecargaPaisFragment() {
    }

    public static synchronized RecargaPaisFragment get(List<PaisResponse.PaisEntity> paises) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("paises", (ArrayList<? extends Parcelable>) paises);
        RecargaPaisFragment paisFragment = new RecargaPaisFragment();
        paisFragment.setArguments(bundle);
        return paisFragment;
    }

    public static synchronized RecargaPaisFragment get(CatalogoResponse.RecargaEntity carrier,
                                                       List<CatalogoResponse.MontoEntity> montos, List<PaisResponse.PaisEntity> paises) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("carrier", carrier);
        bundle.putParcelableArrayList("montos", (ArrayList<? extends Parcelable>) montos);
        bundle.putParcelableArrayList("paises", (ArrayList<? extends Parcelable>) paises);
        RecargaPaisFragment paisFragment = new RecargaPaisFragment();
        paisFragment.setArguments(bundle);
        return paisFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .recargaPaisSubcomponent(new RecargaPaisModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_country_selection, container, false);
        unbinder = ButterKnife.bind(this, view);
        paisView.setAdapter(adapter);
        paisView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(paisView).setOnItemClickListener((recyclerView, position, v) -> {
            RecyclerView.ViewHolder vh = recyclerView.getChildViewHolder(v);
            if (vh instanceof RecargaPaisAdapter.ItemViewHolder) {
                onItemClicked(position);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mainActivity.setToolbarTitle(R.string.txt_nav_recargas_us);
        mainActivity.setBottomMenuUseCase(UseCase.PAISES);
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(paisView);
        paisView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (mainActivity != null) mainActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        if (mainActivity != null) mainActivity.hideProgress();
    }

    @Override
    public void onItemClicked(int pos) {
        PaisResponse.PaisEntity model = adapter.getItem(pos);
        if (model.getId() != 11) {
            presenter.getCategorias(model);
        } else {
            startActivity(new Intent(mainActivity, BlackstoneActivity.class));
        }
    }

    @Override
    public void onCategorias(List<CatalogoResponse.RecargaEntity> categorias,
                             PaisResponse.PaisEntity paisModel) {
        if (isVisible()) {
            RecargaUseCaseObject useCaseObject = new RecargaUseCaseObject(categorias, paisModel);
            startActivity(PagoContainerActivity.get(mainActivity, useCaseObject));
        }
    }

    @Override
    public void onError(String msg) {
        mainActivity.showError(msg);
    }
}
