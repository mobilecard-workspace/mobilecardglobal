package addcel.mobilecard.ui.usuario.aci;

import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.ui.usuario.aci.list.AciListModule;
import addcel.mobilecard.ui.usuario.aci.list.AciListSubcomponent;
import addcel.mobilecard.ui.usuario.aci.nodata.AciPagoDatosModule;
import addcel.mobilecard.ui.usuario.aci.nodata.AciPagoDatosSubcomponent;
import addcel.mobilecard.ui.usuario.aci.pago.AciPagoModule;
import addcel.mobilecard.ui.usuario.aci.pago.AciPagoSubcomponent;
import dagger.Subcomponent;

/**
 * ADDCEL on 22/03/17.
 */
@PerActivity
@Subcomponent(modules = AciModule.class)
public interface AciSubcomponent {
    void inject(AciActivity aciActivity);

    AciListSubcomponent aciServiceSubcomponent(AciListModule module);

    AciPagoSubcomponent aciPagoSubcomponent(AciPagoModule module);

    AciPagoDatosSubcomponent aciPagoDatosSubcomponent(AciPagoDatosModule module);
}
