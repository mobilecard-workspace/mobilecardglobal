package addcel.mobilecard.ui.usuario.viamericas.confirmation

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.usuario.viamericas.ViamericasTransactionElementAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 27/03/17.
 */

@Module
class ViamericasConfirmModule(private val fragment: ViamericasConfirmFragment) {

    @PerFragment
    @Provides
    fun provideContainerActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideModel(): ViamericasConfirmModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideUsuario(session: SessionOperations): Usuario {
        return session.usuario
    }

    @PerFragment
    @Provides
    fun provideAdapter(): ViamericasTransactionElementAdapter {
        return ViamericasTransactionElementAdapter()
    }

    @PerFragment
    @Provides
    internal fun provideTokenizer(retrofit: Retrofit): TokenizerAPI {
        return TokenizerAPI.provideTokenizerAPI(retrofit)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            viamericasService: ViamericasService, tokenizerAPI: TokenizerAPI
    ): ViamericasConfirmPresenter {
        return ViamericasConfirmPresenterImpl(
                viamericasService,
                tokenizerAPI,
                CompositeDisposable(),
                fragment
        )
    }
}


@PerFragment
@Subcomponent(modules = [ViamericasConfirmModule::class])
interface ViamericasConfirmSubcomponent {
    fun inject(fragment: ViamericasConfirmFragment)
}
