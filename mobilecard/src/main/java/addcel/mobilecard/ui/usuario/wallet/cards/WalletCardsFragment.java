package addcel.mobilecard.ui.usuario.wallet.cards;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.base.Strings;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.wallet.AptoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.MovementEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.apto.AptoIntroActivity;
import addcel.mobilecard.ui.usuario.apto.AptoIntroModel;
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingModel;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingResult;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormModel;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import addcel.mobilecard.ui.usuario.wallet.WalletUpdateEvent;
import addcel.mobilecard.ui.usuario.wallet.create.WalletCreateFragment;
import addcel.mobilecard.ui.usuario.wallet.transactions.WalletTransactionsFragment;
import addcel.mobilecard.ui.usuario.wallet.update.WalletUpdateFragment;
import addcel.mobilecard.utils.CreditCardFormatUtil;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 09/10/17.
 */
public final class WalletCardsFragment extends Fragment implements WalletCardsContract.View {
    @BindView(R.id.recycler_wallet_cards)
    RecyclerView cardsView;

    @BindView(R.id.b_wallet_cards_edit)
    ImageButton editButton;

    @BindDrawable(R.drawable.b_wallet_guardar)
    Drawable imgGuardar;

    @BindDrawable(R.drawable.b_wallet_editar)
    Drawable imgEditar;

    @Inject
    WalletContainerActivity activity;
    @Inject
    WalletCardsContract.Presenter presenter;
    @Inject
    WalletCardsAdapter adapter;
    @Inject
    Bus bus;

    private Unbinder unbinder;

    public WalletCardsFragment() {
    }

    public static WalletCardsFragment get() {
        return new WalletCardsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .walletCardsSubcomponent(new WalletCardsModule(this))
                .inject(this);
        bus.register(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.view_wallet_cards, container, false);
        unbinder = ButterKnife.bind(this, view);

        adapter.attach(this);
        cardsView.setAdapter(adapter);
        cardsView.setLayoutManager(new LinearLayoutManager(getContext()));

        ItemClickSupport.addTo(cardsView).setOnItemClickListener((recyclerView, position, v) -> {
            CardEntity card = adapter.getItem(position);
            boolean editable = adapter.isEditable();
            evalCard(card, editable);
        });
        return view;
    }

    private void evalCard(CardEntity card, boolean statusEditable) {
        if (!statusEditable) {
            String pan = AddcelCrypto.decryptHard(card.getPan());
            if (CreditCardFormatUtil.isValidCard(pan)) {
                launchEdit(card);
            } else {
                showError("Este plástico no está autorizado en nuestro sistema");
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);


        adapter.notifyDataSetChanged();
        activity.toolbar.setTitle(R.string.txt_menu_wallet);

        if (adapter.getItemCount() == 0) {
            presenter.list();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
    }

    @Override
    public void onDestroyView() {
        adapter.detach();
        ItemClickSupport.removeFrom(cardsView);
        cardsView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        activity.getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showError(String msg) {
        Toasty.error(activity, msg).show();
    }

    @Override
    public void showError(int resId) {
        Toasty.error(activity, getString(resId)).show();
    }

    @Override
    public void showSuccess(String msg) {
        Toasty.success(activity, msg).show();
    }

    @OnClick(R.id.b_wallet_cards_add)
    @Override
    public void clickAdd() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .addToBackStack(null)
                .hide(this)
                .add(R.id.container_wallet, new WalletCreateFragment())
                .commit();
    }

    @OnClick(R.id.b_wallet_cards_edit)
    @Override
    public void clickEdit() {
        clickEditImpl();
    }

    private void clickEditImpl() {
        new Handler().postDelayed(() -> {
            boolean editable = adapter.isEditable();
            editButton.setImageDrawable(editable ? imgEditar : imgGuardar);
            adapter.setEditable(!editable);
        }, 250);
    }

    @Override
    public void clickAllTransactions() {
        presenter.getLastMovements();
    }

    @Override
    public void launchEdit(CardEntity model) {
        if (isVisible()) {
            if (!Strings.isNullOrEmpty(model.getAccountId())) {
                showProgress();
                MyMcRoutingModel rModel = new MyMcRoutingModel(presenter.getUsuario().getIdeUsuario(), presenter.getUsuario().getIdPais());
                startActivityForResult(MyMcRoutingActivity.Companion.get(activity, rModel), MyMcRoutingActivity.REQUEST_CODE);
            } else {
                if (model.getPrevivale() || model.getMobilecard()) {
                    startActivity(MyMobilecardActivity.get(activity, model));
                } else {
                    activity.getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                            .addToBackStack(null)
                            .hide(this)
                            .add(R.id.container_wallet, WalletUpdateFragment.get(model))
                            .commit();
                }
            }
        }
    }

    @Override
    public void clickDelete(CardEntity model) {
        if (model.getDeterminada()) {
            showError(R.string.error_wallet_default_edit);
        } else {
            new AlertDialog.Builder(activity).setTitle(R.string.txt_wallet_eliminar)
                    .setMessage(
                            Phrase.from(WalletCardsFragment.this.getString(R.string.txt_wallet_eliminar_msg))
                                    .put("terminacion", presenter.getLast4Digits(model))
                                    .format())
                    .setPositiveButton(android.R.string.yes,
                            (dialogInterface, i) -> presenter.deleteCard(model))
                    .show();
        }
    }

    @Override
    public void clickFav(CardEntity model) {
        presenter.favCard(model);
    }

    @Override
    public void onCardsUpdated(List<CardEntity> models) {
        adapter.update(models);
        editButton.setImageDrawable(imgEditar);
    }

    @Subscribe
    @Override
    public void postCardEvent(WalletUpdateEvent event) {
        adapter.update(event.getModels());
    }

    @Override
    public void onMovementsReceived(List<MovementEntity> movementEntities) {
        if (isVisible()) {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .addToBackStack(null)
                    .hide(this)
                    .add(R.id.container_wallet, WalletTransactionsFragment.get(movementEntities))
                    .commit();
        }
    }

    @Override
    public void onTebcaFormResult(int resultCode, @org.jetbrains.annotations.Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                List<CardEntity> resultCards =
                        data.getParcelableArrayListExtra(TebcaFormActivity.DATA_CARD_LIST);
                if (resultCards != null) {
                    onCardsUpdated(resultCards);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == TebcaFormActivity.REQUEST_CODE) {
            onTebcaFormResult(resultCode, data);
        } else if (requestCode == MyMcRoutingActivity.REQUEST_CODE) {
            onMyMcRoutingActivityResult(resultCode, data);
        }
    }

    @Override
    public void onMyMcRoutingActivityResult(int resultCode, @NotNull Intent data) {
        hideProgress();
        if (resultCode == Activity.RESULT_OK) {
            MyMcRoutingResult result = data.getParcelableExtra(MyMcRoutingActivity.RESULT_DATA);
            if (result != null) {
                switch (result.getIdPais()) {
                    case PaisResponse.PaisEntity.MX:
                        onPrevivale(result.getMobileCard());
                        break;
                    case PaisResponse.PaisEntity.CO:
                        onPowie(result.getMobileCard());
                        break;
                    case PaisResponse.PaisEntity.US:
                        onApto(result.getUsaAptoInfo(), result.getMobileCard());
                        break;
                    case PaisResponse.PaisEntity.PE:
                        onTebca(result.getMobileCard());
                        break;
                }
            } else {
                showError("No se consultó la tarjeta MobileCard");
            }
        } else {
            showError("No se consultó la tarjeta MobileCard");
        }
    }

    @Override
    public void onPrevivale(@org.jetbrains.annotations.Nullable CardEntity card) {
        if (card != null)
            startActivity(MyMobilecardActivity.get(activity, card));
        else
            startActivity(MyMobilecardActivity.get(activity));
    }

    @Override
    public void onPowie(@org.jetbrains.annotations.Nullable CardEntity card) {
        showError("Próximamente disponible. Enero 2020");
    }

    @Override
    public void onApto(@org.jetbrains.annotations.Nullable AptoResponse aptoInfo, @org.jetbrains.annotations.Nullable CardEntity card) {
        startActivity(AptoIntroActivity.Companion.get(activity, new AptoIntroModel(card, aptoInfo)));
    }

    @Override
    public void onTebca(@org.jetbrains.annotations.Nullable CardEntity card) {
        if (card != null) {
            startActivity(MyMobilecardActivity.get(activity, card));
        } else {
            TebcaFormModel model1 = new TebcaFormModel(
                    presenter.getUsuario().getEMail(),
                    presenter.getUsuario().getIdeUsuario(),
                    presenter.getUsuario().getUsrTelefono(),
                    presenter.getUsuario().getUsrNombre(),
                    presenter.getUsuario().getUsrApellido(),
                    presenter.getUsuario().getUsrMaterno(), "",
                    "", "", "", "", TipoTarjetaEntity.CREDITO);

            startActivityForResult(TebcaFormActivity.Companion.get(activity, model1), TebcaFormActivity.REQUEST_CODE);
        }
    }
}
