package addcel.mobilecard.ui.usuario.main;

import android.annotation.SuppressLint;

import com.google.firebase.iid.FirebaseInstanceId;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.McConstants;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.PushResponse;
import addcel.mobilecard.data.net.usuarios.model.UserValidation;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.domain.usuario.menu.MenuInteractor;
import addcel.mobilecard.domain.usuario.menu.MenuUseCase;
import addcel.mobilecard.domain.usuario.menu.MenuValidationEvent;
import addcel.mobilecard.utils.StringUtil;
import timber.log.Timber;

/**
 * ADDCEL on 05/07/18.
 */
class MainPresenter implements MainContract.Presenter {
    public static long SELECTED_FRAGMENT = -1;
    private final MenuInteractor interactor;
    private final MainContract.View view;

    MainPresenter(MenuInteractor interactor, MainContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @Override
    public String getPaisFormatJUMIO(int idPais) {
        switch (idPais) {
            case 1:
                return "MEX";
            case 2:
                return "COL";
            case 3:
                return "USA";
            case 4:
                return "PER";
        }
        return "MEX";
    }

    @Override
    public boolean isMenuLaunched() {
        return interactor.isMenuLaunched();
    }

    @Override
    public void setMenuLaunched(boolean launched) {
        interactor.setMenuLaunched(launched);
    }

    @Override
    public void onRestart() {
        try {
            view.setNameOnDrawer(getUsuarioName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setDrawerByCountry();
    }

    @Override
    public void loadProfile() {
        interactor.loadProfile();
    }

    @Override
    public void launchOnLocationChecked(int useCase, boolean locationEnabled) {
        if (locationEnabled) {
            switch (useCase) {
                case MainContract.UseCase.CHEQUE:
                    view.launchIngoActivity();
                    break;
                case MainContract.UseCase.TRANSFERENCIAS:
                    view.launchVmActivity();
                    break;
            }
        } else {
            view.goToLocationSettings();
        }
    }

    @Override
    public void checkIfIngoAlive() {
        view.showProgress();
        interactor.checkIfIngoActive(new InteractorCallback<IngoAliveResponse>() {
            @Override
            public void onSuccess(@NotNull IngoAliveResponse result) {
                view.hideProgress();
                view.setUsaDrawer(true);
                interactor.postCountryUpdateEvent(true);
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.setUsaDrawer(false);
                interactor.postCountryUpdateEvent(false);
            }
        });
    }

    @Override
    public void checkIfIngoAlive(InteractorCallback<IngoAliveResponse> interactorCallback) {
        interactor.checkIfIngoActive(interactorCallback);
    }

    @Override
    public Usuario getUsuario() {
        Usuario temp = interactor.getUsuario();
        //SETEAMOS IMAGEN DE TARJETA A VACIO
        temp.setImg("");
        return temp;
    }

    @Override
    public long getIdUsuario() {
        return interactor.getIdUsuario();
    }

    @Override
    public String getUsuarioName() throws NullPointerException {
        if (interactor == null) throw new NullPointerException("El interactor no puede ser NULL");
        if (interactor.getUsuarioName() == null)
            throw new NullPointerException("El nombre del usuario no puede ser NULL");

        return interactor.getUsuarioName().toUpperCase(Locale.getDefault());
    }

    @Override
    public String getUsuarioApellidos() throws NullPointerException {

        if (interactor == null) throw new NullPointerException("El interactor no puede ser NULL");
        if (interactor.getUsuarioApellidos() == null)
            throw new NullPointerException("El apellido del usuario no puede ser NULL");

        return interactor.getUsuarioApellidos().toUpperCase(Locale.getDefault());
    }

    @Override
    public String getUsuarioProfile() {
        return interactor.getUsuarioProfilePic();
    }

    @Override
    public String getEmail() {
        return interactor.getEmail();
    }

    @Override
    public int getIdPais() {
        return interactor.getIdPais();
    }

    @Override
    public void verifyAndLaunchUseCase(int useCase) {
        view.showProgress();
        interactor.verifyUser(useCase, BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<MenuValidationEvent>() {
                    @Override
                    public void onSuccess(@NotNull MenuValidationEvent result) {
                        view.hideProgress();
                        evalUser(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void setDrawerByCountry() {
        int idPais = interactor.getIdPais();
        switch (idPais) {
            case McConstants.PAIS_ID_MX:
                view.setMexicoDrawer();
                interactor.postCountryUpdateEvent(false);
                break;
            case McConstants.PAIS_ID_CO:
                view.setColombiaDrawer();
                interactor.postCountryUpdateEvent(false);
                break;
            case McConstants.PAIS_ID_USA:
                view.setUsaDrawer(true);
                interactor.postCountryUpdateEvent(true);
                break;
            case McConstants.PAIS_ID_PE:
                view.setPeruDrawer();
                interactor.postCountryUpdateEvent(false);
                break;
        }
    }

    @Override
    public void onLogged(int useCase) {
        switch (useCase) {
            case MainContract.UseCase.INICIO:
                view.startFragmentFromMain(useCase, R.string.nav_home);
                break;
            case MainContract.UseCase.PROFILE:
                view.launchProfileActivity();
                break;
            case MainContract.UseCase.RECARGAS:
                getRecargas();
                break;
            case MainContract.UseCase.SERVICIOS:
                getServicios();
                break;
            case MainContract.UseCase.WALLET:
                view.launchWalletActivity();
                break;
            case MainContract.UseCase.EDOCUENTA:
                view.launchEdoCuentaActivity();
                break;
            case MainContract.UseCase.CHEQUE:
                view.checkLocationSettings(useCase);
                break;
            case MainContract.UseCase.TRANSFERENCIAS:
                launchTransfers();
                break;
            case MainContract.UseCase.CONTACTO:
                view.launchContactoActivity(getIdUsuario(), getEmail(), getIdPais());
                break;
            case MainContract.UseCase.FRECUENTES:
                view.startFragmentFromMain(useCase, R.string.txt_nav_frecuentes);
                break;
            case MainContract.UseCase.SCAN:
                view.launchScanPayActivity();
                break;
            case MainContract.UseCase.MY_MC:
                view.launchMyMcActivity();
                break;
            case MainContract.UseCase.COL_PEAJE:
                view.launchColombiaTelepeaje();
                break;
            case MainContract.UseCase.COL_SOAT:
                view.launchColombiaSoad();
                break;
            case MainContract.UseCase.COL_TAE:
                view.launchColombiaActivity();
                break;
            case MainContract.UseCase.COL_PREPAGOS:
                view.launchColombiaServicios(MainContract.UseCase.COL_PREPAGOS);
                break;
            case MainContract.UseCase.COL_SERVICIOS:
                view.launchColombiaServicios(MainContract.UseCase.SERVICIOS);
                break;
            case MainContract.UseCase.MOBILE_TAG:
                view.launchMobileTagActivity();
                break;
        }
    }

    @Override
    public void onEmailAuthRequired(UserValidation validation) {
        view.showSuccess(validation.getMensajeError());
        view.showResendDialog();
    }

    @Override
    public void onSmsAuthRequired(UserValidation validation) {
        view.showSuccess(validation.getMensajeError());
        view.launchSmsactivity(validation.getIdUSuario(), validation.getIdPais());
    }

    @Override
    public void evalJumioUser(MenuValidationEvent event) {
        switch (event.getValidation().getUsrJumio()) {
            case McConstants.JUMIO_STATUS_NOT_ENROLLED:
                view.launchOnBoardingInfo(event.getValidation().getIdUSuario());
                break;
            case McConstants.JUMIO_STATUS_ENROLLED:
                evalUser(event);
                break;
            case McConstants.JUMIO_STATUS_REJECTED:
                view.showOnboardingErrorDialog(getIdUsuario());
                break;
            default:
                view.showError(event.getValidation().getMensajeError());
                break;
        }
    }

    @Override
    public void evalJumioUserBottom(UserValidation validation, UseCase useCase) {
        if (validation.getIdError() == 0) {
            switch (validation.getUsrJumio()) {
                case McConstants.JUMIO_STATUS_NOT_ENROLLED:
                    view.launchOnBoardingInfo(validation.getIdUSuario());
                    break;
                case McConstants.JUMIO_STATUS_ENROLLED:
                    processMenuEvent(validation, useCase);
                    break;
                case McConstants.JUMIO_STATUS_REJECTED:
                    view.showOnboardingErrorDialog(getIdUsuario());
                    break;
                default:
                    view.showError(validation.getMensajeError());
                    break;
            }
        } else {
            view.showError(validation.getMensajeError());
        }
    }

    @Override
    public void updateToken(String token) {
        interactor.updateToken(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), token);
    }

    @Override
    public void deleteToken(String token) {
        interactor.deleteToken(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), token, new InteractorCallback<PushResponse>() {
            @Override
            public void onSuccess(@NotNull PushResponse result) {
                onLogout();
            }

            @Override
            public void onError(@NotNull String error) {
                onLogout();
            }
        });
    }

    @Override
    public void evalUser(MenuValidationEvent event) {
        switch (event.getValidation().getIdUsrStatus()) {
            case McConstants.USER_STATUS_ACTIVO:
                onLogged(event.getUseCase());
                break;
            case McConstants.USER_STATUS_EMAIL_VERIFICATION:
                onEmailAuthRequired(event.getValidation());
                break;
            case McConstants.USER_STATUS_SMS_VERIFICATION:
                onSmsAuthRequired(event.getValidation());
                break;
            default:
                view.showError(event.getValidation().getMensajeError());
                break;
        }
    }

    @Override
    public void resendActivationLink() {
        view.showProgress();
        interactor.resendEmailAuth(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<DefaultResponse>() {
                    @Override
                    public void onSuccess(@NotNull DefaultResponse result) {
                        view.hideProgress();
                        view.hideResendDialog();
                        view.showSuccess(result.getMensajeError());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getRecargas() {
        switch (interactor.getIdPais()) {
            case PaisResponse.PaisEntity
                    .MX:
                getRecargasMx();
                break;
            case PaisResponse.PaisEntity
                    .CO:
                view.launchColombiaActivity();
                break;
            case PaisResponse.PaisEntity
                    .US:
                //getPaises();
                view.showError("Disponible a partir de febrero 2020");
                break;
            case PaisResponse.PaisEntity
                    .PE:
                view.showError("Próximamente disponible. Enero 2020");
                break;

        }
    }

    private void getRecargasMx() {
        view.showProgress();
        interactor.getRecargasMx(MenuUseCase.MX_RECARGAS, BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<CatalogoResponse.RecargaEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<CatalogoResponse.RecargaEntity> result) {
                        view.hideProgress();
                        view.launchContainerActivityRecargasMX(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    private void getPaises() {
        view.showProgress();
        interactor.getTelefonicaPaises(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<PaisResponse.PaisEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<PaisResponse.PaisEntity> result) {
                        view.hideProgress();
                        view.launchContainerActivityRecargasUS(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getServicios() {
        switch (interactor.getIdPais()) {
            case McConstants.PAIS_ID_MX:
                getCategoriasMX();
                break;
            case McConstants.PAIS_ID_CO:
                view.launchColombiaServicios(MainContract.UseCase.SERVICIOS);
                break;
            case McConstants.PAIS_ID_USA:
                view.showError("Disponible a partir de febrero 2020");
                break;
            default:
                break;
        }
    }

    private void getCategoriasMX() {
        view.showProgress();
        interactor.getCategoriasMx(MainContract.UseCase.SERVICIOS, BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<CatalogoResponse.CategoriaEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<CatalogoResponse.CategoriaEntity> result) {
                        view.hideProgress();
                        view.launchContainerActivityServicios(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    private void launchTransfers() {
        switch (getIdPais()) {
            case McConstants.PAIS_ID_MX:
                view.launchHTHActivity();
                break;
            case McConstants.PAIS_ID_CO:
            case McConstants.PAIS_ID_PE:
                view.showError("Próximamente disponible. Enero 2020");
                break;
            case McConstants.PAIS_ID_USA:
                view.checkLocationSettings(MainContract.UseCase.TRANSFERENCIAS);
                break;
        }
    }

    @Override
    public void updateReference(String reference) {
        view.showProgress();
        interactor.updateReference(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                interactor.getIdPais(), reference, new InteractorCallback<McResponse>() {
                    @Override
                    public void onSuccess(@NotNull McResponse result) {
                        view.hideProgress();
                        view.showSuccess(result.getMensajeError());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void logout() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Timber.e(task.getException(), "Error al obtener instanceId");
                onLogout();
                return;
            }

            if (task.getResult() != null) {
                Timber.d("Token push: %s", task.getResult().getToken());
                deleteToken(task.getResult().getToken());
            }

        });
    }

    @Override
    public void onLogout() {
        interactor.logout(new InteractorCallback<Integer>() {
            @Override
            public void onSuccess(@NotNull Integer result) {
                view.logout();
            }

            @Override
            public void onError(@NotNull String error) {
                view.logout();
            }
        });
    }

    @Override
    public void processMenuEvent(UserValidation result, UseCase useCase) {
        if (result.getIdError() == 0) {
            int status = result.getIdUsrStatus();
            if (status == 1) {
                if (useCase.equals(UseCase.HOME) && !view.getBottomMenuUseCase().equals(UseCase.HOME)) {
                    view.goTo(MainContract.UseCase.INICIO);
                } else if (useCase.equals(UseCase.WALLET)) {
                    view.launchWalletActivity();
                } else if (useCase.equals(UseCase.CONTACTO) && !view.getBottomMenuUseCase()
                        .equals(UseCase.CONTACTO)) {
                    view.goTo(MainContract.UseCase.CONTACTO);
                } else if (useCase.equals(UseCase.HISTORIAL)) {
                    view.launchEdoCuentaActivity();
                }
            } else if (status == 99) {
                view.showSuccess(result.getMensajeError());
                view.showResendDialog();
            } else if (status == 100) {
                view.showSuccess(result.getMensajeError());
                view.launchSmsactivity(result.getIdUSuario(), result.getIdPais());
            } else {
                view.showError(result.getMensajeError());
            }
        } else {
            view.showError(result.getMensajeError());
        }
    }
}
