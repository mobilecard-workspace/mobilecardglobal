package addcel.mobilecard.ui.usuario.wallet;

import java.util.List;

import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 10/10/17.
 */
public final class WalletUpdateEvent implements BusEvent {
    private final List<CardEntity> models;

    WalletUpdateEvent(List<CardEntity> models) {
        this.models = models;
    }

    public List<CardEntity> getModels() {
        return models;
    }
}
