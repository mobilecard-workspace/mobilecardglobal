package addcel.mobilecard.ui.usuario.main.contacto;

import java.util.Locale;

import addcel.mobilecard.data.net.usuarios.model.Usuario;

/**
 * ADDCEL on 22/11/16.
 */
public class ContactoPresenter implements ContactoContract.Presenter {

    private final ContactoContract.View view;
    private final Usuario usuario;

    ContactoPresenter(ContactoContract.View view, Usuario usuario) {
        this.view = view;
        this.usuario = usuario;
    }

    @Override
    public String buildEmailSubject() {
        return String.format(Locale.getDefault(), "%s: %s - %s", "ANDROID", usuario.getUsrLogin(),
                usuario.getIdeUsuario());
    }

    @Override
    public void lanzaAccion(int position) {
        switch (position) {
            case 0:
                view.llamanos();
                break;
            case 1:
                view.escribenos();
                break;
            case 2:
                view.lanzaMobilecard();
                break;
            case 3:
                view.lanzaFacebook();
                break;
            case 4:
                view.lanzaTwitter();
                break;
            case 5:
                view.lanzaYoutube();
                break;
            case 6:
                view.lanzaLinkedIn();
                break;
            default:
                break;
        }
    }
}
