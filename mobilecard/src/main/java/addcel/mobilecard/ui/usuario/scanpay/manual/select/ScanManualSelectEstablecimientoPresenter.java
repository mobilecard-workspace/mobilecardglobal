package addcel.mobilecard.ui.usuario.scanpay.manual.select;

import android.annotation.SuppressLint;

import com.squareup.otto.Subscribe;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoDownloadEvent;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoErrorEvent;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoFilterEvent;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 22/08/18.
 */
public class ScanManualSelectEstablecimientoPresenter
        implements ScanManualSelectEstablecimientoContract.Presenter {
    private final ScanManualSelectEstablecimientoInteractor interactor;
    private final ScanManualSelectEstablecimientoContract.View view;

    ScanManualSelectEstablecimientoPresenter(ScanManualSelectEstablecimientoInteractor interactor,
                                             ScanManualSelectEstablecimientoContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void clearCompositeDisposable() {
        if (interactor.getCompositeDisposable() != null)
            interactor.getCompositeDisposable().clear();
    }

    @Override
    public void register() {
        interactor.getBus().register(this);
    }

    @Override
    public void unregister() {
        interactor.getBus().unregister(this);
    }

    @SuppressLint("CheckResult")
    @Override
    public void downloadEstablecimientos() {
        view.showProgress();
        interactor.downloadItems(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage());
    }

    @Subscribe
    @Override
    public void onEstablecimientosDownloaded(ScanManualSelectEstablecimientoDownloadEvent event) {
        view.hideProgress();
        view.onEstablecimientosDownloaded();
    }

    @Subscribe
    @Override
    public void onEstablecimientosError(ScanManualSelectEstablecimientoErrorEvent event) {
        view.hideProgress();
        view.onEstablecimientosError(event.getIdError());
    }

    @Override
    public void filterList(String pattern) {
        interactor.filterItems(pattern);
    }

    @Subscribe
    @Override
    public void onEstablecimientosFiltered(ScanManualSelectEstablecimientoFilterEvent event) {
        view.setEstablecimientos(event.getEstablecimientos());
    }
}
