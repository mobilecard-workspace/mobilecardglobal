package addcel.mobilecard.ui.usuario.colombia.telepeaje.menu

import addcel.mobilecard.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

/**
 * ADDCEL on 9/25/19.
 */
class ColombiaTelepeajeMenuAdapter :
        RecyclerView.Adapter<ColombiaTelepeajeMenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return ColombiaTelepeajeModel.values().size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = ColombiaTelepeajeModel.values()[position]
        holder.logo.setImageResource(model.logoWhite)
        holder.text.text = model.text
    }

    fun getItem(pos: Int): ColombiaTelepeajeModel {
        return ColombiaTelepeajeModel.values()[pos]
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val logo: ImageView = view.findViewById(R.id.img_menu_item_icon)
        val text: TextView = view.findViewById(R.id.text_menu_item_name)
    }
}