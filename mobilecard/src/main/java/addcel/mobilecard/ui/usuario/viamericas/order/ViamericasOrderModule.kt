package addcel.mobilecard.ui.usuario.viamericas.order

import addcel.mobilecard.R
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.CurrencyModel
import addcel.mobilecard.data.net.viamericas.model.LocationModel
import addcel.mobilecard.data.net.viamericas.model.VmCountryModel
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.custom.extension.DetachableClickListener
import android.app.AlertDialog
import android.widget.ArrayAdapter
import com.google.android.material.textfield.TextInputLayout
import com.google.common.collect.Lists
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Named

/**
 * ADDCEL on 27/03/17.
 */

@Module
class ViamericasOrderModule(private val fragment: ViamericasOrderFragment) {

    @PerFragment
    @Provides
    fun provideActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideValidator(adapter: ViewDataAdapter<TextInputLayout, String>): Validator {
        val validator = Validator(fragment)
        validator.registerAdapter(TextInputLayout::class.java, adapter)
        validator.setValidationListener(fragment)
        return validator
    }

    @PerFragment
    @Provides
    fun provideModel(): ViamericasOrderModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideInfoDialog(activity: ContainerActivity): AlertDialog {
        return AlertDialog.Builder(activity).setTitle(
                android.R.string.dialog_alert_title
        )
                .setMessage(R.string.info_viamericas_1)
                .setPositiveButton(android.R.string.ok,
                        DetachableClickListener.wrap { dialogInterface, i -> dialogInterface.dismiss() })
                .create()
    }

    @PerFragment
    @Provides
    @Named("viamericasCountryAdapter")
    fun provideCountriesAdapter(activity: ContainerActivity): ArrayAdapter<VmCountryModel> {
        val countryAdapter =
                ArrayAdapter<VmCountryModel>(activity, android.R.layout.simple_spinner_item)
        countryAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        countryAdapter.setNotifyOnChange(true)
        return countryAdapter
    }

    @PerFragment
    @Provides
    fun provideCurrencies(): List<CurrencyModel> {
        return Lists.newArrayList()
    }

    @PerFragment
    @Provides
    fun provideModeAdapter(): ViamericasModeAdapter {
        return ViamericasModeAdapter()
    }

    @PerFragment
    @Provides
    @Named("viamericasBranchAdapter")
    fun provideBranchAdapter(activity: ContainerActivity): ArrayAdapter<LocationModel> {
        val branchAdapter =
                ArrayAdapter<LocationModel>(activity, android.R.layout.simple_spinner_item)
        branchAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        branchAdapter.setNotifyOnChange(true)
        return branchAdapter
    }

    @PerFragment
    @Provides
    fun provideLookup(): Map<String, Int> {
        return mapOf(
                "MEX" to 1,
                "COL" to 2,
                "ELS" to 3,
                "GUA" to 4,
                "RD" to 5,
                "NIC" to 6,
                "CCR" to 7
        )
    }

    @PerFragment
    @Provides
    fun providePresenter(
            service: ViamericasService,
            lookup: Map<String, Int>
    ): ViamericasOrderPresenter {
        return ViamericasOrderPresenterImpl(service, lookup, CompositeDisposable(), fragment)
    }
}


@PerFragment
@Subcomponent(modules = [ViamericasOrderModule::class])
interface ViamericasOrderSubcomponent {
    fun inject(fragment: ViamericasOrderFragment)
}
