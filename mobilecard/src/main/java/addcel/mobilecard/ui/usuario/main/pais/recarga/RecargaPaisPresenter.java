package addcel.mobilecard.ui.usuario.main.pais.recarga;

import android.annotation.SuppressLint;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 11/12/17.
 */
class RecargaPaisPresenter implements RecargaPaisContract.Presenter {
    private final CatalogoService catalogoService;
    private final Usuario usuario;
    private final CompositeDisposable disposables;
    private final RecargaPaisContract.View view;

    RecargaPaisPresenter(CatalogoService catalogoService, Usuario usuario,
                         RecargaPaisContract.View view) {
        this.catalogoService = catalogoService;
        this.usuario = usuario;
        this.disposables = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCategorias(PaisResponse.PaisEntity paisModel) {
        view.showProgress();
        disposables.add(catalogoService.getRecargas(BuildConfig.ADDCEL_APP_ID, usuario.getIdPais(),
                StringUtil.getCurrentLanguage(), ServiciosRequest.empty())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(catalogoResponse -> {
                    view.hideProgress();
                    if (catalogoResponse.getIdError() == 0) {
                        view.onCategorias(processRecargaList(catalogoResponse.getRecargas()), paisModel);
                    } else {
                        view.onError(catalogoResponse.getMensajeError());
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.onError(StringUtil.getNetworkError());
                }));
    }

    private ArrayList<CatalogoResponse.RecargaEntity> processRecargaList(List<CatalogoResponse.RecargaEntity> src) {
        if (usuario.getIdPais() == 3) {
            for (CatalogoResponse.RecargaEntity c : src) {
                if (c.getId() == 2)
                    src.remove(c);
            }

            /*return Lists.newArrayList(
                    Collections2.filter(src, new Predicate<CatalogoResponse.RecargaEntity>() {
                        @Override
                        public boolean apply(CatalogoResponse.@Nullable RecargaEntity input) {
                            return input != null && input.getId() != 2;
                        }
                    }));*/
        }
        return Lists.newArrayList(src);
    }
}
