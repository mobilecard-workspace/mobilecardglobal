package addcel.mobilecard.ui.usuario.scanpay.qr.confirm

import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.PurchaseScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureCallback

/**
 * ADDCEL on 2020-01-16.
 */
interface ScanQrConfirmView : PurchaseScreenView, BillPocketSecureCallback {
    fun getEmail(): String
    fun getMsi(): String
    fun setUiData()
    fun launchGetToken(profile: String)
    fun onTokenReceived(tokenEntity: TokenEntity)
    fun launchPago(tokenEntity: TokenEntity)
    fun onPagoSuccess(resultEntity: SPReceiptEntity)
}


interface ScanQrConfirmPresenter {
    fun fetchIdUsuario(): Long
    val idPais: Int
    val formaPago: CardEntity
    val comision: Double
    val monto: Double
    val propina: Double
    val concepto: String
    val establecimiento: String
    val formattedMonto: String
    val formattedPropina: String
    val formattedComision: String
    val formattedTotal: String
    val referenciaNegocio: String

    fun buildRequest(): SPPagoEntity
    fun buildRequestString(model: SPPagoEntity): String
    val jumioRef: String
    fun isOnWhiteList()

    fun getToken(profile: String)
    fun launchPago(tokenEntity: TokenEntity, request: SPPagoEntity)
}