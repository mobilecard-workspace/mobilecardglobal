package addcel.mobilecard.ui.usuario.colombia.recargas.datos

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.data.net.catalogo.OperadorRecarga
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 9/21/19.
 */
interface ColombiaRecargaPaquetesDatosPresenter {
    fun addToDisposables(disposable: Disposable)
    fun clearDisposables()
    fun getPaquetes(operador: OperadorRecarga)
}

class ColombiaRecargaPaqueteDatosPresenterImpl(
        val catalogo: CatalogoAPI,
        val disposables: CompositeDisposable, val view: ColombiaRecargaPaquetesDatosView
) :
        ColombiaRecargaPaquetesDatosPresenter {
    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getPaquetes(operador: OperadorRecarga) {
        view.showProgress()
        val dispPaquetes = catalogo.getCategoriasPuntored(
                BuildConfig.ADDCEL_APP_ID, 2,
                StringUtil.getCurrentLanguage(), operador.id
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    view.hideProgress()
                    if (r.idError == 0) view.onGetPaquetes(r.categorias)
                    else {
                        view.showError(r.mensajeError)
                        view.onGetPaquetesError()
                    }
                }, { t ->
                    view.hideProgress()
                    view.showError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                    view.onGetPaquetesError()
                })

        addToDisposables(dispPaquetes)
    }
}