package addcel.mobilecard.ui.usuario.edocuenta.list

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.wallet.Transaction
import addcel.mobilecard.data.net.wallet.TransactionResponse
import addcel.mobilecard.data.net.wallet.TransactionsPerMonth
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.historial.HistorialInteractor
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.edocuenta.EdocuentaActivity
import addcel.mobilecard.ui.usuario.edocuenta.receipt.EdocuentaReceiptFragment
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.LayoutAnimationController
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.transaction
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.screen_edocuenta_list.*
import javax.inject.Inject


/**
 * ADDCEL on 2019-06-12.
 */
interface View {

    fun showProgress()

    fun hideProgress()

    fun showError(msg: String)

    fun setFlattenedMovements(flattenedMovements: List<Any>)

    fun clickMovement(entity: Any)

    fun showReceipt(entity: Transaction)
}

interface Presenter {
    fun getMovements()

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun flattenMovements(response: TransactionResponse) // Modificarse cuando cambie a una sola lista
}

class HistorialListPresenter(val interactor: HistorialInteractor, val view: View) : Presenter {
    override fun flattenMovements(response: TransactionResponse) {
        val flatMoves: MutableList<Any> = ArrayList()
        flattenMovementsImpl(flatMoves, response.data)
        view.setFlattenedMovements(flatMoves)
    }

    private fun flattenMovementsImpl(objs: MutableList<Any>, tpm: List<TransactionsPerMonth>) {
        for (t: TransactionsPerMonth in tpm) {
            objs.add(t.mes)
            for (tr: Transaction in t.transacciones) {
                objs.add(tr)
            }
        }
    }

    override fun getMovements() {
        view.showProgress()
        interactor.getTransactions(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                object : InteractorCallback<TransactionResponse> {
                    override fun onSuccess(result: TransactionResponse) {
                        view.hideProgress()
                        flattenMovements(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun addToDisposables(disposable: Disposable) {
        interactor.addToDisposables(disposable)
    }

    override fun clearDisposables() {
        interactor.clearDisposables()
    }
}

class HistorialListFragment : Fragment(), View {

    @Inject
    lateinit var edoActivity: EdocuentaActivity
    @Inject
    lateinit var adapter: EdocuentaListAdapter
    @Inject
    lateinit var presenter: Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.edocuentaListSubcomponent(EdocuentaListModule(this))
                .inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): android.view.View? {
        return inflater.inflate(R.layout.screen_edocuenta_list, container, false)
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        screen_edocuenta_list.adapter = adapter
        screen_edocuenta_list.layoutManager = LinearLayoutManager(view.context)

        val resId: Int = R.anim.layout_animation
        val animation: LayoutAnimationController =
                android.view.animation.AnimationUtils.loadLayoutAnimation(view.context, resId)
        screen_edocuenta_list.layoutAnimation = animation

        ItemClickSupport.addTo(screen_edocuenta_list).setOnItemClickListener { _, position, _ ->
            val o = adapter.getItem(position)
            clickMovement(o)
        }

        if (adapter.itemCount == 0) {
            presenter.getMovements()
        }
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun showError(msg: String) {
        edoActivity.showError(msg)
    }

    override fun showProgress() {
        edoActivity.showProgress()
    }

    override fun hideProgress() {
        edoActivity.hideProgress()
    }

    override fun setFlattenedMovements(flattenedMovements: List<Any>) {
        presenter.addToDisposables(adapter.update(flattenedMovements))
    }

    override fun clickMovement(entity: Any) {
        if (entity is Transaction) showReceipt(entity)
    }

    override fun showReceipt(entity: Transaction) {
        fragmentManager?.commit {
            add(R.id.frame_edocuenta, EdocuentaReceiptFragment.get(entity))
            addToBackStack(null)
            hide(this@HistorialListFragment)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }
}