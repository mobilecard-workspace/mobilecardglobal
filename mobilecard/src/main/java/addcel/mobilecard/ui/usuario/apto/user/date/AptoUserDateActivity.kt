package addcel.mobilecard.ui.usuario.apto.user.date

import addcel.mobilecard.R
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog
import addcel.mobilecard.ui.usuario.apto.user.address.AptoUserAddressActivity
import addcel.mobilecard.ui.usuario.apto.user.address.AptoUserAddressModel
import addcel.mobilecard.utils.AndroidUtils
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_user_date.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

@Parcelize
data class AptoUserDateModel(
        val phone: String, val phoneVerification: String,
        val email: String, val emailVerification: String, val firstName: String, val lastName: String
) :
        Parcelable

interface AptoUserDateView : ScreenView, TextWatcher {
    fun launchContinue()
}

class AptoUserDateActivity : AppCompatActivity(), AptoUserDateView {
    override fun launchContinue() {
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(msg: String) {
    }

    override fun showSuccess(msg: String) {
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (til_shift_user_date_dob.hasFocus()) {
                if (p0.isEmpty()) {
                    til_shift_user_date_dob.error = "Pick a valid birth date"
                } else {
                    til_shift_user_date_dob.error = null
                }
            }

            if (til_shift_user_date_ssn.hasFocus()) {
                if (p0.matches(Regex.fromLiteral(SSN_REGEX)) || p0.matches(
                                Regex.fromLiteral(
                                        ITIN_REGEX
                                )
                        )
                ) {
                    til_shift_user_date_dob.error = "Enter a valid SSN / ITIN"
                } else {
                    til_shift_user_date_dob.error = null
                }
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    lateinit var datePickerDialog: DatePickerDialog
    lateinit var model: AptoUserDateModel
    private var dobDate: LocalDateTime = LocalDateTime.now()

    companion object {
        private val dobFormatterView: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        private val dobFormatterApi: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

        private const val SSN_REGEX =
                "^(?!219099999|078051120)(?!666|000|9\\d{2})\\d{3}(?!00)\\d{2}(?!0{4})\\d{4}\$"
        private const val ITIN_REGEX =
                "^(9\\d{2})([ \\-]?)(7\\d|8[0-8]|9[0-2]|9[4-9])([ \\-]?)(\\d{4})\$"

        fun get(context: Context, model: AptoUserDateModel): Intent {
            return Intent(context, AptoUserDateActivity::class.java).putExtra("model", model)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_user_date)

        model = intent?.getParcelableExtra("model")!!

        buildDatePickerDialog()
        datePickerDialog.show()
        til_shift_user_date_dob.setOnFocusChangeListener { _, p1 ->
            if (p1) {
                datePickerDialog.show()
            }
        }
        text_shift_user_date_dob.addTextChangedListener(this)
        text_shift_user_date_ssn.addTextChangedListener(this)

        b_shift_user_date_continuar.setOnClickListener {

            val pModel = AptoUserAddressModel(
                    model.phone,
                    model.phoneVerification,
                    model.email,
                    model.emailVerification,
                    model.firstName,
                    model.lastName,
                    dobFormatterApi.format(dobDate),
                    AndroidUtils.getText(text_shift_user_date_ssn)
            )

            startActivity(AptoUserAddressActivity.get(this@AptoUserDateActivity, pModel))

        }
    }

    private fun buildDatePickerDialog() {
        val listener = DatePickerDialog.OnDateSetListener { _, i, i1, i2 ->
            val time = LocalDateTime.of(i, i1 + 1, i2, 0, 0)
            dobDate = time
            AndroidUtils.setText(til_shift_user_date_dob.editText, dobFormatterView.format(dobDate))

        }

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            datePickerDialog =
                    FixedHoloDatePickerDialog(
                            this, listener, Calendar.getInstance().get(Calendar.YEAR) - 18,
                            Calendar.getInstance().get(Calendar.MONTH),
                            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                    )
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog = DatePickerDialog(
                        this, R.style.CustomDatePickerDialogTheme, listener,
                        Calendar.getInstance().get(Calendar.YEAR) - 18,
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                )
            } else {
                datePickerDialog =
                        DatePickerDialog(
                                this, listener, Calendar.getInstance().get(Calendar.YEAR) - 18,
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                        )
                datePickerDialog.datePicker.calendarViewShown = false
            }
        }
        datePickerDialog.setTitle("Pick date of birth")
    }
}
