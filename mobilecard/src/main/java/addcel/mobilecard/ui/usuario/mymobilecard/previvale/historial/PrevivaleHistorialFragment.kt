package addcel.mobilecard.ui.usuario.mymobilecard.previvale.historial

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.previvale.historial.PrevivaleTransaction
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_edocuenta_list.*
import org.threeten.bp.LocalDate
import javax.inject.Inject

/**
 * ADDCEL on 2020-01-27.
 */


@Parcelize
data class PrevivaleHistorialModel(val idUsuario: Long, val pan: String) : Parcelable

interface PrevivaleHistorialView : ScreenView {
    fun launchConsulta()
    fun setResults(movimientos: List<PrevivaleTransaction>)
    fun clickResult(position: Int)
}

class PrevivaleHistorialFragment : Fragment(), PrevivaleHistorialView {

    companion object {
        fun get(idUsuario: Long, pan: String): PrevivaleHistorialFragment {

            val model = PrevivaleHistorialModel(idUsuario, pan)
            val args = BundleBuilder().putParcelable("model", model).build()
            val frag = PrevivaleHistorialFragment()
            frag.arguments = args

            return frag
        }
    }

    @Inject
    lateinit var containerAct: MyMobilecardActivity
    @Inject
    lateinit var model: PrevivaleHistorialModel
    @Inject
    lateinit var presenter: PrevivaleHistorialPresenter

    private val adapter = PrevivaleHistorialAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.previvaleHistorialSubcomponent(PrevivaleHistorialModule(this)).inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_edocuenta_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        containerAct.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
        containerAct.setAppToolbarTitle(R.string.txt_wallet_movimientos)

        screen_edocuenta_list.adapter = adapter
        screen_edocuenta_list.layoutManager = LinearLayoutManager(view.context)
        screen_edocuenta_list.addItemDecoration(DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL))

        if (adapter.itemCount == 0) launchConsulta()
    }

    override fun launchConsulta() {
        containerAct.addToDisposables(presenter.getTransactions(model.idUsuario, model.pan, LocalDate.now()))
    }

    override fun setResults(movimientos: List<PrevivaleTransaction>) {
        adapter.update(movimientos)
    }

    override fun clickResult(position: Int) {

    }

    override fun showSuccess(msg: String) {
        containerAct.showSuccess(msg)
    }

    override fun showError(msg: String) {
        containerAct.showError(msg)
    }

    override fun hideProgress() {
        containerAct.hideProgress()
    }

    override fun showProgress() {
        containerAct.showProgress()
    }

}