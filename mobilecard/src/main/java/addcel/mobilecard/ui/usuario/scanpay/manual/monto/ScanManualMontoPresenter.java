package addcel.mobilecard.ui.usuario.scanpay.manual.monto;

import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 23/08/18.
 */
public class ScanManualMontoPresenter implements ScanManualMontoContract.Presenter {

    private final LcpfEstablecimiento establecimiento;

    public ScanManualMontoPresenter(ScanPayService service, Usuario usuario,
                                    LcpfEstablecimiento establecimiento, ScanManualMontoContract.View view) {
        this.establecimiento = establecimiento;
    }

    @Override
    public LcpfEstablecimiento getEstablecimiento() {
        return establecimiento;
    }

    @Override
    public void getCards() {

    }

    @Override
    public boolean validateMonto(String monto) {
        return StringUtil.isDecimalAmount(monto);
    }
}
