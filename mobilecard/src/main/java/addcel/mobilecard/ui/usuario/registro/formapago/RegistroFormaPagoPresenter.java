package addcel.mobilecard.ui.usuario.registro.formapago;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 29/09/17.
 */
class RegistroFormaPagoPresenter implements RegistroFormaPagoContract.Presenter {
    private final UsuariosService service;
    private final String email;
    private final String password;
    private final String nombre;
    private final String apellido;
    private final String celular;
    private final int tipoCedula;
    private final String cedula;
    private final int pais;
    private final Calendar calendarVigencia;
    private final RegistroFormaPagoContract.View view;

    RegistroFormaPagoPresenter(UsuariosService service, String email, String password, String nombre,
                               String apellido, String celular, int tipoCedula, String cedula, int pais,
                               RegistroFormaPagoContract.View view) {
        this.service = service;
        this.email = email;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
        this.celular = celular;
        this.tipoCedula = tipoCedula;
        this.cedula = cedula;
        this.pais = pais;
        this.view = view;
        this.calendarVigencia = Calendar.getInstance();
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public String getApellido() {
        return apellido;
    }

    @Override
    public Calendar getCalendarVigencia() {
        return calendarVigencia;
    }

    @Override
    public String getVigenciaFormatted() {
        return DateFormat.format("MM/yy", calendarVigencia).toString();
    }

    @Override
    public void updateCalendarVigencia(int year, int month) {
        calendarVigencia.set(Calendar.YEAR, year);
        calendarVigencia.set(Calendar.MONTH, month);
    }

    @SuppressLint("CheckResult")
    @Override
    public void getPrivacy() {
        view.showProgress();
        String client = StringUtil.sha256("mobilecardandroid");
        service.getAvisoPrivacidad(BuildConfig.ADDCEL_APP_ID, client, pais,
                StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(terminosResponse -> {
                    view.hideProgress();
                    if (terminosResponse.getIdError() == 0) {
                        view.onPrivacy(terminosResponse.getTerminos());
                    } else {
                        view.showError(terminosResponse.getMensajeError());
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.showError(StringUtil.getNetworkError());
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getTerminos() {
        view.showProgress();
        String client = StringUtil.sha256("mobilecardandroid");
        service.getTerminos(BuildConfig.ADDCEL_APP_ID, client, pais, StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(terminosResponse -> {
                    view.hideProgress();
                    if (terminosResponse.getIdError() == 0) {
                        view.showTerminosDialog(terminosResponse);
                    } else {
                        view.showError(terminosResponse.getMensajeError());
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.showError(StringUtil.getNetworkError());
                });
    }

    @Override
    public void saveUser(@NotNull String profile) {


    }
}
