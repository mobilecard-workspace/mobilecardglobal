package addcel.mobilecard.ui.usuario.endtoend.qr

import addcel.mobilecard.R
import addcel.mobilecard.data.net.hth.ReceiptEntity
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity
import addcel.mobilecard.utils.BundleBuilder
import android.graphics.Bitmap
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.phrase.Phrase
import kotlinx.android.synthetic.main.view_lcpf_qrcode.*
import java.text.NumberFormat

/**
 * ADDCEL on 2019-05-14.
 */
class H2HQrFragment : Fragment() {

    companion object {
        fun get(receipt: ReceiptEntity, qrBmp: Bitmap): H2HQrFragment {
            val bundle =
                    BundleBuilder().putParcelable("receipt", receipt).putParcelable("qrBmp", qrBmp)
                            .build()

            val fragment = H2HQrFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var transferActivity: MxTransferActivity
    private lateinit var receipt: ReceiptEntity
    private lateinit var qrBmp: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        transferActivity = activity as MxTransferActivity
        receipt = arguments?.getParcelable("receipt")!!
        qrBmp = arguments?.getParcelable("qrBmp")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_lcpf_qrcode, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        transferActivity.setStreenStatus(MxTransferActivity.STATUS_NORMAL)
        b_qrcode_ok.setOnClickListener {
            startActivity(MxTransferActivity.get(view.context)); activity?.finish()
        }

        h_qrcode.visibility = View.GONE
        b_qrcode_share.visibility = View.INVISIBLE
        label_qrcode_mesero.visibility = View.GONE
        setQrCode(qrBmp)

        showSuccess(getString(R.string.txt_confirmacion_pago_msg))
    }

    private fun setQrCode(bmp: Bitmap) {
        view_qrcode.setImageBitmap(bmp)
    }

    private fun showSuccess(msg: String) {
        view_qrcode_msg.text = buildMsg(msg)
    }

    private fun buildMsg(base: String): CharSequence {
        return Phrase.from(base).put("mensaje", receipt.message)
                .put("idbitacora", receipt.idTransaccion.toString()).put("auth", receipt.authNumber)
                .put("tdc", receipt.maskedPAN)
                .put("date", DateFormat.format("yyyy-MM-dd HH:mm:ss", receipt.dateTime))
                .put("monto", NumberFormat.getCurrencyInstance().format(receipt.amount)).format()
    }
}