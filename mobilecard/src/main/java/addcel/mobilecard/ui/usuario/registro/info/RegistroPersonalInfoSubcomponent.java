package addcel.mobilecard.ui.usuario.registro.info;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 28/09/17.
 */
@PerFragment
@Subcomponent(modules = RegistroPersonalInfoModule.class)
public interface RegistroPersonalInfoSubcomponent {
    void inject(RegistroPersonalInfoFragment fragment);
}
