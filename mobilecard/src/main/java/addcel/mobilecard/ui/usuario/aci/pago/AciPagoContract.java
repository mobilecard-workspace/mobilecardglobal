package addcel.mobilecard.ui.usuario.aci.pago;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 1/23/19.
 */
public interface AciPagoContract {
    interface View extends ScreenView, Validator.ValidationListener {
        String getIMEI();
    }

    interface Presenter {

        void clearDisposables();

        void getAddresses();

        String calculaComision(double monto);

        void saveToFav(ServiceModel servicio, int logo, String etiqueta);

        void pagar(Address address, String cuenta, double monto, CardEntity cardCard);
    }
}
