package addcel.mobilecard.ui.usuario.blackstone.monto;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/07/18.
 */
@PerFragment
@Subcomponent(modules = BSMontoModule.class)
public interface BSMontoSubcomponent {
    void inject(BSMontoFragment fragment);
}
