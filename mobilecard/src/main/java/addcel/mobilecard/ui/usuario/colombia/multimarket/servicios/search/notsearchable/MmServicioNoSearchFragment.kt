package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search.notsearchable

import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Categoria
import addcel.mobilecard.data.net.colombia.multimarket.Servicio
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.monto.MmServicioMontoFragment
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_producto_list.*

/**
 * ADDCEL on 2019-12-20.
 */


@Parcelize
data class MmServicioNoSearchModel(val categoria: Categoria, val servicios: List<Servicio>) : Parcelable

interface MmServicioNoSearchView : ScreenView {

    fun configServicioList(view: View)

    fun clickServicio(pos: Int)
}

class MmServicioNoSearchFragment : Fragment(), MmServicioNoSearchView {

    companion object {
        fun get(categoria: Categoria, servicios: List<Servicio>): MmServicioNoSearchFragment {
            val args = BundleBuilder().putParcelable("model", MmServicioNoSearchModel(categoria, servicios)).build()
            val frag = MmServicioNoSearchFragment()
            frag.arguments = args
            return frag
        }
    }

    private lateinit var model: MmServicioNoSearchModel
    private lateinit var adapter: MmServicioNoSearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = arguments?.getParcelable("model")!!
        adapter = MmServicioNoSearchAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_producto_list, container, false)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configServicioList(view)
        if (adapter.itemCount == 0) adapter.update(model.servicios)
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_productos)
        super.onDestroyView()
    }

    override fun configServicioList(view: View) {
        recycler_productos.adapter = adapter
        recycler_productos.layoutManager = GridLayoutManager(view.context, 2)
        recycler_productos.setHasFixedSize(true)
        ItemClickSupport.addTo(recycler_productos).setOnItemClickListener { _, position, _ ->
            clickServicio(position)
        }
    }

    override fun clickServicio(pos: Int) {
        (activity as ContainerActivity).fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, MmServicioMontoFragment.get(model.categoria, adapter.getItem(pos)))
            hide(this@MmServicioNoSearchFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    override fun showSuccess(msg: String) {
        (activity as ContainerActivity).showSuccess(msg)
    }

    override fun showError(msg: String) {
        (activity as ContainerActivity).showError(msg)
    }

    override fun hideProgress() {
        (activity as ContainerActivity).hideProgress()
    }

    override fun showProgress() {
        (activity as ContainerActivity).showProgress()
    }

}