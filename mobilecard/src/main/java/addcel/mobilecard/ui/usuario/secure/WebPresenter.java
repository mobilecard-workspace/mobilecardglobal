package addcel.mobilecard.ui.usuario.secure;

import androidx.annotation.NonNull;

import com.google.common.collect.Maps;

import java.util.Map;

import addcel.mobilecard.R;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.secure.WebInteractor;

/**
 * ADDCEL on 19/12/16.
 */
public class WebPresenter implements WebContract.Presenter {

    private final WebInteractor interactor;
    private final String formUrl;
    private final WebContract.View view;

    WebPresenter(WebInteractor interactor, String formUrl, WebContract.View view) {
        this.interactor = interactor;
        this.formUrl = formUrl;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        if (interactor.getCompositeDisposable() != null)
            interactor.getCompositeDisposable().clear();
    }

    @Override
    public void getCardDataForPeaje(String endpointForm) {
        if (endpointForm.contains(formUrl)) view.setCardData();
    }

    @Override
    public String buildLoadDataFun() {
        return interactor.getFormData();
    }

    @Override
    public void processUrl(String url, byte[] data, String auth) {
        view.startProgress(R.string.progress);
        Map<String, String> headers = Maps.newHashMap();
        headers.put("Authorization", auth);
        interactor.processUrl(url, data, headers, new InteractorCallback<String>() {
            @Override
            public void onSuccess(@NonNull String result) {
                view.stopProgress();
                view.onUrlProcessed(url, result);
            }

            @Override
            public void onError(@NonNull String error) {
                view.stopProgress();
                view.showError(error, true);
            }
        });
    }
}
