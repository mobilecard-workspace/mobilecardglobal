package addcel.mobilecard.ui.usuario.viamericas.result;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.common.base.Strings;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;

import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.viamericas.model.CreditTransactionConfirmationResponse;
import addcel.mobilecard.event.impl.ViamericasShareEvent;
import addcel.mobilecard.ui.ContainerActivity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * ADDCEL on 28/03/17.
 */

public final class ViamericasResultFragment extends Fragment {

    private final NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);

    @BindDrawable(R.drawable.ic_historial_success)
    Drawable SUCCESS;
    @BindDrawable(R.drawable.ic_historial_error)
    Drawable ERROR;
    @BindString(R.string.txt_viamericas_dummyTitle)
    String SUCCESS_TITLE;
    @BindString(R.string.txt_transaction_error)
    String ERROR_TITLE;
    @BindString(R.string.txt_viamericas_confirm_url_msg)
    String URL_MSG;
    @BindString(R.string.txt_viamericas_pago_msg)
    String SUCCESS_MSG;
    @BindView(R.id.b_success_disclaimer)
    Button disclaimerButton;
    @BindView(R.id.title_result)
    TextView resultTitle;
    @BindView(R.id.img_result)
    ImageView resultImg;
    @BindView(R.id.text_result)
    TextView resultView;

    @Inject
    Bus bus;
    @Inject
    SessionOperations session;

    private Unbinder unbinder;
    private CreditTransactionConfirmationResponse result;
    private AlertDialog disclaimerDialog;

    public ViamericasResultFragment() {
    }

    public static ViamericasResultFragment get(CreditTransactionConfirmationResponse response) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("result", response);
        ViamericasResultFragment fragment = new ViamericasResultFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            result = getArguments().getParcelable("result");
            Mobilecard.get().getNetComponent().inject(this);
            bus.register(this);
        } catch (Throwable t) {
            Timber.e(t);
        }
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_success, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ContainerActivity) view.getContext()).setStreenStatus(ContainerScreenView.STATUS_FINISHED);
        view.findViewById(R.id.footer_viamericas).setVisibility(View.VISIBLE);
        view.findViewById(R.id.b_viamericas_share).setVisibility(View.VISIBLE);
        view.findViewById(R.id.b_viamericas_ok).setVisibility(View.VISIBLE);
        if (result.getIdError() == 0 && result.getResponseCode() == 0) {
            resultTitle.setText(SUCCESS_TITLE);
            resultImg.setImageDrawable(SUCCESS);
            resultView.setText(buildSuccessMSG(result));
            disclaimerButton.setVisibility(View.GONE);
        } else {
            resultTitle.setText(ERROR_TITLE);
            resultImg.setImageDrawable(ERROR);
            resultView.setText(result.getResponseMessage());
            disclaimerButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @OnClick(R.id.b_success_disclaimer)
    void clickDisclaimer() {
        showDisclaimer(result);
    }

    @OnClick(R.id.b_viamericas_share)
    void share() {
        share(result);
    }

    @OnClick(R.id.b_viamericas_ok)
    void ok() {
        if (getActivity() != null) getActivity().onBackPressed();
    }

    private CharSequence buildSuccessMSG(CreditTransactionConfirmationResponse response) {
        return Phrase.from(getContext(), R.string.txt_viamericas_pago_msg)
                .put("tdc", Strings.nullToEmpty(response.getCardNumber()))
                .put("monto", format.format(response.getObjReceipt().getTotalPayReceiver()))
                .put("curr_envio", response.getObjReceipt().getCurrencySrc())
                .put("monto_recibido", format.format(response.getObjReceipt().getTotalPayReceiver()))
                .put("curr_recibido", response.getObjReceipt().getCurrencyPayer())
                .put("auth", response.getApprovalCode())
                .put("date", response.getTransactionDate())
                .format();
    }

    private void share(CreditTransactionConfirmationResponse response) {
        if (response.getIdError() == 0 && response.getResponseCode() == 0) {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, SUCCESS_TITLE);
            sharingIntent.putExtra(Intent.EXTRA_TEXT, buildRecipientMsg(response));
            startActivity(Intent.createChooser(sharingIntent, getString(R.string.txt_transaction_share)));
        }
    }

    @Subscribe
    public void postShareFromActivity(ViamericasShareEvent event) {
        share(result);
    }

    private CharSequence buildRecipientMsg(CreditTransactionConfirmationResponse response) {
        return Phrase.from(URL_MSG)
                .put("sender_name", session.getUsuario().getUsrNombre() + " " + session.getUsuario().getUsrApellido())
                .put("url", "")
                .format();
    }

    private void showDisclaimer(CreditTransactionConfirmationResponse response) {
        if (disclaimerDialog == null) {
            disclaimerDialog =
                    new AlertDialog.Builder(getContext()).setTitle(R.string.txt_title_disclaimer)
                            .setPositiveButton(android.R.string.ok,
                                    DetachableClickListener.wrap((dialogInterface, i) -> dialogInterface.dismiss()))
                            .create();
        }
        disclaimerDialog.setMessage("");
        disclaimerDialog.show();
    }
}
