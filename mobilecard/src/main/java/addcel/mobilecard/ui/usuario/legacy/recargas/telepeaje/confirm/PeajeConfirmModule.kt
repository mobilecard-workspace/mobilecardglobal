package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.confirm

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.telepeaje.TagsApi
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.token.TokenizerAPI.Companion.provideTokenizerAPI
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import retrofit2.Retrofit
import java.text.NumberFormat

/**
 * ADDCEL on 20/10/17.
 */
@Module
class PeajeConfirmModule(private val fragment: PeajeConfirmFragment) {

    @PerFragment
    @Provides
    fun provideContainer(): PagoContainerActivity {
        return fragment.activity as PagoContainerActivity
    }

    @PerFragment
    @Provides
    fun provideToken(retrofit: Retrofit): TokenizerAPI {
        return provideTokenizerAPI(retrofit)
    }

    @PerFragment
    @Provides
    fun provideApi(retrofit: Retrofit): TagsApi {
        return retrofit.create(TagsApi::class.java)
    }

    @PerFragment
    @Provides
    fun providePresenter(tagsApi: TagsApi, tokenizerAPI: TokenizerAPI,
                         session: SessionOperations, numberFormat: NumberFormat): PeajeConfirmPresenter {

        val saldoTag = fragment.arguments?.getDouble("saldoTag")!!
        val card: CardEntity = fragment.arguments?.getParcelable("card")!!
        val monto: CatalogoResponse.MontoEntity = fragment.arguments?.getParcelable("monto")!!
        val tag = fragment.arguments?.getString("tag")!!
        val verificador = fragment.arguments?.getString("pin")!!

        return PeajeConfirmPresenterImpl(tagsApi, tokenizerAPI, session, saldoTag, monto, card, tag,
                verificador, numberFormat, fragment)
    }

}

@PerFragment
@Subcomponent(modules = [PeajeConfirmModule::class])
interface PeajeConfirmSubcomponent {
    fun inject(fragment: PeajeConfirmFragment)
}