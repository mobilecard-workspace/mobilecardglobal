package addcel.mobilecard.ui.usuario.scanpay;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.model.SPQrEntity;
import addcel.mobilecard.ui.usuario.scanpay.qr.SpQrScannerActivity;
import addcel.mobilecard.ui.usuario.scanpay.qr.SpQrScannerModel;
import addcel.mobilecard.ui.usuario.scanpay.qr.mx.monto.SpMxMontoQrKeyboardFragment;
import addcel.mobilecard.ui.usuario.scanpay.qr.mx.monto.SpMxMontoQrKeyboardViewModel;
import addcel.mobilecard.ui.usuario.scanpay.qr.pe.qr.ScanPeQrMontoData;
import addcel.mobilecard.ui.usuario.scanpay.qr.pe.qr.ScanPeQrMontoFragment;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.utils.JsonUtil;
import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;

public class ScanPayActivity extends AppCompatActivity implements ScanPayContract.View {

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ProgressBar progressBar;
    private int idPais;
    private boolean finished;

    public static synchronized Intent get(Context context, int idPais) {
        return new Intent(context, ScanPayActivity.class).putExtras(
                new BundleBuilder().putInt("idPais", idPais).build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_pay);

        idPais = Objects.requireNonNull(getIntent().getExtras()).getInt("idPais");
        final RxPermissions permissions = new RxPermissions(this);


        /*

        if (idPais == PaisResponse.PaisEntity.PE) {
            compositeDisposable.add(
                    permissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE)
                            .subscribe(aBoolean -> {
                                if (aBoolean) {
                                    startActivityForResult(ScanPeQrActivity.Companion.get(ScanPayActivity.this),
                                            ScanPeQrActivity.REQUEST_CODE);
                                }
                            }));
        } else {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_scanpay, ScanQrFragment.get())
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        }*/

        compositeDisposable.add(
                permissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE)
                        .subscribe(aBoolean -> {
                            if (aBoolean) {

                                SpQrScannerModel model = new SpQrScannerModel(idPais, "");

                                startActivityForResult(SpQrScannerActivity.Companion.get(ScanPayActivity.this, model),
                                        SpQrScannerActivity.REQUEST_CODE);
                            }
                        }));


    }

    @Override
    public void onBackPressed() {
        if (finished) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override
    public void showProgress() {
        if (getProgressBar().getVisibility() == View.GONE)
            getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (getProgressBar().getVisibility() == View.VISIBLE)
            getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showError(String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void showSuccess(String msg) {
        Toasty.success(this, msg).show();
    }

    private ProgressBar getProgressBar() {
        if (progressBar == null) progressBar = findViewById(R.id.progress_scanpay);
        return progressBar;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SpQrScannerActivity.REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    final SpQrScannerModel model = data.getParcelableExtra(SpQrScannerActivity.RESULT_DATA);
                    if (model != null) {
                        evalCountry(model.getIdPais(), model.getResult());
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                showError("Cancelado por el usuario");
                finish();
            }
        }
    }

    private void evalCountry(int idPais, String code) {
        switch (idPais) {
            case PaisResponse.PaisEntity.MX:
                processMX(code);
                break;
            case PaisResponse.PaisEntity.US:
                processUSA(code);
                break;
            case PaisResponse.PaisEntity.PE:
                processPe(code);
                break;
            default:
                showError("Próximamente disponible. Enero 2020");
        }
    }

    private void processMxAmount(SPQrEntity result) {
        SPPagoEntity pEntity =
                new SPPagoEntity(result.getAmountMxn(), result.getComision(), result.getConcept(),
                        "", result.getEstablecimientoId(), "",
                        result.getIdBitacora(), 0, 0,
                        DeviceUtil.Companion.getDeviceId(this), 0.0, 0.0,
                        "", 0, result.getPropina(),
                        result.getReferenciaNeg(), "", "",
                        "", "", "",
                        "", "", "", "");

        Bundle bundle = new BundleBuilder().putParcelable("pago", pEntity).build();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_scanpay,
                        WalletSelectFragment.get(WalletSelectFragment.SCANPAY_QR_MX, bundle))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    private void processMxNoAmount(SPQrEntity result) {
        final SPPagoEntity noMontoPago =
                new SPPagoEntity(result.getAmountMxn(), result.getComision(), result.getConcept(),
                        "", result.getEstablecimientoId(), "",
                        result.getIdBitacora(), 0, 0,
                        DeviceUtil.Companion.getDeviceId(this), 0.0, 0.0,
                        "", 0, result.getPropina(),
                        result.getReferenciaNeg(), "", "",
                        "", "", "",
                        "", "", "", "");

        final SpMxMontoQrKeyboardViewModel noMontoModel = new SpMxMontoQrKeyboardViewModel(noMontoPago);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_scanpay, SpMxMontoQrKeyboardFragment.Companion.get(noMontoModel))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    private void processMX(@NotNull String code) {
        SPQrEntity qrEntity = JsonUtil.fromJson(code, SPQrEntity.class);
        if (qrEntity.getAmountMxn() > 0) {
            processMxAmount(qrEntity);
        } else {
            processMxNoAmount(qrEntity);
        }

    }

    private void processUSA(@NonNull String code) {
        SPQrEntity qrEntity = JsonUtil.fromJson(code, SPQrEntity.class);
        SPPagoEntity pEntity =
                new SPPagoEntity(qrEntity.getAmountUsd(), qrEntity.getComision(), qrEntity.getConcept(),
                        "", qrEntity.getEstablecimientoId(), "",
                        qrEntity.getIdBitacora(), 0, 0,
                        DeviceUtil.Companion.getDeviceId(this), 0.0, 0.0,
                        "", 0, qrEntity.getPropina(),
                        qrEntity.getReferenciaNeg(), "", "",
                        "", "", "",
                        "", "", "", "");

        Bundle bundle = new BundleBuilder().putParcelable("pago", pEntity).build();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_scanpay,
                        WalletSelectFragment.get(WalletSelectFragment.SCANPAY_QR_MX, bundle))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    private void processPe(@NotNull String code) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_scanpay, ScanPeQrMontoFragment.Companion.get(new ScanPeQrMontoData(code)))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }
}