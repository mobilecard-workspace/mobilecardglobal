package addcel.mobilecard.ui.usuario.aci.confirmation;

import android.annotation.SuppressLint;
import android.os.Build;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.aci.confirmation.AciConfirmationInteractor;
import addcel.mobilecard.domain.aci.confirmation.model.PagoModel;
import addcel.mobilecard.domain.aci.confirmation.model.PagoNoDataModel;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 17/10/17.
 */
class AciConfirmationPresenter implements AciConfirmationContract.Presenter {

    private final AciConfirmationInteractor interactor;
    private final boolean withData;
    private final ServiceModel serviceModel;
    private final Address address;
    private final String account;
    private final double monto;
    private final double fee;
    private final double total;
    private final CardEntity card;
    private final EstadoResponse.EstadoEntity estado;
    private final String ciudad;
    private final String zipCode;
    private final String direccion;
    private final String cvv;
    private final AciConfirmationContract.View view;

    private AciConfirmationPresenter(Builder builder) {
        interactor = builder.interactor;
        view = builder.view;
        withData = builder.withData;
        serviceModel = builder.serviceModel;
        address = builder.address;
        account = builder.account;
        monto = builder.monto;
        fee = builder.fee;
        total = builder.total;
        card = builder.card;
        estado = builder.estado;
        ciudad = builder.ciudad;
        zipCode = builder.zipCode;
        direccion = builder.direccion;
        cvv = builder.cvv;
    }

    @Override
    public void clearDisposables() {
        interactor.getCompositeDisposable().clear();
    }

    @Override
    public String getAccount() {
        return account;
    }

    @Override
    public double getMonto() {
        return monto;
    }

    @Override
    public double getFee() {
        return fee;
    }

    @Override
    public double getTotal() {
        return total;
    }

    @Override
    public void pagar() {
        view.showProgress();
        if (withData) {
            pagarWithData();
        } else {
            pagarNoData();
        }
    }

    @SuppressLint("CheckResult")
    private void pagarWithData() {
        PagoModel model = new PagoModel();
        model.setCiudadServicio(address.getCity());
        model.setDireccionServicio(address.getStreet());
        model.setIdioma(StringUtil.getCurrentLanguage());
        model.setIdServicio(String.valueOf(serviceModel.getId()));
        model.setImei(view.getImei());
        model.setModelo(Build.MODEL);
        model.setMonto(monto);
        model.setNombreServicio(serviceModel.getName());
        model.setReferencia(account);
        model.setSoftware(String.valueOf(Build.VERSION.SDK_INT));
        model.setWkey(view.getImei());

        interactor.pagar(model, new InteractorCallback<McResponse>() {
            @Override
            public void onSuccess(@NotNull McResponse result) {
                view.hideProgress();
                view.onPagoSuccess(result);
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError(error);
            }
        });
    }

    @SuppressLint("CheckResult")
    private void pagarNoData() {
        PagoNoDataModel model = new PagoNoDataModel();
        model.setCiudadServicio(address.getCity());
        model.setCiudadUsuario(ciudad);
        model.setCodigo(AddcelCrypto.encryptHard(cvv));
        model.setCpUsuario(zipCode);
        model.setDireccionServicio(address.getStreet());
        model.setDireccionUsuario(StringUtil.removeAcentosMultiple(direccion));
        model.setEstadoUsuario(estado.getId());
        model.setIdServicio(String.valueOf(serviceModel.getId()));
        model.setIdTarjeta(card.getIdTarjeta());
        model.setImei(view.getImei());
        model.setModelo(Build.MODEL);
        model.setMonto(monto);
        model.setNombreServicio(serviceModel.getName());
        model.setReferencia(account);
        model.setSoftware(String.valueOf(Build.VERSION.SDK_INT));
        model.setWkey(view.getImei());

        interactor.pagarSinDatos(model, new InteractorCallback<McResponse>() {
            @Override
            public void onSuccess(@NotNull McResponse result) {
                view.hideProgress();
                view.onPagoSuccess(result);
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError(error);
            }
        });
    }

    static class Builder {
        private final AciConfirmationInteractor interactor;
        private final AciConfirmationContract.View view;
        private boolean withData;
        private ServiceModel serviceModel;
        private Address address;
        private String account;
        private double monto;
        private double fee;
        private double total;
        private CardEntity card;
        private EstadoResponse.EstadoEntity estado;
        private String ciudad;
        private String zipCode;
        private String direccion;
        private String cvv;

        public Builder(AciConfirmationInteractor interactor, AciConfirmationContract.View view) {
            this.interactor = interactor;
            this.view = view;
        }

        public Builder setWithData(boolean withData) {
            this.withData = withData;
            return this;
        }

        public Builder setServiceModel(ServiceModel serviceModel) {
            this.serviceModel = serviceModel;
            return this;
        }

        public Builder setAddress(Address address) {
            this.address = address;
            return this;
        }

        public Builder setAccount(String account) {
            this.account = account;
            return this;
        }

        public Builder setMonto(double monto) {
            this.monto = monto;
            return this;
        }

        public Builder setFee(double fee) {
            this.fee = fee;
            return this;
        }

        public Builder setTotal(double total) {
            this.total = total;
            return this;
        }

        public Builder setCard(CardEntity card) {
            this.card = card;
            return this;
        }

        public Builder setEstado(EstadoResponse.EstadoEntity estado) {
            this.estado = estado;
            return this;
        }

        public Builder setCiudad(String ciudad) {
            this.ciudad = ciudad;
            return this;
        }

        public Builder setZipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public Builder setDireccion(String direccion) {
            this.direccion = direccion;
            return this;
        }

        public Builder setCvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public AciConfirmationPresenter build() {
            return new AciConfirmationPresenter(this);
        }
    }
}
