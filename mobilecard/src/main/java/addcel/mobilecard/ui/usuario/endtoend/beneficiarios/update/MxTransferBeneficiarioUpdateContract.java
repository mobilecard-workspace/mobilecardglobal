package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.update;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.AccountResponse;

/**
 * ADDCEL on 09/11/17.
 */
public interface MxTransferBeneficiarioUpdateContract {
    interface View extends Validator.ValidationListener {
        String getAlias();

        String getEmail();

        String getNumTelefono();

        void clickSave(android.view.View view);

        void showProgress();

        void hideProgress();

        void showError(String msg);

        void onSave(AccountResponse accountResponse);
    }

    interface Presenter {

        AccountEntity getAccount();

        void save();
    }
}
