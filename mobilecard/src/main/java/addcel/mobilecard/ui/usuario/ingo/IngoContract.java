package addcel.mobilecard.ui.usuario.ingo;

import androidx.appcompat.widget.Toolbar;

import com.ingomoney.ingosdk.android.IngoSdkManager;

import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;

/**
 * ADDCEL on 16/07/18.
 */
public interface IngoContract {

    interface View {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        Toolbar getToolbarFromActivity();

        void setFinished(boolean finished);

        void kill();

        IngoService getService();

        IngoAliveAPI getAliveApi();

        void isRegistered(IngoUserData data);

        void notRegistered(String msg);

        IngoSdkManager getIngoSdkManager();

        String getImei();

        void onIngoAvailable();

        void showMessageNotAvailable();
    }

    interface Presenter {

        void onDestroy();

        IngoService getService();

        IngoAliveAPI getAliveApi();

        void getUserData();

        void checkIfAvailable();
    }
}
