package addcel.mobilecard.ui.usuario.wallet.select.add;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/10/17.
 */
@PerFragment
@Subcomponent(modules = WalletSelectAddModule.class)
public interface WalletSelectAddSubcomponent {
    void inject(WalletSelectAddFragment fragment);
}
