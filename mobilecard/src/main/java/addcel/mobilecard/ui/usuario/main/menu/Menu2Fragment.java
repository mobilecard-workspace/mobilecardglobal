package addcel.mobilecard.ui.usuario.main.menu;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.usuarios.CountryNotificationEnvent;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.event.impl.PostPaisEvent;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.ui.usuario.main.MainContract;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * ADDCEL on 04/10/17.
 */
public class Menu2Fragment extends Fragment implements MenuContract.View {

    @Inject
    MainActivity activity;
    @Inject
    Bus bus;
    @Inject
    MenuAdapter adapter;
    @Inject
    MenuContract.Presenter presenter;

    @BindView(R.id.recycler_menu)
    RecyclerView recyclerView;

    private Unbinder unbinder;
    private boolean listLocked = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().menu2Subcomponent(new Menu2Module(this)).inject(this);
        bus.register(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_menu_2, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener((recyclerView, position, v) -> {
            RecyclerView.ViewHolder vh = recyclerView.getChildViewHolder(v);
            if (vh instanceof MenuAdapter.ItemViewHolder) {
                if (!listLocked) clickModel(position);
            }
        });

        activity.setBottomMenuUseCase(UseCase.HOME);
        activity.setSelection(MainContract.UseCase.INICIO);

        setWelcomeName();

        presenter.setLaunched();
        presenter.updateDrawer();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setWelcomeName();
            presenter.updateDrawer();
            bus.post(new CountryNotificationEnvent(presenter.getIdPais()));
            activity.setBottomMenuUseCase(UseCase.HOME);
            activity.setSelection(MainContract.UseCase.INICIO);
            listLocked = false;
        }
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(recyclerView);
        recyclerView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void setWelcomeName() {

    }

    @Override
    public void clickModel(int pos) {
        if (isVisible()) {
            listLocked = true;
            MenuModel model = adapter.getItem(pos);
            presenter.launchView(model);
            new Handler().postDelayed(() -> listLocked = false, 1500);
        }
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String mensajeError) {
        activity.showError(mensajeError);
    }

    @Override
    public void showSuccess(String mensaje) {
        activity.showSuccess(mensaje);
    }

    @Override
    public void finish() {
        activity.finish();
    }

    @Subscribe
    @Override
    public void updatePais(PostPaisEvent event) {
        Timber.d("PostPaisEvent: %d", event.getIdPais());
        adapter.update(Lists.newArrayList(MenuModel.values()), presenter.getIdPais(), true);
        if (getView() != null && isVisible()) {
            setWelcomeName();
        }
    }

    @Subscribe
    @Override
    public void postProfile(Bitmap bitmap) {
    }

    @Override
    public void clickProfile() {
        activity.goTo(MainContract.UseCase.PROFILE);
    }
}
