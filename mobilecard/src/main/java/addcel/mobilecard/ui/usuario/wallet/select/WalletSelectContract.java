package addcel.mobilecard.ui.usuario.wallet.select;

import java.util.List;

import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 09/10/17.
 */
public interface WalletSelectContract {
    interface View {
        void showRetry();

        void hideRetry();

        void clickRetry();

        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showError(int resId);

        void showSuccess(String msg);

        void clickSelect(int pos);

        void onCardsUpdated(List<CardEntity> models);

        void goToNext();
    }

    interface Presenter {

        void getCards();

        void clearDisposables();
    }
}
