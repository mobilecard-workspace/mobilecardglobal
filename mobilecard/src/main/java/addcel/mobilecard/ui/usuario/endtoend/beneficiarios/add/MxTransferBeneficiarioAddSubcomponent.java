package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/11/17.
 */
@PerFragment
@Subcomponent(modules = MxTransferBeneficiarioAddModule.class)
public interface MxTransferBeneficiarioAddSubcomponent {
    void inject(MxTransferBeneficiarioAddFragment fragment);
}
