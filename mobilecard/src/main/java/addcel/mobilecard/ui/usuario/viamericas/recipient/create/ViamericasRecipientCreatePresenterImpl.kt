package addcel.mobilecard.ui.usuario.viamericas.recipient.create

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.RecipientModel
import addcel.mobilecard.data.net.viamericas.model.VmCountryModel
import addcel.mobilecard.data.net.viamericas.model.VmStateModel
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * ADDCEL on 13/10/17.
 */

interface ViamericasRecipientCreatePresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getEstadosPorPais(country: VmCountryModel)

    fun getCiudadesPorEstado(country: VmCountryModel, state: VmStateModel)

    fun addRecipient(recipient: RecipientModel)
}


class ViamericasRecipientCreatePresenterImpl(
        private val service: ViamericasService, private val compositeDisposable: CompositeDisposable,
        private val view: ViamericasRecipientCreateView
) : ViamericasRecipientCreatePresenter {
    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }


    override fun getEstadosPorPais(country: VmCountryModel) {
        val json = "{\"idCountry\": \"" + country.idCountry + "\", \"operation\":\"NO\"}"
        Timber.d(json)
        val eDisp =
                service.getStates(
                        ViamericasService.AUTH,
                        BuildConfig.ADDCEL_APP_ID,
                        StringUtil.getCurrentLanguage(),
                        json
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) {
                                view.postStates(it.states)
                            } else {
                                view.showError(it.mensajeError)
                            }
                        }, {
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })
        addToDisposables(eDisp)
    }

    override fun getCiudadesPorEstado(country: VmCountryModel, state: VmStateModel) {
        view.showProgress()
        val json = ("{\"idCountry\": \""
                + country.idCountry
                + "\", \"idState\":\""
                + state.idState
                + "\", \"operation\":\"NO\"}")
        Timber.d(json)
        val citDisp = service.getCitiesByState(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                json
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) {
                        view.postCities(it.cities)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(citDisp)
    }

    override fun addRecipient(recipient: RecipientModel) {
        view.showProgress()
        val request = JsonUtil.toJson(recipient)
        Timber.d("Peticion recipient: %s", request)

        val rDisp = service.createRecipient(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                request
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) {
                        view.postRecipientResponse(it)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })

        addToDisposables(rDisp)
    }
}
