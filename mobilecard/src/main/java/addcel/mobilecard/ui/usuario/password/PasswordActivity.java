package addcel.mobilecard.ui.usuario.password;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public final class PasswordActivity extends AppCompatActivity implements PasswordContract.View {

    private static final String PASS_KEY_USUARIO = "usuario";

    @NotEmpty(messageResId = R.string.error_password)
    @BindView(R.id.til_password_old)
    TextInputLayout currentTil;

    @Password(scheme = Password.Scheme.ANY, messageResId = R.string.error_password, min = 8)
    @BindView(R.id.til_password_new)
    TextInputLayout newTil;

    @ConfirmPassword(messageResId = R.string.error_conf_password)
    @BindView(R.id.til_password_conf)
    TextInputLayout confTil;

    @BindView(R.id.progress_password)
    ProgressBar progressBar;

    @Inject
    Validator validator;
    @Inject
    PasswordContract.Presenter presenter;

    public static synchronized Intent get(Context context, Usuario usuario) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PASS_KEY_USUARIO, usuario);
        return new Intent(context, PasswordActivity.class).putExtras(bundle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        ButterKnife.bind(this);
        Mobilecard.get().getNetComponent().passwordSubcomponent(new PasswordModule(this)).inject(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(@NonNull String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void showSuccess(@NotNull String charSequence) {
        Toasty.success(this, charSequence).show();
    }

    @Override
    public void onValidationSucceeded() {
        presenter.setPassword(
                Objects.requireNonNull(currentTil.getEditText()).getText().toString().trim(),
                Objects.requireNonNull(newTil.getEditText()).getText().toString().trim());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationUtils.onValidationFailed(this, errors);
    }

    @OnClick(R.id.b_password_actualizar)
    @Override
    public void clickActualizar() {
        ValidationUtils.clearViewErrors(currentTil, newTil, confTil);
        validator.validate();
    }

    @Override
    public void onPasswordChanged(@NonNull String msg) {
        showSuccess(msg);
        startActivity(MainActivity.get(this, true));
    }
}
