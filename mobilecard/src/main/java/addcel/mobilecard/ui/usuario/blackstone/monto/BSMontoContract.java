package addcel.mobilecard.ui.usuario.blackstone.monto;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.data.net.blackstone.model.BSProductEntity;
import addcel.mobilecard.data.net.usuarios.model.MobilecardContact;
import addcel.mobilecard.ui.ScreenView;
import io.reactivex.disposables.Disposable;

/**
 * ADDCEL on 09/07/18.
 */
public interface BSMontoContract {
    interface View extends ScreenView, Validator.ValidationListener {

        MobilecardContact getSelectedContact();

        void clickContact();

        String getCapturedPhone();

        double getCapturedMonto();

        void setContactComboEnabled(boolean enable);

        void setPhoneSwitchEnabled(boolean enabled);

        void onPhoneSwitchEnabled(boolean enabled);

        void onPhoneCaptured(CharSequence chars);

        void onConfirmCaptured(CharSequence chars);

        void onMontoCaptured(CharSequence chars);

        void setContinuarEnabled(@Nullable MobilecardContact contact, double monto);

        void clickContinuar();
    }

    interface Presenter {

        void addToDisposables(Disposable disposable);

        void clearDisposables();

        BSProductEntity getProduct();

        Bundle buildPagoBundle();
    }
}
