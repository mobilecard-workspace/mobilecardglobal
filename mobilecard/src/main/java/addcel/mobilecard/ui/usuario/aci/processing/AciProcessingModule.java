package addcel.mobilecard.ui.usuario.aci.processing;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.common.base.Preconditions;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 16/10/17.
 */
@Module
public final class AciProcessingModule {
    private final AciProcessingFragment fragment;

    AciProcessingModule(AciProcessingFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    AciActivity provideActivity() {
        return (AciActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    AciService provideAciService(Retrofit retrofit) {
        return retrofit.create(AciService.class);
    }

    @PerFragment
    @Provides
    @Nullable
    Bundle provideArgs() {
        return fragment.getArguments();
    }

    @PerFragment
    @Provides
    AciProcessingContract.Presenter providePresenter(AciService service,
                                                     SessionOperations session, @Nullable Bundle args) {
        ServiceModel serviceModel = Preconditions.checkNotNull(args).getParcelable("service");
        CardEntity card = Preconditions.checkNotNull(args).getParcelable("card");
        Address address = Preconditions.checkNotNull(args).getParcelable("address");
        String referencia = Preconditions.checkNotNull(args).getString("referencia");
        double amount = Preconditions.checkNotNull(args).getDouble("monto");
        double comision = Preconditions.checkNotNull(args).getDouble("comision");
        return new AciProcessingPresenter(service, serviceModel, card, referencia, amount, comision,
                address, session.getUsuario(), fragment);
    }
}
