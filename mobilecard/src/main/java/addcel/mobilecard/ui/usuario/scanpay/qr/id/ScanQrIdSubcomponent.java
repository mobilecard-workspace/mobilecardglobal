package addcel.mobilecard.ui.usuario.scanpay.qr.id;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 28/08/18.
 */
@PerFragment
@Subcomponent(modules = ScanQrIdModule.class)
public interface ScanQrIdSubcomponent {
    void inject(ScanQrIdFragment fragment);
}
