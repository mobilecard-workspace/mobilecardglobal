package addcel.mobilecard.ui.usuario.viamericas.sender

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.viamericas.VmActivity
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.StringUtil
import addcel.mobilecard.validation.ValidationUtils
import addcel.mobilecard.validation.annotation.Celular
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import butterknife.*
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.Checked
import com.mobsandgeeks.saripaar.annotation.Email
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Select
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_viamericas_sender.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject
import javax.inject.Named


interface VmSenderView : ScreenView, Validator.ValidationListener,
        DatePickerDialog.OnDateSetListener {
    fun postCountries(countries: List<VmCountryModel>)

    fun postStates(states: List<VmStateModel>)

    fun postCities(cities: List<VmCityModel>)

    fun postZipCodes(zipCodes: List<VmZipModel>)

    fun postSender(response: SenderResponse)
}

class ViamericasSenderActivity : AppCompatActivity(), VmSenderView {

    companion object {

        const val REQUEST_CODE = 428
        const val RESULT_DATA = "sender"

        private val INITIAL_STATE = VmStateModel("-1", "Selecciona")
        private val INITIAL_CITY = VmCityModel("-1", "Selecciona")
        private val INITIAL_ZIP = VmZipModel("-1", "-1", "Selecciona")

        fun get(context: Context): Intent {
            return Intent(context, ViamericasSenderActivity::class.java)
        }
    }


    //VIEWS
    @Email(messageResId = R.string.error_email)
    @BindView(R.id.til_viamericas_sender_email)
    lateinit var emailTil: TextInputLayout
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_viamericas_sender_first)
    lateinit var nombreTil: TextInputLayout
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_viamericas_sender_last)
    lateinit var apellidoTil: TextInputLayout
    @BindView(R.id.til_viamericas_sender_second)
    lateinit var maternoTil: TextInputLayout
    @Celular(messageResId = R.string.error_celular)
    @BindView(R.id.til_viamericas_sender_celular)
    lateinit var celularTil: TextInputLayout
    @BindView(R.id.text_viamericas_sender_date)
    lateinit var dateText: EditText
    @BindView(R.id.b_viamericas_sender_date)
    lateinit var dateButton: Button
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_viamericas_sender_address)
    lateinit var addressTil: TextInputLayout
    @BindView(R.id.til_viamericas_sender_address_two)
    lateinit var addressTwoTil: TextInputLayout
    @Select(messageResId = R.string.error_viamericas_estado)
    @BindView(R.id.spinner_viamericas_sender_state)
    lateinit var estadoSpinner: Spinner
    @Select(messageResId = R.string.error_viamericas_ciudad)
    @BindView(R.id.spinner_viamericas_sender_city)
    lateinit var citySpinner: Spinner
    @Select(messageResId = R.string.error_viamericas_zip)
    @BindView(R.id.spinner_viamericas_sender_zip)
    lateinit var zipSpinner: Spinner
    @BindView(R.id.spinner_viamericas_sender_destination)
    lateinit var paisDestSpinner: Spinner
    @Checked(messageResId = R.string.error_viamericas_terms)
    @BindView(R.id.check_viamericas_terms)
    lateinit var termsCheck: CheckBox
    @Checked(messageResId = R.string.error_viamericas_privacy)
    @BindView(R.id.check_viamericas_privacy)
    lateinit var privacyCheck: CheckBox

    //DEPENDENCIAS
    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var validator: Validator

    @field:[Inject Named("viamericaEstadosAdapter")]
    lateinit var estadoAdapter: ArrayAdapter<VmStateModel>

    @field:[Inject Named("viamericaCityAdapter")]
    lateinit var cityAdapter: ArrayAdapter<VmCityModel>

    @field:[Inject Named("viamericaZipAdapter")]
    lateinit var zipAdapter: ArrayAdapter<VmZipModel>

    @field:[Inject Named("viamericaCountryAdapter")]
    lateinit var countryAdapter: ArrayAdapter<VmCountryModel>

    @Inject
    lateinit var nacimientoPicker: DatePickerDialog
    @Inject
    lateinit var presenter: ViamericasSenderPresenter

    lateinit var unbinder: Unbinder


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.slide_out_right)

        Mobilecard.get()
                .netComponent
                .viamericasSenderSubcomponent(ViamericasSenderModule(this))
                .inject(this)

        setContentView(R.layout.activity_viamericas_sender)
        unbinder = ButterKnife.bind(this)

        AndroidUtils.setText(emailTil.editText, presenter.fetchUserEmail())
        AndroidUtils.setText(nombreTil.editText, presenter.fetchUserName())
        AndroidUtils.setText(apellidoTil.editText, presenter.fetchUserLastName())
        AndroidUtils.setText(maternoTil.editText, presenter.fetchUserMaterno())
        AndroidUtils.setText(celularTil.editText, presenter.fetchUserPhone())
        AndroidUtils.setText(addressTil.editText, presenter.fetchUserAddress())

        estadoSpinner.adapter = estadoAdapter
        citySpinner.adapter = cityAdapter
        zipSpinner.adapter = zipAdapter
        paisDestSpinner.adapter = countryAdapter

        presenter.getEstados(true)
        presenter.getCountries()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun postCountries(countries: List<VmCountryModel>) {
        countryAdapter.add(VmCountryModel("-1", getString(R.string.txt_viamericas_opcional)))
        countryAdapter.addAll(countries)
    }

    override fun postStates(states: List<VmStateModel>) {
        estadoAdapter.add(INITIAL_STATE)
        estadoAdapter.addAll(states)
    }

    override fun postCities(cities: List<VmCityModel>) {
        cityAdapter.clear()
        cityAdapter.add(INITIAL_CITY)
        cityAdapter.addAll(cities)
    }

    override fun postZipCodes(zipCodes: List<VmZipModel>) {
        zipAdapter.clear()
        zipAdapter.add(INITIAL_ZIP)
        zipAdapter.addAll(zipCodes)
    }

    override fun postSender(response: SenderResponse) {
        val intent = Intent().putExtra(ViamericasSenderActivity.RESULT_DATA, response)
        setResult(RESULT_OK, intent)
        finish()
    }

    @OnItemSelected(
            value = [R.id.spinner_viamericas_sender_state],
            callback = OnItemSelected.Callback.ITEM_SELECTED
    )
    fun onEstadoSelected(pos: Int) {
        if (pos > 0) presenter.getCities(
                Objects.requireNonNull<VmStateModel>(
                        estadoAdapter.getItem(
                                pos
                        )
                )
        )
    }

    @OnItemSelected(
            value = [R.id.spinner_viamericas_sender_state],
            callback = OnItemSelected.Callback.NOTHING_SELECTED
    )
    fun onEstadoNotSelected() {

    }

    @OnItemSelected(
            value = [R.id.spinner_viamericas_sender_city],
            callback = OnItemSelected.Callback.ITEM_SELECTED
    )
    fun onCitySelected(pos: Int) {
        if (pos > 0) presenter.getZipCode(
                Objects.requireNonNull<VmCityModel>(
                        cityAdapter.getItem(
                                pos
                        )
                )
        )
    }

    @OnItemSelected(
            value = [R.id.spinner_viamericas_sender_city],
            callback = OnItemSelected.Callback.NOTHING_SELECTED
    )
    fun onCityNotSelected() {

    }

    @OnClick(R.id.b_viamericas_sender_date)
    fun showDateDialog() {
        if (!nacimientoPicker.isShowing) nacimientoPicker.show()
    }

    @OnClick(R.id.b_viamericas_sender_continuar)
    fun continuar() {
        validator.validate()
    }


    override fun showProgress() {
        if (progress_viamericas_sender.visibility == View.GONE) progress_viamericas_sender.visibility =
                View.VISIBLE
    }

    override fun hideProgress() {
        if (progress_viamericas_sender.visibility == View.VISIBLE) progress_viamericas_sender.visibility =
                View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        ValidationUtils.onValidationFailed(this, errors)
    }

    override fun onValidationSucceeded() {
        if (dateText.tag != null && VmActivity.hasAgeRequirement(
                        dateText.tag as LocalDateTime,
                        18
                )
        ) {
            val sAddress = StringUtil.removeAcentos(AndroidUtils.getText(addressTil.editText))
            val sEmail = AndroidUtils.getText(emailTil.editText)
            val sName = StringUtil.removeAcentos(AndroidUtils.getText(nombreTil.editText))
            val sApellido = StringUtil.removeAcentos(AndroidUtils.getText(apellidoTil.editText))
            val sCelular = AndroidUtils.getText(celularTil.editText)
            val sAddress2 = StringUtil.removeAcentos(AndroidUtils.getText(addressTwoTil.editText))
            val sApellidoMaterno =
                    StringUtil.removeAcentos(AndroidUtils.getText(maternoTil.editText))
            val dobTime = dateText.tag as LocalDateTime
            val dob = DateTimeFormatter.ofPattern("yyyy/MM/dd").format(dobTime)

            val sender = SenderModel.Builder().setAddress(sAddress)
                    .setEmail(sEmail)
                    .setFirstName(sName)
                    .setIdCity((citySpinner.selectedItem as VmCityModel).idCity)
                    .setIdCountry("USA")
                    .setIdState((estadoSpinner.selectedItem as VmStateModel).idState)
                    .setIdUsuario(session.usuario.ideUsuario)
                    .setLastName(sApellido)
                    .setPhone(sCelular)
                    .setZipCode((zipSpinner.selectedItem as VmZipModel).zipCode)
                    .setAddress2(sAddress2)
                    .setBirthDate(dob)
                    .setLName(sApellido)
                    .setPhone1(sCelular)
                    .setSLName(sApellidoMaterno)
                    .setZIP((zipSpinner.selectedItem as VmZipModel).zipCode)
                    .build()

            presenter.addSender(sender)
        } else {
            showError(getString(R.string.error_fecha_nacimiento))
        }
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        //SUMAMOS 1 POR INDICE DE MES
        dateText.setText(String.format(Locale.US, "%d/%d/%d", p3, p2 + 1, p1))
        dateText.tag = LocalDateTime.of(p1, p2 + 1, p3, 0, 0)
    }
}
