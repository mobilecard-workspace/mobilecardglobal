package addcel.mobilecard.ui.usuario.viamericas.recipient.select


import addcel.mobilecard.R
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.RecipientResponse
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import android.widget.ArrayAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Named

/**
 * ADDCEL on 27/03/17.
 */

@Module
class ViamericasRecipientModule(private val fragment: ViamericasRecipientFragment) {

    @PerFragment
    @Provides
    fun provideActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideModel(): ViamericasRecipientModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    @Named("viamericasRecipientAdapter")
    fun provideRecipientAdapter(containerActivity: ContainerActivity): ArrayAdapter<RecipientResponse> {
        val recipientResponseArrayAdapter =
                ArrayAdapter<RecipientResponse>(containerActivity, android.R.layout.simple_spinner_item)
        recipientResponseArrayAdapter.setDropDownViewResource(
                R.layout.support_simple_spinner_dropdown_item
        )
        recipientResponseArrayAdapter.setNotifyOnChange(true)
        return recipientResponseArrayAdapter
    }

    @PerFragment
    @Provides
    fun providePresenter(service: ViamericasService): ViamericasRecipientPresenter {
        return ViamericasRecipientPresenterImpl(service, CompositeDisposable(), fragment)
    }
}


@PerFragment
@Subcomponent(modules = [ViamericasRecipientModule::class])
interface ViamericasRecipientSubcomponent {
    fun inject(fragment: ViamericasRecipientFragment)
}
