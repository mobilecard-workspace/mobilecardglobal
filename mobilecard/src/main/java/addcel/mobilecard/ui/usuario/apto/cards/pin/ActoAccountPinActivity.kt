package addcel.mobilecard.ui.usuario.apto.cards.pin

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.accounts.AccountsAPI
import addcel.mobilecard.data.net.apto.accounts.AptoCardData
import addcel.mobilecard.data.net.apto.accounts.AptoPinRequest
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.ErrorUtil
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.isDigitsOnly
import androidx.core.text.trimmedLength
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_acto_account_pin.*

@Parcelize
data class ActoAccountPinModel(val userToken: String, val accountId: String) : Parcelable

class ActoAccountPinActivity : AppCompatActivity() {

    companion object {

        const val REQUEST_CODE = 430
        const val RESULT_DATA = "data"

        fun get(context: Context, model: ActoAccountPinModel): Intent {
            return Intent(context, ActoAccountPinActivity::class.java).putExtra("model", model)
        }
    }

    private lateinit var model: ActoAccountPinModel
    private val compositeDisposable = CompositeDisposable()
    private val api = AccountsAPI.get(BuildConfig.APTO_VAULT_URL)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acto_account_pin)
        b_apto_account_pin_cambiar.setOnClickListener {

            val pin = AndroidUtils.getText(til_apto_account_pin.editText)
            val conf = AndroidUtils.getText(til_apto_account_pin_confirm.editText)

            if (eval(pin, conf)) compositeDisposable.add(changePin(pin))
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun eval(pin: String, conf: String): Boolean {
        val validPin = pin.isDigitsOnly() && pin.trimmedLength() == 4
        if (validPin) til_apto_account_pin.error = null else til_apto_account_pin.error =
                "El pin no es válido"

        val validConf = conf == pin
        if (validConf) til_apto_account_pin_confirm.error =
                null else til_apto_account_pin_confirm.error = "Confirmación Incorrecta"

        return validPin && validConf
    }

    private fun changePin(pin: String): Disposable {
        return api.pin("Bearer " + model.userToken, model.accountId, AptoPinRequest(pin))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onPinChanged(it)
                }, {
                    val code = ErrorUtil.getFormattedHttpErrorCode(it)
                    if (code == 404) {
                        Toasty.error(this, "La tarjeta no existe").show()
                    } else {
                        Toasty.error(this, ErrorUtil.getFormattedHttpErrorMsg(it))
                                .show()
                    }
                })
    }

    private fun onPinChanged(data: AptoCardData) {
        intent = Intent().putExtra(RESULT_DATA, data)
        setResult(Activity.RESULT_OK, intent)
    }


}
