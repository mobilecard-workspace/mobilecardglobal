package addcel.mobilecard.ui.usuario.perfil;

import android.content.DialogInterface;
import android.graphics.Bitmap;

import com.mobsandgeeks.saripaar.Validator;

import java.io.IOException;
import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.PhotoResponseEvent;
import addcel.mobilecard.ui.usuario.main.MainContract;

/**
 * ADDCEL on 05/10/17.
 */
public interface PerfilContract {

    int REQUEST_CODE = MainContract.UseCase.PROFILE;

    interface View extends Validator.ValidationListener, DialogInterface.OnShowListener {
        String getNombre();

        String getApellido();

        String getEmail();

        int getPais();

        void setPais(int idPais);

        String getCelular();

        void clickCamera();

        void clickEdit();

        void clickPassword();

        void clickSave();

        void showProgress();

        void hideProgress();

        void onError(int resId);

        void onError(String msg);

        void onSuccess(String msg);

        void notifyUpdate();

        void notifyPasswordUpdate();

        void onImageLoaded(Bitmap bitmap);

        void updatePhoto(PhotoResponseEvent event);

        void notifyPaises(List<PaisResponse.PaisEntity> paises);

        void notifyPaisesError(String msg);
    }

    interface Presenter {
        String NAME_TEL = "usr_telefono";
        String NAME_NOMBRE = "usr_nombre";
        String NAME_APELLIDO = "usr_apellido";
        String NAME_PAIS = "idpais";

        long getIdUsuario();

        String getNombre();

        String getApellido();

        String getEmail();

        String getPassword();

        void getPaises(boolean withRetry);

        int getPais();

        String getCelular();

        void loadImage();

        void save();

        void updatePassword(String password);

        void updateProfile(String path);

        String encodeImage(String path) throws IOException;

        void clearDisposables();
    }
}
