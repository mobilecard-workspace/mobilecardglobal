package addcel.mobilecard.ui.usuario.aci.result;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * ADDCEL on 28/03/17.
 */
public final class AciResultFragment extends DialogFragment {

    @BindDrawable(R.drawable.ic_historial_success)
    Drawable SUCCESS;
    @BindDrawable(R.drawable.ic_historial_error)
    Drawable ERROR;
    @BindString(R.string.txt_transaction_success)
    String SUCCESS_TITLE;

    @BindString(R.string.txt_transaction_error)
    String ERROR_TITLE;

    @BindView(R.id.title_result)
    TextView resultTitle;

    @BindView(R.id.img_result)
    ImageView resultImg;

    @BindView(R.id.text_result)
    TextView resultView;

    private AciActivity aciActivity;
    private Unbinder unbinder;
    private McResponse result;

    public AciResultFragment() {
    }

    public static AciResultFragment get(McResponse response) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("result", response);
        AciResultFragment fragment = new AciResultFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aciActivity = (AciActivity) Preconditions.checkNotNull(getActivity());
        result = Preconditions.checkNotNull(getArguments()).getParcelable("result");
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_success, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        aciActivity.setStreenStatus(ContainerScreenView.STATUS_FINISHED);

        view.findViewById(R.id.b_viamericas_ok).setVisibility(View.VISIBLE);
        if (result.getIdError() == 0) {
            resultTitle.setText(SUCCESS_TITLE);
            resultImg.setImageDrawable(SUCCESS);
            resultView.setText(result.getMensajeError().replace("\\n", "\n"));
        } else {
            resultTitle.setText(ERROR_TITLE);
            resultImg.setImageDrawable(ERROR);
            resultView.setText(result.getMensajeError());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.b_viamericas_ok)
    void clickOk() {
        onDismiss(Objects.requireNonNull(getDialog()));
    }

    @Override
    public void onDismiss(@NotNull DialogInterface dialog) {
        aciActivity.onBackPressed();
    }
}
