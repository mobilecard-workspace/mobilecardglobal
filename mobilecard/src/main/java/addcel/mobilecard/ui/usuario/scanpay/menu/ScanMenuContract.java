package addcel.mobilecard.ui.usuario.scanpay.menu;

import addcel.mobilecard.R;

/**
 * ADDCEL on 21/08/18.
 */
public interface ScanMenuContract {
    enum Tipo {
        QR(R.drawable.b_scan_qr, R.string.txt_scanpay_menu_qr);
        //MANUAL(R.drawable.b_scan_manual, R.string.txt_scanpay_menu_manual);

        private final int icon;
        private final int str;

        Tipo(int icon, int str) {
            this.icon = icon;
            this.str = str;
        }

        public int getIcon() {
            return icon;
        }

        public int getStr() {
            return str;
        }
    }

    interface View {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void clickItem(Tipo t);

        void launchScanner();
    }

    interface Presenter {
    }
}
