package addcel.mobilecard.ui.usuario.blackstone.confirm;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import java.text.NumberFormat;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.blackstone.BlackstoneService;
import addcel.mobilecard.data.net.blackstone.model.BSPagoRequestEntity;
import addcel.mobilecard.data.net.blackstone.model.BSProductEntity;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 10/07/18.
 */
class BSConfirmPresenter implements BSConfirmContract.Presenter {
    private final BlackstoneService service;
    private final TokenizerAPI token;
    private final SessionOperations session;
    private final NumberFormat currencyFormat;
    private final CardEntity card;
    private final BSProductEntity product;
    private final String telefono;
    private final double monto;
    private final BSConfirmContract.View view;
    private final CompositeDisposable disposables;

    BSConfirmPresenter(BlackstoneService service, TokenizerAPI token, SessionOperations session,
                       NumberFormat currencyFormat, CardEntity card, BSProductEntity product, String telefono,
                       double monto, BSConfirmContract.View view) {
        this.service = service;
        this.token = token;
        this.session = session;
        this.currencyFormat = currencyFormat;
        this.card = card;
        this.product = product;
        this.telefono = telefono;
        this.monto = monto;
        this.disposables = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void addToDisposables(Disposable disposable) {
        disposables.add(disposable);
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @Override
    public String getJumioRef() {
        return session.getUsuario().getScanReference();
    }

    @Override
    public BSProductEntity getProduct() {
        return product;
    }

    @Override
    public String getPhone() {
        return telefono;
    }

    @Override
    public double getMonto() {
        return monto;
    }

    @Override
    public double getComision() {
        return monto * 0.05;
    }

    @Override
    public double getGrandTotal() {
        return monto + getComision();
    }

    @Override
    public CardEntity getCard() {
        return card;
    }

    @Override
    public String formatCantidad(double cantidad) {
        return currencyFormat.format(cantidad);
    }

    @Override
    public void checkWhiteList() {
        view.disablePagarButton();
        view.showProgress();
        final Disposable disp = token.isOnWhiteList(BuildConfig.ADDCEL_APP_ID, session.getUsuario().getIdPais(),
                StringUtil.getCurrentLanguage(), session.getUsuario().getIdeUsuario())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tokenBaseResponse -> {
                    view.enablePagarButton();
                    view.hideProgress();
                    if (tokenBaseResponse.getCode() == 0) {
                        view.onWhiteList();
                    } else {
                        view.notOnWhiteList();
                    }
                }, throwable -> {
                    view.enablePagarButton();
                    view.hideProgress();
                    view.showError(ErrorUtil.Companion.getFormattedHttpErrorMsg(throwable));
                });
        addToDisposables(disp);
    }

    @SuppressLint("CheckResult")
    @Override
    public void pagar() {
        view.disablePagarButton();
        view.showProgress();

        BSPagoRequestEntity request = new BSPagoRequestEntity.Builder().withAmount(getMonto())
                .withCountryCode("USA")
                .withIdTarjeta(getCard().getIdTarjeta())
                .withMainCode(getProduct().getCode())
                .withPhoneNumber(StringUtil.trimPhone(getPhone()))
                .withIdUsuario(session.getUsuario().getIdeUsuario())
                .withCarrierName(product.getCarrierName())
                .withtLat(session.getMinimalLocationData().getLat())
                .withLon(session.getMinimalLocationData().getLon())
                .build();

        addToDisposables(
                service.doTopUp(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(bsPagoResponseEntity -> {
                            view.hideProgress();
                            view.enablePagarButton();
                            view.onPagoSuccess(bsPagoResponseEntity);
                        }, throwable -> {
                            view.hideProgress();
                            view.enablePagarButton();
                            view.showError(Strings.nullToEmpty(throwable.getLocalizedMessage()));
                        }));
    }
}
