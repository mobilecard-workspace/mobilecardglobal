package addcel.mobilecard.ui.usuario.scanpay.qr.pe.processing

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.crypto.AddcelCrypto

/**
 * ADDCEL on 2019-07-17.
 */
interface ScanPeQrProcessingPresenter {
    fun launchPago(pago: SPPagoEntity, token: TokenEntity)

    fun getToken(pago: SPPagoEntity)
}

class ScanPeQrProcessingPresenterImpl(
        val api: SPApi, val tokenizer: TokenizerAPI,
        val disposables: CompositeDisposable, val view: View
) : ScanPeQrProcessingPresenter {

    override fun launchPago(pago: SPPagoEntity, token: TokenEntity) {
        view.showProgress()
        disposables.add(
                api.pagoBP(
                        token.token, BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage(),
                        SPApi.TIPO_NEGOCIO, token.accountId, pago
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r ->
                    view.hideProgress()
                    view.onPago(r)
                }, { t ->
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(t))
                })
        )
    }

    override fun getToken(pago: SPPagoEntity) {
        view.showProgress()

        val auth = AddcelCrypto.encryptSensitive(JsonUtil.toJson(pago))

        disposables.add(
                tokenizer.getToken(
                        auth, "", BuildConfig.ADDCEL_APP_ID, 4,
                        StringUtil.getCurrentLanguage()
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ t ->
                    view.hideProgress()
                    if (t.code == 0) launchPago(pago, t)
                    else view.showError(t.message)
                }, { t ->
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(t))
                })
        )
    }
}