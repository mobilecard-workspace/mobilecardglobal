package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search

import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

/**
 * ADDCEL on 2019-12-20.
 */
@Module
class MmServicioSearchModule(val fragment: MmServicioSearchFragment) {

    @PerFragment
    @Provides
    fun provideContainer(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideModel(): MmServicioSearchModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideApi(@Named("cacheRetrofit") retrofit: Retrofit): MultimarketAPI {
        return MultimarketAPI.get(retrofit)
    }

    @PerFragment
    @Provides
    fun providePresenter(multimarket: MultimarketAPI): MmServicioSearchPresenter {
        return MmServicioSearchPresenterImpl(multimarket, CompositeDisposable(), fragment)
    }

}

@PerFragment
@Subcomponent(modules = [MmServicioSearchModule::class])
interface MmServicioSearchSubcomponent {

    fun inject(fragment: MmServicioSearchFragment)
}