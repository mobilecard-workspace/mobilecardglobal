package addcel.mobilecard.ui.usuario.perfil.photo

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.R
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.utils.ErrorUtil
import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.provider.MediaStore
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.tbruyelle.rxpermissions2.RxPermissions
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_perfil_photo.*
import java.io.File
import java.io.IOException
import java.text.DateFormat
import java.util.*

@Parcelize
data class PerfilPhotoModel(val idUsuario: Long) : Parcelable

class PerfilPhotoActivity : AppCompatActivity(), ScreenView {

    companion object {
        const val REQUEST_CODE = 420
        const val REQUEST_GALLERY = 500
        const val REQUEST_CAMERA = 501
        const val DATA_PATH = "img_path"

        fun get(context: Context, model: PerfilPhotoModel): Intent {
            return Intent(context, PerfilPhotoActivity::class.java).putExtra("model", model)
        }
    }

    private val disposables = CompositeDisposable()
    private val photoUtil = PerfilPhotoUtil()

    lateinit var model: PerfilPhotoModel
    lateinit var photoPath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil_photo)
        model = intent?.getParcelableExtra("model")!!

        val permissionGal = RxPermissions(this)
        val permissionCam = RxPermissions(this)

        b_photo_gallery.setOnClickListener {
            disposables.add(permissionGal.request(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).subscribe { accept -> if (accept) launchGallery() })
        }

        b_photo_camera.setOnClickListener {
            disposables.add(permissionCam.request(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).subscribe { accept -> if (accept) launchCamera() })

        }
        b_photo_guardar.setOnClickListener {
            if (::photoPath.isInitialized && photoPath.isNotEmpty()) {
                val intent = Intent().putExtra(DATA_PATH, photoPath)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    override fun showProgress() {
        progress_photo.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_photo.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    private fun launchGallery() {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"

        val chooserIntent = Intent.createChooser(getIntent, "Select Image")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

        startActivityForResult(chooserIntent, REQUEST_GALLERY)
    }

    private fun launchCamera() {

        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) { // Error occurred while creating the File
                    ex.printStackTrace()
                    Toasty.error(
                            this,
                            ex.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.OPERATION)
                    )
                            .show()
                    null
                } // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri =
                            FileProvider.getUriForFile(
                                    this,
                                    BuildConfig.APPLICATION_ID + ".fileprovider",
                                    it
                            )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File { // Create an image file name
        val timeStamp: String = DateFormat.getDateInstance().format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
                "JPEG_${timeStamp}_", /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            photoPath = absolutePath
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val selectedImage: Uri? = data.data
                    if (selectedImage != null) {
                        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                        val cursor =
                                contentResolver.query(selectedImage, filePathColumn, null, null, null)
                        cursor!!.moveToFirst()
                        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                        val picturePath = cursor.getString(columnIndex)
                        try {
                            photoPath = photoUtil.compressImage(this, picturePath)
                            img_photo_profile.setImageBitmap(BitmapFactory.decodeFile(photoPath))
                        } catch (e: IllegalStateException) {
                            e.printStackTrace()
                            showError(
                                    e.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.OPERATION)
                            )
                        } finally {
                            cursor.close()
                        }
                    }
                }
            }
        } else if (requestCode == REQUEST_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                photoPath = photoUtil.compressImage(this, photoPath)
                img_photo_profile.setImageBitmap(BitmapFactory.decodeFile(photoPath))
            }
        } else {
            if (::photoPath.isInitialized) {
                photoPath = ""
                img_photo_profile.setImageDrawable(null)
            }
        }
    }
}
