package addcel.mobilecard.ui.usuario.aci.pago;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.impl.AciAddressReceivedEvent;
import addcel.mobilecard.event.impl.ErrorEvent;
import addcel.mobilecard.ui.custom.view.CheckableImageButton;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import addcel.mobilecard.ui.usuario.aci.result.AciResultFragment;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.utils.ListUtil;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;

/**
 * A simple {@link Fragment} subclass.
 */
public class AciPagoFragment extends Fragment implements AciPagoContract.View {

    private final CompositeDisposable permDisposables = new CompositeDisposable();
    @BindString(R.string.txt_transfer_comision)
    String comisionBaseStr;
    @BindView(R.id.label_aci_pagos_title)
    TextView titleView;
    @BindView(R.id.spinner_aci_address)
    Spinner spinnerAddress;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_aci_referencia)
    TextInputLayout referenciaTil;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_aci_monto)
    TextInputLayout montoTil;
    @BindView(R.id.view_aci_comision)
    TextView comisionView;
    @BindView(R.id.b_aci_service_fav)
    CheckableImageButton favButton;
    @BindView(R.id.til_aci_service_favorito)
    TextInputLayout favTil;
    @BindView(R.id.view_aci_service_favorito)
    View favView;
    @Inject
    Validator validator;
    @Inject
    ServiceModel model;
    @Inject
    boolean fav;
    @Inject
    Bus bus;
    @Inject
    List<CardEntity> formasPago;
    @Inject
    @Named("addressAdapter")
    ArrayAdapter<Address> addressAdapter;
    @Inject
    AciPagoContract.Presenter presenter;
    private AciActivity aciActivity;
    private Unbinder unbinder;
    private RxPermissions permissions;

    public AciPagoFragment() {
        // Required empty public constructor
    }

    public static AciPagoFragment get(ServiceModel model) {
        return get(model, Boolean.FALSE);
    }

    public static AciPagoFragment get(ServiceModel model, boolean fav) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("model", model);
        bundle.putBoolean("fav", fav);
        AciPagoFragment fragment = new AciPagoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @OnTextChanged(value = R.id.text_aci_monto, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void onMontoCapturado(CharSequence sequence) {
        if (Strings.isNullOrEmpty(sequence.toString())) {
            comisionView.setText(
                    Phrase.from(comisionBaseStr).put("comision", presenter.calculaComision(0)).format());
        } else {
            comisionView.setText(Phrase.from(comisionBaseStr)
                    .put("comision", presenter.calculaComision(Double.parseDouble(sequence.toString())))
                    .format());
        }
    }

    @OnClick(R.id.b_aci_pagar)
    void clickPagar() {
        validator.validate();
    }

    @Subscribe
    public void setAddresses(AciAddressReceivedEvent event) {
        View v = Objects.requireNonNull(getView());
        if (ListUtil.notEmpty(event.getAddresses())) {
            addressAdapter.addAll(event.getAddresses());
            v.findViewById(R.id.label_aci_address).setVisibility(View.VISIBLE);
            spinnerAddress.setVisibility(View.VISIBLE);
        } else {
            v.findViewById(R.id.label_aci_address).setVisibility(View.GONE);
            spinnerAddress.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResult(McResponse response) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = aciActivity.getFragmentManagerLazy().beginTransaction();
        Fragment prev = this;

        ft.remove(prev);
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = AciResultFragment.get(response);
        newFragment.show(ft, "dialog");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aciActivity = (AciActivity) Preconditions.checkNotNull(getActivity());
        aciActivity.getSubcomponent().aciPagoSubcomponent(new AciPagoModule(this)).inject(this);
        permissions = new RxPermissions(this);
        bus.register(this);

        presenter.getAddresses();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.view_aci_pago, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleView.setText(model.getName());
        spinnerAddress.setAdapter(addressAdapter);
        onMontoCapturado("");
        setInitialFav(fav);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        presenter.clearDisposables();
        permDisposables.clear();
        super.onDestroy();
    }

    @OnClick(R.id.b_aci_service_fav)
    public void clickFav() {
        setFav(!favButton.isChecked());
    }

    @OnClick(R.id.b_aci_service_fav_add)
    public void clickSaveFav() {
        if (!Strings.isNullOrEmpty(AndroidUtils.getText(favTil.getEditText()))) {
            favTil.setError(null);
            presenter.saveToFav(model, R.drawable.logo_aci, AndroidUtils.getText(favTil.getEditText()));
        } else {
            favTil.setError(getText(R.string.error_default));
        }
    }

    private void setFav(boolean fav) {
        favButton.setChecked(fav);
        new Handler().postDelayed(() -> favView.setVisibility(fav ? View.VISIBLE : View.GONE), 250);
    }

    private void setInitialFav(boolean fav) {
        favButton.setChecked(fav);
        favButton.setEnabled(!fav);
        if (!fav) favButton.requestFocus();
    }

    @Subscribe
    public void postFavEvent(ErrorEvent event) {
        Context context = Preconditions.checkNotNull(getContext());
        if (event.getErrorRes() == R.string.txt_favoritos_success) {
            Toasty.success(context,
                    Strings.isNullOrEmpty(event.getErrorMsg()) ? getString(event.getErrorRes())
                            : event.getErrorMsg()).show();
            favButton.setEnabled(false);
            favView.setVisibility(View.GONE);
        } else {
            Toasty.error(context,
                    Strings.isNullOrEmpty(event.getErrorMsg()) ? getString(event.getErrorRes())
                            : event.getErrorMsg()).show();
        }
    }

    @Override
    public void onValidationSucceeded() {
        permDisposables.add(
                permissions.request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                        .subscribe(aBoolean -> {
                            if (aBoolean) launchWallet();
                        })
        );
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(getActivity(), errors);
    }

    private void launchWallet() {
        Address address = spinnerAddress.getVisibility() == View.VISIBLE ? addressAdapter.getItem(
                spinnerAddress.getSelectedItemPosition()) : new Address("", "");
        double monto = Double.parseDouble(AndroidUtils.getText(montoTil.getEditText()));

        Bundle bundle = new Bundle();
        bundle.putParcelable("service", model);
        bundle.putParcelable("address", address);
        bundle.putString("referencia", AndroidUtils.getText(referenciaTil.getEditText()));
        bundle.putDouble("amount", monto);
        bundle.putDouble("comision", monto * model.getPctComision());

        aciActivity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.frame_aci, WalletSelectFragment.get(WalletSelectFragment.SERVICIOS_US, bundle))
                .addToBackStack(null)
                .hide(this)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();
    }

    @Override
    public void showProgress() {
        aciActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        aciActivity.hideProgress();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        aciActivity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        aciActivity.showSuccess(msg);
    }

    @Override
    public String getIMEI() {
        return DeviceUtil.Companion.getDeviceId(aciActivity);
    }
}
