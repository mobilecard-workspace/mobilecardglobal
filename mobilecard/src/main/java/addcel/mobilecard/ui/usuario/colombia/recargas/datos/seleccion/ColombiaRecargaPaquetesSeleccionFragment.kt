package addcel.mobilecard.ui.usuario.colombia.recargas.datos.seleccion

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.Categoria
import addcel.mobilecard.data.net.catalogo.OperadorRecarga
import addcel.mobilecard.data.net.catalogo.Paquete
import addcel.mobilecard.data.net.usuarios.model.MobilecardContact
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.phonecontacts.PhoneContactsActivity
import addcel.mobilecard.ui.usuario.colombia.recargas.ColombiaRecargasActivity
import addcel.mobilecard.ui.usuario.colombia.recargas.categorias.ColombiaCategoria
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.confirm.ColombiaRecargaPaquetesConfirmModel
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.LinearLayoutManager
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_colombia_tae.*
import javax.inject.Inject

/**
 * ADDCEL on 9/23/19.
 */

@Parcelize
data class ColombiaRecargaPaquetesSeleccionModel(
        val colombiaCategoria: ColombiaCategoria, val operador: OperadorRecarga,
        val paquete: Categoria
) : Parcelable

interface ColombiaRecargaPaquetesSeleccionView : ScreenView, TextWatcher {
    fun setLogo(bmp: Bitmap)
    fun onOtroNumeroEnabled(showOtro: Boolean)
    fun setContinuarEnabled(mobilecardContact: MobilecardContact?, paquete: Paquete?)
    fun clickContacto()
    fun onMontos(montos: List<Paquete>)
    fun onMontosError(msg: String)
    fun clickMonto(monto: Paquete)
    fun getSelectedMonto(): Paquete?
    fun showRetry()
    fun hideRetry()
    fun clickRetry()
    fun clickContinuar()
}

class ColombiaRecargaPaquetesSeleccionFragment : Fragment(), ColombiaRecargaPaquetesSeleccionView {

    companion object {
        fun get(
                model: ColombiaRecargaPaquetesSeleccionModel
        ): ColombiaRecargaPaquetesSeleccionFragment {
            val frag = ColombiaRecargaPaquetesSeleccionFragment()
            frag.arguments = BundleBuilder().putParcelable("model", model).build()
            return frag
        }
    }

    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var model: ColombiaRecargaPaquetesSeleccionModel
    @Inject
    lateinit var adapter: ColombiaRecargaPaquetesSeleccionAdapter
    @Inject
    lateinit var presenter: ColombiaRecargaPaquetesSeleccionPresenter
    private lateinit var permission: RxPermissions
    var mobileCardContact: MobilecardContact? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get().netComponent.colombiaRecargaPaquetesSeleccionSubcomponent(
                ColombiaRecargaPaquetesSeleccionModule(this)
        ).inject(this)

        presenter.getMontos(model.paquete)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_colombia_tae, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        permission = RxPermissions(this)

        recycler_montos.adapter = adapter
        recycler_montos.layoutManager = LinearLayoutManager(view.context)
        ItemClickSupport.addTo(recycler_montos).setOnItemClickListener { _, pos, _ ->
            adapter.toggleSelection(pos)
            setContinuarEnabled(mobileCardContact, getSelectedMonto())
        }

        b_recarga_eligenumero.setOnClickListener { clickContacto() }
        switch_recarga_otronumero.setOnCheckedChangeListener { p0, p1 ->
            if (p0 != null) onOtroNumeroEnabled(p1)
        }

        text_recarga_telefono.addTextChangedListener(this)

        b_recarga_pagar.setOnClickListener { clickContinuar() }

        switch_recarga_otronumero.isChecked = true
        onOtroNumeroEnabled(true)

        presenter.loadLogo(model.operador.imgColor, R.drawable.flag_colombia)
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun setLogo(bmp: Bitmap) {
        view_recarga_header.setImageBitmap(bmp)
    }

    override fun onOtroNumeroEnabled(showOtro: Boolean) {
        val visibility = if (showOtro) View.VISIBLE else View.GONE
        b_recarga_eligenumero.text = ""
        text_recarga_telefono.setText("")
        text_recarga_telefono.visibility = visibility
        mobileCardContact = null
    }

    override fun setContinuarEnabled(mobilecardContact: MobilecardContact?, paquete: Paquete?) {
        b_recarga_pagar.isChecked = mobilecardContact != null && paquete != null
    }

    override fun clickContacto() {
        if (activity != null) {
            presenter.addToDisposables(permission.request(
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_CONTACTS
            ).subscribe { accept ->
                if (accept) startActivityForResult(
                        PhoneContactsActivity.get(activity!!),
                        PhoneContactsActivity.REQUEST_CODE
                )
            })
        }
    }

    override fun onMontos(montos: List<Paquete>) {
        hideRetry()
        adapter.update(montos)
    }

    override fun onMontosError(msg: String) {
        showError(msg)
        showRetry()
    }

    override fun clickMonto(monto: Paquete) {
    }

    override fun getSelectedMonto(): Paquete? {
        return if (adapter.selectedItems.isEmpty()) null
        else adapter.getItem(adapter.selectedItems[0])
    }

    override fun showRetry() {
        if (retry_montos.visibility == View.GONE) retry_montos.visibility = View.VISIBLE
    }

    override fun hideRetry() {
        if (retry_montos.visibility == View.VISIBLE) retry_montos.visibility = View.GONE
    }

    override fun clickRetry() {
        presenter.getMontos(model.paquete)
    }

    override fun clickContinuar() {
        if (mobileCardContact != null && getSelectedMonto() != null && b_recarga_pagar.isChecked) {

            val confModel = ColombiaRecargaPaquetesConfirmModel(
                    session.usuario, model.colombiaCategoria,
                    model.operador, model.paquete, getSelectedMonto()!!,
                    StringUtil.trimPhone(mobileCardContact!!.telefono)
            )

            val pagoArgs = BundleBuilder().putParcelable("model", confModel).build()
            (activity as ColombiaRecargasActivity).fragmentManagerLazy.commit {
                add(
                        R.id.frame_colombia,
                        WalletSelectFragment.get(WalletSelectFragment.RECARGAS_PAQUETES_COL, pagoArgs)
                )
                hide(this@ColombiaRecargaPaquetesSeleccionFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        } else {
            showError("Seleccione número destino y paquete a abonar")
        }
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (text_recarga_telefono.hasFocus()) {
                if (p0.isDigitsOnly() && p0.length >= 9 && p0.length <= 12) {
                    text_recarga_telefono.error = null
                    mobileCardContact = MobilecardContact("", p0.toString())
                } else {
                    text_recarga_telefono.error = getText(R.string.error_celular)
                    mobileCardContact = null
                }

                setContinuarEnabled(mobileCardContact, getSelectedMonto())
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun showProgress() {
        (activity as ColombiaRecargasActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ColombiaRecargasActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ColombiaRecargasActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ColombiaRecargasActivity).showSuccess(msg)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PhoneContactsActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val contact: MobilecardContact? =
                            data.getParcelableExtra(PhoneContactsActivity.DATA_CONTACT)
                    if (contact != null) {
                        if (switch_recarga_otronumero.isChecked) {
                            switch_recarga_otronumero.isChecked = false
                            onOtroNumeroEnabled(false)
                        }
                        mobileCardContact = contact
                        b_recarga_eligenumero.text = mobileCardContact.toString()
                        b_recarga_eligenumero.maxLines = 1
                        b_recarga_eligenumero.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0F)
                    }
                    setContinuarEnabled(mobileCardContact, getSelectedMonto())
                }
            }
        }
    }
}