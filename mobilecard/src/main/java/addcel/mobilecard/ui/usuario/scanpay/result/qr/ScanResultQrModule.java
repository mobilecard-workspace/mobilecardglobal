package addcel.mobilecard.ui.usuario.scanpay.result.qr;

import android.app.Activity;
import android.os.Bundle;

import java.text.NumberFormat;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.di.scope.PerFragment;
import dagger.Module;
import dagger.Provides;

@Module
public final class ScanResultQrModule {
    private final ScanResultQrFragment fragment;

    public ScanResultQrModule(ScanResultQrFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    Activity provideActivity() {
        return Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    @Named("idPais")
    int provideIdPais(SessionOperations session) {
        return session.getUsuario().getIdPais();
    }

    @PerFragment
    @Provides
    Bundle provideArgs() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    SPReceiptEntity provideResult(Bundle args) {
        return args.getParcelable("result");
    }

    @PerFragment
    @Provides
    boolean provideFromNegocio(Bundle args) {
        return args.getBoolean("fromNegocio");
    }

    @PerFragment
    @Provides
    ScanResultQrContract.Presenter providePresenter(SPReceiptEntity result,
                                                    NumberFormat currFormat) {
        return new ScanResultQrPresenter(result, currFormat, fragment);
    }
}
