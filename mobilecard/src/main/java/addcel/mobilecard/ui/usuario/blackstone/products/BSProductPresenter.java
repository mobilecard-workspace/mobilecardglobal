package addcel.mobilecard.ui.usuario.blackstone.products;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.blackstone.BlackstoneService;
import addcel.mobilecard.data.net.blackstone.model.BSCarrierEntity;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 09/07/18.
 */
class BSProductPresenter implements BSProductContract.Presenter {
    private final BlackstoneService service;
    private final BSCarrierEntity carrier;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final BSProductContract.View view;

    BSProductPresenter(BlackstoneService service, BSCarrierEntity carrier,
                       BSProductContract.View view) {
        this.service = service;
        this.carrier = carrier;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @Override
    public BSCarrierEntity getCarrier() {
        return carrier;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getProducts() {
        view.showProgress();
        disposables.add(
                service.getProductsByCarrier(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                        carrier.getCarrierName())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(bsProductEntities -> {
                            view.hideProgress();
                            view.setProducts(bsProductEntities);
                        }, throwable -> {
                            view.hideProgress();
                            view.showError(Strings.nullToEmpty(throwable.getLocalizedMessage()));
                        }));
    }
}
