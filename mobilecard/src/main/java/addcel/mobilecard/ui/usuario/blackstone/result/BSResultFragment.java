package addcel.mobilecard.ui.usuario.blackstone.result;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.common.base.Strings;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.io.FileOutputStream;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.blackstone.model.BSPagoResponseEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 11/07/18.
 */
public final class BSResultFragment extends Fragment implements BSResultContract.View {
    private static final String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String SCREENSHOT_PATH = "/Pictures/Screenshots";
    private final CompositeDisposable disposables = new CompositeDisposable();
    @BindDrawable(R.drawable.ic_historial_success)
    Drawable SUCCESS;
    @BindDrawable(R.drawable.ic_historial_error)
    Drawable ERROR;
    @BindString(R.string.txt_transaction_success)
    String SUCCESS_TITLE;
    @BindString(R.string.txt_transaction_error)
    String ERROR_TITLE;
    @BindString(R.string.txt_bs_pago_info)
    String RESULT_MSG;
    @BindView(R.id.img_result)
    ImageView resultImg;
    @BindView(R.id.title_result)
    TextView resultTitleView;
    @BindView(R.id.text_result)
    TextView resultView;
    @BindView(R.id.b_viamericas_share)
    ImageView shareButton;
    @BindView(R.id.b_viamericas_ok)
    Button finishButton;
    @Inject
    BlackstoneActivity activity;
    @Inject
    BSResultContract.Presenter presenter;

    private RxPermissions rxPermissions;
    private Unbinder unbinder;

    public BSResultFragment() {
    }

    public static synchronized BSResultFragment get(BSPagoResponseEntity result) {
        BSResultFragment fragment = new BSResultFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("result", result);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().bsResultSubcomponent(new BSResultModule(this)).inject(this);
        rxPermissions = new RxPermissions(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_bs_result, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (presenter.getResult().getErrorCode().equals(Strings.nullToEmpty("0"))) {
            setSuccess(presenter.getResult());
        } else {
            setError(presenter.getResult());
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }

    @Override
    public void setError(BSPagoResponseEntity result) {
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        resultImg.setImageDrawable(ERROR);
        resultTitleView.setText(ERROR_TITLE);
        resultView.setText(result.getErrorMessage());
        shareButton.setVisibility(View.GONE);
        finishButton.setVisibility(View.GONE);
    }

    @Override
    public void setSuccess(BSPagoResponseEntity result) {
        activity.setStreenStatus(ContainerScreenView.STATUS_FINISHED);
        resultImg.setImageDrawable(SUCCESS);
        resultTitleView.setText(SUCCESS_TITLE);
        resultView.setText(presenter.buildSuccessMsg(RESULT_MSG));
        shareButton.setVisibility(View.VISIBLE);
        finishButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.b_viamericas_share)
    @Override
    public void clickShare() {
        disposables.add(
                rxPermissions.request(permissions).subscribe(aBoolean -> {
                    if (aBoolean) {
                        shareScreenshot();
                    }
                })
        );
    }

    @OnClick(R.id.b_viamericas_ok)
    @Override
    public void clickFinish() {
        activity.finish();
    }


    private Bitmap getScreenshot(View view) {
        if (isVisible()) {
            View screenView = view.getRootView();
            screenView.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
            screenView.setDrawingCacheEnabled(false);
            return bitmap;
        }
        return null;
    }

    private File store(Bitmap bm, String fileName) {
        final String dirPath =
                Environment.getExternalStorageDirectory().getAbsolutePath() + SCREENSHOT_PATH;
        File dir = new File(dirPath);
        if (!dir.exists()) dir.mkdirs();
        File file = new File(dirPath, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    private void shareImage(File file) {
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");

        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        try {
            startActivity(Intent.createChooser(intent, "Share Screenshot"));
        } catch (ActivityNotFoundException e) {
            activity.showError("No App Available");
        }
    }

    private void shareScreenshot() {
        if (isVisible()) {
            View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
            Bitmap sc = getScreenshot(rootView);
            File file = store(sc, presenter.buildScreenShotName());
            shareImage(file);
        }
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }
}
