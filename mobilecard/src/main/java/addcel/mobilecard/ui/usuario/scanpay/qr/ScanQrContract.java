package addcel.mobilecard.ui.usuario.scanpay.qr;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.data.net.scanpay.model.SPQrEntity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * ADDCEL on 21/08/18.
 */
public interface ScanQrContract {
    interface View extends ZXingScannerView.ResultHandler {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void onScanSuccess(SPQrEntity pagoEntity);

        void onIdReceived(@NotNull String id);

        void clickId();
    }

    interface Presenter {
        void validateScanResult(@NonNull String result);

        long getIdUsuario();

        int getIdPais();
    }
}
