package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search

import addcel.mobilecard.data.net.colombia.multimarket.Servicio
import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil

/**
 * ADDCEL on 2019-11-26.
 */
class MmServicioDiffUtilCallback(
        private val oldList: List<Servicio>,
        private val newList: List<Servicio>
) : DiffUtil.Callback() {
    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].productId == newList[newItemPosition].productId

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].name == newList[newItemPosition].name

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldItem: Servicio = oldList[oldItemPosition]
        val newItem: Servicio = newList[newItemPosition]

        val diff = Bundle()

        if (oldItem.name != newItem.name) {
            diff.putString("name", newItem.name)
        }

        return if (diff.size() == 0) {
            null
        } else diff
    }
}