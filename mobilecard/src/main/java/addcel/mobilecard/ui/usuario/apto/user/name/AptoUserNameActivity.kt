package addcel.mobilecard.ui.usuario.apto.user.name

import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.user.UserAPI
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.apto.user.date.AptoUserDateActivity
import addcel.mobilecard.ui.usuario.apto.user.date.AptoUserDateModel
import addcel.mobilecard.utils.AndroidUtils
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_user_name.*

@Parcelize
data class AptoUserNameModel(
        val phone: String, val phoneVerification: String,
        val email: String, val emailVerification: String
) : Parcelable

interface AptoUserNameView : ScreenView, TextWatcher {
    fun clickContinue()
}

class AptoUserNameActivity : AppCompatActivity(), AptoUserNameView {

    companion object {
        fun enableButton(vararg tils: TextInputLayout): Boolean {
            var enabled = true
            for (t: TextInputLayout in tils) {
                if (t.error != null && AndroidUtils.getText(t.editText).isNotEmpty()) {
                    enabled = false
                    break
                }
            }
            return enabled
        }

        fun get(context: Context, model: AptoUserNameModel): Intent {
            return Intent(context, AptoUserNameActivity::class.java).putExtra("model", model)
        }
    }

    val api = UserAPI.get()
    val compositeDisposable = CompositeDisposable()
    lateinit var model: AptoUserNameModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_user_name)

        model = intent?.getParcelableExtra("model")!!

        text_shift_name_verification_email.addTextChangedListener(this)
        AndroidUtils.setText(text_shift_name_verification_email, model.email)

        text_shift_name_verification_first_name.addTextChangedListener(this)
        text_shift_name_verification_last_name.addTextChangedListener(this)

        b_shift_name_verification_continuar.setOnClickListener { clickContinue() }
    }

    override fun clickContinue() {
        val pModel = AptoUserDateModel(
                model.phone, model.phoneVerification, model.email,
                model.emailVerification, AndroidUtils.getText(text_shift_name_verification_first_name),
                AndroidUtils.getText(text_shift_name_verification_last_name)
        )
        startActivity(AptoUserDateActivity.get(this, pModel))
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (til_shift_name_verification_first_name.hasFocus()) {
                if (p0.isEmpty()) {
                    til_shift_name_verification_first_name.error = "Enter a valid first name"
                } else {
                    til_shift_name_verification_first_name.error = null
                }
            }

            if (til_shift_name_verification_last_name.hasFocus()) {
                if (p0.isEmpty()) {
                    til_shift_name_verification_last_name.error = "Enter a valid last name"
                } else {
                    til_shift_name_verification_last_name.error = null
                }
            }

            if (til_shift_name_verification_email.hasFocus()) {
                if (!Patterns.EMAIL_ADDRESS.matcher(p0).matches()) {
                    text_shift_name_verification_email.error = "Enter a valid email address"
                } else {
                    text_shift_name_verification_email.error = null
                }
            }
            b_shift_name_verification_continuar.isEnabled =
                    enableButton(
                            til_shift_name_verification_first_name,
                            til_shift_name_verification_last_name, til_shift_name_verification_email
                    )
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}
