package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add.MxTransferBeneficiarioAddFragment;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.update.MxTransferBeneficiarioUpdateFragment;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import dagger.Lazy;

/**
 * ADDCEL on 08/11/17.
 */
public class MxTransferBeneficiarioSelectFragment extends Fragment
        implements MxTransferBeneficiarioSelectContract.View {
    @BindView(R.id.spinner_transfermx_beneficiarios_select_beneficiarios)
    Spinner beneficiarioSpinner;

    @BindView(R.id.til_transfermx_select_monto)
    TextInputLayout montoTil;

    @BindView(R.id.view_transfermx_select_comision)
    TextView comisionView;

    @BindView(R.id.view_transfermx_select_total)
    TextView totalView;

    @BindView(R.id.b_transfermx_beneficiarios_select_delete)
    ImageButton deleteButton;

    @BindView(R.id.b_transfermx_beneficiarios_select_update)
    ImageButton updateButton;

    @Inject
    MxTransferActivity activity;
    @Inject
    ArrayAdapter<AccountEntity> beneficiarioAdapter;
    @Inject
    Bus bus;

    @Inject
    @Named("okListener")
    DetachableClickListener okListener;

    @Inject
    @Named("cancelListener")
    DetachableClickListener cancelListener;

    @Inject
    Lazy<AlertDialog> deleteDialog;
    @Inject
    MxTransferBeneficiarioSelectContract.Presenter presenter;

    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .mxTransferBeneficiarioSelectSubcomponent(new MxTransferBeneficiarioSelectModule(this))
                .inject(this);
        bus.register(this);
        presenter.getBeneficiarios();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_mx_transfer_beneficiario_select, container, false);
        unbinder = ButterKnife.bind(this, view);
        beneficiarioSpinner.setAdapter(beneficiarioAdapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        okListener.clearOnDetach(deleteDialog.get());
        cancelListener.clearOnDetach(deleteDialog.get());

        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        activity.findViewById(R.id.b_success_share).setVisibility(View.GONE);
        setComision("0");
        setTotal("0");
        if (beneficiarioAdapter.getCount() > 0) {
            beneficiarioSpinner.setSelection(0);
            deleteButton.setEnabled(true);
            updateButton.setEnabled(true);
        } else {
            deleteButton.setEnabled(false);
            updateButton.setEnabled(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    public void showProgress() {
        if (activity != null) activity.showProgress();
    }

    @Override
    public void hideProgress() {
        if (activity != null) activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void updateBeneficiarioAdapter(List<AccountEntity> accounts) {
        if (beneficiarioAdapter.getCount() > 0) beneficiarioAdapter.clear();
        beneficiarioAdapter.addAll(accounts);
        beneficiarioSpinner.setSelection(0, true);
    }

    @Override
    public void setBeneficiarios(List<AccountEntity> accounts) {
        if (getView() != null) {
            updateBeneficiarioAdapter(accounts);
            if (beneficiarioAdapter.getCount() == 0) {
                deleteButton.setEnabled(false);
                updateButton.setEnabled(false);
                montoTil.setEnabled(false);
            } else {
                deleteButton.setEnabled(true);
                updateButton.setEnabled(true);
                montoTil.setEnabled(true);
            }
        }
    }

    @Subscribe
    @Override
    public void updateBeneficiarios(MxTransferBeneficiarioSelectEvent event) {
        setBeneficiarios(event.getAccounts());
    }

    @OnItemSelected(value = R.id.spinner_transfermx_beneficiarios_select_beneficiarios, callback = OnItemSelected.Callback.ITEM_SELECTED)
    @Override
    public void onBeneficiarioSelected(int pos) {
        onMontoCaptured(getCapturedMonto());
        deleteButton.setEnabled(true);
        updateButton.setEnabled(true);
    }

    @Override
    public AccountEntity getSelectedAccount() {
        return (AccountEntity) beneficiarioSpinner.getSelectedItem();
    }

    @Override
    public String getCapturedMonto() {
        return Strings.nullToEmpty(Objects.requireNonNull(montoTil.getEditText()).getText().toString())
                .trim();
    }

    @OnItemSelected(value = R.id.spinner_transfermx_beneficiarios_select_beneficiarios, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    @Override
    public void onBeneficiarioNotSelected() {
        deleteButton.setEnabled(false);
        updateButton.setEnabled(false);
    }

    @OnClick(R.id.b_transfermx_beneficiarios_select_delete)
    @Override
    public void clickDelete() {
        CharSequence sequence =
                Phrase.from(activity, R.string.txt_transfermx_beneficiarios_select_delete)
                        .put("beneficiario", getSelectedAccount().getAlias())
                        .format();
        deleteDialog.get().setMessage(sequence);
        deleteDialog.get().show();
    }

    @OnClick(R.id.b_transfermx_beneficiarios_select_update)
    @Override
    public void clickUpdate() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_transfermx, MxTransferBeneficiarioUpdateFragment.get(getSelectedAccount()))
                .hide(this)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @OnClick(R.id.b_transfermx_beneficiarios_select_add)
    @Override
    public void clickAgregar() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_transfermx, new MxTransferBeneficiarioAddFragment())
                .hide(this)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @OnTextChanged(value = R.id.text_transfermx_select_monto, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void onMontoCaptured(CharSequence sequence) {
        if (!Strings.isNullOrEmpty(sequence.toString())) {
            setComision(sequence.toString());
            setTotal(sequence.toString());
        } else {
            setComision("0");
            setTotal("0");
        }
    }

    @Override
    public void setComision(String monto) {
        if (getView() != null) {
            CharSequence sComision =
                    Phrase.from(getView(), R.string.txt_transfermx_beneficiarios_comision_amount)
                            .put("comision", presenter.getComision(monto, getSelectedAccount()))
                            .format();
            comisionView.setText(sComision);
        }
    }

    @Override
    public void setTotal(String monto) {
        if (getView() != null) {
            CharSequence sTotal =
                    Phrase.from(getView(), R.string.txt_transfermx_beneficiarios_total_amount)
                            .put("total", presenter.getTotal(monto, getSelectedAccount()))
                            .format();
            totalView.setText(sTotal);
        }
    }

    @Override
    public boolean validate() {
        if (Strings.isNullOrEmpty(Objects.requireNonNull(montoTil.getEditText()).getText().toString())
                || Double.valueOf(getCapturedMonto()) <= 0) {
            montoTil.setError(getText(R.string.error_cantidad));
            return false;
        } else {
            montoTil.setError(null);
            return true;
        }
    }

    @OnClick(R.id.b_transfermx_beneficiarios_select_continue)
    @Override
    public void clickContinuar() {
        if (validate()) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("account", getSelectedAccount());
            bundle.putDouble("monto", Double.valueOf(getCapturedMonto()));
            bundle.putDouble("comision",
                    presenter.getDoubleComision(getCapturedMonto(), getSelectedAccount()));

            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_transfermx,
                            WalletSelectFragment.get(WalletSelectFragment.TRANSFER_MX, bundle))
                    .addToBackStack(null)
                    .hide(this)
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .commit();
        }
    }
}
