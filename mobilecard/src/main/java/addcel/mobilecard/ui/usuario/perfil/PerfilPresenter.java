package addcel.mobilecard.ui.usuario.perfil;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.model.DataUpdateResponse;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.UserValidation;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.usuario.perfil.PerfilInteractor;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.Observable;
import mx.mobilecard.crypto.AddcelCrypto;
import timber.log.Timber;

/**
 * ADDCEL on 06/10/17.
 */
class PerfilPresenter implements PerfilContract.Presenter {

    private final PerfilInteractor interactor;
    private final PerfilContract.View view;

    PerfilPresenter(PerfilInteractor interactor, PerfilContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public long getIdUsuario() {
        return interactor.getIdUsuario();
    }

    @Override
    public String getNombre() {
        return interactor.getNombre();
    }

    @Override
    public String getApellido() {
        return interactor.getApellido();
    }

    @Override
    public String getEmail() {
        return interactor.getEmail();
    }

    @Override
    public String getPassword() {
        return AddcelCrypto.decryptHard(interactor.getPassword());
    }

    @SuppressLint("CheckResult")
    @Override
    public void getPaises(boolean withRetry) {
        view.showProgress();
        interactor.getPaises(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<PaisResponse.PaisEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<PaisResponse.PaisEntity> result) {
                        view.hideProgress();
                        if (withRetry && result.isEmpty()) {
                            view.notifyPaisesError(
                                    "Catálogo de países no disponible. Para descargar de nuevo, presiona reintentar");
                        } else {
                            view.notifyPaises(result);
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        if (withRetry) {
                            view.notifyPaisesError(
                                    "Catálogo de países no disponible. Para descargar de nuevo, presiona reintentar");
                        } else {
                            view.onError(error);
                        }
                    }
                });
    }

    @Override
    public int getPais() {
        return interactor.getPais();
    }

    @Override
    public String getCelular() {
        return interactor.getCelular();
    }

    @Override
    public void loadImage() {
        view.showProgress();
        interactor.loadImageAsBitmap();
    }

    @Override
    public void updatePassword(String password) {
        view.showProgress();
        interactor.updatePassword(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                AddcelCrypto.encryptHard(password), new InteractorCallback<McResponse>() {
                    @Override
                    public void onSuccess(@NotNull McResponse result) {
                        view.hideProgress();
                        view.notifyPasswordUpdate();
                        view.onSuccess(result.getMensajeError());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.onError(error);
                    }
                });
    }

    @Override
    public void updateProfile(String path) {
        view.showProgress();
        try {
            interactor.updateImage(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                    encodeImage(path));
        } catch (IOException e) {
            view.onError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.IMAGE));
        }
    }

    @Override
    public String encodeImage(String path) throws IOException {
        File imagefile = new File(path);
        FileInputStream fis;
        fis = new FileInputStream(imagefile);
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Timber.d("Tamaño array: %d", baos.size());
        bm.compress(Bitmap.CompressFormat.JPEG, 10, baos);
        byte[] b = baos.toByteArray();
        fis.close();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @SuppressLint("CheckResult")
    @Override
    public void save() {
        int idApp = BuildConfig.ADDCEL_APP_ID;
        String idioma = StringUtil.getCurrentLanguage();

        Observable<DataUpdateResponse> nameObservable =
                interactor.getUpdateInfoObservable(idApp, idioma, NAME_NOMBRE,
                        StringUtil.removeAcentosMultiple(view.getNombre()));
        Observable<DataUpdateResponse> lastNameObservable =
                interactor.getUpdateInfoObservable(idApp, idioma, NAME_APELLIDO,
                        StringUtil.removeAcentosMultiple(view.getApellido()));
        Observable<DataUpdateResponse> paisObservable =
                interactor.getUpdateInfoObservable(idApp, idioma, NAME_PAIS,
                        String.valueOf(view.getPais()));
        List<Observable<DataUpdateResponse>> observables =
                Arrays.asList(nameObservable, lastNameObservable, paisObservable);

        view.showProgress();
        interactor.updateInfo(idApp, idioma, observables, new InteractorCallback<UserValidation>() {
            @Override
            public void onSuccess(@NotNull UserValidation result) {
                view.hideProgress();
                view.notifyUpdate();
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.onError(error);
            }
        });
    }
}
