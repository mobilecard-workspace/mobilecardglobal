package addcel.mobilecard.ui.usuario.registro.formapago;

import android.app.DatePickerDialog;

import com.mobsandgeeks.saripaar.Validator;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

import addcel.mobilecard.data.net.usuarios.model.RegistroResponse;
import addcel.mobilecard.data.net.usuarios.model.TerminosResponse;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;

/**
 * ADDCEL on 29/09/17.
 */
interface RegistroFormaPagoContract {
    interface View extends Validator.ValidationListener, DatePickerDialog.OnDateSetListener {
        void validateTarjetaOnCapture(CharSequence s);

        void validateVigenciaOnCapture();

        String getImei();

        String getPan();

        String getHolderName();

        String getVigencia();

        String getCvv();

        String getDomicilio();

        String getZip();

        TipoTarjetaEntity getTipoTarjeta();

        boolean areTerminosChecked();

        void clickPrivacy();

        void clickTerminos();

        void clickInfo();

        void clickContinue();

        void showProgress();

        void hideProgress();

        void showError(int resId);

        void showError(String msg);

        void showSuccess(String msg);

        void onRegistroSuccess(RegistroResponse response);

        void finish();

        void onPrivacy(String privacy);

        void showTerminosDialog(TerminosResponse terminosResponse);
    }

    interface Presenter {

        String getNombre();

        String getApellido();

        Calendar getCalendarVigencia();

        String getVigenciaFormatted();

        void updateCalendarVigencia(int year, int month);

        void getPrivacy();

        void getTerminos();

        void saveUser(@NotNull String profile);
    }
}
