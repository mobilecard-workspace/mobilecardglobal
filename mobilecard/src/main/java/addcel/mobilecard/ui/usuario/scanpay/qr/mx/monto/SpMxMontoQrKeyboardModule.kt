package addcel.mobilecard.ui.usuario.scanpay.qr.mx.monto

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.di.scope.PerFragment
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import retrofit2.Retrofit

/**
 * ADDCEL on 2020-01-17.
 */

@Module
class SpMxMontoQrKeyboardModule(private val fragment: SpMxMontoQrKeyboardFragment) {

    @PerFragment
    @Provides
    fun provideViewModel(): SpMxMontoQrKeyboardViewModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideApi(r: Retrofit): SPApi {
        return SPApi.provide(r)
    }


    @PerFragment
    @Provides
    fun providePresenter(spApi: SPApi, sessionOperations: SessionOperations): SpMxMontoQrKeyboardPresenter {
        return SpMxMontoQrKeyboardPresenterImpl(spApi, sessionOperations, fragment)
    }
}

@PerFragment
@Subcomponent(modules = [SpMxMontoQrKeyboardModule::class])
interface SpMxMontoQrKeyboardSubcomponent {
    fun inject(fragment: SpMxMontoQrKeyboardFragment)
}