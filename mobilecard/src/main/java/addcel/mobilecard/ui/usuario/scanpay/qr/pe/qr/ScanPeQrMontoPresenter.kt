package addcel.mobilecard.ui.usuario.scanpay.qr.pe.qr

import addcel.mobilecard.data.net.qr.QrHexaRequest
import addcel.mobilecard.data.net.qr.VisaQrWalletAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-07-17.
 */
interface ScanPeQrMontoPresenter {
    fun clearDisposables()
    fun decodeQr(hexa: String)
}

class ScanPeQrMontoPresenterImpl(
        val api: VisaQrWalletAPI, val disposables: CompositeDisposable,
        val view: View
) : ScanPeQrMontoPresenter {

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun decodeQr(hexa: String) {
        view.showProgress()
        disposables.add(
                api.qrDecodeHexadecimal(QrHexaRequest(hexa)).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ qr ->
                    view.hideProgress()
                    view.onDecode(qr)
                }, { t ->
                    t.printStackTrace()
                    view.hideProgress()
                    view.showError("Ocurrió un error al procesar el código QR")
                })
        )
    }
}