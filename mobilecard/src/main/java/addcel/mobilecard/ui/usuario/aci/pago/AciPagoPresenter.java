package addcel.mobilecard.ui.usuario.aci.pago;

import android.annotation.SuppressLint;
import android.os.Build;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.squareup.otto.Bus;

import java.text.NumberFormat;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.PagoRequest;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.impl.AciAddressReceivedEvent;
import addcel.mobilecard.event.impl.ErrorEvent;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 23/03/17.
 */
public class AciPagoPresenter implements AciPagoContract.Presenter {
    private final AciService service;
    private final FavoritoDaoRx dao;
    private final ServiceModel model;
    private final SessionOperations session;
    private final Bus bus;
    private final CompositeDisposable disposables;
    private final NumberFormat usdCurrFormat;
    private final AciPagoContract.View view;

    AciPagoPresenter(AciService service, FavoritoDaoRx dao, ServiceModel model,
                     SessionOperations session, Bus bus, NumberFormat format, AciPagoContract.View view) {
        this.service = service;
        this.dao = dao;
        this.model = model;
        this.session = session;
        this.bus = bus;
        this.disposables = new CompositeDisposable();
        this.usdCurrFormat = format;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getAddresses() {
        view.showProgress();
        disposables.add(service.getAddresses((int) model.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(addresses -> {
                    view.hideProgress();
                    bus.post(new AciAddressReceivedEvent(addresses));
                }, throwable -> {
                    view.hideProgress();
                    bus.post(Lists.newArrayList());
                }));
    }

    @Override
    public String calculaComision(double monto) {
        return usdCurrFormat.format(monto * model.getPctComision());
    }

    @SuppressLint("CheckResult")
    @Override
    public void saveToFav(ServiceModel servicio, int logo, String etiqueta) {
        Favorito favorito = new Favorito();
        favorito.setCategoria(Favorito.CAT_SERVICIO_US);
        favorito.setNombre(model.getName());
        favorito.setPais(session.getUsuario().getIdPais());
        favorito.setDescripcion(etiqueta);
        favorito.setLogotipo(logo);
        favorito.setMontos("");
        favorito.setServicio(JsonUtil.toJson(model));

        Single<Long> single = Single.fromCallable(() -> dao.insert(favorito))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        disposables.add(single.subscribe(aLong -> {
            if (aLong > 0) {
                bus.post(new ErrorEvent(R.string.txt_favoritos_success, "", false));
            } else {
                bus.post(new ErrorEvent(R.string.error_default, "", false));
            }
        }, throwable -> bus.post(new ErrorEvent(R.string.error_default, "", false))));
    }

    @SuppressLint("CheckResult")
    @Override
    public void pagar(Address address, String cuenta, double monto, CardEntity cardCard) {
        view.showProgress();
        PagoRequest request = new PagoRequest.Builder(StringUtil.getCurrentLanguage(),
                session.getUsuario().getIdeUsuario(), cardCard.getIdTarjeta()).setCiudadServicio(
                address.getCity())
                .setDireccionServicio(address.getStreet())
                .setIdServicio(String.valueOf(model.getId()))
                .setImei(view.getIMEI())
                .setModelo(Build.MODEL)
                .setMonto(monto)
                .setNombreServicio(model.getName())
                .setReferencia(cuenta)
                .setSoftware(String.valueOf(Build.VERSION.SDK_INT))
                .setWkey(view.getIMEI())
                .build();

        disposables.add(service.payment(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mcResponse -> {
                    view.hideProgress();
                    bus.post(mcResponse);
                }, throwable -> {
                    view.hideProgress();
                    view.showError(Strings.nullToEmpty(throwable.getLocalizedMessage()));
                }));
    }
}
