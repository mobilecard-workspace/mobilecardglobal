package addcel.mobilecard.ui.usuario.legacy.recargas;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 14/12/16.
 */
@PerFragment
@Subcomponent(modules = CarrierSelectionModule.class)
public interface CarrierSelectionSubcomponent {
    void inject(CarrierSelectionFragment fragment);
}
