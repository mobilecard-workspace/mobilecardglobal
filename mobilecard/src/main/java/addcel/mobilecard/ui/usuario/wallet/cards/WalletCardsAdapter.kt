package addcel.mobilecard.ui.usuario.wallet.cards

import addcel.mobilecard.R
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.FranquiciaEntity
import addcel.mobilecard.utils.StringUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.mobilecard.crypto.AddcelCrypto

internal class WalletCardsAdapter(
        private val models: MutableList<CardEntity>,
        private val desiredWidth: Int,
        private val picasso: Picasso
) : RecyclerView.Adapter<WalletCardsAdapter.ViewHolder>() {
    private var editable: Boolean = false
    private var view: WalletCardsContract.View? = null

    var isEditable: Boolean
        get() = editable
        set(editable) {
            this.editable = editable
            notifyDataSetChanged()
        }

    fun update(models: List<CardEntity>) {
        this.models.clear()
        this.models.addAll(models)
        this.editable = false
        notifyDataSetChanged()
    }

    fun attach(view: WalletCardsContract.View) {
        this.view = view
    }

    fun detach() {
        this.view = null
    }

    fun getItem(pos: Int): CardEntity {
        return this.models[pos]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false),
                picasso, desiredWidth
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]

        if (model.idTarjeta != -1) {
            // AndroidUtils.setText(holder.panV, Strings.nullToEmpty(model.getNombre()));
            holder.panView.text = StringUtil.maskCard(AddcelCrypto.decryptHard(model.pan))
            holder.setFavUI(model)

            holder.favButton.setOnClickListener {
                if (holder.favView.visibility == View.GONE) {
                    holder.showFav()
                } else {
                    holder.hideFav()
                }
            }

            holder.deleteButton.setOnClickListener {
                if (holder.eliminarView.visibility == View.GONE) {
                    holder.showDelete()
                } else {
                    holder.hideDelete()
                }
            }

            holder.favView.setOnClickListener { v ->
                view?.clickFav(model)
                v.visibility = View.GONE
            }

            holder.eliminarView.setOnClickListener { v ->
                view?.clickDelete(model)
                v.visibility = View.GONE
            }

            if (isEditable) {
                holder.deleteButton.visibility =
                        if (!model.determinada) View.VISIBLE else View.GONE
                holder.favButton.visibility = if (!model.determinada) View.VISIBLE else View.GONE
            } else {
                holder.favButton.visibility = View.GONE
                holder.deleteButton.visibility = View.GONE
            }

            holder.loadFranquiciaBg(model)
        } else {

            this.picasso.load(R.drawable.bg_mobilecard_get)
                    .resize(desiredWidth, 0)
                    .centerInside()
                    .error(R.drawable.bg_mobilecard_get)
                    .placeholder(R.drawable.bg_mobilecard_get)
                    .into(holder.fondoIv)

            holder.panView.text = ""
            holder.favButton.visibility = View.GONE
            holder.deleteButton.visibility = View.GONE
            holder.favView.visibility = View.GONE
            holder.favLabel.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return models.size
    }

    internal class ViewHolder(
            itemView: View,
            private val picasso: Picasso,
            private val desiredWidth: Int
    ) : RecyclerView.ViewHolder(itemView) {

        val fondoIv: ImageView = itemView.findViewById(R.id.bg_card)
        val panView: TextView = itemView.findViewById(R.id.pan_card)
        val favLabel: ImageView = itemView.findViewById(R.id.label_wallet_fav)
        val favView: TextView = itemView.findViewById(R.id.shade_card_fav)
        val eliminarView: TextView = itemView.findViewById(R.id.shade_card_eliminar)
        val deleteButton: ImageButton = itemView.findViewById(R.id.b_card_delete)
        val favButton: ImageButton = itemView.findViewById(R.id.b_card_fav)

        private fun getFranquiciaPH(model: CardEntity): Int {
            if (model.tipo == null) return R.drawable.card_visa
            return when (model.tipo) {
                FranquiciaEntity.VISA -> R.drawable.card_visa
                FranquiciaEntity.MasterCard -> R.drawable.card_master
                FranquiciaEntity.AmEx, FranquiciaEntity.Amex -> R.drawable.card_american
                else -> R.drawable.card_carnet
            }
        }

        private fun getFranquiciaError(model: CardEntity): Int {
            if (model.tipo == null) return R.drawable.bg_mobilecard
            return when (model.tipo) {
                FranquiciaEntity.VISA -> R.drawable.bg_visa
                FranquiciaEntity.MasterCard -> R.drawable.bg_mastercard
                FranquiciaEntity.AmEx, FranquiciaEntity.Amex -> R.drawable.bg_amex
                else -> R.drawable.bg_carnet
            }
        }

        fun showDelete() {
            eliminarView.visibility = View.VISIBLE
            favButton.visibility = View.GONE
        }

        fun hideDelete() {
            eliminarView.visibility = View.GONE
            favButton.visibility = View.VISIBLE
        }

        fun showFav() {
            favView.visibility = View.VISIBLE
            deleteButton.visibility = View.GONE
        }

        fun hideFav() {
            favView.visibility = View.GONE
            deleteButton.visibility = View.VISIBLE
        }

        fun setFavUI(model: CardEntity) {
            favLabel.visibility = if (model.determinada) View.VISIBLE else View.GONE
        }

        fun loadFranquiciaBg(model: CardEntity) {
            this.picasso.load(model.imgShort)
                    .resize(this.desiredWidth, 0)
                    .centerInside()
                    .error(getFranquiciaError(model))
                    .placeholder(getFranquiciaPH(model))
                    .into(fondoIv)
        }
    }
}
