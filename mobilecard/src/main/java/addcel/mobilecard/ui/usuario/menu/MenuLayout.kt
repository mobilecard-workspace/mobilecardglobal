package addcel.mobilecard.ui.usuario.menu

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.domain.usuario.bottom.BottomMenuInteractor
import addcel.mobilecard.domain.usuario.bottom.MenuEvent
import addcel.mobilecard.domain.usuario.bottom.UseCase
import addcel.mobilecard.utils.StringUtil
import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.layout_menu.view.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-06-06.
 */

interface Presenter {

    fun clearDisposables()

    fun clickHome()

    fun clickWallet()

    fun clickFrecuentes()

    fun clickHistorial()
}

interface BottomMenuEventListener {
    fun onMenuEventReceived(event: MenuEvent)
}

class MenuLayoutPresenter(val interactor: BottomMenuInteractor) : Presenter {
    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun clickHome() {
        interactor.verify(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), UseCase.HOME)
    }

    override fun clickWallet() {
        interactor.verify(
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                UseCase.WALLET
        )
    }

    override fun clickFrecuentes() {
        interactor.verify(
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                UseCase.CONTACTO
        )
    }

    override fun clickHistorial() {
        interactor.verify(
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                UseCase.HISTORIAL
        )
    }
}

class MenuLayout(context: Context?, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    @Inject
    lateinit var presenter: Presenter

    init {
        if (!isInEditMode) Mobilecard.get().netComponent.bottomMenuSubcomponent(
                BottomMenuModule()
        ).inject(this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        b_menu_home.setOnClickListener {
            presenter.clickHome() //context.startActivity(MainActivity.get(context, false))
            block()
        }

        b_menu_wallet.setOnClickListener {
            presenter.clickWallet()
            block()
        } //context.startActivity(WalletContainerActivity.get(context))

        b_menu_frecuentes.setOnClickListener {
            presenter.clickFrecuentes() //context.startActivity(MainActivity.get(context, false))
            block()
        }
        b_menu_historial.setOnClickListener {
            presenter.clickHistorial() //context.startActivity(EdocuentaActivity.get(context))
            block()
        }
    }

    fun block() {
        b_menu_home.isEnabled = false
        b_menu_wallet.isEnabled = false
        b_menu_frecuentes.isEnabled = false
        b_menu_historial.isEnabled = false
    }

    fun unblock() {
        b_menu_home.isEnabled = true
        b_menu_wallet.isEnabled = true
        b_menu_frecuentes.isEnabled = true
        b_menu_historial.isEnabled = true
    }
}