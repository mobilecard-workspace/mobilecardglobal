package addcel.mobilecard.ui.usuario.wallet.create.pay;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Calendar;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog;
import dagger.Module;
import dagger.Provides;

@Module
public final class WalletCreatePayModule {
    private final WalletCreatePayFragment fragment;

    WalletCreatePayModule(WalletCreatePayFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    Activity provideActivity() {
        return Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    @Named("module")
    int provideModule() {
        return Objects.requireNonNull(fragment.getArguments()).getInt("module");
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    Calendar provideVigenciaCalendar() {
        return Calendar.getInstance();
    }

    @PerFragment
    @Provides
    DatePickerDialog datePickerDialog(Activity activity, Calendar calendar) {
        DatePickerDialog datePickerDialog;
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            datePickerDialog =
                    new FixedHoloDatePickerDialog(activity, fragment, calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog =
                        new DatePickerDialog(activity, R.style.CustomDatePickerDialogTheme, fragment,
                                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                calendar.get(Calendar.DAY_OF_MONTH));
            } else {
                // R.style.CustomDatePickerDialogTheme
                datePickerDialog = new DatePickerDialog(activity, fragment, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            }
        }

        datePickerDialog.getDatePicker()
                .findViewById(Resources.getSystem().getIdentifier("day", "id", "android"))
                .setVisibility(View.GONE);
        return datePickerDialog;
    }

    @PerFragment
    @Provides
    WalletCreatePayContract.Presenter providePresenter(WalletAPI service,
                                                       SessionOperations session) {
        return new WalletCreatePayPresenter(service, session.getUsuario(), fragment);
    }
}
