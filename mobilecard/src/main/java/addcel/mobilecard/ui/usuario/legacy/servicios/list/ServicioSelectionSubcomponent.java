package addcel.mobilecard.ui.usuario.legacy.servicios.list;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 14/12/16.
 */
@PerFragment
@Subcomponent(modules = ServicioSelectionModule.class)
public interface ServicioSelectionSubcomponent {
    void inject(ServicioSelectionFragment fragment);
}
