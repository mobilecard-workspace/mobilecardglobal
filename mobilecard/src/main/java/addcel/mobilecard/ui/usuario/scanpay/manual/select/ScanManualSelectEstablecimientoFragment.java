package addcel.mobilecard.ui.usuario.scanpay.manual.select;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import addcel.mobilecard.ui.usuario.scanpay.manual.monto.ScanManualMontoFragment;

/**
 * ADDCEL on 22/08/18.
 */
public final class ScanManualSelectEstablecimientoFragment extends Fragment
        implements ScanManualSelectEstablecimientoContract.View {
    @Inject
    ScanPayActivity activity;
    @Inject
    ScanManualSelectEstablecimientoAdapter adapter;
    @Inject
    ScanManualSelectEstablecimientoContract.Presenter presenter;
    private RecyclerView establecimientosView;

    public static synchronized ScanManualSelectEstablecimientoFragment get() {
        return new ScanManualSelectEstablecimientoFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .scanManualSelectEstablecimientoSubcomponent(
                        new ScanManualSelectEstablecimientoModule(this))
                .inject(this);

        presenter.register();

        if (adapter.getItemCount() == 0) {
            presenter.downloadEstablecimientos();
        }
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen_scanpay_manual_select, container, false);
        establecimientosView = v.findViewById(R.id.recycler_scanpay_manual_select_establishment);
        configEstablecimientosView(Objects.requireNonNull(establecimientosView));
        EditText establecimientosText = v.findViewById(R.id.text_scanpay_manual_select_search);
        configSearchText(Objects.requireNonNull(establecimientosText));
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setFinished(Boolean.FALSE);
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(establecimientosView);
        establecimientosView.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearCompositeDisposable();
        presenter.unregister();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void onEstablecimientosDownloaded() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onEstablecimientosError(int errorRes) {
        showError(activity.getString(errorRes));
    }

    @Override
    public void filter(CharSequence pattern) {
        presenter.filterList(pattern.toString());
    }

    @Override
    public void setEstablecimientos(List<LcpfEstablecimiento> models) {
        adapter.update(models);
    }

    private void configEstablecimientosView(@NonNull RecyclerView view) {
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(view.getContext()));
        view.addItemDecoration(
                new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL));

        ItemClickSupport.addTo(establecimientosView)
                .setOnItemClickListener((recyclerView, position, v1) -> activity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.frame_scanpay, ScanManualMontoFragment.get(adapter.getItem(position)))
                        .hide(ScanManualSelectEstablecimientoFragment.this)
                        .addToBackStack(null)
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                        .commit());
    }

    private void configSearchText(@NonNull EditText text) {
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s);
            }
        });
    }
}
