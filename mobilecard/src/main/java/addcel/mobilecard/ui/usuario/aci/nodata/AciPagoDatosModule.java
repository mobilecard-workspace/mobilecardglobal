package addcel.mobilecard.ui.usuario.aci.nodata;

import android.widget.ArrayAdapter;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.squareup.otto.Bus;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.Ciudad;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerFragment;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 28/03/17.
 */
@Module
public final class AciPagoDatosModule {
    private final AciPagoDatosFragment fragment;

    AciPagoDatosModule(AciPagoDatosFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ServiceModel provideModel() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelable("service");
    }

    @PerFragment
    @Provides
    String provideCuenta() {
        return Preconditions.checkNotNull(fragment.getArguments()).getString("referencia");
    }

    @PerFragment
    @Provides
    double provideMonto() {
        return Preconditions.checkNotNull(fragment.getArguments()).getDouble("amount");
    }

    @PerFragment
    @Provides
    Address provideAddress() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelable("address");
    }

    @PerFragment
    @Provides
    CardEntity provideFormaPago() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelable("card");
    }

    @PerFragment
    @Provides
    AciService provideAciService(Retrofit retrofit) {
        return retrofit.create(AciService.class);
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.setValidationListener(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        return validator;
    }

    @PerFragment
    @Provides
    ArrayAdapter<EstadoResponse.EstadoEntity> provideAdapter() {
        final ArrayAdapter<EstadoResponse.EstadoEntity> estadoArrayAdapter =
                new ArrayAdapter<>(Preconditions.checkNotNull(fragment.getContext()),
                        android.R.layout.simple_spinner_item);
        estadoArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        estadoArrayAdapter.setNotifyOnChange(true);
        return estadoArrayAdapter;
    }

    @PerFragment
    @Provides
    @Named("ciudadAdapter")
    ArrayAdapter<Ciudad> provideCiudadAdapter() {
        final ArrayAdapter<Ciudad> adapter =
                new ArrayAdapter<>(Preconditions.checkNotNull(fragment.getContext()),
                        android.R.layout.simple_dropdown_item_1line);
        adapter.setNotifyOnChange(true);
        return adapter;
    }

    @PerFragment
    @Provides
    AciPagoDatosContract.Presenter providePresenter(AciService aciService,
                                                    CatalogoService catalogo, SessionOperations operations, ServiceModel model, Address address,
                                                    CardEntity formaPago, String cuenta, double monto, Bus bus) {
        return new AciPagoDatosPresenter(aciService, catalogo, operations, model, address, formaPago,
                cuenta, monto, bus, fragment);
    }
}
