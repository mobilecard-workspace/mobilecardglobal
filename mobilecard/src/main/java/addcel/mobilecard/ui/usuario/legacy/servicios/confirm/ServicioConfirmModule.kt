package addcel.mobilecard.ui.usuario.legacy.servicios.confirm

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse
import addcel.mobilecard.data.net.pago.BPApi
import addcel.mobilecard.data.net.pago.BPApi.Companion.get
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.token.TokenizerAPI.Companion.provideTokenizerAPI
import addcel.mobilecard.data.net.transacto.entity.SaldoEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.domain.billpocket.BillPocketInteractor
import addcel.mobilecard.domain.billpocket.BillPocketInteractorImpl
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import java.text.NumberFormat

/**
 * ADDCEL on 19/10/17.
 */
@Module
class ServicioConfirmModule(private val fragment: ServicioConfirmFragment) {
    @PerFragment
    @Provides
    fun provideActivity(): PagoContainerActivity {
        return fragment.activity as PagoContainerActivity
    }

    @PerFragment
    @Provides
    fun providePagoApi(retrofit: Retrofit): BPApi {
        return get(retrofit)
    }

    @PerFragment
    @Provides
    fun provideTokenizer(retrofit: Retrofit): TokenizerAPI {
        return provideTokenizerAPI(retrofit)
    }

    @PerFragment
    @Provides
    fun provideInteractor(api: BPApi, tokenizer: TokenizerAPI,
                          session: SessionOperations): BillPocketInteractor {
        return BillPocketInteractorImpl(api, tokenizer, session.usuario,
                session.minimalLocationData, CompositeDisposable())
    }

    @PerFragment
    @Provides
    fun providePresenter(interactor: BillPocketInteractor, numberFormat: NumberFormat): ServicioConfirmPresenter {
        val servicio: ServicioResponse.ServicioEntity = fragment.arguments?.getParcelable("servicio")!!
        val cardEntity: CardEntity = fragment.arguments?.getParcelable("card")!!
        val saldoEntity: SaldoEntity = fragment.arguments?.getParcelable("saldo")!!
        return ServicioConfirmPresenterImpl(interactor, servicio, cardEntity, saldoEntity, numberFormat, fragment)
    }

}

/**
 * ADDCEL on 19/10/17.
 */
@PerFragment
@Subcomponent(modules = [ServicioConfirmModule::class])
interface ServicioConfirmSubcomponent {
    fun inject(fragment: ServicioConfirmFragment)
}