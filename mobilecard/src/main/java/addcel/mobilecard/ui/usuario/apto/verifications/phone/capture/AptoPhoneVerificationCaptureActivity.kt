package addcel.mobilecard.ui.usuario.apto.verifications.phone.capture

import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.verification.DataPointType
import addcel.mobilecard.data.net.apto.verification.Verification
import addcel.mobilecard.data.net.apto.verification.VerificationAPI
import addcel.mobilecard.data.net.apto.verification.VerificationResponse
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.apto.verifications.phone.code.AptoPhoneVerificationCodeActivity
import addcel.mobilecard.ui.usuario.apto.verifications.phone.code.AptoPhoneVerificationCodeModel
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.Retrofit2HttpErrorHandling
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.isDigitsOnly
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_apto_phone_verification_capture.*

interface AptoPhoneVerificationCaptureView : ScreenView, TextWatcher {
    fun onVerification(result: VerificationResponse)
}

class AptoPhoneVerificationCaptureActivity : AppCompatActivity(),
        AptoPhoneVerificationCaptureView {

    companion object {
        private const val USA_CODE = 1

        private fun valueInTextToLong(text: EditText): Long {
            val str = AndroidUtils.getText(text)
            if (str.isNotEmpty() && str.isDigitsOnly()) {
                return str.toLong()
            }
            throw NumberFormatException("Edit text code must be digits")
        }
    }

    override fun onVerification(result: VerificationResponse) {
        if (result.status == VerificationAPI.STATUS_PENDING) {
            showSuccess("${result.type} ${result.status}")
            val model = AptoPhoneVerificationCodeModel(
                    AndroidUtils.getText(text_shift_phone_verification_phone), result
            )
            startActivity(AptoPhoneVerificationCodeActivity.get(this, model))
        } else {
            showError("${result.type} ${result.status}")
        }
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (text_shift_phone_verification_phone.hasFocus() && p0.length == 10) {
                val body = Verification(
                        DataPointType.phone, mapOf(
                        "country_code" to USA_CODE,
                        "phone_number" to valueInTextToLong(text_shift_phone_verification_phone)
                )
                )

                compositeDisposable.add(
                        api.start(body)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        { r -> onVerification(r) },
                                        { t ->
                                            val errorRes = Retrofit2HttpErrorHandling.getBodyFromErrorBody(
                                                    t,
                                                    VerificationResponse::class.java
                                            )
                                            if (errorRes != null)
                                                onVerification(errorRes)
                                            else
                                                showError(
                                                        t.localizedMessage
                                                                ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)
                                                )
                                        }
                                )
                )
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    val api: VerificationAPI = VerificationAPI.get()
    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_phone_verification_capture)
        text_shift_phone_verification_phone.addTextChangedListener(this)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
