package addcel.mobilecard.ui.usuario.apto.cards.activation

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.apto.accounts.AccountsAPI
import addcel.mobilecard.data.net.apto.accounts.AptoCardData
import addcel.mobilecard.data.net.wallet.AptoCardDataRequest
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.ui.usuario.apto.cards.account.AptoAccountActivity
import addcel.mobilecard.ui.usuario.apto.cards.account.AptoAccountModel
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_card_activation.*
import mx.mobilecard.crypto.AddcelCrypto
import javax.inject.Inject

@Parcelize
data class CardActivationModel(val userToken: String, val accountId: String) : Parcelable

class CardActivationActivity : AppCompatActivity() {

    companion object {
        fun get(context: Context, model: CardActivationModel): Intent {
            return Intent(context, CardActivationActivity::class.java).putExtra("model", model)
        }
    }

    val api = AccountsAPI.get(BuildConfig.APTO_URL)
    private val vaultApi = AccountsAPI.get(BuildConfig.APTO_VAULT_URL)
    val compositeDisposable = CompositeDisposable()

    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var wallet: WalletAPI

    lateinit var model: CardActivationModel
    lateinit var card: AptoCardData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.inject(this)
        setContentView(R.layout.activity_card_activation)
        model = intent?.getParcelableExtra("model")!!
        b_activation_continue.setOnClickListener {
            compositeDisposable.add(checkActivation())
        }
    }

    private fun checkActivation(): Disposable {
        return api.getAccount("Bearer " + model.userToken, model.accountId)
                .subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({
                    card = it
                    if (card.state == "active") {
                        storeCard()
                    } else {
                        Toasty.error(
                                this@CardActivationActivity, "La tarjeta no ha sido activada."
                        ).show()
                    }
                }, {
                    Toasty.error(
                            this@CardActivationActivity, ErrorUtil.getFormattedHttpErrorMsg(it)
                    ).show()

                })
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun storeCard() {
        compositeDisposable.add(
                vaultApi.details("Bearer ${model.userToken}", card.accountId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .flatMap {
                            val aData = AptoCardDataRequest(
                                    card.accountId,
                                    AddcelCrypto.encryptHard(it.cvv),
                                    session.usuario.ideUsuario,
                                    card.nameOnCard,
                                    AddcelCrypto.encryptHard(it.pan),
                                    AddcelCrypto.encryptHard(it.expiration)
                            )
                            wallet.addAptoCard(BuildConfig.ADDCEL_APP_ID, aData)
                        }
                        .subscribe({
                            if (it.idError == 0) {
                                getAptoInfoFromMc()
                            } else {
                                Toasty.error(this@CardActivationActivity, it.mensajeError).show()
                            }
                        }, {
                            Toasty.error(this@CardActivationActivity, "Error de servidor interno").show()
                        })
        )
    }


    private fun getAptoInfoFromMc() {
        compositeDisposable.add(
                wallet.getMobilecardCard(
                        BuildConfig.ADDCEL_APP_ID,
                        session.usuario.ideUsuario,
                        StringUtil.getCurrentLanguage(),
                        session.usuario.idPais
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it.idError == 0) {
                                startActivity(
                                        AptoAccountActivity.get(
                                                this@CardActivationActivity,
                                                AptoAccountModel(model.userToken, it.card)
                                        )
                                )
                                finish()
                            } else {
                                Toasty.error(
                                        this@CardActivationActivity,
                                        "Error desconocido contacte a un administrador"
                                ).show()
                            }
                        }, {
                            Toasty.error(
                                    this@CardActivationActivity,
                                    ErrorUtil.getFormattedHttpErrorMsg(it)
                            )
                        })
        )
    }
}
