package addcel.mobilecard.ui.usuario.aci.list;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 22/03/17.
 */
@PerFragment
@Subcomponent(modules = AciListModule.class)
public interface AciListSubcomponent {
    void inject(AciListFragment fragment);
}
