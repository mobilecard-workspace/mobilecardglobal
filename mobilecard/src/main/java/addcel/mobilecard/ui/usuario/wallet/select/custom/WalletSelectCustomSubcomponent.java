package addcel.mobilecard.ui.usuario.wallet.select.custom;

import addcel.mobilecard.di.scope.PerView;
import dagger.Subcomponent;

/**
 * ADDCEL on 24/08/18.
 */
@PerView
@Subcomponent(modules = WalletSelectCustomModule.class)
public interface WalletSelectCustomSubcomponent {
    void inject(WalletSelectLayout layout);
}
