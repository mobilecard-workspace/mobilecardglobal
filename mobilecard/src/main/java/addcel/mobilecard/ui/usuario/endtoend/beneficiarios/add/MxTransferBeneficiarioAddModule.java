package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add;

import android.widget.ArrayAdapter;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.hth.BankEntity;
import addcel.mobilecard.data.net.hth.H2HApi;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.h2h.H2HAccountInteractor;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

/**
 * ADDCEL on 09/11/17.
 */
@Module
public class MxTransferBeneficiarioAddModule {
    private final MxTransferBeneficiarioAddFragment fragment;

    MxTransferBeneficiarioAddModule(MxTransferBeneficiarioAddFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    boolean fromKeyboard() {
        return fragment.getArguments() != null && fragment.getArguments().getBoolean("fromKeyboard");
    }

    @PerFragment
    @Provides
    MxTransferActivity provideAdActivity() {
        return (MxTransferActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    ArrayAdapter<BankEntity> provideAdapter(MxTransferActivity activity) {
        ArrayAdapter<BankEntity> adapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        adapter.setNotifyOnChange(true);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        return adapter;
    }

    @PerFragment
    @Provides
    H2HApi provideService(Retrofit retrofit) {
        return H2HApi.Companion.get(retrofit);
    }

    @PerFragment
    @Provides
    H2HAccountInteractor provideInteractor(H2HApi api,
                                           SessionOperations session) {
        return new H2HAccountInteractor(api, session, new CompositeDisposable());
    }

    @PerFragment
    @Provides
    MxTransferBeneficiarioAddContract.Presenter providePresenter(
            H2HAccountInteractor interactor) {
        return new MxTransferBeneficiarioAddPresenter(interactor, fragment);
    }
}
