package addcel.mobilecard.ui.usuario.aci.confirmation;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import addcel.mobilecard.ui.usuario.aci.result.AciResultFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.DeviceUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * ADDCEL on 16/10/17.
 */
public final class AciConfirmationFragment extends Fragment
        implements AciConfirmationContract.View {

    @BindView(R.id.til_aci_confirm_account)
    TextInputLayout accountTil;
    @BindView(R.id.til_aci_confirm_amount)
    TextInputLayout montoTil;
    @BindView(R.id.til_aci_confirm_comision)
    TextInputLayout feeTil;
    @BindView(R.id.til_aci_confirm_total)
    TextInputLayout totalTil;
    @Inject
    AciActivity aciActivity;
    @Inject
    SessionOperations session;
    @Inject
    AciConfirmationContract.Presenter presenter;
    @Inject
    NumberFormat currFormat;
    private Unbinder unbinder;

    public AciConfirmationFragment() {
    }

    public static AciConfirmationFragment get(Bundle bundle) {
        AciConfirmationFragment fragment = new AciConfirmationFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .aciConfirmationSubcomponent(new AciConfirmationModule(this))
                .inject(this);
    }

    @NotNull
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_aci_confirm, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AndroidUtils.setText(accountTil.getEditText(), presenter.getAccount());
        AndroidUtils.setText(montoTil.getEditText(), currFormat.format(presenter.getMonto()));
        AndroidUtils.setText(feeTil.getEditText(), currFormat.format(presenter.getFee()));
        AndroidUtils.setText(totalTil.getEditText(),
                currFormat.format(presenter.getMonto() + presenter.getFee()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(aciActivity);
    }

    @Override
    public void showProgress() {
        aciActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        aciActivity.hideProgress();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        aciActivity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        aciActivity.showSuccess(msg);
    }

    @OnClick(R.id.b_aci_confirm_pagar)
    @Override
    public void clickPagar() {
        onWhiteList();
    }

    @Override
    public void onPagoSuccess(McResponse response) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        // Create and show the dialog.
        DialogFragment newFragment = AciResultFragment.get(response);
        newFragment.show(aciActivity.getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onWhiteList() {
        presenter.pagar();
    }

    @Override
    public void notOnWhiteList() {

    }
}
