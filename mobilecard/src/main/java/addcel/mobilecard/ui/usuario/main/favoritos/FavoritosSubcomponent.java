package addcel.mobilecard.ui.usuario.main.favoritos;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 02/10/17.
 */
@PerFragment
@Subcomponent(modules = FavoritosModule.class)
public interface FavoritosSubcomponent {
    void inject(FavoritosFragment fragment);
}
