package addcel.mobilecard.ui.usuario.wallet.cards;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.CardResponse;
import addcel.mobilecard.data.net.wallet.MovementEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.wallet.list.WalletListInteractor;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 09/10/17.
 */
class WalletCardsPresenter implements WalletCardsContract.Presenter {
    private final WalletListInteractor interactor;
    private final WalletCardsContract.View view;

    WalletCardsPresenter(WalletListInteractor interactor, WalletCardsContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @Override
    public Usuario getUsuario() {
        return interactor.getUsuario();
    }

    @Override
    public String getLast4Digits(CardEntity model) {
        return StringUtil.getUltimos4DigitosTDC(
                AddcelCrypto.decryptHard(Strings.nullToEmpty(model.getPan())));
    }

    @Override
    public void list() {
        view.showProgress();
        interactor.list(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<CardEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<CardEntity> result) {
                        view.hideProgress();
                        view.onCardsUpdated(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void deleteCard(CardEntity model) {
        view.showProgress();

        interactor.delete(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                model.getIdTarjeta(), new InteractorCallback<CardResponse>() {
                    @Override
                    public void onSuccess(@NotNull CardResponse result) {
                        view.hideProgress();
                        if (result.getIdError() == 0) {
                            view.onCardsUpdated(result.getTarjetas());
                            view.showSuccess(result.getMensajeError());
                        } else {
                            view.showError(result.getMensajeError());
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void favCard(CardEntity model) {
        view.showProgress();
        interactor.fav(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), model.getIdTarjeta(),
                new InteractorCallback<CardResponse>() {
                    @Override
                    public void onSuccess(@NotNull CardResponse result) {
                        view.hideProgress();
                        if (result.getIdError() == 0) {
                            view.onCardsUpdated(result.getTarjetas());
                            view.showSuccess(result.getMensajeError());
                        } else {
                            view.showError(result.getMensajeError());
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getLastMovements() {
        view.showProgress();

        interactor.listLastMovements(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<MovementEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<MovementEntity> result) {
                        view.hideProgress();
                        view.onMovementsReceived(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }
}
