package addcel.mobilecard.ui.usuario.legacy.recargas.tae.select;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.model.MobilecardContact;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.custom.view.CheckableButton;
import addcel.mobilecard.ui.custom.view.CheckableImageButton;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.utils.KListUtil;
import addcel.mobilecard.utils.ListUtil;
import addcel.mobilecard.utils.MapUtils;
import addcel.mobilecard.validation.ValidationUtils;
import addcel.mobilecard.validation.annotation.Celular;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass. Use the {@link RecargaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecargaFragment extends Fragment implements RecargaContract.View {

    @BindView(R.id.view_recarga_header)
    ImageView header;

    @BindView(R.id.label_recarga_eligenumero)
    TextView telefonoLabel;

    @BindView(R.id.spinner_recarga_eligenumero)
    Spinner telefonoSpinner;

    @BindView(R.id.view_recarga_currency)
    TextView currencyView;

    @Celular(messageResId = R.string.error_celular)
    @BindView(R.id.text_recarga_telefono)
    EditText
            telefonoText;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.text_recarga_telefono_conf)
    EditText telefonoConfText;

    @BindView(R.id.switch_recarga_otronumero)
    Switch otroNumeroSwitch;

    @BindView(R.id.recycler_montos)
    RecyclerView montosRecycler;

    @BindView(R.id.b_recarga_pagar)
    CheckableButton pagarButton;

    @BindView(R.id.b_recarga_fav)
    CheckableImageButton favButton;

    @BindView(R.id.til_recarga_favorito)
    TextInputLayout favTil;

    @BindView(R.id.view_recarga_favorito)
    View favView;

    @Inject
    PagoContainerActivity activity;
    @Inject
    CatalogoResponse.RecargaEntity servicio;
    @Inject
    Validator validator;
    @Inject
    List<CatalogoResponse.MontoEntity> montos;
    @Inject
    RecargaContract.Presenter presenter;
    @Inject
    MontoAdapter montoAdapter;

    @Inject
    @Named("colorLogoMap")
    Map<Integer, Integer> headerMap;

    @Inject
    @Named("recargaMap")
    Map<Integer, Integer> whiteMap;

    @Inject
    boolean fav;

    private Unbinder unbinder;

    public RecargaFragment() {
        // Required empty public constructor
    }

    public static RecargaFragment newInstance(CatalogoResponse.RecargaEntity servicio,
                                              List<CatalogoResponse.MontoEntity> montos) {
        return newInstance(servicio, montos, false);
    }

    public static RecargaFragment newInstance(CatalogoResponse.RecargaEntity servicio,
                                              List<CatalogoResponse.MontoEntity> montos, boolean fav) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", servicio);
        bundle.putParcelableArrayList("montos", Lists.newArrayList(montos));
        bundle.putBoolean("fav", fav);
        RecargaFragment fragment = new RecargaFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static RecargaFragment newInstance(CatalogoResponse.RecargaEntity servicio,
                                              List<CatalogoResponse.MontoEntity> montos, PaisResponse.PaisEntity pais) {
        return newInstance(servicio, montos, pais, false);
    }

    private static RecargaFragment newInstance(CatalogoResponse.RecargaEntity servicio,
                                               List<CatalogoResponse.MontoEntity> montos, PaisResponse.PaisEntity paisModel, boolean fav) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", servicio);
        bundle.putParcelableArrayList("montos", Lists.newArrayList(montos));
        bundle.putParcelable("pais", paisModel);
        bundle.putBoolean("fav", fav);
        RecargaFragment fragment = new RecargaFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().recargaSubcomponent(new RecargaModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_recarga, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(PagoContainerActivity.STATUS_NORMAL);
        try {
            presenter.getContacts(activity);
        } catch (Exception e) {
            Timber.e(e, "Error al obtener contactos del dispositivo");
            telefonoSpinner.setEnabled(false);
            telefonoSpinner.setVisibility(View.GONE);
            otroNumeroSwitch.setChecked(true);
        }
        header.setImageResource(
                MapUtils.getOrDefault(headerMap, servicio.getId(), R.drawable.logo_cfe));
        RecyclerView.LayoutManager manager = new GridLayoutManager(getContext(), 2);
        montosRecycler.setLayoutManager(manager);
        montosRecycler.setAdapter(montoAdapter);
        montosRecycler.setHasFixedSize(true);
        ItemClickSupport.addTo(montosRecycler).setOnItemClickListener((recyclerView, position, v) -> {
            montoAdapter.toggleSelection(position);
            if (!otroNumeroSwitch.isChecked()) {
                pagarButton.setChecked(true);
            } else {
                onOtroNumeroCaptured(telefonoText.getText());
            }
        });
        setInitialFav(fav);
        presenter.setCurrencyCaption(servicio);
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(montosRecycler);
        montosRecycler.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearCompositeDisposable();
        super.onDestroy();
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(activity);
    }

    @Override
    public void setContacts(List<MobilecardContact> contacts) {
        if (getView() != null && isVisible()) {
            if (KListUtil.Companion.isNullOrEmpty(contacts)) {
                telefonoSpinner.setEnabled(false);
                telefonoLabel.setVisibility(View.GONE);
                telefonoSpinner.setVisibility(View.GONE);
                otroNumeroSwitch.setChecked(true);
                otroNumeroSwitch.setEnabled(false);
            } else {
                ArrayAdapter<MobilecardContact> adapter =
                        new ArrayAdapter<>(Objects.requireNonNull(getContext()),
                                android.R.layout.simple_spinner_item, contacts);
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                // Create the list view and bind the peajeMontoAdapter
                telefonoSpinner.setAdapter(adapter);
            }
        }
    }

    @Override
    public boolean validatePhoneNumberConfirmation(EditText original, EditText confirmation) {
        return !confirmation.isShown() || TextUtils.equals(original.getText(), confirmation.getText());
    }

    @OnCheckedChanged(R.id.switch_recarga_otronumero)
    @Override
    public void toggleOtroNumeroViews(boolean checked) {
        otroNumeroSwitch.setChecked(checked);
        telefonoSpinner.setEnabled(!checked);
        telefonoText.setText("");
        telefonoText.setVisibility(checked ? View.VISIBLE : View.GONE);
        telefonoConfText.setText("");
        telefonoConfText.setVisibility(checked ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setCurrency(String currency) {
        currencyView.setText(currency);
    }

    @OnTextChanged(value = R.id.text_recarga_telefono, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void onOtroNumeroCaptured(CharSequence s) {
        if (otroNumeroSwitch.isChecked()) {
            if (montoAdapter.getSelectedItemCount() > 0
                    && TextUtils.getTrimmedLength(s) >= 6
                    && TextUtils.getTrimmedLength(s) <= 12) {
                pagarButton.setChecked(true);
            } else {
                pagarButton.setChecked(false);
            }
        }
    }

    @OnClick(R.id.b_recarga_pagar)
    @Override
    public void clickRecarga() {
        validator.validate();
    }

    @OnClick(R.id.b_recarga_fav)
    @Override
    public void clickFav() {
        setFav(!favButton.isChecked());
    }

    @OnClick(R.id.b_recarga_fav_add)
    @Override
    public void clickSaveFav() {
        if (!Strings.isNullOrEmpty(Objects.requireNonNull(favTil.getEditText()).getText().toString())) {
            favTil.setError(null);
            presenter.saveToFav(servicio, montos,
                    MapUtils.getOrDefault(whiteMap, servicio.getId(), R.drawable.recargas),
                    favTil.getEditText().getText().toString().trim());
        } else {
            favTil.setError(getText(R.string.error_default));
        }
    }

    @Override
    public void updateFavViewsOnSuccess() {
        favButton.setEnabled(false);
        favView.setVisibility(View.GONE);
    }

    @Override
    public void setFav(boolean fav) {
        favButton.setChecked(fav);
        new Handler().postDelayed(() -> favView.setVisibility(fav ? View.VISIBLE : View.GONE), 250);
    }

    @Override
    public void setInitialFav(boolean fav) {
        favButton.setChecked(fav);
        favButton.setEnabled(!fav);
        if (!fav) favButton.requestFocus();
    }

    @Override
    public void onTipoCambioReceived(double tipoCambio) {
        PaisResponse.PaisEntity destino =
                presenter.getPaisDestino() == null ? PaisResponse.PaisEntity.DEFAULT_VALUE
                        : presenter.getPaisDestino();

        Bundle args = new BundleBuilder().putParcelable("servicio", servicio)
                .putParcelable("monto", montoAdapter.getItem(montoAdapter.getSelectedItems().get(0)))
                .putParcelable("pais", destino)
                .putString("referencia",
                        presenter.getContacto((MobilecardContact) telefonoSpinner.getSelectedItem(),
                                telefonoText.getText().toString().trim()))
                .putDouble("tipoCambio", tipoCambio)
                .build();

        continuar(args);
    }

    private void continuar(final Bundle args) {
        activity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.activity_pago_container,
                        WalletSelectFragment.get(WalletSelectFragment.RECARGAS_MX, args))
                .hide(this)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();
    }

    @Override
    public void onValidationSucceeded() {
        ValidationUtils.clearViewErrors(telefonoText, telefonoConfText);
        if (validatePhoneNumberConfirmation(telefonoText, telefonoConfText)) {
            if (!pagarButton.isChecked()) {
                Toasty.error(activity, getString(R.string.error_monto_select), Toast.LENGTH_SHORT).show();
            } else {
                presenter.getTipoCambio(servicio);
            }
        } else {
            telefonoConfText.setError(getText(R.string.error_phone_conf));
            telefonoConfText.requestFocus();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationUtils.onValidationFailed(activity, errors);
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }
}
