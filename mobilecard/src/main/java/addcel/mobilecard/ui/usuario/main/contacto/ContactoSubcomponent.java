package addcel.mobilecard.ui.usuario.main.contacto;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 21/04/17.
 */
@PerFragment
@Subcomponent(modules = ContactoModule.class)
public interface ContactoSubcomponent {
    void inject(ContactoFragment subcomponent);
}
