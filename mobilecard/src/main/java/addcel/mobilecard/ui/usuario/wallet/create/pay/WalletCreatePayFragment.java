package addcel.mobilecard.ui.usuario.wallet.create.pay;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.CreditCard;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.ui.usuario.ingo.processing.IngoProcessingFragment;
import addcel.mobilecard.ui.usuario.wallet.select.add.WalletSelectAddFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import commons.validator.routines.CreditCardValidator;
import dagger.Lazy;
import es.dmoral.toasty.Toasty;
import io.card.payment.CardIOActivity;

/**
 * ADDCEL on 10/10/17.
 */
public class WalletCreatePayFragment extends Fragment implements WalletCreatePayContract.View {
    @BindColor(R.color.colorDeny)
    int RED;

    @BindColor(R.color.colorBlack)
    int BLACK;

    @BindColor(R.color.colorAccent)
    int ACCENT;

    @NotEmpty(messageResId = R.string.error_nombre)
    @BindView(R.id.til_wallet_card_info_cardholder)
    TextInputLayout holderNameTil;

    @CreditCard(messageResId = R.string.error_tarjeta, cardTypes = {
            CreditCard.Type.VISA, CreditCard.Type.MASTERCARD, CreditCard.Type.AMEX,
    })
    @BindView(R.id.til_wallet_card_info_card)
    TextInputLayout numberTil;

    @NotEmpty(messageResId = R.string.error_cvv)
    @BindView(R.id.til_wallet_card_info_cvv)
    TextInputLayout cvvTil;

    @NotEmpty(messageResId = R.string.error_vigencia)
    @BindView(R.id.til_wallet_card_info_exp)
    TextInputLayout expirationTil;

    @BindView(R.id.img_wallet_card_info_franquicia)
    ImageView franquiciaImg;

    @BindView(R.id.spinner_wallet_card_info_tipo)
    Spinner cardTipoSpinner;

    @NotEmpty(messageResId = R.string.error_domicilio)
    @BindView(R.id.til_wallet_card_info_address1)
    TextInputLayout address1Til;

    @BindView(R.id.til_wallet_card_info_address2)
    TextInputLayout address2Til;

    @NotEmpty(messageResId = R.string.error_cp)
    @BindView(R.id.til_wallet_card_info_cp)
    TextInputLayout zipTil;

    @Inject
    @Named("module")
    int module;

    @Inject
    Activity activity;
    @Inject
    Validator validator;
    @Inject
    Calendar expirationCalendar;
    @Inject
    Lazy<DatePickerDialog> expirationDialog;
    @Inject
    WalletCreatePayContract.Presenter presenter;

    private Unbinder unbinder;

    public WalletCreatePayFragment() {
    }

    public static WalletCreatePayFragment get(int module, Bundle arguments) {
        WalletCreatePayFragment fragment = new WalletCreatePayFragment();
        arguments.putInt("module", module);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .walletCreatePaySubcomponent(new WalletCreatePayModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_wallet_card_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_wallet_create, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan:
                launchCardScan();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AndroidUtils.setText(holderNameTil.getEditText(), presenter.getCardHolderName());
        AndroidUtils.getEditText(numberTil).setOnFocusChangeListener((view1, b) -> {
            if (b) numberTil.setError(null);
        });
        AndroidUtils.getEditText(expirationTil).setOnFocusChangeListener((view12, b) -> {
            if (b) {
                expirationTil.setError(null);
                showExpirationDialog();
            } else {
                dismissExpirationDialog();
            }
        });
        AndroidUtils.getEditText(cvvTil).setOnFocusChangeListener((view1, b) -> {
            if (b) cvvTil.setError(null);
        });
        AndroidUtils.getEditText(address1Til).setOnFocusChangeListener((view1, b) -> {
            if (b) address1Til.setError(null);
        });
        AndroidUtils.getEditText(zipTil).setOnFocusChangeListener((view1, b) -> {
            if (b) zipTil.setError(null);
        });
        cardTipoSpinner.setSelection(1);
        cardTipoSpinner.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            expirationDialog.get().setOnDateSetListener(null);
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                io.card.payment.CreditCard scanResult =
                        data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                setCardValuesFromScan(scanResult);
            }
        }
    }

    @Override
    public String getAlias() {
        return "";
    }

    @Override
    public boolean isPrimary() {
        return true;
    }

    @Override
    public String getCardHolderName() {
        return AndroidUtils.getText(holderNameTil.getEditText());
    }

    @Override
    public String getCard() {
        return AndroidUtils.getText(numberTil.getEditText());
    }

    @OnTextChanged(value = R.id.text_wallet_card_info_card, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void validateCardPan(CharSequence s) {
        int icon;
        String pan = Strings.nullToEmpty(s.toString());
        if (CreditCardValidator.VISA_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_visa;
            AndroidUtils.getEditText(numberTil).setTextColor(BLACK);
        } else if (CreditCardValidator.MASTERCARD_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_master;
            AndroidUtils.getEditText(numberTil).setTextColor(BLACK);
        } else if (CreditCardValidator.AMEX_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_american;
            AndroidUtils.getEditText(numberTil).setTextColor(BLACK);
        } else {
            icon = android.R.color.transparent;
            AndroidUtils.getEditText(numberTil).setTextColor(RED);
        }
        franquiciaImg.setImageResource(icon);
    }

    @Override
    public String getCvv() {
        return AndroidUtils.getText(cvvTil.getEditText());
    }

    @OnClick(R.id.text_wallet_card_info_exp)
    @Override
    public void clickVigencia() {
        expirationTil.setError(null);
        showExpirationDialog();
    }

    @Override
    public String getVigencia() {
        return DateFormat.format("MM/yy", expirationCalendar).toString();
    }

    @Override
    public TipoTarjetaEntity getTipoTarjeta() {
        return cardTipoSpinner.getSelectedItemPosition() == 0 ? TipoTarjetaEntity.CREDITO
                : TipoTarjetaEntity.DEBITO;
    }

    @Override
    public String getAddress1() {
        return AndroidUtils.getText(address1Til.getEditText());
    }

    @Override
    public String getAddress2() {
        return AndroidUtils.getText(address2Til.getEditText());
    }

    @Override
    public String getZipCode() {
        return AndroidUtils.getText(zipTil.getEditText());
    }

    @OnClick(R.id.b_wallet_card_info_save)
    @Override
    public void clickSave() {
        validator.validate();
    }

    @Override
    public void showProgress() {
        // activity.getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        // activity.getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showExpirationDialog() {
        if (isVisible() && !expirationDialog.get().isShowing()) expirationDialog.get().show();
    }

    @Override
    public void dismissExpirationDialog() {
        if (isVisible() && expirationDialog.get().isShowing()) expirationDialog.get().dismiss();
    }

    @Override
    public void showError(int resId) {
        showError(getString(resId));
    }

    @Override
    public void showError(String msg) {
        Toasty.error(activity, msg).show();
    }

    @Override
    public void showSuccess(String msg) {
        Toasty.success(activity, msg).show();
    }

    @Override
    public void updateCards(List<CardEntity> models) {
    }

    @Override
    public void onSuccess(CardEntity model) {
        Objects.requireNonNull(getArguments()).putParcelable("card", model);
        switch (module) {
            case WalletSelectAddFragment.INGO:
                Objects.requireNonNull(getFragmentManager())
                        .beginTransaction()
                        .add(R.id.frame_ingo, IngoProcessingFragment.get(getArguments()), "processing")
                        .commit();
                break;
            default:
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        ValidationUtils.clearViewErrors(holderNameTil, numberTil, cvvTil, expirationTil, address1Til,
                address2Til, zipTil);
        presenter.create();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        expirationCalendar.set(Calendar.YEAR, i);
        expirationCalendar.set(Calendar.MONTH, i1);
        AndroidUtils.setText(expirationTil.getEditText(),
                DateFormat.format("MM/yy", expirationCalendar));
    }

    private void launchCardScan() {
        Intent scanIntent = new Intent(activity, CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_INSTRUCTIONS, getString(R.string.txt_cardio_instruction));
        scanIntent.putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, ACCENT);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false);

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, REQUEST_CODE);
    }

    private void setCardValuesFromScan(io.card.payment.CreditCard card) {
        if (getView() != null) {
            if (!Strings.isNullOrEmpty(card.cardholderName)) {
                AndroidUtils.setText(holderNameTil.getEditText(), card.cardholderName);
            }
            AndroidUtils.setText(numberTil.getEditText(), card.cardNumber);
            validateCardPan(card.cardNumber);
            AndroidUtils.setText(cvvTil.getEditText(), Strings.nullToEmpty(card.cvv));
            AndroidUtils.setText(zipTil.getEditText(), Strings.nullToEmpty(card.postalCode));
            setExpiryDateFromScan(card.expiryYear, card.expiryMonth);
        }
    }

    private void setExpiryDateFromScan(int year, int month) {
        expirationCalendar.set(Calendar.YEAR, year);
        expirationCalendar.set(Calendar.MONTH, month - 1);
        AndroidUtils.setText(expirationTil.getEditText(),
                DateFormat.format("MM/yy", expirationCalendar));
    }
}
