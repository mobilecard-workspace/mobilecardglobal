package addcel.mobilecard.ui.usuario.legacy.recargas.list

import addcel.mobilecard.di.scope.PerFragment
import dagger.Subcomponent

/**
 * ADDCEL on 2019-06-15.
 */
@PerFragment
@Subcomponent(modules = [RecargaListModule::class])
interface RecargaListSubcomponent {
    fun inject(fragment: RecargaListFragment)
}