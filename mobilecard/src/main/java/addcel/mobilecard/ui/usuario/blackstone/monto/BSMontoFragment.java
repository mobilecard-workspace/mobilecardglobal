package addcel.mobilecard.ui.usuario.blackstone.monto;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.blackstone.model.BSProductEntity;
import addcel.mobilecard.data.net.usuarios.model.MobilecardContact;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.custom.view.CheckableButton;
import addcel.mobilecard.ui.phonecontacts.PhoneContactsActivity;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.StringUtil;
import addcel.mobilecard.validation.ValidationUtils;
import addcel.mobilecard.validation.annotation.Celular;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

/**
 * ADDCEL on 09/07/18.
 */
public final class BSMontoFragment extends Fragment implements BSMontoContract.View {
    @Inject
    BlackstoneActivity activity;
    @Inject
    BSMontoContract.Presenter presenter;
    @Inject
    Validator validator;

    @BindView(R.id.b_bs_monto_eligenumero)
    Button phoneButton;

    @BindView(R.id.switch_bs_monto_otronumero)
    Switch phoneSwitch;

    @Celular
    @BindView(R.id.text_bs_monto_telefono)
    EditText phoneText;

    @Celular
    @BindView(R.id.text_bs_monto_telefono_conf)
    EditText phoneConfText;

    @NotEmpty(messageResId = R.string.error_cantidad)
    @BindView(R.id.til_bs_monto_monto)
    TextInputLayout montoTil;

    @BindView(R.id.b_bs_monto_pagar)
    CheckableButton buttonPagar;

    private Unbinder unbinder;
    private MobilecardContact contact;
    private RxPermissions contactPermission;

    public static synchronized BSMontoFragment get(BSProductEntity product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("product", product);
        BSMontoFragment fragment = new BSMontoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().bsMontoSubcomponent(new BSMontoModule(this)).inject(this);
        contactPermission = new RxPermissions(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_bs_montos, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        activity.setAppToolbarTitle(presenter.getProduct().getName());
        setPhoneSwitchEnabled(Boolean.FALSE);
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public MobilecardContact getSelectedContact() {
        return contact;
    }

    @OnClick(R.id.b_bs_monto_eligenumero)
    @Override
    public void clickContact() {
        presenter.addToDisposables(contactPermission.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.READ_CONTACTS).subscribe(aBoolean -> {
            if (aBoolean) {
                startActivityForResult(PhoneContactsActivity.Companion.get(activity),
                        PhoneContactsActivity.REQUEST_CODE);
            }
        }));
    }

    @Override
    public String getCapturedPhone() {
        return Strings.nullToEmpty(Objects.requireNonNull(phoneText).getText().toString());
    }

    @Override
    public double getCapturedMonto() {
        String strMonto =
                Strings.nullToEmpty(Objects.requireNonNull(montoTil.getEditText()).getText().toString());
        if (Strings.isNullOrEmpty(strMonto)) return 0;
        return Double.parseDouble(strMonto);
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void setContactComboEnabled(boolean enable) {
        phoneButton.setEnabled(enable);
    }

    @Override
    public void setPhoneSwitchEnabled(boolean enabled) {
        phoneSwitch.setChecked(enabled);
        onPhoneSwitchEnabled(enabled);
    }

    @OnCheckedChanged(R.id.switch_bs_monto_otronumero)
    @Override
    public void onPhoneSwitchEnabled(boolean enabled) {
        setContactComboEnabled(!enabled);
        phoneText.setVisibility(enabled ? View.VISIBLE : View.GONE);
        phoneConfText.setVisibility(enabled ? View.VISIBLE : View.GONE);
        phoneText.setText("");
        phoneConfText.setText("");

        if (!enabled) {
            setContinuarEnabled(contact, getCapturedMonto());
        } else {
            buttonPagar.setChecked(Boolean.FALSE);
        }
    }

    @OnTextChanged(value = R.id.text_bs_monto_telefono, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void onPhoneCaptured(CharSequence chars) {
        CharSequence conf = Strings.nullToEmpty(phoneConfText.getText().toString());
        if (chars.length() > 0 && TextUtils.equals(chars, conf)) {
            contact = new MobilecardContact("", chars.toString());
        } else {
            contact = null;
            phoneButton.setText("");
        }
        setContinuarEnabled(contact, getCapturedMonto());
    }

    @OnTextChanged(value = R.id.text_bs_monto_telefono_conf, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void onConfirmCaptured(CharSequence chars) {
        CharSequence phone = Strings.nullToEmpty(phoneText.getText().toString());
        if (chars.length() > 0 && TextUtils.equals(chars, phone)) {
            contact = new MobilecardContact("", phone.toString());
        } else {
            contact = null;
            phoneButton.setText("");
        }
        setContinuarEnabled(contact, getCapturedMonto());
    }

    @OnTextChanged(value = R.id.text_bs_monto_monto, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void onMontoCaptured(CharSequence chars) {
        if (!phoneSwitch.isChecked()) {
            if (!Strings.isNullOrEmpty(chars.toString())) {
                buttonPagar.setChecked(Boolean.TRUE);
            } else {
                buttonPagar.setChecked(Boolean.FALSE);
            }
        } else {
            CharSequence tel = phoneText.getText();
            CharSequence conf = phoneConfText.getText();
            if (!Strings.isNullOrEmpty(tel.toString())
                    && !Strings.isNullOrEmpty(conf.toString())
                    && TextUtils.equals(tel, conf)
                    && !Strings.isNullOrEmpty(chars.toString())) {
                buttonPagar.setChecked(Boolean.TRUE);
            } else {
                buttonPagar.setChecked(Boolean.FALSE);
            }
        }
    }

    @Override
    public void setContinuarEnabled(@Nullable MobilecardContact contact, double monto) {
        buttonPagar.setChecked(contact != null && monto > 0);
    }

    @OnClick(R.id.b_bs_monto_pagar)
    @Override
    public void clickContinuar() {
        clearErrors();
        if (buttonPagar.isChecked()) {
            validator.validate();
        }
    }

    @Override
    public void onValidationSucceeded() {
        clearErrors();
        if (phoneSwitch.isChecked()) {
            if (evalComparacion(Strings.nullToEmpty(phoneText.getText().toString()),
                    Strings.nullToEmpty(phoneConfText.getText().toString())) && evalMonto(
                    Objects.requireNonNull(montoTil.getEditText()).getText())) {
                launchWalletFragment();
            }
        } else {
            if (contact != null) {
                if (evalMonto(Objects.requireNonNull(montoTil.getEditText()).getText())) {
                    launchWalletFragment();
                } else {
                    showError(getString(R.string.error_cantidad));
                }
            }
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    private void clearErrors() {
        if (isVisible()) ValidationUtils.clearViewErrors(phoneText, phoneConfText, montoTil);
    }

    private boolean evalComparacion(@NonNull CharSequence tel, @NonNull CharSequence telComp) {
        boolean result = TextUtils.equals(tel, telComp);
        if (!result) showError(getString(R.string.error_phone_conf));
        return result;
    }

    private boolean evalMonto(@NonNull CharSequence strMonto) {
        if (!StringUtil.isDecimalAmount(Strings.nullToEmpty(strMonto.toString()))) {
            showError(getString(R.string.error_cantidad));
            return false;
        }

        double monto = Double.parseDouble(String.valueOf(strMonto));
        boolean result =
                monto >= presenter.getProduct().getMinDenominations() && monto <= presenter.getProduct()
                        .getMaxDenominations();
        if (!result) showError(getString(R.string.error_cantidad));
        return result;
    }

    private void launchWalletFragment() {
        activity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.frame_blackstone,
                        WalletSelectFragment.get(WalletSelectFragment.BLACKSTONE, presenter.buildPagoBundle()))
                .hide(this)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PhoneContactsActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    MobilecardContact tempContact =
                            data.getParcelableExtra(PhoneContactsActivity.DATA_CONTACT);
                    if (tempContact != null) {
                        if (phoneSwitch.isChecked()) {
                            phoneSwitch.setChecked(false);
                            onPhoneSwitchEnabled(false);
                        }
                        contact = tempContact;
                        phoneButton.setText(contact.toString());
                        phoneButton.setMaxLines(1);
                        phoneButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0F);
                    }
                    setContinuarEnabled(contact, getCapturedMonto());
                }
            }
        }
    }
}
