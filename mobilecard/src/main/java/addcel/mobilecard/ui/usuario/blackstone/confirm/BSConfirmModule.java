package addcel.mobilecard.ui.usuario.blackstone.confirm;

import android.os.Bundle;

import java.text.NumberFormat;
import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.blackstone.BlackstoneService;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 10/07/18.
 */
@Module
public final class BSConfirmModule {
    private final BSConfirmFragment fragment;

    BSConfirmModule(BSConfirmFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    BlackstoneActivity provideActivity() {
        return (BlackstoneActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Bundle provideBundle() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    BlackstoneService provideService(Retrofit retrofit) {
        return retrofit.create(BlackstoneService.class);
    }

    @PerFragment
    @Provides
    TokenizerAPI provideTokenizer(Retrofit retrofit) {
        return TokenizerAPI.Companion.provideTokenizerAPI(retrofit);
    }

    @PerFragment
    @Provides
    BSConfirmContract.Presenter providePresenter(BlackstoneService service, TokenizerAPI token,
                                                 SessionOperations session, NumberFormat format, Bundle bundle) {
        return new BSConfirmPresenter(service, token, session, format, bundle.getParcelable("card"),
                bundle.getParcelable("product"), bundle.getString("numero"), bundle.getDouble("monto"),
                fragment);
    }
}
