package addcel.mobilecard.ui.usuario.blackstone.monto;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Objects;

import addcel.mobilecard.data.net.blackstone.BlackstoneService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 09/07/18.
 */
@Module
public final class BSMontoModule {
    private final BSMontoFragment fragment;

    BSMontoModule(BSMontoFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    BlackstoneActivity provideActivity() {
        return (BlackstoneActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.setValidationListener(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        return validator;
    }

    @PerFragment
    @Provides
    @Nullable
    Bundle provideArguments() {
        return fragment.getArguments();
    }

    @PerFragment
    @Provides
    BlackstoneService provideService(Retrofit retrofit) {
        return retrofit.create(BlackstoneService.class);
    }

    @PerFragment
    @Provides
    BSMontoContract.Presenter providePresenter(@Nullable Bundle bundle) {
        return new BSMontoPresenter(Preconditions.checkNotNull(bundle).getParcelable("product"),
                fragment);
    }
}
