package addcel.mobilecard.ui.usuario.aci.list;

import android.annotation.SuppressLint;

import androidx.annotation.Nullable;

import com.google.common.base.Strings;
import com.squareup.otto.Subscribe;

import addcel.mobilecard.domain.aci.list.AciListInteractor;
import addcel.mobilecard.event.impl.AciServiceInsertEvent;
import addcel.mobilecard.event.impl.AciServiceUpdateEvent;

/**
 * ADDCEL on 23/03/17.
 */
public class AciListPresenter implements AciListContract.Presenter {

    private final AciListInteractor interactor;
    private final AciListContract.View view;

    AciListPresenter(AciListInteractor interactor, AciListContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void clearCompositeDisposable() {
        if (interactor.getCompositeDisposable() != null)
            interactor.getCompositeDisposable().clear();
    }

    @Override
    public void register() {
        interactor.getBus().register(this);
    }

    @Override
    public void unregister() {
        interactor.getBus().unregister(this);
    }

    @SuppressLint("CheckResult")
    @Override
    public void downloadItems() {
        view.showProgress();
        interactor.downloadItems();
    }

    @Subscribe
    @Override
    public void onDownload(AciServiceInsertEvent event) {
        view.hideProgress();
        switch (event.getPos()) {
            case AciServiceInsertEvent.ITEM_LOAD_ERROR:
                view.showError("An error occurred while processing this request");
                break;
            case AciServiceInsertEvent.ITEM_LOAD_FINISHED:
                view.onLoadFinished();
                break;
        }
    }

    @Override
    public void filterItems(@Nullable String pattern) {
        interactor.filterItems(Strings.nullToEmpty(pattern).trim());
    }

    @Subscribe
    @Override
    public void onFilter(AciServiceUpdateEvent event) {
        view.onItemsUpdated(event.getModels());
    }
}
