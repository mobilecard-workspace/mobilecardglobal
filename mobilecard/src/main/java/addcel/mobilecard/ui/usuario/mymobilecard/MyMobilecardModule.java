package addcel.mobilecard.ui.usuario.mymobilecard;

import androidx.annotation.Nullable;

import com.google.common.base.Preconditions;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerActivity;
import dagger.Module;
import dagger.Provides;

@Module
public final class MyMobilecardModule {
    private final MyMobilecardActivity activity;

    MyMobilecardModule(MyMobilecardActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    MyMobileCardViewModel provideModel() {
        return activity.getIntent().getParcelableExtra("model");
    }


    @PerActivity
    @Provides
    Usuario provideUsuario(SessionOperations session) {
        return session.getUsuario();
    }
}
