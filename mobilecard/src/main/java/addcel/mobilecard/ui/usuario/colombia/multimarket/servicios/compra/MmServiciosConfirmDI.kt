package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.compra

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-12-20.
 */
@Module
class MmServiciosConfirmModule(val fragment: MmServiciosConfirmFragment) {

    @PerFragment
    @Provides
    fun provideActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideViewModel(): MmServiciosConfirmViewModel {
        return MmServiciosConfirmViewModel(
                fragment.arguments?.getParcelable("categoria")!!,
                fragment.arguments?.getParcelable("servicio")!!,
                fragment.arguments?.getParcelable("saldo")!!,
                fragment.arguments?.getParcelable("card")!!
        )
    }

    @PerFragment
    @Provides
    fun provideMultimarket(r: Retrofit): MultimarketAPI {
        return MultimarketAPI.get(r)
    }

    @PerFragment
    @Provides
    fun provideUsuario(session: SessionOperations): Usuario {
        return session.usuario
    }

    @PerFragment
    @Provides
    fun providePresenter(multimarket: MultimarketAPI, usuario: Usuario): MmServiciosConfirmPresenter {
        return MmServiciosConfirmPresenterImpl(multimarket, usuario, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [MmServiciosConfirmModule::class])
interface MmServiciosConfirmSubcomponent {
    fun inject(fragment: MmServiciosConfirmFragment)
}