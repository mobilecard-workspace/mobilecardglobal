package addcel.mobilecard.ui.usuario.scanpay.result;

import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;

/**
 * ADDCEL on 11/09/18.
 */
interface ScanResultContract {
    interface View {
        void setUIData();

        void clickQR();

        void clickOk();
    }

    interface Presenter {
        SPReceiptEntity getResult();
    }
}
