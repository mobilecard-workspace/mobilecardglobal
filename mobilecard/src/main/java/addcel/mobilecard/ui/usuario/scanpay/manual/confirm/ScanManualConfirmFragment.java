package addcel.mobilecard.ui.usuario.scanpay.manual.confirm;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.otto.Bus;

import javax.inject.Inject;

import addcel.mobilecard.McConstants;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import addcel.mobilecard.ui.usuario.scanpay.result.ScanResultFragment;
import addcel.mobilecard.ui.usuario.scanpay.secure.ScanSecureFragment;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.FragmentUtil;

/**
 * ADDCEL on 24/08/18.
 */
public final class ScanManualConfirmFragment extends Fragment
        implements ScanManualConfirmContract.View {

    @Inject
    ScanPayActivity activity;
    @Inject
    Bus bus;
    @Inject
    ScanManualConfirmContract.Presenter presenter;

    private TextView uiData;
    private Spinner msiSpinner;

    public ScanManualConfirmFragment() {
    }

    public static synchronized ScanManualConfirmFragment get(double monto, double propina,
                                                             String concepto, LcpfEstablecimiento establecimiento, CardEntity formaPgo) {
        ScanManualConfirmFragment fragment = new ScanManualConfirmFragment();
        fragment.setArguments(new BundleBuilder().putDouble("monto", monto)
                .putDouble("propina", propina)
                .putString("concepto", concepto)
                .putParcelable("establecimiento", establecimiento)
                .putParcelable("formaPago", formaPgo)
                .build());
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .scanManualConfirmSubcomponent(new ScanManualConfirmModule(this))
                .inject(this);
        bus.register(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_scanpay_manual_confirm, container, false);
        uiData = view.findViewById(R.id.view_scanpay_manual_confirm_data);
        msiSpinner = view.findViewById(R.id.spinner_scanpay_card_msi);
        view.findViewById(R.id.b_scanpay_manual_confirm_continuar)
                .setOnClickListener(v -> presenter.getToken(""));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setFinished(Boolean.FALSE);
        setUiData();
        setMsiEnabled();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void setUiData() {
        String invoice;
        if (presenter.getEstablecimiento().getComisionPorcentaje() + presenter.getEstablecimiento()
                .getComisionFija() == 0) {
            invoice = getString(R.string.txt_scanpay_manual_confirm_invoice_no_fee,
                    presenter.getFormattedMonto(), presenter.getFormattedPropina(), presenter.getConcepto(),
                    presenter.getEstablecimiento().getAlias());
        } else {
            invoice =
                    getString(R.string.txt_scanpay_manual_confirm_invoice, presenter.getFormattedMonto(),
                            presenter.getFormattedPropina(), presenter.getFormattedComision(),
                            presenter.getConcepto(), presenter.getEstablecimiento().getAlias());
        }
        uiData.setText(invoice);
    }

    @Override
    public String getMsi() {
        return (String) msiSpinner.getSelectedItem();
    }

    @Override
    public void setMsiEnabled() {
        if (presenter.isMsiDisabled()) {
            msiSpinner.setSelection(0);
            msiSpinner.setEnabled(false);
        } else {
            msiSpinner.setEnabled(true);
        }
    }

    @Override
    public void launchSecure(SPPagoEntity request, TokenEntity token) {
        FragmentUtil.addFragmentHidingCurrent(activity.getSupportFragmentManager(), R.id.frame_scanpay,
                ScanManualConfirmFragment.this, ScanSecureFragment.get(presenter.buildRequest()));
    }

    @Override
    public void launchOpen(SPReceiptEntity request, TokenEntity tokenEntity) {
        FragmentUtil.addFragmentHidingCurrent(activity.getSupportFragmentManager(), R.id.frame_scanpay,
                ScanManualConfirmFragment.this, ScanResultFragment.Companion.get(request, McConstants.PAIS_ID_MX, false));
    }
}
