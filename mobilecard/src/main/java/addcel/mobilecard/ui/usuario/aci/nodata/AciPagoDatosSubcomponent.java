package addcel.mobilecard.ui.usuario.aci.nodata;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 28/03/17.
 */
@PerFragment
@Subcomponent(modules = AciPagoDatosModule.class)
public interface AciPagoDatosSubcomponent {
    void inject(AciPagoDatosFragment fragment);
}
