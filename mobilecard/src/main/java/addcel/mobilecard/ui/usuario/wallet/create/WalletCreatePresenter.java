package addcel.mobilecard.ui.usuario.wallet.create;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.McRegexPatterns;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardRequest;
import addcel.mobilecard.data.net.wallet.CardResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.wallet.add.WalletCreateInteractor;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormModel;
import addcel.mobilecard.utils.StringUtil;
import io.card.payment.CardIOActivity;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 10/10/17.
 */
class WalletCreatePresenter implements WalletCreateContract.Presenter {
    private final WalletCreateInteractor interactor;
    private final WalletCreateContract.View view;

    WalletCreatePresenter(WalletCreateInteractor interactor, WalletCreateContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public TebcaFormModel buildModel() {
        return new TebcaFormModel(interactor.getUsuario().getEMail(),
                interactor.getUsuario().getIdeUsuario(),
                interactor.getUsuario().getUsrTelefono(),
                interactor.getUsuario().getUsrNombre(),
                interactor.getUsuario().getUsrApellido(),
                interactor.getUsuario().getUsrMaterno(), view.getCard(), view.getVigencia(),
                view.getCvv(), view.getCardHolderName(), view.getAddress1(), view.getTipoTarjeta());
    }

    @Override
    public boolean isTebca(String pan) {
        return getIdPais() == 4 && pan.matches(McRegexPatterns.TEBCA);
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @Override
    public String getCardHolderName() {
        return interactor.getUsuarioName();
    }

    @Override
    public Intent buildCardIOIntent(Intent intent, int accentColor, String instruction) {

        // customize these values to suit your needs.
        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE,
                getIdPais() == 1 || getIdPais() == 3); // default: false
        intent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        intent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false);
        intent.putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, accentColor);
        intent.putExtra(CardIOActivity.EXTRA_SCAN_INSTRUCTIONS, instruction);

        return intent;
    }

    @SuppressLint("CheckResult")
    @Override
    public void create() {
        view.showProgress();
        CardRequest request =
                new CardRequest(0, interactor.getIdUsuario(),
                        AddcelCrypto.encryptHard(view.getCard()),
                        AddcelCrypto.encryptHard(view.getVigencia()),
                        AddcelCrypto.encryptHard(view.getCvv()),
                        view.getCardHolderName(), true,
                        view.getAddress1().concat(" ").concat(view.getAddress2()), view.getZipCode(), false,
                        view.getTipoTarjeta(), StringUtil.getCurrentLanguage(), view.getDirUsa(), view.getCityUsa(), view.getSelectedState(), view.getZipUsa());

        interactor.create(BuildConfig.ADDCEL_APP_ID, request, new InteractorCallback<CardResponse>() {
            @Override
            public void onSuccess(@NotNull CardResponse result) {
                view.hideProgress();
                if (result.getIdError() == 0) {
                    view.showSuccess(result.getMensajeError());
                    view.updateCards(result.getTarjetas());
                } else {
                    view.showError(result.getMensajeError());
                }
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError(error);
            }
        });
    }

    @Override
    public Bitmap parseImage(byte[] data) {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    @Override
    public int getIdPais() {
        return interactor.getUsuario().getIdPais();
    }

    @Override
    public void getEstados() {
        interactor.getEstados(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), new InteractorCallback<EstadoResponse>() {
            @Override
            public void onSuccess(@NotNull EstadoResponse result) {
                view.hideProgress();
                view.updateStates(result.getEstados());
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError(error);
            }
        });
    }
}
