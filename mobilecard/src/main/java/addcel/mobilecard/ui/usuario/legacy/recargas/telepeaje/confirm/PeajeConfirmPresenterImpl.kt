package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.telepeaje.TagsApi
import addcel.mobilecard.data.net.telepeaje.model.PagoModel
import addcel.mobilecard.data.net.telepeaje.model.Tag
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.utils.ErrorUtil.Companion.getFormattedHttpErrorMsg
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import android.os.Build
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.crypto.AddcelCrypto
import okio.internal.commonAsUtf8ToByteArray
import java.text.NumberFormat

/**
 * ADDCEL on 20/10/17.
 */
internal class PeajeConfirmPresenterImpl(private val tagsApi: TagsApi, private val token: TokenizerAPI,
                                         private val session: SessionOperations, private val saldoActual: Double,
                                         private val montoModel: CatalogoResponse.MontoEntity,
                                         private val cardEntity: CardEntity, private val tag: String,
                                         private val verificador: String, private val numberFormat: NumberFormat,
                                         private val view: PeajeConfirmView) : PeajeConfirmPresenter {

    private val compositeDisposable = CompositeDisposable()

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun getSaldoTag() {
        view.showProgress()
        val tag = Tag(getVerificador().toInt(), "", getTag(), montoModel.emisor.toInt())
        val tDisp = tagsApi.checkBalance(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                session.usuario.ideUsuario, tag)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError.toInt() == 0) {
                        view.setSaldoActualTag(numberFormat.format(it.balance))
                    } else {
                        view.onErrorSaldo(it.mensajeError)
                    }
                }) {
                    view.hideProgress()
                    view.onErrorSaldo(getFormattedHttpErrorMsg(it))
                }
        addToDisposables(tDisp)
    }

    override fun getJumioRef(): String {
        return session.usuario.scanReference
    }

    override fun getTag(): String {
        return tag
    }

    override fun getVerificador(): String {
        return verificador
    }

    override fun getFormattedTag(): String {
        return String.format("%s %s", tag, verificador)
    }

    override fun getSaldoActual(): String {
        return String.format("%s %s", numberFormat.format(saldoActual), PESOS_SYMBOL)
    }

    override fun getBalance(): String {
        return String.format("%s %s", numberFormat.format(montoModel.monto), PESOS_SYMBOL)
    }

    override fun getCard(): CardEntity {
        return cardEntity
    }

    override fun getFee(): String {
        return String.format("%s %s", numberFormat.format(montoModel.comision), PESOS_SYMBOL)
    }

    override fun getTotal(): String {
        val total = montoModel.monto + montoModel.comision
        return String.format("%s %s", numberFormat.format(total), PESOS_SYMBOL)
    }

    override fun getStartUrl(): String {
        val base = BuildConfig.BASE_URL + TagsApi.PAGO
        return base.replace("{idApp}", BuildConfig.ADDCEL_APP_ID.toString())
                .replace("{idioma}", StringUtil.getCurrentLanguage())
    }

    override fun getSuccessUrl(): String {
        return "payworks/respuesta"
    }

    override fun getErrorUrl(): String {
        return ""
    }

    override fun getFormUrl(): String {
        return "8039159_imdm.jsp"
    }

    override fun getIdPais(): Int {
        return session.usuario.idPais
    }

    override fun checkWhiteList() {
        view.showProgress()
        val wDisposable = token.isOnWhiteList(BuildConfig.ADDCEL_APP_ID, session.usuario.idPais,
                StringUtil.getCurrentLanguage(), session.usuario.ideUsuario)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.code == 0) view.onWhiteList() else view.notOnWhiteList()
                }) {
                    view.hideProgress()
                    view.showError(getFormattedHttpErrorMsg(it))
                }
        addToDisposables(wDisposable)
    }

    override fun buildPagoRequest(): ByteArray {
        val model = PagoModel.Builder().withLon(session.minimalLocationData.lon)
                .withLat(session.minimalLocationData.lat)
                .withIdTarjeta(cardEntity.idTarjeta)
                .withImei(view.getImei())
                .withKey(view.getImei())
                .withLogin(session.usuario.usrLogin)
                .withModelo(Build.MODEL)
                .withPin(verificador)
                .withProducto(montoModel.operacion)
                .withSoftware(Build.VERSION.SDK_INT.toString())
                .withTipo(Build.MANUFACTURER)
                .withTarjeta(tag)
                .withIdUsuario(session.usuario.ideUsuario)
                .withDebug(BuildConfig.DEBUG)
                .build()
        val request = AddcelCrypto.encryptSensitive(JsonUtil.toJson(model))
        return String.format("json=%s", request).commonAsUtf8ToByteArray()
    }

    companion object {
        private const val PESOS_SYMBOL = "MXN"
    }

}