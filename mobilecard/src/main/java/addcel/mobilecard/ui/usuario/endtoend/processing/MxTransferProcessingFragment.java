package addcel.mobilecard.ui.usuario.endtoend.processing;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.otto.Bus;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.PaymentEntity;
import addcel.mobilecard.data.net.hth.ReceiptEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureActivity;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureModel;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketUseCase;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import addcel.mobilecard.ui.usuario.endtoend.result.H2HResultFragment;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.utils.JsonUtil;
import timber.log.Timber;

/**
 * ADDCEL on 22/11/17.
 */
public class MxTransferProcessingFragment extends Fragment
        implements MxTransferProcessingContract.View {

    @Inject
    MxTransferProcessingContract.Presenter presenter;
    @Inject
    MxTransferActivity activity;
    @Inject
    Bus bus;

    public MxTransferProcessingFragment() {
    }

    public static MxTransferProcessingFragment get(Bundle bundle) {
        MxTransferProcessingFragment fragment = new MxTransferProcessingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .mxTransferProcessingSubcomponent(new MxTransferProcessingModule(this))
                .inject(this);

        bus.register(this);

        activity.showProgress();
        activity.setStreenStatus(ContainerScreenView.STATUS_BLOCKED);

        presenter.checkWhiteList();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void onTokenReceived(TokenEntity token) {
        if (token.getSecure()) {
            Timber.d("MxTransferProcessingFragment - %s", token.getSecure());

            PaymentEntity request = presenter.buildRequestModel();

            BillPocketSecureModel model =
                    new BillPocketSecureModel(presenter.fetchIdUsuario(), presenter.fetchCard(), token,
                            presenter.buildRequestString(request), BillPocketUseCase.USUARIO);

            try {
                startActivityForResult(BillPocketSecureActivity.Companion.get(activity, model), BillPocketSecureActivity.REQUEST_CODE);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        } else {
            presenter.enqueuePayment(token, presenter.buildRequestModel());
        }
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(activity);
    }

    @Override
    public String getSoftware() {
        return String.valueOf(Build.VERSION.SDK_INT);
    }

    @Override
    public String getModel() {
        return Build.MODEL;
    }

    @Override
    public void lockUI() {
        activity.setStreenStatus(ContainerScreenView.STATUS_BLOCKED);
    }

    @Override
    public void unLockUI() {
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
    }

    @Override
    public void onWhiteList() {
        presenter.getToken("");
    }

    @Override
    public void notOnWhiteList() {
        showError("No estás autorizado para realizar esta operación. Contacta a soporte.");
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
        activity.getFragmentManagerLazy().beginTransaction().remove(this).commit();
    }

    @Override
    public void showSuccess(@NonNull String msg) {
    }

    @Override
    public void onSuccess(ReceiptEntity receipt) {
        activity.getFragmentManagerLazy()
                .beginTransaction()
                .addToBackStack(null)
                .add(R.id.frame_transfermx, H2HResultFragment.Companion.get(receipt))
                .commit();
    }


    @Override
    public void onBillPocketSecureResult(int resultCode, @org.jetbrains.annotations.Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String resultData = data.getStringExtra(BillPocketSecureActivity.RESULT_DATA);
                if (JsonUtil.isJson(resultData)) {
                    ReceiptEntity response = JsonUtil.fromJson(resultData, ReceiptEntity.class);
                    onSuccess(response);
                }
            }
        }
    }
}
