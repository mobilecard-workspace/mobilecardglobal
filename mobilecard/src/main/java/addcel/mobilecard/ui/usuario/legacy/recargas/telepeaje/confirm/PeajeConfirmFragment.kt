package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.confirm

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import addcel.mobilecard.ui.usuario.secure.WebActivity
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.DeviceUtil.Companion.getDeviceId
import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import butterknife.OnClick
import com.google.common.base.Preconditions
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.view_peaje_confirm.*
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 20/10/17.
 */
class PeajeConfirmFragment : Fragment(), PeajeConfirmView {

    private val permDisposable = CompositeDisposable()

    @Inject
    lateinit var cActivity: PagoContainerActivity
    @Inject
    lateinit var presenter: PeajeConfirmPresenter
    private lateinit var permissions: RxPermissions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get()
                .netComponent
                .peajeConfirmSubcomponent(PeajeConfirmModule(this))
                .inject(this)

        permissions = RxPermissions(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_peaje_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cActivity.setStreenStatus(PagoContainerActivity.STATUS_NORMAL)
        cActivity.setAppToolbarTitle(R.string.nav_topups)
        b_peaje_confirm_pagar.setOnClickListener { clickPagar() }
        updateUiOnIdPais()
        permDisposable.add(
                permissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_PHONE_STATE)
                        .subscribe()
        )
    }

    override fun onDestroy() {
        permDisposable.clear()
        super.onDestroy()
    }

    override fun getImei(): String {
        return getDeviceId(cActivity)
    }

    override fun updateUiOnIdPais() {
        label_peaje_confirm_referencia.setText(R.string.txt_peaje_tag_numero)
        view_peaje_confirm_referencia.text = presenter.getFormattedTag()
        AndroidUtils.setText(til_peaje_confirm_saldo.editText, presenter.getSaldoActual())
        AndroidUtils.setText(til_peaje_confirm_monto_recarga.editText, presenter.getBalance())
        AndroidUtils.setText(til_peaje_confirm_fee.editText, presenter.getFee())
        AndroidUtils.setText(til_peaje_confirm_total.editText, presenter.getTotal())
    }

    override fun setSaldoActualTag(saldo: String) {
        AndroidUtils.setText(til_peaje_confirm_saldo.editText, saldo)
    }

    override fun onErrorSaldo(msg: String) {
        showError(msg)
        cActivity.onBackPressed()
    }

    override fun clickPagar() {
        presenter.checkWhiteList()
    }

    override fun showProgress() {
        cActivity.showProgress()
    }

    override fun hideProgress() {
        cActivity.hideProgress()
    }

    override fun onWhiteList() {
        if (isVisible) {
            val intent = WebActivity.get(activity, getString(R.string.txt_peaje_monto_msg),
                    presenter.getStartUrl(), presenter.getFormUrl(), presenter.getSuccessUrl(),
                    presenter.getErrorUrl(), presenter.getCard(), presenter.buildPagoRequest(), "")

            startActivity(intent)
        }
    }

    override fun notOnWhiteList() {
        showError("No estás autorizado para realizar esta operación. Contacta a soporte.")
    }

    override fun showError(resId: Int) {
        cActivity.showError(getString(resId))
    }

    override fun showError(msg: String) {
        cActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        cActivity.showSuccess(msg)
    }

    companion object {

        fun get(args: Bundle?): PeajeConfirmFragment {
            val fragment = PeajeConfirmFragment()
            fragment.arguments = args
            return fragment
        }
    }
}