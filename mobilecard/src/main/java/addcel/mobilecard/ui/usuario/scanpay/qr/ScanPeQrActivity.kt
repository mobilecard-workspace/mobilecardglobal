package addcel.mobilecard.ui.usuario.scanpay.qr

import addcel.mobilecard.R
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_negocio_registro_pe_qr.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import timber.log.Timber
import java.util.regex.Pattern


class ScanPeQrActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    override fun handleResult(p0: Result?) {
        if (p0 != null) {
            val code = p0.text
            if (code.isNotEmpty() && Pattern.matches("-?[0-9a-fA-F]+", code)) {
                Timber.d("Scan result: %s", code)
                val intent = Intent().putExtra("qrBase64", code)
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                Toast.makeText(
                        this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                        Toast.LENGTH_SHORT
                ).show()
                scanner.resumeCameraPreview(this)
            }
        } else {
            Toast.makeText(
                    this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                    Toast.LENGTH_SHORT
            ).show()
            scanner.resumeCameraPreview(this)
        }
    }

    companion object {
        const val REQUEST_CODE = 406

        fun get(context: Context): Intent {
            return Intent(context, ScanPeQrActivity::class.java)
        }
    }

    private lateinit var scanner: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_registro_pe_qr)
        scanner = scanner_pe_negocio_registro
    }

    override fun onResume() {
        super.onResume()
        scanner.setResultHandler(this)
        scanner.startCamera()
    }

    public override fun onPause() {
        super.onPause()
        scanner.stopCamera()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, Intent())
        finish()
    }
}
