package addcel.mobilecard.ui.usuario.registro.create;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 28/09/17.
 */
@PerFragment
@Subcomponent(modules = RegistroCreateModule.class)
public interface RegistroCreateSubcomponent {
    void inject(RegistroCreateFragment fragment);
}
