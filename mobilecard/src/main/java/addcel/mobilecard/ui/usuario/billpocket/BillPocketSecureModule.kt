package addcel.mobilecard.ui.usuario.billpocket

import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.domain.secure.WebInteractor
import addcel.mobilecard.domain.secure.WebInteractorImpl
import android.webkit.WebViewClient
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import okhttp3.OkHttpClient

/**
 * ADDCEL on 2019-09-09.
 */
@Module
class BillPocketSecureModule(val activity: BillPocketSecureActivity) {

    @PerActivity
    @Provides
    fun provideModel(): BillPocketSecureModel {
        return activity.intent.getParcelableExtra("model")!!
    }

    @PerActivity
    @Provides
    fun provideInteractor(
            client: OkHttpClient,
            model: BillPocketSecureModel
    ): WebInteractor {
        return WebInteractorImpl(client, model.card)
    }

    @PerActivity
    @Provides
    fun providePresenter(
            interactor: WebInteractor
    ): BillPocketSecurePresenter {
        return BillPocketSecurePresenterImpl(interactor, activity)
    }

    @Provides
    @PerActivity
    fun provideWebClient(presenter: BillPocketSecurePresenter): WebViewClient {
        return BillPocketSecureClient(presenter, activity)
    }
}

@PerActivity
@Subcomponent(modules = [BillPocketSecureModule::class])
interface BillPocketSecureSubcomponent {
    fun inject(activity: BillPocketSecureActivity)
}