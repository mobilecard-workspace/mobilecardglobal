package addcel.mobilecard.ui.usuario.wallet.create;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.McRegexPatterns;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormActivity;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import commons.validator.routines.CreditCardValidator;
import dagger.Lazy;
import es.dmoral.toasty.Toasty;
import io.card.payment.CardIOActivity;

/**
 * ADDCEL on 10/10/17.
 */
public class WalletCreateFragment extends Fragment implements WalletCreateContract.View {

    @BindColor(R.color.colorDeny)
    int RED;

    @BindColor(R.color.colorBlack)
    int BLACK;

    @BindColor(R.color.colorAccent)
    int ACCENT;

    @NotEmpty(messageResId = R.string.error_nombre)
    @BindView(R.id.til_wallet_card_info_cardholder)
    TextInputLayout holderNameTil;

    @BindView(R.id.til_wallet_card_info_card)
    TextInputLayout numberTil;

    @NotEmpty(messageResId = R.string.error_cvv)
    @BindView(R.id.til_wallet_card_info_cvv)
    TextInputLayout cvvTil;

    @NotEmpty(messageResId = R.string.error_vigencia)
    @BindView(R.id.til_wallet_card_info_exp)
    TextInputLayout expirationTil;

    @BindView(R.id.img_wallet_card_info_franquicia)
    ImageView franquiciaImg;

    @BindView(R.id.spinner_wallet_card_info_tipo)
    Spinner cardTipoSpinner;

    @NotEmpty(messageResId = R.string.error_domicilio)
    @BindView(R.id.til_wallet_card_info_address1)
    TextInputLayout address1Til;

    @BindView(R.id.til_wallet_card_info_address2)
    TextInputLayout address2Til;

    @NotEmpty(messageResId = R.string.error_cp)
    @BindView(R.id.til_wallet_card_info_cp)
    TextInputLayout zipTil;


    @BindView(R.id.container_wallet_address_latam)
    LinearLayout latamContainer;

    //USA

    @BindView(R.id.container_wallet_address_usa)
    LinearLayout usaContainer;

    @BindView(R.id.spinner_wallet_card_info_state_usa)
    Spinner estadoUsaSpiner;

    @NotEmpty(messageResId = R.string.error_domicilio)
    @BindView(R.id.til_wallet_card_info_address_usa)
    TextInputLayout addressUsaTil;

    @NotEmpty(message = "Ingresa una ciudad válida")
    @BindView(R.id.til_wallet_card_info_city_usa)
    TextInputLayout cityUsaTil;

    @NotEmpty(messageResId = R.string.error_cp)
    @BindView(R.id.til_wallet_card_info_zip_usa)
    TextInputLayout zipUsaTil;

    //--


    @BindView(R.id.b_wallet_card_scan)
    ImageButton cardScanButton;

    @Inject
    WalletContainerActivity activity;
    @Inject
    Validator validator;
    @Inject
    Calendar expirationCalendar;
    @Inject
    Lazy<DatePickerDialog> expirationDialog;
    @Inject
    ArrayAdapter<EstadoResponse.EstadoEntity> usaEstadoAdapter;
    @Inject
    WalletCreateContract.Presenter presenter;

    private Unbinder unbinder;
    private boolean validCard = false;

    public WalletCreateFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .walletCreateSubcomponent(new WalletCreateModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_wallet_card_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden)
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        cardScanButton.setVisibility(View.VISIBLE);
        estadoUsaSpiner.setAdapter(usaEstadoAdapter);
        AndroidUtils.setText(holderNameTil.getEditText(), presenter.getCardHolderName());
        AndroidUtils.getEditText(numberTil).setOnFocusChangeListener((view1, b) -> {
            if (b) numberTil.setError(null);
        });
        AndroidUtils.getEditText(expirationTil).setOnFocusChangeListener((view12, b) -> {
            if (b) {
                expirationTil.setError(null);
                showExpirationDialog();
            } else {
                dismissExpirationDialog();
            }
        });
        AndroidUtils.getEditText(cvvTil).setOnFocusChangeListener((view1, b) -> {
            if (b) cvvTil.setError(null);
        });
        AndroidUtils.getEditText(address1Til).setOnFocusChangeListener((view1, b) -> {
            if (b) address1Til.setError(null);
        });
        AndroidUtils.getEditText(zipTil).setOnFocusChangeListener((view1, b) -> {
            if (b) zipTil.setError(null);
        });
        activity.toolbar.setTitle(R.string.txt_wallet_card_settings);

        showAdressFields(presenter.getIdPais());
    }

    private void showAdressFields(int idPais) {
        switch (idPais) {
            case PaisResponse.PaisEntity
                    .MX:
                usaContainer.setVisibility(View.GONE);
                latamContainer.setVisibility(View.VISIBLE);
                break;
            case PaisResponse.PaisEntity.CO:
            case PaisResponse.PaisEntity.PE:
                usaContainer.setVisibility(View.GONE);
                latamContainer.setVisibility(View.VISIBLE);
                zipTil.setVisibility(View.GONE);
                break;
            case PaisResponse.PaisEntity.US:
                usaContainer.setVisibility(View.VISIBLE);
                latamContainer.setVisibility(View.GONE);
                presenter.getEstados();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        activity.toolbar.setTitle(R.string.txt_menu_wallet);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                io.card.payment.CreditCard scanResult =
                        data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                setCardValuesFromScan(scanResult);

                byte[] capturedCardArr = data.getByteArrayExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE);
                if (capturedCardArr != null) {
                    cardScanButton.setImageBitmap(presenter.parseImage(
                            data.getByteArrayExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE)));
                }
            }
        } else if (requestCode == TebcaFormActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    List<CardEntity> cards =
                            data.getParcelableArrayListExtra(TebcaFormActivity.DATA_CARD_LIST);
                    updateCards(cards);
                    new Handler().postDelayed(() -> activity.onBackPressed(), 250);
                }
            }
        }
    }

    @Override
    public String getAlias() {
        return "";
    }

    @Override
    public boolean isPrimary() {
        return true;
    }

    @Override
    public String getCardHolderName() {
        return AndroidUtils.getText(holderNameTil.getEditText());
    }

    @Override
    public String getCard() {
        return AndroidUtils.getText(numberTil.getEditText());
    }

    @OnTextChanged(value = R.id.text_wallet_card_info_card, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void validateCardPan(CharSequence s) {
        int icon;
        String pan = Strings.nullToEmpty(s.toString());

        if (pan.matches(McRegexPatterns.PREVIVALE)) {
            icon = R.drawable.card_carnet;
            AndroidUtils.getEditText(numberTil).setTextColor(BLACK);
            validCard = true;
        } else if (CreditCardValidator.VISA_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_visa;
            AndroidUtils.getEditText(numberTil).setTextColor(BLACK);
            validCard = true;
        } else if (CreditCardValidator.MASTERCARD_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_master;
            AndroidUtils.getEditText(numberTil).setTextColor(BLACK);
            validCard = true;
        } else if (CreditCardValidator.AMEX_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_american;
            AndroidUtils.getEditText(numberTil).setTextColor(BLACK);
            validCard = true;
        } else {
            icon = android.R.color.transparent;
            AndroidUtils.getEditText(numberTil).setTextColor(RED);
            validCard = false;
        }
        franquiciaImg.setImageResource(icon);
    }

    @Override
    public String getCvv() {
        return AndroidUtils.getText(cvvTil.getEditText());
    }

    @OnClick(R.id.b_wallet_card_scan)
    @Override
    public void clickScan() {
        launchCardScan();
    }

    @OnLongClick(R.id.b_wallet_card_scan)
    @Override
    public boolean longClickScan() {
        showProgress();
        new Handler().postDelayed(() -> {
            clearCardData();
            hideProgress();
        }, 250);
        return true;
    }

    @Override
    public void clearCardData() {
        AndroidUtils.clearForm(holderNameTil.getEditText(), numberTil.getEditText(),
                cvvTil.getEditText(), expirationTil.getEditText(), address1Til.getEditText(),
                address2Til.getEditText(), zipTil.getEditText());
        expirationCalendar.clear();
        cardScanButton.setImageResource(R.drawable.sp_card_scan);
    }

    @OnClick(R.id.text_wallet_card_info_exp)
    @Override
    public void clickVigencia() {
        expirationTil.setError(null);
        showExpirationDialog();
    }

    @Override
    public String getVigencia() {
        return DateFormat.format("MM/yy", expirationCalendar).toString();
    }

    @Override
    public TipoTarjetaEntity getTipoTarjeta() {
        switch (cardTipoSpinner.getSelectedItemPosition()) {
            case 1:
                return TipoTarjetaEntity.DEBITO;
            case 2:
                return TipoTarjetaEntity.CUENTA_BANCARIA;
            default:
                return TipoTarjetaEntity.CREDITO;

        }
    }

    @Override
    public String getAddress1() {
        return AndroidUtils.getText(address1Til.getEditText());
    }

    @Override
    public String getAddress2() {
        return AndroidUtils.getText(address2Til.getEditText());
    }

    @Override
    public String getZipCode() {
        return AndroidUtils.getText(zipTil.getEditText());
    }

    @OnClick(R.id.b_wallet_card_info_save)
    @Override
    public void clickSave() {
        if (validCard) {
            numberTil.setError(null);
            validator.validate();
        } else {
            numberTil.setError(getString(R.string.error_wallet_not_valid));
        }
    }

    @Override
    public void showProgress() {
        activity.getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        activity.getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showExpirationDialog() {
        if (isVisible() && !expirationDialog.get().isShowing()) expirationDialog.get().show();
    }

    @Override
    public void dismissExpirationDialog() {
        if (isVisible() && expirationDialog.get().isShowing()) expirationDialog.get().dismiss();
    }

    @Override
    public void showError(int resId) {
        showError(getString(resId));
    }

    @Override
    public void showError(String msg) {
        Toasty.error(activity, msg).show();
    }

    @Override
    public void showSuccess(String msg) {
        Toasty.success(activity, msg).show();
    }

    @Override
    public void updateCards(List<CardEntity> models) {
        activity.postWalletUpdateEvent(models);
    }

    @Override
    public int getSelectedState() {
        if (usaEstadoAdapter.isEmpty()) return 0;
        else {
            return ((EstadoResponse.EstadoEntity) estadoUsaSpiner.getSelectedItem()).getId();
        }
    }

    @Override
    public void updateStates(List<EstadoResponse.EstadoEntity> states) {
        if (usaEstadoAdapter.getCount() > 0) usaEstadoAdapter.clear();
        usaEstadoAdapter.addAll(states);
    }

    @Override
    public String getDirUsa() {
        return AndroidUtils.getText(addressUsaTil.getEditText());
    }

    @Override
    public String getCityUsa() {
        return AndroidUtils.getText(cityUsaTil.getEditText());
    }

    @Override
    public String getZipUsa() {
        return AndroidUtils.getText(zipUsaTil.getEditText());
    }

    @Override
    public void onValidationSucceeded() {
        ValidationUtils.clearViewErrors(holderNameTil, numberTil, cvvTil, expirationTil, address1Til,
                address2Til, zipTil, addressUsaTil, cityUsaTil, zipUsaTil);

        if (presenter.isTebca(AndroidUtils.getText(numberTil.getEditText()))) {
            new AlertDialog.Builder(activity).setMessage(R.string.s_tebca_info_msg)
                    .setPositiveButton(R.string.txt_continuar, (dialogInterface, i) -> {
                        startActivityForResult(
                                TebcaFormActivity.Companion.get(activity, presenter.buildModel()),
                                TebcaFormActivity.REQUEST_CODE);
                        dialogInterface.dismiss();
                    })
                    .setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss())
                    .show();
        } else {
            presenter.create();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        expirationCalendar.set(Calendar.YEAR, i);
        expirationCalendar.set(Calendar.MONTH, i1);
        AndroidUtils.setText(expirationTil.getEditText(),
                DateFormat.format("MM/yy", expirationCalendar));
    }

    private void launchCardScan() {
        Intent scanIntent = new Intent(activity, CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE,
                presenter.getIdPais() == 1 || presenter.getIdPais() == 3); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false);
        scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_INSTRUCTIONS,
                getString(R.string.txt_cardio_instruction));

        startActivityForResult(scanIntent, REQUEST_CODE);
    }

    private void setCardValuesFromScan(io.card.payment.CreditCard card) {
        if (getView() != null) {
            if (!Strings.isNullOrEmpty(card.cardholderName)) {
                AndroidUtils.setText(holderNameTil.getEditText(), card.cardholderName);
            }
            AndroidUtils.setText(numberTil.getEditText(), card.cardNumber);
            validateCardPan(card.cardNumber);
            AndroidUtils.setText(cvvTil.getEditText(), Strings.nullToEmpty(card.cvv));
            AndroidUtils.setText(zipTil.getEditText(), Strings.nullToEmpty(card.postalCode));
            setExpiryDateFromScan(card.expiryYear, card.expiryMonth);
        }
    }

    private void setExpiryDateFromScan(int year, int month) {
        expirationCalendar.set(Calendar.YEAR, year);
        expirationCalendar.set(Calendar.MONTH, month - 1);
        AndroidUtils.setText(expirationTil.getEditText(),
                DateFormat.format("MM/yy", expirationCalendar));
    }
}
