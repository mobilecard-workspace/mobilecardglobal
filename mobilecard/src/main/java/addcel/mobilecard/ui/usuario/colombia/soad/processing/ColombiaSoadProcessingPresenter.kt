package addcel.mobilecard.ui.usuario.colombia.soad.processing

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.colombia.SoatConsultaResponse
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

/**
 * ADDCEL on 10/4/19.
 */
interface ColombiaSoadProcessingPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun isOnWhiteList(idUsuario: Long, idPais: Int)

    fun pagoPoliza(request: SoatConsultaResponse)
}

class ColombiaSoadProcessingPresenterImpl(
        val middleware: ColombiaMiddleware, val token: TokenizerAPI,
        val disposables: CompositeDisposable, val view: ColombiaSoadProcessingView
) :
        ColombiaSoadProcessingPresenter {
    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun isOnWhiteList(idUsuario: Long, idPais: Int) {
        view.showProgress()
        val wDisp = token.isOnWhiteList(
                BuildConfig.ADDCEL_APP_ID, idPais, StringUtil.getCurrentLanguage(), idUsuario
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.code == 0) view.onWhiteList() else view.notOnWhiteList()
                }, {
                    view.hideProgress()
                    view.notOnWhiteList()
                })
        addToDisposables(wDisp)
    }

    override fun pagoPoliza(request: SoatConsultaResponse) {
        view.showProgress()
        val pDisp =
                middleware.expedirPoliza(ColombiaMiddleware.getAuth(), request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                            view.hideProgress()
                            view.onPago(SPReceiptEntity(r.codigo, r.codigo, r.mensaje))
                        }, { t ->
                            view.hideProgress()
                            if (t is HttpException) view.onPago(SPReceiptEntity(t.code(), t.message()))
                            else view.onPago(
                                    SPReceiptEntity(
                                            -9999,
                                            t.localizedMessage
                                                    ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)
                                    )
                            )
                        })
        addToDisposables(pDisp)
    }
}
