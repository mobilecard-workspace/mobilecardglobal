package addcel.mobilecard.ui.usuario.legacy.servicios.list;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.servicios.consulta.ServicioConsultaFragment;
import addcel.mobilecard.ui.usuario.legacy.servicios.directo.ServicioFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * ADDCEL on 13/12/16.
 */
public final class ServicioSelectionFragment extends Fragment
        implements ServicioSelectionContract.View {

    @Inject
    PagoContainerActivity containerActivity;
    @Inject
    CatalogoResponse.CategoriaEntity categoria;
    @Inject
    SessionOperations session;
    @Inject
    ServicioSelectionAdapter adapter;

    @BindView(R.id.recycler_categoria)
    RecyclerView carriersView;

    private Unbinder unbinder;

    public ServicioSelectionFragment() {
    }

    public static ServicioSelectionFragment get(CatalogoResponse.CategoriaEntity model,
                                                List<ServicioResponse.ServicioEntity> servicios) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("categoria", model);
        bundle.putParcelableArrayList("servicios", (ArrayList<? extends Parcelable>) servicios);
        ServicioSelectionFragment fragment = new ServicioSelectionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .servicioSelectionSubcomponent(new ServicioSelectionModule(this))
                .inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_categoria, container, false);
        unbinder = ButterKnife.bind(this, v);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        // new GridLayoutManager(getContext(), 2);
        carriersView.setLayoutManager(manager);
        carriersView.setAdapter(adapter);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        carriersView.setItemAnimator(itemAnimator);
        enableList();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        containerActivity.setStreenStatus(PagoContainerActivity.STATUS_NORMAL);
        containerActivity.setAppToolbarTitle(categoria.getDescripcion());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        containerActivity.setAppToolbarTitle(categoria.getDescripcion());
    }

    @Override
    public void onDestroyView() {
        disableList();
        carriersView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void enableList() {
        if (carriersView != null) {
            ItemClickSupport.addTo(carriersView)
                    .setOnItemClickListener(
                            (recyclerView, position, v) -> ServicioSelectionFragment.this.onItemClicked(
                                    adapter.getItem(position)));
        }
    }

    @Override
    public void disableList() {
        if (carriersView != null) ItemClickSupport.removeFrom(carriersView);
    }

    @Override
    public void onItemClicked(ServicioResponse.ServicioEntity item) {
        disableList();
        launchFragment(item);
        new Handler().postDelayed(this::enableList, TimeUnit.SECONDS.toMillis(1));

    }

    private void launchFragment(ServicioResponse.ServicioEntity entity) {
        containerActivity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.activity_pago_container, getFragmentOnConsulta(entity))
                .hide(this)
                .addToBackStack(null)
                .commit();
    }

    private Fragment getFragmentOnConsulta(ServicioResponse.ServicioEntity entity) {
        return entity.getConsultaSaldo() ? ServicioConsultaFragment.get(entity)
                : ServicioFragment.get(entity);
    }

    @Override
    public void showProgress() {
        containerActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        containerActivity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        containerActivity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull String charSequence) {
        containerActivity.showSuccess(charSequence);
    }
}
