package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.squareup.phrase.Phrase;

import java.util.List;

import addcel.mobilecard.data.net.telepeaje.model.Tag;

public final class PeajeNewAdapter extends BaseAdapter {
    private final List<Tag> mData;

    public PeajeNewAdapter() {
        this.mData = Lists.newArrayList();
    }

    PeajeNewAdapter(List<Tag> tags) {
        this.mData = tags;
    }

    public void update(List<Tag> tags) {
        mData.clear();
        mData.addAll(tags);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public long getItemId(int position) {
        // TODO implement you own logic with ID
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(android.R.layout.simple_spinner_item, parent, false);
            holder = new ViewHolder(convertView, false);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Tag item = (Tag) getItem(position);

        // TODO replace findViewById by ViewHolder
        holder.text1.setText(item.getEtiqueta());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(android.R.layout.simple_list_item_2, parent, false);
            holder = new ViewHolder(convertView, true);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Tag item = mData.get(position);

        // TODO replace findViewById by ViewHolder
        holder.text1.setText(item.getEtiqueta());
        holder.text2.setText(Phrase.from("Tag: {tag}-{pin}")
                .put("tag", item.getTag())
                .put("pin", String.valueOf(item.getDv()))
                .format());

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    static class ViewHolder {
        private final TextView text1;
        private TextView text2;

        ViewHolder(View v, boolean dropdown) {
            text1 = v.findViewById(android.R.id.text1);
            if (dropdown) text2 = v.findViewById(android.R.id.text2);
        }
    }
}
