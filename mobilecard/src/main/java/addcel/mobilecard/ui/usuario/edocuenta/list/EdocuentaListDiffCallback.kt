package addcel.mobilecard.ui.usuario.edocuenta.list

import addcel.mobilecard.data.net.wallet.Transaction
import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil


/**
 * ADDCEL on 2019-12-02.
 */
class EdocuentaListDiffCallback(private val oldList: List<Any>, private val newList: List<Any>) :
        DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldIt = oldList[oldItemPosition]
        val newIt = newList[newItemPosition]
        return if (oldIt is Transaction && newIt is Transaction) {
            oldIt.id == newIt.id
        } else if (oldIt is String && newIt is String) {
            oldIt == newIt
        } else {
            false
        }
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldIt = oldList[oldItemPosition]
        val newIt = newList[newItemPosition]
        return if (oldIt is Transaction && newIt is Transaction) {
            oldIt.ticket == newIt.ticket && oldIt.importe == newIt.importe && oldIt.date == newIt.date
                    && oldIt.status == newIt.status && oldIt.currency == newIt.currency && oldIt.idPais == newIt.idPais && oldIt.total == newIt.total
        } else if (oldIt is String && newIt is String) {
            oldIt.hashCode() == newIt.hashCode()
        } else {
            false
        }
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {

        val oldItem: Any = oldList[oldItemPosition]
        val newItem: Any = newList[newItemPosition]

        val diff = Bundle()

        if (oldItem is Transaction && newItem is Transaction) {
            if (oldItem.ticket != newItem.ticket) {
                diff.putString("ticket", newItem.ticket)
            }

            if (oldItem.date != newItem.ticket) {
                diff.putString("date", newItem.date)
            }

            if (oldItem.total != newItem.total) {
                diff.putDouble("importe", newItem.total)
            }

            if (oldItem.status != newItem.status) {
                diff.putInt("status", newItem.status)
            }

            if (oldItem.currency != newItem.currency) {
                diff.putString("currency", newItem.currency)
            }

            if (oldItem.idPais != newItem.idPais) {
                diff.putInt("idPais", newItem.idPais)
            }

        } else if (oldItem is String && newItem is String) {
            if (oldItem != newItem) {
                diff.putString("title", newItem)
            }
        }

        return if (diff.size() == 0) {
            null
        } else diff
    }
}