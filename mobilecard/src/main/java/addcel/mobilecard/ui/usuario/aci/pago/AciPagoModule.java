package addcel.mobilecard.ui.usuario.aci.pago;

import android.widget.ArrayAdapter;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.squareup.otto.Bus;

import java.text.NumberFormat;
import java.util.List;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerFragment;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 23/03/17.
 */
@Module
public final class AciPagoModule {

    private final AciPagoFragment fragment;

    AciPagoModule(AciPagoFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ServiceModel provideModel() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelable("model");
    }

    @PerFragment
    @Provides
    boolean provideIsFav() {
        return Preconditions.checkNotNull(fragment.getArguments()).getBoolean("fav", false);
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        final Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    List<CardEntity> provideFormasPago() {
        return Lists.newArrayList();
    }

    @PerFragment
    @Provides
    @Named("addressAdapter")
    ArrayAdapter<Address> provideAddressAdapter() {
        final ArrayAdapter<Address> adapter =
                new ArrayAdapter<>(Preconditions.checkNotNull(fragment.getContext()),
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        return adapter;
    }

    @PerFragment
    @Provides
    AciService provideAciService(Retrofit retrofit) {
        return retrofit.create(AciService.class);
    }

    @PerFragment
    @Provides
    AciPagoContract.Presenter providePresenter(AciService service,
                                               FavoritoDaoRx dao, SessionOperations session, ServiceModel model, Bus bus,
                                               NumberFormat currFormat) {
        return new AciPagoPresenter(service, dao, model, session, bus, currFormat, fragment);
    }
}
