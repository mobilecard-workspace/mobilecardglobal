package addcel.mobilecard.ui.usuario.colombia.telepeaje

import addcel.mobilecard.R
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.usuario.colombia.telepeaje.menu.ColombiaTelepeajeMenuFragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_colombia_telepeaje.*

class ColombiaTelepeajeActivity : ContainerActivity() {

    companion object {
        fun get(context: Context): Intent {
            return Intent(context, ColombiaTelepeajeActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_colombia_telepeaje)

        fragmentManagerLazy.commit {
            add(R.id.frame_colombia_telepeaje, ColombiaTelepeajeMenuFragment())
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    override fun getRetry(): View? {
        return null
    }

    override fun showProgress() {
        progress_colombia_telepeaje.visibility = View.VISIBLE
    }

    override fun hideRetry() {

    }

    override fun hideProgress() {
        progress_colombia_telepeaje.visibility = View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_colombia_telepeaje.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_colombia_telepeaje.title = title
    }

    override fun showRetry() {

    }
}
