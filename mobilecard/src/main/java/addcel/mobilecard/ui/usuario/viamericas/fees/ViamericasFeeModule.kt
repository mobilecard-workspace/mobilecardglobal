package addcel.mobilecard.ui.usuario.viamericas.fees

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.usuario.viamericas.ViamericasTransactionElementAdapter
import addcel.mobilecard.ui.usuario.viamericas.fees.bank.ViamericasCardFragment
import addcel.mobilecard.ui.usuario.viamericas.fees.cash.ViamericasCashFragment
import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 27/03/17.
 */

@Module
class ViamericasFeeModule(private val fragment: Fragment, private val view: VmFeeView) {

    @PerFragment
    @Provides
    fun provideActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideModel(): VmFeeViewModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideUsuario(session: SessionOperations): Usuario {
        return session.usuario
    }

    @PerFragment
    @Provides
    fun provideCardModel(): CardEntity {
        return fragment.arguments?.getParcelable("card")!!
    }

    @PerFragment
    @Provides
    fun provideAndroidLocation(session: SessionOperations): McLocationData {
        return session.minimalLocationData
    }

    @PerFragment
    @Provides
    fun provideAdapter(): ViamericasTransactionElementAdapter {
        return ViamericasTransactionElementAdapter()
    }

    @PerFragment
    @Provides
    fun providePresenter(viamericasService: ViamericasService): ViamericasFeePresenter {
        return ViamericasFeePresenterImpl(
                viamericasService, CompositeDisposable(),
                view
        )
    }
}


@PerFragment
@Subcomponent(modules = [ViamericasFeeModule::class])
interface ViamericasFeeSubcomponent {

    fun inject(fragment: ViamericasCashFragment)

    fun inject(fragment: ViamericasCardFragment)
}

