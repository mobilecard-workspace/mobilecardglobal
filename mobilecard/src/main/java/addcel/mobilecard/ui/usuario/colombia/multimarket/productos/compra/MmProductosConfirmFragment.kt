package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.compra

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.McConstants
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.MmCompraProductoEntity
import addcel.mobilecard.data.net.colombia.multimarket.MmPagoResponse
import addcel.mobilecard.data.net.colombia.multimarket.Producto
import addcel.mobilecard.data.net.colombia.multimarket.Valor
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.PurchaseScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.listener.NumberKeyboardListener
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.result.MmProductosResultFragment
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_scanpay_qr_confirm.*
import mx.mobilecard.crypto.AddcelCrypto
import javax.inject.Inject

/**
 * ADDCEL on 2019-12-19.
 */

@Parcelize
data class MmProductosConfirmViewModel(val producto: Producto,
                                       val valor: Valor,
                                       val amount: Double = 0.0, val card: CardEntity) : Parcelable

interface MmProductosConfirmView : PurchaseScreenView {
    fun setInfo()
    fun launchLoadCard(card: CardEntity)
    fun onPagar(result: MmPagoResponse)
}

class MmProductosConfirmFragment : Fragment(), MmProductosConfirmView {

    companion object {
        fun get(args: Bundle): MmProductosConfirmFragment {
            val frag = MmProductosConfirmFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var cActivity: ContainerActivity
    @Inject
    lateinit var viewModel: MmProductosConfirmViewModel
    @Inject
    lateinit var presenter: MmProductosConfirmPresenter
    @Inject
    lateinit var picasso: Picasso

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.mmProductosConfirmSubcomponent(MmProductosConfirmModule(this)).inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_scanpay_qr_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInfo()
        launchLoadCard(viewModel.card)
        b_scanpay_qr_confirm_continuar.setOnClickListener {
            clickPurchaseButton()
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) enablePurchaseButton()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun setInfo() {
        cActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)

        val monto = presenter.getMonto(viewModel.valor, viewModel.amount)
        val comision = presenter.getComision(viewModel.valor, viewModel.amount)

        val invoice = getString(
                R.string.txt_mobiletag_confirm_invoice,
                McConstants.CO_CURR_FORMAT.format(monto),
                McConstants.CO_CURR_FORMAT.format(comision),
                viewModel.producto.nombre
        )

        view_scanpay_qr_confirm_data.text = invoice
        view_scanpay_qr_confirm_total.text = String.format("Total: %s", McConstants.CO_CURR_FORMAT.format(monto + comision))
    }

    override fun launchLoadCard(card: CardEntity) {
        picasso.load(card.imgShort)
                .placeholder(R.drawable.bg_visa)
                .placeholder(R.drawable.bg_visa)
                .fit()
                .into(bg_scan_card, object : Callback {
                    override fun onSuccess() {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }

                    override fun onError(e: Exception) {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }
                })
    }

    override fun onPagar(result: MmPagoResponse) {
        cActivity.fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, MmProductosResultFragment.get(result, viewModel.card, viewModel.amount))
            hide(this@MmProductosConfirmFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }

        enablePurchaseButton()
    }

    override fun isPurchaseButtonEnabled(): Boolean {
        return b_scanpay_qr_confirm_continuar.isEnabled
    }

    override fun disablePurchaseButton() {
        if (isVisible) b_scanpay_qr_confirm_continuar.isEnabled = false
    }

    override fun enablePurchaseButton() {
        if (isVisible) b_scanpay_qr_confirm_continuar.isEnabled = true
    }

    override fun clickPurchaseButton() {
        if (isPurchaseButtonEnabled()) {

            disablePurchaseButton()

            val request = MmCompraProductoEntity(viewModel.amount,
                    BuildConfig.ADDCEL_APP_ID, McConstants.PAIS_ID_CO,
                    viewModel.valor.productId, viewModel.card.idTarjeta,
                    presenter.getIdUsuario(),
                    StringUtil.getCurrentLanguage(), viewModel.amount * (0.05))

            presenter.comprarProducto(request)
        }
    }

    override fun showSuccess(msg: String) {
        cActivity.showSuccess(msg)
    }

    override fun showError(msg: String) {
        cActivity.showError(msg)
    }

    override fun hideProgress() {
        cActivity.hideProgress()
    }

    override fun showProgress() {
        cActivity.showProgress()
    }

    override fun onWhiteList() {

    }

    override fun notOnWhiteList() {

    }

}