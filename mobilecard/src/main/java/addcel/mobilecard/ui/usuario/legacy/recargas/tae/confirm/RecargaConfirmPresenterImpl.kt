package addcel.mobilecard.ui.usuario.legacy.recargas.tae.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.PaisResponse.PaisEntity
import addcel.mobilecard.data.net.pago.PagoEntity
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.data.net.token.TokenBaseResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.transacto.TransactoService
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.billpocket.BillPocketInteractor
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import android.annotation.SuppressLint
import android.os.Build
import mx.mobilecard.crypto.AddcelCrypto
import okio.ByteString.Companion.encodeUtf8
import java.text.NumberFormat

/**
 * ADDCEL on 19/10/17.
 */
class RecargaConfirmPresenterImpl(
        private val interactor: BillPocketInteractor,
        private val montoModel: CatalogoResponse.MontoEntity?,
        private val paisDestinoRecarga: PaisEntity?,
        private val numberFormat: NumberFormat,
        private val cardEntity: CardEntity,
        private val referencia: String,
        private val view: RecargaConfirmView,
        private val tipoCambio: Double
) : RecargaConfirmPresenter {

    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun getIdUsuario(): Long {
        return interactor.getIdUsuario()
    }

    override fun getCard(): CardEntity {
        return cardEntity
    }

    override fun getReferenciaValue(): String {
        return referencia
    }

    override fun getBalance(): String {
        return formatAmount(montoModel!!.monto, getCurrency(paisDestinoRecarga))
    }

    override fun getBalanceUSD(): String {
        val balanceUsd = montoModel!!.monto / tipoCambio
        return formatAmount(balanceUsd, CURR_US)
    }

    override fun getFee(): String {
        return formatAmount(montoModel!!.comision, getCurrency(paisDestinoRecarga))
    }

    override fun getFeeUSD(): String {
        val feeUsd = montoModel!!.comision / tipoCambio
        return formatAmount(feeUsd, CURR_US)
    }

    override fun getTotal(): String {
        val total = montoModel!!.monto + montoModel.comision
        return formatAmount(total, getCurrency(paisDestinoRecarga))
    }

    override fun getTotalUSD(): String {
        val totalUsd = (montoModel!!.monto + montoModel.comision) / tipoCambio
        return formatAmount(totalUsd, CURR_US)
    }

    override fun getFormattedTipoCambio(): String {
        return numberFormat.format(tipoCambio)
    }

    override fun getStartUrl(accountId: String): String {
        val base = BuildConfig.BASE_URL + TransactoService.PAGO
        return base.replace("{idApp}", BuildConfig.ADDCEL_APP_ID.toString())
                .replace("{idioma}", StringUtil.getCurrentLanguage())
                .replace("{accountId}", accountId)
    }

    override fun getSuccessUrl(): String {
        return "payworks2RecRespuesta"
    }

    override fun getErrorUrl(): String {
        return "Transacto/"
    }

    override fun getFormUrl(): String {
        return "https://www.procom.prosa.com.mx/eMerchant/7627488_ADCL_Diestel.jsp"
    }

    override fun getJumioRef(): String {
        return interactor.getJumioRef()
    }

    override fun getjumioStatus(): String {
        return interactor.getJumioStat()
    }

    override fun getIdPais(): Int {
        return interactor.getIdPais()
    }

    override fun getPaisDestino(): PaisEntity? {
        return paisDestinoRecarga
    }

    override fun getTipoCambio(): Double {
        return tipoCambio
    }

    override fun shouldHideUSAFields(): Boolean {
        val idPais = interactor.getIdPais()
        return idPais == PaisEntity.MX || getPaisDestino() != null && getPaisDestino()!!.id != 7
    }

    override fun checkWhiteList() {
        view.showProgress()
        interactor.checkWhiteList(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), object : InteractorCallback<TokenBaseResponse> {
            override fun onSuccess(result: TokenBaseResponse) {
                view.hideProgress()
                if (result.code == 0) {
                    view.onWhiteList()
                } else {
                    view.notOnWhiteList()
                    view.enablePurchaseButton()
                }
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
                view.enablePurchaseButton()
            }
        })
    }

    @SuppressLint("CheckResult")
    override fun getToken(profile: String) {
        view.showProgress()
        interactor.getToken(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), profile,
                buildPagoRequestAsString(buildPagoRequest()), object : InteractorCallback<TokenEntity> {
            override fun onSuccess(result: TokenEntity) {
                view.hideProgress()
                view.onTokenReceived(result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
                view.enablePurchaseButton()
            }
        })
    }

    override fun buildPagoRequest(): PagoEntity {
        val codigoPais = if (paisDestinoRecarga == null) "" else paisDestinoRecarga.codigoPais
        val idPais = interactor.getIdPais()
        val emisor = Integer.valueOf(montoModel!!.emisor)
        val software = Build.VERSION.SDK_INT.toString()
        return PagoEntity(codigoPais, montoModel.comision, montoModel.concepto,
                BuildConfig.DEBUG, emisor, BuildConfig.ADDCEL_APP_ID, idPais, montoModel.emisor,
                cardEntity.idTarjeta, interactor.getIdUsuario(), view.getImei(), interactor.getLat(),
                interactor.getLon(), montoModel.monto, montoModel.operacion, referencia, software,
                Build.MODEL)
    }

    override fun buildPagoRequestAsString(entity: PagoEntity): String {
        return AddcelCrypto.encryptSensitive(JsonUtil.toJson(entity))
    }

    override fun buildPagoRequestForBrowser(entity: PagoEntity): ByteArray {
        return ("json=" + buildPagoRequestAsString(entity)).encodeUtf8().toByteArray()
    }

    override fun executePagoBP(token: TokenEntity?, entity: PagoEntity) {
        view.lockScreen()
        view.showProgress()
        interactor.pay(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), token!!, entity,
                object : InteractorCallback<PagoResponse> {
                    override fun onSuccess(result: PagoResponse) {
                        view.hideProgress()
                        view.unlockScreen()
                        view.onBpSuccess(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.unlockScreen()
                        view.showError(error)
                    }
                })
    }

    private fun getCurrency(paisDestino: PaisEntity?): String {
        if (paisDestino != null && paisDestino.id == 7) return CURR_MX
        when (interactor.getIdPais()) {
            PaisEntity.MX -> return CURR_MX
            PaisEntity.CO -> return CURR_CO
            PaisEntity.US -> return CURR_US
            PaisEntity.PE -> return CURR_PE
        }
        return CURR_MX
    }

    private fun formatAmount(amount: Double, currency: String): String {
        return String.format("%s %s", numberFormat.format(amount), currency)
    }


    companion object {
        private const val CURR_MX = "MXN"
        private const val CURR_CO = "COP"
        private const val CURR_US = "USD"
        private const val CURR_PE = "PEN"
    }
}