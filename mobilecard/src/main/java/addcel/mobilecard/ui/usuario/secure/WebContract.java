package addcel.mobilecard.ui.usuario.secure;

import android.net.http.SslError;
import android.webkit.SslErrorHandler;

import addcel.mobilecard.MobilecardBaseView;

/**
 * ADDCEL on 29/06/18.
 */
public interface WebContract {
    interface View extends MobilecardBaseView {

        void setCurrentUrl(String url);

        void evalJS(String fun);

        void logHtml(String dom);

        void setCardData();

        void goToMainMenu();

        void goToPreviousPage();

        void showWarningExitDialog();

        void onUrlProcessed(String url, String processedUrl);

        void showDebugHandlerDialog(SslErrorHandler handler, SslError error);
    }

    interface Presenter {

        void clearDisposables();

        void getCardDataForPeaje(String endpointForm);

        String buildLoadDataFun();

        void processUrl(String url, byte[] data, String auth);
    }
}
