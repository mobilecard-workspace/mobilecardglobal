package addcel.mobilecard.ui.usuario.legacy.recargas.list

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-06-24.
 */
interface RecargaListPresenter {

    fun clearDisposables()

    fun getCarriers(categoria: CatalogoResponse.RecargaEntity)

    fun getMontos(
            categoria: CatalogoResponse.RecargaEntity,
            carrier: CatalogoResponse.RecargaEntity
    )
}

class RecargaListPresenterImpl(
        val api: CatalogoService, val usuario: Usuario,
        val disposables: CompositeDisposable, val view: View
) : RecargaListPresenter {

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getCarriers(categoria: CatalogoResponse.RecargaEntity) {

        view.showProgress()

        val request = ServiciosRequest.Builder().setIdAplicacion(BuildConfig.ADDCEL_APP_ID)
                .setIdRecarga(categoria.id).setIdioma(StringUtil.getCurrentLanguage()).build()

        disposables.add(api.getRecargaServicios(
                BuildConfig.ADDCEL_APP_ID, usuario.idPais,
                StringUtil.getCurrentLanguage(), request
        ).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
        ).subscribe({ r ->
            view.hideProgress()
            if (r.idError == 0) view.onProductos(categoria, r.servicios) else view.showError(
                    r.mensajeError
            )
        }, { t ->
            view.hideProgress()
            t.printStackTrace()
            view.showError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }

    override fun getMontos(
            categoria: CatalogoResponse.RecargaEntity,
            carrier: CatalogoResponse.RecargaEntity
    ) {
        view.showProgress()

        val request = ServiciosRequest.Builder().setIdAplicacion(BuildConfig.ADDCEL_APP_ID)
                .setIdRecarga(carrier.id).setIdServicio(carrier.id)
                .setIdioma(StringUtil.getCurrentLanguage()).build()

        disposables.add(api.getRecargaMontos(
                BuildConfig.ADDCEL_APP_ID, usuario.idPais,
                StringUtil.getCurrentLanguage(), request
        ).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
        ).subscribe({ r ->
            view.hideProgress()
            if (r.idError == 0) view.onMontos(categoria, carrier, r.montos) else view.showError(
                    r.mensajeError
            )
        }, { t ->
            view.hideProgress()
            t.printStackTrace()
            view.showError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
        })
        )
    }
}