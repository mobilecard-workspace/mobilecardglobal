package addcel.mobilecard.ui.usuario.endtoend.processing;

import com.google.common.base.Preconditions;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.H2HApi;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.h2h.H2HPaymentInteractor;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * ADDCEL on 16/10/17.
 */
@Module
public final class MxTransferProcessingModule {
    private final MxTransferProcessingFragment fragment;

    MxTransferProcessingModule(MxTransferProcessingFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MxTransferActivity provideActivity() {
        return (MxTransferActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    TokenizerAPI provideTokenAPI(Retrofit retrofit) {
        return TokenizerAPI.Companion.provideTokenizerAPI(retrofit);
    }

    @PerFragment
    @Provides
    @Named("longRetrofit")
    Retrofit provideLongRetrofit(
            @Named("longClient") OkHttpClient client, Retrofit retrofit) {
        return retrofit.newBuilder().client(client).build();
    }

    @PerFragment
    @Provides
    H2HApi providePaymentAPI(@Named("longRetrofit") Retrofit retrofit) {
        return H2HApi.Companion.get(retrofit);
    }

    @PerFragment
    @Provides
    H2HPaymentInteractor provideInteractor(H2HApi h2HApi,
                                           TokenizerAPI tokenizerAPI, SessionOperations session) {
        return new H2HPaymentInteractor(h2HApi, tokenizerAPI, session, new CompositeDisposable());
    }

    @PerFragment
    @Provides
    MxTransferProcessingContract.Presenter providePresenter(
            H2HPaymentInteractor h2HPaymentInteractor) {
        AccountEntity account =
                Preconditions.checkNotNull(fragment.getArguments()).getParcelable("account");
        CardEntity card = fragment.getArguments().getParcelable("card");
        double monto = fragment.getArguments().getDouble("monto");
        double comision = fragment.getArguments().getDouble("comision");
        return new MxTransferProcessingPresenter(h2HPaymentInteractor, account, card, monto, comision,
                fragment);
    }
}
