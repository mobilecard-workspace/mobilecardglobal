package addcel.mobilecard.ui.usuario.main.contacto;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * ADDCEL on 24/11/17.
 */
public interface ContactoContract {
    interface Constants {
        String TEL_CONTACTO = "018009255001";
        String EMAIL_CONTACTO = "soporte@addcel.com";
        String TWITTER = "https://www.twitter.com/mobilecardmx";
        String FACEBOOK = "https://www.facebook.com/mobilecardmx/";
        String WEB = "http://www.mobilecard.mx/";
    }

    interface View {

        void setUI(@NonNull android.view.View v);

        void configRecyclerView(@NonNull RecyclerView view);

        void setClicks(@NonNull android.view.View v);

        void clickFb();

        void clickTw();

        void clickLanzaAccion(int pos);

        void llamanos();

        void escribenos();

        void llamame();

        void lanzaMobilecard();

        void lanzaFacebook();

        void lanzaTwitter();

        void lanzaGPlus();

        void lanzaYoutube();

        void lanzaLinkedIn();

        void launchIntent(Intent intent);
    }

    interface Presenter {

        String buildEmailSubject();

        void lanzaAccion(int position);
    }
}
