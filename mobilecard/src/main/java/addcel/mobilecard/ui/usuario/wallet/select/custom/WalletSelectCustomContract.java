package addcel.mobilecard.ui.usuario.wallet.select.custom;

import java.util.List;

import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.usuario.wallet.select.WalletCardSelectModel;

/**
 * ADDCEL on 24/08/18.
 */
public interface WalletSelectCustomContract {
    interface View {
        void clickAddNewCard();

        void setCards(List<CardEntity> models);

        void clickChecked(int pos);

        WalletCardSelectModel getSelectedCard();
    }

    interface Presenter {
        void getCards();

        void clearDisposables();
    }
}
