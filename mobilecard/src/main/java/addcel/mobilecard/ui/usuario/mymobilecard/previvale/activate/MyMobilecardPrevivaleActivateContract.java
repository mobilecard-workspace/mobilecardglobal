package addcel.mobilecard.ui.usuario.mymobilecard.previvale.activate;

import android.text.TextWatcher;

import androidx.annotation.NonNull;

import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;

/**
 * ADDCEL on 11/16/18.
 */
public interface MyMobilecardPrevivaleActivateContract {
    interface View extends Validator.ValidationListener, TextWatcher {


        String CURP_URL = "https://www.gob.mx/curp/";

        void showProgress();

        void hideProgress();

        void showError(@NonNull String msg);

        void finishActivity();

        void showSuccess(@NonNull String msg);

        void setEstados(List<EstadoResponse.EstadoEntity> estados);

        void onPanCaptured(CharSequence digit);

        void clickCurp();

        void clickActivate();

        void onActivated(CardEntity response);
    }

    interface Presenter {

        void clearDisposables();

        String getNombre();

        String getPaterno();

        String getMaterno();

        void getEstados();

        void onClickActivate(PrevivaleUserRequest request);
    }
}
