package addcel.mobilecard.ui.usuario.wallet.update;

import android.app.DatePickerDialog;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Calendar;
import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.wallet.update.WalletUpdateInteractor;
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import mx.mobilecard.crypto.AddcelCrypto;
import timber.log.Timber;

@Module
public final class WalletUpdateModule {
    private final WalletUpdateFragment fragment;

    WalletUpdateModule(WalletUpdateFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    WalletContainerActivity provideActivity() {
        return (WalletContainerActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    CardEntity provideModel() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelable("card");
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    Calendar provideVigenciaCalendar(CardEntity model) {
        final Calendar c = Calendar.getInstance();
        final String[] vigencia =
                CardEntity.Companion.getVigenciaAsArray(AddcelCrypto.decryptHard(model.getVigencia()));
        Timber.d("Mes y año: %s, %s", vigencia[0], vigencia[1]);
        c.set(Calendar.MONTH, Integer.parseInt(vigencia[0]) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(vigencia[1]) + 2000);
        return c;
    }

    @PerFragment
    @Provides
    DatePickerDialog datePickerDialog(
            @NonNull WalletContainerActivity activity, Calendar calendar) {

        DatePickerDialog datePickerDialog;
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            datePickerDialog =
                    new FixedHoloDatePickerDialog(activity, fragment, calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog =
                        new DatePickerDialog(activity, R.style.CustomDatePickerDialogTheme, fragment,
                                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                calendar.get(Calendar.DAY_OF_MONTH));
            } else {
                // R.style.CustomDatePickerDialogTheme
                datePickerDialog = new DatePickerDialog(activity, fragment, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            }
        }

        datePickerDialog.getDatePicker()
                .findViewById(Resources.getSystem().getIdentifier("day", "id", "android"))
                .setVisibility(View.GONE);
        return datePickerDialog;
    }


    @PerFragment
    @Provides
    ArrayAdapter<EstadoResponse.EstadoEntity> provideAdapter(WalletContainerActivity activity) {
        ArrayAdapter<EstadoResponse.EstadoEntity> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter.setNotifyOnChange(true);

        return adapter;
    }

    @PerFragment
    @Provides
    WalletUpdateInteractor provideInteractor(WalletAPI api, CatalogoService catalogo,
                                             SessionOperations session) {
        return new WalletUpdateInteractor(api, catalogo, session.getUsuario(), new CompositeDisposable());
    }

    @PerFragment
    @Provides
    WalletUpdateContract.Presenter providePresenter(
            WalletUpdateInteractor interactor) {
        return new WalletUpdatePresenter(interactor, fragment);
    }
}
