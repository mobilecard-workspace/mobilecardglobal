package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select;

import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mobsandgeeks.saripaar.Validator;
import com.squareup.phrase.Phrase;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.telepeaje.model.Tag;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 09/08/17.
 */
public interface PeajeContract {

    interface View extends ScreenView, Validator.ValidationListener, DialogInterface.OnShowListener {

        Tag getPeajeTag();

        String getNumeroTag();

        String getPin();

        String getImei();

        boolean isOtroNumeroChecked();

        void checkOtroNumeroSwitch(boolean checked);

        void onTagCaptured(CharSequence sequence);

        void onDvCaptured(CharSequence sequence);

        boolean validateTagCaptured(String tag, String dv);

        void notifyNoTags();

        void setTags(List<Tag> tags);

        void onTagSelected(int pos);

        void onTagNotSelected();

        void clickFav();

        void clickSaveFav();

        void setFav(boolean fav);

        void setInitialFav(boolean fav);

        void updateFavViewsOnSuccess();

        void clickAdd();

        void clickDelete();

        void showAliasDialog(String tag, String dv);

        void clearAliasDialog();

        void clickRecarga();

        void showSuccess(int resId);

        void onSaldoTagChecked(double saldoTag);

        void hideSaldo();

        void showSaldo(double saldo);
    }

    interface Presenter {

        void clearCompositeDisposables();

        void getTags(CatalogoResponse.RecargaEntity model);

        void addTag(CatalogoResponse.RecargaEntity model, String tag, String dv, String alias);

        void removeTag(CatalogoResponse.RecargaEntity servicio, Tag tag);

        void saveToFav(CatalogoResponse.RecargaEntity recargaModel,
                       List<CatalogoResponse.MontoEntity> montos, int logo, String etiqueta);

        String buildSaldoText(Phrase phrase, double saldo);

        String buildAliasText(Phrase phrase, String tag, String dv);

        String[] selectTagAndPin(@Nullable Tag selectedTag, String writtenTag, String pin);

        String[] getTagFromModel(@NonNull Tag selectedTag) throws IllegalArgumentException;

        String[] getTagFromInput(String tagNumber, String pin);

        boolean validateTagInput(@Nullable String tag, @Nullable String dv);

        void checkSaldoTag(String dv, String etiqueta, String tag,
                           CatalogoResponse.MontoEntity montoModel);

        void checkAndShowSaldo(Tag tag);
    }
}
