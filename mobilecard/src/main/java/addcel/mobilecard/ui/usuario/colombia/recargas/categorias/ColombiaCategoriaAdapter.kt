package addcel.mobilecard.ui.usuario.colombia.recargas.categorias

import addcel.mobilecard.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

/**
 * ADDCEL on 9/19/19.
 */

enum class ColombiaCategoria(val logo: Int, val text: Int) {
    TAE(R.drawable.col_tae, R.string.nav_col_tae_tae), DATOS(
            R.drawable.col_tae,
            R.string.nav_col_tae_datos
    )
}

class ColombiaCategoriaAdapter : RecyclerView.Adapter<ColombiaCategoriaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ColombiaCategoria.values().size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cat = ColombiaCategoria.values()[position]
        holder.logo.setImageResource(cat.logo)
        holder.text.setText(cat.text)
    }

    fun getItem(pos: Int): ColombiaCategoria {
        return ColombiaCategoria.values()[pos]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val logo: ImageView = itemView.findViewById(R.id.img_menu_item_icon)
        val text: TextView = itemView.findViewById(R.id.text_menu_item_name)
    }
}