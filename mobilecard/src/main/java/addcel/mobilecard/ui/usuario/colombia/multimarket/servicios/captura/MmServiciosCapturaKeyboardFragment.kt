package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.captura

import addcel.mobilecard.McConstants
import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Categoria
import addcel.mobilecard.data.net.colombia.multimarket.MmConsultaResponse
import addcel.mobilecard.data.net.colombia.multimarket.Servicio
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.listener.NumberKeyboardListener
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.view_keyboard_full.*
import timber.log.Timber

/**
 * ADDCEL on 2019-12-19.
 */

@Parcelize
data class MmServiciosCapturaKeyboardViewModel(val cat: Categoria, val servicio: Servicio, val consulta: MmConsultaResponse) : Parcelable

class MmServiciosCapturaKeyboardFragment : Fragment(), NumberKeyboardListener {

    companion object {
        fun get(categoria: Categoria, servicio: Servicio, consulta: MmConsultaResponse): MmServiciosCapturaKeyboardFragment {
            val args = BundleBuilder()
                    .putParcelable("model",
                            MmServiciosCapturaKeyboardViewModel(categoria, servicio, consulta)
                    ).build()
            val frag = MmServiciosCapturaKeyboardFragment()
            frag.arguments = args
            return frag
        }
    }


    private lateinit var viewModel: MmServiciosCapturaKeyboardViewModel

    private var montoString = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = arguments?.getParcelable("model")!!
    }


    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (hidden) {
            (activity as ContainerActivity).setAppToolbarTitle(R.string.nav_col_prepago)
        } else {
            (activity as ContainerActivity).setAppToolbarTitle("Ingresa el valor de tu factura")
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_keyboard_full, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as ContainerActivity).setAppToolbarTitle("Ingresa el valor de tu factura")
        (activity as ContainerActivity).setStreenStatus(ContainerScreenView.STATUS_NORMAL)

        text_keyboard_monto.isFocusable = false
        text_keyboard_monto.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                b_keyboard_total_delete.isEnabled = editable.isNotEmpty()
                b_keyboard_recargar.isEnabled = editable.isNotEmpty()
            }
        })
        b_keyboard_total_delete.isEnabled = false
        b_keyboard_recargar.isEnabled = false
        b_keyboard_concepto.visibility = View.INVISIBLE

        b_keyboard_total_one.setOnClickListener { onKeyStroke("1") }
        b_keyboard_total_two.setOnClickListener { onKeyStroke("2") }
        b_keyboard_total_three.setOnClickListener { onKeyStroke("3") }
        b_keyboard_total_four.setOnClickListener { onKeyStroke("4") }
        b_keyboard_total_five.setOnClickListener { onKeyStroke("5") }
        b_keyboard_total_six.setOnClickListener { onKeyStroke("6") }
        b_keyboard_total_seven.setOnClickListener { onKeyStroke("7") }
        b_keyboard_total_eight.setOnClickListener { onKeyStroke("8") }
        b_keyboard_total_nine.setOnClickListener { onKeyStroke("9") }
        b_keyboard_total_delete.setOnClickListener { onDeleteStroke() }
        b_keyboard_total_zero.setOnClickListener { onKeyStroke("0") }
        b_keyboard_recargar.setOnClickListener { launchConcepto() }
    }

    private fun launchConcepto() {

        if (montoString.isNotEmpty()) {
            val monto = montoString.toDouble()
            val comision = monto * 0.05

            val updateConsulta = viewModel.consulta.copy(monto = monto, comision = comision)


            val bundle = BundleBuilder().putParcelable("categoria", viewModel.cat)
                    .putParcelable("servicio", viewModel.servicio)
                    .putParcelable("saldo", updateConsulta)
                    .build()

            (activity as ContainerActivity).fragmentManagerLazy.commit {
                add(R.id.frame_colombia_servicios, WalletSelectFragment.get(WalletSelectFragment.SERVICIOS_COL, bundle))
                hide(this@MmServiciosCapturaKeyboardFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        } else {
            (activity as ContainerActivity).showError(getString(R.string.error_cantidad))
        }
    }

    override fun onKeyStroke(captured: String) {
        Timber.d("KEY CAPTURADA: %s", captured)

        if (captured.isNotEmpty() && StringUtil.isDecimalAmount(captured)) {
            montoString += captured
            text_keyboard_monto.text = NumberKeyboardListener.getFormattedMontoDecimal(montoString, McConstants.PAIS_ID_CO)
        } else {
            montoString = ""
            text_keyboard_monto.text = ""
        }
    }

    override fun onDeleteStroke() {
        if (montoString.isNotEmpty()) {
            montoString = StringUtil.removeLast(montoString)
            text_keyboard_monto.text = NumberKeyboardListener.getFormattedMontoDecimal(montoString, McConstants.PAIS_ID_CO)
        }
    }

}