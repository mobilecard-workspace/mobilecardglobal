package addcel.mobilecard.ui.usuario.wallet.update;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.CardRequest;
import addcel.mobilecard.data.net.wallet.CardResponse;
import addcel.mobilecard.data.net.wallet.FranquiciaEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.wallet.update.WalletUpdateInteractor;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 10/10/17.
 */
class WalletUpdatePresenter implements WalletUpdateContract.Presenter {
    private final WalletUpdateInteractor interactor;
    private final WalletUpdateContract.View view;

    WalletUpdatePresenter(WalletUpdateInteractor interactor, WalletUpdateContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public String getAlias(CardEntity entity) {
        return String.format("%s %s", entity.getTipo().getDescription(),
                entity.getTipoTarjeta().name());
    }

    @Override
    public boolean isPrimary(CardEntity entity) {
        return entity.getDeterminada();
    }

    @Override
    public boolean isMobilecard(CardEntity entity) {
        return entity.getMobilecard();
    }

    @Override
    public String getCardHolderName(CardEntity entity) {
        return Strings.nullToEmpty(entity.getNombre());
    }

    @Override
    public String getCard(CardEntity entity) {
        return StringUtil.maskCard(Strings.nullToEmpty(AddcelCrypto.decryptHard(entity.getPan())));
    }

    @Override
    public FranquiciaEntity getFranquicia(CardEntity entity) {
        return entity.getTipo();
    }

    @Override
    public String getCvv(CardEntity entity) {
        return Strings.nullToEmpty(AddcelCrypto.decryptHard(entity.getCodigo()));
    }

    @Override
    public String[] getVigencia(CardEntity entity) {
        return CardEntity.Companion.getVigenciaAsArray(AddcelCrypto.decryptHard(entity.getVigencia()));
    }

    @Override
    public TipoTarjetaEntity getTipoTarjeta(CardEntity entity) {
        return entity.getTipoTarjeta();
    }

    @Override
    public String getDireccion1(CardEntity entity) {
        return Strings.nullToEmpty(entity.getDomAmex());
    }

    @Override
    public String getDireccion2(CardEntity entity) {
        return "";
    }

    @Override
    public String getZipCode(CardEntity entity) {
        return Strings.nullToEmpty(entity.getCpAmex());
    }

    @SuppressLint("CheckResult")
    @Override
    public void update(CardEntity entity) {
        view.showProgress();
        CardRequest request = new CardRequest(entity.getIdTarjeta(), interactor.getIdUsuario(),
                AddcelCrypto.encryptHard(view.getCard()), AddcelCrypto.encryptHard(view.getVigencia()),
                AddcelCrypto.encryptHard(view.getCvv()), view.getCardHolderName(), entity.getDeterminada(),
                StringUtil.removeAcentosMultiple(
                        view.getAddress1().concat(" ").concat(view.getAddress2()).trim()), view.getZipCode(),
                entity.getMobilecard(), view.getTipoTarjeta(), StringUtil.getCurrentLanguage(), view.getDirUsa(), view.getCityUsa(), view.getSelectedState(), view.getZipUsa());

        interactor.update(BuildConfig.ADDCEL_APP_ID, request, new InteractorCallback<CardResponse>() {
            @Override
            public void onSuccess(@NotNull CardResponse result) {
                view.hideProgress();
                if (result.getIdError() == 0) {
                    view.updateCards(result.getTarjetas());
                    view.showSuccess(result.getMensajeError());
                } else {
                    view.setValuesOnError();
                    view.showError(result.getMensajeError());
                }
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.setValuesOnError();
                view.showError(error);
            }
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void delete(CardEntity entity) {
        view.showProgress();

        interactor.delete(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                entity.getIdTarjeta(), new InteractorCallback<CardResponse>() {
                    @Override
                    public void onSuccess(@NotNull CardResponse result) {
                        view.hideProgress();
                        if (result.getIdError() == 0) {
                            view.updateCardsAndFinish(result.getTarjetas());
                            view.showSuccess(result.getMensajeError());
                        } else {
                            view.setValuesOnError();
                            view.showError(result.getMensajeError());
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.setValuesOnError();
                        view.showError(error);
                    }
                });
    }

    @Override
    public int getIdPais() {
        return interactor.getUsuario().getIdPais();
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @Override
    public void getEstados() {
        interactor.getEstados(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), new InteractorCallback<EstadoResponse>() {
            @Override
            public void onSuccess(@NotNull EstadoResponse result) {
                view.hideProgress();
                view.updateStates(result.getEstados());

            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError(error);
            }
        });
    }
}
