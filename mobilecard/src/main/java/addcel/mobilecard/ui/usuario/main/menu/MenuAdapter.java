package addcel.mobilecard.ui.usuario.main.menu;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.os.BuildCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.base.Strings;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.CountryNotificationEnvent;
import addcel.mobilecard.data.net.usuarios.NameNotificationEvent;
import addcel.mobilecard.data.net.usuarios.PhotoNotificationEvent;
import addcel.mobilecard.ui.usuario.perfil.Perfil2Activity;
import de.hdodenhof.circleimageview.CircleImageView;
import okio.ByteString;
import timber.log.Timber;

/**
 * ADDCEL on 04/10/17.
 */
final class MenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static int TYPE_HEADER = 0;
    private final static int TYPE_ITEM = 1;

    private final List<MenuModel> menuModels;
    private final String name;
    private final String profileB64;
    private final Bus bus;
    private int country;

    MenuAdapter(List<MenuModel> menuModels, String profileB64, String name, int idPais, Bus bus,
                boolean hasIngo) {
        this.menuModels = menuModels;
        this.profileB64 = profileB64;
        this.name = name;
        this.country = idPais;
        this.bus = bus;
        this.menuModels.remove(MenuModel.WALLET);
        this.menuModels.remove(MenuModel.FAVORITES);
        processMenuModelByCountry(this.menuModels, idPais, true);
    }

    public void update(List<MenuModel> menuModels, int idPais, boolean hasIngo) {
        this.menuModels.clear();
        this.menuModels.addAll(menuModels);
        this.menuModels.remove(MenuModel.WALLET);
        this.menuModels.remove(MenuModel.FAVORITES);
        this.country = idPais;
        processMenuModelByCountry(this.menuModels, idPais, hasIngo);
        notifyDataSetChanged();
    }

    private void processMenuModelByCountry(List<MenuModel> models, int idPais, boolean hasIngo) {
        switch (idPais) {
            case 1:
                if (!BuildConfig.DEBUG) models.remove(MenuModel.MOBILE_TAG);
                models.remove(MenuModel.CHECKS);
                models.remove(MenuModel.TRANSFERS);
                models.remove(MenuModel.BILLING_US);
                models.remove(MenuModel.ESCANEA);
                models.remove(MenuModel.COL_SERVICIOS);
                models.remove(MenuModel.COL_PEAJE);
                models.remove(MenuModel.COL_SOAT);
                models.remove(MenuModel.COL_TAE);
                models.remove(MenuModel.COL_PREPAGO);
                break;
            case 2:
                models.remove(MenuModel.MOBILE_TAG);
                models.remove(MenuModel.CHECKS);
                models.remove(MenuModel.ESCANEA);
                models.remove(MenuModel.TRANSFERS);
                models.remove(MenuModel.TRANSFERS_MX);
                models.remove(MenuModel.SCANPAY);
                models.remove(MenuModel.TOPUP);
                models.remove(MenuModel.BILLING);
                models.remove(MenuModel.BILLING_US);

                //COMENTAR PARA SIGUIENTE RELEASE
                //models.remove(MenuModel.BILLING);
                models.remove(MenuModel.COL_PEAJE);
                //models.remove(MenuModel.COL_SOAT);
                break;
            case 3:
                models.remove(MenuModel.MOBILE_TAG);
                models.remove(MenuModel.TRANSFERS_MX);
                models.remove(MenuModel.BILLING);
                models.remove(MenuModel.ESCANEA); //AGREGAR ESTE LLAMADO
                models.remove(MenuModel.SCANPAY);
                models.remove(MenuModel.COL_PEAJE);
                models.remove(MenuModel.COL_SOAT);
                models.remove(MenuModel.COL_TAE);
                models.remove(MenuModel.COL_PREPAGO);
                models.remove(MenuModel.COL_SERVICIOS);
                break;
            case 4:
                models.remove(MenuModel.MOBILE_TAG);
                models.remove(MenuModel.CHECKS);
                models.remove(MenuModel.TRANSFERS);
                models.remove(MenuModel.BILLING_US);
                models.remove(MenuModel.SCANPAY);
                models.remove(MenuModel.COL_PEAJE);
                models.remove(MenuModel.COL_SOAT);
                models.remove(MenuModel.COL_TAE);
                models.remove(MenuModel.COL_PREPAGO);
                models.remove(MenuModel.COL_SERVICIOS);
                break;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
            return new ItemViewHolder(v);
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_header_menu, parent, false);
            return new HeaderViewHolder(v, bus);
        } else {
            throw new RuntimeException("there is no type that matches the type "
                    + viewType
                    + " + make sure your using types correctly");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {

            MenuModel model = getItem(position);

            ((ItemViewHolder) holder).icon.setImageResource(model.icon);
            ((ItemViewHolder) holder).name.setText(model.desc);
        } else if (holder instanceof HeaderViewHolder) {
            TextView nameV = ((HeaderViewHolder) holder).nameView;
            if (TextUtils.isEmpty(nameV.getText())) {
                nameV.setText(String.format("Bienvenido %s", name));
            }
            ((HeaderViewHolder) holder).postImage(new PhotoNotificationEvent(profileB64));
            ((HeaderViewHolder) holder).postCountry(new CountryNotificationEnvent(country));
        }
    }

    @Override
    public int getItemCount() {
        return menuModels.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public MenuModel getItem(int pos) {
        return menuModels.get(pos - 1);
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        private final ImageView icon;
        private final TextView name;

        ItemViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.img_menu_item_icon);
            name = itemView.findViewById(R.id.text_menu_item_name);
        }
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final CircleImageView fotoView;
        private final CircleImageView perfilButton;
        private final CircleImageView paisView;
        private final TextView nameView;
        private final Bus mBus;

        HeaderViewHolder(@NonNull View itemView, Bus bus) {
            super(itemView);
            this.mBus = bus;
            fotoView = itemView.findViewById(R.id.img_menu_profile);
            perfilButton = itemView.findViewById(R.id.b_menu_profile);
            paisView = itemView.findViewById(R.id.img_menu_flag);
            nameView = itemView.findViewById(R.id.text_menu_welcome);

            perfilButton.setOnClickListener(
                    view -> view.getContext().startActivity(Perfil2Activity.get(view.getContext())));

            mBus.register(this);
        }

        @Subscribe
        public void postName(NameNotificationEvent event) {
            Timber.d("Setteando nombre :%s", Strings.nullToEmpty(event.getName()));
            nameView.setText(String.format("Bienvenido %s", Strings.nullToEmpty(event.getName())));
        }

        @Subscribe
        public void postCountry(CountryNotificationEnvent event) {
            Timber.d("Setteando pais: %d", event.getId());
            if (event.getId() == PaisResponse.PaisEntity.MX)
                paisView.setImageResource(R.drawable.flag_mex);
            else if (event.getId() == PaisResponse.PaisEntity.CO)
                paisView.setImageResource(R.drawable.flag_colombia);
            else if (event.getId() == PaisResponse.PaisEntity.US)
                paisView.setImageResource(R.drawable.flag_usa);
            else if (event.getId() == PaisResponse.PaisEntity.PE)
                paisView.setImageResource(R.drawable.flag_peru);
        }

        @Subscribe
        public void postImage(PhotoNotificationEvent event) {
            if (!Strings.isNullOrEmpty(event.getPhoto64())) {
                ByteString bts = ByteString.decodeBase64(event.getPhoto64());
                if (bts != null) {
                    byte[] data = bts.toByteArray();
                    Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                    fotoView.setImageBitmap(bmp);
                }
            }
        }
    }
}
