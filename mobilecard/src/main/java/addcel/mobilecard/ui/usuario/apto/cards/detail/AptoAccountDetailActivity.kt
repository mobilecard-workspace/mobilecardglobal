package addcel.mobilecard.ui.usuario.apto.cards.detail

import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.accounts.Data
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_account_detail.*
import java.text.NumberFormat
import java.util.*

@Parcelize
data class AptoAccountDetailModel(val transaction: Data) : Parcelable

class AptoAccountDetailActivity : AppCompatActivity(), OnMapReadyCallback {
    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        val sydney = LatLng(19.432608, -99.133209)
        mMap.addMarker(MarkerOptions().position(sydney).title(model.transaction.description))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    companion object {
        fun get(context: Context, model: AptoAccountDetailModel): Intent {
            return Intent(context, AptoAccountDetailActivity::class.java).putExtra("model", model)
        }

        val USA_FORMAT: NumberFormat = NumberFormat.getCurrencyInstance(Locale.US)
    }

    lateinit var model: AptoAccountDetailModel
    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_account_detail)
        model = intent?.getParcelableExtra("model")!!
        detail_concept.text = model.transaction.declineCode
        detail_amount.text = USA_FORMAT.format(model.transaction.localAmount.amount)
        view_detail_device.visibility = View.GONE
        view_detail_type.text = model.transaction.transactionType
        view_detail_id.text = model.transaction.id
        view_detail_to_account.visibility = View.GONE
        info_detail_to_account.text =
                "ID: ${model.transaction.store.id}\n${model.transaction.store.name}"
        view_detail_from_account.text = model.transaction.store.name
        info_detail_from_account.text =
                "ID: ${model.transaction.id}\n${model.transaction.fundingSourceName}"

        getString(R.string.google_maps_key)

        val mapFragment =
                supportFragmentManager.findFragmentById(R.id.detail_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }
}
