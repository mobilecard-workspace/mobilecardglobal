package addcel.mobilecard.ui.usuario.colombia.recargas.monto;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.colombia.entity.ColombiaRecargaCarrierEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaTiendaEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.custom.view.CheckableButton;
import addcel.mobilecard.ui.usuario.colombia.recargas.ColombiaRecargasActivity;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.BundleBuilder;

/**
 * ADDCEL on 10/17/18.
 */
public final class ColombiaRecargaMontoFragment extends Fragment
        implements ColombiaRecargaMontoContract.View {
    private LinearLayout dataContainer;
    private RecyclerView montosView;
    private CheckableButton pagarButton;
    private ColombiaRecargaCarrierEntity carrier;
    private ColombiaRecargaMontoAdapter adapter;
    private ColombiaRecargasActivity activity;
    private ColombiaRecargaMontoContract.Presenter presenter;

    public ColombiaRecargaMontoFragment() {
    }

    public static synchronized ColombiaRecargaMontoFragment get(
            @NonNull ColombiaTiendaEntity.RecargaEntity recarga,
            @NonNull ColombiaRecargaCarrierEntity carrier) {
        ColombiaRecargaMontoFragment fragment = new ColombiaRecargaMontoFragment();
        Bundle arguments =
                new BundleBuilder().putParcelable(ColombiaRecargaMontoContract.KEY_RECARGA, recarga)
                        .putParcelable(ColombiaRecargaMontoContract.KEY_CARRIER, carrier)
                        .build();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (ColombiaRecargasActivity) Objects.requireNonNull(getActivity());
        ColombiaTiendaEntity.RecargaEntity recarga = Objects.requireNonNull(getArguments())
                .getParcelable(ColombiaRecargaMontoContract.KEY_RECARGA);
        carrier = getArguments().getParcelable(ColombiaRecargaMontoContract.KEY_CARRIER);
        adapter = new ColombiaRecargaMontoAdapter(Objects.requireNonNull(carrier).getMontos());
        presenter = new ColombiaRecargaMontoPresenterImpl(recarga, carrier);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_colombia_recarga_monto, container, false);
        dataContainer = view.findViewById(R.id.container_colombia_recarga_monto);
        montosView = view.findViewById(R.id.recycler_colombia_recarga_monto);
        pagarButton = view.findViewById(R.id.b_colombia_recarga_monto_pagar);
        pagarButton.setOnClickListener(v -> clickContinuar());
        configRecyclerView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        activity.setAppToolbarTitle(carrier.getLabel());
        presenter.fillContainer(dataContainer);
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void configRecyclerView() {
        montosView.setAdapter(adapter);
        montosView.setLayoutManager(new GridLayoutManager(activity, 2));
        montosView.setHasFixedSize(true);
        ItemClickSupport.addTo(montosView).setOnItemClickListener((recyclerView, position, v) -> {
            adapter.toggleSelection(position);
            if (adapter.getSelectedItemCount() > 0) {
                if (presenter.evalForm(dataContainer)) {
                    pagarButton.setChecked(true);
                } else {
                    pagarButton.setChecked(false);
                }
            } else {
                pagarButton.setChecked(false);
            }
        });
    }

    @Override
    public void clickContinuar() {
        presenter.clearErrorsInForm(dataContainer);
        if (presenter.evalForm(dataContainer)) {
            if (adapter.getSelectedItemCount() > 0) {
                List<ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity> filledCampos =
                        presenter.getFilledCampos(dataContainer);
                Bundle arguments = presenter.buildPagoBundle(filledCampos, adapter.getSelectedItem());
                activity.getFragmentManagerLazy()
                        .beginTransaction()
                        .add(R.id.frame_colombia,
                                WalletSelectFragment.get(WalletSelectFragment.RECARGAS_COL, arguments))
                        .hide(this)
                        .addToBackStack(null)
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                        .commit();
            } else {
                showError(activity.getString(R.string.error_monto_select));
            }
        }
    }
}
