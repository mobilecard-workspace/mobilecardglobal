package addcel.mobilecard.ui.usuario.mymobilecard.previvale.activate;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 11/16/18.
 */
@PerFragment
@Subcomponent(modules = MyMobilecardPrevivaleActivateModule.class)
public interface MyMobilecardPrevivaleActivateSubcomponent {
    void inject(MyMobilecardPrevivaleActivateFragment fragment);
}
