package addcel.mobilecard.ui.usuario.edocuenta.list;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 2019-06-12.
 */
@PerFragment
@Subcomponent(modules = EdocuentaListModule.class)
public interface EdocuentaListSubcomponent {
    void inject(HistorialListFragment fragment);
}
