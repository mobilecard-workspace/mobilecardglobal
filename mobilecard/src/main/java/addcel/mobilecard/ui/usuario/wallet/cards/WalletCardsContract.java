package addcel.mobilecard.ui.usuario.wallet.cards;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.MovementEntity;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingCallback;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormCallback;
import addcel.mobilecard.ui.usuario.wallet.WalletUpdateEvent;

/**
 * ADDCEL on 09/10/17.
 */
public interface WalletCardsContract {
    interface View extends TebcaFormCallback, MyMcRoutingCallback {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showError(int resId);

        void showSuccess(String msg);

        void clickAdd();

        void clickEdit();

        void clickAllTransactions();

        void launchEdit(CardEntity model);

        void clickDelete(CardEntity model);

        void clickFav(CardEntity model);

        void onCardsUpdated(List<CardEntity> models);

        void postCardEvent(WalletUpdateEvent event);

        void onMovementsReceived(List<MovementEntity> movementEntities);
    }

    interface Presenter {

        void clearDisposables();

        Usuario getUsuario();

        String getLast4Digits(CardEntity model);

        void list();

        void deleteCard(CardEntity model);

        void favCard(CardEntity model);

        void getLastMovements();
    }
}
