package addcel.mobilecard.ui.usuario.ingo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.common.base.Strings;
import com.ingomoney.ingosdk.android.IngoSdkManager;
import com.ingomoney.ingosdk.android.manager.GoogleAnalyticsHelper;
import com.ingomoney.ingosdk.android.manager.IovationHelper;
import com.ingomoney.ingosdk.android.manager.JsonDeserializer;
import com.ingomoney.ingosdk.android.manager.JsonSerializer;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.domain.permission.PermissionRequestConstants;
import addcel.mobilecard.ui.usuario.ingo.registro.IngoRegistroFragment;
import addcel.mobilecard.ui.usuario.wallet.select.add.WalletSelectAddFragment;
import addcel.mobilecard.utils.DeviceUtil;
import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;

public class IngoActivity extends AppCompatActivity implements IngoContract.View {
    private static final String[] INGO_PERMISSION = {
            "android.permission.READ_PHONE_STATE", "android.permission.ACCESS_COARSE_LOCATION",
            "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_NETWORK_STATE",
            "android.permission.CAMERA", "android.permission.INTERNET",
            "android.permission.WRITE_EXTERNAL_STORAGE"
    };

    private static final int[] INGO_GRANTED = {0, 0, 0, 0, 0, 0, 0};
    private final CompositeDisposable permissionsDisposables = new CompositeDisposable();
    @Inject
    IngoContract.Presenter presenter;
    @Inject
    GoogleAnalyticsHelper googleAnalyticsHelper;
    @Inject
    IovationHelper iovationHelper;
    @Inject
    JsonDeserializer jsonDeserializer;
    @Inject
    JsonSerializer jsonSerializer;


    private IngoSdkManager ingoSdkManager;
    private Button retryButton;
    private FrameLayout container;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private boolean finished;

    public static synchronized Intent create(Context context) {
        return new Intent(context, IngoActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().ingSubcomponent(new IngoModule(this)).inject(this);
        setContentView(R.layout.activity_ingo);


        container = findViewById(R.id.frame_ingo);
        retryButton = findViewById(R.id.b_ingo_permission_retry);
        retryButton.setOnClickListener(v -> checkPermissions());
        getToolbarFromActivity().setTitle(R.string.txt_menu_cheque);
        presenter.checkIfAvailable();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }

    private void checkPermissions() {

        RxPermissions permissions = new RxPermissions(this);

        permissionsDisposables.add(
                permissions.request(INGO_PERMISSION)
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                IngoSdkManager.getInstance().onRequestPermissionsResult(PermissionRequestConstants.INGO, INGO_PERMISSION, INGO_GRANTED);
                                container.setVisibility(View.VISIBLE);
                                retryButton.setVisibility(View.GONE);
                                ingoSdkManager = initIngoSdk();
                                if (ingoSdkManager != null) {
                                    presenter.getUserData();
                                } else {
                                    showError(getString(R.string.txt_ingo_error_compatibility));
                                    kill();
                                }
                            } else {
                                container.setVisibility(View.GONE);
                                retryButton.setVisibility(View.VISIBLE);
                            }
                        })
        );
    }

    @Override
    protected void onDestroy() {
        ingoSdkManager = null;
        presenter.onDestroy();
        permissionsDisposables.clear();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IngoSdkManager.getInstance().onActivityResult(requestCode, resultCode);
    }

    @Override
    public void showProgress() {
        getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showError(@NonNull String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        Toasty.success(this, msg).show();
    }

    @Override
    public Toolbar getToolbarFromActivity() {
        if (toolbar == null) toolbar = findViewById(R.id.toolbar_ingo);
        return toolbar;
    }

    @Override
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override
    public void kill() {
        finish();
    }

    @Override
    public IngoService getService() {
        return presenter.getService();
    }

    @Override
    public IngoAliveAPI getAliveApi() {
        return presenter.getAliveApi();
    }

    @Override
    public void isRegistered(IngoUserData data) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("ingoData", data);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_ingo, WalletSelectAddFragment.get(WalletSelectAddFragment.INGO, bundle))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @Override
    public void notRegistered(String msg) {
        showError(msg);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_ingo, IngoRegistroFragment.get())
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @Override
    public IngoSdkManager getIngoSdkManager() {
        return ingoSdkManager;
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(this);
    }

    @Override
    public void onIngoAvailable() {
        checkPermissions();
    }

    @Override
    public void showMessageNotAvailable() {
        new AlertDialog.Builder(this).setMessage(R.string.txt_ingo_error_country)
                .setPositiveButton(R.string.action_settings, (dialog, which) -> {
                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    startActivity(intent);
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                    dialog.dismiss();
                    finish();
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        if (getProgressBar().getVisibility() == View.GONE) {
            if (finished) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    private ProgressBar getProgressBar() {
        if (progressBar == null) progressBar = findViewById(R.id.progress_ingo);
        return progressBar;
    }

    @Nullable
    private IngoSdkManager initIngoSdk() {
        boolean canRun = initIngoSdkImpl(getApplicationContext(), jsonSerializer, jsonDeserializer,
                googleAnalyticsHelper, iovationHelper);
        if (canRun) {
            return IngoSdkManager.getInstance();
        } else {
            return null;
        }
    }

    private boolean initIngoSdkImpl(@NonNull Context context, JsonSerializer serializer,
                                    JsonDeserializer deserializer, GoogleAnalyticsHelper analyticsHelper, IovationHelper helper) {
        try {
            return IngoSdkManager.initIngoSdk(context, serializer, deserializer, analyticsHelper, helper,
                    null);
        } catch (Throwable t) {
            t.printStackTrace();
            if (BuildConfig.DEBUG) showError(Strings.nullToEmpty(t.getLocalizedMessage()));
            return false;
        }
    }
}
