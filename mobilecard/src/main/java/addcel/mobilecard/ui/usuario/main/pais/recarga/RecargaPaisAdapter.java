package addcel.mobilecard.ui.usuario.main.pais.recarga;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.utils.MapUtils;

/**
 * ADDCEL on 11/12/17.
 */
public final class RecargaPaisAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private final List<PaisResponse.PaisEntity> paises;
    private final Map<Integer, Integer> flagMap;

    RecargaPaisAdapter(List<PaisResponse.PaisEntity> paises, Map<Integer, Integer> flagMap) {
        this.paises = paises;
        this.flagMap = flagMap;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TYPE_HEADER) {
            return new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_text_header, parent, false));
        } else if (viewType == TYPE_ITEM) {
            return new ItemViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pais, parent, false));
        } else {
            throw new RuntimeException("there is no type that matches the type "
                    + viewType
                    + " + make sure your using types correctly");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) holder).title.setText(R.string.txt_recarga_pais_msg);
            ((HeaderViewHolder) holder).title.setTextAppearance(
                    ((HeaderViewHolder) holder).title.getContext(), R.attr.textAppearanceHeadline6);
        } else if (holder instanceof ItemViewHolder) {
            PaisResponse.PaisEntity pais = getItem(position);
            ((ItemViewHolder) holder).flagImg.setImageResource(
                    MapUtils.getOrDefault(flagMap, pais.getId(), R.drawable.flag_usa));
            ((ItemViewHolder) holder).nameText.setText(pais.getNombrePais());
        }
    }

    @Override
    public int getItemCount() {
        return paises.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public PaisResponse.PaisEntity getItem(int pos) {
        return paises.get(pos - 1);
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        private final ImageView flagImg;
        private final TextView nameText;

        ItemViewHolder(View itemView) {
            super(itemView);
            flagImg = itemView.findViewById(R.id.img_pais_item_flag);
            nameText = itemView.findViewById(R.id.text_pais_item_name);
        }
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView title;

        HeaderViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.text_header);
        }
    }
}
