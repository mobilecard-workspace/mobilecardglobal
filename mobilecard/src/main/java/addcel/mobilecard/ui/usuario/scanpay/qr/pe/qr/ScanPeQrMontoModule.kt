package addcel.mobilecard.ui.usuario.scanpay.qr.pe.qr

import addcel.mobilecard.data.net.qr.VisaQrWalletAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-07-17.
 */
@Module
class ScanPeQrMontoModule(val fragment: ScanPeQrMontoFragment) {

    @PerFragment
    @Provides
    fun provideContainer(): ScanPayActivity {
        return fragment.activity as ScanPayActivity
    }

    @PerFragment
    @Provides
    fun provideData(): ScanPeQrMontoData {
        return fragment.arguments?.getParcelable("data")!!
    }

    @PerFragment
    @Provides
    fun provideApi(r: Retrofit): VisaQrWalletAPI {
        return VisaQrWalletAPI.get(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(api: VisaQrWalletAPI): ScanPeQrMontoPresenter {
        return ScanPeQrMontoPresenterImpl(api, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [ScanPeQrMontoModule::class])
interface ScanPeQrMontoSubcomponent {
    fun inject(fragment: ScanPeQrMontoFragment)
}