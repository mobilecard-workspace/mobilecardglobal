package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.update;

import android.annotation.SuppressLint;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.AccountResponse;
import addcel.mobilecard.data.net.hth.AccountUpdateRequest;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.h2h.H2HAccountInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 10/01/18.
 */
class MxTransferBeneficiarioUpdatePresenter
        implements MxTransferBeneficiarioUpdateContract.Presenter {

    private final H2HAccountInteractor interactor;
    private final AccountEntity account;
    private final MxTransferBeneficiarioUpdateContract.View view;

    MxTransferBeneficiarioUpdatePresenter(H2HAccountInteractor interactor, AccountEntity account,
                                          MxTransferBeneficiarioUpdateContract.View view) {
        this.interactor = interactor;
        this.account = account;
        this.view = view;
    }

    @Override
    public AccountEntity getAccount() {
        return account;
    }

    @SuppressLint("CheckResult")
    @Override
    public void save() {
        view.showProgress();

        AccountUpdateRequest request =
                new AccountUpdateRequest(interactor.getIdUsuario(), account.getId(), view.getNumTelefono(),
                        view.getAlias(), view.getEmail(), StringUtil.getCurrentLanguage());

        interactor.updateAccount(BuildConfig.ADDCEL_APP_ID, request,
                new InteractorCallback<AccountResponse>() {
                    @Override
                    public void onSuccess(@NotNull AccountResponse result) {
                        view.hideProgress();
                        view.onSave(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }
}
