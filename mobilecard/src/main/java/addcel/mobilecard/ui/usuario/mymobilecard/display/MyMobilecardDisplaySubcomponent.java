package addcel.mobilecard.ui.usuario.mymobilecard.display;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 17/11/17.
 */
@PerFragment
@Subcomponent(modules = MyMobilecardDisplayModule.class)
public interface MyMobilecardDisplaySubcomponent {
    void inject(MyMobilecardDisplayFragment fragment);
}
