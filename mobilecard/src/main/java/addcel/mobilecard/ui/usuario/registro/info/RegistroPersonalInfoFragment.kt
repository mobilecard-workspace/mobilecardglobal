package addcel.mobilecard.ui.usuario.registro.info

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.usuarios.model.RegistroResponse
import addcel.mobilecard.ui.usuario.main.MainActivity
import addcel.mobilecard.ui.usuario.registro.RegistroActivity2
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.DeviceUtil
import addcel.mobilecard.validation.ValidationUtils
import addcel.mobilecard.validation.annotation.Celular
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Spinner
import androidx.fragment.app.Fragment
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginResult
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.*
import com.squareup.otto.Bus
import com.squareup.picasso.Picasso
import dagger.Lazy
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.screen_registro_form.*
import kotlinx.android.synthetic.main.view_registro_info_personal.b_registro_info_personal_privacy
import kotlinx.android.synthetic.main.view_registro_info_personal.b_registro_info_personal_terms
import kotlinx.android.synthetic.main.view_registro_info_personal.label_registro_info_personal_cedula
import org.json.JSONObject
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import javax.inject.Inject

/**
 * ADDCEL on 27/09/17.
 */

class RegistroPersonalInfoFragment : Fragment(), addcel.mobilecard.ui.usuario.registro.info.View {
    @NotEmpty(messageResId = R.string.error_nombre)
    @BindView(R.id.til_registro_info_personal_nombre)
    lateinit var nombreTil: TextInputLayout

    @NotEmpty(messageResId = R.string.error_apellido)
    @BindView(R.id.til_registro_info_personal_apellido)
    lateinit var apellidoTil: TextInputLayout

    @Celular(messageResId = R.string.error_celular)
    @BindView(R.id.til_registro_info_personal_celular)
    lateinit var celularTil: TextInputLayout

    @Email(messageResId = R.string.error_email)
    @BindView(R.id.til_registro_create_email)
    lateinit var emailTil: TextInputLayout

    @ConfirmEmail(messageResId = R.string.error_conf_password)
    @BindView(R.id.til_registro_create_email_conf)
    lateinit var emailConfTil: TextInputLayout

    @Password(messageResId = R.string.error_password, scheme = Password.Scheme.ANY, min = 8)
    @BindView(R.id.til_registro_create_password)
    lateinit var passwordTil: TextInputLayout

    @ConfirmPassword(messageResId = R.string.error_conf_password)
    @BindView(R.id.til_registro_create_password_conf)
    lateinit var passwordConfTil: TextInputLayout
    //@BindView(R.id.view_registro_info_personal_pais) TextView paisView;
    //@BindView(R.id.img_registro_info_personal_flag) ImageView flagView;
    // @BindView(R.id.view_registro_info_personal_colombia) LinearLayout colombiaView;
    @BindView(R.id.spinner_registro_info_personal_cedula)
    lateinit var cedulaSpinner: Spinner

    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_registro_info_personal_cedula)
    lateinit var cedulaTil: TextInputLayout

    @Checked(messageResId = R.string.error_terms_conditions)
    @BindView(R.id.check_registro_info_personal_terms)
    lateinit var terminosCheck: CheckBox

    lateinit var infoDialog: androidx.appcompat.app.AlertDialog
    @Inject
    lateinit var containerActivity: RegistroActivity2
    @Inject
    lateinit var validator: Validator
    @Inject
    lateinit var bus: Bus
    @Inject
    lateinit var state: StateSession
    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var presenter: Presenter
    @Inject
    lateinit var picasso: Picasso

    lateinit var unbinder: Unbinder
    private var validatationLauncherId: Int = 0
    lateinit var callbackManager: CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.registroPersonalInfoSubcomponent(
                RegistroPersonalInfoModule(
                        this
                )
        )
                .inject(this)
        bus.register(this)
        callbackManager = CallbackManager.Factory.create()
        session.logout()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_registro_form, container, false)
    }

    override fun setUpFBButton() {
        b_registro_form_facebook.setPermissions(listOf(PROFILE, EMAIL))
        b_registro_form_facebook.fragment = this
        b_registro_form_facebook.registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) { // App coder
                        if (loginResult.accessToken != null) {
                            val request =
                                    GraphRequest.newMeRequest(loginResult.accessToken) { json, _ ->
                                        processJsonResponse(json)

                                    }
                            val b = BundleBuilder().putString(
                                    "fields",
                                    "id, first_name, middle_name, last_name, picture"
                            ).build()
                            request.parameters = b
                            request.executeAsync()
                        }
                    }

                    override fun onCancel() { // App code
                    }

                    override fun onError(exception: FacebookException) {
                        exception.printStackTrace()
                    }
                })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)
        containerActivity.setLocked(false)

        infoDialog = AndroidUtils.createInformationDialog(containerActivity, android.R.string.dialog_alert_title, R.string.info_mymobilecard_1, R.string.ok)

        b_registro_info_personal_privacy.paintFlags =
                b_registro_info_personal_privacy.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        b_registro_info_personal_terms.paintFlags =
                b_registro_info_personal_terms.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        setUpFBButton()

        if (containerActivity.pais.id == 2) {
            label_registro_info_personal_cedula.visibility = View.VISIBLE
            cedulaSpinner.visibility = View.VISIBLE
            cedulaTil.visibility = View.VISIBLE
        } else {
            label_registro_info_personal_cedula.visibility = View.GONE
            cedulaSpinner.visibility = View.GONE
            cedulaTil.visibility = View.GONE
        }

        val email = containerActivity.email
        val phone = containerActivity.phone

        AndroidUtils.setText(AndroidUtils.getEditText(til_registro_create_email), email)
        AndroidUtils.setText(AndroidUtils.getEditText(til_registro_info_personal_celular), phone)
        til_registro_create_email.isEnabled = email.isEmpty()
        til_registro_info_personal_celular.isEnabled = phone.isEmpty()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        containerActivity.setLocked(false)
    }

    override fun onDestroyView() {
        unbinder.unbind()
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        bus.unregister(this)
        super.onDestroy()
    }

    override fun getNombre(): String {
        return AndroidUtils.getText(nombreTil.editText)
    }

    override fun getApellido(): String {
        return AndroidUtils.getText(apellidoTil.editText)
    }

    override fun getCelular(): String {
        return AndroidUtils.getText(celularTil.editText)
    }

    override fun getEmail(): String {
        return AndroidUtils.getText(emailTil.editText)
    }

    override fun getPassword(): String {
        return AndroidUtils.getText(passwordTil.editText)
    }

    override fun areTerminosChecked(): Boolean {
        return terminosCheck.isChecked
    }

    override fun getPais(): Int {
        return containerActivity.pais.id
    }

    override fun getTipoCedula(): Int {
        return cedulaSpinner.selectedItemPosition + 1
    }

    override fun getCedula(): String {
        return AndroidUtils.getText(cedulaTil.editText)
    }

    override fun getImei(): String {
        return if (BuildConfig.DEBUG) {
            ThreadLocalRandom.current().nextLong().toString()
        } else {
            DeviceUtil.getDeviceId(containerActivity)
        }
    }

    override fun getProfile(): String {
        return ""
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPaisSelected(pos: Int) {
        AndroidUtils.setText(
                cedulaTil.editText,
                ""
        ) //colombiaView.setVisibility(pos == 1 ? View.VISIBLE : View.GONE);
    }

    //@OnClick(R.id.b_registro_info_personal_payment)
    override fun clickFormaPago() {
        validatationLauncherId = R.id.b_registro_info_personal_payment
        validator.validate()
    }

    //@OnClick(R.id.b_registro_info_personal_info)
    override fun clickInfo() {
        infoDialog.show()
    }

    @OnClick(R.id.b_registro_info_personal_privacy)
    override fun clickPrivacy() {
        presenter.getPrivacy()
    }

    @OnClick(R.id.b_registro_info_personal_terms)
    override fun clickTerminos() {
        presenter.getTerminos()
    }

    @OnClick(R.id.b_registro_info_personal_continue)
    override fun clickContinue() {
        validatationLauncherId = R.id.b_registro_info_personal_continue
        ValidationUtils.clearViewErrors(
                nombreTil, apellidoTil, celularTil, emailTil, emailConfTil, passwordTil,
                passwordConfTil, cedulaTil
        )
        validator.validate()
    }

    override fun showProgress() {
        containerActivity.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        containerActivity.progressBar.visibility = View.GONE
    }

    override fun showError(resId: Int) {
        if (isVisible) {
            Toasty.error(containerActivity, getString(resId)).show()
            AndroidUtils.findViewById<View>(view, R.id.b_registro_info_personal_continue)
                    .isEnabled =
                    java.lang.Boolean.TRUE
        }
    }

    override fun showError(msg: String) {
        if (isVisible) {
            Toasty.error(containerActivity, msg).show()
            AndroidUtils.findViewById<View>(view, R.id.b_registro_info_personal_continue)
                    .isEnabled =
                    java.lang.Boolean.TRUE
        }
    }

    override fun showSuccess(msg: String) {
        if (isVisible) {
            Toasty.success(containerActivity, msg).show()
            AndroidUtils.findViewById<View>(view, R.id.b_registro_info_personal_continue)
                    .isEnabled =
                    java.lang.Boolean.TRUE
        }
    }

    override fun onRegistroSuccess(response: RegistroResponse) {

    }

    override fun onLoginSuccess(msg: String) {
        state.setCapturedEmail("")
        state.setCapturedPhone("")
        state.setMenuLaunched(false)

        startActivity(
                MainActivity.get(
                        containerActivity,
                        true
                ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        )

        containerActivity.finish()
    }

    override fun finish() {
        Objects.requireNonNull(containerActivity).finish()
    }

    override fun showTerminosDialog(terminos: String) {
        if (isVisible) {
            AlertDialog.Builder(context).setTitle(R.string.txt_terms_and_conditions_title)
                    .setMessage(terminos)
                    .setPositiveButton(android.R.string.yes) { _, _ -> terminosCheck.isChecked = true }
                    .setNegativeButton(android.R.string.no) { _, _ -> terminosCheck.isChecked = false }
                    .show()
        }
    }


    override fun onPrivacy(privacy: String) {
        if (isVisible) {
            AlertDialog.Builder(context).setTitle(R.string.txt_registro_privacy).setMessage(privacy)
                    .setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }.show()
        }
    }

    override fun onValidationSucceeded() {
        presenter.saveUser()
    }

    override fun onValidationFailed(errors: List<ValidationError>) {
        if (isVisible) ValidationUtils.onValidationFailed(containerActivity, errors)
    }

    private fun processJsonResponse(json: JSONObject) {
        til_registro_info_personal_nombre.editText?.setText(json.getString("first_name"))
        til_registro_info_personal_apellido.editText?.setText(json.getString("last_name"))
    }

    companion object {

        private const val EMAIL = "email"
        private const val PROFILE = "public_profile"

        operator fun get(email: String, password: String): RegistroPersonalInfoFragment {
            val bundle = Bundle()
            bundle.putString("email", email)
            bundle.putString("password", password)
            val fragment = RegistroPersonalInfoFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
