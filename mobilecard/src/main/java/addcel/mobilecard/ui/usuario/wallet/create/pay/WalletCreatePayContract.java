package addcel.mobilecard.ui.usuario.wallet.create.pay;

import android.app.DatePickerDialog;

import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;

/**
 * ADDCEL on 10/10/17.
 */
public interface WalletCreatePayContract {
    interface View extends Validator.ValidationListener, DatePickerDialog.OnDateSetListener {

        int REQUEST_CODE = 1;

        String getAlias();

        boolean isPrimary();

        String getCardHolderName();

        String getCard();

        void validateCardPan(CharSequence s);

        String getCvv();

        void clickVigencia();

        String getVigencia();

        TipoTarjetaEntity getTipoTarjeta();

        String getAddress1();

        String getAddress2();

        String getZipCode();

        void clickSave();

        void showProgress();

        void hideProgress();

        void showExpirationDialog();

        void dismissExpirationDialog();

        void showError(int resId);

        void showError(String msg);

        void showSuccess(String msg);

        void updateCards(List<CardEntity> models);

        void onSuccess(CardEntity model);
    }

    interface Presenter {
        String getCardHolderName();

        void create();
    }
}
