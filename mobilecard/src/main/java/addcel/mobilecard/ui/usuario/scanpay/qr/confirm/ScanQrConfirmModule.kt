package addcel.mobilecard.ui.usuario.scanpay.qr.confirm

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.data.net.scanpay.SPApi.Companion.provide
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.token.TokenizerAPI.Companion.provideTokenizerAPI
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import android.os.Bundle
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import java.text.NumberFormat
import java.util.*
import javax.inject.Named

@Module
class ScanQrConfirmModule(private val fragment: ScanQrConfirmFragment) {

    @PerFragment
    @Provides
    fun provideActivity(): ScanPayActivity {
        return fragment.activity as ScanPayActivity
    }

    @PerFragment
    @Provides
    fun provideArgs(): Bundle {
        return fragment.arguments!!
    }

    @PerFragment
    @Provides
    fun providePago(bundle: Bundle): SPPagoEntity {
        return bundle.getParcelable("pago")!!
    }

    @PerFragment
    @Provides
    fun provideCard(bundle: Bundle): CardEntity {
        return bundle.getParcelable("card")!!
    }

    @PerFragment
    @Provides
    fun provideInteractor(retrofit: Retrofit,
                          session: SessionOperations): ScanPayOpenInteractor {
        return ScanPayOpenInteractor(provide(retrofit),
                provideTokenizerAPI(retrofit), session, CompositeDisposable())
    }

    @PerFragment
    @Provides
    @Named("localizedFormat")
    fun provideCurrFormat(interactor: ScanPayOpenInteractor): NumberFormat {
        return when (interactor.getIdPais(SPApi.TIPO_USUARIO)) {
            1 -> NumberFormat.getCurrencyInstance(Locale("es", "MX"))
            2 -> NumberFormat.getCurrencyInstance(Locale("es", "CO"))
            3 -> NumberFormat.getCurrencyInstance(Locale.US)
            4 -> NumberFormat.getCurrencyInstance(Locale("es", "PE"))
            else -> NumberFormat.getCurrencyInstance(Locale.US)
        }
    }

    @PerFragment
    @Provides
    fun providePresenter(
            interactor: ScanPayOpenInteractor, pago: SPPagoEntity, card: CardEntity,
            @Named("localizedFormat") format: NumberFormat): ScanQrConfirmPresenter {
        return ScanQrConfirmPresenterImpl(interactor, card, pago, format, fragment)
    }

}

@PerFragment
@Subcomponent(modules = [ScanQrConfirmModule::class])
interface ScanQrConfirmSubcomponent {
    fun inject(fragment: ScanQrConfirmFragment?)
}
