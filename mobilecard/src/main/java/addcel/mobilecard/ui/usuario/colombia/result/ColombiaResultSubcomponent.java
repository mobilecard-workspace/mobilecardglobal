package addcel.mobilecard.ui.usuario.colombia.result;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 10/29/18.
 */
@PerFragment
@Subcomponent(modules = ColombiaResultModule.class)
public interface ColombiaResultSubcomponent {
    void inject(ColombiaResultFragment colombiaResultFragment);
}
