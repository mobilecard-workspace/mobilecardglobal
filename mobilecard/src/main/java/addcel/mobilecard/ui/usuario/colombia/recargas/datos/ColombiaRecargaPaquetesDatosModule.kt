package addcel.mobilecard.ui.usuario.colombia.recargas.datos

import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.di.scope.PerFragment
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 9/21/19.
 */
@Module
class ColombiaRecargaPaquetesDatosModule(
        val fragment: ColombiaRecargaPaquetesDatosFragment
) {

    @PerFragment
    @Provides
    fun provideModel(): ColombiaRecargaPaquetesDatosModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideAdapter(
            picasso: Picasso,
            model: ColombiaRecargaPaquetesDatosModel
    ): ColombiaRecargaPaquetesDatosAdapter {
        return ColombiaRecargaPaquetesDatosAdapter(picasso, model.operador)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            api: CatalogoAPI
    ): ColombiaRecargaPaquetesDatosPresenter {
        return ColombiaRecargaPaqueteDatosPresenterImpl(api, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [ColombiaRecargaPaquetesDatosModule::class])
interface ColombiaRecargaPaqueteDatosSucomponent {
    fun inject(fragment: ColombiaRecargaPaquetesDatosFragment)
}