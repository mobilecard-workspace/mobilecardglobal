package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.telepeaje.TagsApi;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.telepeaje.select.PeajeSelectInteractor;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.MontoAdapter;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

/**
 * ADDCEL on 09/08/17.
 */
@Module
public final class PeajeNewModule {
    private final PeajeNewFragment fragment;

    PeajeNewModule(PeajeNewFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    @Named("colorLogoMap")
    Map<Integer, Integer> provideRecargaLabelMap() {
        final Map<Integer, Integer> map = new LinkedHashMap<>();
        map.put(1, R.drawable.logo_telcel);
        map.put(2, R.drawable.logo_movistar);
        map.put(3, R.drawable.logo_iusacell);
        map.put(4, R.drawable.logo_nextel);
        map.put(5, R.drawable.logo_unefon);
        map.put(6, R.drawable.logo_id);
        map.put(7, R.drawable.logo_todito);
        map.put(8, R.drawable.logo_pase);
        return map;
    }

    @PerFragment
    @Provides
    PagoContainerActivity provideActivity() {
        return (PagoContainerActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    CatalogoResponse.RecargaEntity provideServicio() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelable("servicio");
    }

    @PerFragment
    @Provides
    List<CatalogoResponse.MontoEntity> montos() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelableArrayList("montos");
    }

    @PerFragment
    @Provides
    boolean fav() {
        return Preconditions.checkNotNull(fragment.getArguments()).getBoolean("fav");
    }

    @PerFragment
    @Provides
    TagsApi provideApi(Retrofit retrofit) {
        return retrofit.create(TagsApi.class);
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    PeajeNewAdapter provideTagAdapter() {
        return new PeajeNewAdapter(Lists.newArrayList());
    }

    @PerFragment
    @Provides
    MontoAdapter provideAdapter(List<CatalogoResponse.MontoEntity> montos,
                                NumberFormat numberFormat) {
        return new MontoAdapter(montos, numberFormat);
    }

    @PerFragment
    @Provides
    Map<Integer, Integer> provideProveedorLookup() {
        Map<Integer, Integer> lookup = Maps.newHashMap();
        lookup.put(6, 1);
        lookup.put(8, 4);
        return lookup;
    }

    @PerFragment
    @Provides
    PeajeSelectInteractor provideInteractor(TagsApi api, FavoritoDaoRx dao,
                                            SessionOperations session, Map<Integer, Integer> lookup) {
        return new PeajeSelectInteractor(api, dao, session, lookup, new CompositeDisposable());
    }

    @PerFragment
    @Provides
    PeajeContract.Presenter providePresenter(PeajeSelectInteractor interactor,
                                             NumberFormat currFormat) {
        return new PeajePresenter(interactor, currFormat, fragment);
    }

    @PerFragment
    @Provides
    DetachableClickListener provideCancelListener() {
        return DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    @PerFragment
    @Provides
    AlertDialog provideAliasDialog(PagoContainerActivity activity, DetachableClickListener cancelListener) {
        final View cardView = View.inflate(activity, R.layout.view_add_tag, null);

        return new AlertDialog.Builder(activity).setTitle(R.string.txt_peaje_tag_guardar)
                .setView(cardView)
                .setNegativeButton(android.R.string.cancel, cancelListener)
                .setPositiveButton(android.R.string.yes, null)
                .create();
    }
}
