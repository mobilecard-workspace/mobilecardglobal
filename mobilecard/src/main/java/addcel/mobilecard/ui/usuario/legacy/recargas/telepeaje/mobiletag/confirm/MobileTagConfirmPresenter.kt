package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.pago.BPApi
import addcel.mobilecard.data.net.pago.PagoEntity
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 31/10/19.
 */
interface MobileTagConfirmPresenter {

    companion object {

        private lateinit var CURR_FORMAT: NumberFormat

        fun formatAmount(amount: Double, idPais: Int): String {
            if (!::CURR_FORMAT.isInitialized) {
                CURR_FORMAT = when (idPais) {
                    PaisResponse.PaisEntity.MX -> NumberFormat.getCurrencyInstance(
                            Locale("es", "MX")
                    )
                    PaisResponse.PaisEntity.CO -> NumberFormat.getCurrencyInstance(
                            Locale("es", "CO")
                    )
                    PaisResponse.PaisEntity.US -> NumberFormat.getCurrencyInstance(
                            Locale.US
                    )
                    PaisResponse.PaisEntity.PE -> NumberFormat.getCurrencyInstance(
                            Locale("es", "PE")
                    )
                    else -> NumberFormat.getCurrencyInstance(Locale.US)
                }
            }
            return CURR_FORMAT.format(amount)
        }
    }

    fun fetchUsuario(): Usuario

    fun fetchLocation(): McLocationData

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun checkWhitelist()

    fun getToken(auth: String, profile: String)

    fun pagar(tokenEntity: TokenEntity, request: PagoEntity)
}

class MobileTagConfirmPresenterImpl(
        val api: BPApi,
        val token: TokenizerAPI,
        val session: SessionOperations,
        val compositeDisposable: CompositeDisposable,
        val view: MobileTagConfirmView
) : MobileTagConfirmPresenter {

    override fun fetchUsuario(): Usuario {
        return session.usuario
    }

    override fun fetchLocation(): McLocationData {
        return session.minimalLocationData
    }

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun checkWhitelist() {
        view.disablePurchaseButton()
        view.showProgress()
        val whiteListDisp = token.isOnWhiteList(
                BuildConfig.ADDCEL_APP_ID,
                fetchUsuario().idPais,
                StringUtil.getCurrentLanguage(),
                fetchUsuario().ideUsuario
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.enablePurchaseButton()
                    view.hideProgress()
                    if (it.code == 0) view.onWhiteList() else view.notOnWhiteList()
                }, {
                    view.enablePurchaseButton()
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(whiteListDisp)
    }

    override fun getToken(auth: String, profile: String) {
        view.disablePurchaseButton()
        view.showProgress()
        val tokenDisp = token.getToken(
                auth,
                profile,
                BuildConfig.ADDCEL_APP_ID,
                fetchUsuario().idPais,
                StringUtil.getCurrentLanguage()
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.code == 0) view.onToken(it)
                    else {
                        view.enablePurchaseButton()
                        view.showError(it.message)
                    }
                }, {
                    view.enablePurchaseButton()
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(tokenDisp)

    }

    override fun pagar(tokenEntity: TokenEntity, request: PagoEntity) {
        view.showProgress()

        val pagoDisp = api.executePagoBP(
                tokenEntity.token,
                BuildConfig.ADDCEL_APP_ID,
                fetchUsuario().idPais,
                StringUtil.getCurrentLanguage(),
                tokenEntity.accountId,
                request
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    view.onPagoSuccess(it)
                }, {
                    view.hideProgress()
                    view.enablePurchaseButton()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })


        addToDisposables(pagoDisp)
    }

}