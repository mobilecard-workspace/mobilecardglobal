package addcel.mobilecard.ui.usuario.legacy.servicios.directo;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 14/12/16.
 */
@PerFragment
@Subcomponent(modules = ServicioModule.class)
public interface ServicioSubcomponent {
    void inject(ServicioFragment fragment);
}
