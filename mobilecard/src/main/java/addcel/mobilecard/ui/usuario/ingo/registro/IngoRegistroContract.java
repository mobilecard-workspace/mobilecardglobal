package addcel.mobilecard.ui.usuario.ingo.registro;

import com.mobsandgeeks.saripaar.Validator;

import org.threeten.bp.LocalDateTime;

import java.util.List;

import addcel.mobilecard.data.net.ingo.model.IngoEstadoResponse;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;

/**
 * ADDCEL on 18/07/18.
 */
public interface IngoRegistroContract {

    String DOB_FORMAT = "yyyy-MM-dd";

    interface View extends Validator.ValidationListener {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(IngoUserData data);

        void setEstados(List<IngoEstadoResponse.EstadoEntity> estados);

        LocalDateTime getDob();

        void setDob(LocalDateTime dobTime);

        void clickDobButton();

        void clickRegistro();

        String getSsn();

        String getDireccion();

        String getCiudad();

        String getZipCode();

        IngoEstadoResponse.EstadoEntity getEstado();

        String getImei();
    }

    interface Presenter {
        String getEmail();

        String getNombres();

        String getApellidos();

        String getPhone();

        String getFormattedDateTime(LocalDateTime dobTime);

        void getEstados();

        void enroll();
    }
}
