package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.categorias

import addcel.mobilecard.Mobilecard
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Categoria
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search.MmServicioSearchFragment
import android.widget.Button
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.screen_producto_list.*
import timber.log.Timber
import javax.inject.Inject

/**
 * ADDCEL on 2019-12-19.
 */

interface MmCategoriasView : ScreenView {
    fun launchCategorias()
    fun configCategoriaList(view: View)
    fun setCategorias(categorias: List<Categoria>)
    fun clickCategoria(pos: Int)
    fun showRetry()
    fun hideRetry()
    fun clickRetry()
}

class MmCategoriasFragment : Fragment(), MmCategoriasView {


    companion object {
        fun get(): MmCategoriasFragment {
            return MmCategoriasFragment()
        }
    }

    @Inject
    lateinit var cActivity: ContainerActivity
    @Inject
    lateinit var presenter: MmCategoriasPresenter
    @Inject
    lateinit var adapter: MmCategoriasAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.mmCategoriasSubcomponent(MmCategoriasModule(this)).inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_producto_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cActivity.retry?.findViewById<Button>(R.id.b_retry)?.setOnClickListener {
            clickRetry()
        }
        configCategoriaList(view)
        launchCategorias()
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_productos)
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun launchCategorias() {
        if (adapter.itemCount == 0)
            presenter.getCategorias()
    }

    override fun configCategoriaList(view: View) {
        recycler_productos.adapter = adapter
        recycler_productos.layoutManager = GridLayoutManager(view.context, 2)
        recycler_productos.setHasFixedSize(true)
        ItemClickSupport.addTo(recycler_productos).setOnItemClickListener { _, position, _ ->
            clickCategoria(position)
        }
    }

    override fun setCategorias(categorias: List<Categoria>) {
        adapter.update(categorias)
    }

    override fun clickCategoria(pos: Int) {
        Timber.d("CATEGORIA POS: %d", pos)

        val cat = adapter.getItem(pos)

        cActivity.fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, MmServicioSearchFragment.get(cat))
            hide(this@MmCategoriasFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    override fun showRetry() {
        cActivity.showRetry()
    }

    override fun hideRetry() {
        cActivity.hideRetry()
    }

    override fun clickRetry() {
        launchCategorias()
    }

    override fun showSuccess(msg: String) {
        cActivity.showSuccess(msg)
    }

    override fun showError(msg: String) {
        cActivity.showError(msg)
    }

    override fun hideProgress() {
        cActivity.hideProgress()
    }

    override fun showProgress() {
        cActivity.showProgress()
    }

}