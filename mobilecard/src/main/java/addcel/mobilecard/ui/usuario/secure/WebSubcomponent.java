package addcel.mobilecard.ui.usuario.secure;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 15/12/16.
 */
@PerActivity
@Subcomponent(modules = WebModule.class)
public interface WebSubcomponent {
    void inject(WebActivity activity);
}
