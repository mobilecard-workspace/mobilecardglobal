package addcel.mobilecard.ui.usuario.viamericas.sender


import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.SenderModel
import addcel.mobilecard.data.net.viamericas.model.VmCityModel
import addcel.mobilecard.data.net.viamericas.model.VmStateModel
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * ADDCEL on 27/03/17.
 */


interface ViamericasSenderPresenter {

    fun addToDisposables(d: Disposable)

    fun clearDisposables()

    fun fetchUserEmail(): String

    fun fetchUserName(): String

    fun fetchUserLastName(): String

    fun fetchUserMaterno(): String

    fun fetchUserPhone(): String

    fun fetchUserAddress(): String

    fun getEstados(withUI: Boolean)

    fun getCities(state: VmStateModel)

    fun getZipCode(city: VmCityModel)

    fun getCountries()

    fun addSender(address: String, state: VmStateModel, ciudad: VmCityModel, zipCode: String)

    fun addSender(sender: SenderModel)
}

class ViamericasSenderPresenterImpl(
        private val service: ViamericasService, private val session: SessionOperations,
        private val disposables: CompositeDisposable,
        private val view: VmSenderView
) : ViamericasSenderPresenter {

    override fun addToDisposables(d: Disposable) {
        disposables.add(disposables)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun fetchUserEmail(): String {
        return session.usuario.eMail
    }

    override fun fetchUserName(): String {
        return session.usuario.usrNombre
    }

    override fun fetchUserLastName(): String {
        return session.usuario.usrApellido
    }

    override fun fetchUserMaterno(): String {
        return session.usuario.usrMaterno
    }

    override fun fetchUserPhone(): String {
        return session.usuario.usrTelefono
    }

    override fun fetchUserAddress(): String {
        return session.usuario.usrDireccion
    }

    override fun getEstados(withUI: Boolean) {
        view.showProgress()
        val json = "{\"idCountry\":\"USA\", \"operation\":\"RS\"}"
        Timber.d(json)
        val eDisp =
                service.getStates(
                        ViamericasService.AUTH,
                        BuildConfig.ADDCEL_APP_ID,
                        StringUtil.getCurrentLanguage(),
                        json
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { r ->
                                    view.hideProgress()
                                    if (r.idError == 0) {
                                        view.postStates(r.states)
                                    } else {
                                        view.showError(r.mensajeError)
                                    }
                                },
                                { t ->
                                    view.hideProgress()
                                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(t))
                                })
        addToDisposables(eDisp)
    }

    override
    fun getCities(state: VmStateModel) {
        view.showProgress()
        val json = ("{\"idCountry\": \"USA\", \"idState\": \""
                + state.idState
                + "\",\"operation\":\"RS\"}")
        Timber.d(json)

        val cDisp = service.getCitiesByState(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                json
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ r ->
                    view.hideProgress()
                    if (r.idError == 0) {
                        view.postCities(r.cities)
                    } else {
                        view.showError(r.mensajeError)
                    }
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })

        addToDisposables(cDisp)
    }

    override
    fun getZipCode(city: VmCityModel) {
        view.showProgress()
        val request = "{\"idCity\":\"" + city.idCity + "\"}"
        Timber.d(request)
        val zDisp =
                service.getZipCode(
                        ViamericasService.AUTH,
                        BuildConfig.ADDCEL_APP_ID,
                        StringUtil.getCurrentLanguage(),
                        request
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            view.hideProgress()
                            if (it.idError == 0) {
                                view.postZipCodes(
                                        it.zipCodeList
                                ) //MODIFICAR CUANDO CAMBIE RESPONSE A ARREGLO
                            } else {
                                view.showError(it.mensajeError)
                            }
                        }, {
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })
        addToDisposables(zDisp)
    }

    override fun getCountries() {
        view.showProgress()
        val cDisp = service.getCountries(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                "{\"operation\":\"NO\"}"
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) {
                        view.postCountries(it.countries)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(cDisp)
    }

    override
    fun addSender(address: String, state: VmStateModel, ciudad: VmCityModel, zipCode: String) {
        view.showProgress()
        val sender = SenderModel.Builder().setIdUsuario(session.usuario.ideUsuario)
                .setFirstName(session.usuario.usrNombre)
                .setLastName(session.usuario.usrApellido)
                .setPhone(session.usuario.usrTelefono)
                .setAddress(address)
                .setIdCountry("USA")
                .setIdState(state.idState)
                .setIdCity(ciudad.idCity)
                .setZipCode(zipCode)
                .build()

        val request = JsonUtil.toJson(sender)
        val sDisp = service.createSender(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                request
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) {
                        view.postSender(it)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(sDisp)
    }

    override fun addSender(sender: SenderModel) {
        view.showProgress()
        val request = JsonUtil.toJson(sender)
        val sDisp = service.createSender(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                request
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) {
                        view.postSender(it)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(sDisp)
    }
}
