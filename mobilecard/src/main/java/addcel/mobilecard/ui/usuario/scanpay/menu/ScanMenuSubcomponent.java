package addcel.mobilecard.ui.usuario.scanpay.menu;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 21/08/18.
 */
@PerFragment
@Subcomponent(modules = ScanMenuModule.class)
public interface ScanMenuSubcomponent {
    void inject(ScanMenuFragment fragment);
}
