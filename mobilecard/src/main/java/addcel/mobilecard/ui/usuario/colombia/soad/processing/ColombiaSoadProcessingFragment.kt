package addcel.mobilecard.ui.usuario.colombia.soad.processing

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.colombia.SoatConsultaResponse
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.colombia.result.ColombiaResultFragment
import addcel.mobilecard.ui.usuario.colombia.soad.ColombiaSoadActivity
import addcel.mobilecard.utils.StringUtil
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_soat_confirm.*
import mx.mobilecard.crypto.AddcelCrypto
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 10/3/19.
 */

@Parcelize
data class ColombiaSoadProcessingModel(
        val info: SoatConsultaResponse, val idUser: Long,
        val jumioRef: String
) : Parcelable

interface ColombiaSoadProcessingView : ScreenView, Authenticable {

    fun loadInfo()

    fun loadCard()

    fun launchPago()

    fun onPago(receipt: SPReceiptEntity)
}

class ColombiaSoadProcessingFragment : Fragment(), ColombiaSoadProcessingView {


    companion object {
        fun get(args: Bundle): ColombiaSoadProcessingFragment {
            val frag = ColombiaSoadProcessingFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var picasso: Picasso
    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var presenter: ColombiaSoadProcessingPresenter

    lateinit var card: CardEntity
    lateinit var model: ColombiaSoadProcessingModel
    private lateinit var oldColor: ColorStateList

    private val colFormat = NumberFormat.getCurrencyInstance(Locale("es", "CO"))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get().netComponent.colombiaSoadProcessingSubcomponent(
                ColombiaSoadProcessingModule(this)
        ).inject(this)

        card = arguments?.getParcelable("card")!!
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_soat_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        oldColor = view_soad_nombre.textColors
        loadInfo()
        loadCard()
        b_soat_info_continuar.setOnClickListener {
            if (activity != null) {
                presenter.isOnWhiteList(model.idUser, PaisResponse.PaisEntity.CO)
            }
        }
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun loadInfo() {
        view_soad_nombre.text = getTwoColorText(
                "Nombre\n",
                session.usuario.usrNombre + " " + session.usuario.usrApellido + " " + session.usuario.usrMaterno
        )
        view_soad_marca.text = getTwoColorText("Marca\n", model.info.marca ?: "")
        view_soad_cc.text = getTwoColorText("CC\n", session.usuario.cedula)
        view_soad_linea.text = getTwoColorText("Línea\n", model.info.linea ?: "")
        view_soad_telefono.text = getTwoColorText("Teléfono\n", session.usuario.usrTelefono)
        view_soad_modelo.text = getTwoColorText("Modelo\n", model.info.modelo ?: "")
        view_soad_email.text = getTwoColorText("Email\n", session.usuario.eMail)
        view_soad_placa.text = getTwoColorText("Placa\n", model.info.placa ?: "")
        view_soad_direccion.text =
                getTwoColorText("Dirección de Residencia\n", session.usuario.usrDireccion)
        view_soad_categoria.text = getTwoColorText("Categoría\n", model.info.categoria ?: "")
        view_soad_ciudad.text = getTwoColorText("Ciudad\n", session.usuario.usrCiudad)
        view_soad_seguro.text =
                getTwoColorText("Seguro SOAT ", colFormat.format(model.info.seguroSoat))
        view_soad_vigencia_desde.text = getString(R.string.txt_col_desde, model.info.vigenciaIni)
        view_soad_vigencia_hasta.text = getString(R.string.txt_col_hasta, model.info.vigenciaFin)
        view_soad_total.text = getTwoColorText("Total ", colFormat.format(model.info.total))
    }

    override fun loadCard() {
        picasso.load(card.imgShort).placeholder(R.drawable.bg_visa).placeholder(R.drawable.bg_visa)
                .fit().into(bg_soad_info_card, object : Callback {
                    override fun onSuccess() {
                        pan_soad_info_card.text =
                                StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }

                    override fun onError(e: Exception) {
                        pan_soad_info_card.text =
                                StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }
                })
    }

    override fun launchPago() {
        b_soat_info_continuar.isEnabled = false

        val request = model.info.copy(
                idUSuario = model.idUser, idCard = card.idTarjeta,
                idApp = BuildConfig.ADDCEL_APP_ID, idioma = StringUtil.getCurrentLanguage()
        )

        presenter.pagoPoliza(request)
    }

    override fun onPago(receipt: SPReceiptEntity) {
        b_soat_info_continuar.isEnabled = true
        (activity as ContainerActivity).fragmentManagerLazy.commit {
            addToBackStack(null)
            add(R.id.frame_colombia_soad, ColombiaResultFragment.get(receipt))
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    override fun showProgress() {
        (activity as ColombiaSoadActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ColombiaSoadActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ColombiaSoadActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ColombiaSoadActivity).showSuccess(msg)
    }

    override fun onWhiteList() {
        launchPago()
    }

    override fun notOnWhiteList() {
        b_soat_info_continuar.isEnabled = false

        showError("No está autorizado para realizar esta operacion. Contacte a soporte.")
    }


    private fun getTwoColorText(first: String, second: String): CharSequence {
        val firstPart = SpannableString(first)
        val lastPart = SpannableString(second)

        firstPart.setSpan(
                ForegroundColorSpan(
                        ContextCompat.getColor(activity!!.applicationContext, R.color.colorAccent)
                ), 0,
                firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                ForegroundColorSpan(oldColor.defaultColor), 0, lastPart.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                StyleSpan(Typeface.BOLD), 0, lastPart.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )


        return TextUtils.concat(firstPart, lastPart)
    }
}