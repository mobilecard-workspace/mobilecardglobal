package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.McRegexPatterns;
import addcel.mobilecard.data.net.hth.AccountRequest;
import addcel.mobilecard.data.net.hth.AccountResponse;
import addcel.mobilecard.data.net.hth.BankEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.h2h.H2HAccountInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 09/11/17.
 */
class MxTransferBeneficiarioAddPresenter implements MxTransferBeneficiarioAddContract.Presenter {
    private final H2HAccountInteractor interactor;
    private final MxTransferBeneficiarioAddContract.View view;

    MxTransferBeneficiarioAddPresenter(H2HAccountInteractor interactor,
                                       MxTransferBeneficiarioAddContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getBankCodes() {
        view.showProgress();

        interactor.fetchBanks(BuildConfig.ADDCEL_APP_ID, new InteractorCallback<List<BankEntity>>() {
            @Override
            public void onSuccess(@NotNull List<BankEntity> result) {
                view.hideRetry();
                view.hideProgress();
                view.setBankCodes(result);
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showRetry();
                view.showError(error);
            }
        });
    }

    @Override
    public String generateRfc() {
        String paterno =
                view.getApellidoPaterno().charAt(0) + fetchFirstVowel(view.getApellidoPaterno());
        String materno = view.getApellidoMaterno().substring(0, 1);
        String nombre = view.getNombre().substring(0, 1);
        return StringUtil.removeAcentos(
                String.format("%s%s%s000000MC0", paterno, materno, nombre).toUpperCase());
    }

    private String fetchFirstVowel(@NonNull String word) {
        Pattern pattern =
                Pattern.compile("[\\w]([aeiouâãäåæçèéêëìíîïðñòóôõøùúûü])", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(word);
        if (matcher.find()) {
            return String.valueOf(matcher.group(1));
        }
        return "";
    }

    @Override
    public int getBanorteSelectedPos(BankEntity bank) {
        if (bank == null) return 1;
        return (TextUtils.equals(bank.getClave(), BANORTE_KEY) || TextUtils.equals(bank.getClave(),
                IXE_KEY)) ? 0 : 1;
    }

    @SuppressLint("CheckResult")
    @Override
    public void save() {
        view.showProgress();

        AccountRequest request = new AccountRequest(view.getCuentaOrClable(), getActType(),
                StringUtil.removeAcentosMultiple(view.getAlias()), view.getBank().getClave(),
                getHolderName(), view.getEmail(), getHolderName(), interactor.getIdUsuario(),
                StringUtil.getCurrentLanguage(), view.getNumTelefono(), generateRfc());

        interactor.addAccount(BuildConfig.ADDCEL_APP_ID, request,
                new InteractorCallback<AccountResponse>() {
                    @Override
                    public void onSuccess(@NotNull AccountResponse result) {
                        view.hideProgress();
                        view.onSave(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public boolean isCLABE() {
        return Pattern.matches(McRegexPatterns.TRANSFER_CLABE, view.getCuentaOrClable());
    }

    @Override
    public boolean isAccount() {
        return Pattern.matches(McRegexPatterns.TRANSFER_ACCOUNT, view.getCuentaOrClable());
    }

    @Override
    public boolean validateAccountOrClabe() {
        int pos = view.getTipoCuentaSelectedPos();
        String error = view.getErrorFromSelectionPos(pos);
        boolean validation = (pos == 0 && isAccount() || pos == 1 && isCLABE());
        if (!validation) view.showError(error);
        return validation;
    }

    private String getActType() {
        return view.getTipoCuentaSelectedPos() == 0 ? ACT : CLABE;
    }

    private String getHolderName() {
        return String.format("%s %s %s", StringUtil.removeAcentosMultiple(view.getNombre()),
                StringUtil.removeAcentosMultiple(view.getApellidoPaterno()),
                StringUtil.removeAcentosMultiple(view.getApellidoMaterno()));
    }
}
