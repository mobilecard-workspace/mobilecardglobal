package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;

import java.text.NumberFormat;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.H2HApi;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.h2h.H2HAccountInteractor;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

/**
 * ADDCEL on 08/11/17.
 */
@Module
public final class MxTransferBeneficiarioSelectModule {
    private final MxTransferBeneficiarioSelectFragment fragment;

    MxTransferBeneficiarioSelectModule(MxTransferBeneficiarioSelectFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MxTransferActivity provideActivity() {
        return (MxTransferActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    ArrayAdapter<AccountEntity> provideAdapter(MxTransferActivity activity) {
        ArrayAdapter<AccountEntity> adapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, activity.getCuentas());
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter.setNotifyOnChange(true);
        return adapter;
    }

    @PerFragment
    @Provides
    H2HApi provideAPI(Retrofit retrofit) {
        return H2HApi.Companion.get(retrofit);
    }

    @PerFragment
    @Provides
    H2HAccountInteractor provideAccountINteractor(H2HApi api,
                                                  SessionOperations session) {
        return new H2HAccountInteractor(api, session, new CompositeDisposable());
    }

    @PerFragment
    @Provides
    MxTransferBeneficiarioSelectContract.Presenter providePresenter(
            H2HAccountInteractor interactor, NumberFormat numberFormat) {
        return new MxTransferBeneficiarioSelectPresenter(interactor, numberFormat, fragment);
    }


    @PerFragment
    @Provides
    @Named("okListener")
    DetachableClickListener provideWrapperOk(MxTransferBeneficiarioSelectContract.Presenter presenter) {
        return DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                presenter.deleteBeneficiario();
            }
        });
    }


    @PerFragment
    @Provides
    @Named("cancelListener")
    DetachableClickListener provideWrapperCancel() {
        return DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
    }

    @PerFragment
    @Provides
    AlertDialog provideDeleteDialog(MxTransferActivity activity, @Named("okListener") DetachableClickListener okListener,
                                    @Named("cancelListener") DetachableClickListener cancelListenet) {
        return new AlertDialog.Builder(activity).setTitle(R.string.nav_transfer_mx)
                .setPositiveButton(R.string.txt_wallet_eliminar, okListener)
                .setNegativeButton(android.R.string.cancel, cancelListenet)
                .create();
    }
}
