package addcel.mobilecard.ui.usuario.main;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 28/10/16.
 */
@PerActivity
@Subcomponent(modules = MainModule.class)
public interface MainSubcomponent {
    void inject(MainActivity activity);
}
