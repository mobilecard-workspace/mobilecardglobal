package addcel.mobilecard.ui.usuario.ingo.registro;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import org.threeten.bp.LocalDateTime;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.ingo.model.IngoEstadoResponse;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.ui.usuario.ingo.IngoActivity;
import addcel.mobilecard.ui.usuario.wallet.select.add.WalletSelectAddFragment;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.Lazy;

/**
 * ADDCEL on 16/07/18.
 */
public final class IngoRegistroFragment extends Fragment implements IngoRegistroContract.View {
    @BindView(R.id.til_ingo_registro_email)
    TextInputLayout emailTil;

    @BindView(R.id.til_ingo_registro_nombre)
    TextInputLayout nombreTil;

    @BindView(R.id.til_ingo_registro_apellido)
    TextInputLayout apellidosTil;

    @BindView(R.id.til_ingo_registro_telefono)
    TextInputLayout celularTil;

    @Pattern(regex = "^(?!219099999|078051120)(?!666|000|9\\d{2})\\d{3}(?!00)\\d{2}(?!0{4})\\d{4}$", messageResId = R.string.error_ssn)
    @BindView(R.id.til_ingo_registro_ssn)
    TextInputLayout ssnTil;

    @NotEmpty(messageResId = R.string.error_default)
    @BindView(R.id.til_ingo_registro_dob)
    TextInputLayout dobTil;

    @BindView(R.id.text_ingo_registro_dob)
    TextInputEditText dobText;

    @NotEmpty(messageResId = R.string.error_default)
    @BindView(R.id.til_ingo_registro_direccion)
    TextInputLayout direccionTil;

    @NotEmpty(messageResId = R.string.error_default)
    @BindView(R.id.til_ingo_registro_ciudad)
    TextInputLayout ciudadTil;

    @NotEmpty(messageResId = R.string.error_default)
    @BindView(R.id.til_ingo_registro_zip)
    TextInputLayout zipTil;

    @BindView(R.id.spinner_ingo_registro_estado)
    Spinner estadosSpinner;

    @Inject
    IngoActivity activity;
    @Inject
    IngoRegistroContract.Presenter presenter;
    @Inject
    Lazy<DatePickerDialog> dobDialog;
    @Inject
    ArrayAdapter<IngoEstadoResponse.EstadoEntity> adapter;
    @Inject
    Validator validator;

    private Unbinder unbinder;

    public IngoRegistroFragment() {
    }

    public static synchronized IngoRegistroFragment get() {
        return new IngoRegistroFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .ingoRegistroSubcomponent(new IngoRegistroModule(this))
                .inject(this);
        presenter.getEstados();
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_ingo_registro, container, false);
        unbinder = ButterKnife.bind(this, view);
        estadosSpinner.setAdapter(adapter);
        dobText.setOnFocusChangeListener((view1, b) -> {
            if (isVisible()) {
                if (b) {
                    dobDialog.get().show();
                } else {
                    dobDialog.get().dismiss();
                }
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setFinished(false);
        activity.getToolbarFromActivity().setTitle(R.string.txt_ingo_registro_title);
        Objects.requireNonNull(emailTil.getEditText()).setText(presenter.getEmail());
        emailTil.setEnabled(false);
        Objects.requireNonNull(nombreTil.getEditText()).setText(presenter.getNombres());
        nombreTil.setEnabled(false);
        Objects.requireNonNull(apellidosTil.getEditText()).setText(presenter.getApellidos());
        apellidosTil.setEnabled(false);
        Objects.requireNonNull(celularTil.getEditText()).setText(presenter.getPhone());
        celularTil.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            dobDialog.get().setOnDateSetListener(null);
        }
        adapter = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {

        presenter = null;
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(IngoUserData data) {
        activity.showSuccess(data.getErrorMessage());
        Bundle bundle = new Bundle();
        bundle.putParcelable("ingoData", data);
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_ingo, WalletSelectAddFragment.get(WalletSelectAddFragment.INGO, bundle))
                .hide(this)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @Override
    public void setEstados(List<IngoEstadoResponse.EstadoEntity> estados) {
        if (adapter.getCount() == 0) adapter.addAll(estados);
    }

    @Override
    public LocalDateTime getDob() {
        return (LocalDateTime) dobTil.getTag();
    }

    @Override
    public void setDob(LocalDateTime dobTime) {
        if (isVisible()) {
            dobTil.setError(null);
            dobText.setText(presenter.getFormattedDateTime(dobTime));
            dobTil.setTag(dobTime);
        }
    }

    @OnClick({R.id.b_ingo_registro_add_dob, R.id.text_ingo_registro_dob})
    @Override
    public void clickDobButton() {
        if (isVisible() && !dobDialog.get().isShowing()) dobDialog.get().show();
    }

    @OnClick(R.id.b_ingo_registro_registrar)
    @Override
    public void clickRegistro() {
        ValidationUtils.clearViewErrors(ssnTil, dobTil, direccionTil, ciudadTil, zipTil);
        validator.validate();
    }

    @Override
    public String getSsn() {
        return Strings.nullToEmpty(Objects.requireNonNull(ssnTil.getEditText()).getText().toString());
    }

    @Override
    public String getDireccion() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(direccionTil.getEditText()).getText().toString());
    }

    @Override
    public String getCiudad() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(ciudadTil.getEditText()).getText().toString());
    }

    @Override
    public String getZipCode() {
        return Strings.nullToEmpty(Objects.requireNonNull(zipTil.getEditText()).getText().toString());
    }

    @Override
    public IngoEstadoResponse.EstadoEntity getEstado() {
        return (IngoEstadoResponse.EstadoEntity) Objects.requireNonNull(
                estadosSpinner.getSelectedItem());
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(activity);
    }

    @Override
    public void onValidationSucceeded() {
        ValidationUtils.clearViewErrors(ssnTil, dobTil, direccionTil, ciudadTil, zipTil);
        presenter.enroll();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }
}
