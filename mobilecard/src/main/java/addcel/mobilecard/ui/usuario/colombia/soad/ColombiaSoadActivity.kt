package addcel.mobilecard.ui.usuario.colombia.soad

import addcel.mobilecard.R
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.usuario.colombia.soad.placa.ColombiaSoadPlacaFragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_colombia_soad.*

class ColombiaSoadActivity : ContainerActivity() {

    companion object {
        fun get(context: Context): Intent {
            return Intent(context, ColombiaSoadActivity::class.java)
        }
    }

    override fun showProgress() {
        if (progress_colombia_soad.visibility == View.GONE) progress_colombia_soad.visibility =
                View.VISIBLE
    }

    override fun hideRetry() {

    }

    override fun hideProgress() {
        if (progress_colombia_soad.visibility == View.VISIBLE) progress_colombia_soad.visibility =
                View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_colombia_soad.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_colombia_soad.title = title
    }

    override fun showRetry() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_colombia_soad)

        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        fragmentManagerLazy.commit {
            add(R.id.frame_colombia_soad, ColombiaSoadPlacaFragment())
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    override fun getRetry(): View? {
        return null
    }
}
