package addcel.mobilecard.ui.usuario.main.pais.recarga;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;

/**
 * ADDCEL on 11/12/17.
 */
public interface RecargaPaisContract {
    interface View {

        void showProgress();

        void hideProgress();

        void onItemClicked(int pos);

        void onCategorias(List<CatalogoResponse.RecargaEntity> categorias,
                          PaisResponse.PaisEntity paisModel);

        void onError(String msg);
    }

    interface Presenter {

        void clearDisposables();

        void getCategorias(PaisResponse.PaisEntity model);
    }
}
