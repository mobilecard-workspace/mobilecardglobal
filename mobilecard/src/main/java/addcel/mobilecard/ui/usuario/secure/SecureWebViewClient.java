package addcel.mobilecard.ui.usuario.secure;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.common.base.Strings;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.R;
import timber.log.Timber;

final class SecureWebViewClient extends WebViewClient {

    private static final String JS_LOG_FUN = "javascript:window.HTMLOUT.logHtml("
            + "'<html>'"
            + "+document.getElementsByTagName('html')[0].innerHTML+"
            + "'</html>'"
            + ");";

    private final WebContract.Presenter presenter;
    private final WebContract.View view;

    SecureWebViewClient(WebContract.Presenter presenter, WebContract.View view) {
        this.presenter = presenter;
        this.view = view;
    }

    @Override
    public void onPageStarted(WebView webView, String url, Bitmap favicon) {
        view.startProgress(R.string.progress);
        if (BuildConfig.DEBUG) Timber.d("onPageStarted: %s", url);
    }

    @Override
    public void onPageFinished(WebView webView, String url) {
        view.stopProgress();
        if (BuildConfig.DEBUG) {
            Timber.d("onPageFinished: %s", url);
            webView.evaluateJavascript(JS_LOG_FUN, null);
        }
        view.setCurrentUrl(url);
        presenter.getCardDataForPeaje(url);
    }

    @Override
    public void onReceivedSslError(WebView webView, SslErrorHandler handler, SslError error) {
        view.stopProgress();
        if (BuildConfig.DEBUG) {
            handler.proceed();
        } else {
            if (error.getUrl().contains("procom.prosa.com.mx")) {
                view.showDebugHandlerDialog(handler, error);
            } else {
                view.showError(Strings.nullToEmpty(error.toString()), true);
            }
        }
    }


    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.view.stopProgress();
            Timber.d("onReceivedError: %s -> %d, %s", request.getUrl(), error.getErrorCode(), error.getDescription());
        }

    }

    @Override
    public void onReceivedHttpError(WebView webView, WebResourceRequest request,
                                    WebResourceResponse errorResponse) {

        view.stopProgress();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Timber.d("onReceivedHttpError: %s -> %d, %s", request.getUrl(), errorResponse.getStatusCode(), errorResponse.getReasonPhrase());
            if (errorResponse.getStatusCode() != 404 && errorResponse.getStatusCode() != 403) {
                view.showError(errorResponse.getReasonPhrase(), true);
            }
        }
    }
}
