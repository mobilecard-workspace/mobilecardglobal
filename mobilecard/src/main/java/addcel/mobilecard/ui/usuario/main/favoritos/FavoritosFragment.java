package addcel.mobilecard.ui.usuario.main.favoritos;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.event.impl.PostPaisEvent;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.PeajeFavUseCaseObject;
import addcel.mobilecard.ui.usuario.legacy.ServicioFavUseCaseObject;
import addcel.mobilecard.ui.usuario.legacy.TaeFavUseCaseObject;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.ui.usuario.main.MainContract;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * ADDCEL on 02/10/17.
 */
public final class FavoritosFragment extends Fragment implements FavoritosContract.View {
    @BindView(R.id.b_fav_editar)
    ImageButton editButton;

    @BindView(R.id.b_fav_edit_finish)
    Button finishButton;

    @BindView(R.id.recycler_favoritos)
    RecyclerView favoritosRecycler;

    @BindView(R.id.div_fav_edit)
    View editDivider;

    @Inject
    MainActivity activity;
    @Inject
    FavoritosAdapter adapter;
    @Inject
    FavoritosContract.Presenter presenter;
    @Inject
    Bus bus;

    private Unbinder unbinder;

    public FavoritosFragment() {
    }

    public static synchronized FavoritosFragment get() {
        return new FavoritosFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mobilecard.get()
                .getNetComponent()
                .favoritosSubcomponent(new FavoritosModule(this))
                .inject(this);
        bus.register(this);

        if (adapter.getItemCount() == 0) presenter.getAll();
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_favoritos, container, false);
        unbinder = ButterKnife.bind(this, view);

        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(Objects.requireNonNull(getContext()).getApplicationContext());

        favoritosRecycler.setLayoutManager(mLayoutManager);
        favoritosRecycler.setItemAnimator(new DefaultItemAnimator());
        favoritosRecycler.setAdapter(adapter);

        ItemClickSupport.addTo(favoritosRecycler)
                .setOnItemClickListener((recyclerView, position, v) -> {
                    if (!adapter.isEditable()) {
                        clickFavorito(position);
                    }
                });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setBottomMenuUseCase(UseCase.FAVORITOS);
        activity.setSelection(MainContract.UseCase.FRECUENTES);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        Timber.d("onHiddenChanged(%b)", hidden);
        super.onHiddenChanged(hidden);
        if (!hidden) {
            activity.setBottomMenuUseCase(UseCase.FAVORITOS);
            activity.setSelection(MainContract.UseCase.FRECUENTES);
            if (adapter.getItemCount() == 0) presenter.getAll();
        } else {
            adapter.setEditable(false);
        }
    }

    @Override
    public void onDestroyView() {
        cleanRecyclerView();
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        presenter.clearCompositeDisposable();
        super.onDestroy();
    }

    private void cleanRecyclerView() {
        ItemClickSupport.removeFrom(favoritosRecycler);
    }

    @Override
    public void notifyRemoval(int pos) {

        Timber.d("Favoritos before removal: %d", adapter.getItemCount());
        adapter.removeItem(pos);
        Timber.d("Favoritos after removal: %d", adapter.getItemCount());

        if (adapter.getItemCount() > 0) {
            clickEdit();
        } else {
            onFinishEdit();
        }
    }

    @Override
    public void notifyUpdate(int update, Favorito favorito) {
        adapter.restoreItem(update);
    }

    @Override
    public void populate(List<Favorito> favoritos) {
        adapter.updateItems(favoritos);
    }

    @Subscribe
    @Override
    public void onPaisUpdate(PostPaisEvent event) {
        presenter.getAll();
    }

    @OnClick(R.id.b_fav_editar)
    @Override
    public void clickEdit() {
        onEdit();
    }

    @Override
    public void onEdit() {
        Timber.d("llamando onEdit()");

        editDivider.setVisibility(View.VISIBLE);
        finishButton.setVisibility(View.VISIBLE);
        editButton.setEnabled(Boolean.FALSE);

        new Handler().postDelayed(() -> adapter.setEditable(Boolean.TRUE), 250);
    }

    @OnClick(R.id.b_fav_edit_finish)
    @Override
    public void clickFinishEdit() {
        onFinishEdit();
    }

    @Override
    public void onFinishEdit() {
        editDivider.setVisibility(View.GONE);
        finishButton.setVisibility(View.GONE);
        editButton.setEnabled(Boolean.TRUE);
        adapter.setEditable(false);
    }

    @Override
    public void clickFavorito(int pos) {
        Favorito item = adapter.getItem(pos);
        Intent intent = null;
        switch (item.getCategoria()) {
            case 1:
                TaeFavUseCaseObject tUseCase =
                        new TaeFavUseCaseObject(presenter.getRecargaModel(item), presenter.getMontos(item));
                intent = PagoContainerActivity.get(activity, tUseCase);
                break;
            case 2:
                PeajeFavUseCaseObject pUseCase =
                        new PeajeFavUseCaseObject(presenter.getRecargaModel(item), presenter.getMontos(item));
                intent = PagoContainerActivity.get(activity, pUseCase);
                break;
            case 3:
                ServicioFavUseCaseObject sUseCase =
                        new ServicioFavUseCaseObject(presenter.getServicioModel(item));
                intent = PagoContainerActivity.get(activity, sUseCase);
                break;
            case 4:
                ServiceModel serviceModel = presenter.getServiceModel(item);
                Timber.d("Creacion Model ACI: %s", serviceModel);
                intent = AciActivity.get(activity, presenter.getServiceModel(item), Boolean.TRUE);
                break;
        }
        startActivity(intent);
    }

    @Override
    public void clickDelete(int pos, Favorito fav) {
        if (isVisible()) showDeleteDialog(pos, fav);
    }

    @Override
    public void showDeleteDialog(int pos, Favorito fav) {
        new AlertDialog.Builder(activity).setMessage("¿Deseas eliminar este producto?")
                .setPositiveButton(android.R.string.ok,
                        (dialogInterface, i) -> presenter.removeFavorite(pos, fav))
                .setNegativeButton(android.R.string.cancel,
                        (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }
}
