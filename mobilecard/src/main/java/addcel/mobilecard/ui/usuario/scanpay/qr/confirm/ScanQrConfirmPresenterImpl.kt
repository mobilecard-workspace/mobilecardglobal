package addcel.mobilecard.ui.usuario.scanpay.qr.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenBaseResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import android.os.Build
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber
import java.text.NumberFormat

/**
 * ADDCEL on 11/09/18.
 */
internal class ScanQrConfirmPresenterImpl(private val interactor: ScanPayOpenInteractor, override val formaPago: CardEntity, private val pago: SPPagoEntity,
                                          private val format: NumberFormat, private val view: ScanQrConfirmView) : ScanQrConfirmPresenter {
    override fun fetchIdUsuario(): Long {
        return interactor.getIdUsuario()
    }

    override val idPais: Int
        get() = interactor.getIdPais(SPApi.TIPO_USUARIO)

    override val comision: Double
        get() = pago.comision

    override val monto: Double
        get() = pago.amount

    override val propina: Double
        get() = pago.propina

    override val concepto: String
        get() = pago.concept

    override val establecimiento: String
        get() = pago.establecimientoId.toString()

    override val formattedMonto: String
        get() = format.format(monto)

    override val formattedPropina: String
        get() = format.format(propina)

    override val formattedComision: String
        get() = format.format(pago.comision)

    override val formattedTotal: String
        get() {
            val total = monto + propina + comision
            return format.format(total)
        }

    override val referenciaNegocio: String
        get() = pago.referenciaNeg

    override fun buildRequest(): SPPagoEntity {
        return pago.copy(pago.amount, pago.comision, pago.concept, pago.ct,
                pago.establecimientoId, pago.firma, pago.idBitacora, formaPago.idTarjeta,
                interactor.getIdUsuario(), pago.imei, interactor.getLocation().lat,
                interactor.getLocation().lon, pago.modelo, view.getMsi().toInt(),
                pago.propina, pago.referenciaNeg, Build.VERSION.RELEASE, pago.tarjeta,
                pago.tipoTarjeta, pago.vigencia, pago.qrBase64, null, null, view.getEmail(), null)
    }

    override fun buildRequestString(model: SPPagoEntity): String {
        return requestToString(model)
    }

    override val jumioRef: String
        get() = interactor.getScanReference()

    override fun isOnWhiteList() {
        view.showProgress()
        interactor.isOnWhiteList(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), interactor.getIdPais(SPApi.TIPO_USUARIO), object : InteractorCallback<TokenBaseResponse> {
            override fun onSuccess(result: TokenBaseResponse) {
                view.hideProgress()
                if (result.code == 0) {
                    view.onWhiteList()
                } else {
                    view.notOnWhiteList()
                    view.enablePurchaseButton()
                }
            }

            override fun onError(error: String) {
                Timber.e(error)
                view.hideProgress()
                view.showError(error)
                view.enablePurchaseButton()
            }
        })
    }

    override fun getToken(profile: String) {
        view.showProgress()
        val request = requestToString(buildRequest())
        interactor.getToken(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                interactor.getIdPais(SPApi.TIPO_USUARIO), request, profile,
                object : InteractorCallback<TokenEntity> {
                    override fun onSuccess(result: TokenEntity) {
                        view.hideProgress()
                        view.onTokenReceived(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                        view.enablePurchaseButton()
                    }
                })
    }

    override fun launchPago(tokenEntity: TokenEntity, request: SPPagoEntity) {
        view.showProgress()
        interactor.pagoBP(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                tokenEntity.token, SPApi.TIPO_USUARIO, tokenEntity.accountId, buildRequest(),
                object : InteractorCallback<SPReceiptEntity> {
                    override fun onSuccess(result: SPReceiptEntity) {
                        view.hideProgress()
                        view.onPagoSuccess(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                        view.enablePurchaseButton()
                    }
                })
    }

    private fun requestToString(request: SPPagoEntity): String {
        return AddcelCrypto.encryptSensitive(JsonUtil.toJson(request))
    }

}