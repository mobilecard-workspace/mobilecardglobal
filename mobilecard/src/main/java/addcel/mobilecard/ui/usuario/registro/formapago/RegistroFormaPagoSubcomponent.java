package addcel.mobilecard.ui.usuario.registro.formapago;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 29/09/17.
 */
@PerFragment
@Subcomponent(modules = RegistroFormaPagoModule.class)
public interface RegistroFormaPagoSubcomponent {
    void inject(RegistroFormaPagoFragment fragment);
}
