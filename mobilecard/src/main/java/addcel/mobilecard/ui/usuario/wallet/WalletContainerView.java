package addcel.mobilecard.ui.usuario.wallet;

import java.util.List;

import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.usuario.menu.BottomMenuEventListener;

/**
 * ADDCEL on 10/10/17.
 */
interface WalletContainerView extends BottomMenuEventListener {
    void updateCards(List<CardEntity> models);

    void postWalletUpdateEvent(List<CardEntity> models);
}
