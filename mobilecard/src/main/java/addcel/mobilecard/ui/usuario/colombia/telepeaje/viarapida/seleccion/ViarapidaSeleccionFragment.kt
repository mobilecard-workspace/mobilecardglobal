package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.seleccion

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.colombia.VrTag
import addcel.mobilecard.data.net.colombia.VrTagDetalle
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.recargas.tae.seleccion.ColombiaRecargaTaeSeleccionAdapter
import addcel.mobilecard.ui.usuario.colombia.telepeaje.menu.ColombiaTelepeajeModel
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.confirm.ViarapidaConfirmModel
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import com.squareup.picasso.Picasso
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_viarapida_seleccion.*
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 9/26/19.
 */

@Parcelize
data class ViarapidaSeleccionModel(
        val proveedor: ColombiaTelepeajeModel,
        val tag: VrTag
) : Parcelable

interface ViarapidaSeleccionView : ScreenView {

    fun launchGetDetalle()

    fun onGetDetalle(detalle: VrTagDetalle)

    fun setContinuarEnabled(monto: Double)

    fun getSelectedMonto(): Double

    fun clickContinuar()
}

class ViarapidaSeleccionFragment : Fragment(), ViarapidaSeleccionView {

    companion object {

        private val COL_FORMAT = NumberFormat.getCurrencyInstance(Locale("es", "CO"))

        fun get(model: ViarapidaSeleccionModel): ViarapidaSeleccionFragment {
            val frag = ViarapidaSeleccionFragment()
            frag.arguments = BundleBuilder().putParcelable("model", model).build()
            return frag
        }
    }

    @Inject
    lateinit var picasso: Picasso
    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var presenter: ViarapidaSeleccionPresenter

    private lateinit var model: ViarapidaSeleccionModel
    private lateinit var tagDetalle: VrTagDetalle
    val adapter = ColombiaRecargaTaeSeleccionAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.viarapidaSeleccionSubcomponent(ViarapidaSeleccionModule(this))
                .inject(this)
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_viarapida_seleccion, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_seleccion_viarapida_montos.adapter = adapter
        recycler_seleccion_viarapida_montos.layoutManager = GridLayoutManager(view.context, 2)

        ItemClickSupport.addTo(recycler_seleccion_viarapida_montos)
                .setOnItemClickListener { _, pos, _ ->
                    adapter.toggleSelection(pos)
                    setContinuarEnabled(getSelectedMonto())
                }


        img_logo_seleccion_viarapida.setImageResource(model.proveedor.logoColor)
        view_seleccion_viarapida_cuenta.text = getTwoColorText("Cuenta: ", model.tag.cuenta)
        view_seleccion_viarapida_placa.text = StringUtil.parsePlaca(model.tag.placa, "-")
        b_seleccion_viarapida_continuar.setOnClickListener({ clickContinuar() })

        launchGetDetalle()
        setContinuarEnabled(0.0)
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    private fun getTwoColorText(first: String, second: String): CharSequence {
        val firstPart = SpannableString(first)
        val lastPart = SpannableString(second)

        firstPart.setSpan(
                ForegroundColorSpan(
                        ContextCompat.getColor(activity!!.applicationContext, R.color.colorBlack)
                ), 0,
                firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                ForegroundColorSpan(
                        ContextCompat.getColor(activity!!.applicationContext, R.color.colorAccent)
                ), 0,
                lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, lastPart)
    }

    override fun launchGetDetalle() {
        presenter.getDetalleTag(session.usuario.ideUsuario, model.tag)
    }

    override fun onGetDetalle(detalle: VrTagDetalle) {
        tagDetalle = detalle
        adapter.update(tagDetalle.montos)
        view_seleccion_viarapida_cuenta.text = getTwoColorText("Cuenta: ", tagDetalle.cuenta)
        view_seleccion_viarapida_saldo.text =
                getTwoColorText("SALDO\n", COL_FORMAT.format(tagDetalle.saldo))
        view_seleccion_viarapida_placa.text = StringUtil.parsePlaca(tagDetalle.placa, "-")
    }

    override fun setContinuarEnabled(monto: Double) {
        b_seleccion_viarapida_continuar.isChecked = monto > 0.0
    }

    override fun getSelectedMonto(): Double {
        return if (adapter.selectedItems.isEmpty()) 0.0
        else adapter.getItem(adapter.selectedItems[0])
    }

    override fun clickContinuar() {
        if (::tagDetalle.isInitialized && b_seleccion_viarapida_continuar.isChecked) {
            val confModel =
                    ViarapidaConfirmModel(
                            session.usuario, model.proveedor, tagDetalle, getSelectedMonto(),
                            COL_FORMAT
                    )
            val args = BundleBuilder().putParcelable("model", confModel).build()
            (activity as ContainerActivity).fragmentManagerLazy.commit {
                add(
                        R.id.frame_colombia_telepeaje,
                        WalletSelectFragment.get(WalletSelectFragment.PEAJE_COL, args)
                )
                hide(this@ViarapidaSeleccionFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        } else {
            (activity as ContainerActivity).showError(getString(R.string.error_monto_select))
        }
    }

    override fun showProgress() {
        (activity as ContainerActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ContainerActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ContainerActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ContainerActivity).showSuccess(msg)
    }
}