package addcel.mobilecard.ui.usuario.mymobilecard.previvale.historial

import addcel.mobilecard.data.net.previvale.historial.PrevivaleTransaction
import addcel.mobilecard.data.net.wallet.Transaction
import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil


/**
 * ADDCEL on 2019-12-02.
 */
class PrevivaleHistorialDiffCallback(private val oldList: List<PrevivaleTransaction>, private val newList: List<PrevivaleTransaction>) :
        DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldIt = oldList[oldItemPosition]
        val newIt = newList[newItemPosition]
        return oldIt.establecimiento == newIt.establecimiento && oldIt.fecha == newIt.fecha
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldIt = oldList[oldItemPosition]
        val newIt = newList[newItemPosition]
        return oldIt == newIt
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {

        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        val diff = Bundle()

        if (oldItem.establecimiento != newItem.establecimiento) {
            diff.putString("establecimiento", newItem.establecimiento)
        }

        if (oldItem.fecha != newItem.fecha) {
            diff.putString("fecha", newItem.fecha)
        }

        if (oldItem.status != newItem.status) {
            diff.putString("status", newItem.status)
        }

        if (oldItem.cantidad != newItem.cantidad) {
            diff.putString("cantidad", newItem.cantidad)
        }

        return if (diff.size() == 0) {
            null
        } else diff
    }
}