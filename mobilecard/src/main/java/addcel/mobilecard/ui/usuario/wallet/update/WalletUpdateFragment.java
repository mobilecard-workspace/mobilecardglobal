package addcel.mobilecard.ui.usuario.wallet.update;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.StringUtil;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.Lazy;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;

/**
 * ADDCEL on 10/10/17.
 */
public final class WalletUpdateFragment extends Fragment implements WalletUpdateContract.View {
    @BindDimen(R.dimen.widget_vertical_margin)
    int DIMEN;

    @NotEmpty(messageResId = R.string.error_nombre)
    @BindView(R.id.til_wallet_card_info_cardholder)
    TextInputLayout holderNameTil;

    @BindView(R.id.til_wallet_card_info_card)
    TextInputLayout numberTil;

    @NotEmpty(messageResId = R.string.error_cvv)
    @BindView(R.id.til_wallet_card_info_cvv)
    TextInputLayout cvvTil;

    @NotEmpty(messageResId = R.string.error_vigencia)
    @BindView(R.id.til_wallet_card_info_exp)
    TextInputLayout expirationTil;

    @BindView(R.id.img_wallet_card_info_franquicia)
    ImageView franquiciaImg;

    @BindView(R.id.spinner_wallet_card_info_tipo)
    Spinner cardTipoSpinner;

    @NotEmpty(messageResId = R.string.error_domicilio)
    @BindView(R.id.til_wallet_card_info_address1)
    TextInputLayout address1Til;

    @BindView(R.id.til_wallet_card_info_address2)
    TextInputLayout address2Til;

    @NotEmpty(messageResId = R.string.error_cp)
    @BindView(R.id.til_wallet_card_info_cp)
    TextInputLayout zipTil;

    @BindView(R.id.container_wallet_address_latam)
    LinearLayout latamContainer;

    //USA

    @BindView(R.id.container_wallet_address_usa)
    LinearLayout usaContainer;

    @BindView(R.id.spinner_wallet_card_info_state_usa)
    Spinner estadoUsaSpiner;

    @NotEmpty(messageResId = R.string.error_domicilio)
    @BindView(R.id.til_wallet_card_info_address_usa)
    TextInputLayout addressUsaTil;

    @NotEmpty(message = "Ingresa una ciudad válida")
    @BindView(R.id.til_wallet_card_info_city_usa)
    TextInputLayout cityUsaTil;

    @NotEmpty(messageResId = R.string.error_cp)
    @BindView(R.id.til_wallet_card_info_zip_usa)
    TextInputLayout zipUsaTil;

    //--


    @Inject
    WalletContainerActivity activity;
    @Inject
    CardEntity cardEntity;
    @Inject
    Validator validator;
    @Inject
    Calendar expirationCalendar;
    @Inject
    Lazy<DatePickerDialog> expirationDialog;
    @Inject
    ArrayAdapter<EstadoResponse.EstadoEntity> usaEstadoAdapter;
    @Inject
    WalletUpdateContract.Presenter presenter;
    @Inject
    Picasso picasso;

    private Unbinder unbinder;

    public WalletUpdateFragment() {
    }

    public static WalletUpdateFragment get(CardEntity model) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("card", model);
        WalletUpdateFragment fragment = new WalletUpdateFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .walletUpdateSubcomponent(new WalletUpdateModule(this))
                .inject(this);
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        try {
            if (!presenter.isPrimary(cardEntity) && !presenter.isMobilecard(cardEntity)) {
                inflater.inflate(R.menu.menu_wallet, menu);
            }
        } catch (Throwable t) {
            Timber.e(t);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            presenter.delete(cardEntity);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_wallet_card_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        estadoUsaSpiner.setAdapter(usaEstadoAdapter);
        Objects.requireNonNull(numberTil.getEditText()).setOnFocusChangeListener((view1, b) -> {
            if (b) numberTil.setError(null);
        });
        Objects.requireNonNull(cvvTil.getEditText()).setOnFocusChangeListener((view1, b) -> {
            if (b) cvvTil.setError(null);
        });
        Objects.requireNonNull(expirationTil.getEditText()).setOnFocusChangeListener((view12, b) -> {
            if (b) {
                expirationTil.setError(null);
                showExpirationDialog();
            } else {
                dismissExpirationDialog();
            }
        });
        Objects.requireNonNull(address1Til.getEditText()).setOnFocusChangeListener((view1, b) -> {
            if (b) address1Til.setError(null);
        });
        Objects.requireNonNull(zipTil.getEditText()).setOnFocusChangeListener((view1, b) -> {
            if (b) zipTil.setError(null);
        });
        Objects.requireNonNull(holderNameTil.getEditText())
                .setText(presenter.getCardHolderName(cardEntity));
        numberTil.getEditText().setText(presenter.getCard(cardEntity));
        numberTil.setEnabled(false);
        setCardField();
        cvvTil.getEditText().setText(presenter.getCvv(cardEntity));
        expirationTil.getEditText()
                .setText(String.format("%s/%s", presenter.getVigencia(cardEntity)[0],
                        presenter.getVigencia(cardEntity)[1]));
        cardTipoSpinner.setSelection(
                presenter.getTipoTarjeta(cardEntity).equals(TipoTarjetaEntity.CREDITO) ? 0 : 1);
        address1Til.getEditText().setText(presenter.getDireccion1(cardEntity));
        Objects.requireNonNull(address2Til.getEditText()).setText(presenter.getDireccion2(cardEntity));
        zipTil.getEditText().setText(presenter.getZipCode(cardEntity));

        view.findViewById(R.id.label_wallet_card_scan).setVisibility(View.GONE);
        view.findViewById(R.id.label_wallet_card_info).setVisibility(View.GONE);
        activity.toolbar.setTitle(R.string.txt_wallet_card_info);

        showAdressFields(presenter.getIdPais());
    }

    private void showAdressFields(int idPais) {
        switch (idPais) {
            case PaisResponse.PaisEntity
                    .MX:
                usaContainer.setVisibility(View.GONE);
                latamContainer.setVisibility(View.VISIBLE);
                break;
            case PaisResponse.PaisEntity.CO:
            case PaisResponse.PaisEntity.PE:
                usaContainer.setVisibility(View.GONE);
                latamContainer.setVisibility(View.VISIBLE);
                zipTil.setVisibility(View.GONE);
                break;
            case PaisResponse.PaisEntity.US:
                usaContainer.setVisibility(View.VISIBLE);
                latamContainer.setVisibility(View.GONE);
                presenter.getEstados();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        activity.toolbar.setTitle(R.string.txt_menu_wallet);
        unbinder.unbind();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            expirationDialog.get().setOnDateSetListener(null);
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public boolean isPrimary() {
        return cardEntity.getDeterminada();
    }

    @Override
    public String getCardHolderName() {
        return StringUtil.removeAcentosMultiple(Strings.nullToEmpty(
                Objects.requireNonNull(holderNameTil.getEditText()).getText().toString()));
    }

    @Override
    public void setCardField() {
        int icon;
        switch (presenter.getFranquicia(cardEntity)) {
            case MasterCard:
                icon = R.drawable.logo_master;
                break;
            case AmEx:
                icon = R.drawable.logo_american;
                break;
            case Carnet:
                icon = R.drawable.card_carnet;
                break;
            default:
                icon = R.drawable.logo_visa;
                break;
        }
        franquiciaImg.setImageResource(icon);
    }

    @Override
    public String getCard() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(numberTil.getEditText()).getText().toString());
    }

    @Override
    public String getCvv() {
        return Strings.nullToEmpty(Objects.requireNonNull(cvvTil.getEditText()).getText().toString());
    }

    @OnClick(R.id.text_wallet_card_info_exp)
    @Override
    public void clickVigencia() {
        expirationTil.setError(null);
        if (!expirationDialog.get().isShowing()) expirationDialog.get().show();
    }

    @Override
    public String getVigencia() {
        return DateFormat.format("MM/yy", expirationCalendar).toString();
    }

    @Override
    public TipoTarjetaEntity getTipoTarjeta() {
        switch (cardTipoSpinner.getSelectedItemPosition()) {
            case 1:
                return TipoTarjetaEntity.DEBITO;
            case 2:
                return TipoTarjetaEntity.CUENTA_BANCARIA;
            default:
                return TipoTarjetaEntity.CREDITO;

        }
    }

    @Override
    public String getAddress1() {
        return AndroidUtils.getText(address1Til.getEditText());
    }

    @Override
    public String getAddress2() {
        return AndroidUtils.getText(address2Til.getEditText());
    }

    @Override
    public String getZipCode() {
        return AndroidUtils.getText(zipTil.getEditText());
    }

    @OnClick(R.id.b_wallet_card_info_save)
    @Override
    public void clickSave() {
        validator.validate();
    }

    @Override
    public void showProgress() {
        activity.getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        activity.getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showExpirationDialog() {
        if (isVisible() && !expirationDialog.get().isShowing()) expirationDialog.get().show();
    }

    @Override
    public void dismissExpirationDialog() {
        if (isVisible() && expirationDialog.get().isShowing()) expirationDialog.get().dismiss();
    }

    @Override
    public void showError(int resId) {
        showError(getString(resId));
    }

    @Override
    public void showError(String msg) {
        Toasty.error(activity, msg).show();
    }

    @Override
    public void showSuccess(String msg) {
        Toasty.success(activity, msg).show();
    }

    @Override
    public void setValuesOnError() {
        Objects.requireNonNull(holderNameTil.getEditText())
                .setText(presenter.getCardHolderName(cardEntity));
        Objects.requireNonNull(numberTil.getEditText()).setText(presenter.getCard(cardEntity));
        numberTil.setEnabled(false);
        Objects.requireNonNull(cvvTil.getEditText()).setText(presenter.getCvv(cardEntity));
        Objects.requireNonNull(expirationTil.getEditText())
                .setText(String.format("%s/%s", presenter.getVigencia(cardEntity)[0],
                        presenter.getVigencia(cardEntity)[1]));
        cardTipoSpinner.setSelection(
                presenter.getTipoTarjeta(cardEntity).equals(TipoTarjetaEntity.CREDITO) ? 0 : 1);
        Objects.requireNonNull(address1Til.getEditText()).setText(presenter.getDireccion1(cardEntity));
        Objects.requireNonNull(address2Til.getEditText()).setText(presenter.getDireccion2(cardEntity));
        Objects.requireNonNull(zipTil.getEditText()).setText(presenter.getZipCode(cardEntity));
    }

    @Override
    public void updateCards(List<CardEntity> models) {
        activity.postWalletUpdateEvent(models);
    }

    @Override
    public void updateCardsAndFinish(List<CardEntity> models) {
        updateCards(models);
        activity.onBackPressed();
    }

    @Override
    public int getSelectedState() {
        if (usaEstadoAdapter.isEmpty()) return 0;
        else {
            return ((EstadoResponse.EstadoEntity) estadoUsaSpiner.getSelectedItem()).getId();
        }
    }

    @Override
    public void updateStates(List<EstadoResponse.EstadoEntity> states) {
        if (usaEstadoAdapter.getCount() > 0) usaEstadoAdapter.clear();
        usaEstadoAdapter.addAll(states);
    }

    @Override
    public String getDirUsa() {
        return AndroidUtils.getText(addressUsaTil.getEditText());
    }

    @Override
    public String getCityUsa() {
        return AndroidUtils.getText(cityUsaTil.getEditText());
    }

    @Override
    public String getZipUsa() {
        return AndroidUtils.getText(zipUsaTil.getEditText());
    }

    @Override
    public void lockIfMobilecard() {
    }

    @Override
    public void onValidationSucceeded() {
        ValidationUtils.clearViewErrors(holderNameTil, numberTil, cvvTil, expirationTil, address1Til,
                zipTil, addressUsaTil, cityUsaTil, zipUsaTil);

        presenter.update(cardEntity);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        expirationCalendar.set(Calendar.YEAR, i);
        expirationCalendar.set(Calendar.MONTH, i1);
        Objects.requireNonNull(expirationTil.getEditText())
                .setText(DateFormat.format("MM/yy", expirationCalendar));
    }
}
