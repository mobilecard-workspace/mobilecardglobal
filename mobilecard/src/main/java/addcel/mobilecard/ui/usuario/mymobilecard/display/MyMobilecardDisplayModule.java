package addcel.mobilecard.ui.usuario.mymobilecard.display;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.Objects;

import addcel.mobilecard.data.net.previvale.historial.PrevivaleAPI;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public final class MyMobilecardDisplayModule {
    private final MyMobilecardDisplayFragment fragment;

    MyMobilecardDisplayModule(MyMobilecardDisplayFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MyMobilecardActivity provideActivity() {
        return (MyMobilecardActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    CardEntity provideModel() {
        return Objects.requireNonNull(fragment.getArguments()).getParcelable("myMobilecard");
    }

    @PerFragment
    @Provides
    MyMobilecardDisplayTransactionAdapter provideAdapter(CardEntity model,
                                                         NumberFormat currFormat) {
        if (model.getMovements() == null) {
            return new MyMobilecardDisplayTransactionAdapter(Collections.emptyList(), currFormat);
        } else {
            return new MyMobilecardDisplayTransactionAdapter(model.getMovements(), currFormat);
        }
    }

    @PerFragment
    @Provides
    PrevivaleAPI provideApi(Retrofit retrofit) {
        return PrevivaleAPI.Companion.create(retrofit);
    }

    @PerFragment
    @Provides
    MyMobilecardDisplayPresenter providePresenter(PrevivaleAPI previvaleAPI, CardEntity model,
                                                  NumberFormat numberFormat) {
        return new MyMobilecardDisplayPresenterImpl(previvaleAPI, model, numberFormat, fragment);
    }
}
