package addcel.mobilecard.ui.usuario.apto.user.disclaimer

import addcel.mobilecard.R
import addcel.mobilecard.ui.usuario.apto.user.name.AptoUserNameActivity
import addcel.mobilecard.ui.usuario.apto.user.name.AptoUserNameModel
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_disclaimer.*


@Parcelize
data class AptoDisclaimerModel(
        val phone: String,
        val phoneVerification: String,
        val email: String,
        val emailVerification: String
) : Parcelable

class AptoDisclaimerActivity : AppCompatActivity() {

    companion object {
        fun get(context: Context, model: AptoDisclaimerModel): Intent {
            return Intent(context, AptoDisclaimerActivity::class.java).putExtra("model", model)
        }
    }

    lateinit var model: AptoDisclaimerModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_disclaimer)

        model = intent?.getParcelableExtra("model")!!

        b_apto_disclaimer_agree.setOnClickListener {
            startActivity(
                    AptoUserNameActivity.get(
                            this@AptoDisclaimerActivity,
                            AptoUserNameModel(
                                    model.phone,
                                    model.phoneVerification,
                                    model.email,
                                    model.emailVerification
                            )
                    )
            )
        }

        b_apto_disclaimer_disagree.setOnClickListener {
            finish()
        }
    }
}
