package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.compra

import addcel.mobilecard.data.net.colombia.multimarket.MmPagoServicioEntity
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-12-20.
 */
interface MmServiciosConfirmPresenter {
    fun addToDisposables(disposable: Disposable)
    fun clearDisposables()
    fun getIdUsuario(): Long
    fun pagarServicio(request: MmPagoServicioEntity)
}

class MmServiciosConfirmPresenterImpl(
        val multimarket: MultimarketAPI,
        val usuario: Usuario,
        val compositeDisposable: CompositeDisposable,
        val view: MmServiciosConfirmView) : MmServiciosConfirmPresenter {

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun getIdUsuario(): Long {
        return usuario.ideUsuario
    }

    override fun pagarServicio(request: MmPagoServicioEntity) {

        view.showProgress()
        val cDisp = multimarket.pagoServicio(
                request
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    view.onPagar(it)
                }, {
                    view.enablePurchaseButton()
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(cDisp)
    }

}