package addcel.mobilecard.ui.usuario.edocuenta;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.domain.usuario.bottom.MenuEvent;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.ui.ScreenView;
import addcel.mobilecard.ui.contacto.ContactoActivity;
import addcel.mobilecard.ui.contacto.ContactoModel;
import addcel.mobilecard.ui.usuario.edocuenta.list.HistorialListFragment;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.ui.usuario.main.MainContract;
import addcel.mobilecard.ui.usuario.menu.BottomMenuEventListener;
import addcel.mobilecard.ui.usuario.menu.MenuLayout;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class EdocuentaActivity extends AppCompatActivity
        implements ScreenView, BottomMenuEventListener {
    @BindView(R.id.toolbar_edocuenta)
    Toolbar toolbar;
    @BindView(R.id.menu_edocuenta)
    MenuLayout bottomMenu;
    @Inject
    Bus bus;
    @Inject
    Picasso picasso;
    @Inject
    SessionOperations session;

    public static synchronized Intent get(Context context) {
        return new Intent(context, EdocuentaActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().inject(this);
        setContentView(R.layout.activity_edocuenta);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_edocuenta, new HistorialListFragment())
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
        bottomMenu.unblock();
    }

    @Override
    protected void onPause() {
        bottomMenu.block();
        bus.unregister(this);
        super.onPause();
    }

    @Override
    public void showProgress() {
        findViewById(R.id.progress_edocuenta).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        findViewById(R.id.progress_edocuenta).setVisibility(View.GONE);
    }

    @Override
    public void showError(@NonNull String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void showSuccess(@NotNull String charSequence) {
        Toasty.success(this, charSequence).show();
    }

    @Subscribe
    @Override
    public void onMenuEventReceived(@NotNull MenuEvent event) {
        bottomMenu.unblock();
        final UseCase useCase = event.getUseCase();
        if (useCase.equals(UseCase.HOME)) {
            startActivity(MainActivity.get(this, MainContract.UseCase.INICIO, false));
        } else if (useCase.equals(UseCase.WALLET)) {
            startActivity(WalletContainerActivity.get(this));
        } else if (useCase.equals(UseCase.CONTACTO)) {
            startActivity(ContactoActivity.Companion.get(this, new ContactoModel(session.getUsuario().getIdeUsuario(),
                    session.getUsuario().getEMail(),
                    session.getUsuario().getIdPais())));
        }
    }

    public Picasso getPicasso() {
        return picasso;
    }
}
