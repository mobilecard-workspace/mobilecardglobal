package addcel.mobilecard.ui.usuario.blackstone.result;

import android.text.format.DateFormat;

import com.google.common.base.Strings;
import com.squareup.phrase.Phrase;

import java.text.NumberFormat;
import java.util.Calendar;

import addcel.mobilecard.data.net.blackstone.model.BSPagoResponseEntity;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 11/07/18.
 */
class BSResultPresenter implements BSResultContract.Presenter {

    private final BSPagoResponseEntity result;
    private final NumberFormat currencyFormat;

    BSResultPresenter(BSPagoResponseEntity result, NumberFormat currencyFormat) {
        this.result = result;
        this.currencyFormat = currencyFormat;
    }

    @Override
    public BSPagoResponseEntity getResult() {
        return result;
    }

    @Override
    public CharSequence buildSuccessMsg(String base) {
        return Phrase.from(base)
                .put("servicio", Strings.nullToEmpty(result.getCarrierName()))
                .put("fecha", Strings.nullToEmpty(result.getFecha()))
                .put("destino", Strings.nullToEmpty(result.getToppedUpNumber()))
                .put("folio", Strings.nullToEmpty(result.getTransactionID()))
                .put("card", Strings.isNullOrEmpty(result.getMaskCard()) ? ""
                        : StringUtil.maskCard(AddcelCrypto.decryptCard(result.getMaskCard())))
                .put("auth", Strings.nullToEmpty(result.getAuthorizationCode()))
                .put("monto", currencyFormat.format(result.getBalance()))
                .put("comision", currencyFormat.format(result.getComision()))
                .put("total", currencyFormat.format(result.getTotal()))
                .format();
    }

    @Override
    public String buildScreenShotName() {
        String fecha = DateFormat.format("yyyyMMddHHmmss", Calendar.getInstance()).toString();
        return "topup_bs_mc_" + fecha + ".png";
    }
}
