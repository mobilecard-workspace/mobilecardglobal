package addcel.mobilecard.ui.usuario.wallet.update;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 10/10/17.
 */
@PerFragment
@Subcomponent(modules = WalletUpdateModule.class)
public interface WalletUpdateSubcomponent {
    void inject(WalletUpdateFragment fragment);
}
