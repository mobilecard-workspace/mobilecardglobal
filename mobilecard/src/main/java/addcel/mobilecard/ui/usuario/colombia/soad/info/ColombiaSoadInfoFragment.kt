package addcel.mobilecard.ui.usuario.colombia.soad.info

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.colombia.SoatConsultaRequest
import addcel.mobilecard.data.net.colombia.SoatConsultaResponse
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.colombia.soad.processing.ColombiaSoadProcessingModel
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.app.AlertDialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_soad_info.*
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 10/1/19.
 */

@Parcelize
data class ColombiaSoadInfoModel(val placa: String) : Parcelable

interface ColombiaSoadInfoView : ScreenView {
    fun getData()

    fun setData(info: SoatConsultaResponse)

    fun showRetry()

    fun hideRetry()

    fun clickRetry()

    fun clickContinuar()

    fun clickBack()

    fun checkTerms(checked: Boolean)

    fun showTerminosDialog(data: String)
}

class ColombiaSoadInfoFragment : Fragment(), ColombiaSoadInfoView {

    companion object {

        private val COL_FORMAT = NumberFormat.getCurrencyInstance(Locale("es", "CO"))

        fun get(model: ColombiaSoadInfoModel): ColombiaSoadInfoFragment {
            val frag = ColombiaSoadInfoFragment()
            frag.arguments = BundleBuilder().putParcelable("model", model).build()
            return frag
        }
    }

    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var model: ColombiaSoadInfoModel
    @Inject
    lateinit var presenter: ColombiaSoatInfoPresenter

    private lateinit var oldColor: ColorStateList
    private lateinit var soatInfo: SoatConsultaResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.colombiaSoatInfoSubcomponent(ColombiaSoatInfoModule(this))
                .inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_soad_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view_info_retry.findViewById<Button>(R.id.b_retry).setOnClickListener { clickRetry() }

        b_soat_info_continuar.setOnClickListener { clickContinuar() }

        switch_soat_info_terms.setOnCheckedChangeListener { _, checked -> checkTerms(checked) }

        oldColor = view_soad_nombre.textColors
        getData()
    }

    override fun getData() {
        val request = SoatConsultaRequest(
                BuildConfig.ADDCEL_APP_ID,
                PaisResponse.PaisEntity.CO,
                session.usuario.ideUsuario,
                StringUtil.getCurrentLanguage(),
                model.placa
        )

        presenter.getInfo(request)
    }

    override fun setData(info: SoatConsultaResponse) {

        if (isVisible) {

            soatInfo = info

            view_soad_nombre.text = getTwoColorText(
                    "Nombre\n",
                    session.usuario.usrNombre + " " + session.usuario.usrApellido + " " + session.usuario.usrMaterno
            )
            view_soad_marca.text = getTwoColorText("Marca\n", soatInfo.marca ?: "")
            view_soad_cc.text = getTwoColorText("CC\n", session.usuario.cedula)
            view_soad_linea.text = getTwoColorText("Línea\n", soatInfo.linea ?: "")
            view_soad_telefono.text = getTwoColorText("Teléfono\n", session.usuario.usrTelefono)
            view_soad_modelo.text = getTwoColorText("Modelo\n", soatInfo.modelo ?: "")
            view_soad_email.text = getTwoColorText("Email\n", session.usuario.eMail)
            view_soad_placa.text = getTwoColorText("Placa\n", model.placa)
            view_soad_direccion.text =
                    getTwoColorText("Dirección de Residencia\n", session.usuario.usrDireccion)
            view_soad_categoria.text = getTwoColorText("Categoría\n", soatInfo.categoria ?: "")
            view_soad_ciudad.text = getTwoColorText("Ciudad\n", session.usuario.usrCiudad)
            view_soad_seguro.text =
                    getTwoColorText("Seguro SOAT ", COL_FORMAT.format(soatInfo.seguroSoat))
            view_soad_vigencia_desde.setText(
                    getString(
                            R.string.txt_col_desde,
                            soatInfo.vigenciaIni ?: ""
                    )
            )
            view_soad_vigencia_hasta.setText(
                    getString(
                            R.string.txt_col_hasta,
                            soatInfo.vigenciaFin ?: ""
                    )
            )
            view_soad_total.text = getTwoColorText("Total ", COL_FORMAT.format(soatInfo.total))
        }
    }

    override fun showRetry() {
        if (view_info_retry.visibility == View.GONE) view_info_retry.visibility = View.VISIBLE
    }

    override fun hideRetry() {
        if (view_info_retry.visibility == View.VISIBLE) view_info_retry.visibility = View.GONE
    }

    override fun clickRetry() {
        getData()
    }

    override fun clickContinuar() {
        if (::soatInfo.isInitialized) {
            if (b_soat_info_continuar.isChecked) {
                val pModel = ColombiaSoadProcessingModel(
                        soatInfo, session.usuario.ideUsuario,
                        session.usuario.scanReference
                )

                (activity as ContainerActivity).fragmentManagerLazy.commit {
                    add(
                            R.id.frame_colombia_soad, WalletSelectFragment.get(
                            WalletSelectFragment.SOAD_COL,
                            BundleBuilder().putParcelable("model", pModel).build()
                    )
                    )
                    hide(this@ColombiaSoadInfoFragment)
                    addToBackStack(null)
                    setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
                }
            } else {
                showError(getString(R.string.error_terms_conditions))
            }
        } else {
            showError(getString(R.string.error_colombia_consulta_poliza))
        }
    }

    override fun clickBack() {
        (activity as ContainerActivity).onBackPressed()
    }

    override fun showProgress() {
        (activity as ContainerActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ContainerActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ContainerActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ContainerActivity).showSuccess(msg)
    }

    private fun getTwoColorText(first: String, second: String): CharSequence {
        val firstPart = SpannableString(first)
        val lastPart = SpannableString(second)

        firstPart.setSpan(
                ForegroundColorSpan(
                        ContextCompat.getColor(activity!!.applicationContext, R.color.colorAccent)
                ), 0,
                firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                ForegroundColorSpan(oldColor.defaultColor), 0, lastPart.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                StyleSpan(android.graphics.Typeface.BOLD), 0, lastPart.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )


        return TextUtils.concat(firstPart, lastPart)
    }

    override fun checkTerms(checked: Boolean) {
        if (checked) {
            presenter.getTerminos()
        } else {
            b_soat_info_continuar.isChecked = false
        }
    }

    override fun showTerminosDialog(data: String) {
        if (view != null) {
            AlertDialog.Builder(context).setTitle(R.string.txt_terms_and_conditions_title)
                    .setMessage(data).setPositiveButton(android.R.string.yes) { _, _ ->
                        if (switch_soat_info_terms != null) switch_soat_info_terms.isChecked = true
                        if (b_soat_info_continuar != null) b_soat_info_continuar.isChecked = true
                    }.setNegativeButton(android.R.string.no) { _, _ ->
                        if (switch_soat_info_terms != null) switch_soat_info_terms.isChecked = false
                        if (b_soat_info_continuar != null) b_soat_info_continuar.isChecked = false
                    }.show()
        }
    }
}