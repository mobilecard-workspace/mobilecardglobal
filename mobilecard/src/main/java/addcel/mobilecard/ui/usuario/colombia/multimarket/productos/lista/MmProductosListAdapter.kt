package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.lista

import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Producto
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

/**
 * ADDCEL on 2019-12-18.
 */
class MmProductosListAdapter(val data: MutableList<Producto> = ArrayList()) : RecyclerView.Adapter<MmProductosListAdapter.ViewHolder>() {

    fun update(args: List<Producto>) {
        data.clear()
        data.addAll(args)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_mm_producto, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val producto: Producto? = data[position]
        if (producto != null) {
            holder.text.text = producto.nombre
        }
    }

    fun getItem(position: Int): Producto {
        return data[position]
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val text: TextView = v.findViewById(R.id.text_mm_item)
    }

}