package addcel.mobilecard.ui.usuario.scanpay.menu;

import java.util.Objects;

import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import dagger.Module;
import dagger.Provides;

@Module
public final class ScanMenuModule {
    private final ScanMenuFragment fragment;

    ScanMenuModule(ScanMenuFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ScanPayActivity provideActivity() {
        return (ScanPayActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    ScanMenuAdapter provideAdapter() {
        return new ScanMenuAdapter();
    }
}
