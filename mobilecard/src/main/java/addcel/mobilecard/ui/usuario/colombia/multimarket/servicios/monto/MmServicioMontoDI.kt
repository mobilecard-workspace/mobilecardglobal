package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.monto

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-12-24.
 */
@Module
class MmServicioMontoModule(val fragment: MmServicioMontoFragment) {

    @PerFragment
    @Provides
    fun provideContainerActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }


    @PerFragment
    @Provides
    fun provideViewModel(): MmServicioMontoModel {
        return fragment.arguments?.getParcelable("model")!!
    }


    @PerFragment
    @Provides
    fun provideApi(retrofit: Retrofit): MultimarketAPI {
        return MultimarketAPI.get(retrofit)
    }


    @PerFragment
    @Provides
    fun provideUser(sessionOperations: SessionOperations): Usuario {
        return sessionOperations.usuario
    }


    @PerFragment
    @Provides
    fun providePresenter(multimarketAPI: MultimarketAPI, usuario: Usuario): MmServicioMontoPresenter {
        return MmServicioMontoPresenterImpl(multimarketAPI, usuario, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [MmServicioMontoModule::class])
interface MmServicioMontoSubcomponent {
    fun inject(fragment: MmServicioMontoFragment)
}