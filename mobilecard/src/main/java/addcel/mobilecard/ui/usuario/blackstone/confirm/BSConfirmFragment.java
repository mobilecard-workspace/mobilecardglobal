package addcel.mobilecard.ui.usuario.blackstone.confirm;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.blackstone.model.BSPagoResponseEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import addcel.mobilecard.ui.usuario.blackstone.result.BSResultFragment;
import addcel.mobilecard.utils.AndroidUtils;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * ADDCEL on 10/07/18.
 */
public final class BSConfirmFragment extends Fragment implements BSConfirmContract.View {
    @Inject
    BlackstoneActivity wrapperActivity;
    @Inject
    BSConfirmContract.Presenter presenter;

    private Unbinder unbinder;
    private RxPermissions rxPermissions;

    public BSConfirmFragment() {
    }

    public static synchronized BSConfirmFragment get(Bundle bundle) {
        BSConfirmFragment fragment = new BSConfirmFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mobilecard.get()
                .getNetComponent()
                .bsConfirmSubcomponent(new BSConfirmModule(this))
                .inject(this);

        rxPermissions = new RxPermissions(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_bs_confirm, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        wrapperActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);

        TextView referenciaView = view.findViewById(R.id.view_bs_confirm_referencia);
        referenciaView.setText(presenter.getPhone());
        TextInputLayout montoTil = view.findViewById(R.id.til_bs_confirm_balance_usd);
        AndroidUtils.setText(montoTil.getEditText(), presenter.formatCantidad(presenter.getMonto()));
        TextInputLayout feeTil = view.findViewById(R.id.til_bs_confirm_fee);
        AndroidUtils.setText(feeTil.getEditText(), presenter.formatCantidad(presenter.getComision()));
        TextInputLayout totalTil = view.findViewById(R.id.til_bs_confirm_total);
        AndroidUtils.setText(totalTil.getEditText(),
                presenter.formatCantidad(presenter.getGrandTotal()));

        presenter.addToDisposables(rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION).subscribe(aBoolean -> {
            if (!aBoolean) {
                showError("Requerimos tu localización para garantizar la seguridad de tu transacción");
                wrapperActivity.onBackPressed();
            }
        }));
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        wrapperActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        wrapperActivity.hideProgress();
    }

    @Override
    public void showError(@NotNull String msg) {
        wrapperActivity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        wrapperActivity.showSuccess(msg);
    }

    @Override
    public void onPagoSuccess(BSPagoResponseEntity response) {
        wrapperActivity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.frame_blackstone, BSResultFragment.get(response))
                .hide(this)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();
    }

    @Override
    public void enablePagarButton() {
        if (getView() != null) {
            getView().findViewById(R.id.b_bs_confirm_pagar).setEnabled(true);
        }
    }

    @Override
    public void disablePagarButton() {
        if (getView() != null) {
            getView().findViewById(R.id.b_bs_confirm_pagar).setEnabled(false);
        }
    }

    @OnClick(R.id.b_bs_confirm_pagar)
    @Override
    public void clickPagar() {
        presenter.checkWhiteList();
    }

    @Override
    public void onWhiteList() {
        presenter.pagar();
    }

    @Override
    public void notOnWhiteList() {
        showError("No estás autorizado para realizar esta operación. Contacta a soporte.");
    }
}
