package addcel.mobilecard.ui.usuario.mymobilecard.display

import addcel.mobilecard.R
import addcel.mobilecard.data.net.wallet.MovementEntity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.phrase.Phrase
import java.text.NumberFormat

/**
 * ADDCEL on 21/11/17.
 */
class MyMobilecardDisplayTransactionAdapter internal constructor(
        private val movement: List<MovementEntity>, private val format: NumberFormat
) : RecyclerView.Adapter<MyMobilecardDisplayTransactionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_my_mobilecard_transaction, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val phrase = Phrase.from(holder.text.context, R.string.pattern_mymc_transaction)
        val (_, total, ticket, date) = movement[position]

        holder.text.text = phrase.put("name", ticket)
                .put("amount", format.format(total))
                .put("date", date)
                .format()
    }

    override fun getItemCount(): Int {
        return movement.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val text: TextView = itemView.findViewById(R.id.text_mymobilecard_transactions)

    }
}
