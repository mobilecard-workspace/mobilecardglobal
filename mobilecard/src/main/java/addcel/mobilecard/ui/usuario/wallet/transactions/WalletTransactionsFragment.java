package addcel.mobilecard.ui.usuario.wallet.transactions;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.MovementEntity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * ADDCEL on 09/10/17.
 */
public final class WalletTransactionsFragment extends Fragment {

    @BindView(R.id.recycler_wallet_transactions)
    RecyclerView transactionsView;

    private Unbinder unbinder;

    public WalletTransactionsFragment() {
    }

    public static WalletTransactionsFragment get(List<MovementEntity> transactions) {
        WalletTransactionsFragment fragment = new WalletTransactionsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("transactions", (ArrayList<? extends Parcelable>) transactions);
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_wallet_transactions, container, false);
        unbinder = ButterKnife.bind(this, view);
        transactionsView.setAdapter(new WalletTransactionsAdapter(
                Objects.requireNonNull(getArguments()).getParcelableArrayList("transactions")));
        transactionsView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onDestroyView() {
        transactionsView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }
}
