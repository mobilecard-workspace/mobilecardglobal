package addcel.mobilecard.ui.usuario.edocuenta.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.List;

import addcel.mobilecard.McConstants;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.Transaction;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 23/06/17.
 */
public final class EdocuentaListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private final List<Object> data;

    EdocuentaListAdapter(List<Object> data) {
        this.data = data;
    }

    public Disposable update(List<Object> newData) {

        DiffUtil.Callback callback = new EdocuentaListDiffCallback(this.data, newData);

        return Observable.fromCallable(() -> DiffUtil.calculateDiff(callback)
        ).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(diffResult -> {
                    diffResult.dispatchUpdatesTo(this);
                    data.clear();
                    data.addAll(newData);
                });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            return new IViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_historial, parent, false));
        } else if (viewType == TYPE_HEADER) {
            return new HViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_edocuenta_header, parent, false));
        } else {
            throw new RuntimeException("");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        int viewType = getItemViewType(position);
        String ticket, date, title, currency;
        double importe;
        int status, idPais;
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            Bundle o = (Bundle) payloads.get(0);
            for (String key : o.keySet()) {
                if (viewType == TYPE_HEADER) {
                    title = o.getString("title");
                    ((HViewHolder) holder).header.setText(title);
                } else {
                    if (key.equals("ticket")) {
                        ticket = o.getString(key);
                        ((IViewHolder) holder).ticketView.setText(ticket);
                    }
                    if (key.equals("date")) {
                        date = o.getString(key);
                        ((IViewHolder) holder).dateView.setText(date);
                    }
                    if (key.equals("importe")) {
                        importe = o.getDouble(key);
                        idPais = o.getInt("idPais");
                        currency = o.getString("currency");
                        String total = getTotalFormat(idPais, importe, currency);
                        ((IViewHolder) holder).totalView.setText(total);
                    }
                    if (key.equals("status")) {
                        status = o.getInt(key);
                        ((IViewHolder) holder).setStatusResource(status);
                    }
                }
            }
        }
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof IViewHolder) {
            Transaction t = (Transaction) data.get(position);
            ((IViewHolder) holder).ticketView.setText(t.getTicket());
            ((IViewHolder) holder).totalView.setText(
                    getTotalFormat(t.getIdPais(), t.getTotal(), t.getCurrency()));
            ((IViewHolder) holder).dateView.setText(t.getDate());
            ((IViewHolder) holder).setStatusResource(t.getStatus());
        } else if (holder instanceof HViewHolder) {
            ((HViewHolder) holder).header.setText(String.valueOf(data.get(position)));
        }
    }

    private NumberFormat getPaisFormat(int idPais) {
        switch (idPais) {
            case McConstants.PAIS_ID_MX:
                return McConstants.Companion.getMX_CURR_FORMAT();
            case McConstants.PAIS_ID_CO:
                return McConstants.Companion.getCO_CURR_FORMAT();
            case McConstants.PAIS_ID_PE:
                return McConstants.Companion.getPE_CURR_FORMAT();
            default:
                return McConstants.Companion.getUSA_CURR_FORMAT();
        }
    }

    private String getTotalFormat(int idPais, double total, String currency) {
        return getPaisFormat(idPais).format(total) + " " + currency;
    }

  /*
     ((IViewHolder) holder).shareButton.setOnClickListener(view -> {
       Intent sharingIntent = new Intent(Intent.ACTION_SEND);
       sharingIntent.setType("text/plain");
       sharingIntent.putExtra(Intent.EXTRA_SUBJECT, view.getResources()
           .getString(R.string.txt_wallet_transaction_share_subject, Integer.parseInt(t.getId())));
       sharingIntent.putExtra(Intent.EXTRA_TEXT,
           t.getTicket() + "\n" + currFormat.format(t.getTotal()) + "\n" + t.getDate());
       view.getContext()
           .startActivity(Intent.createChooser(sharingIntent,
               view.getContext().getString(R.string.txt_wallet_transaction_share_selection)));
     });
  */

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof String) return TYPE_HEADER;
        return TYPE_ITEM;
    }

    public Object getItem(int pos) {
        return data.get(pos);
    }

    static class HViewHolder extends RecyclerView.ViewHolder {
        private final TextView header;

        HViewHolder(View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.text_edocuenta_header);
        }
    }

    static class IViewHolder extends RecyclerView.ViewHolder {
        private final TextView totalView;
        private final TextView ticketView;
        private final TextView dateView;
        private final ImageView statusView;

        IViewHolder(View itemView) {
            super(itemView);
            statusView = itemView.findViewById(R.id.img_historial_status);
            totalView = itemView.findViewById(R.id.view_historial_monto);
            ticketView = itemView.findViewById(R.id.view_historial_concepto);
            dateView = itemView.findViewById(R.id.view_historial_fecha);
        }

        void setStatusResource(int status) {
            int res = R.drawable.ic_historial_list_processing;
            if (status == 1) res = R.drawable.ic_historial_list_success;
            if (status == 0) res = R.drawable.ic_historial_list_error;
            statusView.setImageResource(res);
        }
    }
}
