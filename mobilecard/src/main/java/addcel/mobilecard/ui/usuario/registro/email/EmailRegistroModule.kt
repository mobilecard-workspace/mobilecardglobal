package addcel.mobilecard.ui.usuario.registro.email

import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.net.registro.RegistroAPI
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.domain.email.EmailRegistroInteractor
import addcel.mobilecard.domain.email.EmailRegistroInteractorImpl
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-07-24.
 */
@Module
class EmailRegistroModule(val activity: EmailRegistroActivity) {

    @PerActivity
    @Provides
    fun provideModel(): EmailRegistroModel {
        return activity.intent?.getParcelableExtra("model")!!
    }

    @PerActivity
    @Provides
    fun provideApi(retrofit: Retrofit): RegistroAPI {
        return RegistroAPI.get(retrofit)
    }

    @PerActivity
    @Provides
    fun provideInteractor(
            api: RegistroAPI, state: StateSession,
            model: EmailRegistroModel
    ): EmailRegistroInteractor {
        return EmailRegistroInteractorImpl(api, state, model.useCase, CompositeDisposable())
    }

    @PerActivity
    @Provides
    fun providePresenter(
            interactor: EmailRegistroInteractor,
            model: EmailRegistroModel
    ): EmailRegistroPresenter {
        return EmailRegistroPresenterImpl(interactor, model, activity)
    }
}

@PerActivity
@Subcomponent(modules = [EmailRegistroModule::class])
interface EmailRegistroSubcomponent {
    fun inject(activity: EmailRegistroActivity)
}