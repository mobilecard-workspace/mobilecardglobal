package addcel.mobilecard.ui.usuario.wallet.create.pay;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardRequest;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 10/10/17.
 */
class WalletCreatePayPresenter implements WalletCreatePayContract.Presenter {
    private final WalletAPI service;
    private final Usuario usuario;
    private final WalletCreatePayContract.View view;

    WalletCreatePayPresenter(WalletAPI service, Usuario usuario, WalletCreatePayContract.View view) {
        this.service = service;
        this.usuario = usuario;
        this.view = view;
    }

    @Override
    public String getCardHolderName() {
        return StringUtil.removeAcentosMultiple(usuario.getUsrNombre())
                + " "
                + StringUtil.removeAcentosMultiple(usuario.getUsrApellido())
                + " "
                + StringUtil.removeAcentosMultiple(Strings.nullToEmpty(usuario.getUsrMaterno()));
    }

    @SuppressLint("CheckResult")
    @Override
    public void create() {
        view.showProgress();

        CardRequest request =
                new CardRequest(0, usuario.getIdeUsuario(), AddcelCrypto.encryptHard(view.getCard()),
                        AddcelCrypto.encryptHard(view.getVigencia()), AddcelCrypto.encryptHard(view.getCvv()),
                        view.getCardHolderName(), true,
                        view.getAddress1().concat(" ").concat(view.getAddress2()), view.getZipCode(), false,
                        view.getTipoTarjeta(), StringUtil.getCurrentLanguage(), "", "", 0, "");

        service.addTarjeta(BuildConfig.ADDCEL_APP_ID, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cardResponse -> {
                    view.hideProgress();
                    if (cardResponse.getIdError() == 0) {
                        view.showSuccess(cardResponse.getMensajeError());
                        view.onSuccess(cardResponse.getTarjetas().get(0));
                    } else {
                        view.showError(cardResponse.getMensajeError());
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.showError(StringUtil.getNetworkError());
                });
    }
}
