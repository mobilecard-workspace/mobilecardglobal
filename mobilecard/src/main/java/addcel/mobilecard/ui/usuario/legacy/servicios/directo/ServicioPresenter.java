package addcel.mobilecard.ui.usuario.legacy.servicios.directo;

import android.annotation.SuppressLint;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.location.McLocationData;
import addcel.mobilecard.domain.transacto.TransactoInteractor;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 14/12/16.
 */
public final class ServicioPresenter implements ServicioContract.Presenter {
    private final TransactoInteractor interactor;
    private final ServicioContract.View view;

    ServicioPresenter(TransactoInteractor interactor, ServicioContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @SuppressLint("CheckResult")
    @Override
    public void saveToFav(ServicioResponse.ServicioEntity servicio, int logo, String etiqueta) {
        Favorito favorito = new Favorito();
        favorito.setCategoria(Favorito.CAT_SERVICIO);
        favorito.setNombre(servicio.getEmpresa());
        favorito.setPais(interactor.getIdPais());
        favorito.setDescripcion(etiqueta);
        favorito.setLogotipo(logo);
        favorito.setMontos("");
        favorito.setServicio(JsonUtil.toJson(servicio));

        interactor.saveToFavs(favorito, new InteractorCallback<Long>() {
            @Override
            public void onSuccess(@NotNull Long result) {
                if (result > 0) {
                    view.updateFavViewsOnSuccess();
                    view.showSuccess(StringUtil.successOrDefault("", StringUtil.getCurrentLanguage()));
                } else {
                    view.showError(StringUtil.errorOrDefault("", StringUtil.getCurrentLanguage()));
                    view.clear();
                }
            }

            @Override
            public void onError(@NotNull String error) {
                view.showError(StringUtil.errorOrDefault("", StringUtil.getCurrentLanguage()));
                view.clear();
            }
        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void getTipoCambio(ServicioResponse.ServicioEntity model, String referencia,
                              double monto) {
        view.showProgress();
        interactor.getTipoCambio(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<Double>() {
                    @Override
                    public void onSuccess(@NotNull Double result) {
                        view.hideProgress();
                        view.setTipoCambio(model, referencia, monto, result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                        view.clear();
                    }
                });
    }

    @Override
    public McLocationData fetchLocation() {
        return interactor.getLocation();
    }
}
