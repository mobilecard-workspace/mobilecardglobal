package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.base.Strings;
import com.squareup.phrase.Phrase;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.telepeaje.model.Tag;
import addcel.mobilecard.data.net.telepeaje.model.TagsResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.telepeaje.select.PeajeSelectInteractor;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 09/08/17.
 */
class PeajePresenter implements PeajeContract.Presenter {
    private final PeajeSelectInteractor interactor;
    private final NumberFormat numberFormat;
    private final PeajeContract.View view;

    PeajePresenter(PeajeSelectInteractor interactor, NumberFormat numberFormat,
                   PeajeContract.View view) {
        this.interactor = interactor;
        this.numberFormat = numberFormat;
        this.view = view;
    }

    @Override
    public void clearCompositeDisposables() {
        interactor.clearDisposables();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getTags(CatalogoResponse.RecargaEntity model) {
        view.showProgress();
        interactor.list(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), model.getId(),
                new InteractorCallback<List<Tag>>() {
                    @Override
                    public void onSuccess(@NonNull List<Tag> result) {
                        view.hideProgress();
                        view.setTags(result);
                    }

                    @Override
                    public void onError(@NonNull String error) {
                        view.hideProgress();
                        view.notifyNoTags();
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void addTag(CatalogoResponse.RecargaEntity model, String tag, String dv, String alias) {
        view.showProgress();

        int mTipoRecargaTag = interactor.getPeajeProvider(model.getId());

        Tag request = new Tag(Integer.valueOf(dv), alias, tag, mTipoRecargaTag);
        interactor.add(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), model.getId(),
                request, new InteractorCallback<TagsResponse>() {
                    @Override
                    public void onSuccess(@NotNull TagsResponse result) {
                        view.hideProgress();
                        view.setTags(result.getTags());
                        view.showSuccess(result.getMensajeError());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.hideSaldo();
                        view.showError(error);
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void removeTag(CatalogoResponse.RecargaEntity servicio, Tag tag) {
        view.showProgress();

        interactor.delete(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), servicio.getId(),
                tag.getTag(), new InteractorCallback<TagsResponse>() {
                    @Override
                    public void onSuccess(@NotNull TagsResponse result) {
                        view.hideProgress();
                        view.setTags(result.getTags());
                        view.showSuccess(result.getMensajeError());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void saveToFav(CatalogoResponse.RecargaEntity recargaModel,
                          List<CatalogoResponse.MontoEntity> montos, int logo, String etiqueta) {
        Favorito favorito = new Favorito();
        favorito.setCategoria(Favorito.CAT_PEAJE);
        favorito.setNombre(recargaModel.getDescripcion());
        favorito.setPais(interactor.getIdPais());
        favorito.setDescripcion(etiqueta);
        favorito.setLogotipo(logo);
        favorito.setMontos(JsonUtil.toJson(montos));
        favorito.setServicio(JsonUtil.toJson(recargaModel));

        interactor.addToFav(favorito, new InteractorCallback<Long>() {
            @Override
            public void onSuccess(@NotNull Long result) {
                view.hideProgress();
                view.showSuccess(StringUtil.errorOrDefault("", StringUtil.getCurrentLanguage()));
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError(error);
            }
        });
    }

    @Override
    public String buildSaldoText(Phrase phrase, double saldo) {
        return phrase.put("saldo", numberFormat.format(saldo)).format().toString();
    }

    @Override
    public String buildAliasText(Phrase phrase, String tag, String dv) {
        return StringUtil.fromSequence(phrase.put("tag", tag).put("dv", dv).format());
    }

    @Override
    public String[] selectTagAndPin(@Nullable Tag selectedTag, String writtenTag, String pin) {
        String[] arr = new String[2];
        if (Strings.isNullOrEmpty(writtenTag) || Strings.isNullOrEmpty(pin)) {
            if (selectedTag != null) {
                arr[0] = selectedTag.getTag();
                arr[1] = String.valueOf(selectedTag.getDv());
            }
        } else {
            arr[0] = writtenTag;
            arr[1] = pin;
        }
        return arr;
    }

    @Override
    public String[] getTagFromModel(@NonNull Tag selectedTag)
            throws IllegalArgumentException {
        return new String[]{
                Strings.nullToEmpty(selectedTag.getTag()),
                Strings.nullToEmpty(String.valueOf(selectedTag.getDv()))
        };
    }

    @Override
    public String[] getTagFromInput(String tagNumber, String pin) {
        return new String[]{Strings.nullToEmpty(tagNumber), Strings.nullToEmpty(pin)};
    }

    @Override
    public boolean validateTagInput(@Nullable String tag, @Nullable String dv) {
        return !Strings.isNullOrEmpty(tag) && !Strings.isNullOrEmpty(dv);
    }

    @Override
    public void checkSaldoTag(String dv, String etiqueta, String tag,
                              CatalogoResponse.MontoEntity montoModel) {
        view.showProgress();
        Tag request = new Tag(Integer.parseInt(dv), Strings.nullToEmpty(etiqueta), tag,
                Integer.parseInt(montoModel.getEmisor()));

        interactor.checkBalance(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request,
                new InteractorCallback<Double>() {
                    @Override
                    public void onSuccess(@NotNull Double result) {
                        view.hideProgress();
                        view.onSaldoTagChecked(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void checkAndShowSaldo(Tag tag) {
        view.showProgress();
        interactor.checkBalance(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), tag,
                new InteractorCallback<Double>() {
                    @Override
                    public void onSuccess(@NotNull Double result) {
                        view.hideProgress();
                        view.showSaldo(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.hideSaldo();
                        view.showError(error);
                    }
                });
    }
}
