package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.lista

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Producto
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.captura.MmProductosCapturaKeyboardFragment
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.montos.MmProductosMontosFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.screen_producto_list.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-12-18.
 */


interface MmProductosListView : ScreenView {

    fun showRetry()

    fun hideRetry()

    fun clickRetry()

    fun configList(view: View)

    fun launchProductos()

    fun setProductos(productos: List<Producto>)

    fun clickProductos(pos: Int)
}

class MmProductosListFragment : Fragment(), MmProductosListView {

    @Inject
    lateinit var ctActivity: ContainerActivity
    @Inject
    lateinit var adapter: MmProductosListAdapter
    @Inject
    lateinit var presenter: MmProductosListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.mmProductosListSubcomponent(MmProductosListModule(this)).inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_producto_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ctActivity.retry?.findViewById<Button>(R.id.b_retry)?.setOnClickListener {
            clickRetry()
        }
        configList(view)
        launchProductos()
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_productos)
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun showRetry() {
        ctActivity.showRetry()
    }

    override fun hideRetry() {
        ctActivity.hideRetry()
    }

    override fun clickRetry() {
        launchProductos()
    }

    override fun configList(view: View) {
        recycler_productos.adapter = adapter
        recycler_productos.layoutManager = GridLayoutManager(view.context, 2)
        ItemClickSupport.addTo(recycler_productos).setOnItemClickListener { _, pos, _ -> clickProductos(pos) }
    }

    override fun launchProductos() {
        if (adapter.itemCount == 0)
            presenter.getProductos()
    }

    override fun setProductos(productos: List<Producto>) {
        adapter.update(productos)
    }

    override fun clickProductos(pos: Int) {
        val producto = adapter.getItem(pos)

        if (producto.valor.size == 1 && producto.valor[0].valor == "N/A") {
            ctActivity.fragmentManagerLazy.commit {
                add(R.id.frame_colombia_servicios, MmProductosCapturaKeyboardFragment.get(producto, producto.valor[0]))
                hide(this@MmProductosListFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        } else {
            ctActivity.fragmentManagerLazy.commit {
                add(R.id.frame_colombia_servicios, MmProductosMontosFragment.get(producto))
                hide(this@MmProductosListFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }
    }

    override fun showSuccess(msg: String) {
        ctActivity.showSuccess(msg)
    }

    override fun showError(msg: String) {
        ctActivity.showError(msg)
    }

    override fun hideProgress() {
        ctActivity.hideProgress()
    }

    override fun showProgress() {
        ctActivity.showProgress()
    }

}