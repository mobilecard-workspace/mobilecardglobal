package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.lista

import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-12-18.
 */
@Module
class MmProductosListModule(val fragment: MmProductosListFragment) {

    @PerFragment
    @Provides
    fun provideContainerActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }


    @PerFragment
    @Provides
    fun provideAdapter(): MmProductosListAdapter {
        return MmProductosListAdapter()
    }

    @PerFragment
    @Provides
    fun provideMultimarket(r: Retrofit): MultimarketAPI {
        return r.create(MultimarketAPI::class.java)
    }

    @PerFragment
    @Provides
    fun providePresenter(multimarket: MultimarketAPI): MmProductosListPresenter {
        return MmProductosListPresenterImpl(multimarket, CompositeDisposable(), fragment)
    }

}

@PerFragment
@Subcomponent(modules = [MmProductosListModule::class])
interface MmProductosListSubcomponent {
    fun inject(fragment: MmProductosListFragment)
}