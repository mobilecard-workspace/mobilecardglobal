package addcel.mobilecard.ui.usuario.password;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.di.scope.PerActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 22/12/16.
 */
@Module
public final class PasswordModule {
    private final PasswordActivity activity;

    PasswordModule(PasswordActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    Usuario provideUsuario() {
        return Objects.requireNonNull(activity.getIntent().getExtras()).getParcelable("usuario");
    }

    @PerActivity
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(activity);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(activity);
        return validator;
    }

    @PerActivity
    @Provides
    PasswordContract.Presenter providePresenter(SessionOperations session,
                                                UsuariosService service, Usuario usuario) {
        return new PasswordPresenter(activity, session, service, usuario);
    }
}
