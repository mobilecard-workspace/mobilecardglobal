package addcel.mobilecard.ui.usuario.scanpay.secure;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.scanpay.secure.ScanPaySecureInteractor;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;
import timber.log.Timber;

import static addcel.mobilecard.data.net.scanpay.ScanPayService.AUTH_FINISHED;
import static addcel.mobilecard.data.net.scanpay.ScanPayService.ERROR_PREVIO;
import static addcel.mobilecard.data.net.scanpay.ScanPayService.PROCESS_FINISHED;
import static addcel.mobilecard.ui.usuario.scanpay.secure.ScanSecureConstants.FORCED_USER_AGENT;
import static addcel.mobilecard.ui.usuario.scanpay.secure.ScanSecureConstants.FUN_JSON_PROCESS;
import static addcel.mobilecard.ui.usuario.scanpay.secure.ScanSecureConstants.LOG_FUN;

/**
 * ADDCEL on 08/11/17.
 */
@Module
public final class ScanSecureModule {
    private final ScanSecureFragment fragment;

    ScanSecureModule(ScanSecureFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    ScanPayActivity provideActivity() {
        return (ScanPayActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @Provides
    @PerFragment
    boolean provideFromIdBitacora() {
        return Preconditions.checkNotNull(fragment.getArguments()).getBoolean("withIdBitacora");
    }

    @Provides
    @PerFragment
    SPPagoEntity provideRequest() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelable("data");
    }

    @Provides
    @PerFragment
    CookieManager provideManager() {
        return CookieManager.getInstance();
    }

    @PerFragment
    @Provides
    ScanPayService provideService(@Named("ScalarRetrofit") Retrofit retrofit) {
        return retrofit.create(ScanPayService.class);
    }

    @PerFragment
    @Provides
    ScanPaySecureInteractor provideInteractor(ScanPayService service,
                                              SessionOperations session) {
        return new ScanPaySecureInteractor(service, session, new CompositeDisposable());
    }

    @Provides
    @PerFragment
    ScanSecureContract.Presenter providePresenter(
            ScanPaySecureInteractor interactor, boolean withIdBitacora) {
        return new ScanSecurePresenter(interactor, withIdBitacora, fragment) {
        };
    }

    @Provides
    @PerFragment
    WebViewClient provideWebClient() {
        return new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Timber.i("onPageStarted: %s", url);
                fragment.showProgress();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Timber.i("onPageFinished: %s", url);
                fragment.hideProgress();
                if (url.contains(ERROR_PREVIO) || url.contains(PROCESS_FINISHED) || url.contains(
                        AUTH_FINISHED)) {
                    view.evaluateJavascript(FUN_JSON_PROCESS, null);
                }
                view.evaluateJavascript(LOG_FUN, null);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request,
                                            WebResourceResponse errorResponse) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (errorResponse.getStatusCode() != 404 && errorResponse.getStatusCode() != 403) {
                        Timber.d("%s %s", errorResponse.getReasonPhrase(), request.getUrl().toString());
                        fragment.processResult(new SPReceiptEntity(
                                (errorResponse.getStatusCode() == 0 ? -9999 : errorResponse.getStatusCode()),
                                errorResponse.getReasonPhrase()));
                    }
                }
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                Timber.e("Error %d SSL al intentar cargar: %s\n%s",
                        error.getPrimaryError() == 0 ? -9999 : error.getPrimaryError(), error.getUrl(),
                        error.toString());
                if (BuildConfig.DEBUG) handler.proceed();
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request,
                                        WebResourceError error) {
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description,
                                        String failingUrl) {

                SPReceiptEntity entity = new SPReceiptEntity(errorCode == 0 ? -9999 : errorCode,
                        Strings.nullToEmpty(description));

                fragment.processResult(entity);
            }
        };
    }

    @SuppressLint({"SetJavaScriptEnabled", "ClickableViewAccessibility"})
    @Provides
    @PerFragment
    WebView provideBrowser(ScanPayActivity activity, WebViewClient client, CookieManager manager) {
        final WebView browser = new WebView(activity);
        browser.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        browser.requestFocus(View.FOCUS_DOWN);
        browser.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager.setAcceptThirdPartyCookies(browser, true);
            manager.removeAllCookies(null);
            browser.clearCache(true);
        } else {
            manager.removeAllCookie();
        }
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        browser.getSettings().setUserAgentString(FORCED_USER_AGENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        browser.addJavascriptInterface(fragment, "HTMLOUT");

        browser.setWebViewClient(client);
        return browser;
    }
}
