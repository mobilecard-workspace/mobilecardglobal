package addcel.mobilecard.ui.usuario.aci.nodata;

import android.annotation.SuppressLint;
import android.os.Build;

import com.google.common.base.Strings;
import com.squareup.otto.Bus;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.NoDataPagoRequest;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.event.impl.StateReceivedEvent;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 28/03/17.
 */
public final class AciPagoDatosPresenter implements AciPagoDatosContract.Presenter {

    private final AciService aciService;
    private final CatalogoService catalogo;
    private final SessionOperations session;
    private final ServiceModel model;
    private final Address address;
    private final CardEntity formaPago;
    private final String cuenta;
    private final double monto;
    private final Bus bus;
    private final CompositeDisposable disposables;
    private final AciPagoDatosContract.View view;

    AciPagoDatosPresenter(AciService aciService, CatalogoService catalogo, SessionOperations session,
                          ServiceModel model, Address address, CardEntity formaPago, String cuenta, double monto,
                          Bus bus, AciPagoDatosContract.View view) {
        this.aciService = aciService;
        this.catalogo = catalogo;
        this.session = session;
        this.model = model;
        this.address = address;
        this.formaPago = formaPago;
        this.cuenta = cuenta;
        this.monto = monto;
        this.bus = bus;
        this.disposables = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getEstados() {
        disposables.add(catalogo.getEstados(BuildConfig.ADDCEL_APP_ID, session.getUsuario().getIdPais(),
                StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(estadoResponse -> {
                    switch (estadoResponse.getIdError()) {
                        case 0:
                            bus.post(new StateReceivedEvent(estadoResponse.getEstados()));
                            break;
                        default:
                            view.showError(estadoResponse.getMensajeError());
                            break;
                    }
                }, throwable -> view.showError(Strings.nullToEmpty(throwable.getLocalizedMessage()))));
    }

    @Override
    public int getIdEstado() {
        return session.getUsuario().getUsrIdEstado();
    }

    @Override
    public String getCiudad() {
        return Strings.nullToEmpty(session.getUsuario().getUsrCiudad());
    }

    @Override
    public String getAddress() {
        return Strings.nullToEmpty(session.getUsuario().getUsrDireccion());
    }

    @Override
    public String getZipCode() {
        return Strings.nullToEmpty(session.getUsuario().getUsrCp());
    }

    @Override
    public String getSecurityCode() {
        return Strings.nullToEmpty(AddcelCrypto.decryptHard(formaPago.getCodigo()));
    }

    @SuppressLint("CheckResult")
    @Override
    public void pagar(EstadoResponse.EstadoEntity estado, String ciudad, String zipCode,
                      String direccion, String cvv) {
        view.showProgress();
    /*
    new NoDataPagoRequest.Builder(StringUtil.getCurrentLanguage(),
       session.getUsuario().getIdeUsuario(), formaPago.getIdTarjeta())
    */
        final NoDataPagoRequest request = new NoDataPagoRequest.Builder(StringUtil.getCurrentLanguage(),
                session.getUsuario().getIdeUsuario(), formaPago.getIdTarjeta()).setCiudadServicio(
                address.getCity())
                .setCiudadUsuario(ciudad)
                .setCodigo(AddcelCrypto.encryptHard(cvv))
                .setCpUsuario(zipCode)
                .setDireccionServicio(address.getStreet())
                .setDireccionUsuario(StringUtil.removeAcentosMultiple(direccion))
                .setEstadoUsuario(estado.getId())
                .setIdServicio(String.valueOf(model.getId()))
                .setImei(view.getIMEI())
                .setModelo(Build.MODEL)
                .setMonto(monto)
                .setNombreServicio(model.getName())
                .setReferencia(cuenta)
                .setSoftware(String.valueOf(Build.VERSION.SDK_INT))
                .setWkey(view.getIMEI())
                .build();

        disposables.add(aciService.paymentNotData(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mcResponse -> {
                    view.hideProgress();
                    bus.post(mcResponse);
                }, throwable -> {
                    view.hideProgress();
                    view.showError(Strings.nullToEmpty(throwable.getLocalizedMessage()));
                }));
    }
}
