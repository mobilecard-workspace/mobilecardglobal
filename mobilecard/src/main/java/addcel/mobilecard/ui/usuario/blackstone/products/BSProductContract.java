package addcel.mobilecard.ui.usuario.blackstone.products;

import androidx.annotation.NonNull;

import java.util.List;

import addcel.mobilecard.data.net.blackstone.model.BSCarrierEntity;
import addcel.mobilecard.data.net.blackstone.model.BSProductEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 09/07/18.
 */
public interface BSProductContract {
    interface View extends ScreenView {
        void setProducts(@NonNull List<BSProductEntity> products);

        void onProductClicked(int pos);
    }

    interface Presenter {
        void clearDisposables();

        BSCarrierEntity getCarrier();

        void getProducts();
    }
}
