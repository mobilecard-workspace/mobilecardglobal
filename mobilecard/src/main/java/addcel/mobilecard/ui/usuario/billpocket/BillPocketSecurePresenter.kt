package addcel.mobilecard.ui.usuario.billpocket

import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.secure.WebInteractor

/**
 * ADDCEL on 2019-09-09.
 */
interface BillPocketSecurePresenter {
    fun clearDisposables()

    fun getCardDataForPeaje(currentUrl: String, formUrl: String)

    fun processUrl(url: String, data: ByteArray, auth: String)
}

class BillPocketSecurePresenterImpl(val interactor: WebInteractor, val view: BillPocketSecureView) :
        BillPocketSecurePresenter {
    override fun clearDisposables() {
        interactor.compositeDisposable.clear()
    }

    override fun getCardDataForPeaje(currentUrl: String, formUrl: String) {
        if (currentUrl.contains(formUrl)) view.setCardData()
    }

    override fun processUrl(url: String, data: ByteArray, auth: String) {
        view.showProgress()
        val headers = mapOf("Authorization" to auth)
        interactor.processUrl(url, data, headers, object : InteractorCallback<String> {
            override fun onSuccess(result: String) {
                view.hideProgress()
                view.onUrlProcessed(url, result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
            }
        })
    }
}