package addcel.mobilecard.ui.usuario.legacy;

import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.ui.usuario.legacy.recargas.CarrierSelectionModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.CarrierSelectionSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.RecargaModule;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.RecargaSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.servicios.consulta.ServicioConsultaModule;
import addcel.mobilecard.ui.usuario.legacy.servicios.consulta.ServicioConsultaSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.servicios.directo.ServicioModule;
import addcel.mobilecard.ui.usuario.legacy.servicios.directo.ServicioSubcomponent;
import addcel.mobilecard.ui.usuario.legacy.servicios.list.ServicioSelectionModule;
import addcel.mobilecard.ui.usuario.legacy.servicios.list.ServicioSelectionSubcomponent;
import dagger.Subcomponent;

/**
 * ADDCEL on 05/12/16.
 */

@PerActivity
@Subcomponent(modules = PagoContainerModule.class)
public interface PagoContainerSubcomponent {
    void inject(PagoContainerActivity activity);

    CarrierSelectionSubcomponent carrierSelectionSubcomponent(CarrierSelectionModule module);

    ServicioSelectionSubcomponent servicioSelectionSubcomponent(ServicioSelectionModule module);

    ServicioSubcomponent servicioSubcomponent(ServicioModule module);

    ServicioConsultaSubcomponent servicioConsultaSubcomponent(ServicioConsultaModule module);

    RecargaSubcomponent recargaSubcomponent(RecargaModule module);
}
