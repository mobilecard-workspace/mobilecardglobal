package addcel.mobilecard.ui.usuario.legacy.servicios.directo;

import androidx.annotation.NonNull;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.domain.location.McLocationData;
import addcel.mobilecard.ui.usuario.legacy.servicios.HideTutorialEvent;
import addcel.mobilecard.utils.ActivityResultEvent;

/**
 * ADDCEL on 14/12/16.
 */
public interface ServicioContract {
    interface View extends Validator.ValidationListener {

        void showProgress();

        void hideProgress();

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);

        String getImei();

        void scanReferencia();

        void cambiaReferencia(CharSequence seq);

        void postReferencia(ActivityResultEvent event);

        void showReciboImage();

        void clickFav();

        void clickSaveFav();

        void updateFavViewsOnSuccess();

        void setFav(boolean fav);

        void setInitialFav(boolean fav);

        void hideReciboImage(HideTutorialEvent event);

        void cambiaCantidad(CharSequence seq);

        void launchPago();

        void clear();

        void showSuccess(int resId);

        void setTipoCambio(ServicioResponse.ServicioEntity model, String referencia, double monto,
                           double tipoCambio);
    }

    interface Presenter {

        void clearDisposables();

        void saveToFav(ServicioResponse.ServicioEntity servicio, int logo, String etiqueta);

        void getTipoCambio(ServicioResponse.ServicioEntity model, String referencia, double monto);

        McLocationData fetchLocation();
    }
}
