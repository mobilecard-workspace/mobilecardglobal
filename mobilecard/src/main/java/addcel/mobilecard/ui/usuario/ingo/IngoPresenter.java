package addcel.mobilecard.ui.usuario.ingo;

import android.annotation.SuppressLint;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.ingo.IngoInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 19/07/18.
 */
class IngoPresenter implements IngoContract.Presenter {
    private IngoInteractor interactor;
    private IngoContract.View view;

    IngoPresenter(IngoInteractor interactor, IngoContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void onDestroy() {
        interactor = null;
        view = null;
    }

    @Override
    public IngoService getService() {
        return interactor.getService();
    }

    @Override
    public IngoAliveAPI getAliveApi() {
        return interactor.getAliveApi();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getUserData() {
        view.showProgress();

        interactor.checkIngoData(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                view.getImei(), new InteractorCallback<IngoUserData>() {
                    @Override
                    public void onSuccess(@NotNull IngoUserData result) {
                        view.hideProgress();
                        if (result.getErrorCode() == 0) {
                            view.isRegistered(result);
                        } else {
                            view.notRegistered(result.getErrorMessage());
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                        view.kill();
                    }
                });
    }

    @Override
    public void checkIfAvailable() {
        view.showProgress();
        interactor.checkIngoAlive(new InteractorCallback<IngoAliveResponse>() {
            @Override
            public void onSuccess(@NotNull IngoAliveResponse result) {
                view.hideProgress();
                if (result.getServiceAvailable()) {
                    view.onIngoAvailable();
                } else {
                    view.showMessageNotAvailable();
                }
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showMessageNotAvailable();
            }
        });
    }
}
