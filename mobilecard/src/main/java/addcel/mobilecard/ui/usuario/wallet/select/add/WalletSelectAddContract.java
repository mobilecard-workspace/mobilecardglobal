package addcel.mobilecard.ui.usuario.wallet.select.add;

import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectContract;

/**
 * ADDCEL on 10/08/18.
 */
public interface WalletSelectAddContract {
    interface View extends WalletSelectContract.View {
        void clickAdd();
    }

    interface Presenter extends WalletSelectContract.Presenter {
        void getDebitCards();
    }
}
