package addcel.mobilecard.ui.usuario.registro.formapago;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Calendar;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog;
import addcel.mobilecard.ui.usuario.registro.RegistroActivity2;
import dagger.Module;
import dagger.Provides;

@Module
public final class RegistroFormaPagoModule {
    private final RegistroFormaPagoFragment fragment;

    RegistroFormaPagoModule(RegistroFormaPagoFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    RegistroActivity2 provideActivity2() {
        return (RegistroActivity2) Preconditions.checkNotNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    AlertDialog provideInfoDialog(RegistroActivity2 activity2) {
        return new AlertDialog.Builder(activity2).setTitle(android.R.string.dialog_alert_title)
                .setMessage(R.string.info_registro_2)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        dialog1.dismiss();
                    }
                })
                .create();
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    @Named("email")
    String provideEmail() {
        return Preconditions.checkNotNull(fragment.getArguments()).getString("email");
    }

    @PerFragment
    @Provides
    @Named("password")
    String providePassword() {
        return Preconditions.checkNotNull(fragment.getArguments()).getString("password");
    }

    @PerFragment
    @Provides
    @Named("nombre")
    String provideNombre() {
        return Preconditions.checkNotNull(fragment.getArguments()).getString("nombre");
    }

    @PerFragment
    @Provides
    @Named("apellido")
    String provideApellido() {
        return Preconditions.checkNotNull(fragment.getArguments()).getString("apellido");
    }

    @PerFragment
    @Provides
    @Named("celular")
    String providecelular() {
        return Preconditions.checkNotNull(fragment.getArguments()).getString("celular");
    }

    @PerFragment
    @Provides
    @Named("pais")
    int providePais() {
        return Preconditions.checkNotNull(fragment.getArguments()).getInt("pais");
    }

    @PerFragment
    @Provides
    @Named("cedula")
    String provideCedula() {
        return Preconditions.checkNotNull(fragment.getArguments()).getString("cedula");
    }

    @PerFragment
    @Provides
    @Named("tipoCedula")
    int provideTipoCedula() {
        return Preconditions.checkNotNull(fragment.getArguments()).getInt("tipoCedula");
    }

    @PerFragment
    @Provides
    DatePickerDialog datePickerDialog(RegistroActivity2 activity2) {
        DatePickerDialog datePickerDialog;
        if (Build.VERSION.SDK_INT == 24) {
            datePickerDialog = new FixedHoloDatePickerDialog(activity2, fragment,
                    Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        } else {
            if (Build.VERSION.SDK_INT >= 21) {
                datePickerDialog =
                        new DatePickerDialog(activity2, R.style.CustomDatePickerDialogTheme, fragment,
                                Calendar.getInstance().get(Calendar.YEAR),
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            } else {
                // R.style.CustomDatePickerDialogTheme
                datePickerDialog =
                        new DatePickerDialog(activity2, fragment, Calendar.getInstance().get(Calendar.YEAR),
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            }
        }

        datePickerDialog.getDatePicker()
                .findViewById(Resources.getSystem().getIdentifier("day", "id", "android"))
                .setVisibility(View.GONE);
        return datePickerDialog;
    }

    @PerFragment
    @Provides
    RegistroFormaPagoContract.Presenter providePresenter(
            UsuariosService mobilecardService, @Named("email") String email,
            @Named("password") String password, @Named("nombre") String nombre,
            @Named("apellido") String apellido, @Named("celular") String celular, @Named("pais") int pais,
            @Named("cedula") String cedula, @Named("tipoCedula") int tipoCedula) {
        return new RegistroFormaPagoPresenter(mobilecardService, email, password, nombre, apellido,
                celular, tipoCedula, cedula, pais, fragment);
    }
}
