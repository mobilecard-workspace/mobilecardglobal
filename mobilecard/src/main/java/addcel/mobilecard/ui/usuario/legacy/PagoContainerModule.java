package addcel.mobilecard.ui.usuario.legacy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;

import androidx.fragment.app.FragmentManager;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.domain.location.McLocationData;
import addcel.mobilecard.utils.JsonUtil;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 05/12/16.
 */

@Module
public final class PagoContainerModule {

    private final PagoContainerActivity activity;

    PagoContainerModule(PagoContainerActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    ProgressDialog provideProgress() {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    @PerActivity
    @Provides
    Usuario provideUsuario(SharedPreferences preferences) {
        return JsonUtil.fromJson(preferences.getString("usuario", ""), Usuario.class);
    }

    @PerActivity
    @Provides
    McLocationData provideLocation(SessionOperations session) {
        return session.getMinimalLocationData();
    }

    @PerActivity
    @Provides
    String provideTipo() {
        return activity.getIntent().getStringExtra("tipo");
    }

    @PerActivity
    @Provides
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @PerActivity
    @Provides
    AlertDialog.Builder provideDialogBuilder() {
        return new AlertDialog.Builder(activity);
    }
}
