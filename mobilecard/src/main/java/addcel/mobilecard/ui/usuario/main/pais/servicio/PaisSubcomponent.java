package addcel.mobilecard.ui.usuario.main.pais.servicio;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 11/10/17.
 */
@PerFragment
@Subcomponent(modules = PaisModule.class)
public interface PaisSubcomponent {
    void inject(PaisFragment fragment);
}
