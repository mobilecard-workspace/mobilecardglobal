package addcel.mobilecard.ui.usuario.scanpay.result.qr;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.View;

import com.google.common.base.Strings;
import com.squareup.phrase.Phrase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.Calendar;

import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.QRUtil;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 11/09/18.
 */
class ScanResultQrPresenter implements ScanResultQrContract.Presenter {
    private final SPReceiptEntity result;
    private final NumberFormat currFormat;
    private final CompositeDisposable compositeDisposable;
    private final ScanResultQrContract.View view;

    ScanResultQrPresenter(SPReceiptEntity result, NumberFormat currFormat,
                          ScanResultQrContract.View view) {
        this.result = result;
        this.currFormat = currFormat;
        this.compositeDisposable = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearCompositeDisposable() {
        compositeDisposable.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void createQrCode(int sideLen, int fgColor, int bgColor) {

        Single<Bitmap> s =
                QRUtil.Companion.generateQRCodeSingle(JsonUtil.toJson(result), sideLen, sideLen, fgColor,
                        bgColor);

        compositeDisposable.add(s.subscribe(view::setQrCode,
                throwable -> view.showError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.QR))));
    }

    @Override
    public Bitmap getBitmapOfRootView(View view) {
        View rootview = view.getRootView();
        rootview.setDrawingCacheEnabled(true);
        return rootview.getDrawingCache();
    }

    @Override
    public String buildScreenShotName() {
        String fecha = DateFormat.format("yyyyMMddhhmmss", Calendar.getInstance()).toString();
        return "scanpay_" + fecha + ".png";
    }

    @Override
    public void createImage(Bitmap bm) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        File file = new File(Environment.getExternalStorageDirectory() + "/sc_transfer.jpg");
        try {
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(bytes.toByteArray());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public CharSequence buildMsg(String base) {
        return Phrase.from(base)
                .put("mensaje", Strings.nullToEmpty(result.getTxnISOCode()))
                .put("idbitacora", String.valueOf(result.getIdTransaccion()))
                .put("auth", Strings.nullToEmpty(result.getAuthNumber()))
                .put("tdc", Strings.nullToEmpty(result.getMaskedPAN()))
                .put("date", DateFormat.format("dd-MM-yyyy HH:mm:ss", result.getDateTime()))
                .put("monto", currFormat.format(result.getAmount()))
                .format();
    }
}
