package addcel.mobilecard.ui.usuario.viamericas;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;
import com.squareup.phrase.Phrase;

import java.text.NumberFormat;
import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.viamericas.model.CurrencyModel;
import addcel.mobilecard.data.net.viamericas.model.ExchangeRateModel;
import addcel.mobilecard.data.net.viamericas.model.OrderFeeModel;

/**
 * ADDCEL on 25/05/17.
 */

public final class ViamericasTransactionElementAdapter
        extends RecyclerView.Adapter<ViamericasTransactionElementAdapter.ViewHolder> {

    private static final int[] NAMES = {
            R.string.txt_viamericas_confirm_element_exchange, R.string.txt_viamericas_confirm_element_fee,
            R.string.txt_viamericas_confirm_element_tax, R.string.txt_viamericas_confirm_element_total
    };

    private final List<String> montos;

    public ViamericasTransactionElementAdapter() {
        montos = Lists.newArrayList();
    }

    public ViamericasTransactionElementAdapter(double sendAmount, OrderFeeModel fees,
                                               List<CurrencyModel> currencies, ExchangeRateModel exchangeRate, NumberFormat format) {
        double totalFee = Double.parseDouble(fees.getFundingFee());
        double stateTax = Double.parseDouble(fees.getStateTax());
        double total = sendAmount + totalFee + stateTax;
        montos = Lists.newArrayList(buildExchangeRateText(exchangeRate, currencies, format),
                buildAmountText(totalFee, currencies, format),
                buildAmountText(stateTax, currencies, format), buildAmountText(total, currencies, format));
    }

    public void update(double sendAmount, OrderFeeModel fees, List<CurrencyModel> currencies,
                       ExchangeRateModel exchangeRate, NumberFormat format) {
        double totalFee = Double.parseDouble(fees.getFundingFee());
        double stateTax = Double.parseDouble(fees.getStateTax());
        double total = sendAmount + totalFee + stateTax;
        montos.clear();
        montos.addAll(Lists.newArrayList(buildExchangeRateText(exchangeRate, currencies, format),
                buildAmountText(totalFee, currencies, format),
                buildAmountText(stateTax, currencies, format), buildAmountText(total, currencies, format)));
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_viamericas_transaction_element, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.nameView.setText(NAMES[position]);
        holder.valueView.setText(montos.get(position));
    }

    @Override
    public int getItemCount() {
        return montos.size();
    }

    private String buildExchangeRateText(ExchangeRateModel rate, List<CurrencyModel> currencies,
                                         NumberFormat numberFormat) {
        CharSequence currText = Phrase.from("{s_amount} {s_curr} -> {r_amount} {r_curr}")
                .put("s_amount", numberFormat.format(1))
                .put("s_curr", currencies.get(0).getNAME())
                .put("r_amount", numberFormat.format(rate.getExchangeRate()))
                .put("r_curr", currencies.get(currencies.size() - 1).getNAME())
                .format();
        return currText.toString();
    }

    private String buildAmountText(double amount, List<CurrencyModel> currencies,
                                   NumberFormat numberFormat) {
        final String formatTotal = numberFormat.format(amount);
        return Phrase.from("{amount} {curr}")
                .put("amount", formatTotal)
                .put("curr", currencies.get(0).getNAME())
                .format()
                .toString();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView nameView;
        private final TextView valueView;

        ViewHolder(View itemView) {
            super(itemView);
            nameView = itemView.findViewById(R.id.view_transaction_element_name);
            valueView = itemView.findViewById(R.id.view_transaction_element_value);
        }
    }
}
