package addcel.mobilecard.ui.usuario.colombia.recargas.datos.confirm

import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.di.scope.PerFragment
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 9/25/19.
 */
@Module
class ColombiaRecargaPaquetesConfirmModule(
        val fragment: ColombiaRecargaPaquetesConfirmFragment
) {

    @PerFragment
    @Provides
    fun provideMiddleware(r: Retrofit): ColombiaMiddleware {
        return ColombiaMiddleware.get(r)
    }

    @PerFragment
    @Provides
    fun provideTokenizer(r: Retrofit): TokenizerAPI {
        return TokenizerAPI.provideTokenizerAPI(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            middleware: ColombiaMiddleware, tokenizerAPI: TokenizerAPI
    ): ColombiaRecargaPaquetesConfirmPresenter {
        return ColombiaRecargaPaquetesConfirmPresenterImpl(
                middleware, tokenizerAPI,
                CompositeDisposable(),
                fragment
        )
    }
}

@PerFragment
@Subcomponent(modules = [ColombiaRecargaPaquetesConfirmModule::class])
interface ColombiaRecargaPaquetesConfirmSubcomponent {
    fun inject(fragment: ColombiaRecargaPaquetesConfirmFragment)
}
