package addcel.mobilecard.ui.usuario.registro.info;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.usuario.registro.RegistroInteractor;
import addcel.mobilecard.domain.usuario.registro.RegistroInteractorImpl;
import addcel.mobilecard.ui.usuario.registro.RegistroActivity2;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 28/09/17.
 */
@Module
public final class RegistroPersonalInfoModule {
    private final RegistroPersonalInfoFragment fragment;

    RegistroPersonalInfoModule(RegistroPersonalInfoFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    RegistroActivity2 provideActivity2() {
        return (RegistroActivity2) fragment.getActivity();
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    RegistroInteractor provideInteractor(UsuariosService service, SessionOperations repository,
                                         StateSession state) {
        return new RegistroInteractorImpl(service, repository, state, new CompositeDisposable());
    }

    @PerFragment
    @Provides
    Presenter providePresenter(RegistroInteractor interactor) {
        return new RegistroPersonalInfoPresenter(interactor, fragment);
    }
}
