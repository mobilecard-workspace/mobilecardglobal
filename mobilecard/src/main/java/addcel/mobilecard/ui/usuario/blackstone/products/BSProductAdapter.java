package addcel.mobilecard.ui.usuario.blackstone.products;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.blackstone.model.BSProductEntity;
import addcel.mobilecard.utils.ListUtil;

/**
 * ADDCEL on 09/07/18.
 */
public final class BSProductAdapter extends RecyclerView.Adapter<BSProductAdapter.ViewHolder> {
    private final List<BSProductEntity> mProducts;

    BSProductAdapter() {
        this.mProducts = Lists.newArrayList();
    }

    public void update(List<BSProductEntity> products) {
        if (ListUtil.notEmpty(mProducts)) mProducts.clear();
        mProducts.addAll(products);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_servicio, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.text.setText(mProducts.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    public BSProductEntity getItem(int pos) {
        return mProducts.get(pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView text;
        private final ImageView logo;

        ViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text_servicio);
            logo = itemView.findViewById(R.id.img_servicio_logo);
            init();
        }

        private void init() {
            logo.setImageResource(R.drawable.recargas);
        }
    }
}
