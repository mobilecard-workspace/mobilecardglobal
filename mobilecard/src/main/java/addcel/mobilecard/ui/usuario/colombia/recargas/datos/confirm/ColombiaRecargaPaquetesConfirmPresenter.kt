package addcel.mobilecard.ui.usuario.colombia.recargas.datos.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.colombia.PaqueteRequest
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 9/25/19.
 */
interface ColombiaRecargaPaquetesConfirmPresenter {
    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun isOnWhiteList(idUsuario: Long, idPais: Int)

    fun launchPago(request: PaqueteRequest)
}

class ColombiaRecargaPaquetesConfirmPresenterImpl(
        val middleware: ColombiaMiddleware, val token: TokenizerAPI,
        val disposables: CompositeDisposable, val view: ColombiaRecargaPaquetesConfirmView
) :
        ColombiaRecargaPaquetesConfirmPresenter {
    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun isOnWhiteList(idUsuario: Long, idPais: Int) {
        view.showProgress()
        val wDisp = token.isOnWhiteList(
                BuildConfig.ADDCEL_APP_ID, idPais, StringUtil.getCurrentLanguage(), idUsuario
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.code == 0) {
                        view.onWhiteList()
                    } else {
                        view.enablePurchaseButton()
                        view.hideProgress()
                        view.notOnWhiteList()
                    }
                }, {
                    view.enablePurchaseButton()
                    view.hideProgress()
                    view.notOnWhiteList()
                })
        addToDisposables(wDisp)
    }

    override fun launchPago(request: PaqueteRequest) {
        view.showProgress()
        val pagoDisp =
                middleware.paquete(ColombiaMiddleware.getAuth(), request).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                            view.hideProgress()
                            view.onPagoSuccess(SPReceiptEntity(r.codigo, r.mensaje))
                        }, { t ->
                            view.hideProgress()
                            view.enablePurchaseButton()
                            view.showError(t.localizedMessage
                                    ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                        })

        addToDisposables(pagoDisp)
    }
}