package addcel.mobilecard.ui.usuario.registro.sms

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.registro.SmsActivationEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.login.LoginActivity
import addcel.mobilecard.ui.login.LoginContract
import addcel.mobilecard.ui.login.tipo.LoginTipoContract
import addcel.mobilecard.ui.negocio.mx.registro.NegocioRegistroActivity
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeActivity
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeModel
import addcel.mobilecard.ui.usuario.registro.RegistroActivity2
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.DeviceUtil
import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Parcelable
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_sms_registro.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface SmsRegistroView : ScreenView, TextWatcher {
    fun launchMsg(phoneNumber: String)
    fun onMsg(result: SmsActivationEntity)
    fun onPreviouslyValidated(phone: String)
    fun launchStorage(phone: String)
    fun launchVerification(code: String)
    fun onVerification(result: SmsActivationEntity)
}

@Parcelize
data class SmsRegistroModel(val pais: PaisResponse.PaisEntity, val useCase: Int) :
        Parcelable

class SmsRegistroActivity : AppCompatActivity(), SmsRegistroView {

    companion object {
        const val USE_CASE_USUARIO = 1
        const val USE_CASE_NEGOCIO = 2

        fun get(context: Context, model: SmsRegistroModel): Intent {
            return Intent(context, SmsRegistroActivity::class.java).putExtra("model", model)
        }
    }

    @Inject
    lateinit var presenter: SmsRegistroPresenter
    lateinit var permissions: RxPermissions
    val compositeDisposable = CompositeDisposable()
    lateinit var timer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.smsRegistroSubcomponent(SmsRegistroModule(this)).inject(this)

        setContentView(R.layout.activity_sms_registro)

        permissions = RxPermissions(this)

        pinview_registro_sms_code.isEnabled = false

        if (presenter.requires9Digits()) {
            til_registro_sms_phone.hint = "Ingresa tu número a 9 dígitos"
            til_registro_sms_phone.counterMaxLength = 9
            AndroidUtils.getEditText(til_registro_sms_phone).filters =
                    arrayOf(InputFilter.LengthFilter(9))
        }

        AndroidUtils.getEditText(til_registro_sms_phone).addTextChangedListener(this)

        pinview_registro_sms_code.setOnCompleteListener { completed, pinResults ->
            if (pinResults != null) {
                timer.cancel()
                if (completed) {
                    launchVerification(pinResults)
                }
            }
        }

        b_registro_sms_reenviar.setOnClickListener {
            b_registro_sms_reenviar.isEnabled = false
            launchMsg(AndroidUtils.getText(til_registro_sms_phone.editText))
        }

        compositeDisposable.add(
                permissions.request(Manifest.permission.READ_PHONE_STATE).subscribe { accept ->
                    if (accept) {
                        val capturedPhone = presenter.getStoredPhone()
                        if (capturedPhone.isNotEmpty()) {
                            launchOnPhoneCaptured(capturedPhone)
                        }
                    } else {
                        showError(getString(R.string.txt_ingo_permission_retry))
                    }
                })
    }


    private fun launchOnPhoneCaptured(phone: String) {
        AlertDialog.Builder(this).setTitle("Información")
                .setMessage("Se inición un proceso de registro con el número $phone.\n¿Deseas continuar?")
                .setPositiveButton("Continuar") { d, _ ->
                    AndroidUtils.getEditText(til_registro_sms_phone).setText(phone)
                    launchMsg(phone)
                    d.dismiss()
                }
                .setNegativeButton("Cancelar") { d, _ ->
                    presenter.storePhone("")
                    AndroidUtils.getEditText(til_registro_sms_phone).setText("")
                    d.dismiss()
                }.show()
    }

    override fun onRestart() {
        super.onRestart()
        val capturedPhone = presenter.getStoredPhone()
        if (capturedPhone.isNotEmpty()) {
            launchOnPhoneCaptured(capturedPhone)
        }
    }


    override fun onDestroy() {
        compositeDisposable.clear()
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun onBackPressed() {
        val perfil =
                if (presenter.fetchModel().useCase == USE_CASE_USUARIO) LoginTipoContract.Tipo.USUARIO else LoginTipoContract.Tipo.NEGOCIO

        startActivity(
                LoginActivity.get(
                        this,
                        LoginContract.UseCase.INICIO, presenter.fetchModel().pais
                        , perfil
                ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        )
        finish()
    }

    override fun launchMsg(phoneNumber: String) {
        presenter.sendSms(phoneNumber, DeviceUtil.getDeviceId(this@SmsRegistroActivity))
    }

    override fun onMsg(result: SmsActivationEntity) {
        img_sms_registro_placeholder.visibility = View.GONE
        pinview_registro_sms_code.isEnabled = true
        b_registro_sms_reenviar.isEnabled = false
        counter_registro_sms_retry.visibility = View.VISIBLE

        timer =
                object : CountDownTimer(TimeUnit.SECONDS.toMillis(10), TimeUnit.SECONDS.toMillis(1)) {
                    override fun onFinish() {
                        counter_registro_sms_retry.visibility = View.GONE
                        counter_registro_sms_retry.text = 10.toString()
                        b_registro_sms_reenviar.isEnabled = true
                    }

                    override fun onTick(p0: Long) {
                        counter_registro_sms_retry.text = TimeUnit.MILLISECONDS.toSeconds(p0).toString()
                    }
                }

        timer.start()
    }

    override fun onPreviouslyValidated(phone: String) {

        if (presenter.fetchModel().useCase == USE_CASE_USUARIO) {
            startActivity(RegistroActivity2.get(this, presenter.fetchModel().pais, phone, ""))
        } else {
            if (presenter.fetchModel().pais.id == PaisResponse.PaisEntity.MX) {
                startActivity(NegocioRegistroActivity.get(this, phone, ""))
            } else {
                val model = NegocioRegistroPeModel(phone, "")
                startActivity(NegocioRegistroPeActivity.get(this, model))
            }
        }

        finish()
    }

    override fun launchStorage(phone: String) {
        presenter.storePhone(phone)
    }

    override fun launchVerification(code: String) {
        presenter.validateSms(
                code, AndroidUtils.getText(til_registro_sms_phone.editText),
                DeviceUtil.getDeviceId(this)
        )
    }

    override fun onVerification(result: SmsActivationEntity) {

        val phone = result.data?.phoneNumber!!

        /* val emailModel =
           EmailRegistroModel(
             presenter.fetchModel().pais,
             presenter.fetchModel().useCase,
             phone,
             -1
           )

         startActivity(EmailRegistroActivity.get(this, emailModel))*/

        if (presenter.fetchModel().useCase == USE_CASE_USUARIO) {
            startActivity(RegistroActivity2.get(this, presenter.fetchModel().pais, phone, ""))
        } else {
            if (presenter.fetchModel().pais.id == PaisResponse.PaisEntity.MX) {
                startActivity(NegocioRegistroActivity.get(this, phone, ""))
            } else {

                val model = NegocioRegistroPeModel(phone, "")
                startActivity(NegocioRegistroPeActivity.get(this, model))
            }
        }

        finish()
    }

    override fun showProgress() {
        progress_registro_sms.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_registro_sms.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (til_registro_sms_phone.hasFocus()) {
                if (p0.length == til_registro_sms_phone.counterMaxLength) {
                    til_registro_sms_phone.error = null
                    launchMsg(p0.toString())
                } else {
                    til_registro_sms_phone.error = getText(R.string.error_phone)
                }
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}
