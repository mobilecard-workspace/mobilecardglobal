package addcel.mobilecard.ui.usuario.registro.email

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.registro.EmailStatusEntity
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.email.EmailRegistroInteractor
import addcel.mobilecard.utils.StringUtil

/**
 * ADDCEL on 2019-07-24.
 */
interface EmailRegistroPresenter {

    fun getStoredEmail(): String

    fun storeEmail(email: String)

    fun fetchModel(): EmailRegistroModel

    fun sendVerificationLink(email: String, phone: String, imei: String)

    fun checkIfEmailVerified(id: Long, email: String)
}

class EmailRegistroPresenterImpl(
        val interactor: EmailRegistroInteractor,
        val model: EmailRegistroModel, val view: EmailRegistroView
) : EmailRegistroPresenter {
    override fun storeEmail(email: String) {
        interactor.storeEmail(email)
    }

    override fun getStoredEmail(): String {
        return interactor.fetchEmail()
    }

    override fun fetchModel(): EmailRegistroModel {
        return model
    }

    override fun sendVerificationLink(email: String, phone: String, imei: String) {
        view.showProgress()
        interactor.sendVerificationLink(BuildConfig.ADDCEL_APP_ID, model.pais.id,
                StringUtil.getCurrentLanguage(), imei, model.phone, email,
                object : InteractorCallback<EmailStatusEntity> {
                    override fun onSuccess(result: EmailStatusEntity) {
                        view.hideProgress()
                        when (result.idError) {
                            0 -> view.onVerificar(result)
                            2 -> view.onPreviouslyValidated(email, result)
                            else -> {
                                model.emailId = -1
                                view.showError(result.mensajeError)
                            }
                        }
                    }

                    override fun onError(error: String) {
                        model.emailId = -1
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun checkIfEmailVerified(id: Long, email: String) {
        interactor.checkIfEmailVerified(BuildConfig.ADDCEL_APP_ID, model.pais.id,
                StringUtil.getCurrentLanguage(), id, object : InteractorCallback<EmailStatusEntity> {
            override fun onSuccess(result: EmailStatusEntity) {
                view.hideProgress()
                if (result.idError == 0) {
                    interactor.storeEmail(email)
                    view.onContinuar(result)
                } else {
                    view.showError(result.mensajeError)
                }
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
            }
        })
    }
}