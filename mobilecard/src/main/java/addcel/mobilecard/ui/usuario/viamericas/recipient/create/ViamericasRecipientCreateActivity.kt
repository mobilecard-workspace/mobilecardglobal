package addcel.mobilecard.ui.usuario.viamericas.recipient.create

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.viamericas.VmActivity
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.StringUtil
import addcel.mobilecard.validation.ValidationUtils
import addcel.mobilecard.validation.annotation.Celular
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import butterknife.*
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Select
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_viamericas_recipient_create.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject
import javax.inject.Named


@Parcelize
data class ViamericasRecipientCreateModel(val idSender: String, val pais: VmCountryModel) :
        Parcelable


interface ViamericasRecipientCreateView : ScreenView, Validator.ValidationListener,
        DatePickerDialog.OnDateSetListener {
    fun postStates(states: List<VmStateModel>)

    fun postCities(cities: List<VmCityModel>)

    fun postRecipientResponse(recipient: RecipientResponse)

    fun clickInfo()

    fun onStateSelected(pos: Int)

    fun showDateDialog()

    fun clickCreateOrder()
}

class ViamericasRecipientCreateActivity : AppCompatActivity(), ViamericasRecipientCreateView {

    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_viamericas_recipient_firstname)
    lateinit var firstNameTil: TextInputLayout
    @BindView(R.id.til_viamericas_recipient_middlename)
    lateinit var middleNameTil: TextInputLayout
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_viamericas_recipient_lastname)
    lateinit var lastNameTil: TextInputLayout
    @BindView(R.id.til_viamericas_recipient_second)
    lateinit var maternoTil: TextInputLayout
    @Celular
    @BindView(R.id.til_viamericas_recipient_celular)
    lateinit var celularTil: TextInputLayout
    @BindView(R.id.til_viamericas_recipient_email)
    lateinit var emailTil: TextInputLayout
    @BindView(R.id.text_viamericas_recipient_date)
    lateinit var dateText: EditText
    @BindView(R.id.b_viamericas_recipient_date)
    lateinit var dateButton: Button
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_viamericas_recipient_address)
    lateinit var addressTil: TextInputLayout
    @BindView(R.id.til_viamericas_recipient_address_two)
    lateinit var addressTwoTil: TextInputLayout
    @BindView(R.id.text_viamericas_recipient_pais)
    lateinit var paisText: EditText
    @Select(messageResId = R.string.error_viamericas_estado)
    @BindView(R.id.spinner_viamericas_recipient_state)
    lateinit var estadoSpinner: Spinner
    @Select(messageResId = R.string.error_viamericas_ciudad)
    @BindView(R.id.spinner_viamericas_recipient_city)
    lateinit var citySpinner: Spinner
    @BindView(R.id.til_viamericas_recipient_cp)
    lateinit var zipTil: TextInputLayout

    @Inject
    lateinit var model: ViamericasRecipientCreateModel
    @Inject
    lateinit var estadoAdapter: ArrayAdapter<VmStateModel>
    @field: [Inject Named("viamericaCityAdapter")]
    lateinit var cityAdapter: ArrayAdapter<VmCityModel>
    @Inject
    lateinit var presenter: ViamericasRecipientCreatePresenter
    @Inject
    lateinit var infoDialog: AlertDialog
    @Inject
    lateinit var validator: Validator
    @Inject
    lateinit var nacimientoPicker: DatePickerDialog

    private lateinit var unbinder: Unbinder
    private lateinit var nacimientoDateTime: LocalDateTime


    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun postStates(states: List<VmStateModel>) {
        if (estadoAdapter.count > 0) {
            estadoAdapter.clear()
        }

        estadoAdapter.add(INITIAL_STATE)
        estadoAdapter.addAll(states)
    }

    override fun postCities(cities: List<VmCityModel>) {
        if (cityAdapter.count > 0) {
            cityAdapter.clear()
        }
        cityAdapter.add(INITIAL_CITY)
        cityAdapter.addAll(cities)
    }

    override fun postRecipientResponse(recipient: RecipientResponse) {
        val intent = Intent()
        intent.putExtra(RESULT_DATA, recipient)
        setResult(RESULT_OK, intent)
        finish()
    }

    @OnClick(R.id.b_viamericas_recipient_create_info)
    override fun clickInfo() {
        infoDialog.show()
    }

    @OnItemSelected(
            value = [R.id.spinner_viamericas_recipient_state],
            callback = OnItemSelected.Callback.ITEM_SELECTED
    )
    override fun onStateSelected(pos: Int) {
        cityAdapter.clear()
        if (pos > 0) {
            presenter.getCiudadesPorEstado(model.pais, estadoAdapter.getItem(pos)!!)
        }
    }

    @OnClick(R.id.b_viamericas_recipient_date)
    override fun showDateDialog() {
        if (!nacimientoPicker.isShowing) nacimientoPicker.show()
    }

    @OnClick(R.id.b_viamericas_recipient_continuar)
    override fun clickCreateOrder() {
        validator.validate()
    }

    override fun showProgress() {
        if (progress_viamericas_recipient.visibility == View.GONE) progress_viamericas_recipient.visibility =
                View.VISIBLE
    }

    override fun hideProgress() {
        if (progress_viamericas_recipient.visibility == View.VISIBLE) progress_viamericas_recipient.visibility =
                View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        ValidationUtils.onValidationFailed(this, errors)
    }

    override fun onValidationSucceeded() {
        val name = StringUtil.removeAcentos(AndroidUtils.getText(firstNameTil.editText))
        val last = StringUtil.removeAcentos(AndroidUtils.getText(lastNameTil.editText))
        val email = AndroidUtils.getText(emailTil.editText)
        val address = StringUtil.removeAcentos(AndroidUtils.getText(addressTil.editText))
        val state = (estadoSpinner.selectedItem as VmStateModel).idState
        val city = (citySpinner.selectedItem as VmCityModel).idCity
        val mName = StringUtil.removeAcentos(AndroidUtils.getText(middleNameTil.editText))
        val slName = StringUtil.removeAcentos(AndroidUtils.getText(maternoTil.editText))
        val address2 = StringUtil.removeAcentos(AndroidUtils.getText(addressTwoTil.editText))
        val phone = StringUtil.removeAcentos(AndroidUtils.getText(celularTil.editText))
        val zipCode = AndroidUtils.getText(zipTil.editText)

        val builder = RecipientModel.Builder().setAddress(address)
                .setEmail(email)
                .setFirstName(name)
                .setIdCity(city)
                .setIdCountry(model.pais.idCountry)
                .setIdSender(model.idSender)
                .setIdState(state)
                .setLastName(last)
                .setLName(last)
                .setMName(mName)
                .setSLName(slName)
                .setAddress2(address2)
                .setPhone1(phone)
                .setZIP(zipCode)


        if (::nacimientoDateTime.isInitialized && VmActivity.hasAgeRequirement(nacimientoDateTime, 18)) {
            val dob = DateTimeFormatter.ofPattern("yyyy/MM/dd").format(nacimientoDateTime)
            builder.setBirthDate(dob)
        }

        val recipient = builder.build()

        presenter.addRecipient(recipient)
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        //SUMAMOS 1 POR INDICE DE MES
        dateText.setText(String.format(Locale.US, "%d/%d/%d", p1, p2 + 1, p3))
        nacimientoDateTime = LocalDateTime.of(p1, p2 + 1, p3, 0, 0)
    }


    companion object {
        const val REQUEST_CODE = 427
        const val RESULT_DATA = "recipient"

        private val INITIAL_STATE = VmStateModel("-1", "Selecciona")
        private val INITIAL_CITY = VmCityModel("-1", "Selecciona")

        fun get(context: Context, model: ViamericasRecipientCreateModel): Intent {
            return Intent(context, ViamericasRecipientCreateActivity::class.java).putExtra(
                    "model",
                    model
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viamericas_recipient_create)

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.slide_out_right)

        Mobilecard
                .get()
                .netComponent
                .viamericasRecipientCreateSubcomponent(ViamericasRecipientCreateModule(this))
                .inject(this)

        setContentView(R.layout.activity_viamericas_recipient_create)

        unbinder = ButterKnife.bind(this)

        estadoSpinner.adapter = estadoAdapter
        citySpinner.adapter = cityAdapter

        paisText.setText(model.pais.nameCountry)
        presenter.getEstadosPorPais(model.pais)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
