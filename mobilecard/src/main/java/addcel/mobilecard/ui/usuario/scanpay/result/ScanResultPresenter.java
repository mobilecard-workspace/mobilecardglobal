package addcel.mobilecard.ui.usuario.scanpay.result;

import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;

/**
 * ADDCEL on 11/09/18.
 */
public class ScanResultPresenter implements ScanResultContract.Presenter {

    private final SPReceiptEntity result;

    ScanResultPresenter(SPReceiptEntity result) {
        this.result = result;
    }

    @Override
    public SPReceiptEntity getResult() {
        return result;
    }
}
