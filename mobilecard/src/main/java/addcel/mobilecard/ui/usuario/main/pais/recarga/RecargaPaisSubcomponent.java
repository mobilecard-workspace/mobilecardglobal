package addcel.mobilecard.ui.usuario.main.pais.recarga;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 11/12/17.
 */
@PerFragment
@Subcomponent(modules = RecargaPaisModule.class)
public interface RecargaPaisSubcomponent {
    void inject(RecargaPaisFragment fragment);
}
