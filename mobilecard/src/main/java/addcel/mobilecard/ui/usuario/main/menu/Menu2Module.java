package addcel.mobilecard.ui.usuario.main.menu;

import com.google.common.collect.Lists;
import com.squareup.otto.Bus;

import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 04/10/17.
 */
@Module
public final class Menu2Module {
    private final Menu2Fragment fragment;

    Menu2Module(Menu2Fragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MainActivity provideActivity() {
        return (MainActivity) Objects.requireNonNull(fragment.getActivity());
    }


    @PerFragment
    @Provides
    MenuContract.Presenter providePresenter(MainActivity activity) {
        return new MenuPresenter(activity.getPresenter());
    }

    @PerFragment
    @Provides
    MenuAdapter provideAdapter(MainActivity activity,
                               SessionOperations session, Bus bus) {

        String name = "";
        try {
            name = activity.getPresenter().getUsuarioName();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new MenuAdapter(Lists.newArrayList(MenuModel.values()), session.getUsuario().getImg(),
                name, activity.getPresenter().getIdPais(), bus, false);
    }
}
