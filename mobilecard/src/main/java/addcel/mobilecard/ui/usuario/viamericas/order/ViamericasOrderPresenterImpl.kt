package addcel.mobilecard.ui.usuario.viamericas.order

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.MapUtils
import addcel.mobilecard.utils.StringUtil
import com.google.common.collect.Collections2
import com.google.common.collect.Lists
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

/**
 * ADDCEL on 30/03/17.
 */

interface ViamericasOrderPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getCountries(withUI: Boolean)

    fun getCurrencies(country: VmCountryModel)

    fun getModes(country: VmCountryModel, currency: CurrencyModel)

    fun getBranches(country: VmCountryModel, currency: CurrencyModel, mode: PaymentModeModel)

    fun getExchangeRate(mode: PaymentModeModel, currency: CurrencyModel, location: LocationModel)

    fun processCountryList(allCountries: List<VmCountryModel>): List<VmCountryModel>
}


class ViamericasOrderPresenterImpl(
        private val service: ViamericasService,
        private val countryLookupValues: Map<String, Int>,
        private val disposables: CompositeDisposable,
        private val view: VmOrderView
) : ViamericasOrderPresenter {
    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getBranches(
            country: VmCountryModel,
            currency: CurrencyModel,
            mode: PaymentModeModel
    ) {
        view.disableOnCallStarted()
        view.showProgress()
        val request = ("{\"idCountry\":\""
                + country.idCountry
                + "\", \"idModePayCurrency\":\""
                + currency.id
                + "\", \"idPaymentMode\":\""
                + mode.paymentModeId
                + "\", \"amount\":\"0\", \"onlyNetwork\":\"C\", \"withCost\":\"true\"}")
        Timber.d(request)

        val bDisp = service.getLocations(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                request
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    view.enableOnCallFinished()
                    if (it.idError == 0) view.postBranches(it.locations)
                    else view.showError(it.mensajeError)
                }, {
                    view.hideProgress()
                    view.enableOnCallFinished()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(bDisp)
    }

    override fun getExchangeRate(
            mode: PaymentModeModel, currency: CurrencyModel,
            location: LocationModel
    ) {
        view.disableOnCallStarted()
        view.disableInitialTexts()
        view.showProgress()

        val erDisp = service.getExchangeRate(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                "{\"modePayment\":\""
                        + mode.paymentModeId
                        + "\", \"modeCurrency\":\""
                        + currency.id
                        + "\", \"idPayBranch\":\""
                        + location.idbranchpaymentlocation
                        + "\"}"
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.enableOnCallFinished()
                    view.hideProgress()
                    view.enableTexts()
                    view.postExchangeRate(it)
                }, {
                    view.enableOnCallFinished()
                    view.hideProgress()
                    view.postExchangeRate(
                            ExchangeRateResponse(
                                    ErrorUtil.getFormattedHttpErrorCode(it),
                                    ErrorUtil.getFormattedHttpErrorMsg(it)
                            )
                    )
                })

        addToDisposables(erDisp)
    }

    override fun processCountryList(allCountries: List<VmCountryModel>): List<VmCountryModel> {
        var countries = allCountries
        Timber.d("Filtro de paises")
        val temp = Collections2.filter(countries, { input ->
            val weight = MapUtils.getOrDefault(
                    countryLookupValues, input!!.idCountry,
                    -99
            )
            input.weight = weight
            weight > 0
        })
        Timber.d("Ordenamiento de paises")
        countries = Lists.newArrayList(temp)
        Collections.sort(countries)
        return countries
    }

    override fun getCountries(withUI: Boolean) {
        if (withUI) {
            view.disableOnCallStarted()
            view.showProgress()
        }
        val cDisp = service.getCountries(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                "{\"operation\":\"NO\"}"
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (withUI) {
                        view.hideProgress()
                        view.enableOnCallFinished()
                    }
                    if (it.idError == 0) {
                        view.postCountries(it.countries)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    if (withUI) {
                        view.hideProgress()
                        view.enableOnCallFinished()
                    }
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(cDisp)
    }

    override fun getCurrencies(country: VmCountryModel) {
        view.disableOnCallStarted()
        view.showProgress()
        val json = "{\"idCountry\": \"" + country.idCountry + "\"}"
        val currDisp =
                service.getCurrency(
                        ViamericasService.AUTH,
                        BuildConfig.ADDCEL_APP_ID,
                        StringUtil.getCurrentLanguage(),
                        json
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            view.hideProgress()
                            view.enableOnCallFinished()
                            if (it.idError == 0) {
                                view.postCurrencies(it.currencies)
                            } else {
                                view.showError(it.mensajeError)
                            }
                        }, {
                            view.hideProgress()
                            view.enableOnCallFinished()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })
        addToDisposables(currDisp)
    }

    override fun getModes(country: VmCountryModel, currency: CurrencyModel) {
        view.disableOnCallStarted()
        view.showProgress()
        val json = ("{\"idCountry\": \""
                + country.idCountry
                + "\", \"idCurrency\":\""
                + currency.id
                + "\"}")
        val mDisp = service.getPaymentModes(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                json
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    view.enableOnCallFinished()
                    if (it.idError == 0) {
                        view.postModes(it.paymentModes)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    view.hideProgress()
                    view.enableOnCallFinished()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        addToDisposables(mDisp)
    }
}
