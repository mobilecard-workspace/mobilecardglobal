package addcel.mobilecard.ui.usuario.scanpay.qr.mx.monto

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity
import addcel.mobilecard.ui.listener.NumberKeyboardListener
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import addcel.mobilecard.ui.usuario.scanpay.qr.mx.concepto.SpMxConceptoQrFragment
import addcel.mobilecard.ui.usuario.scanpay.qr.mx.concepto.SpMxConceptoQrViewModel
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.view_keyboard_full.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-07-12.
 */

@Parcelize
data class SpMxMontoQrKeyboardViewModel(val pago: SPPagoEntity) : Parcelable


interface SpMxMontoQrKeyboardView {
    fun setComision(comisionModel: SPComisionEntity)
    fun disableContinuar()
    fun enableContinuar()
    fun showProgress()
    fun hideProgress()
    fun showError(msg: String)
    fun showSuccess(msg: String)
}

class SpMxMontoQrKeyboardFragment : Fragment(), NumberKeyboardListener, SpMxMontoQrKeyboardView {
    companion object {
        fun get(model: SpMxMontoQrKeyboardViewModel): SpMxMontoQrKeyboardFragment {
            val args = BundleBuilder().putParcelable("model", model).build()
            val frag = SpMxMontoQrKeyboardFragment()
            frag.arguments = args
            return frag
        }
    }

    private var montoString = ""
    private val disposables = CompositeDisposable()

    @Inject
    lateinit var presenter: SpMxMontoQrKeyboardPresenter
    @Inject
    lateinit var viewModel: SpMxMontoQrKeyboardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.spMxMontoQrKeyboardSubcomponent(SpMxMontoQrKeyboardModule(this)).inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_keyboard_full, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        text_keyboard_monto.isFocusable = false
        text_keyboard_monto.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                b_keyboard_total_delete.isEnabled = editable.isNotEmpty()
                b_keyboard_recargar.isEnabled = editable.isNotEmpty()
            }
        })
        b_keyboard_total_delete.isEnabled = false
        b_keyboard_recargar.isEnabled = false

        b_keyboard_total_one.setOnClickListener { onKeyStroke("1") }
        b_keyboard_total_two.setOnClickListener { onKeyStroke("2") }
        b_keyboard_total_three.setOnClickListener { onKeyStroke("3") }
        b_keyboard_total_four.setOnClickListener { onKeyStroke("4") }
        b_keyboard_total_five.setOnClickListener { onKeyStroke("5") }
        b_keyboard_total_six.setOnClickListener { onKeyStroke("6") }
        b_keyboard_total_seven.setOnClickListener { onKeyStroke("7") }
        b_keyboard_total_eight.setOnClickListener { onKeyStroke("8") }
        b_keyboard_total_nine.setOnClickListener { onKeyStroke("9") }
        b_keyboard_total_delete.setOnClickListener { onDeleteStroke() }
        b_keyboard_total_zero.setOnClickListener { onKeyStroke("0") }
        b_keyboard_recargar.setOnClickListener { disposables.add(presenter.getComision(viewModel.pago.establecimientoId)) }
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun launchConcepto(comisionModel: SPComisionEntity) {
        if (montoString.isNotEmpty()) {

            val monto = montoString.toDouble() / 100
            val comision = (monto * comisionModel.comisionPorcentaje) + comisionModel.comisionFija

            val resultEntity = viewModel.pago.copy(amount = monto, comision = comision)

            (activity as ScanPayActivity).supportFragmentManager.commit {
                add(
                        R.id.frame_scanpay,
                        SpMxConceptoQrFragment.get(SpMxConceptoQrViewModel(resultEntity))
                ).hide(
                        this@SpMxMontoQrKeyboardFragment
                ).addToBackStack(null)
                        .setCustomAnimations(
                                android.R.anim.slide_in_left,
                                android.R.anim.slide_out_right
                        )
            }
        } else {
            (activity as ScanPayActivity).showError(getString(R.string.error_cantidad))
        }
    }

    override fun onKeyStroke(captured: String) {
        if (captured.isNotEmpty() && StringUtil.isDecimalAmount(captured)) {
            montoString += captured
            text_keyboard_monto.text =
                    NumberKeyboardListener.getFormattedMonto(montoString, PaisResponse.PaisEntity.MX)
        } else {
            montoString = ""
            text_keyboard_monto.text = ""
        }
    }

    override fun onDeleteStroke() {
        montoString = StringUtil.removeLast(montoString)
        text_keyboard_monto.text =
                NumberKeyboardListener.getFormattedMonto(montoString, PaisResponse.PaisEntity.MX)
    }

    override fun setComision(comisionModel: SPComisionEntity) {
        if (isVisible)
            launchConcepto(comisionModel)
    }

    override fun disableContinuar() {
        if (isVisible) b_keyboard_recargar.isEnabled = false
    }

    override fun enableContinuar() {
        if (isVisible) b_keyboard_recargar.isEnabled = true
    }

    override fun showProgress() {
        if (activity != null)
            (activity as ScanPayActivity).showProgress()
    }

    override fun hideProgress() {
        if (activity != null)
            (activity as ScanPayActivity).hideProgress()
    }

    override fun showError(msg: String) {
        if (activity != null)
            (activity as ScanPayActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        if (activity != null)
            (activity as ScanPayActivity).showSuccess(msg)
    }
}