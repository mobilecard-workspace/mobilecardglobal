package addcel.mobilecard.ui.usuario.legacy.recargas;

import android.annotation.SuppressLint;

import androidx.annotation.Nullable;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 14/12/16.
 */
final class CarrierSelectionPresenter implements CarrierSelectionContract.Presenter {
    private final CatalogoService service;
    private final Usuario usuario;
    private final PaisResponse.PaisEntity paisModel;
    private final CatalogoResponse.RecargaEntity categoria;
    private final CarrierSelectionContract.View view;
    private final CompositeDisposable disposable = new CompositeDisposable();

    CarrierSelectionPresenter(CatalogoService service, Usuario usuario,
                              CatalogoResponse.RecargaEntity categoria, PaisResponse.PaisEntity paisModel,
                              CarrierSelectionContract.View view) {
        this.usuario = usuario;
        this.service = service;
        this.categoria = categoria;
        this.paisModel = paisModel;
        this.view = view;
    }

    @Override
    public CatalogoResponse.RecargaEntity getCategoria() {
        return categoria;
    }

    @Override
    public void clearDisposable() {
        disposable.clear();
    }

    @Override
    public PaisResponse.PaisEntity getPais() {
        return paisModel;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getServicios(CatalogoResponse.RecargaEntity recarga) {
        ServiciosRequest request =
                new ServiciosRequest.Builder().setIdAplicacion(BuildConfig.ADDCEL_APP_ID)
                        .setIdRecarga(recarga.getId())
                        .setIdioma(StringUtil.getCurrentLanguage())
                        .build();

        disposable.add(service.getRecargaServicios(BuildConfig.ADDCEL_APP_ID, usuario.getIdPais(),
                StringUtil.getCurrentLanguage(), request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactoResponse -> {
                    if (transactoResponse.getIdError() == 0) {
                        List<CatalogoResponse.RecargaEntity> carriers =
                                processRecargaList(transactoResponse.getServicios(), paisModel);
                        view.setCarriers(carriers);
                    } else {
                        view.showError(transactoResponse.getMensajeError());
                    }
                }, throwable -> view.showError(StringUtil.getNetworkError())));
    }

    @SuppressLint("CheckResult")
    @Override
    public void getMontos(CatalogoResponse.RecargaEntity carrier) {
        view.showProgress();
        view.disableList();
        ServiciosRequest request =
                new ServiciosRequest.Builder().setIdAplicacion(BuildConfig.ADDCEL_APP_ID)
                        .setIdServicio(carrier.getId())
                        .setIdioma(StringUtil.getCurrentLanguage())
                        .setIdPais(usuario.getIdPais())
                        .build();

        disposable.add(service.getRecargaMontos(BuildConfig.ADDCEL_APP_ID,
                usuario.getIdPais(),
                StringUtil.getCurrentLanguage(), request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactoResponse -> {
                    view.hideProgress();
                    view.enableList();
                    if (transactoResponse.getIdError() == 0) {
                        CarrierSelectionPresenter.this.processMontos(carrier, transactoResponse.getMontos());
                    } else {
                        view.showError(transactoResponse.getMensajeError());
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.enableList();
                    view.showError(StringUtil.getNetworkError());
                }));
    }

    @SuppressLint("CheckResult")
    @Override
    public void getPaises(CatalogoResponse.RecargaEntity carrier,
                          List<CatalogoResponse.MontoEntity> montos) {
        view.showProgress();
        disposable.add(service.getPaisesTelefonica(BuildConfig.ADDCEL_APP_ID, usuario.getIdPais(),
                StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(paisResponse -> {
                    view.hideProgress();
                    if (paisResponse.getIdError() != 0) {
                        view.showError(paisResponse.getMensajeError());
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.showError(StringUtil.getNetworkError());
                }));
    }

    private ArrayList<CatalogoResponse.RecargaEntity> processRecargaList(
            List<CatalogoResponse.RecargaEntity> src, @Nullable PaisResponse.PaisEntity pais) {

        if (usuario.getIdPais() == 3 && pais != null && pais.getId() != 7) {
            return Lists.newArrayList(Collections2.filter(src, input -> Objects.requireNonNull(input).getId() == 2));
        }
        return Lists.newArrayList(src);
    }

    private void processMontos(CatalogoResponse.RecargaEntity model,
                               List<CatalogoResponse.MontoEntity> montos) {
        if (categoria.getId() == 2) {
            view.launchPeajeFragment(model, montos);
        } else {
            view.launchRecargaFragment(model, montos);
        }
    }
}
