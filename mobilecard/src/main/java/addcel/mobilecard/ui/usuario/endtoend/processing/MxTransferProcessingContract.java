package addcel.mobilecard.ui.usuario.endtoend.processing;

import addcel.mobilecard.data.net.hth.PaymentEntity;
import addcel.mobilecard.data.net.hth.ReceiptEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.ScreenView;
import addcel.mobilecard.ui.Authenticable;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureCallback;

/**
 * ADDCEL on 22/11/17.
 */
public interface MxTransferProcessingContract {
    interface View extends ScreenView, Authenticable, BillPocketSecureCallback {

        void onTokenReceived(TokenEntity token);

        String getImei();

        String getSoftware();

        String getModel();

        void lockUI();

        void unLockUI();

        void onWhiteList();

        void notOnWhiteList();

        void onSuccess(ReceiptEntity receipt);
    }

    interface Presenter {

        long fetchIdUsuario();

        CardEntity fetchCard();

        String getJumioRef();

        void clearDisposables();

        void checkWhiteList();

        void getToken(String profile);

        PaymentEntity buildRequestModel();

        String buildRequestString(PaymentEntity model);

        byte[] buildRequestArray(PaymentEntity model);

        void enqueuePayment(TokenEntity tokenEntity, PaymentEntity paymentEntity);

        String getStartUrl();

        String getFormUrl();

        String getSuccessUrl();

        String getErrorUrl();
    }
}
