package addcel.mobilecard.ui.usuario.aci.list;

import com.squareup.otto.Bus;

import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.aci.list.AciListInteractor;
import addcel.mobilecard.domain.aci.list.AciListInteractorImpl;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * ADDCEL on 22/03/17.
 */
@Module
public final class AciListModule {

    private final AciListFragment fragment;

    AciListModule(AciListFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    AciService provideAPI(@Named("longClient") OkHttpClient client) {
        final Retrofit retrofit = new Retrofit.Builder().client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
        return retrofit.create(AciService.class);
    }

    @PerFragment
    @Provides
    AciListInteractor provideInteractor(AciService api, Bus bus) {
        return new AciListInteractorImpl(api, bus);
    }

    @PerFragment
    @Provides
    AciListAdapter provideAdapter(AciListInteractor interactor) {
        return new AciListAdapter(interactor.getItems());
    }

    @PerFragment
    @Provides
    AciListContract.Presenter providePresenter(AciListInteractor interactor) {
        return new AciListPresenter(interactor, fragment);
    }
}
