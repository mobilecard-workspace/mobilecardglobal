package addcel.mobilecard.ui.usuario.registro.email

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.registro.EmailStatusEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.negocio.mx.registro.NegocioRegistroActivity
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeActivity
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeModel
import addcel.mobilecard.ui.usuario.registro.RegistroActivity2
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.DeviceUtil
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_email_registro.*
import timber.log.Timber
import javax.inject.Inject

@Parcelize
data class EmailRegistroModel(
        val pais: PaisResponse.PaisEntity, val useCase: Int,
        val phone: String, var emailId: Long = -1
) : Parcelable

interface EmailRegistroView : ScreenView, TextWatcher {
    fun clickVerificar(email: String)

    fun onPreviouslyValidated(email: String, result: EmailStatusEntity)

    fun onVerificar(result: EmailStatusEntity)

    fun clickAbrir()

    fun clickContinuar()

    fun launchStorage(email: String)

    fun onContinuar(result: EmailStatusEntity)
}

class EmailRegistroActivity : AppCompatActivity(), EmailRegistroView {
    override fun launchStorage(email: String) {
        presenter.storeEmail(email)
    }

    companion object {

        const val USE_CASE_USUARIO = 1

        fun get(context: Context, model: EmailRegistroModel): Intent {
            return Intent(context, EmailRegistroActivity::class.java).putExtra("model", model)
        }
    }

    @Inject
    lateinit var presenter: EmailRegistroPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get().netComponent.emailRegistroSubcomponent(EmailRegistroModule(this))
                .inject(this)

        setContentView(R.layout.activity_email_registro)
        AndroidUtils.getEditText(til_registro_email_email).addTextChangedListener(this)

        b_registro_email_verificar.isEnabled = false
        b_registro_email_continuar.isEnabled = false

        b_registro_email_verificar.setOnClickListener {
            clickVerificar(AndroidUtils.getText(til_registro_email_email.editText))
        }

        b_registro_email_abrir.setOnClickListener { clickAbrir() }

        b_registro_email_continuar.setOnClickListener {
            clickContinuar()
        }

        val capturedEmail = presenter.getStoredEmail()
        AndroidUtils.getEditText(til_registro_email_email).setText(capturedEmail)

        if (capturedEmail.isNotEmpty()) {
            b_registro_email_verificar.isEnabled = true
            b_registro_email_continuar.isEnabled = true
            clickVerificar(capturedEmail)
        }
    }

    override fun clickVerificar(email: String) {
        presenter.sendVerificationLink(
                email,
                presenter.fetchModel().phone,
                DeviceUtil.getDeviceId(this)
        )
    }

    override fun onVerificar(result: EmailStatusEntity) {
        presenter.fetchModel().emailId = result.id
        showSuccess(result.mensajeError)
        img_email_registro_placeholder.visibility = View.GONE
        b_registro_email_continuar.isEnabled = true
    }

    override fun onPreviouslyValidated(email: String, result: EmailStatusEntity) {
        showSuccess(result.mensajeError)
        if (presenter.fetchModel().useCase == USE_CASE_USUARIO) {
            startActivity(

                    RegistroActivity2.get(
                            this, presenter.fetchModel().pais, presenter.fetchModel().phone,
                            AndroidUtils.getText(til_registro_email_email.editText)
                    )
            )
        } else {
            if (presenter.fetchModel().pais.id == 4) {

                val model = NegocioRegistroPeModel(
                        presenter.fetchModel().phone,
                        AndroidUtils.getText(til_registro_email_email.editText)
                )

                startActivity(NegocioRegistroPeActivity.get(this, model))
            } else {
                startActivity(
                        NegocioRegistroActivity.get(
                                this, presenter.fetchModel().phone,
                                AndroidUtils.getText(til_registro_email_email.editText)
                        )
                )
            }
        }
        finish()
    }

    override fun clickAbrir() {
        AlertDialog.Builder(this).setMessage(getString(R.string.txt_registro_email_launch_client))
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    abrirCliente()
                }.show()
    }

    private fun abrirCliente() {
        try {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_APP_EMAIL)
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Timber.e(e)
        }
    }

    override fun clickContinuar() {
        presenter.checkIfEmailVerified(
                presenter.fetchModel().emailId,
                AndroidUtils.getText(til_registro_email_email.editText)
        )
    }

    override fun onContinuar(result: EmailStatusEntity) {
        if (result.status == 1) {
            showSuccess(result.mensajeError)
            if (presenter.fetchModel().useCase == USE_CASE_USUARIO) {
                startActivity(
                        RegistroActivity2.get(
                                this, presenter.fetchModel().pais, presenter.fetchModel().phone,
                                AndroidUtils.getText(til_registro_email_email.editText)
                        )
                )
            } else {
                if (presenter.fetchModel().pais.id == 4) {

                    val model = NegocioRegistroPeModel(
                            presenter.fetchModel().phone,
                            AndroidUtils.getText(til_registro_email_email.editText)
                    )

                    startActivity(NegocioRegistroPeActivity.get(this, model))
                } else {
                    startActivity(
                            NegocioRegistroActivity.get(
                                    this, presenter.fetchModel().phone,
                                    AndroidUtils.getText(til_registro_email_email.editText)
                            )
                    )
                }
            }
            finish()
        } else {
            showError(result.mensajeError)
        }
    }

    override fun showProgress() {
        progress_registro_email.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_registro_email.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (text_registro_email_email.hasFocus()) {
                if (Patterns.EMAIL_ADDRESS.matcher(p0).matches()) {
                    b_registro_email_verificar.isEnabled = true
                    b_registro_email_continuar.isEnabled = true
                } else {
                    b_registro_email_verificar.isEnabled = false
                    b_registro_email_continuar.isEnabled = false
                }
            }
        } else {
            b_registro_email_verificar.isEnabled = false
            b_registro_email_continuar.isEnabled = false
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val email = text_registro_email_email.text.toString()
        outState.putString("email", email)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val email = savedInstanceState.getString("email")
        if (email != null && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            b_registro_email_verificar.isEnabled = true
            b_registro_email_continuar.isEnabled = true
            text_registro_email_email.setText(email)
            clickVerificar(email)
        }
    }
}
