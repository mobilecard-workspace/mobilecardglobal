package addcel.mobilecard.ui.usuario.mymobilecard.routing

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.wallet.AptoResponse
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject


@Parcelize
data class MyMcRoutingModel(val idUsuario: Long, val idPais: Int) : Parcelable

@Parcelize
data class MyMcRoutingResult(
        val mobileCard: CardEntity? = null,
        val usaAptoInfo: AptoResponse? = null,
        val idPais: Int = PaisResponse.PaisEntity.MX
) : Parcelable

interface MyMcRoutingCallback {

    fun onMyMcRoutingActivityResult(resultCode: Int, data: Intent)

    fun onPrevivale(card: CardEntity?)

    fun onPowie(card: CardEntity?)

    fun onApto(aptoInfo: AptoResponse?, card: CardEntity?)

    fun onTebca(card: CardEntity?)
}

class MyMcRoutingActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE = 431
        const val RESULT_DATA = "addcel.mobilecard.ui.usuario.mymobilecard.routing.data"
        private const val PARAM_MODEL = "addcel.mobilecard.ui.usuario.mymobilecard.routing.model"

        fun get(context: Context, model: MyMcRoutingModel): Intent {
            return Intent(context, MyMcRoutingActivity::class.java).putExtra(PARAM_MODEL, model)
        }
    }

    @Inject
    lateinit var wallet: WalletAPI
    private lateinit var model: MyMcRoutingModel
    private lateinit var result: MyMcRoutingResult

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.inject(this)
        setContentView(R.layout.activity_my_mc_routing)
        model = intent?.getParcelableExtra(PARAM_MODEL)!!
        result = MyMcRoutingResult()
        compositeDisposable.add(evalCardStatus())
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun evalCardStatus(): Disposable {

        if (model.idPais == PaisResponse.PaisEntity.US) {
            return wallet.getInfoApto(
                    BuildConfig.ADDCEL_APP_ID,
                    model.idPais,
                    StringUtil.getCurrentLanguage(),
                    model.idUsuario
            )
                    .subscribeOn(Schedulers.io())
                    .flatMap {
                        if (it.code == 0) {
                            result = result.copy(usaAptoInfo = it, idPais = model.idPais)
                        }
                        wallet.getMobilecardCard(
                                BuildConfig.ADDCEL_APP_ID,
                                model.idUsuario,
                                StringUtil.getCurrentLanguage(),
                                model.idPais
                        )
                    }.observeOn(AndroidSchedulers.mainThread()).subscribe({
                        if (it.idError == 0) {
                            onCard(it.card)
                        } else {
                            if (result.usaAptoInfo != null)
                                onApto(result.usaAptoInfo!!)
                            else
                                onNoCard()
                        }
                    }, {
                        Toasty.error(
                                this@MyMcRoutingActivity,
                                ErrorUtil.getFormattedHttpErrorMsg(it)
                        )
                                .show()
                        onError()
                    })

        } else {
            return wallet.getMobilecardCard(
                    BuildConfig.ADDCEL_APP_ID,
                    model.idUsuario,
                    StringUtil.getCurrentLanguage(),
                    model.idPais
            )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                if (it.idError == 0) {
                                    onCard(it.card)
                                } else {
                                    Toasty.error(
                                            this@MyMcRoutingActivity,
                                            it.mensajeError
                                    ).show()
                                    onNoCard()
                                }
                            }, {
                        Toasty.error(
                                this@MyMcRoutingActivity,
                                ErrorUtil.getFormattedHttpErrorMsg(it)
                        )
                                .show()
                        onError()
                    }
                    )
        }
    }

    private fun onCard(card: CardEntity) {
        val resultIntent = Intent()
        result = result.copy(mobileCard = card, idPais = model.idPais)
        resultIntent.putExtra(RESULT_DATA, result)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    private fun onApto(aptoResponse: AptoResponse) {
        val resultIntent = Intent()
        result = result.copy(usaAptoInfo = aptoResponse, idPais = model.idPais)
        resultIntent.putExtra(RESULT_DATA, result)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    private fun onNoCard() {
        val resultIntent = Intent()
        result = result.copy(idPais = model.idPais)
        resultIntent.putExtra(RESULT_DATA, result)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    private fun onError() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}
