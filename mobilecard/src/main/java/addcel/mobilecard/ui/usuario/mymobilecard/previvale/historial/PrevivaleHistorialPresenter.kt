package addcel.mobilecard.ui.usuario.mymobilecard.previvale.historial

import addcel.mobilecard.McConstants
import addcel.mobilecard.data.net.previvale.historial.PrevivaleAPI
import addcel.mobilecard.data.net.previvale.historial.PrevivaleHistorialRequest
import addcel.mobilecard.data.net.previvale.historial.PrevivaleTransaction
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.LocalDate
import org.threeten.bp.Month
import org.threeten.bp.format.DateTimeFormatter

/**
 * ADDCEL on 2020-01-27.
 */
interface PrevivaleHistorialPresenter {

    fun getTransactions(idUsuario: Long, pan: String, hoy: LocalDate): Disposable

    fun formatDateParam(date: LocalDate, isLast: Boolean): String

    fun sortMovimientos(movimientos: List<PrevivaleTransaction>): List<PrevivaleTransaction>

}

class PrevivaleHistorialPresenterImpl(val api: PrevivaleAPI, val view: PrevivaleHistorialView) : PrevivaleHistorialPresenter {

    companion object {
        private val REQUEST_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd")
        private val RESPONSE_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        private const val FIRST_OF_MONTH = 1
        private const val CODE_SUCCESS = 1000
    }

    override fun getTransactions(idUsuario: Long, pan: String, hoy: LocalDate): Disposable {
        view.showProgress()
        val now = LocalDate.now()
        val request = PrevivaleHistorialRequest(formatDateParam(now, true),
                formatDateParam(now, false), idUsuario, pan)

        val observable = api.movimientosTarjeta(3, McConstants.PAIS_ID_MX,
                StringUtil.getCurrentLanguage(), request).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

        return observable.subscribe({
            view.hideProgress()
            if (it.code == CODE_SUCCESS && it.data.isNotEmpty())
                view.setResults(sortMovimientos(it.data))
            else {
                view.showError(it.message)
                view.setResults(listOf())
            }
        }, {
            view.hideProgress()
            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
            view.setResults(listOf())
        })
    }

    override fun formatDateParam(date: LocalDate, isLast: Boolean): String {
        val dayParam = if (isLast) {
            if (date.month == Month.FEBRUARY && date.lengthOfMonth() == 29) 28 else
                date.lengthOfMonth()
        } else {
            FIRST_OF_MONTH
        }
        val newDate = LocalDate.of(date.year, date.month, dayParam)
        return newDate.format(REQUEST_FORMATTER)
    }

    override fun sortMovimientos(movimientos: List<PrevivaleTransaction>): List<PrevivaleTransaction> {
        return movimientos.sortedByDescending { LocalDate.parse(it.fecha, RESPONSE_FORMATTER) }
    }


}