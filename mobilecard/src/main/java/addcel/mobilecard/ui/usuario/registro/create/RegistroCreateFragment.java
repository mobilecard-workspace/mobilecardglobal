package addcel.mobilecard.ui.usuario.registro.create;

import android.app.AlertDialog;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.ui.usuario.registro.RegistroActivity2;
import addcel.mobilecard.ui.usuario.registro.info.RegistroPersonalInfoFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.Lazy;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistroCreateFragment extends Fragment implements RegistroCreateContract.View {

    @Email(messageResId = R.string.error_email)
    @BindView(R.id.til_registro_create_email)
    TextInputLayout emailTil;

    @Password(scheme = Password.Scheme.ANY, messageResId = R.string.error_password, min = 8)
    @BindView(R.id.til_registro_create_password)
    TextInputLayout passwordTil;

    @ConfirmPassword(messageResId = R.string.error_conf_password)
    @BindView(R.id.til_registro_create_password_conf)
    TextInputLayout passConfTil;

    @Inject
    RegistroActivity2 registroActivity;
    @Inject
    Validator validator;
    @Inject
    RegistroCreateContract.Presenter presenter;

    private androidx.appcompat.app.AlertDialog infoDialog;
    private Unbinder unbinder;

    public RegistroCreateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .registroCreateSubcomponent(new RegistroCreateModule(this))
                .inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.view_registro_create, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registroActivity.setLocked(false);
        infoDialog = AndroidUtils.createInformationDialog(registroActivity, android.R.string.dialog_alert_title, R.string.info_login, R.string.ok);
        Button privacyButton = view.findViewById(R.id.b_registro_create_privacy);
        privacyButton.setPaintFlags(privacyButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        registroActivity.setLocked(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.b_registro_create_continue)
    @Override
    public void clickContinue() {
        validator.validate();
    }

    @Override
    public void showProgress() {
        registroActivity.getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        registroActivity.getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showError(int res) {
        if (isVisible()) Toasty.error(Objects.requireNonNull(getContext()), getString(res)).show();
    }

    @Override
    public void showError(String msg) {
        if (isVisible()) Toasty.error(Objects.requireNonNull(getContext()), msg).show();
    }

    @Override
    public void onPrivacy(String privacy) {
        new AlertDialog.Builder(registroActivity).setTitle(R.string.txt_registro_privacy)
                .setMessage(privacy)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    @Override
    public String getEmail() {
        CharSequence email = Objects.requireNonNull(emailTil.getEditText()).getText();
        return Strings.nullToEmpty(email.toString());
    }

    @Override
    public String getPassword() {
        CharSequence password = Objects.requireNonNull(passwordTil.getEditText()).getText();
        return Strings.nullToEmpty(password.toString());
    }

    @OnClick(R.id.b_registro_create_info)
    @Override
    public void clickInfo() {
        if (infoDialog != null) infoDialog.show();
    }

    @OnClick(R.id.b_registro_create_privacy)
    @Override
    public void clickPrivacy() {
        presenter.getPrivacy();
    }

    @Override
    public void onValidationSucceeded() {

        ValidationUtils.clearViewErrors(emailTil, passwordTil, passConfTil);

        registroActivity.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .addToBackStack(null)
                .hide(this)
                .add(R.id.registro_container, RegistroPersonalInfoFragment.Companion.get(getEmail(), getPassword()))
                .commit();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(registroActivity, errors);
    }
}
