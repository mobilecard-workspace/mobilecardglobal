package addcel.mobilecard.ui.usuario.aci.processing;

import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 16/10/17.
 */
public interface AciProcessingContract {
    interface View extends ScreenView {
        void onHasData();

        void onNoData();
    }

    interface Presenter {

        void clearDisposables();

        ServiceModel getServiceModel();

        CardEntity getCardEntity();

        double getMonto();

        double getComision();

        Address getAddress();

        String getReferencia();

        void checkData();
    }
}
