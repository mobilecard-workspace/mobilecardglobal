package addcel.mobilecard.ui.usuario.password;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 22/12/16.
 */
@PerActivity
@Subcomponent(modules = PasswordModule.class)
public interface PasswordSubcomponent {
    void inject(PasswordActivity activity);
}
