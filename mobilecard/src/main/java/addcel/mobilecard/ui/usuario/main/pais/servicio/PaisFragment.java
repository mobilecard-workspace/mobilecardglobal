package addcel.mobilecard.ui.usuario.main.pais.servicio;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.event.impl.PostPaisEvent;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import addcel.mobilecard.ui.usuario.legacy.CategoriaUseCaseObject;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.ui.usuario.main.MainContract;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * ADDCEL on 11/10/17.
 */
public class PaisFragment extends Fragment implements PaisContract.View {

    public static final int SERVICIO_MODULE = 1;
    @Inject
    int module;
    @Inject
    Bus bus;
    @Inject
    MainActivity activity;
    @Inject
    PaisAdapter adapter;
    @Inject
    PaisContract.Presenter presenter;

    @BindView(R.id.recycler_pais)
    RecyclerView recyclerView;

    private Unbinder unbinder;

    public PaisFragment() {
    }

    public static PaisFragment get(int module) {
        Bundle b = new Bundle();
        b.putInt("module", module);
        PaisFragment paisFragment = new PaisFragment();
        paisFragment.setArguments(b);
        return paisFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().paisSubcomponent(new PaisModule(this)).inject(this);
        bus.register(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_country_selection, container, false);
        unbinder = ButterKnife.bind(this, view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        enableList();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setSelection(MainContract.UseCase.SERVICIOS);
        activity.setBottomMenuUseCase(UseCase.PAISES);
        if (adapter.getItemCount() == 1) onPaisClick(0);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            activity.setSelection(MainContract.UseCase.SERVICIOS);
            notifyPaisChange(new PostPaisEvent(presenter.getIdPais(), Boolean.TRUE, false));
        }
    }

    @Override
    public void onDestroyView() {
        disableList();
        recyclerView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void setModule(int module) {
        this.module = module;
    }

    @Override
    public void onPaisClick(int pos) {
        Pais pais = adapter.getItem(pos);
        if (pais == Pais.US) {
            clickServicioUsa();
        } else {
            clickServicioMx();
        }
    }

    @Override
    public void clickServicioMx() {
        presenter.getServiciosMX();
    }

    @Override
    public void clickServicioUsa() {
        startActivity(AciActivity.get(activity, null, Boolean.FALSE));
    }

    @Override
    public void onCategoriasMX(List<CatalogoResponse.CategoriaEntity> models) {
        if (getView() != null) {

            CategoriaUseCaseObject useCase = new CategoriaUseCaseObject(models);
            activity.startActivity(PagoContainerActivity.get(activity, useCase));

            if (adapter.getItemCount() == 1) {
                activity.getFragmentManagerFromActivity().popBackStackImmediate();
            }
        }
    }

    @Override
    public void onCategoriasUSA(List<CatalogoResponse.CategoriaEntity> models) {
        if (getView() != null) {
            CategoriaUseCaseObject useCase = new CategoriaUseCaseObject(models);
            startActivity(PagoContainerActivity.get(activity, useCase));
        }
    }

    @Override
    public void disableList() {
        if (recyclerView != null) ItemClickSupport.removeFrom(recyclerView);
    }

    @Override
    public void enableList() {
        if (recyclerView != null) {
            ItemClickSupport.addTo(recyclerView).setOnItemClickListener((recyclerView, position, v) -> {
                RecyclerView.ViewHolder vh = recyclerView.getChildViewHolder(v);
                if (vh instanceof PaisAdapter.ItemViewHolder) {
                    onPaisClick(position);
                }
            });
        }
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(int resId) {
        showError(getString(resId));
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Subscribe
    @Override
    public void notifyPaisChange(PostPaisEvent event) {
        if (adapter != null) adapter.update(Pais.values(), event.getIdPais());
    }
}
