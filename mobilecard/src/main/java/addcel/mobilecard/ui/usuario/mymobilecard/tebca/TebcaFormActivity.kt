package addcel.mobilecard.ui.usuario.mymobilecard.tebca

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.DeptoEntity
import addcel.mobilecard.data.net.catalogo.DistritoEntity
import addcel.mobilecard.data.net.catalogo.ProvinciaEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.TebcaAltaRequest
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.StringUtil
import addcel.mobilecard.validation.ValidationUtils
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_tebca_form.*
import mx.mobilecard.crypto.AddcelCrypto
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject

/*
 new CardRequest(0, interactor.getIdUsuario(), AddcelCrypto.encryptHard(view.getCard()),
            AddcelCrypto.encryptHard(view.getVigencia()), AddcelCrypto.encryptHard(view.getCvv()),
            view.getCardHolderName(), true,
            view.getAddress1().concat(" ").concat(view.getAddress2()), view.getZipCode(), false,
            view.getTipoTarjeta(), StringUtil.getCurrentLanguage());
 */

@Parcelize
data class TebcaFormModel(val email: String, val usuario: Long, val telefono: String, val nombre: String,
                          val paterno: String, val materno: String, val pan: String, val vigencia: String,
                          val cvv: String, val holder: String, val address: String, val tipoTarjeta: TipoTarjetaEntity
) :
        Parcelable

interface TebcaFormCallback {
    fun onTebcaFormResult(resultCode: Int, data: Intent?)
}

interface TebcaFormView : ScreenView {

    fun configDepto()

    fun configProvincia()

    fun configDistrito()

    fun loadDeptos()

    fun onDeptosLoaded(deptos: List<DeptoEntity>)

    fun onDeptoSelected(pos: Int)

    fun loadProvincias(depto: DeptoEntity)

    fun onProvinciasLoaded(provincias: List<ProvinciaEntity>)

    fun onProvinciaSelected(pos: Int)

    fun loadDistritos(provincia: ProvinciaEntity)

    fun onDistritosLoaded(distritos: List<DistritoEntity>)

    fun clickContinuar()

    fun onCardSaved(cards: List<CardEntity>)
}

/**
 *
 *
 *
4830393259644661
4830397123944744
4830399374496556
4830398422892055
4830394724678169
 */
class TebcaFormActivity : AppCompatActivity(), TebcaFormView {

    companion object {

        const val REQUEST_CODE = 421
        const val DATA_CARD_LIST = "cards"

        private val dobFormatterView: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        private val dobFormatterApi: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

        fun get(context: Context, model: TebcaFormModel): Intent {
            return Intent(context, TebcaFormActivity::class.java).putExtra("model", model)
        }
    }

    @Inject
    lateinit var model: TebcaFormModel
    @Inject
    lateinit var presenter: TebcaFormPresenter

    lateinit var deptoAdapter: ArrayAdapter<DeptoEntity>
    lateinit var provinciaAdapter: ArrayAdapter<ProvinciaEntity>
    lateinit var distritoAdapter: ArrayAdapter<DistritoEntity>

    lateinit var datePickerDialog: DatePickerDialog
    private var fechaNacimientoDueno: LocalDateTime = LocalDateTime.now()

    private var currDeptoPosition = -1
    private var currProvinciaPosition = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.tebcaFormSubcomponent(TebcaFormModule(this)).inject(this)
        setContentView(R.layout.activity_tebca_form)

        configDepto()
        configProvincia()
        configDistrito()
        configDatePickerDialog()

        b_pe_negocio_registro_nacimiento.setOnClickListener {
            datePickerDialog.show()
        }

        b_tebca_form_continuar.setOnClickListener {
            clickContinuar()
        }

        AndroidUtils.setText(til_pe_negocio_registro_nombres.editText, model.nombre)
        AndroidUtils.setText(til_pe_negocio_registro_paterno.editText, model.paterno)
        AndroidUtils.setText(til_pe_negocio_registro_materno.editText, model.materno)
        AndroidUtils.setText(til_pe_negocio_registro_direccion.editText, model.address)
        AndroidUtils.setText(til_pe_negocio_registro_nacionalidad.editText, "PERUANA")
        AndroidUtils.setText(text_scan_card, model.pan)

        loadDeptos()

        datePickerDialog.show()

    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun configDepto() {
        deptoAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item)
        deptoAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        deptoAdapter.setNotifyOnChange(true)
        spinner_pe_negocio_registro_departamento.adapter = deptoAdapter
        spinner_pe_negocio_registro_departamento.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        currDeptoPosition = -1
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        onDeptoSelected(p2)
                    }
                }
    }

    override fun configProvincia() {
        provinciaAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item)
        provinciaAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        provinciaAdapter.setNotifyOnChange(true)
        spinner_pe_negocio_registro_provincia.adapter = provinciaAdapter
        spinner_pe_negocio_registro_provincia.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        currProvinciaPosition = -1
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        onProvinciaSelected(p2)
                    }
                }
    }

    override fun configDistrito() {
        distritoAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item)
        distritoAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        distritoAdapter.setNotifyOnChange(true)
        spinner_pe_negocio_registro_distrito.adapter = distritoAdapter
    }

    override fun loadDeptos() {
        presenter.loadDepartamentos()
    }

    override fun onDeptosLoaded(deptos: List<DeptoEntity>) {

        deptoAdapter.clear()
        if (deptos.isNotEmpty()) {
            deptoAdapter.addAll(deptos)
            onDeptoSelected(0)
        }
    }

    override fun onDeptoSelected(pos: Int) {
        if (pos != currDeptoPosition) {
            loadProvincias(deptoAdapter.getItem(pos)!!)
            currDeptoPosition = pos
        }
    }

    override fun loadProvincias(depto: DeptoEntity) {
        presenter.loadProvincias(depto)
    }

    override fun onProvinciasLoaded(provincias: List<ProvinciaEntity>) {
        provinciaAdapter.clear()
        if (provincias.isNotEmpty()) {
            provinciaAdapter.addAll(provincias)
            onProvinciaSelected(0)
        }
    }

    override fun onProvinciaSelected(pos: Int) {
        if (pos != currProvinciaPosition) {
            loadDistritos(provinciaAdapter.getItem(pos)!!)
            currProvinciaPosition = pos
        }
    }

    override fun loadDistritos(provincia: ProvinciaEntity) {
        presenter.loadDistritos(provincia)
    }

    override fun onDistritosLoaded(distritos: List<DistritoEntity>) {
        distritoAdapter.clear()
        if (distritos.isNotEmpty()) distritoAdapter.addAll(distritos)
    }

    override fun clickContinuar() {

        ValidationUtils.clearViewErrors(
                til_pe_negocio_registro_dni,
                til_pe_negocio_registro_verificador,
                til_pe_negocio_registro_direccion,
                til_pe_negocio_registro_centro_laboral,
                til_pe_negocio_registro_ocupacion
        )

        val dni = AndroidUtils.getText(til_pe_negocio_registro_dni.editText)
        val codigo = AddcelCrypto.encryptHard(model.cvv)
        val verificador = AndroidUtils.getText(til_pe_negocio_registro_verificador.editText)
        val pan = AddcelCrypto.encryptHard(model.pan)
        val paterno = AndroidUtils.getText(til_pe_negocio_registro_paterno.editText)
        val cargoPublico = if (check_pe_negocio_registro_burocrata.isChecked) "1" else "0"
        val centroLaboral = AndroidUtils.getText(til_pe_negocio_registro_centro_laboral.editText)
        val depto = (spinner_pe_negocio_registro_departamento.selectedItem as DeptoEntity).codigo
        val direccion = AndroidUtils.getText(til_pe_negocio_registro_direccion.editText)
        val distrito = (spinner_pe_negocio_registro_distrito.selectedItem as DistritoEntity).codigo
        val nacimiento = dobFormatterApi.format(fechaNacimientoDueno)
        val institucion = AndroidUtils.getText(til_pe_negocio_registro_centro_laboral.editText)
        val nacionalidad = AndroidUtils.getText(til_pe_negocio_registro_nacionalidad.editText)
        val nombre = AndroidUtils.getText(til_pe_negocio_registro_nombres.editText)
        val ocupacion = StringUtil.removeAcentosMultiple(
                AndroidUtils.getText(til_pe_negocio_registro_ocupacion.editText)
        )
        val provincia =
                (spinner_pe_negocio_registro_provincia.selectedItem as ProvinciaEntity).codigo
        val vigencia = AddcelCrypto.encryptHard(model.vigencia)

        when {
            (fechaNacimientoDueno.year == LocalDateTime.now().year) -> showError("Ingresa una fecha de nacimiento válida")
            dni.isNullOrEmpty() -> til_pe_negocio_registro_dni.error = "Ingresa un DNI válido"
            verificador.isNullOrEmpty() -> til_pe_negocio_registro_verificador.error =
                    "Ingresa un dígito verificador válido"
            direccion.isNullOrEmpty() -> til_pe_negocio_registro_direccion.error =
                    "Ingresa una dirección válida"
            centroLaboral.isNullOrEmpty() -> til_pe_negocio_registro_centro_laboral.error =
                    "Ingresa un centro laboral válido"
            ocupacion.isNullOrEmpty() -> til_pe_negocio_registro_ocupacion.error =
                    "Ingresa una ocupación válida"
            else -> {

                ValidationUtils.clearViewErrors(
                        til_pe_negocio_registro_dni,
                        til_pe_negocio_registro_verificador,
                        til_pe_negocio_registro_direccion,
                        til_pe_negocio_registro_centro_laboral,
                        til_pe_negocio_registro_ocupacion
                )

                val body =
                        TebcaAltaRequest(
                                dni,
                                codigo,
                                verificador,
                                model.email,
                                model.usuario,
                                pan,
                                "1",
                                paterno,
                                cargoPublico,
                                centroLaboral,
                                depto,
                                direccion,
                                distrito,
                                nacimiento,
                                institucion,
                                "PE",
                                nombre,
                                ocupacion,
                                provincia,
                                "M",
                                model.telefono,
                                vigencia
                        )

                presenter.saveCard(body)
            }
        }
    }

    override fun onCardSaved(cards: List<CardEntity>) {
        val intent =
                Intent().putParcelableArrayListExtra(DATA_CARD_LIST, cards as ArrayList<out Parcelable>)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun configDatePickerDialog() {
        val listener = DatePickerDialog.OnDateSetListener { _, i, i1, i2 ->
            val time = LocalDateTime.of(i, i1 + 1, i2, 0, 0)
            fechaNacimientoDueno = time
            b_pe_negocio_registro_nacimiento.text = dobFormatterView.format(fechaNacimientoDueno)

        }

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            datePickerDialog =
                    FixedHoloDatePickerDialog(
                            this, listener, Calendar.getInstance().get(Calendar.YEAR) - 18,
                            Calendar.getInstance().get(Calendar.MONTH),
                            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                    )
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog = DatePickerDialog(
                        this, R.style.CustomDatePickerDialogTheme, listener,
                        Calendar.getInstance().get(Calendar.YEAR) - 18,
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                )
            } else {
                datePickerDialog =
                        DatePickerDialog(
                                this, listener, Calendar.getInstance().get(Calendar.YEAR) - 18,
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                        )
                datePickerDialog.datePicker.calendarViewShown = false
            }
        }
        datePickerDialog.setTitle(R.string.txt_mobilecard_add_dob)
    }

    override fun showProgress() {
        progress_tebca_form.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_tebca_form.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }
}
