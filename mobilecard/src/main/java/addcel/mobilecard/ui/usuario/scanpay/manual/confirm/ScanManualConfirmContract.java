package addcel.mobilecard.ui.usuario.scanpay.manual.confirm;

import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 24/08/18.
 */
public interface ScanManualConfirmContract {
    interface View {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void setUiData();

        String getMsi();

        void setMsiEnabled();

        void launchSecure(SPPagoEntity request, TokenEntity token);

        void launchOpen(SPReceiptEntity request, TokenEntity tokenEntity);
    }

    interface Presenter {
        double getMonto();

        String getConcepto();

        LcpfEstablecimiento getEstablecimiento();


        CardEntity getFormaPago();

        String getFormattedFormaPago();

        String getFormattedMonto();

        String getFormattedPropina();

        String getFormattedComision();

        SPPagoEntity buildRequest();

        boolean isMsiDisabled();

        void getToken(String profile);

        void launchOpenPago(TokenEntity token);
    }
}
