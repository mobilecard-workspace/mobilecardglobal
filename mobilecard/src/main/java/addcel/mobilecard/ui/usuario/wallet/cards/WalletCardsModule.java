package addcel.mobilecard.ui.usuario.wallet.cards;

import com.squareup.picasso.Picasso;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.wallet.list.WalletListInteractor;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public final class WalletCardsModule {
    private final WalletCardsFragment fragment;

    WalletCardsModule(WalletCardsFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    WalletContainerActivity provideActivity() {
        return (WalletContainerActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    @Named("desiredW")
    int provideDesiredWidth(
            WalletContainerActivity activity) {
        return activity.getWindow().getWindowManager().getDefaultDisplay().getWidth();
    }

    @PerFragment
    @Provides
    WalletCardsAdapter provideAdapter(WalletContainerActivity activity,
                                      Picasso picasso, @Named("desiredW") int desiredW) {
        return new WalletCardsAdapter(activity.getModels(), desiredW, picasso);
    }

    @PerFragment
    @Provides
    WalletListInteractor provideInteractor(WalletAPI api,
                                           SessionOperations session) {
        return new WalletListInteractor(api, session.getUsuario(), new CompositeDisposable());
    }

    @PerFragment
    @Provides
    WalletCardsContract.Presenter providePresenter(
            WalletListInteractor interactor) {
        return new WalletCardsPresenter(interactor, fragment);
    }
}
