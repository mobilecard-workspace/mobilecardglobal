package addcel.mobilecard.ui.usuario.colombia.recargas.tae.seleccion

import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.di.scope.PerFragment
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 9/24/19.
 */
@Module
class ColombiaRecargaTaeSeleccionModule(val fragment: ColombiaRecargaTaeSeleccionFragment) {

    @PerFragment
    @Provides
    fun provideModel(): ColombiaRecargaTaeSeleccionModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideAdapter(): ColombiaRecargaTaeSeleccionAdapter {
        return ColombiaRecargaTaeSeleccionAdapter()
    }

    @PerFragment
    @Provides
    fun providePresenter(
            catalogoAPI: CatalogoAPI,
            picasso: Picasso
    ): ColombiaRecargaTaeSeleccionPresenter {
        return ColombiaRecargaTaeSeleccionPresenterImpl(
                catalogoAPI, picasso, CompositeDisposable(),
                fragment
        )
    }
}

@PerFragment
@Subcomponent(modules = [ColombiaRecargaTaeSeleccionModule::class])
interface ColombiaRecargaTaeSeleccionSubcomponent {
    fun inject(fragment: ColombiaRecargaTaeSeleccionFragment)
}