package addcel.mobilecard.ui.usuario.scanpay.manual.select;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 22/08/18.
 */
@PerFragment
@Subcomponent(modules = ScanManualSelectEstablecimientoModule.class)
public interface ScanManualSelectEstablecimientoSubcomponent {
    void inject(ScanManualSelectEstablecimientoFragment fragment);
}
