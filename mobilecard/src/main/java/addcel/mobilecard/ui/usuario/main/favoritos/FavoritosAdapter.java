package addcel.mobilecard.ui.usuario.main.favoritos;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.utils.ListUtil;
import timber.log.Timber;

/**
 * ADDCEL on 02/10/17.
 */
public final class FavoritosAdapter extends RecyclerView.Adapter<FavoritosAdapter.ViewHolder> {
    private final List<Favorito> favoritos = Lists.newArrayList();
    private final FavoritosContract.View view;
    private boolean editable;

    FavoritosAdapter(FavoritosContract.View view) {
        this.view = view;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        Timber.d("setEditable(%s)", editable);
        this.editable = editable;
        notifyDataSetChanged();
    }

    void updateItems(List<Favorito> favoritos) {
        if (ListUtil.notEmpty(this.favoritos)) this.favoritos.clear();
        this.favoritos.addAll(favoritos);
        this.editable = false;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favoritos, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Favorito item = favoritos.get(position);
        Timber.d("Es editable: %s", String.valueOf(editable));

        try {
            holder.logoView.setImageResource(item.getLogotipo());
        } catch (Resources.NotFoundException e) {
            holder.logoView.setImageResource(R.drawable.icon_fav_orange);
        } finally {
            holder.description.setText(item.getNombre());
        }
        holder.setDeleteVisible(editable);
        holder.hideDeleteShade();

        holder.deleteButton.setOnClickListener(view -> {
            if (holder.viewBackground.getVisibility() == View.VISIBLE) {
                holder.hideDeleteShade();
            } else {
                holder.showDeleteShade();
            }
        });

        holder.viewBackground.setOnClickListener(v -> view.clickDelete(position, item));
    }

    @Override
    public int getItemCount() {
        return favoritos.size();
    }

    public Favorito getItem(int pos) {
        return favoritos.get(pos);
    }

    void removeItem(int position) {
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        favoritos.remove(position);
        notifyItemRemoved(position);
    }

    void restoreItem(int position) {
        // notify item added by position
        notifyItemInserted(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView viewBackground;
        final ConstraintLayout viewForeground;
        final TextView description;
        final ImageView logoView;
        final ImageButton deleteButton;

        ViewHolder(View itemView) {
            super(itemView);
            logoView = itemView.findViewById(R.id.img_favoritos_logo);
            description = itemView.findViewById(R.id.text_favoritos);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
            deleteButton = itemView.findViewById(R.id.b_fav_delete);
        }

        void setDeleteVisible(boolean editable) {
            int visibility = editable ? View.VISIBLE : View.GONE;
            deleteButton.setVisibility(visibility);
        }

        void showDeleteShade() {
            viewBackground.setVisibility(View.VISIBLE);
        }

        void hideDeleteShade() {
            viewBackground.setVisibility(View.GONE);
        }
    }
}
