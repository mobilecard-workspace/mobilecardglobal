package addcel.mobilecard.ui.usuario.scanpay.qr.id;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.squareup.otto.Bus;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.McConstants;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import addcel.mobilecard.ui.usuario.scanpay.result.ScanResultFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.FragmentUtil;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 27/08/18.
 */
public final class ScanQrIdFragment extends Fragment implements ScanQrIdContract.View {

    @Inject
    ScanPayActivity activity;
    @Inject
    Bus bus;
    @Inject
    CardEntity card;
    @Inject
    Picasso picasso;
    @Inject
    ScanQrIdContract.Presenter presenter;

    private TextInputLayout idTil;
    private TextView dataView;
    private Spinner msiSpinner;
    private Button consultarButton;

    public ScanQrIdFragment() {
    }

    public static synchronized ScanQrIdFragment get(Bundle arguments) {
        ScanQrIdFragment frag = new ScanQrIdFragment();
        frag.setArguments(arguments);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().scanQrIdSubcomponent(new ScanQrIdModule(this)).inject(this);
        bus.register(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen_scanpay_qr_id, container, false);
        idTil = v.findViewById(R.id.til_scanpay_qr_id_id);
        dataView = v.findViewById(R.id.view_scanpay_qr_id_data);
        msiSpinner = v.findViewById(R.id.spinner_scanpay_card_msi);
        consultarButton = v.findViewById(R.id.b_scanpay_qr_id_consultar);

        AndroidUtils.getEditText(idTil).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                consultarButton.setEnabled(!Strings.isNullOrEmpty(editable.toString()));
                if (Strings.isNullOrEmpty(editable.toString())) {
                    dataView.setText("");
                    dataView.setVisibility(View.GONE);
                    presenter.clearData();
                }
            }
        });
        consultarButton.setOnClickListener(view -> clickValidate());
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setFinished(Boolean.FALSE);
        picasso.load(card.getImgShort())
                .placeholder(R.drawable.bg_visa)
                .placeholder(R.drawable.bg_visa)
                .fit()
                .into(view.findViewById(R.id.bg_scan_card), new Callback() {
                    @Override
                    public void onSuccess() {
                        ((TextView) view.findViewById(R.id.pan_scan_card)).setText(
                                StringUtil.maskCard(AddcelCrypto.decryptHard(card.getPan())));
                    }

                    @Override
                    public void onError(Exception e) {
                        ((TextView) view.findViewById(R.id.pan_scan_card)).setText(
                                StringUtil.maskCard(AddcelCrypto.decryptHard(card.getPan())));
                    }
                });

        if (isDebito(card) || isAmex(card)) {
            msiSpinner.setSelection(0);
            msiSpinner.setEnabled(false);
        } else {
            msiSpinner.setEnabled(true);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public String getScanPayId() {
        return Strings.nullToEmpty(AndroidUtils.getText(idTil.getEditText()));
    }

    @Override
    public String getMsi() {
        return (String) msiSpinner.getSelectedItem();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void clickValidate() {
        idTil.setError(null);
        long l = Long.parseLong(getScanPayId());
        if (l <= Integer.MAX_VALUE) {
            presenter.checkWhiteList();
        } else {
            idTil.setError(getString(R.string.error_scanpay_id));
        }
    }

    @Override
    public void clickConfirmar() {
    }

    @Override
    public CardEntity getcard() {
        return card;
    }

    @Override
    public void setData(SPPagoEntity e) {
        dataView.setText(getString(R.string.txt_scanpay_qr_id_invoice, String.valueOf(e.getAmount()),
                e.getConcept()));
    }

    @Override
    public void setTestData(String... data) {
        dataView.setText(getString(R.string.txt_scanpay_qr_id_invoice, Strings.nullToEmpty(data[0]),
                Strings.nullToEmpty(data[1])));
        dataView.setVisibility(View.VISIBLE);
    }

    @Override
    public void launchGetProfile() {
        showProgress();
    }

    @Override
    public void launchGetToken(String profile) {
        presenter.getToken(profile);
    }

    @Override
    public void onTokenReceived(TokenEntity tokenEntity) {
        launchPago(tokenEntity);
    }

    @Override
    public void launchPago(TokenEntity token) {
        presenter.pago(token, presenter.buildRequest());
    }

    @Override
    public void onPagoReceived(SPReceiptEntity result) {
        FragmentUtil.addFragmentHidingCurrent(activity.getSupportFragmentManager(), R.id.frame_scanpay,
                this, ScanResultFragment.Companion.get(result, McConstants.PAIS_ID_MX, false));
    }

    private boolean isDebito(CardEntity card) {
        return Objects.equals(card.getTipoTarjeta(), TipoTarjetaEntity.DEBITO);
    }

    private boolean isAmex(CardEntity card) {
        switch (Objects.requireNonNull(card.getTipo())) {
            case AmEx:
            case Amex:
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onWhiteList() {
        presenter.getToken("");
    }

    @Override
    public void notOnWhiteList() {
        showError("No estás autorizado para realizar esta operación. Contacta a soporte.");
    }
}
