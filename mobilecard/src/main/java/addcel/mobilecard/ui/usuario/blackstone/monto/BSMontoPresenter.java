package addcel.mobilecard.ui.usuario.blackstone.monto;

import android.os.Bundle;

import addcel.mobilecard.data.net.blackstone.model.BSProductEntity;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * ADDCEL on 09/07/18.
 */
class BSMontoPresenter implements BSMontoContract.Presenter {
    private final BSProductEntity product;
    private final CompositeDisposable disposables;
    private final BSMontoContract.View view;

    BSMontoPresenter(BSProductEntity product, BSMontoContract.View view) {
        this.product = product;
        this.disposables = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void addToDisposables(Disposable disposable) {
        disposables.add(disposable);
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @Override
    public BSProductEntity getProduct() {
        return product;
    }

    @Override
    public Bundle buildPagoBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("product", product);
        bundle.putDouble("monto", view.getCapturedMonto());
        bundle.putString("numero", StringUtil.trimPhone(view.getSelectedContact().getTelefono()));
        return bundle;
    }
}
