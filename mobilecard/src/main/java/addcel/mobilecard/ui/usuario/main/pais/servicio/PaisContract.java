package addcel.mobilecard.ui.usuario.main.pais.servicio;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.event.impl.PostPaisEvent;

/**
 * ADDCEL on 11/10/17.
 */
public interface PaisContract {
    interface View {

        void setModule(int module);

        void onPaisClick(int pos);

        void clickServicioMx();

        void clickServicioUsa();

        void onCategoriasMX(List<CatalogoResponse.CategoriaEntity> models);

        void onCategoriasUSA(List<CatalogoResponse.CategoriaEntity> models);

        void disableList();

        void enableList();

        void showProgress();

        void hideProgress();

        void showError(int resId);

        void showError(String msg);

        void notifyPaisChange(PostPaisEvent event);
    }

    interface Presenter {

        void clearDisposables();

        int getIdPais();

        void getServiciosMX();
    }
}
