package addcel.mobilecard.ui.usuario.legacy.recargas;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 1/23/19.
 */
public interface CarrierSelectionContract {
    interface View extends ScreenView {

        void disableList();

        void enableList();

        void setCarriers(List<CatalogoResponse.RecargaEntity> carriers);

        void goToPago(int carrierPos);

        void clickCarrier(int pos);

        void launchPeajeFragment(CatalogoResponse.RecargaEntity recargaModel,
                                 List<CatalogoResponse.MontoEntity> montos);

        void launchRecargaFragment(CatalogoResponse.RecargaEntity recargaModel,
                                   List<CatalogoResponse.MontoEntity> montos);

        void launchPaisFragment(CatalogoResponse.RecargaEntity recargaModel,
                                List<CatalogoResponse.MontoEntity> montos, List<PaisResponse.PaisEntity> paises);
    }

    interface Presenter {
        CatalogoResponse.RecargaEntity getCategoria();

        void clearDisposable();

        PaisResponse.PaisEntity getPais();

        void getServicios(CatalogoResponse.RecargaEntity recarga);

        void getMontos(CatalogoResponse.RecargaEntity carrier);

        void getPaises(CatalogoResponse.RecargaEntity carrier,
                       List<CatalogoResponse.MontoEntity> montos);
    }
}
