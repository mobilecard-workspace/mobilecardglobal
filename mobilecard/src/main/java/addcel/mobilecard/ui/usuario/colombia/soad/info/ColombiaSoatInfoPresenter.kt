package addcel.mobilecard.ui.usuario.colombia.soad.info

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.colombia.SoatConsultaRequest
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 10/1/19.
 */
interface ColombiaSoatInfoPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getInfo(request: SoatConsultaRequest)

    fun getTerminos()
}

class ColombiaSoatInfoPresenterImpl(
        val api: ColombiaMiddleware,
        val disposables: CompositeDisposable, val view: ColombiaSoadInfoView
) :
        ColombiaSoatInfoPresenter {
    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getInfo(request: SoatConsultaRequest) {
        view.showProgress()
        val pDisp =
                api.consultaPoliza(ColombiaMiddleware.getAuth(), request).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            view.hideProgress()
                            if (it.codigo == 0) {
                                view.hideRetry()
                                view.setData(it)
                            } else {
                                view.showError(it.mensaje
                                        ?: ErrorUtil.getErrorMsg(ErrorUtil.OPERATION))
                                view.clickBack()
                            }
                        }, {
                            view.hideProgress()
                            view.showError(it.localizedMessage
                                    ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                            view.showRetry()
                        })

        addToDisposables(pDisp)
    }

    override fun getTerminos() {
        val client = StringUtil.sha256("mobilecardandroid")

        val tDisp = api.getTerminos(
                BuildConfig.ADDCEL_APP_ID, client, "terminos", 2,
                StringUtil.getCurrentLanguage()
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    if (r.mIdError == 0) view.showTerminosDialog(r.mTerminos)
                    else view.showError(r.mMensajeError)
                }, { t ->
                    t.printStackTrace()
                    view.showError(ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })

        addToDisposables(tDisp)
    }
}