package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search

import addcel.mobilecard.data.net.colombia.multimarket.Servicio
import androidx.lifecycle.ViewModel
import io.reactivex.Completable

/**
 * ADDCEL on 2019-11-26.
 */
class MmServicioSeleccionViewModel : ViewModel() {

    val originalCards: MutableList<Servicio> = mutableListOf()
    val filteredCards: MutableList<Servicio> = mutableListOf()
    val oldFilteredCards: MutableList<Servicio> = mutableListOf()

    fun search(query: String): Completable = Completable.create {
        val wanted = originalCards.filter { s ->
            s.name.contains(query, true)
        }.toList()

        filteredCards.clear()
        filteredCards.addAll(wanted)
        it.onComplete()
    }

}