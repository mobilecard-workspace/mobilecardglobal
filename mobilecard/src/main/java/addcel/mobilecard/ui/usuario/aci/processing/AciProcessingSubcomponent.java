package addcel.mobilecard.ui.usuario.aci.processing;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 16/10/17.
 */
@PerFragment
@Subcomponent(modules = AciProcessingModule.class)
public interface AciProcessingSubcomponent {
    void inject(AciProcessingFragment fragment);
}
