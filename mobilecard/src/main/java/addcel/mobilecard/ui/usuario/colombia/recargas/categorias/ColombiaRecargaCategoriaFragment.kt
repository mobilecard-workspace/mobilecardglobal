package addcel.mobilecard.ui.usuario.colombia.recargas.categorias

import addcel.mobilecard.R
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.recargas.operadores.ColombiaRecargaOperadoresFragment
import addcel.mobilecard.ui.usuario.colombia.recargas.operadores.ColombiaRecargaOperadoresModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.view_categoria.*

/**
 * ADDCEL on 9/19/19.
 */
class ColombiaRecargaCategoriaFragment : Fragment() {

    companion object {
        fun get(): ColombiaRecargaCategoriaFragment {
            return ColombiaRecargaCategoriaFragment()
        }
    }

    lateinit var adapter: ColombiaCategoriaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ColombiaCategoriaAdapter()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_categoria, container, false)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) (activity as ContainerActivity).setAppToolbarTitle(R.string.nav_col_tae)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ContainerActivity).setAppToolbarTitle(R.string.nav_col_tae)

        recycler_categoria.adapter = adapter
        recycler_categoria.layoutManager = LinearLayoutManager(view.context)

        ItemClickSupport.addTo(recycler_categoria).setOnItemClickListener { _, pos, _ ->
            (activity as ContainerActivity).fragmentManagerLazy.commit {
                val model = ColombiaRecargaOperadoresModel(adapter.getItem(pos))
                add(R.id.frame_colombia, ColombiaRecargaOperadoresFragment.get(model))
                hide(this@ColombiaRecargaCategoriaFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_categoria)
        super.onDestroyView()
    }
}