package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.monto

import addcel.mobilecard.data.net.colombia.multimarket.MmConsultaEntity
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.utils.ErrorUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-12-24.
 */
interface MmServicioMontoPresenter {
    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun consultaSaldo(referencia: String, idConvenio: Int, barcode: Boolean)
}

class MmServicioMontoPresenterImpl(val multimarketAPI: MultimarketAPI, val usuario: Usuario, val compositeDisposable: CompositeDisposable, val view: MmServicioMontoView) : MmServicioMontoPresenter {
    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun consultaSaldo(referencia: String, idConvenio: Int, barcode: Boolean) {
        view.showProgress()

        val body = MmConsultaEntity(barcode, idConvenio, usuario.ideUsuario, referencia)

        val sDisp = multimarketAPI.escaneaFactura(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.hideProgress()
                            if (it.codigo == 0) {
                                if (it.amountEditable) {
                                    view.onConsultaSuccessEditable(it)
                                } else {
                                    view.onConsultaSuccess(it)
                                    view.showSuccess(it.mensaje)
                                }
                            } else {
                                view.onConsultaError()
                                view.showError(it.mensaje)
                            }
                        },
                        {
                            view.hideProgress()
                            view.onConsultaError()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })

        addToDisposables(sDisp)
    }

}