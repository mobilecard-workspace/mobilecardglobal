package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search

import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Servicio
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-11-26.
 */
class MmServiciosSearchAdapter(private val items: List<Servicio>) :
        RecyclerView.Adapter<MmServiciosSearchAdapter.ItemViewHolder>() {

    fun update(viewModel: MmServicioSeleccionViewModel): Disposable {
        val callback = MmServicioDiffUtilCallback(viewModel.oldFilteredCards, viewModel.filteredCards)

        return Observable.fromCallable {
            DiffUtil.calculateDiff(callback)
        }.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewModel.oldFilteredCards.clear()
                    viewModel.oldFilteredCards.addAll(viewModel.filteredCards)
                    it.dispatchUpdatesTo(this)
                }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_aci_servicio, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val model = getItem(position)
        holder.name.text = model.name
    }


    override fun onBindViewHolder(holder: ItemViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val o = payloads[0] as Bundle
            for (key in o.keySet()) {
                if (key == "name") {
                    val name = o.getString(key)
                    holder.name.text = name
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    fun getItem(pos: Int): Servicio {
        return items[pos]
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.textAci)
    }
}