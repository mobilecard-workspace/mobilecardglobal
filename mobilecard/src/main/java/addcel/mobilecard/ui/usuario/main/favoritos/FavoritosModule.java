package addcel.mobilecard.ui.usuario.main.favoritos;

import java.util.Objects;

import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 02/10/17.
 */
@Module
public final class FavoritosModule {
    private final FavoritosFragment fragment;

    FavoritosModule(FavoritosFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MainActivity provideActivity() {
        return (MainActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    FavoritosAdapter provideAdapter() {
        return new FavoritosAdapter(fragment);
    }

    @PerFragment
    @Provides
    FavoritosContract.Presenter providePresenter(FavoritoDaoRx dao,
                                                 SessionOperations session) {
        return new FavoritosPresenter(dao, session, fragment);
    }
}
