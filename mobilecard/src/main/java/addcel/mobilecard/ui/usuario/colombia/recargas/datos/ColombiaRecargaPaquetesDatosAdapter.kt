package addcel.mobilecard.ui.usuario.colombia.recargas.datos

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.Categoria
import addcel.mobilecard.data.net.catalogo.OperadorRecarga
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

/**
 * ADDCEL on 9/21/19.
 */
class ColombiaRecargaPaquetesDatosAdapter(val picasso: Picasso, val operador: OperadorRecarga) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    private val paquetes: MutableList<Categoria> = ArrayList()

    fun update(paquetes: List<Categoria>) {
        if (this.paquetes.isNotEmpty()) this.paquetes.clear()
        this.paquetes.addAll(paquetes)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> {
                val v = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_contacto, parent, false)
                ItemViewHolder(v)
            }
            TYPE_HEADER -> {
                val v =
                        LayoutInflater.from(parent.context)
                                .inflate(R.layout.item_list_header, parent, false)
                HeaderViewHolder(v)
            }
            else -> throw RuntimeException(
                    "there is no type that matches the type $viewType + make sure your using types correctly"
            )
        }
    }

    override fun getItemCount(): Int {
        return paquetes.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            val model = getItem(position)
            holder.text.text = model.descripcion
        } else if (holder is HeaderViewHolder) {
            picasso.load(operador.imgColor).error(R.drawable.flag_colombia)
                    .placeholder(R.drawable.flag_colombia).into(holder.header)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    fun getItem(pos: Int): Categoria {
        return paquetes[pos - 1]
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val text: TextView = itemView.findViewById(R.id.text_contacto_item_name)
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val header: ImageView = itemView.findViewById(R.id.header_img)
    }
}