package addcel.mobilecard.ui.usuario.legacy.servicios.confirm

import addcel.mobilecard.data.net.pago.PagoEntity
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.PurchaseScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureCallback

/**
 * ADDCEL on 2020-01-15.
 */
interface ServicioConfirmView : PurchaseScreenView, BillPocketSecureCallback {
    fun getImei(): String

    fun updateUiOnIdPais()


    fun lockScreen()

    fun unlockScreen()

    fun showError(resId: Int)

    fun onTokenReceived(tokenEntity: TokenEntity)

    fun onBpSuccess(response: PagoResponse)
}

interface ServicioConfirmPresenter {
    fun clearDisposables()

    fun fetchIdUsuario(): Long

    fun fetchCardEntity(): CardEntity

    fun getReferenciaName(): String

    fun getReferenciaValue(): String

    fun getBalance(): String

    fun getBalanceUSD(): String

    fun getFee(): String

    fun getTotal(): String

    fun getExchangeRate(): String

    fun getStartUrl(accountId: String): String

    fun getSuccessUrl(): String

    fun getErrorUrl(): String

    fun getFormUrl(): String

    fun getIdPais(): Int

    fun getJumioReference(): String

    fun checkWhiteList()

    fun getToken(profile: String)

    fun buildPagoRequest(): PagoEntity

    fun buildPagoRequestAsString(entity: PagoEntity): String

    fun buildPagoRequestForBrowser(entity: PagoEntity): ByteArray

    fun executePagoBP(token: TokenEntity, entity: PagoEntity)
}