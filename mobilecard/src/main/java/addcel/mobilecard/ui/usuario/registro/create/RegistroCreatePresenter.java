package addcel.mobilecard.ui.usuario.registro.create;

import android.annotation.SuppressLint;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 03/01/18.
 */
class RegistroCreatePresenter implements RegistroCreateContract.Presenter {

    private final UsuariosService service;
    private final RegistroCreateContract.View view;

    RegistroCreatePresenter(UsuariosService service, RegistroCreateContract.View view) {
        this.service = service;
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getPrivacy() {
        view.showProgress();
        String client = StringUtil.sha256("mobilecardandroid");
        service.getAvisoPrivacidad(BuildConfig.ADDCEL_APP_ID, client, 1,
                StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(terminosResponse -> {
                    view.hideProgress();
                    if (terminosResponse.getIdError() == 0) {
                        view.onPrivacy(terminosResponse.getTerminos());
                    } else {
                        view.showError(terminosResponse.getMensajeError());
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.showError(StringUtil.getNetworkError());
                });
    }
}
