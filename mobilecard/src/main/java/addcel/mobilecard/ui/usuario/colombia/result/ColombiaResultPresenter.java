package addcel.mobilecard.ui.usuario.colombia.result;

import org.jetbrains.annotations.Nullable;

import addcel.mobilecard.data.net.colombia.entity.ColombiaPagoResponseEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;

/**
 * ADDCEL on 10/29/18.
 */
public class ColombiaResultPresenter implements ColombiaResultContract.Presenter {
    private final SPReceiptEntity receipt;

    ColombiaResultPresenter(@Nullable SPReceiptEntity receipt) {
        this.receipt = receipt;
    }

    @Override
    public boolean isSuccessful() {
        if (receipt != null) {
            return receipt.getCode() == 0;
        }
        return false;
    }

    @Override
    public String getTitle() {
        if (receipt != null) {
            return receipt.getMessage();
        }
        return "";
    }

    @Override
    public String getMsg() {
        if (receipt != null) {
            return receipt.getMessage();
        }

        return "";
    }

    @Override
    public SPReceiptEntity getReceipt() {
        return receipt;
    }
}
