package addcel.mobilecard.ui.usuario.wallet.select;

import android.app.Activity;

import com.google.common.collect.Lists;
import com.squareup.picasso.Picasso;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.wallet.select.WalletSelectInteractor;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public final class WalletSelectModule {
    private final WalletSelectFragment fragment;

    WalletSelectModule(WalletSelectFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    Activity provideActivity() {
        return fragment.getActivity();
    }

    @PerFragment
    @Provides
    WalletSelectAdapter provideAdapter(Picasso picasso, Activity activity) {
        return new WalletSelectAdapter(Lists.newArrayList(),
                activity.getWindow().getWindowManager().getDefaultDisplay().getWidth(), picasso);
    }

    @PerFragment
    @Provides
    WalletSelectInteractor provideInteractor(WalletAPI api,
                                             SessionOperations session) {
        return new WalletSelectInteractor(api, session.getUsuario(), new CompositeDisposable());
    }

    @PerFragment
    @Provides
    WalletSelectContract.Presenter providePresenter(
            WalletSelectInteractor interactor) {
        return new WalletSelectPresenter(interactor, fragment);
    }
}
