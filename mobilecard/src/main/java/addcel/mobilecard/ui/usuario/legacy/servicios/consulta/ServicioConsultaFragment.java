package addcel.mobilecard.ui.usuario.legacy.servicios.consulta;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.text.NumberFormat;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.event.CancelConsultaEvent;
import addcel.mobilecard.event.impl.ErrorEvent;
import addcel.mobilecard.event.impl.SaldoConsultaEvent;
import addcel.mobilecard.ui.custom.view.CheckableButton;
import addcel.mobilecard.ui.custom.view.CheckableImageButton;
import addcel.mobilecard.ui.scanner.NewScannerActivity;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.servicios.HideTutorialEvent;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.ActivityResultEvent;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.MapUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Unbinder;
import dagger.Lazy;
import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 19/04/17.
 */
public final class ServicioConsultaFragment extends Fragment {

    private final CompositeDisposable uiDisposable = new CompositeDisposable();
    @BindView(R.id.img_recibo)
    ImageView reciboView;
    @BindView(R.id.view_servicio_header)
    ImageView header;
    @BindView(R.id.til_servicio_numerorecibo)
    TextInputLayout referenciaLabel;
    @BindView(R.id.text_servicio_numerorecibo)
    TextInputEditText referenciaText;
    @BindView(R.id.b_servicio_help)
    ImageView helpButton;
    @BindView(R.id.b_servicio_escanear)
    Button scanButton;
    @BindView(R.id.b_float_servicio_scan)
    FloatingActionButton scanFloatButton;
    @BindView(R.id.b_servicio_fav)
    CheckableImageButton favButton;
    @BindView(R.id.til_servicio_favorito)
    TextInputLayout favTil;
    @BindView(R.id.view_servicio_favorito)
    View favView;
    @BindView(R.id.b_servicio_pagar)
    CheckableButton consultarButton;
    @Inject
    PagoContainerActivity activity;
    @Inject
    @Named("colorLogoMap")
    Map<Integer, Integer> headerMap;
    @Inject
    @Named("iconMap")
    Map<Integer, Integer> whiteMap;
    @Inject
    ServicioResponse.ServicioEntity servicio;
    @Inject
    Bus bus;
    @Inject
    NumberFormat currencyFormat;
    @Inject
    boolean fav;
    @Inject
    ServicioConsultaContract.Presenter presenter;

    private AlertDialog infoDialog;
    private Unbinder unbinder;

    public ServicioConsultaFragment() {
    }

    public static ServicioConsultaFragment get(ServicioResponse.ServicioEntity servicio) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", servicio);
        final ServicioConsultaFragment fragment = new ServicioConsultaFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static ServicioConsultaFragment get(ServicioResponse.ServicioEntity servicio,
                                               boolean fav) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", servicio);
        bundle.putBoolean("fav", fav);
        final ServicioConsultaFragment fragment = new ServicioConsultaFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .servicioConsultaSubcomponent(new ServicioConsultaModule(this))
                .inject(this);

        bus.register(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.view_servicio_consulta, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        infoDialog = AndroidUtils.createInformationDialog(activity, android.R.string.dialog_alert_title, R.string.info_servicios_1, R.string.ok);

        activity.setStreenStatus(PagoContainerActivity.STATUS_NORMAL);
        activity.setAppToolbarTitle(R.string.nav_billpayments);
        header.setImageResource(MapUtils.getOrDefault(headerMap, servicio.getId(), R.drawable.logo_telmex));
        referenciaLabel.setHint(servicio.getNombreReferencia());
        scanButton.setText(Phrase.from(Objects.requireNonNull(getContext()), R.string.txt_servicio_scan)
                .put("referencia", Strings.nullToEmpty(servicio.getNombreReferencia()))
                .format());
        consultarButton.setText(R.string.txt_continuar);
        consultarButton.setAllCaps(false);
        setInitialFav(fav);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        postReferencia(new ActivityResultEvent(requestCode, resultCode, data));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        uiDisposable.clear();
        bus.unregister(this);
        super.onDestroy();
    }

    @OnClick({R.id.b_servicio_escanear, R.id.b_float_servicio_scan})
    void scanReferencia() {
        //startActivityForResult(new Intent(getContext(), ScannerActivity.class), SERVICIO_REQUEST);
        RxPermissions permissions = new RxPermissions(this);
        uiDisposable.add(permissions.request(Manifest.permission.CAMERA).subscribe(
                aBoolean -> {
                    if (aBoolean) {
                        if (getContext() != null)
                            NewScannerActivity.Companion.startForResult(ServicioConsultaFragment.this);
                    } else {
                        Toasty.info(activity, "MobileCard requiere autorización para utilizar la cámara del dispositivo").show();
                    }
                }
                , throwable -> activity.showError(Objects.requireNonNull(throwable.getLocalizedMessage()))));
    }

    @OnClick(R.id.b_servicio_fav)
    public void clickFav() {
        setFav(!favButton.isChecked());
    }

    @OnClick(R.id.b_servicio_fav_add)
    public void clickSaveFav() {
        if (!Strings.isNullOrEmpty(Objects.requireNonNull(favTil.getEditText()).getText().toString())) {
            favTil.setError(null);
            presenter.saveToFav(servicio,
                    MapUtils.getOrDefault(whiteMap, servicio.getId(), R.drawable.servicios),
                    favTil.getEditText().getText().toString().trim());
        } else {
            favTil.setError(getText(R.string.error_default));
        }
    }

    private void setFav(boolean fav) {
        favButton.setChecked(fav);
        new Handler().postDelayed(() -> favView.setVisibility(fav ? View.VISIBLE : View.GONE), 250);
    }

    private void setInitialFav(boolean fav) {
        favButton.setChecked(fav);
        favButton.setEnabled(!fav);
        if (!fav) favButton.requestFocus();
    }

    @OnClick(R.id.b_servicio_pagar)
    void clickConsultar() {
        if (consultarButton.getTag() == null) {
            if (Strings.isNullOrEmpty(Objects.requireNonNull(referenciaText.getText()).toString())) {
                referenciaText.setError(getText(R.string.error_campo_generico));
            } else {
                activity.showProgress();
                presenter.consultaReferencia(servicio, referenciaText.getText().toString().trim());
            }
        }
    }

    @OnLongClick(R.id.b_servicio_pagar)
    boolean longClickConsultar() {
        if (consultarButton.getTag() != null) {
            clearConsultaValues();
        }
        return true;
    }

    @Subscribe
    public void postConsultaResult(SaldoConsultaEvent event) {
        activity.hideProgress();
        if (event.getSaldoResponse().getIdError() == 0) {
            servicio.setReferencia(Objects.requireNonNull(referenciaText.getText()).toString().trim());
            Bundle bundle = new Bundle();
            bundle.putParcelable("servicio", servicio);
            bundle.putParcelable("saldo", event.getSaldoResponse());

            activity.getFragmentManagerLazy()
                    .beginTransaction()
                    .add(R.id.activity_pago_container,
                            WalletSelectFragment.get(WalletSelectFragment.SERVICIOS_MX, bundle))
                    .hide(this)
                    .addToBackStack(null)
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .commit();
        } else {
            Toasty.error(activity, event.getSaldoResponse().getMensajeError()).show();
        }
    }

    @Subscribe
    public void postErrorEvent(ErrorEvent event) {
        if (event.getErrorRes() == R.string.txt_favoritos_success) {
            Toasty.success(activity,
                    Strings.isNullOrEmpty(event.getErrorMsg()) ? getString(event.getErrorRes())
                            : event.getErrorMsg()).show();
            favButton.setEnabled(false);
            favView.setVisibility(View.GONE);
        } else {
            Toasty.error(activity,
                    Strings.isNullOrEmpty(event.getErrorMsg()) ? getString(event.getErrorRes())
                            : event.getErrorMsg()).show();
        }
    }

    @Subscribe
    public void hideReciboImage(HideTutorialEvent event) {
        if (reciboView.getVisibility() == View.VISIBLE) {
            new Handler().postDelayed(() -> {
                reciboView.setVisibility(View.GONE);
                activity.setShowingServicioHelp(false);
            }, 200);
        }
    }

    @OnClick(R.id.b_servicio_help)
    void clickHelp() {
        infoDialog.show();
    }

    @Subscribe
    public void cancelConsulta(CancelConsultaEvent event) {
        if (consultarButton.getTag() != null) {
            activity.setShowingConsultaData(false);
            clearConsultaValues();
        }
    }

    private void postReferencia(ActivityResultEvent event) {
        if (NewScannerActivity.REQUEST_CODE == event.getRequestCode()) {
            if (Activity.RESULT_OK == event.getResultCode()) {
                referenciaText.clearFocus();
                final String referencia = Objects.requireNonNull(event.getData().getExtras()).getString("result");
                if (!TextUtils.isEmpty(referencia)) {
                    referenciaText.setError(null);
                    referenciaText.clearFocus();
                    referenciaText.setText(referencia);
                    activity.showProgress();
                    presenter.consultaReferencia(servicio, referencia);
                }
            }
        }
    }

    private void clearConsultaValues() {
        referenciaText.setText("");
        referenciaText.setEnabled(true);
        scanButton.setVisibility(View.VISIBLE);
        scanFloatButton.setVisibility(View.VISIBLE);
        activity.setShowingConsultaData(false);
    }
}
