package addcel.mobilecard.ui.usuario.perfil;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 06/10/17.
 */
@PerActivity
@Subcomponent(modules = Perfil2Module.class)
public interface Perfil2Subcomponent {
    void inject(Perfil2Activity activity);
}
