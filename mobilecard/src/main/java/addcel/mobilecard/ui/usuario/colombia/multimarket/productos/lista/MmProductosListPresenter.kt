package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.lista

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-12-18.
 */
interface MmProductosListPresenter {
    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getProductos()
}

class MmProductosListPresenterImpl(val multimarket: MultimarketAPI, val disposables: CompositeDisposable, val view: MmProductosListView) : MmProductosListPresenter {
    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getProductos() {
        view.showProgress()
        val pDisp = multimarket.getProductos(BuildConfig.ADDCEL_APP_ID, PaisResponse.PaisEntity.CO, StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.codigo == 0) {
                        view.hideRetry()
                        view.setProductos(it.productos)
                    } else {
                        view.showRetry()
                        view.showError(it.mensaje)
                    }
                }, {
                    view.hideProgress()
                    view.showRetry()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })

        addToDisposables(pDisp)
    }

}