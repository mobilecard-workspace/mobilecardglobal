package addcel.mobilecard.ui.usuario.aci;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.ui.ContainerActivity;
import addcel.mobilecard.ui.usuario.aci.list.AciListFragment;
import addcel.mobilecard.ui.usuario.aci.pago.AciPagoFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AciActivity extends ContainerActivity implements AciView {

    private static final String ACI_MODEL_KEY = "servicio";
    private static final String ACI_FAV_KEY = "isFav";

    @BindView(R.id.toolbar_aci)
    Toolbar toolbar;

    @BindView(R.id.progress_aci)
    ProgressBar progressBar;

    private AciSubcomponent subcomponent;

    public static synchronized Intent get(Context context, @Nullable ServiceModel serviceModel,
                                          boolean isFav) {

        Bundle bundle = new Bundle();
        bundle.putParcelable(ACI_MODEL_KEY, serviceModel);
        bundle.putBoolean(ACI_FAV_KEY, isFav);

        return new Intent(context, AciActivity.class).putExtras(bundle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subcomponent = Mobilecard.get().getNetComponent().aciSubcomponent(new AciModule(this));
        setContentView(R.layout.activity_aci);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        subcomponent.inject(this);
        if (savedInstanceState == null) {
            getFragmentManagerLazy().beginTransaction()
                    .add(R.id.frame_aci, provideInitialFragment())
                    .commit();
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public AciSubcomponent getSubcomponent() {
        return subcomponent;
    }

    private Fragment getFavFragment() {
        ServiceModel model = getIntent().getParcelableExtra(ACI_MODEL_KEY);
        return AciPagoFragment.get(model, true);
    }

    private Fragment getNonFavFragment() {
        return AciListFragment.get();
    }

    private Fragment provideInitialFragment() {
        boolean fromFav = getIntent().getBooleanExtra(ACI_FAV_KEY, false);
        if (fromFav) {
            return getFavFragment();
        } else {
            return getNonFavFragment();
        }
    }

    @Override
    public void setAppToolbarTitle(int title) {
        toolbar.setTitle(title);
    }

    @Override
    public void setAppToolbarTitle(@NonNull String title) {
        toolbar.setTitle(title);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public @org.jetbrains.annotations.Nullable View getRetry() {
        return null;
    }
}
