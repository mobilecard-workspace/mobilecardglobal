package addcel.mobilecard.ui.usuario.blackstone.confirm;

import addcel.mobilecard.data.net.blackstone.model.BSPagoResponseEntity;
import addcel.mobilecard.data.net.blackstone.model.BSProductEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.ScreenView;
import addcel.mobilecard.ui.Authenticable;
import io.reactivex.disposables.Disposable;

/**
 * ADDCEL on 10/07/18.
 */
public interface BSConfirmContract {
    interface View extends ScreenView, Authenticable {

        void enablePagarButton();

        void disablePagarButton();

        void clickPagar();

        void onPagoSuccess(BSPagoResponseEntity response);
    }

    interface Presenter {

        void addToDisposables(Disposable disposable);

        void clearDisposables();

        String getJumioRef();

        BSProductEntity getProduct();

        String getPhone();

        double getMonto();

        double getComision();

        double getGrandTotal();

        CardEntity getCard();

        String formatCantidad(double cantidad);

        void checkWhiteList();

        void pagar();
    }
}
