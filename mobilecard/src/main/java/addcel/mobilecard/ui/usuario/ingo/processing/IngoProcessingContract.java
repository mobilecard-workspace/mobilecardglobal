package addcel.mobilecard.ui.usuario.ingo.processing;

import addcel.mobilecard.ui.Authenticable;

/**
 * ADDCEL on 19/07/18.
 */
public interface IngoProcessingContract {
    interface View extends Authenticable {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showCountryError(String msg);

        void showSuccess(String msg);

        void onTarjetaAddedOrUpdated();

        void onTarjetaNotAdded();

        void launchIngo();

        String getImei();
    }

    interface Presenter {

        void onDestroy();

        void addOrUpdateTarjeta();

        void checkIfIngoAvailable();

        String getCustomerId();

        String getSsoToken();

        String getScanReference();

        void checkWhiteList();
    }
}
