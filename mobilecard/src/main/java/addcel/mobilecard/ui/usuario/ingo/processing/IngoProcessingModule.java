package addcel.mobilecard.ui.usuario.ingo.processing;

import android.os.Bundle;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.ingo.processing.IngoProcessingInteractor;
import addcel.mobilecard.domain.ingo.processing.IngoProcessingInteractorImpl;
import addcel.mobilecard.ui.usuario.ingo.IngoActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 19/07/18.
 */
@Module
public final class IngoProcessingModule {
    private final IngoProcessingFragment fragment;

    IngoProcessingModule(IngoProcessingFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    IngoActivity provideActivity() {
        return (IngoActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    Bundle provideArguments() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    IngoService provideService(Retrofit retrofit) {
        return retrofit.create(IngoService.class);
    }

    @PerFragment
    @Provides
    Usuario provideUsuario(SessionOperations session) {
        return session.getUsuario();
    }


    @PerFragment
    @Provides
    IngoProcessingInteractor provideInteractor(IngoActivity activity, Usuario usuario) {
        return new IngoProcessingInteractorImpl(activity.getService(), activity.getAliveApi(), usuario);
    }

    @PerFragment
    @Provides
    TokenizerAPI provideTokenizer(Retrofit retrofit) {
        return TokenizerAPI.Companion.provideTokenizerAPI(retrofit);
    }

    @PerFragment
    @Provides
    IngoProcessingContract.Presenter providePresenter(
            IngoProcessingInteractor interactor, TokenizerAPI token, Bundle arguments) {
        return new IngoProcessingPresenter(interactor, arguments.getParcelable("ingoData"),
                token, arguments.getParcelable("card"), fragment.getString(R.string.txt_ingo_error_country),
                fragment);
    }
}
