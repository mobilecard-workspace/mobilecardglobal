package addcel.mobilecard.ui.usuario.scanpay.qr.pe.qr

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.qr.QrHexaEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.listener.NumberKeyboardListener
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import addcel.mobilecard.ui.usuario.scanpay.qr.pe.concepto.ScanPeQrConceptoData
import addcel.mobilecard.ui.usuario.scanpay.qr.pe.concepto.ScanPeQrConceptoFragment
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.view_keyboard_sp.*
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-07-17.
 */

@Parcelize
data class ScanPeQrMontoData(val hexaQr: String) : Parcelable

interface View : ScreenView, NumberKeyboardListener {
    fun launchDecode()

    fun onDecode(entity: QrHexaEntity)

    fun launchConcepto(hexaQr: String, entity: QrHexaEntity, monto: String, dynamic: Boolean)
}

class ScanPeQrMontoFragment : Fragment(), addcel.mobilecard.ui.usuario.scanpay.qr.pe.qr.View {

    companion object {

        fun formatAmount(amount: Double): String {
            val formatter = NumberFormat.getCurrencyInstance(Locale("es", "PE"))
            return formatter.format(amount)
        }

        fun get(configData: ScanPeQrMontoData): ScanPeQrMontoFragment {
            val args = BundleBuilder().putParcelable("data", configData).build()
            val frag = ScanPeQrMontoFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var containerAct: ScanPayActivity
    @Inject
    lateinit var configData: ScanPeQrMontoData
    @Inject
    lateinit var presenter: ScanPeQrMontoPresenter

    lateinit var decodedHexa: QrHexaEntity
    private var montoString = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.scanPeQrMontoSubcomponent(ScanPeQrMontoModule(this))
                .inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_keyboard_sp, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        text_keyboard_monto.isFocusable = false
        text_keyboard_monto.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                b_keyboard_total_delete.isEnabled = editable.isNotEmpty()
                b_keyboard_recargar.isEnabled = editable.isNotEmpty()
            }
        })
        b_keyboard_total_delete.isEnabled = false
        b_keyboard_recargar.isEnabled = false

        b_keyboard_total_one.setOnClickListener { onKeyStroke("1") }
        b_keyboard_total_two.setOnClickListener { onKeyStroke("2") }
        b_keyboard_total_three.setOnClickListener { onKeyStroke("3") }
        b_keyboard_total_four.setOnClickListener { onKeyStroke("4") }
        b_keyboard_total_five.setOnClickListener { onKeyStroke("5") }
        b_keyboard_total_six.setOnClickListener { onKeyStroke("6") }
        b_keyboard_total_seven.setOnClickListener { onKeyStroke("7") }
        b_keyboard_total_eight.setOnClickListener { onKeyStroke("8") }
        b_keyboard_total_nine.setOnClickListener { onKeyStroke("9") }
        b_keyboard_total_delete.setOnClickListener { onDeleteStroke() }
        b_keyboard_total_zero.setOnClickListener { onKeyStroke("0") }
        b_keyboard_recargar.setOnClickListener {
            if (::decodedHexa.isInitialized && montoString.isNotEmpty()) {
                launchConcepto(configData.hexaQr, decodedHexa, montoString, false)
            } else {
                (activity as ScanPayActivity).showError(getString(R.string.error_cantidad))
            }
        }

        launchDecode()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun launchDecode() {
        presenter.decodeQr(configData.hexaQr)
    }

    override fun onDecode(entity: QrHexaEntity) {
        if (entity.code == 0) {
            decodedHexa = entity
            if (decodedHexa.amount <= 0) {
                view_keyboard_establecimiento.text = decodedHexa.businessName
            } else {
                val cents = decodedHexa.amount * 100
                launchConcepto(configData.hexaQr, decodedHexa, cents.toString(), true)
            }
        } else {
            (activity as ScanPayActivity).showError(entity.message)
            Handler().postDelayed({
                (activity as ScanPayActivity).finish()
            }, 100)
        }
    }

    override fun launchConcepto(
            hexaQr: String, entity: QrHexaEntity, monto: String,
            dynamic: Boolean
    ) {

        val configData = ScanPeQrConceptoData(hexaQr, entity, monto)

        if (dynamic) {
            fragmentManager?.commit {
                replace(R.id.frame_scanpay, ScanPeQrConceptoFragment.get(configData))
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            }
        } else {
            fragmentManager?.commit {
                add(R.id.frame_scanpay, ScanPeQrConceptoFragment.get(configData))
                hide(this@ScanPeQrMontoFragment).addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            }
        }
    }

    override fun showProgress() {
        containerAct.showProgress()
    }

    override fun hideProgress() {
        containerAct.hideProgress()
    }

    override fun showError(msg: String) {
        containerAct.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerAct.showSuccess(msg)
    }

    override fun onKeyStroke(captured: String) {
        if (captured.isNotEmpty() && StringUtil.isDecimalAmount(captured)) {
            montoString += captured
            text_keyboard_monto.text =
                    NumberKeyboardListener.getFormattedMonto(montoString, PaisResponse.PaisEntity.PE)
        } else {
            montoString = ""
            text_keyboard_monto.text = ""
        }
    }

    override fun onDeleteStroke() {
        montoString = StringUtil.removeLast(montoString)
        text_keyboard_monto.text =
                NumberKeyboardListener.getFormattedMonto(montoString, PaisResponse.PaisEntity.PE)
    }
}