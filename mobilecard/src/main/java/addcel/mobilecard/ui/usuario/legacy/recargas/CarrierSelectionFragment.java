package addcel.mobilecard.ui.usuario.legacy.recargas;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.RecargaFragment;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select.PeajeNewFragment;
import addcel.mobilecard.ui.usuario.main.pais.recarga.RecargaPaisFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * ADDCEL on 13/12/16.
 */
public final class CarrierSelectionFragment extends Fragment
        implements CarrierSelectionContract.View {

    @Inject
    PagoContainerActivity pActivity;
    @Inject
    CatalogoResponse.RecargaEntity categoria;
    @Inject
    CarrierSelectionAdapter adapter;
    @Inject
    CarrierSelectionContract.Presenter presenter;

    @BindView(R.id.recycler_categoria)
    RecyclerView carriersView;

    private Unbinder unbinder;

    public CarrierSelectionFragment() {
    }

    public static CarrierSelectionFragment get(CatalogoResponse.RecargaEntity categoria,
                                               List<CatalogoResponse.RecargaEntity> carriers) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("categoria", categoria);
        bundle.putParcelableArrayList("carriers", (ArrayList<? extends Parcelable>) carriers);
        CarrierSelectionFragment fragment = new CarrierSelectionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static CarrierSelectionFragment get(CatalogoResponse.RecargaEntity categoria,
                                               List<CatalogoResponse.RecargaEntity> carriers, PaisResponse.PaisEntity paisModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("categoria", categoria);
        bundle.putParcelableArrayList("carriers", (ArrayList<? extends Parcelable>) carriers);
        bundle.putParcelable("pais", paisModel);
        CarrierSelectionFragment fragment = new CarrierSelectionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .carrierSelectionSubcomponent(new CarrierSelectionModule(this))
                .inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_categoria, container, false);
        unbinder = ButterKnife.bind(this, v);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        carriersView.setLayoutManager(manager);
        carriersView.setAdapter(adapter);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        carriersView.setItemAnimator(itemAnimator);
        enableList();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        pActivity.setAppToolbarTitle(categoria.getDescripcion());
    }

    @Override
    public void onDestroyView() {
        disableList();
        carriersView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposable();
        super.onDestroy();
    }

    @Override
    public void disableList() {
        if (carriersView != null) ItemClickSupport.removeFrom(carriersView);
    }

    @Override
    public void enableList() {
        if (carriersView != null) {
            ItemClickSupport.addTo(carriersView)
                    .setOnItemClickListener((recyclerView, position, v) -> clickCarrier(position));
        }
    }

    @Override
    public void setCarriers(List<CatalogoResponse.RecargaEntity> carriers) {
        if (getView() != null && isVisible()) {
            adapter.update(carriers);
        }
    }

    @Override
    public void goToPago(int carrierPos) {
        presenter.getMontos(adapter.getItem(carrierPos));
    }

    @Override
    public void clickCarrier(int pos) {
        CatalogoResponse.RecargaEntity model = adapter.getItem(pos);
        presenter.getMontos(model);
    }

    @Override
    public void launchPeajeFragment(CatalogoResponse.RecargaEntity recargaModel,
                                    List<CatalogoResponse.MontoEntity> montos) {
        PeajeNewFragment fragment = PeajeNewFragment.newInstance(recargaModel, montos);
        pActivity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.activity_pago_container, fragment)
                .hide(this)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void launchRecargaFragment(CatalogoResponse.RecargaEntity recargaModel,
                                      List<CatalogoResponse.MontoEntity> montos) {
        PaisResponse.PaisEntity paisModel = presenter.getPais();
        RecargaFragment fragment;
        if (paisModel == null) {
            fragment = RecargaFragment.newInstance(recargaModel, montos);
        } else {
            fragment = RecargaFragment.newInstance(recargaModel, montos, paisModel);
        }
        pActivity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.activity_pago_container, fragment)
                .hide(this)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void launchPaisFragment(CatalogoResponse.RecargaEntity recargaModel,
                                   List<CatalogoResponse.MontoEntity> montos, List<PaisResponse.PaisEntity> paises) {
        RecargaPaisFragment fragment = RecargaPaisFragment.get(recargaModel, montos, paises);
        pActivity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.activity_pago_container, fragment)
                .hide(this)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void showProgress() {
        pActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        pActivity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        pActivity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull String charSequence) {
        pActivity.showSuccess(charSequence);
    }
}
