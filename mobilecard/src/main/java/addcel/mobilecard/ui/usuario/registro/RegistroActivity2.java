package addcel.mobilecard.ui.usuario.registro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.ui.usuario.registro.info.RegistroPersonalInfoFragment;
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroActivity;
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroModel;
import addcel.mobilecard.utils.BundleBuilder;
import io.reactivex.annotations.NonNull;

public class RegistroActivity2 extends AppCompatActivity {
    private ProgressBar progressBar;
    private PaisResponse.PaisEntity paisModel;
    private String phoneNumber;
    private String email;
    private boolean locked;

    public static synchronized Intent get(Context context, PaisResponse.PaisEntity paisModel) {
        Bundle bundle = new BundleBuilder().putParcelable("pais", paisModel)
                .putString("phone", "")
                .putString("email", "")
                .build();
        return new Intent(context, RegistroActivity2.class).putExtras(bundle);
    }

    public static synchronized Intent get(Context context, PaisResponse.PaisEntity paisModel,
                                          String phone, String email) {
        Bundle bundle = new BundleBuilder().putParcelable("pais", paisModel)
                .putString("phone", phone)
                .putString("email", email)
                .build();
        return new Intent(context, RegistroActivity2.class).putExtras(bundle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro2);
        Toolbar toolbar = findViewById(R.id.toolbar_registro);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.registro_container, RegistroPersonalInfoFragment.Companion.get("", ""))
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public ProgressBar getProgressBar() {
        if (progressBar == null) progressBar = findViewById(R.id.progress_registro);
        return progressBar;
    }

    public PaisResponse.PaisEntity getPais() {
        if (paisModel == null) {
            paisModel = Objects.requireNonNull(getIntent().getExtras()).getParcelable("pais");
        }
        return paisModel;
    }

    public String getPhone() {
        if (phoneNumber == null) {
            phoneNumber = Objects.requireNonNull(getIntent().getExtras()).getString("phone");
        }
        return phoneNumber;
    }

    public String getEmail() {
        if (email == null) {
            email = Objects.requireNonNull(getIntent().getExtras()).getString("email");
        }
        return email;
    }

    private boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @Override
    public void onBackPressed() {
        if (!isLocked()) {
            startActivity(
                    SmsRegistroActivity.Companion.get(
                            this, new SmsRegistroModel(paisModel, SmsRegistroActivity.USE_CASE_USUARIO)
                    )
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }
}
