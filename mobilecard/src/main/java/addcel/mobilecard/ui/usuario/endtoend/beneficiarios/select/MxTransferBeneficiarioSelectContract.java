package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select;

import java.util.List;

import addcel.mobilecard.data.net.hth.AccountEntity;

/**
 * ADDCEL on 08/11/17.
 */
public interface MxTransferBeneficiarioSelectContract {
    interface View {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void updateBeneficiarioAdapter(List<AccountEntity> accounts);

        void setBeneficiarios(List<AccountEntity> accounts);

        void updateBeneficiarios(MxTransferBeneficiarioSelectEvent event);

        void onBeneficiarioSelected(int pos);

        AccountEntity getSelectedAccount();

        String getCapturedMonto();

        void onBeneficiarioNotSelected();

        void clickDelete();

        void clickUpdate();

        void clickAgregar();

        void onMontoCaptured(CharSequence sequence);

        void setComision(String monto);

        void setTotal(String monto);

        boolean validate();

        void clickContinuar();
    }

    interface Presenter {
        void getBeneficiarios();

        void deleteBeneficiario();

        double getDoubleComision(String monto, AccountEntity account);

        String getComision(String monto, AccountEntity account);

        double getDoubleTotal(String monto, AccountEntity account);

        String getTotal(String total, AccountEntity account);
    }
}
