package addcel.mobilecard.ui.usuario.legacy.servicios.list;

import android.os.Bundle;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 14/12/16.
 */
@Module
public final class ServicioSelectionModule {
    private final ServicioSelectionFragment fragment;

    ServicioSelectionModule(ServicioSelectionFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    PagoContainerActivity provideActivity() {
        return (PagoContainerActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    Bundle provideBundle() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    CatalogoResponse.CategoriaEntity provideCategoria(Bundle bundle) {
        return bundle.getParcelable("categoria");
    }

    @PerFragment
    @Provides
    List<ServicioResponse.ServicioEntity> provideServicios(Bundle bundle) {
        return bundle.getParcelableArrayList("servicios");
    }

    @PerFragment
    @Provides
    ServicioSelectionAdapter provideAdapter(
            @Named("iconMap") Map<Integer, Integer> icons,
            List<ServicioResponse.ServicioEntity> servicios) {
        return new ServicioSelectionAdapter(servicios, icons);
    }
}
