package addcel.mobilecard.ui.usuario.colombia.recargas.datos.seleccion

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.data.net.catalogo.Categoria
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 9/23/19.
 */
interface ColombiaRecargaPaquetesSeleccionPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getMontos(paquete: Categoria)

    fun loadLogo(path: String, placeholder: Int)
}

class ColombiaRecargaPaquetesSeleccionPresenterImpl(
        val catalogo: CatalogoAPI, val picasso: Picasso,
        val disposables: CompositeDisposable, val view: ColombiaRecargaPaquetesSeleccionView
) :
        ColombiaRecargaPaquetesSeleccionPresenter {
    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getMontos(paquete: Categoria) {
        view.showProgress()
        val montDisp = catalogo.getPaquetesDatosMontoPuntored(
                BuildConfig.ADDCEL_APP_ID, 2,
                StringUtil.getCurrentLanguage(), paquete.id
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    view.hideProgress()
                    if (r.idError == 0) view.onMontos(r.paquetes)
                    else view.onMontosError(r.mensajeError)
                }, { t ->
                    view.hideProgress()
                    view.onMontosError(t.localizedMessage
                            ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })
        addToDisposables(montDisp)
    }

    override fun loadLogo(path: String, placeholder: Int) {
        picasso.load(path).error(placeholder).placeholder(placeholder).into(object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                if (placeHolderDrawable != null) {
                    val bDraw = placeHolderDrawable as BitmapDrawable
                    view.setLogo(bDraw.bitmap)
                }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                e?.printStackTrace()
                if (errorDrawable != null) {
                    val bDraw = errorDrawable as BitmapDrawable
                    view.setLogo(bDraw.bitmap)
                }
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                if (bitmap != null) view.setLogo(bitmap)
            }
        })
    }
}