package addcel.mobilecard.ui.usuario.wallet.select;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/10/17.
 */
@PerFragment
@Subcomponent(modules = WalletSelectModule.class)
public interface WalletSelectSubcomponent {
    void inject(WalletSelectFragment fragment);
}
