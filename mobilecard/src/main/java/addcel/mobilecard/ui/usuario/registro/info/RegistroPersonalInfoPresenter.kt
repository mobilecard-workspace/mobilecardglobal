package addcel.mobilecard.ui.usuario.registro.info

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.usuarios.model.McResponse
import addcel.mobilecard.data.net.usuarios.model.RegistroRequest
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.usuario.registro.RegistroInteractor
import addcel.mobilecard.utils.StringUtil
import android.os.Build
import mx.mobilecard.crypto.AddcelCrypto

/**
 * ADDCEL on 28/09/17.
 */

internal class RegistroPersonalInfoPresenter(
        private val interactor: RegistroInteractor,
        private val view: View
) : Presenter {

    override fun getPrivacy() {
        view.showProgress()
        interactor.getTerms("privacidad", BuildConfig.ADDCEL_APP_ID, view.getPais(),
                StringUtil.getCurrentLanguage(), object : InteractorCallback<String> {
            override fun onSuccess(result: String) {
                view.hideProgress()
                view.onPrivacy(result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
            }
        })
    }

    override fun getTerminos() {
        view.showProgress()
        interactor.getTerms("terminos", BuildConfig.ADDCEL_APP_ID, view.getPais(),
                StringUtil.getCurrentLanguage(), object : InteractorCallback<String> {
            override fun onSuccess(result: String) {
                view.hideProgress()
                view.showTerminosDialog(result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
            }
        })
    }

    override fun saveUser() {
        view.showProgress()

        val b = RegistroRequest.Builder()
        b.setCountry(view.getPais())
                .setEmail(view.getEmail())
                .setFirstName(StringUtil.removeAcentosMultiple(view.getNombre()))
                .setGender("M")
                .setImei(view.getImei())
                .setLastName(StringUtil.removeAcentosMultiple(view.getApellido()))
                .setManufacturer(Build.MANUFACTURER)
                .setOs(Build.VERSION.SDK_INT.toString())
                .setPassword(AddcelCrypto.encryptHard(view.getPassword()))
                .setPhone(view.getCelular())
                .setPlatform()
                .setCedula(view.getCedula())
                .setTipoCedula(view.getTipoCedula())

        val request = b.build()

        interactor.createUser(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request,
                object : InteractorCallback<Usuario> {
                    override fun onSuccess(result: Usuario) {
                        view.hideProgress()
                        processRegistroCreation(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    private fun processRegistroCreation(usuario: Usuario) {
        when (usuario.idError) {
            0, 90 -> {
                interactor.loginUser(usuario, object : InteractorCallback<McResponse> {
                    override fun onSuccess(result: McResponse) {
                        view.showSuccess(result.mensajeError)
                        view.onLoginSuccess(usuario.mensajeError)
                    }

                    override fun onError(error: String) {
                        view.showError(error)
                    }

                })
            }
            else -> view.showError(usuario.mensajeError)
        }
    }
}
