package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.seleccion

import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.colombia.VrTag
import addcel.mobilecard.di.scope.PerFragment
import android.R
import android.widget.ArrayAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 9/26/19.
 */
@Module
class ViarapidaSeleccionModule(val fragment: ViarapidaSeleccionFragment) {

    @PerFragment
    @Provides
    fun provideMiddleware(r: Retrofit): ColombiaMiddleware {
        return ColombiaMiddleware.get(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            middleware: ColombiaMiddleware
    ): ViarapidaSeleccionPresenter {
        return ViarapidaSeleccionPresenterImpl(middleware, CompositeDisposable(), fragment)
    }

    @PerFragment
    @Provides
    fun provideAdapter(): ArrayAdapter<VrTag> {
        val adapter = ArrayAdapter<VrTag>(fragment.activity!!, R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(addcel.mobilecard.R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }
}

@PerFragment
@Subcomponent(modules = [ViarapidaSeleccionModule::class])
interface ViarapidaSeleccionSubcomponent {
    fun inject(fragment: ViarapidaSeleccionFragment)
}