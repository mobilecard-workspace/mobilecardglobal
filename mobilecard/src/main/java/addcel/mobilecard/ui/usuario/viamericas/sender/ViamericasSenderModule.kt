package addcel.mobilecard.ui.usuario.viamericas.sender

import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.VmCityModel
import addcel.mobilecard.data.net.viamericas.model.VmCountryModel
import addcel.mobilecard.data.net.viamericas.model.VmStateModel
import addcel.mobilecard.data.net.viamericas.model.VmZipModel
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog
import android.app.DatePickerDialog
import android.os.Build
import android.widget.ArrayAdapter
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Named

/**
 * ADDCEL on 27/03/17.
 */

@Module
class ViamericasSenderModule(private val activity: ViamericasSenderActivity) {

    @PerActivity
    @Provides
    fun providePicker(): DatePickerDialog {
        val dpDialog: DatePickerDialog = if (Build.VERSION.SDK_INT == 24) {
            FixedHoloDatePickerDialog(
                    activity, activity,
                    Calendar.getInstance().get(Calendar.YEAR) - 18,
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            )
        } else {
            DatePickerDialog(
                    activity, R.style.CustomDatePickerDialogTheme,
                    activity, Calendar.getInstance().get(Calendar.YEAR) - 18,
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            )
        }
        dpDialog.setTitle(R.string.txt_fecha_nacimiento)
        return dpDialog
    }

    @PerActivity
    @Provides
    fun provideValidator(adapter: ViewDataAdapter<TextInputLayout, String>): Validator {
        val validator = Validator(activity)
        validator.registerAdapter(TextInputLayout::class.java, adapter)
        validator.setValidationListener(activity)
        return validator
    }

    @PerActivity
    @Provides
    @Named("viamericaEstadosAdapter")
    fun provideAdapter(): ArrayAdapter<VmStateModel> {
        val estadoArrayAdapter =
                ArrayAdapter<VmStateModel>(activity, android.R.layout.simple_spinner_item)
        estadoArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        estadoArrayAdapter.setNotifyOnChange(true)
        return estadoArrayAdapter
    }

    @PerActivity
    @Provides
    @Named("viamericaCityAdapter")
    fun provideCityAdapter(): ArrayAdapter<VmCityModel> {
        val cityArrayAdapter =
                ArrayAdapter<VmCityModel>(activity, android.R.layout.simple_spinner_item)
        cityArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        cityArrayAdapter.setNotifyOnChange(true)
        return cityArrayAdapter
    }

    @PerActivity
    @Provides
    @Named("viamericaZipAdapter")
    fun provideZipAdapter(): ArrayAdapter<VmZipModel> {
        val zipArrayAdapter =
                ArrayAdapter<VmZipModel>(activity, android.R.layout.simple_spinner_item)
        zipArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        zipArrayAdapter.setNotifyOnChange(true)
        return zipArrayAdapter
    }

    @PerActivity
    @Provides
    @Named("viamericaCountryAdapter")
    fun provideOptCountryAdapter(): ArrayAdapter<VmCountryModel> {
        val cAdapter = ArrayAdapter<VmCountryModel>(activity, android.R.layout.simple_spinner_item)
        cAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        cAdapter.setNotifyOnChange(true)
        return cAdapter
    }

    @PerActivity
    @Provides
    fun providePresenter(
            service: ViamericasService,
            session: SessionOperations
    ): ViamericasSenderPresenter {
        return ViamericasSenderPresenterImpl(service, session, CompositeDisposable(), activity)
    }
}

@PerActivity
@Subcomponent(modules = [ViamericasSenderModule::class])
interface ViamericasSenderSubcomponent {
    fun inject(fragment: ViamericasSenderActivity)
}
