package addcel.mobilecard.ui.usuario.viamericas.fees.cash

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.usuario.viamericas.ViamericasTransactionElementAdapter
import addcel.mobilecard.ui.usuario.viamericas.confirmation.ViamericasConfirmFragment
import addcel.mobilecard.ui.usuario.viamericas.confirmation.ViamericasConfirmModel
import addcel.mobilecard.ui.usuario.viamericas.fees.ViamericasFeeModule
import addcel.mobilecard.ui.usuario.viamericas.fees.ViamericasFeePresenter
import addcel.mobilecard.ui.usuario.viamericas.fees.VmFeeView
import addcel.mobilecard.ui.usuario.viamericas.fees.VmFeeViewModel
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.StringUtil
import android.location.Address
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.google.android.material.textfield.TextInputLayout
import com.google.common.base.Strings
import timber.log.Timber
import java.text.NumberFormat
import javax.inject.Inject

/**
 * ADDCEL on 12/05/17.
 */

class ViamericasCashFragment : Fragment(), VmFeeView {

    @Inject
    lateinit var containerActivity: ContainerActivity
    @Inject
    lateinit var viewModel: VmFeeViewModel
    @Inject
    lateinit var card: CardEntity
    @Inject
    lateinit var locationData: McLocationData
    @Inject
    lateinit var usuario: Usuario
    @Inject
    lateinit var presenter: ViamericasFeePresenter
    @Inject
    lateinit var transactionElementAdapter: ViamericasTransactionElementAdapter
    @Inject
    lateinit var currencyFormat: NumberFormat

    //VIEWS
    @BindView(R.id.recycler_viamericas_cash_detalles)
    lateinit var detallesView: RecyclerView
    @BindView(R.id.til_viamericas_card_holder)
    lateinit var nombreTil: TextInputLayout
    @BindView(R.id.til_viamericas_card_amount)
    lateinit var amountTil: TextInputLayout

    private lateinit var unbinder: Unbinder
    private lateinit var currentAddress: Address
    private lateinit var orderFee: OrderFeeModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get()
                .netComponent
                .viamericasFeeSubcomponent(ViamericasFeeModule(this, this))
                .inject(this)


        /*
        Peticion fees: OrderFeeRequest
        {idModePay=c,
        modeCurrency=cop,
        idBranchPay=u10001,
        idPayment=K,
        exchangeRate=2367.0,
        idStateFrom=null, idCountry=null, paymentModeName=bank (deposit), N
                            AME=pesos, nameGroup=banco davivienda, netAmount=10.0}
         */

        val request =
                OrderFeeRequest.newBuilder()
                        .setIdBranchPay(viewModel.location.idbranchpaymentlocation)
                        //.setIdPayment(if (card.tipoTarjeta == TipoTarjetaEntity.CREDITO) "K" else "D")
                        .setIdModePay(viewModel.paymentMode.paymentModeId)
                        .setNetAmount(viewModel.montoEnvio.toString())
                        .build()

        Timber.d("Peticion fees: %s", request)

        presenter.getOrderFees(request)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.view_viamericas_cash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        unbinder = ButterKnife.bind(this, view)
        detallesView.adapter = transactionElementAdapter
        detallesView.layoutManager = LinearLayoutManager(view.context)

        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
        presenter.getCurrentAddress(containerActivity, locationData)

        AndroidUtils.setText(nombreTil.editText, getHolderName(viewModel.recipient))
        AndroidUtils.setText(amountTil.editText, currencyFormat.format(viewModel.montoEnvio))
    }

    override fun onDestroyView() {
        unbinder.unbind()
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun postAddress(address: Address) {
        this.currentAddress = address
    }

    override fun postOrderCreated(orderResponse: OrderResponse) {
        if (isVisible) {
            if (orderResponse.idError == 0) {

                val confirmViewModel = ViamericasConfirmModel(
                        viewModel.recipient,
                        viewModel.montoEnvio,
                        viewModel.currencies,
                        viewModel.location,
                        viewModel.paymentMode,
                        viewModel.exchangeRate,
                        orderFee,
                        orderResponse,
                        card
                )

                val fragment = ViamericasConfirmFragment.get(confirmViewModel)
                containerActivity.fragmentManagerLazy
                        .beginTransaction()
                        .add(R.id.frame_viamericas, fragment)
                        .hide(this)
                        .addToBackStack(null)
                        .setCustomAnimations(
                                android.R.anim.slide_in_left,
                                android.R.anim.slide_out_right
                        )
                        .commit()
            } else {
                showError(orderResponse.mensajeError)
            }
        }
    }

    override fun setFees(body: OrderFeeModel) {
        orderFee = body

        Timber.d("OrderFee: %s", orderFee.toString())

        transactionElementAdapter.update(
                viewModel.montoEnvio, orderFee, viewModel.currencies, viewModel.exchangeRate,
                currencyFormat
        )
    }

    override fun showProgress() {
        containerActivity.showProgress()
    }

    override fun hideProgress() {
        containerActivity.hideProgress()
    }

    override fun showError(msg: String) {
        containerActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerActivity.showSuccess(msg)
    }

    @OnClick(R.id.b_viamericas_pagar)
    fun clickPagar() {
        val order = OrderModel.Builder().setAccountReceiver("")
                .setAddressReceiver(StringUtil.removeAcentos(viewModel.recipient.address))
                .setAmount(viewModel.montoEnvio)
                .setIdBranchReciever(viewModel.location.idbranchpaymentlocation)
                .setIdCityRecivier(viewModel.recipient.idCity)
                .setIdCountryReciever(viewModel.recipient.idCountry)
                .setIdRecipient(viewModel.recipient.idRecipient)
                .setIdSender(viewModel.recipient.idSender)
                .setIdStateReceiver(viewModel.recipient.idState)
                .setModePayReciever(viewModel.paymentMode.paymentModeId)
                .setModPayCurrencyReceiver(viewModel.currencies[viewModel.currencies.size - 1].id)
                .setRecipientFirstName(StringUtil.removeAcentosMultiple(viewModel.recipient.fName))
                .setRecipientLastName(StringUtil.removeAcentosMultiple(viewModel.recipient.lName))
                .setIdUsuario(usuario.ideUsuario)
                .setLongitude(presenter.trimLocation(currentAddress.longitude))
                .setLatitude(presenter.trimLocation(currentAddress.latitude))
                .setPlaceName(
                        StringUtil.removeAcentosMultiple(
                                Strings.nullToEmpty(
                                        currentAddress.getAddressLine(0)
                                )
                        )
                )
                .setPhoneType("ANDROID")
                .setSoftware(Build.VERSION.RELEASE)
                .setAppVersion(BuildConfig.APPLICATION_ID)
                .build()
        presenter.createOrder(order)
    }

    private fun getHolderName(recipient: RecipientResponse): String {
        return Strings.nullToEmpty(recipient.fName) + " " + Strings.nullToEmpty(
                recipient.lName
        )
    }

    companion object {
        fun get(bundle: Bundle): ViamericasCashFragment {
            val fragment = ViamericasCashFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
