package addcel.mobilecard.ui.usuario.main.contacto;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 21/04/17.
 */
@Module
public final class ContactoModule {
    private final ContactoFragment fragment;

    ContactoModule(ContactoFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MainActivity provideActivity() {
        return (MainActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    @Named("idPais")
    int provideIdPais() {
        return Objects.requireNonNull(fragment.getArguments()).getInt("idPais");
    }

    @PerFragment
    @Provides
    ContactoAdapter provideAdapter() {
        return new ContactoAdapter();
    }

    @PerFragment
    @Provides
    ContactoContract.Presenter providePresenter(SessionOperations session) {
        return new ContactoPresenter(fragment, session.getUsuario());
    }
}
