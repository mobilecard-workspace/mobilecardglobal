package addcel.mobilecard.ui.usuario.blackstone.carriers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.blackstone.model.BSCarrierEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import addcel.mobilecard.ui.usuario.blackstone.products.BSProductFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * ADDCEL on 09/07/18.
 */
public class BSCarrierFragment extends Fragment implements BSCarrierContract.View {
    @BindView(R.id.recycler_bs_carriers)
    RecyclerView recyclerView;

    @Inject
    BlackstoneActivity activity;
    @Inject
    BSCarrierContract.Presenter presenter;
    @Inject
    BSCarrierAdapter adapter;
    private Unbinder unbinder;

    public BSCarrierFragment() {
    }

    public static synchronized BSCarrierFragment get(String countryCode) {
        Bundle bundle = new Bundle();
        bundle.putString("countryCode", countryCode);
        BSCarrierFragment carrierFragment = new BSCarrierFragment();
        carrierFragment.setArguments(bundle);
        return carrierFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .bsCarrierSubcomponent(new BSCarrierModule(this))
                .inject(this);

        if (adapter.getItemCount() == 0) presenter.getCarriers();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_bs_carriers, container, false);
        unbinder = ButterKnife.bind(this, view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(recyclerView)
                .setOnItemClickListener((recyclerView, position, v) -> onCarrierClicked(position));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(recyclerView);
        recyclerView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void setCarriers(@NonNull List<BSCarrierEntity> carriers) {
        if (isVisible()) adapter.update(carriers);
    }

    @Override
    public void onCarrierClicked(int pos) {
        BSCarrierEntity item = adapter.getItem(pos);
        activity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.frame_blackstone, BSProductFragment.get(item))
                .hide(this)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        activity.showSuccess(msg);
    }
}
