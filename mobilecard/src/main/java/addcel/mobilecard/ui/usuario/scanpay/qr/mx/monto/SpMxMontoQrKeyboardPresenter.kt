package addcel.mobilecard.ui.usuario.scanpay.qr.mx.monto

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2020-01-17.
 */
interface SpMxMontoQrKeyboardPresenter {

    fun getComision(idEstablecimiento: Int): Disposable

}

class SpMxMontoQrKeyboardPresenterImpl(val api: SPApi, val session: SessionOperations, val view: SpMxMontoQrKeyboardView) : SpMxMontoQrKeyboardPresenter {


    override fun getComision(idEstablecimiento: Int): Disposable {
        view.disableContinuar()
        view.showProgress()
        return api.checkComision(BuildConfig.ADDCEL_APP_ID, session.usuario.idPais,
                StringUtil.getCurrentLanguage(), idEstablecimiento.toLong())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    view.enableContinuar()
                    if (it.idError == 0) {
                        view.setComision(it)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    view.hideProgress()
                    view.enableContinuar()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
    }

}