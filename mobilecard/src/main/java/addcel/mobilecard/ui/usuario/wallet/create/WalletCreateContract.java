package addcel.mobilecard.ui.usuario.wallet.create;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;

import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormModel;

/**
 * ADDCEL on 10/10/17.
 */
public interface WalletCreateContract {
    interface View extends Validator.ValidationListener, DatePickerDialog.OnDateSetListener {

        int REQUEST_CODE = 1;

        String getAlias();

        boolean isPrimary();

        String getCardHolderName();

        String getCard();

        void validateCardPan(CharSequence s);

        String getCvv();

        void clickScan();

        boolean longClickScan();

        void clearCardData();

        void clickVigencia();

        String getVigencia();

        TipoTarjetaEntity getTipoTarjeta();

        String getAddress1();

        String getAddress2();

        String getZipCode();

        void clickSave();

        void showProgress();

        void hideProgress();

        void showExpirationDialog();

        void dismissExpirationDialog();

        void showError(int resId);

        void showError(String msg);

        void showSuccess(String msg);

        void updateCards(List<CardEntity> models);

        int getSelectedState();

        void updateStates(List<EstadoResponse.EstadoEntity> states);

        String getDirUsa();

        String getCityUsa();

        String getZipUsa();
    }

    interface Presenter {

        TebcaFormModel buildModel();

        boolean isTebca(String pan);

        void clearDisposables();

        String getCardHolderName();

        Intent buildCardIOIntent(Intent intent, int accentColor, String instruction);

        void create();

        Bitmap parseImage(byte[] data);

        int getIdPais();

        void getEstados();
    }
}
