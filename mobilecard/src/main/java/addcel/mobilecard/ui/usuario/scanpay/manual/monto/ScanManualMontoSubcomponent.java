package addcel.mobilecard.ui.usuario.scanpay.manual.monto;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 23/08/18.
 */
@PerFragment
@Subcomponent(modules = ScanManualMontoModule.class)
public interface ScanManualMontoSubcomponent {
    void inject(ScanManualMontoFragment fragment);
}
