package addcel.mobilecard.ui.usuario.apto.user.address

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.apto.user.*
import addcel.mobilecard.data.net.wallet.AptoUserRequest
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.ui.usuario.apto.cards.created.CardCreatedActivity
import addcel.mobilecard.ui.usuario.apto.cards.created.CardCreatedModel
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_user_address.*
import javax.inject.Inject

@Parcelize
data class AptoUserAddressModel(
        val phone: String, val phoneVerification: String,
        val email: String, val emailVerification: String, val firstName: String, val lastName: String,
        val dob: String, val ssn: String
) : Parcelable

class AptoUserAddressActivity : AppCompatActivity() {
    val api = UserAPI.get()
    val compositeDisposable = CompositeDisposable()

    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var wallet: WalletAPI
    lateinit var model: AptoUserAddressModel

    lateinit var insertResponse: UserInsertResponse

    companion object {

        //["Alabama","Alaska","Arizona","Arkansas","California","Carolina del Norte","Carolina del Sur","Colorado","Connecticut","Dakota del Norte","Dakota del Sur","Delaware","Distrito de Columbia","Florida","Fuerzas Armadas de América","Fuerzas Armadas de Europa","Fuerzas Armadas del Pacífico","Georgia","Hawai","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Luisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","Nueva Hampshire","Nueva Jersey","Nueva York","Nuevo México","Ohio","Oklahoma","Oregón","Pensilvania","Rhode Island","Tennessee","Texas","Utah","Vermont","Virginia","Virginia Occidental","Washington","Wisconsin","Wyoming"]
        //["AL","AK","AZ","AR","CA","NC","SC","CO","CT","ND","SD","DE","DC","FL","AA","AE","AP","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NY","NM","OH","OK","OR","PA","RI","TN","TX","UT","VT","VA","WV","WA","WI","WY"]

        val STATES_MAP = mapOf(
                "Alabama" to "AL",
                "Alaska" to "AK",
                "Arizona" to "AZ",
                "Arkansas" to "AR",
                "California" to "CA",
                "Carolina del Norte" to "NC",
                "Carolina del Sur" to "SC",
                "Colorado" to "CO",
                "Connecticut" to "CT",
                "Dakota del Norte" to "ND",
                "Dakota del Sur" to "SD",
                "Delaware" to "DE",
                "Distrito de Columbia" to "DC",
                "Florida" to "FL",
                "Fuerzas Armadas de América" to "AA",
                "Fuerzas Armadas de Europa" to "AE",
                "Fuerzas Armadas del Pacífico" to "AP",
                "Georgia" to "GA",
                "Hawai" to "HI",
                "Idaho" to "ID",
                "Illinois" to "IL",
                "Indiana" to "IN",
                "Iowa" to "IA",
                "Kansas" to "KS",
                "Kentucky" to "KY",
                "Luisiana" to "LA",
                "Maine" to "ME",
                "Maryland" to "MD",
                "Massachusetts" to "MA",
                "Michigan" to "MI",
                "Minnesota" to "MN",
                "Mississippi" to "MS",
                "Missouri" to "MO",
                "Montana" to "MT",
                "Nebraska" to "NE",
                "Nevada" to "NV",
                "Nueva Hampshire" to "NH",
                "Nueva Jersey" to "NJ",
                "Nueva York" to "NY",
                "Nuevo México" to "NM",
                "Ohio" to "OH",
                "Oklahoma" to "OK",
                "Oregón" to "OR",
                "Pensilvania" to "PA",
                "Rhode Island" to "RI",
                "Tennessee" to "TN",
                "Texas" to "TX",
                "Utah" to "UT",
                "Vermont" to "VT",
                "Virginia" to "VA",
                "Virginia Occidental" to "WV",
                "Washington" to "WA",
                "Wisconsin" to "WI",
                "Wyoming" to "WY"
        )


        fun get(context: Context, model: AptoUserAddressModel): Intent {
            return Intent(context, AptoUserAddressActivity::class.java).putExtra("model", model)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get().netComponent.inject(this)

        setContentView(R.layout.activity_apto_user_address)

        model = intent?.getParcelableExtra("model")!!

        fillStateSpinner()

        b_shift_adress_verification_continuar.setOnClickListener {
            validate()
        }

        apto_user_address_retry.findViewById<Button>(R.id.b_retry).setOnClickListener {
            onRetry()
        }
    }

    private fun showRetry() {
        apto_user_address_retry.visibility = View.VISIBLE
    }

    private fun hideRetry() {
        apto_user_address_retry.visibility = View.GONE
    }

    private fun onRetry() {
        if (::insertResponse.isInitialized) {
            compositeDisposable.add(insertAptoUserInMc(insertResponse))
        }
    }

    private fun validate() {
        val address = AndroidUtils.getText(text_shift_address_verification_address)
        val state = STATES_MAP[(spinner_shift_address_verification_state.selectedItem as String)]
                ?: error("")
        val city = AndroidUtils.getText(text_shift_address_verification_city)
        val zip = AndroidUtils.getText(text_shift_address_verification_zip)

        if (address.isEmpty() || state.isEmpty() || city.isEmpty() || zip.isEmpty()) {
            Toasty.error(this, "Verifica que los datos sean correctos").show()
        } else {
            launchCreation()
        }
    }

    private fun launchCreation() {
        hideRetry()
        compositeDisposable.add(
                api.user(buildRequest()).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({
                    insertResponse = it
                    compositeDisposable.add(insertAptoUserInMc(insertResponse))
                }, { t ->
                    val code = ErrorUtil.getFormattedHttpErrorCode(t)
                    val msg: String
                    msg = when (code) {
                        400 -> "Verifique que todos los datos sean válidos para continuar."
                        401 -> "La sesión expiró al intentar crear un usuario con los mismos datos nuevamente."
                        404 -> "No existe la verificación."
                        else -> ErrorUtil.getFormattedHttpErrorMsg(t)
                    }

                    Toasty.error(
                            this@AptoUserAddressActivity, msg
                    ).show()
                })
        )
    }

    private fun insertAptoUserInMc(insertResponse: UserInsertResponse): Disposable {
        hideRetry()
        val aptoUser = AptoUserRequest(
                session.usuario.ideUsuario,
                insertResponse.userId,
                model.phoneVerification,
                model.emailVerification,
                insertResponse.userToken
        )
        return wallet.saveInfoApto(
                BuildConfig.ADDCEL_APP_ID,
                session.usuario.idPais,
                StringUtil.getCurrentLanguage(),
                aptoUser
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.idError == 0) {
                        Toasty.success(this@AptoUserAddressActivity, "Usuario creado exitosamente")
                                .show()
                        startActivity(
                                CardCreatedActivity.get(
                                        this@AptoUserAddressActivity,
                                        CardCreatedModel(insertResponse.userToken)
                                )
                        )
                    } else {
                        showRetry()
                        Toasty.error(
                                this@AptoUserAddressActivity,
                                "Error desconocido contacte a un administrador."
                        ).show()
                    }
                }, {
                    showRetry()
                    Toasty.error(this@AptoUserAddressActivity, ErrorUtil.getFormattedHttpErrorMsg(it))
                            .show()
                })
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun buildRequest(): UserInsertRequest {
        val nameMap = mapOf(
                "data_type" to UserAPI.DATA_TYPE_NAME,
                "first_name" to model.firstName,
                "last_name" to model.lastName
        )
        val documentMap = mapOf(
                "data_type" to UserAPI.DATA_TYPE_DOCUMENT,
                "doc_type" to "ssn",
                "value" to model.ssn,
                "country" to "US"
        )
        val phoneMap = mapOf(
                "data_type" to UserAPI.DATA_TYPE_PHONE,
                "country_code" to "1",
                "phone_number" to model.phone,
                "verification" to UserVerification(model.phoneVerification)
        )
        val dateMap = mapOf("data_type" to UserAPI.DATA_TYPE_BIRTHDATE, "date" to model.dob)
        val emailMap = mapOf(
                "data_type" to UserAPI.DATA_TYPE_EMAIL,
                "email" to model.email,
                "verification" to UserVerification(model.emailVerification)
        )

        val addressMap = buildAddressMap()

        val dataPoints = UserDataPoints(
                "list",
                listOf(nameMap, documentMap, phoneMap, dateMap, emailMap, addressMap)
        )

        return UserInsertRequest(dataPoints)
    }

    private fun buildAddressMap(): Map<String, Any> {
        val address = AndroidUtils.getText(text_shift_address_verification_address)
        val address2 = AndroidUtils.getText(text_shift_address_verification_address_2)
        val country = "US"
        val state = STATES_MAP[(spinner_shift_address_verification_state.selectedItem as String)]
                ?: error("")
        val city = AndroidUtils.getText(text_shift_address_verification_city)
        val zip = AndroidUtils.getText(text_shift_address_verification_zip)

        /*
     {“data_type”: “address”, “locality”: “San Francisco”, “street_one”: “1310 Fillmore St.“,“postal_code”: “94115", “street_two”: “”, “region”: “CA”, “country”: “US”}
     */
        return mapOf(
                "data_type" to UserAPI.DATA_TYPE_ADDRESS,
                "locality" to city,
                "street_one" to address,
                "street_two" to address2,
                "region" to state,
                "country" to country,
                "postal_code" to zip
        )
    }

    private fun fillStateSpinner() {
        val states = STATES_MAP.keys.toList()
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, states)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        spinner_shift_address_verification_state.adapter = adapter
    }
}

/*
 ‘{ “data_points”: { “type”: “list”, “data”: [ {“data_type”: “name”, “first_name”: “Sandor”, “last_name”: “Clegane”},
                    {“data_type”: “id_document”, “doc_type”: “ssn”, “value”: “111110000”, “country”: “US”},
                    {“data_type”: “address”, “locality”: “San Francisco”, “street_one”: “1310 Fillmore St.“,“postal_code”: “94115", “street_two”: “”, “region”: “CA”, “country”: “US”},
                   { “data_type”: “phone”, “country_code”: “1", “phone_number”: “7143082239", “verification”: { “verification_source”: “experian” } },
                   { “data_type”: “birthdate”, “date”: “1900-01-01” },
                    { “data_type”: “email”, “email”: “test@email.com” }]}}’`1
 */