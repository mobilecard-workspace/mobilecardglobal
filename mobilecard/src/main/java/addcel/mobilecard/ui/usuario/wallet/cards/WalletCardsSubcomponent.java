package addcel.mobilecard.ui.usuario.wallet.cards;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/10/17.
 */
@PerFragment
@Subcomponent(modules = WalletCardsModule.class)
public interface WalletCardsSubcomponent {
    void inject(WalletCardsFragment fragment);
}
