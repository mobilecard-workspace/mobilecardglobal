package addcel.mobilecard.ui.usuario.legacy.servicios.categorias

import addcel.mobilecard.di.scope.PerFragment
import dagger.Subcomponent

/**
 * ADDCEL on 2019-06-15.
 */
@PerFragment
@Subcomponent(modules = [CategoriaListModule::class])
interface CategoriaListSubcomponent {
    fun inject(fragment: CategoriaListFragment)
}