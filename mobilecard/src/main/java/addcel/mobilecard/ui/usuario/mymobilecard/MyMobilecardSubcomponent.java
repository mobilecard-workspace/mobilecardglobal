package addcel.mobilecard.ui.usuario.mymobilecard;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = MyMobilecardModule.class)
public interface MyMobilecardSubcomponent {
    void inject(MyMobilecardActivity activity);
}
