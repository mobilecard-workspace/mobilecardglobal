package addcel.mobilecard.ui.usuario.menu

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.usuarios.UsuariosService
import addcel.mobilecard.di.scope.PerView
import addcel.mobilecard.domain.usuario.bottom.BottomMenuInteractor
import addcel.mobilecard.domain.usuario.bottom.BottomMenuInteractorImpl
import com.squareup.otto.Bus
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 2019-06-14.
 */
@Module
class BottomMenuModule {

    @PerView
    @Provides
    fun providesInteractor(
            api: UsuariosService, session: SessionOperations,
            bus: Bus
    ): BottomMenuInteractor {

        return if (session.usuario == null) BottomMenuInteractorImpl(
                api,
                bus,
                0,
                CompositeDisposable()
        )
        else BottomMenuInteractorImpl(
                api, bus, session.usuario.ideUsuario,
                CompositeDisposable()
        )
    }

    @PerView
    @Provides
    fun providesPresenter(interactor: BottomMenuInteractor): Presenter {
        return MenuLayoutPresenter(interactor)
    }
}