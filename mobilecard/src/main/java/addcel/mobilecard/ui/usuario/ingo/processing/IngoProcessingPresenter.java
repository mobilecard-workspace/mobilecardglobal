package addcel.mobilecard.ui.usuario.ingo.processing;

import android.annotation.SuppressLint;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.ingo.model.IngoCardRequest;
import addcel.mobilecard.data.net.ingo.model.IngoCardResponse;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.ingo.processing.IngoProcessingInteractor;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 19/07/18.
 */
class IngoProcessingPresenter implements IngoProcessingContract.Presenter {
    private final IngoUserData ingoData;
    private final TokenizerAPI token;
    private final CardEntity card;
    private final String ingoLocationError;
    private IngoProcessingInteractor interactor;
    private IngoProcessingContract.View view;

    IngoProcessingPresenter(IngoProcessingInteractor interactor, IngoUserData ingoData,
                            TokenizerAPI token, CardEntity card, String ingoLocationError, IngoProcessingContract.View view) {
        this.interactor = interactor;
        this.ingoData = ingoData;
        this.token = token;
        this.card = card;
        this.ingoLocationError = ingoLocationError;
        this.view = view;
    }

    @Override
    public void onDestroy() {
        interactor.clearDisposables();
        view = null;
    }

    @SuppressLint("CheckResult")
    @Override
    public void addOrUpdateTarjeta() {
        view.showProgress();
        IngoCardRequest request = new IngoCardRequest.Builder().withAddressLine1(card.getDomAmex())
                .withAddressLine2("")
                .withCardNickname(interactor.getUsuario().getUsrNombre() + "_" + card.getTipo().getDescription())
                .withCardNumber(card.getPan())
                .withCity(interactor.getUsuario().getUsrCiudad())
                .withCustomerId(ingoData.getCustomerId())
                .withDiviceId(view.getImei())
                .withExpirationMonthYear(card.getVigencia())
                .withNameOnCard(card.getNombre())
                .withState(String.valueOf(interactor.getUsuario().getUsrIdEstado()))
                .withZip(card.getCpAmex())
                .build();

        interactor.addOrUpdateCard(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request,
                new InteractorCallback<IngoCardResponse>() {
                    @Override
                    public void onSuccess(@NotNull IngoCardResponse result) {
                        view.hideProgress();
                        view.onTarjetaAddedOrUpdated();
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                        view.onTarjetaNotAdded();
                    }
                });
    }

    @Override
    public void checkIfIngoAvailable() {
        view.showProgress();
        interactor.checkIfIngoActive(new InteractorCallback<IngoAliveResponse>() {
            @Override
            public void onSuccess(@NotNull IngoAliveResponse result) {
                view.hideProgress();
                if (result.getServiceAvailable()) {
                    view.launchIngo();
                } else {
                    view.showCountryError(result.getMessage());
                }
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showCountryError(ingoLocationError);
            }
        });
    }

    @Override
    public String getCustomerId() {
        return ingoData.getCustomerId();
    }

    @Override
    public String getSsoToken() {
        return ingoData.getSsoToken();
    }

    @Override
    public String getScanReference() {
        return interactor.getUsuario().getScanReference();
    }

    @SuppressLint("CheckResult")
    @Override
    public void checkWhiteList() {
        view.showProgress();
        token.isOnWhiteList(BuildConfig.ADDCEL_APP_ID, interactor.getUsuario().getIdPais(), StringUtil.getCurrentLanguage(), interactor.getUsuario().getIdeUsuario())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tokenBaseResponse -> {
                    view.hideProgress();
                    if (tokenBaseResponse.getCode() == 0) {
                        view.onWhiteList();
                    } else {
                        view.notOnWhiteList();
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.showError(ErrorUtil.Companion.getFormattedHttpErrorMsg(throwable));
                });
    }
}
