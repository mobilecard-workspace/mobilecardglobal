package addcel.mobilecard.ui.usuario.main.menu;

import androidx.annotation.NonNull;

import java.util.Locale;

import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.ui.usuario.main.MainContract;

/**
 * ADDCEL on 04/10/17.
 */
class MenuPresenter implements MenuContract.Presenter {

    private final MainContract.Presenter mainActivityPresenter;

    MenuPresenter(MainContract.Presenter mainActivityPresenter) {
        this.mainActivityPresenter = mainActivityPresenter;
    }

    @Override
    public void launchView(MenuModel model) {
        mainActivityPresenter.verifyAndLaunchUseCase(model.useCase);
    }

    @Override
    public void updateDrawer() {
        mainActivityPresenter.setDrawerByCountry();
    }

    @Override
    public String getNombre() {

        String name = "";

        try {
            name = String.format(Locale.getDefault(), "%s %s", mainActivityPresenter.getUsuarioName(),
                    mainActivityPresenter.getUsuarioApellidos());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return name;
    }

    @Override
    public int getIdPais() {
        return mainActivityPresenter.getIdPais();
    }

    @Override
    public void activate() {
        mainActivityPresenter.resendActivationLink();
    }

    @Override
    public void showIngo() {
        if (getIdPais() == 3) {
            mainActivityPresenter.checkIfIngoAlive(new InteractorCallback<IngoAliveResponse>() {
                @Override
                public void onSuccess(@NonNull IngoAliveResponse result) {
                }

                @Override
                public void onError(@NonNull String error) {
                }
            });
        }
    }

    @Override
    public void setLaunched() {
        mainActivityPresenter.setMenuLaunched(Boolean.TRUE);
    }

    @Override
    public void loadProfileDelegate() {
        mainActivityPresenter.loadProfile();
    }
}
