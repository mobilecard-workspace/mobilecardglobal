package addcel.mobilecard.ui.usuario.apto.verifications.email.capture

import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.verification.DataPointType
import addcel.mobilecard.data.net.apto.verification.Verification
import addcel.mobilecard.data.net.apto.verification.VerificationAPI
import addcel.mobilecard.data.net.apto.verification.VerificationResponse
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.apto.verifications.email.code.AptoEmailVerificationCodeActivity
import addcel.mobilecard.ui.usuario.apto.verifications.email.code.AptoEmailVerificationCodeModel
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.Retrofit2HttpErrorHandling
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_email_verification_capture.*

@Parcelize
data class AptoEmailVerificationCaptureModel(
        val phone: String,
        val result: VerificationResponse
) : Parcelable

interface AptoEmailVerificationCaptureView : ScreenView, TextWatcher {
    fun launchVerification()

    fun onVerification(result: VerificationResponse)
}

class AptoEmailVerificationCaptureActivity : AppCompatActivity(), AptoEmailVerificationCaptureView {

    companion object {

        fun enableButton(vararg tils: TextInputLayout): Boolean {
            var enabled = true
            for (t: TextInputLayout in tils) {
                if (t.error != null && AndroidUtils.getText(t.editText).isNotEmpty()) {
                    enabled = false
                    break
                }
            }
            return enabled
        }

        fun get(context: Context, model: AptoEmailVerificationCaptureModel): Intent {
            return Intent(context, AptoEmailVerificationCaptureActivity::class.java).putExtra(
                    "model",
                    model
            )
        }
    }

    val api = VerificationAPI.get()
    val compositeDisposable = CompositeDisposable()
    lateinit var model: AptoEmailVerificationCaptureModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_email_verification_capture)
        model = intent?.getParcelableExtra("model")!!
        text_shift_email_verification_email.addTextChangedListener(this)
        b_shift_email_verification_continuar.setOnClickListener { launchVerification() }
        b_shift_email_verification_continuar.isEnabled = false
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun launchVerification() {

        val email = AndroidUtils.getText(til_shift_email_verification_email.editText)
        compositeDisposable.add(
                api.start(Verification(DataPointType.email, mapOf("email" to email))).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe(
                        { r -> onVerification(r) }, { t ->
                    val errorRes =
                            Retrofit2HttpErrorHandling.getBodyFromErrorBody(
                                    t,
                                    VerificationResponse::class.java
                            )
                    if (errorRes != null) onVerification(errorRes)
                    else showError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })

        )
    }

    override fun onVerification(result: VerificationResponse) {
        if (result.status == VerificationAPI.STATUS_PENDING) {
            showSuccess(result.status)

            val pModel = AptoEmailVerificationCodeModel(
                    AndroidUtils.getText(til_shift_email_verification_email.editText),
                    result,
                    model.phone,
                    model.result.verificationId
            )

            startActivity(AptoEmailVerificationCodeActivity.get(this, pModel))
        } else {
            showError("${result.type} ${result.status}")
        }
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (til_shift_email_verification_email.hasFocus()) {
                if (!Patterns.EMAIL_ADDRESS.matcher(p0).matches()) {
                    til_shift_email_verification_email.error = "Enter a valid email address"
                } else {
                    til_shift_email_verification_email.error = null
                }
            }
            b_shift_email_verification_continuar.isEnabled =
                    enableButton(til_shift_email_verification_email)
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}
