package addcel.mobilecard.ui.usuario.wallet.select;

import android.annotation.SuppressLint;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.wallet.select.WalletSelectInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 13/10/17.
 */
public class WalletSelectPresenter implements WalletSelectContract.Presenter {
    private final WalletSelectInteractor interactor;
    private final WalletSelectContract.View view;

    WalletSelectPresenter(WalletSelectInteractor interactor, WalletSelectContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCards() {
        view.showProgress();
        interactor.list(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<CardEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<CardEntity> result) {
                        view.hideProgress();
                        view.onCardsUpdated(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showRetry();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }
}
