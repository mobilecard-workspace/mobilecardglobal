package addcel.mobilecard.ui.usuario.mymobilecard;

import androidx.annotation.NonNull;

import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormCallback;

/**
 * ADDCEL on 17/11/17.
 */

public interface MyMobilecardContract {
    interface View extends TebcaFormCallback {

        CatalogoService getCatalogo();

        void onHasPrevivale(@NonNull CardEntity model);

        void onNoPrevivale();
    }
}
