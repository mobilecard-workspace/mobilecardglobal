package addcel.mobilecard.ui.usuario.colombia.multimarket

import addcel.mobilecard.R
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.lista.MmProductosListFragment
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.categorias.MmCategoriasFragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_colombia_servicios.*

class ColombiaServiciosActivity : ContainerActivity() {

    companion object {

        const val USE_CASE_PRODUCTOS = 1
        const val USE_CASE_SERVICIOS = 2

        fun start(context: Context, useCase: Int) {
            val intent = Intent(context, ColombiaServiciosActivity::class.java).putExtra("useCase", useCase)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_colombia_servicios)
        setSupportActionBar(toolbar_colombia_servicios)
        val useCase = intent.getIntExtra("useCase", USE_CASE_PRODUCTOS)
        launchView(useCase)
    }

    override fun getRetry(): View? {
        return retry_colombia_servicios
    }

    override fun hideProgress() {
        if (progress_colombia_servicios.visibility == View.VISIBLE) progress_colombia_servicios.visibility = View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_colombia_servicios.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_colombia_servicios.title = title
    }

    override fun showRetry() {
        if (retry != null && retry!!.visibility == View.GONE) retry!!.visibility = View.VISIBLE
    }

    override fun showProgress() {
        if (progress_colombia_servicios.visibility == View.GONE) progress_colombia_servicios.visibility = View.VISIBLE
    }

    override fun hideRetry() {
        if (retry != null && retry!!.visibility == View.VISIBLE) retry!!.visibility = View.GONE
    }

    private fun launchView(useCase: Int) {
        if (useCase == USE_CASE_PRODUCTOS) {
            launchProductos()
        } else {
            launchServicios()
        }
    }

    private fun launchProductos() {
        setAppToolbarTitle(R.string.nav_col_prepago)
        fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, MmProductosListFragment())
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    private fun launchServicios() {
        setAppToolbarTitle(R.string.txt_nav_servicios)
        fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, MmCategoriasFragment())
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }
}
