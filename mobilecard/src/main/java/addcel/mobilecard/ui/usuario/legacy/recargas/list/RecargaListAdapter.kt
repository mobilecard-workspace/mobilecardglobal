package addcel.mobilecard.ui.usuario.legacy.recargas.list

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.utils.MapUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

/**
 * ADDCEL on 2019-06-15.
 */
class RecargaListAdapter(val data: List<CatalogoResponse.RecargaEntity>) :
        RecyclerView.Adapter<RecargaListAdapter.ViewHolder>() {

    companion object {
        fun getIcons(): Map<Int, Int> {
            return mapOf(1 to R.drawable.b_rec_aire, 2 to R.drawable.b_rec_peaje)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_categoria, parent, false)
        )
    }

    fun getItem(position: Int): CatalogoResponse.RecargaEntity {
        return data[position]
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entity = data[position]
        holder.icon.setImageResource(
                MapUtils.getOrDefault(getIcons(), entity.id, R.drawable.b_rec_aire)
        )
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val icon: ImageView = view.findViewById(R.id.icon_cat)
    }
}