package addcel.mobilecard.ui.usuario.scanpay.secure;

/**
 * ADDCEL on 08/11/17.
 */
final class ScanSecureConstants {
    static final String LOG_FUN = "javascript:window.HTMLOUT.log("
            + "'<html>'"
            + "+document.getElementsByTagName('html')[0].innerHTML+"
            + "'</html>'"
            + ");";
    static final String FUN_JSON_PROCESS =
            "javascript:window.HTMLOUT.showSuccess(document.getElementsByTagName('body')[0].innerHTML);";
    static final String FORCED_USER_AGENT =
            "User-Agent:Mozilla/5.0 (Linux; Android 4.4.2; SM-T230 Build/KOT49H) AppleWebKit/537.36"
                    + "(KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Safari/537.36";
}
