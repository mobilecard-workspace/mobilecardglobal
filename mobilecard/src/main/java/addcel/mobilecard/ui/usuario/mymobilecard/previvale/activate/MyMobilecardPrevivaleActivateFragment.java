package addcel.mobilecard.ui.usuario.mymobilecard.previvale.activate;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.McRegexPatterns;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.display.MyMobilecardDisplayFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.StringUtil;
import addcel.mobilecard.validation.ValidationUtils;
import mx.mobilecard.crypto.AddcelCrypto;

import static addcel.mobilecard.data.net.catalogo.model.EstadoResponse.EstadoEntity;

/**
 * ADDCEL on 11/16/18.
 */
public class MyMobilecardPrevivaleActivateFragment extends Fragment
        implements MyMobilecardPrevivaleActivateContract.View {

    @Inject
    MyMobilecardActivity activity;
    @Inject
    Validator validator;
    @Inject
    ArrayAdapter<EstadoEntity> adapter;
    @Inject
    MyMobilecardPrevivaleActivateContract.Presenter presenter;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout nombreTil;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout paternoTil;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout maternoTil;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout direccionTil;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout coloniaTil;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout ciudadTil;

    @Pattern(regex = "^[0-9]{5}$", messageResId = R.string.error_default)
    TextInputLayout cpTil;

    @Pattern(regex = "([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?)?", messageResId = R.string.error_rfc)
    TextInputLayout rfcTil;

    @Pattern(regex = "[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]", messageResId = R.string.error_rfc_curp)
    TextInputLayout curpTil;

    @Pattern(regex = "(^(?=\\d{16}$)(506450)\\d+)?")
    private EditText panText;

    private Spinner estadoSpinner;
    private Button curpButton;
    private Button activateButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .myMobilecardPrevivaleActivateSubcomponent(new MyMobilecardPrevivaleActivateModule(this))
                .inject(this);

        if (adapter.getCount() == 0) presenter.getEstados();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen_previvale_activate, container, false);
        v.findViewById(R.id.label_previvale_activate).setVisibility(View.GONE);
        v.findViewById(R.id.label_previvale_activate_pan).setVisibility(View.GONE);
        panText = v.findViewById(R.id.text_previvale_activate_pan);
        nombreTil = v.findViewById(R.id.til_previvale_add_nombre);
        paternoTil = v.findViewById(R.id.til_previvale_add_paterno);
        maternoTil = v.findViewById(R.id.til_previvale_add_materno);
        direccionTil = v.findViewById(R.id.til_previvale_add_direccion);
        coloniaTil = v.findViewById(R.id.til_previvale_add_colonia);
        ciudadTil = v.findViewById(R.id.til_previvale_add_ciudad);
        estadoSpinner = v.findViewById(R.id.spinner_previvale_add_estado);
        estadoSpinner.setAdapter(adapter);
        cpTil = v.findViewById(R.id.til_previvale_add_cp);
        rfcTil = v.findViewById(R.id.til_previvale_add_rfc);
        curpTil = v.findViewById(R.id.til_previvale_add_curp);

        panText.addTextChangedListener(this);

        curpButton = v.findViewById(R.id.b_previvale_activate_curp);
        curpButton.setOnClickListener(v1 -> clickCurp());

        activateButton = v.findViewById(R.id.b_previvale_activate);
        activateButton.setOnClickListener(v1 -> clickActivate());
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_FINISHED);
        panText.setVisibility(View.GONE);
        nombreTil.setVisibility(View.VISIBLE);
        paternoTil.setVisibility(View.VISIBLE);
        maternoTil.setVisibility(View.VISIBLE);
        AndroidUtils.setText(nombreTil.getEditText(), presenter.getNombre());
        AndroidUtils.setText(paternoTil.getEditText(), presenter.getPaterno());
        AndroidUtils.setText(maternoTil.getEditText(), presenter.getMaterno());
    }

    @Override
    public void onDestroyView() {
        panText.removeTextChangedListener(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        if (isVisible()) activateButton.setEnabled(true);
        activity.showError(msg);
    }

    @Override
    public void finishActivity() {
        activity.finish();
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void setEstados(List<EstadoEntity> estados) {
        adapter.addAll(estados);
    }

    @Override
    public void onPanCaptured(CharSequence digit) {
        if (Strings.isNullOrEmpty(digit.toString())) {
            panText.setError(null);
            panText.setTextColor(Color.BLACK);
            panText.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
            activateButton.setEnabled(true);
        } else if (java.util.regex.Pattern.matches(McRegexPatterns.PREVIVALE, digit)) {
            panText.setError(null);
            panText.setTextColor(Color.BLACK);
            setCardDrawable(panText);
            activateButton.setEnabled(true);
        } else {
            panText.setTextColor(Color.RED);
            panText.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
            panText.setError(getText(R.string.error_tarjeta));
            activateButton.setEnabled(false);
        }
    }

    @Override
    public void clickCurp() {
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(CURP_URL));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            showError(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private void setCardDrawable(EditText text) {
        text.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.card_carnet, 0, 0, 0);
    }

    @Override
    public void clickActivate() {
        ValidationUtils.clearViewErrors(panText, direccionTil, coloniaTil, ciudadTil, cpTil, rfcTil,
                curpTil, curpTil);
        validator.validate();
    }

    @Override
    public void onActivated(CardEntity response) {
        activateButton.setEnabled(true);
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_mymobilecard, MyMobilecardDisplayFragment.Companion.get(response))
                .hide(this)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @Override
    public void onValidationSucceeded() {

        PrevivaleUserRequest request = new PrevivaleUserRequest(0L,
                StringUtil.removeAcentosMultiple(AndroidUtils.getText(nombreTil.getEditText())),
                StringUtil.removeAcentosMultiple(AndroidUtils.getText(paternoTil.getEditText())),
                StringUtil.removeAcentosMultiple(AndroidUtils.getText(maternoTil.getEditText())),
                StringUtil.removeAcentosMultiple(AndroidUtils.getText(direccionTil.getEditText())),
                StringUtil.removeAcentosMultiple(AndroidUtils.getText(coloniaTil.getEditText())),
                StringUtil.removeAcentosMultiple(AndroidUtils.getText(ciudadTil.getEditText())),
                AndroidUtils.getText(cpTil.getEditText()),
                AndroidUtils.getSelectedItem(estadoSpinner, EstadoEntity.class).getId(),
                AndroidUtils.getText(rfcTil.getEditText()), AndroidUtils.getText(curpTil.getEditText()),
                BuildConfig.ADDCEL_APP_ID, AddcelCrypto.encryptHard(panText.getText().toString().trim()));

        presenter.onClickActivate(request);
        activateButton.setEnabled(false);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationUtils.onValidationFailed(activity, errors);
        activateButton.setEnabled(true);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        onPanCaptured(s);
    }
}
