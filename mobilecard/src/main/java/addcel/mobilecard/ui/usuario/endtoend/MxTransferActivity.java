package addcel.mobilecard.ui.usuario.endtoend;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.collect.Lists;
import com.squareup.otto.Bus;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.ui.ContainerActivity;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add.MxTransferBeneficiarioAddFragment;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select.MxTransferBeneficiarioSelectEvent;
import addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select.MxTransferBeneficiarioSelectFragment;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.ListUtil;
import addcel.mobilecard.utils.ScreenshotUtil;
import io.reactivex.disposables.CompositeDisposable;

public class MxTransferActivity extends ContainerActivity {
    private final CompositeDisposable disposables = new CompositeDisposable();
    @Inject
    Bus bus;
    private ProgressBar progressBar;
    private List<AccountEntity> cuentas;
    private RxPermissions permissions;
    private View retry;

    public static synchronized Intent get(Context context) {
        return new Intent(context, MxTransferActivity.class);
    }

    public static synchronized Intent getforKeyBoard(Context context, boolean fromKeyboard) {
        Intent intent =
                new Intent(context, MxTransferActivity.class).putExtra("fromKeyboard", Boolean.TRUE);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().inject(this);
        setContentView(R.layout.activity_mx_transfer);

        permissions = new RxPermissions(this);

        FloatingActionButton shareButton = findViewById(R.id.b_success_share);

        shareButton.setOnClickListener(
                v -> disposables.add(
                        permissions.request(ScreenshotUtil.Companion.getPERMISSIONS())
                                .subscribe(aBoolean -> {
                                    if (aBoolean) {
                                        Intent intent =
                                                ScreenshotUtil.Companion.shareScreenshot(MxTransferActivity.this, getString(R.string.txt_transaction_share));
                                        if (intent != null) {
                                            shareScreenshot(intent);
                                        }
                                    } else {
                                        showError(getString(R.string.txt_ingo_permission_retry));
                                    }
                                })
                ));

        if (getIntent().getBooleanExtra("fromKeyboard", Boolean.FALSE)) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_transfermx,
                            MxTransferBeneficiarioAddFragment.getForKeyboard(Boolean.TRUE))
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_transfermx, new MxTransferBeneficiarioSelectFragment())
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (progressBar == null) {
            progressBar = findViewById(R.id.progress_transfermx);
        }
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (progressBar == null) {
            progressBar = findViewById(R.id.progress_transfermx);
        }
        progressBar.setVisibility(View.GONE);
    }

    public List<AccountEntity> getCuentas() {
        if (cuentas == null) cuentas = Lists.newArrayList();
        return cuentas;
    }

    public void setCuentas(List<AccountEntity> cuentas) {
        if (ListUtil.notEmpty(this.cuentas)) this.cuentas.clear();
        this.cuentas.addAll(cuentas);
    }

    public void updateBeneficiarios(List<AccountEntity> accounts) {
        getSupportFragmentManager().popBackStack();
        bus.post(new MxTransferBeneficiarioSelectEvent(accounts));
    }

    @Override
    public void setAppToolbarTitle(int title) {
        ((Toolbar) findViewById(R.id.toolbar_transfermx)).setTitle(title);
    }

    @Override
    public void setAppToolbarTitle(@NonNull String title) {
        ((Toolbar) findViewById(R.id.toolbar_transfermx)).setTitle(title);
    }

    @Override
    public void showRetry() {
        if (getRetry() != null && getRetry().getVisibility() == View.GONE)
            getRetry().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        if (getRetry() != null && getRetry().getVisibility() == View.VISIBLE)
            getRetry().setVisibility(View.GONE);
    }

    @Override
    public @Nullable View getRetry() {
        if (retry == null)
            retry = findViewById(R.id.retry_transfermx);

        return retry;
    }

    private void shareScreenshot(@NotNull Intent intent) {
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            showError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.OPERATION));
        }
    }
}
