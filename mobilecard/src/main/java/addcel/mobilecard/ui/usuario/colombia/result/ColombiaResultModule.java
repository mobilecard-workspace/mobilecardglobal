package addcel.mobilecard.ui.usuario.colombia.result;

import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import addcel.mobilecard.data.net.colombia.entity.ColombiaPagoResponseEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.ContainerActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 10/29/18.
 */
@Module
public final class ColombiaResultModule {
    private final ColombiaResultFragment fragment;

    ColombiaResultModule(ColombiaResultFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ContainerActivity provideActivity() {
        return (ContainerActivity) Objects.requireNonNull(fragment.getActivity());
    }


    @PerFragment
    @Provides
    @Nullable SPReceiptEntity provideRecargaResult() {
        return fragment.getArguments().getParcelable("recargaResult");
    }

    @PerFragment
    @Provides
    ColombiaResultContract.Presenter providePresenter(@Nullable SPReceiptEntity recargaResult) {
        return new ColombiaResultPresenter(recargaResult);
    }
}
