package addcel.mobilecard.ui.usuario.viamericas.order

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.viamericas.recipient.select.ViamericasRecipientFragment
import addcel.mobilecard.ui.usuario.viamericas.recipient.select.ViamericasRecipientModel
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.validation.ValidationUtils
import android.app.AlertDialog
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.*
import com.google.android.material.textfield.TextInputLayout
import com.google.common.base.Strings
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.Checked
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Select
import com.squareup.phrase.Phrase
import kotlinx.android.parcel.Parcelize
import timber.log.Timber
import java.text.NumberFormat
import javax.inject.Inject
import javax.inject.Named

/**
 * ADDCEL on 26/03/17.
 */


@Parcelize
data class ViamericasOrderModel(val idSender: String) : Parcelable

class ViamericasOrderFragment : Fragment(), VmOrderView {
    //STRINGS
    @BindString(R.string.txt_viamericas_bank)
    lateinit var bankTitle: String
    @BindString(R.string.txt_viamericas_cash)
    lateinit var cashTitle: String
    @BindString(R.string.txt_viamericas_receiver_info)
    lateinit var recipientBase: String
    @BindString(R.string.txt_viamericas_dummyMsg)
    lateinit var dummyMsg: String
    @BindString(R.string.txt_viamericas_cambio)
    lateinit var cambioBase: String

    //VIEWS
    @BindView(R.id.container_mode)
    lateinit var modeContainer: LinearLayout
    @BindView(R.id.container_montos)
    lateinit var montosContainer: LinearLayout
    @Select(messageResId = R.string.error_viamericas_pais)
    @BindView(R.id.spinner_viamericas_order_pais)
    lateinit var paisSpinner: Spinner
    @BindView(R.id.recycler_modes)
    lateinit var modesRecycler: RecyclerView
    @BindView(R.id.view_viamericas_order_branch_title)
    lateinit var branchTitle: TextView
    @NotEmpty(trim = true, messageResId = R.string.error_cantidad)
    @BindView(R.id.til_viamericas_order_send)
    lateinit var sendCurrText: TextInputLayout
    @BindView(R.id.til_viamericas_order_receive)
    lateinit var receiveCurrText: TextInputLayout
    @Select(messageResId = R.string.error_viamericas_sucursal)
    @BindView(R.id.spinner_viamericas_receiver_branch)
    lateinit var branchSpinner: Spinner
    @BindView(R.id.view_exchange_rate)
    lateinit var exchangeView: TextView
    @Checked(messageResId = R.string.error_viamericas_terms)
    @BindView(R.id.check_viamericas_terms)
    lateinit var termsCheck: CheckBox
    @Checked(messageResId = R.string.error_viamericas_privacy)
    @BindView(R.id.check_viamericas_privacy)
    lateinit var privacyCheck: CheckBox

    //DEPENDENCIAS
    @Inject
    lateinit var containerActivity: ContainerActivity
    @Inject
    lateinit var model: ViamericasOrderModel
    @Inject
    lateinit var allRes: Resources
    @Inject
    lateinit var validator: Validator
    @Inject
    lateinit var infoDialog: AlertDialog

    @field:[Inject Named("viamericasCountryAdapter")]
    lateinit var viamericasCountryAdapter: ArrayAdapter<VmCountryModel>

    @Inject
    lateinit var modeAdapter: ViamericasModeAdapter

    @field:[Inject Named("viamericasBranchAdapter")]
    lateinit var viamericasBranchAdapter: ArrayAdapter<LocationModel>

    @Inject
    lateinit var currencies: MutableList<CurrencyModel>
    @Inject
    lateinit var presenter: ViamericasOrderPresenter
    @Inject
    lateinit var currencyFormat: NumberFormat

    private lateinit var unbinder: Unbinder
    private var exchangeRate: ExchangeRateModel? = null
    private var countryPos: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get()
                .netComponent
                .viamericasOrderSubcomponent(ViamericasOrderModule(this))
                .inject(this)
        presenter.getCountries(false)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return LayoutInflater.from(context)
                .inflate(R.layout.view_viamericas_order, container, false)
    }


    @OnItemSelected(
            value = [R.id.spinner_viamericas_order_pais],
            callback = OnItemSelected.Callback.ITEM_SELECTED
    )
    fun onPaisSelected(pos: Int) {
        if (pos > 0 && countryPos != pos) {
            countryPos = pos
            val paramModel = viamericasCountryAdapter.getItem(pos)
            if (paramModel != null) presenter.getCurrencies(paramModel)
            hideModeView()
        }
    }

    @OnItemSelected(
            value = [R.id.spinner_viamericas_receiver_branch],
            callback = OnItemSelected.Callback.ITEM_SELECTED
    )
    fun onBranchSelected(pos: Int) {
        if (pos > 0) {
            showProgress()
            montosContainer.visibility = View.GONE
            presenter.getExchangeRate(
                    modeAdapter.getItem(modeAdapter.selectedItems[0]),
                    currencies[currencies.size - 1],
                    branchSpinner.getItemAtPosition(pos) as LocationModel
            )
        }
    }

    @OnClick(value = [R.id.b_viamericas_order_branch_address])
    fun showLocationAddress() {
        if (viamericasBranchAdapter.count > 0 && branchSpinner.selectedItemPosition > 0) {
            val location = branchSpinner.selectedItem as LocationModel
            if (!Strings.isNullOrEmpty(location.addressBranch)) {
                AlertDialog.Builder(context).setTitle(R.string.txt_viamericas_receiver_address)
                        .setMessage(location.addressBranch)
                        .show()
            }
        }
    }

    @OnTextChanged(
            value = [R.id.text_viamericas_order_send],
            callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED
    )
    fun onAmountCaptured(string: CharSequence) {
        if (!Strings.isNullOrEmpty(string.toString())) {
            val conversion = java.lang.Double.parseDouble(string.toString())
            if (exchangeRate != null) {
                when {
                    conversion < exchangeRate!!.minimumToSend -> sendCurrText.error =
                            getString(R.string.error_viamericas_send_larger)
                    conversion > exchangeRate!!.maximumToSend -> sendCurrText.error =
                            getString(R.string.error_viamericas_send_smaller)
                    else -> AndroidUtils.setText(
                            receiveCurrText.editText,
                            currencyFormat.format(conversion * exchangeRate!!.exchangeRate)
                                    .replace("$", "")
                    )
                }
            }
        } else {
            AndroidUtils.setText(
                    receiveCurrText.editText,
                    currencyFormat.format(0.0).replace("$", "")
            )
        }
    }

    @OnClick(R.id.b_viamericas_terms)
    fun terms() {
        val webpage = Uri.parse(TERMS_PATH)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(containerActivity.packageManager) != null) {
            startActivity(intent)
        }
    }

    @OnClick(R.id.b_viamericas_privacy)
    fun privacy() {
        val webpage = Uri.parse(PRIVACY_PATH)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(containerActivity.packageManager) != null) {
            startActivity(intent)
        }
    }

    @OnClick(R.id.b_viamericas_pagar)
    fun onClick() {
        validator.validate()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)

        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
        paisSpinner.adapter = viamericasCountryAdapter
        branchSpinner.adapter = viamericasBranchAdapter
        val manager = GridLayoutManager(context, 2)
        modesRecycler.layoutManager = manager
        modesRecycler.adapter = modeAdapter
        modesRecycler.setHasFixedSize(true)
        ItemClickSupport.addTo(modesRecycler).setOnItemClickListener { _, position, v ->
            modeAdapter.toggleSelection(position)
            branchTitle.text =
                    if (modeAdapter.getItem(position).paymentModeId == "C") bankTitle else cashTitle
            viamericasBranchAdapter.clear()
            presenter.getBranches(
                    paisSpinner.selectedItem as VmCountryModel,
                    currencies[currencies.size - 1], modeAdapter.getItem(position)
            )
        }


        montosContainer.visibility = View.GONE
        if (countryPos == 0) {
            receiveCurrText.hint = Phrase.from(containerActivity, R.string.txt_viamericas_receive)
                    .put("currency", "")
                    .format()
            hideModeView()
            disableInitialTexts()
        }
    }

    override fun onDestroyView() {
        unbinder.unbind()
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun onValidationSucceeded() {
        if (isVisible) {
            Timber.d("Valor monto texto: %s", AndroidUtils.getText(sendCurrText.editText))
            sendCurrText.error = null
            when {
                modeAdapter.selectedItemCount <= 0 -> {
                    showError(getString(R.string.error_viamericas_paymode))
                }
                viamericasBranchAdapter.count <= 0 -> {
                    showError(getString(R.string.error_viamericas_locations_empty))
                }
                else -> {
                    startReceiverFragment()
                }
            }
        }
    }

    override fun onValidationFailed(errors: List<ValidationError>) {
        ValidationUtils.onValidationFailed(containerActivity, errors)
    }

    private fun showModeView() {
        if (isVisible) {
            modeContainer.visibility = View.VISIBLE
            modeAdapter.clearSelection()
            AndroidUtils.setTextEmpty(sendCurrText.editText)
            AndroidUtils.setTextEmpty(receiveCurrText.editText)
        }
    }

    private fun hideModeView() {
        if (isVisible) {
            modeContainer.visibility = View.GONE
        }
    }

    private fun startReceiverFragment() {

        val country = paisSpinner.selectedItem as VmCountryModel
        val paymentMode = modeAdapter.getItem(modeAdapter.selectedItems[0])
        val location = branchSpinner.selectedItem as LocationModel
        val sendAmountStr = AndroidUtils.getText(sendCurrText.editText)
        val sendAmount = java.lang.Double.parseDouble(sendAmountStr)

        if (exchangeRate != null) {

            val vrModel = ViamericasRecipientModel(
                    model.idSender,
                    country,
                    currencies,
                    paymentMode,
                    location,
                    sendAmount,
                    exchangeRate!!
            )

            containerActivity.fragmentManagerLazy.commit {
                addToBackStack(null)
                hide(this@ViamericasOrderFragment)
                add(R.id.frame_viamericas, ViamericasRecipientFragment.get(vrModel))
                setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            }
        }
    }

    override fun showProgress() {
        containerActivity.showProgress()
    }

    override fun hideProgress() {
        containerActivity.hideProgress()
    }

    override fun showError(msg: String) {
        containerActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerActivity.showSuccess(msg)
    }

    @OnClick(R.id.b_viamericas_order_info)
    override fun clickInfo() {
        infoDialog.show()
    }

    override fun postCountries(countries: List<VmCountryModel>) {
        if (isVisible) {
            viamericasCountryAdapter.add(
                    VmCountryModel(
                            "-1",
                            allRes.getString(R.string.txt_viamericas_selecciona)
                    )
            )
            viamericasCountryAdapter.addAll(countries)
        }
    }

    override fun postCurrencies(currencies: List<CurrencyModel>) {
        this.currencies.clear()
        this.currencies.addAll(currencies)
        if (isVisible) {
            if (this.currencies.size >= 2) {
                receiveCurrText.hint =
                        Phrase.from(containerActivity, R.string.txt_viamericas_receive)
                                .put("currency", "(" + this.currencies[1].name + ")")
                                .format()
                presenter.getModes(paisSpinner.selectedItem as VmCountryModel, this.currencies[1])
            } else if (this.currencies.size == 1) {
                receiveCurrText.hint =
                        Phrase.from(containerActivity, R.string.txt_viamericas_receive)
                                .put("currency", "(" + this.currencies[0].name + ")")
                                .format()
                presenter.getModes(paisSpinner.selectedItem as VmCountryModel, this.currencies[0])
            }
        }
    }

    override fun postModes(paymentModes: List<PaymentModeModel>) {
        modeAdapter.update(paymentModes)
        if (isVisible) showModeView()
    }

    override fun postBranches(locations: List<LocationModel>) {
        if (isVisible) {
            viamericasBranchAdapter.clear()
            viamericasBranchAdapter.add(
                    LocationModel("-1", allRes.getString(R.string.txt_viamericas_selecciona))
            )
            viamericasBranchAdapter.addAll(locations)
            branchSpinner.setSelection(0)
        }
    }

    override fun postExchangeRate(exchangeRateResponse: ExchangeRateResponse) {
        if (exchangeRateResponse.idError == 0) {
            exchangeRate = exchangeRateResponse.exchangeRateRes[0]
            val ratemsg = Phrase.from(cambioBase)
                    .put("send_monto", currencyFormat.format(1))
                    .put("send_curr", currencies[0].name)
                    .put(
                            "receive_monto", currencyFormat.format(
                            exchangeRateResponse.exchangeRateRes[0].exchangeRate
                    )
                    )
                    .put("receive_curr", currencies[currencies.size - 1].name)
                    .format()
            if (isVisible) {
                exchangeView.text = ratemsg
                montosContainer.visibility = View.VISIBLE
            }
        } else {
            exchangeRate = null
            showError(exchangeRateResponse.mensajeError)
        }
    }

    override fun disableInitialTexts() {
        sendCurrText.isEnabled = false
        receiveCurrText.isEnabled = false
    }

    override fun enableTexts() {
        sendCurrText.isEnabled = true
        receiveCurrText.isEnabled = true
    }

    override fun disableOnCallStarted() {
        if (view != null) {
            paisSpinner.isEnabled = false
            modesRecycler.isEnabled = false
            AndroidUtils.setTextEmpty(sendCurrText.editText)
            AndroidUtils.setTextEmpty(receiveCurrText.editText)
            receiveCurrText.hint = Phrase.from(containerActivity, R.string.txt_viamericas_receive)
                    .put("currency", "")
                    .format()
            branchSpinner.isEnabled = false
        }
    }

    override fun enableOnCallFinished() {
        if (view != null) {
            paisSpinner.isEnabled = true
            modesRecycler.isEnabled = true
            branchSpinner.isEnabled = true
        }
    }

    companion object {
        private const val PRIVACY_PATH =
                "https://www.mobilecard.mx/AddcelContent/files/docs/viamericas_privacy.pdf"
        private const val TERMS_PATH =
                "https://www.mobilecard.mx/AddcelContent/files/docs/viamericas_terms.pdf"

        fun get(model: ViamericasOrderModel): ViamericasOrderFragment {
            val bundle = Bundle()
            bundle.putParcelable("model", model)
            val fragment = ViamericasOrderFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
