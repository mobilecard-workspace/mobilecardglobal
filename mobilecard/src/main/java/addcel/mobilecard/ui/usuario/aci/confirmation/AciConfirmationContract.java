package addcel.mobilecard.ui.usuario.aci.confirmation;

import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.ui.ScreenView;
import addcel.mobilecard.ui.Authenticable;
import io.reactivex.annotations.NonNull;

/**
 * ADDCEL on 17/10/17.
 */
public interface AciConfirmationContract {
    interface View extends ScreenView, Authenticable {

        String getImei();

        void clickPagar();

        void onPagoSuccess(@NonNull McResponse response);
    }

    interface Presenter {

        void clearDisposables();

        String getAccount();

        double getMonto();

        double getFee();

        double getTotal();

        void pagar();
    }
}
