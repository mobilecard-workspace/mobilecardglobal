package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.compra

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.McConstants
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.*
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.PurchaseScreenView
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.result.MmProductosResultFragment
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_scanpay_qr_confirm.*
import mx.mobilecard.crypto.AddcelCrypto
import javax.inject.Inject

/**
 * ADDCEL on 2019-12-20.
 */

@Parcelize
data class MmServiciosConfirmViewModel(val cat: Categoria,
                                       val servicio: Servicio,
                                       val saldo: MmConsultaResponse,
                                       val card: CardEntity) : Parcelable

interface MmServiciosConfirmView : PurchaseScreenView {
    fun setInfo()
    fun launchLoadCard(card: CardEntity)
    fun onPagar(result: MmPagoResponse)
}


class MmServiciosConfirmFragment : Fragment(), MmServiciosConfirmView {


    companion object {
        fun get(args: Bundle): MmServiciosConfirmFragment {
            val frag = MmServiciosConfirmFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var cActivity: ContainerActivity
    @Inject
    lateinit var viewModel: MmServiciosConfirmViewModel
    @Inject
    lateinit var presenter: MmServiciosConfirmPresenter
    @Inject
    lateinit var picasso: Picasso


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.mmServiciosConfirmSubcomponent(MmServiciosConfirmModule(this)).inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_scanpay_qr_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInfo()
        launchLoadCard(viewModel.card)
        b_scanpay_qr_confirm_continuar.setOnClickListener {
            clickPurchaseButton()
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!isHidden) enablePurchaseButton()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun setInfo() {
        cActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)


        val invoice = getString(
                R.string.txt_multimarket_confirm_servicio_invoice,
                viewModel.saldo.reference,
                McConstants.CO_CURR_FORMAT.format(viewModel.saldo.monto),
                McConstants.CO_CURR_FORMAT.format(viewModel.saldo.comision),
                viewModel.servicio.name + " - " + viewModel.servicio.productId.toString()
        )

        view_scanpay_qr_confirm_data.text = invoice
        view_scanpay_qr_confirm_total.text = String.format("Total: %s", McConstants.CO_CURR_FORMAT.format(viewModel.saldo.monto + viewModel.saldo.comision))
    }

    override fun launchLoadCard(card: CardEntity) {
        picasso.load(card.imgShort)
                .placeholder(R.drawable.bg_visa)
                .placeholder(R.drawable.bg_visa)
                .fit()
                .into(bg_scan_card, object : Callback {
                    override fun onSuccess() {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }

                    override fun onError(e: Exception) {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }
                })
    }

    override fun onPagar(result: MmPagoResponse) {
        cActivity.fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, MmProductosResultFragment.get(result, viewModel.card, viewModel.saldo.monto))
            hide(this@MmServiciosConfirmFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }

        enablePurchaseButton()
    }

    override fun isPurchaseButtonEnabled(): Boolean {
        return isVisible && b_scanpay_qr_confirm_continuar.isEnabled
    }

    override fun disablePurchaseButton() {
        if (isVisible) b_scanpay_qr_confirm_continuar.isEnabled = false
    }

    override fun enablePurchaseButton() {
        if (isVisible) b_scanpay_qr_confirm_continuar.isEnabled = true
    }

    override fun clickPurchaseButton() {
        if (isPurchaseButtonEnabled()) {

            disablePurchaseButton()

            val request = MmPagoServicioEntity(viewModel.saldo.barcode,
                    BuildConfig.ADDCEL_APP_ID,
                    viewModel.servicio.productId,
                    McConstants.PAIS_ID_CO,
                    viewModel.card.idTarjeta,
                    presenter.getIdUsuario(),
                    StringUtil.getCurrentLanguage(),
                    viewModel.saldo.reference,
                    viewModel.saldo.comision,
                    viewModel.saldo.idReferencia,
                    viewModel.saldo.monto)

            presenter.pagarServicio(request)
        }
    }

    override fun showSuccess(msg: String) {
        cActivity.showSuccess(msg)
    }

    override fun showError(msg: String) {
        cActivity.showError(msg)
    }

    override fun hideProgress() {
        cActivity.hideProgress()
    }

    override fun showProgress() {
        cActivity.showProgress()
    }

    override fun onWhiteList() {

    }

    override fun notOnWhiteList() {

    }
}