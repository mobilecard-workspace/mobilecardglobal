package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search.notsearchable

import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Producto
import addcel.mobilecard.data.net.colombia.multimarket.Servicio
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

/**
 * ADDCEL on 2019-12-18.
 */
class MmServicioNoSearchAdapter(val data: MutableList<Servicio> = ArrayList()) : RecyclerView.Adapter<MmServicioNoSearchAdapter.ViewHolder>() {

    fun update(args: List<Servicio>) {
        data.clear()
        data.addAll(args)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_mm_producto, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val servicio: Servicio? = data[position]
        if (servicio != null) {
            holder.text.text = servicio.name
        }
    }

    fun getItem(position: Int): Servicio {
        return data[position]
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val text: TextView = v.findViewById(R.id.text_mm_item)
    }

}