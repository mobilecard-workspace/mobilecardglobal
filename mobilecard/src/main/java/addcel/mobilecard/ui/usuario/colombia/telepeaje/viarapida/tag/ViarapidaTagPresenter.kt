package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.tag

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.colombia.VrTag
import addcel.mobilecard.data.net.colombia.VrTagRequest
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 9/26/19.
 */
interface ViarapidaTagPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getTags(idUsuario: Long)

    fun saveTag(request: VrTagRequest)

    fun deleteTag(idUsuario: Long, tag: VrTag)
}

class ViarapidaTagPresenterImpl(
        val middleware: ColombiaMiddleware,
        val disposables: CompositeDisposable, val view: ViarapidaTagView
) : ViarapidaTagPresenter {
    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getTags(idUsuario: Long) {
        view.showProgress()
        val sDisp = middleware.listTags(
                ColombiaMiddleware.getAuth(), BuildConfig.ADDCEL_APP_ID, 2,
                StringUtil.getCurrentLanguage(), idUsuario
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    view.hideProgress()
                    if (r.codigo == 0) {
                        view.onListTags(r.tags)
                    } else {
                        view.showError(r.mensaje)
                    }
                }, { t ->
                    view.hideProgress()
                    view.showError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })

        addToDisposables(sDisp)
    }

    override fun saveTag(request: VrTagRequest) {
        view.showProgress()
        val sDisp =
                middleware.saveTag(ColombiaMiddleware.getAuth(), request).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                            view.hideProgress()
                            if (r.codigo == 0) {
                                view.onTagSaved(r.tag)
                            } else {
                                view.showError(r.mensaje)
                            }
                        }, { t ->
                            view.hideProgress()
                            view.showError(t.localizedMessage
                                    ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                        })

        addToDisposables(sDisp)
    }

    override fun deleteTag(idUsuario: Long, tag: VrTag) {
        val request = VrTagRequest(
                tag.alias, BuildConfig.ADDCEL_APP_ID, 2, idUsuario,
                StringUtil.getCurrentLanguage(), tag.placa, tag.tagId, tag.idViaTag
        )

        val dDisp =
                middleware.deleteTag(ColombiaMiddleware.getAuth(), request).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                            view.hideProgress()
                            if (r.codigo == 0) {
                                view.onTagDeleted(tag)
                                view.showSuccess(r.mensaje)
                            } else {
                                view.showError(r.mensaje)
                            }
                        }, { t ->
                            view.hideProgress()
                            view.showError(t.localizedMessage
                                    ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                        })

        addToDisposables(dDisp)
    }
}