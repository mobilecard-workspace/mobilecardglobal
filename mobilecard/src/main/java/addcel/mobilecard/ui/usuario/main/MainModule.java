package addcel.mobilecard.ui.usuario.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.widget.Toolbar;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.squareup.otto.Bus;

import java.util.Locale;
import java.util.Objects;
import java.util.Stack;

import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.domain.usuario.menu.MenuInteractor;
import addcel.mobilecard.domain.usuario.menu.MenuInteractorImpl;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 28/10/16.
 */
@Module
public final class MainModule {
    private final MainActivity activity;

    MainModule(MainActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    @Named("fromLogin")
    boolean fromLogin() {
        try {
            return Objects.requireNonNull(activity.getIntent().getExtras())
                    .getBoolean("fromLogin", false);
        } catch (NullPointerException e) {
            return false;
        }
    }

    @PerActivity
    @Provides
    @Named("initialUseCase")
    int provideInitialUseCase() {
        try {
            return Objects.requireNonNull(activity.getIntent().getExtras())
                    .getInt("initialUseCase", MainContract.UseCase.INICIO);
        } catch (NullPointerException e) {
            return MainContract.UseCase.INICIO;
        }
    }

    @PerActivity
    @Provides
    ProgressDialog provideProgress() {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    @PerActivity
    @Provides
    @Named("IngoAliveRetrofitMain")
    Retrofit provideIngoRetrofit(
            Retrofit retrofit) {
        return retrofit.newBuilder().baseUrl("https://api.spykemobile.net/").build();
    }

    @PerActivity
    @Provides
    IngoAliveAPI provideIngoAliveApi(
            @Named("IngoAliveRetrofitMain") Retrofit retrofit) {
        return IngoAliveAPI.Companion.get(retrofit);
    }

    @PerActivity
    @Provides
    MenuInteractor provideInteractor(UsuariosService mobilecardService,
                                     CatalogoService catalogoService, IngoAliveAPI ingoAliveAPI, WalletAPI walletService,
                                     SessionOperations session, StateSession state, Bus bus) {
        return new MenuInteractorImpl(mobilecardService, catalogoService, ingoAliveAPI, walletService,
                session, state, bus);
    }

    @PerActivity
    @Provides
    MainContract.Presenter providePresenter(MenuInteractor interactor) {
        return new MainPresenter(interactor, activity);
    }

    @PerActivity
    @Provides
    @Named("activateDialog")
    AlertDialog provideActivateDialog(
            MainContract.Presenter presenter) {
        DetachableClickListener dcListenerOk =
                DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.resendActivationLink();
                    }
                });

        DetachableClickListener dcListenerCancel =
                DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        final AlertDialog dialog =
                new AlertDialog.Builder(activity).setTitle(android.R.string.dialog_alert_title)
                        .setMessage(activity.getString(R.string.txt_account_activation))
                        .setPositiveButton(activity.getString(R.string.txt_account_activation_btn),
                                dcListenerOk)
                        .setNegativeButton(activity.getString(android.R.string.cancel), dcListenerCancel)
                        .create();

        dcListenerOk.clearOnDetach(dialog);
        dcListenerCancel.clearOnDetach(dialog);
        return dialog;
    }

    @PerActivity
    @Provides
    @Named("profileItem")
    PrimaryDrawerItem provideProfileItem(
            SessionOperations sessionOperations) {

        String name = "";
        if (sessionOperations.isUsuarioLogged() && sessionOperations.getUsuario().getUsrNombre() != null)
            name = sessionOperations.getUsuario().getUsrNombre().toUpperCase(Locale.getDefault());

        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.PROFILE)
                .withName(name)
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withIcon(R.drawable.icon_settings);
    }

    @PerActivity
    @Provides
    @Named("inicioItem")
    PrimaryDrawerItem provideInicioItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.INICIO)
                .withName(res.getString(R.string.nav_home).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("recargasItem")
    PrimaryDrawerItem provideRecargaItem(
            Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.RECARGAS)
                .withName(res.getString(R.string.nav_topups).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("serviciosItem")
    PrimaryDrawerItem provideServicioItem(
            Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.SERVICIOS)
                .withName(res.getString(R.string.nav_billpayments).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("serviciosUsItem")
    PrimaryDrawerItem provideServicioUsItem(
            Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.SERVICIOS)
                .withName(res.getString(R.string.nav_billpayments_mx).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("walletItem")
    PrimaryDrawerItem provideWalletItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.WALLET)
                .withName(res.getString(R.string.nav_wallet).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark);
    }

    @PerActivity
    @Provides
    @Named("edocuentaItem")
    PrimaryDrawerItem provideEdocuentaItem(
            Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.EDOCUENTA)
                .withName(res.getString(R.string.nav_accounthistory).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("checkItem")
    PrimaryDrawerItem provideCheckItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.CHEQUE)
                .withName(res.getString(R.string.nav_cashyourcheck).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("transferItem")
    PrimaryDrawerItem provideTransferItem(
            Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.TRANSFERENCIAS)
                .withName(res.getString(R.string.nav_transfer_mx).toUpperCase())
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("frecuentesItem")
    PrimaryDrawerItem provideFrecuentesItem(
            Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.FRECUENTES)
                .withName(res.getString(R.string.nav_favorites).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("scanItem")
    PrimaryDrawerItem provideScanItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.SCAN)
                .withName(res.getString(R.string.nav_scanpay).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("escaneaItem")
    PrimaryDrawerItem provideEscaneaItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.SCAN)
                .withName(res.getString(R.string.nav_escanea).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }


    @PerActivity
    @Provides
    @Named("mobileTagItem")
    PrimaryDrawerItem provideMobiletagItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.MOBILE_TAG)
                .withName(res.getString(R.string.nav_mobiletag).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }


    @PerActivity
    @Provides
    @Named("peajeColItem")
    PrimaryDrawerItem provideColPeajeItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.COL_PEAJE)
                .withName(res.getString(R.string.nav_col_peaje).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("soatColItem")
    PrimaryDrawerItem provideColSoatItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.COL_SOAT)
                .withName(res.getString(R.string.nav_col_soat).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("taeColItem")
    PrimaryDrawerItem provideColTaeItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.COL_TAE)
                .withName(res.getString(R.string.nav_col_tae).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("prepagoColItem")
    PrimaryDrawerItem provideColPrepagoItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.COL_PREPAGOS)
                .withName(res.getString(R.string.nav_col_prepago).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("myMcItem")
    PrimaryDrawerItem provideMyMcItem(Resources res) {
        return new PrimaryDrawerItem().withIdentifier(MainContract.UseCase.MY_MC)
                .withName(res.getString(R.string.txt_menu_mymobilecard).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground);
    }

    @PerActivity
    @Provides
    @Named("dividerItem")
    DividerDrawerItem provideDivider() {
        return new DividerDrawerItem().withIdentifier(MainContract.UseCase.DIVIDER);
    }

    @PerActivity
    @Provides
    @Named("contactoItem")
    SecondaryDrawerItem provideContactoItem(
            Resources res) {
        return new SecondaryDrawerItem().withIdentifier(MainContract.UseCase.CONTACTO)
                .withName(res.getString(R.string.nav_contactus).toUpperCase())
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withIcon(R.drawable.icon_info_white);
    }

    @PerActivity
    @Provides
    @Named("logoutItem")
    SecondaryDrawerItem provideLogoutIten(Resources res) {
        return new SecondaryDrawerItem().withIdentifier(MainContract.UseCase.LOGOUT)
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withName(res.getString(R.string.nav_logout).toUpperCase())
                .withIcon(R.drawable.icon_logout);
    }

    @PerActivity
    @Provides
    @Named("versionItem")
    SecondaryDrawerItem provideVersionItem(
            Resources res) {
        return new SecondaryDrawerItem().withIdentifier(MainContract.UseCase.VERSION)
                .withEnabled(false)
                .withDisabledTextColorRes(R.color.colorBackground)
                .withName(res.getString(R.string.version, BuildConfig.VERSION_NAME).toUpperCase());
    }

    @PerActivity
    @Provides
    Toolbar provideToolbar() {
        Toolbar toolbar = activity.findViewById(R.id.toolbar_main);
        activity.setSupportActionBar(toolbar);
        return toolbar;
    }

    @PerActivity
    @Provides
    Stack<Long> provideSelectionStack() {
        return new Stack<>();
    }

    @PerActivity
    @Provides
    Drawer provideDrawerBuilder(Toolbar toolbar) {
        return new DrawerBuilder().withActivity(activity)
                .withToolbar(toolbar)
                .withSliderBackgroundColorRes(R.color.colorAccent)
                .withActionBarDrawerToggle(false)
                .withDrawerGravity(Gravity.END)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        MainModule.this.activity.goTo((int) drawerItem.getIdentifier());
                        return false;
                    }
                })
                .build();
    }

    @PerActivity
    @Provides
    Stack<CharSequence> provideTitleStack() {
        return new Stack<>();
    }
}
