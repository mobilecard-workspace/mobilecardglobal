package addcel.mobilecard.ui.usuario.colombia.soad.info

import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.di.scope.PerFragment
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 10/1/19.
 */
@Module
class ColombiaSoatInfoModule(val fragment: ColombiaSoadInfoFragment) {

    @PerFragment
    @Provides
    fun provideModel(): ColombiaSoadInfoModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideApi(retrofit: Retrofit): ColombiaMiddleware {
        return ColombiaMiddleware.get(retrofit)
    }

    @PerFragment
    @Provides
    fun providePresenter(api: ColombiaMiddleware): ColombiaSoatInfoPresenter {
        return ColombiaSoatInfoPresenterImpl(api, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [ColombiaSoatInfoModule::class])
interface ColombiaSoatInfoSubcomponent {
    fun inject(fragment: ColombiaSoadInfoFragment)
}