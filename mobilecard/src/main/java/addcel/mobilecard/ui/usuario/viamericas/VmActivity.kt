package addcel.mobilecard.ui.usuario.viamericas

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.SenderResponse
import addcel.mobilecard.event.impl.ViamericasShareEvent
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.usuario.viamericas.order.ViamericasOrderFragment
import addcel.mobilecard.ui.usuario.viamericas.order.ViamericasOrderModel
import addcel.mobilecard.ui.usuario.viamericas.sender.ViamericasSenderActivity
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.fragment.app.commit
import com.squareup.otto.Bus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_viamericas.*
import org.threeten.bp.LocalDateTime
import timber.log.Timber
import javax.inject.Inject

/**
 * ADDCEL on 17/05/17.
 */

class VmActivity : ContainerActivity() {
    @Inject
    lateinit var service: ViamericasService
    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var bus: Bus

    private val disposables = CompositeDisposable()


    public override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_viamericas)
            Mobilecard.get().netComponent.inject(this)
            b_success_share.setOnClickListener { bus.post(ViamericasShareEvent()) }
            view_viamericas_retry.findViewById<Button>(R.id.b_retry).setOnClickListener {
                clickRetry()
            }
            setSupportActionBar(toolbar_viamericas)
            if (savedInstanceState == null) loadInitialFragment()
        } catch (t: Throwable) {
            Timber.e(t)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_viamericas, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_share) {
            bus.post(ViamericasShareEvent())
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getRetry(): View? {
        return null
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val shareButton = menu.findItem(R.id.action_share)
        shareButton.isVisible = screenStatus == ContainerScreenView.STATUS_FINISHED
        return true
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun loadInitialFragment() {
        hideRetry()
        showProgress()
        val json = "{\"idUsuario\":\"" + session.usuario.ideUsuario + "\"}"
        Timber.d(json)

        val sDisp = service.validateRecipient(
                ViamericasService.AUTH,
                addcel.mobilecard.BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                json
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            hideProgress()
                            if (it.idError == 0) {
                                fragmentManagerLazy.commit {
                                    add(
                                            R.id.frame_viamericas,
                                            ViamericasOrderFragment.get(ViamericasOrderModel(it.idSender))
                                    )
                                    setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                }
                            } else {
                                startActivityForResult(
                                        ViamericasSenderActivity.get(this),
                                        ViamericasSenderActivity.REQUEST_CODE
                                )
                            }
                        },
                        {
                            hideProgress()
                            showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                            showRetry()
                        }
                )
        disposables.add(sDisp)
    }

    override fun showProgress() {
        if (progress_viamericas.visibility == View.GONE) progress_viamericas.visibility =
                View.VISIBLE
    }

    override fun hideProgress() {
        if (progress_viamericas.visibility == View.VISIBLE) progress_viamericas.visibility =
                View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_viamericas.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_viamericas.title = title
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ViamericasSenderActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val sender: SenderResponse? =
                            data.getParcelableExtra(ViamericasSenderActivity.RESULT_DATA)
                    if (sender != null && sender.idError == 0) {

                        showSuccess(sender.mensajeError)

                        fragmentManagerLazy.commit {
                            add(
                                    R.id.frame_viamericas,
                                    ViamericasOrderFragment.get(ViamericasOrderModel(sender.idSender))
                            )
                            setCustomAnimations(
                                    android.R.anim.fade_in,
                                    android.R.anim.fade_out
                            )
                        }
                    } else {
                        finish()
                    }
                } else {
                    finish()
                }
            } else {
                finish()
            }
        }
    }

    override fun showRetry() {
        if (view_viamericas_retry.visibility == View.GONE) view_viamericas_retry.visibility =
                View.VISIBLE
    }

    override fun hideRetry() {
        if (view_viamericas_retry.visibility == View.VISIBLE) view_viamericas_retry.visibility =
                View.GONE
    }

    private fun clickRetry() {
        loadInitialFragment()
    }

    companion object {
        fun get(context: Context): Intent {
            return Intent(context, VmActivity::class.java)
        }

        fun hasAgeRequirement(dob: LocalDateTime, age: Int): Boolean {
            val curr = LocalDateTime.now()
            return (curr.year - dob.year >= age)

        }
    }
}
