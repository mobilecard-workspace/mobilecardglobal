package addcel.mobilecard.ui.usuario.aci.nodata;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 28/03/17.
 */
public interface AciPagoDatosContract {

    interface View extends ScreenView, Validator.ValidationListener {
        String getIMEI();
    }

    interface Presenter {
        void clearDisposables();

        void getEstados();

        int getIdEstado();

        String getCiudad();

        String getAddress();

        String getZipCode();

        String getSecurityCode();

        void pagar(EstadoResponse.EstadoEntity estado, String ciudad, String zipCode, String direccion,
                   String cvv);
    }
}
