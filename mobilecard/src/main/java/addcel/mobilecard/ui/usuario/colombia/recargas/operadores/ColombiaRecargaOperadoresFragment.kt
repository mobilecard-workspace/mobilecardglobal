package addcel.mobilecard.ui.usuario.colombia.recargas.operadores

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.OperadorRecarga
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.recargas.ColombiaRecargasActivity
import addcel.mobilecard.ui.usuario.colombia.recargas.categorias.ColombiaCategoria
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.ColombiaRecargaPaquetesDatosFragment
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.ColombiaRecargaPaquetesDatosModel
import addcel.mobilecard.ui.usuario.colombia.recargas.tae.seleccion.ColombiaRecargaTaeSeleccionFragment
import addcel.mobilecard.ui.usuario.colombia.recargas.tae.seleccion.ColombiaRecargaTaeSeleccionModel
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.view_categoria.*
import javax.inject.Inject

/**
 * ADDCEL on 9/19/19.
 */

@Parcelize
data class ColombiaRecargaOperadoresModel(val cat: ColombiaCategoria) : Parcelable

interface ColombiaRecargaOperadoresView : ScreenView {
    fun onGetOperadores(operadores: List<OperadorRecarga>)

    fun onGetOperadoresError()

    fun showRetry()

    fun hideRetry()

    fun clickRetry()

    fun clickOperador(operador: OperadorRecarga)

    fun launchTae(operador: OperadorRecarga)

    fun launchDatos(operador: OperadorRecarga)
}

class ColombiaRecargaOperadoresFragment : Fragment(), ColombiaRecargaOperadoresView {

    companion object {
        fun get(model: ColombiaRecargaOperadoresModel): ColombiaRecargaOperadoresFragment {
            val fragment = ColombiaRecargaOperadoresFragment()
            fragment.arguments = BundleBuilder().putParcelable("model", model).build()
            return fragment
        }
    }

    @Inject
    lateinit var model: ColombiaRecargaOperadoresModel
    @Inject
    lateinit var adapter: ColombiaRecargaOperadorAdapter
    @Inject
    lateinit var presenter: ColombiaRecargaOperadorPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.colombiaRecargaOperadorSubcomponent(
                ColombiaRecargaOperadorModule(this)
        ).inject(this)

        presenter.getOperadores(model.cat)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_categoria, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ContainerActivity).setAppToolbarTitle(model.cat.text)
        recycler_categoria.adapter = adapter
        recycler_categoria.layoutManager = GridLayoutManager(view.context, 2)
        ItemClickSupport.addTo(recycler_categoria).setOnItemClickListener { _, pos, _ ->
            clickOperador(adapter.getItem(pos))
        }

        screen_retry_categoria.findViewById<Button>(R.id.b_retry)
                .setOnClickListener { clickRetry() }
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_categoria)
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun onGetOperadores(operadores: List<OperadorRecarga>) {
        hideRetry()
        adapter.update(operadores)
    }

    override fun onGetOperadoresError() {
        showRetry()
    }

    override fun showRetry() {
        if (screen_retry_categoria.visibility == View.GONE) screen_retry_categoria.visibility =
                View.VISIBLE
    }

    override fun hideRetry() {
        if (screen_retry_categoria.visibility == View.VISIBLE) screen_retry_categoria.visibility =
                View.GONE
    }

    override fun clickRetry() {
        presenter.getOperadores(model.cat)
    }

    override fun clickOperador(operador: OperadorRecarga) {
        if (model.cat == ColombiaCategoria.DATOS) {
            launchDatos(operador)
        } else {
            launchTae(operador)
        }
    }

    override fun launchTae(operador: OperadorRecarga) {
        val taeModel = ColombiaRecargaTaeSeleccionModel(model.cat, operador)
        (activity as ColombiaRecargasActivity).fragmentManagerLazy.commit {
            add(R.id.frame_colombia, ColombiaRecargaTaeSeleccionFragment.get(taeModel))
            hide(this@ColombiaRecargaOperadoresFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    override fun launchDatos(operador: OperadorRecarga) {
        val datosModel = ColombiaRecargaPaquetesDatosModel(model.cat, operador)
        (activity as ColombiaRecargasActivity).fragmentManagerLazy.commit {
            add(R.id.frame_colombia, ColombiaRecargaPaquetesDatosFragment.get(datosModel))
            hide(this@ColombiaRecargaOperadoresFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    override fun showProgress() {
        (activity as ColombiaRecargasActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ColombiaRecargasActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ColombiaRecargasActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ColombiaRecargasActivity).showSuccess(msg)
    }
}