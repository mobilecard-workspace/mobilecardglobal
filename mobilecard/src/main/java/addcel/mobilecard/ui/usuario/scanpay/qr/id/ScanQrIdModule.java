package addcel.mobilecard.ui.usuario.scanpay.qr.id;

import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.SPApi;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

@Module
public final class ScanQrIdModule {
    private final ScanQrIdFragment fragment;

    ScanQrIdModule(ScanQrIdFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ScanPayActivity provideActivity() {
        return (ScanPayActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    ScanPayService provideService(Retrofit retrofit) {
        return retrofit.create(ScanPayService.class);
    }

    @PerFragment
    @Provides
    ScanPayOpenInteractor provideInteractor(Retrofit retrofit,
                                            SessionOperations session) {
        return new ScanPayOpenInteractor(SPApi.Companion.provide(retrofit),
                TokenizerAPI.Companion.provideTokenizerAPI(retrofit), session, new CompositeDisposable());
    }

    @PerFragment
    @Provides
    CardEntity provideCard() {
        return fragment.getArguments().getParcelable("card");
    }

    @PerFragment
    @Provides
    ScanQrIdContract.Presenter providePresenter(
            ScanPayOpenInteractor interactor) {
        return new ScanQrIdPresenter(interactor, fragment);
    }
}
