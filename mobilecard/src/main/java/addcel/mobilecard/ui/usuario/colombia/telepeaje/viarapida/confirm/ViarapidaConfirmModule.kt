package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.confirm

import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.di.scope.PerFragment
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 9/30/19.
 */
@Module
class ViarapidaConfirmModule(val fragment: ViarapidaConfirmFragment) {

    @PerFragment
    @Provides
    fun provideMiddleware(r: Retrofit): ColombiaMiddleware {
        return ColombiaMiddleware.get(r)
    }

    @PerFragment
    @Provides
    fun provideTokenizer(r: Retrofit): TokenizerAPI {
        return TokenizerAPI.provideTokenizerAPI(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            middleware: ColombiaMiddleware, tokenizerAPI: TokenizerAPI
    ): ViarapidaConfirmPresenter {
        return ViarapidaConfirmPresenterImpl(
                middleware,
                tokenizerAPI,
                CompositeDisposable(),
                fragment
        )
    }
}

@PerFragment
@Subcomponent(modules = [ViarapidaConfirmModule::class])
interface ViarapidaConfirmSubcomponent {
    fun inject(fragment: ViarapidaConfirmFragment)
}