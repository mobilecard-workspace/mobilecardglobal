package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.select

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 30/10/19.
 */
interface MobileTagSelectPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getMontos(idPais: Int, request: ServiciosRequest)

}

class MobileTagSelectPresenterImpl(
        val catalogos: CatalogoService,
        val compositeDisposable: CompositeDisposable,
        val view: MobileTagSelectView
) : MobileTagSelectPresenter {


    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun getMontos(idPais: Int, request: ServiciosRequest) {
        view.hideRetry()
        view.showProgress()

        val montosDisp = catalogos.getRecargaMontos(
                BuildConfig.ADDCEL_APP_ID,
                idPais,
                StringUtil.getCurrentLanguage(),
                request
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.hideProgress()
                            if (it.idError == 0 && it.montos.isNotEmpty()) {
                                view.hideRetry()
                                view.setMontos(it.montos)
                            } else {
                                view.showError(it.mensajeError)
                                view.showRetry()
                            }
                        },
                        {
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                            view.showRetry()
                        }
                )

        addToDisposables(montosDisp)
    }

}

