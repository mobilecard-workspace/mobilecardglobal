package addcel.mobilecard.ui.usuario.viamericas.fees

import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.ui.ScreenView
import android.location.Address
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * ADDCEL on 17/05/17.
 */


/**
 *    bundle.putParcelable("recipient", recipient)
bundle.putParcelableArrayList("currencies", Lists.newArrayList(currencies))
bundle.putParcelable("paymentMode", paymentMode)
bundle.putParcelable("location", location)
bundle.putDouble("montoEnvio", montoEnvio)
bundle.putParcelable("exchangeRate", exchangeRate)



bundle.putParcelable("recipient", recipient);
bundle.putParcelableArrayList("currencies", Lists.newArrayList(currencies));
bundle.putParcelable("paymentMode", paymentMode);
bundle.putParcelable("location", location);
bundle.putDouble("montoEnvio", montoEnvio);
bundle.putParcelable("exchangeRate", exchangeRate);

 */

@Parcelize
data class VmFeeViewModel(
        val recipient: RecipientResponse, val currencies: List<CurrencyModel>,
        val paymentMode: PaymentModeModel, val location: LocationModel,
        val montoEnvio: Double, val exchangeRate: ExchangeRateModel
) : Parcelable

interface VmFeeView : ScreenView {
    fun postAddress(address: Address)

    fun postOrderCreated(orderResponse: OrderResponse)

    fun setFees(body: OrderFeeModel)
}
