package addcel.mobilecard.ui.usuario.legacy.servicios.consulta;

import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.domain.location.McLocationData;

/**
 * ADDCEL on 1/23/19.
 */
public interface ServicioConsultaContract {

    interface Presenter {

        void clearDisposables();

        void consultaReferencia(ServicioResponse.ServicioEntity model, String referencia);

        void saveToFav(ServicioResponse.ServicioEntity servicio, int logo, String etiqueta);

        McLocationData fetchLocation();
    }
}
