package addcel.mobilecard.ui.usuario.legacy.servicios.categorias

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-06-24.
 */
interface Presenter {
    fun getProductos(entity: CatalogoResponse.CategoriaEntity)

    fun clearDisposables()
}

class CategoriaPresenterImpl(
        val api: CatalogoService, val usuario: Usuario, val view: View,
        val disposables: CompositeDisposable
) : Presenter {

    override fun getProductos(entity: CatalogoResponse.CategoriaEntity) {
        view.showProgress()

        val request = ServiciosRequest.Builder().setIdAplicacion(BuildConfig.ADDCEL_APP_ID)
                .setIdCategoria(entity.id).setIdioma(StringUtil.getCurrentLanguage()).build()

        disposables.add(
                api.getServicios(
                        BuildConfig.ADDCEL_APP_ID, usuario.idPais, StringUtil.getCurrentLanguage(),
                        request
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({
                    view.hideProgress()
                    if (it.idError == 0) {
                        view.onCategoriaClicked(entity, it.servicios)
                    } else {
                        view.showError(it.mensajeError)
                    }
                }, {
                    it.printStackTrace()
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
        )
    }

    override fun clearDisposables() {
        disposables.clear()
    }
}