package addcel.mobilecard.ui.usuario.registro.info

import addcel.mobilecard.data.net.usuarios.model.RegistroResponse
import com.mobsandgeeks.saripaar.Validator

/**
 * ADDCEL on 28/09/17.
 */
interface View : Validator.ValidationListener {

    fun setUpFBButton()

    fun getNombre(): String

    fun getApellido(): String

    fun getCelular(): String

    fun getEmail(): String

    fun getPassword(): String

    fun getPais(): Int

    fun getTipoCedula(): Int

    fun getCedula(): String

    fun getImei(): String

    fun getProfile(): String

    fun areTerminosChecked(): Boolean

    fun onPaisSelected(pos: Int)

    fun clickFormaPago()

    fun clickInfo()

    fun clickPrivacy()

    fun clickTerminos()

    fun clickContinue()

    fun showProgress()

    fun hideProgress()

    fun showError(resId: Int)

    fun showError(msg: String)

    fun showSuccess(msg: String)

    fun onRegistroSuccess(response: RegistroResponse)

    fun onLoginSuccess(msg: String)

    fun finish()

    fun showTerminosDialog(terminos: String)

    fun onPrivacy(privacy: String)
}

interface Presenter {

    fun getPrivacy()

    fun getTerminos()

    fun saveUser()

    fun clearDisposables()
}

