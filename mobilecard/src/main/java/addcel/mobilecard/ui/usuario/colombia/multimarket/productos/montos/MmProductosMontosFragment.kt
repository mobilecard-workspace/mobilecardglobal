package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.montos

import addcel.mobilecard.McRegexPatterns
import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Producto
import addcel.mobilecard.data.net.colombia.multimarket.Valor
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_mm_prepago_montos.*

/**
 * ADDCEL on 2019-12-18.
 */

@Parcelize
data class MmProductosViewModel(val producto: Producto) : Parcelable

class MmProductosMontosFragment : Fragment() {

    companion object {
        fun get(producto: Producto): MmProductosMontosFragment {
            val viewModel = MmProductosViewModel(producto)
            val frag = MmProductosMontosFragment()
            frag.arguments = BundleBuilder().putParcelable("model", viewModel).build()
            return frag
        }
    }

    lateinit var viewModel: MmProductosViewModel
    private val adapter = MmProductosMontosAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_mm_prepago_montos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title_mm_prepago_montos.text = viewModel.producto.nombre
        configValoresList(view)
        setValores(viewModel.producto.valor)
        b_mm_prepago_montos_pagar.setOnClickListener {
            clickPagar()
        }
    }

    private fun configValoresList(view: View) {
        recycler_mm_prepago_montos.adapter = adapter
        if (viewModel.producto.valor.size > 1) {
            recycler_mm_prepago_montos.layoutManager = GridLayoutManager(view.context, 2)
            recycler_mm_prepago_montos.setHasFixedSize(true)
        } else {
            recycler_mm_prepago_montos.layoutManager = LinearLayoutManager(view.context)
        }

        ItemClickSupport.addTo(recycler_mm_prepago_montos).setOnItemClickListener { _, position, _ ->
            adapter.toggleSelection(position)
            b_mm_prepago_montos_pagar.isChecked = true
        }
    }

    private fun setValores(valores: List<Valor>) {
        adapter.update(valores)
    }

    private fun clickPagar() {
        if (b_mm_prepago_montos_pagar.isChecked) {


            val valor = adapter.getItem(adapter.selectedItems[0])

            val args = BundleBuilder().putParcelable("producto", viewModel.producto)
                    .putParcelable("valor", valor)
                    .putDouble("amount", if (valor.valor.isDigitsOnly()) valor.valor.toDouble() else 0.0).build()

            (activity as ContainerActivity).fragmentManagerLazy.commit {
                add(R.id.frame_colombia_servicios, WalletSelectFragment.get(WalletSelectFragment.PREPAGO_COL, args))
                hide(this@MmProductosMontosFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        } else {
            (activity as ContainerActivity).showError("Seleccione el monto de su pago")
        }
    }
}