package addcel.mobilecard.ui.usuario.edocuenta.receipt

import addcel.mobilecard.McConstants
import addcel.mobilecard.R
import addcel.mobilecard.data.net.wallet.Transaction
import addcel.mobilecard.data.net.wallet.TransactionCard
import addcel.mobilecard.ui.usuario.edocuenta.EdocuentaActivity
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.ScreenshotUtil
import addcel.mobilecard.utils.StringUtil
import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.*
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.squareup.phrase.Phrase
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions2.RxPermissions
import commons.validator.routines.CreditCardValidator
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.screen_edocuenta_receipt.*
import mx.mobilecard.crypto.AddcelCrypto
import java.text.NumberFormat

/**
 * ADDCEL on 2019-06-13.
 */
interface EdocuentaReceiptView {
    fun setReceiptInfo(receipt: Transaction)

    @Throws(Throwable::class)
    fun loadImage(picasso: Picasso, url: String, container: ImageView)

    fun setCardInfo(card: TransactionCard, container: View)

    fun clickShare(receipt: Transaction)

    fun getFranquiciaImg(pan: String): Int
}

interface Presenter

class EdocuentaReceiptFragment : Fragment(), EdocuentaReceiptView {
    override fun getFranquiciaImg(pan: String): Int {
        val pan = receipt.card.pan
        val cleanPan = AddcelCrypto.decryptHard(pan)
        return when {
            CreditCardValidator.VISA_VALIDATOR.isValid(cleanPan) -> R.drawable.full_visa
            CreditCardValidator.MASTERCARD_VALIDATOR.isValid(cleanPan) -> R.drawable.full_mastercard
            CreditCardValidator.AMEX_VALIDATOR.isValid(cleanPan) -> R.drawable.full_amex
            else -> R.drawable.full_carnet
        }
    }

    override fun setCardInfo(card: TransactionCard, container: View) {

        val pan = AddcelCrypto.decryptHard(card.pan)

        val maskedPan =
                if (pan.isNotEmpty()) StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan)) else ""

        val info =
                Phrase.from(container, R.string.txt_historial_receipt_card).put("name", card.nombre)
                        .put("card", maskedPan).put("expiry", "**/**").put("cvv", "***").format()

        view_edocuenta_receipt_card_info.text = info
    }

    override fun loadImage(picasso: Picasso, url: String, container: ImageView) {

        val placeHolder = getFranquiciaImg(receipt.card.pan)

        if (url.isNotEmpty()) {
            picasso.load(url).placeholder(placeHolder).error(placeHolder).into(container)
        } else {
            container.setImageResource(placeHolder)
        }
    }

    private lateinit var receipt: Transaction
    private lateinit var edoActivity: EdocuentaActivity
    private lateinit var rxPermission: RxPermissions
    private val compositeDisposable = CompositeDisposable()

    companion object {
        fun get(receipt: Transaction): EdocuentaReceiptFragment {
            val args = BundleBuilder().putParcelable("receipt", receipt).build()
            val fragment = EdocuentaReceiptFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        receipt = arguments?.getParcelable("receipt")!!
        edoActivity = activity as EdocuentaActivity
        rxPermission = RxPermissions(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.screen_edocuenta_receipt, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setReceiptInfo(receipt)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_historial, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_share) {
            clickShare(receipt)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setReceiptInfo(receipt: Transaction) {

        val toolbar = edoActivity.findViewById<Toolbar>(R.id.toolbar_edocuenta)
        toolbar.title = getTitleSpan(receipt)

        val formatter = McConstants.getNumberFormat(receipt.idPais)

        val icon = getStatusIcon(receipt.status)
        img_historial_status.setImageResource(icon)
        label_historial_fecha.text = receipt.date
        label_historial_cuenta.text = receipt.ticket
        view_historial_cuenta.text = "${formatter.format(receipt.total)} ${receipt.currency}"
        view_historial_importe.text = "${formatter.format(receipt.importe)} ${receipt.currency}"
        view_historial_comision.text = "${formatter.format(receipt.comision)} ${receipt.currency}"
        view_historial_total.text = "${formatter.format(receipt.total)} ${receipt.currency}"

        try {
            loadImage(edoActivity.picasso, receipt.card.imgFull, img_edocuenta_receipt_card)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
        view?.let { setCardInfo(receipt.card, it) }
    }

    override fun onDestroyView() {
        val toolbar = edoActivity.findViewById<Toolbar>(R.id.toolbar_edocuenta)
        toolbar.setTitle(R.string.txt_nav_edocuenta)
        setHasOptionsMenu(false)
        super.onDestroyView()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun clickShare(receipt: Transaction) {
        compositeDisposable.add(rxPermission.request(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).subscribe { accept ->
            if (accept) {
                if (isVisible) {
                    val intent =
                            ScreenshotUtil.shareScreenshot(
                                    activity,
                                    getString(R.string.txt_transaction_share)
                            )
                    shareScreenshot(intent)
                }
            } else {
                if (isVisible) Toasty.error(
                        this.activity!!,
                        getString(R.string.txt_ingo_permission_retry)
                ).show()
            }
        })
    }

    private fun shareScreenshot(intent: Intent?) {
        if (intent != null) {
            try {
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                if (isVisible) Toasty.error(
                        activity!!,
                        e.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.OPERATION)
                ).show()
            }
        }
    }

    private fun getStatusIcon(status: Int): Int {
        return if (status == 1) R.drawable.ic_historial_success else if (status == 0) R.drawable.ic_historial_error else R.drawable.ic_historial_processing
    }

    private fun getStatusText(status: Int): String {
        return if (status == 1) "EXITOSA" else if (status == 0) "FALLIDA" else "EN PROCESO"
    }

    private fun getStatusColor(status: Int): Int {
        return if (status == 1) R.color.colorAccept else if (status == 0) R.color.colorDeny else R.color.colorProcessing
    }

    private fun getTitleSpan(receipt: Transaction): CharSequence {
        val firstPart = SpannableString("OPERACIÓN ")
        val lastPart = SpannableString(getStatusText(receipt.status))

        firstPart.setSpan(
                ForegroundColorSpan(
                        ContextCompat.getColor(edoActivity.applicationContext, R.color.colorBlack)
                ), 0,
                firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                ForegroundColorSpan(
                        ContextCompat.getColor(
                                edoActivity.applicationContext,
                                getStatusColor(receipt.status)
                        )
                ), 0,
                lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, lastPart)
    }

}