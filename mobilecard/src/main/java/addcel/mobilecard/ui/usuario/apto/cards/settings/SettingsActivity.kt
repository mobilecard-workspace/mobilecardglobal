package addcel.mobilecard.ui.usuario.apto.cards.settings

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.accounts.AccountsAPI
import addcel.mobilecard.data.net.apto.accounts.AptoCardData
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.usuario.apto.cards.pin.ActoAccountPinActivity
import addcel.mobilecard.ui.usuario.apto.cards.pin.ActoAccountPinModel
import addcel.mobilecard.utils.ErrorUtil
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.settings_activity.*


@Parcelize
data class SettingsModel(var aptoData: AptoCardData, val card: CardEntity, val userToken: String) :
        Parcelable

class SettingsActivity : AppCompatActivity() {

    val api = AccountsAPI.get(BuildConfig.APTO_VAULT_URL)


    companion object {
        fun get(context: Context, model: SettingsModel): Intent {
            return Intent(context, SettingsActivity::class.java).putExtra("model", model)
        }
    }

    lateinit var model: SettingsModel
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        model = intent?.getParcelableExtra("model")!!

        if (model.aptoData.state == "active") switch_apto_settings_enable_card.isChecked = false
        else if (model.aptoData.state == "inactive") switch_apto_settings_enable_card.isChecked =
                true

        b_apto_settings_change_pin.setOnClickListener {
            startActivityForResult(
                    ActoAccountPinActivity.get(
                            this@SettingsActivity,
                            ActoAccountPinModel(model.userToken, model.card.accountId!!)
                    ), ActoAccountPinActivity.REQUEST_CODE
            )
        }

        switch_apto_settings_enable_card.setOnCheckedChangeListener { _, b ->
            compositeDisposable.add(updateCardStatus(b))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun updateCardStatus(switchChecked: Boolean): Disposable {
        val observable =
                if (switchChecked) api.disable(model.userToken, model.card.accountId!!) else api.enable(
                        model.userToken,
                        model.card.accountId!!
                ).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        return observable.subscribe(
                { onCardStatusUpdated(it) }, {
            val code = ErrorUtil.getFormattedHttpErrorCode(it)
            if (code == 404) {
                Toasty.error(this@SettingsActivity, "La tarjeta no existe").show()
            } else {
                Toasty.error(this@SettingsActivity, ErrorUtil.getFormattedHttpErrorMsg(it))
                        .show()
            }
        }
        )
    }

    private fun onCardStatusUpdated(cardData: AptoCardData) {
        model.aptoData = cardData
        if (model.aptoData.state == "inactive") switch_apto_settings_enable_card.isChecked = true
        else if (model.aptoData.state == "active") switch_apto_settings_enable_card.isChecked =
                false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ActoAccountPinActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val dAptoCard: AptoCardData? =
                            data.getParcelableExtra(ActoAccountPinActivity.RESULT_DATA)
                    if (dAptoCard != null) {
                        model.aptoData = dAptoCard
                        Toasty.success(this, "Pin actualizado correctamente").show()
                    }
                }
            }
        }
    }
}