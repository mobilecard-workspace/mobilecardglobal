package addcel.mobilecard.ui.usuario.mymobilecard.display

import addcel.mobilecard.McRegexPatterns
import addcel.mobilecard.R
import addcel.mobilecard.data.net.previvale.historial.PrevivaleAPI
import addcel.mobilecard.data.net.previvale.historial.PrevivaleHistorialRequest
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import androidx.core.content.ContextCompat
import com.google.common.base.Strings
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.vinaygaba.creditcardview.CreditCardView
import commons.validator.routines.CreditCardValidator
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.mobilecard.crypto.AddcelCrypto
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.text.NumberFormat
import java.util.regex.Pattern

/**
 * ADDCEL on 17/11/17.
 */

internal interface MyMobilecardDisplayPresenter {

    val model: CardEntity

    val pan: String

    val holderName: String

    val vigencia: String

    val balance: String

    fun getHistorialCurrentMonth(idUsuario: Long): Disposable

    fun buildPhoneMessage(): String

    fun buildCardView(context: Context): CreditCardView

    fun loadBmp(url: String, view: CreditCardView)
}

internal class MyMobilecardDisplayPresenterImpl(private val previvale: PrevivaleAPI,
                                                override val model: CardEntity, private val currencyFormat: NumberFormat,
                                                private val view: MyMobilecardDisplayContractView
) : MyMobilecardDisplayPresenter {

    companion object {
        private val HISTORIAL_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd")
    }

    override// utils.maskCard);
    val pan: String
        get() = AddcelCrypto.decryptHard(model.pan)

    override val holderName: String
        get() = model.nombre!!

    override val vigencia: String
        get() = AddcelCrypto.decryptHard(model.vigencia)

    override val balance: String
        get() = currencyFormat.format(model.balance)

    override fun getHistorialCurrentMonth(idUsuario: Long): Disposable {

        view.showProgress()

        val currDate = LocalDate.now()
        val montLen = currDate.lengthOfMonth()
        val first = LocalDate.of(currDate.year, currDate.month, 1)
        val last = LocalDate.of(currDate.year, currDate.month, montLen)
        val cryptoCard = AddcelCrypto.encryptCard(AddcelCrypto.decryptHard(model.pan!!))
        val request = PrevivaleHistorialRequest(last.format(HISTORIAL_FORMATTER), first.format(HISTORIAL_FORMATTER), idUsuario, cryptoCard)

        return previvale.movimientosTarjeta(3, 1, StringUtil.getCurrentLanguage(), request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    view.showSuccess(it.toString())
                }, {
                    view.hideProgress()
                    view.showSuccess(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
    }

    override fun buildPhoneMessage(): String {
        return view.activationPhrase
                .put("phone", Strings.nullToEmpty(model.phoneNumberActivation))
                .format()
                .toString()
    }

    override fun buildCardView(context: Context): CreditCardView {
        val ccView = CreditCardView(context)
        ccView.isEditable = false
        ccView.background = ContextCompat.getDrawable(context, R.drawable.card_ccv)
        ccView.cardName = model.nombre!!
        ccView.cardNameTextColor = context.resources.getColor(R.color.colorBackground)
        ccView.cardNumber = AddcelCrypto.decryptHard(model.pan)!!
        ccView.cardNumberTextColor = context.resources.getColor(R.color.colorBackground)
        ccView.expiryDate = AddcelCrypto.decryptHard(model.vigencia)
        ccView.expiryDateTextColor = context.resources.getColor(R.color.colorBackground)
        try {
            ccView.findViewById<View>(R.id.card_logo)
                    .setBackgroundResource(getFranquiciaLogo(model))
            ccView.invalidate()
            ccView.requestLayout()
            // setVisibility(View.INVISIBLE);
        } catch (t: Throwable) {
            t.printStackTrace()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) ccView.elevation = 4.0.toFloat()
        ccView.putChip(false)
        ccView.setOnLongClickListener {
            view.copyPanToClipboard()
        }
        return ccView
    }

    override fun loadBmp(url: String, view: CreditCardView) {
        Picasso.get()
                .load(model.imgFull)
                .error(R.drawable.card_ccv)
                .placeholder(R.drawable.card_ccv)
                .into(object : Target {
                    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                        view.background = BitmapDrawable(view.resources, bitmap)
                    }

                    override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {}

                    override fun onPrepareLoad(placeHolderDrawable: Drawable) {}
                })
    }

    private fun getFranquiciaLogo(card: CardEntity): Int {
        val cleanPan = AddcelCrypto.decryptHard(card.pan)
        when {
            Pattern.matches(McRegexPatterns.PREVIVALE, cleanPan!!) -> return R.drawable.logo_carnet
            CreditCardValidator.VISA_VALIDATOR.isValid(cleanPan) -> return R.drawable.logo_visa
            CreditCardValidator.MASTERCARD_VALIDATOR.isValid(cleanPan) -> return R.drawable.logo_master
            else -> return if (CreditCardValidator.AMEX_VALIDATOR.isValid(cleanPan)) R.drawable.logo_american else R.drawable.logo_carnet
        }
    }
}
