package addcel.mobilecard.ui.usuario.endtoend.processing;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 22/11/17.
 */
@PerFragment
@Subcomponent(modules = MxTransferProcessingModule.class)
public interface MxTransferProcessingSubcomponent {
    void inject(MxTransferProcessingFragment fragment);
}
