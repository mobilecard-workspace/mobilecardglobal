package addcel.mobilecard.ui.usuario.main.pais.servicio;

import android.annotation.SuppressLint;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 11/10/17.
 */
class PaisPresenter implements PaisContract.Presenter {
    private final CatalogoService service;
    private final Usuario usuario;
    private final CompositeDisposable cDisposable;
    private final PaisContract.View view;

    PaisPresenter(CatalogoService service, Usuario usuario, PaisContract.View view) {
        this.service = service;
        this.usuario = usuario;
        this.cDisposable = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        cDisposable.clear();
    }

    @Override
    public int getIdPais() {
        return usuario.getIdPais();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getServiciosMX() {
        view.showProgress();
        view.disableList();
        cDisposable.add(service.getServiciosCategorias(BuildConfig.ADDCEL_APP_ID, usuario.getIdPais(),
                StringUtil.getCurrentLanguage(), ServiciosRequest.empty())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(transactoResponse -> {
                    view.enableList();
                    view.hideProgress();
                    if (transactoResponse.getIdError() == 0) {
                        view.onCategoriasMX(transactoResponse.getCategorias());
                    } else {
                        view.showError(transactoResponse.getMensajeError());
                    }
                }, throwable -> {
                    view.enableList();
                    view.hideProgress();
                    view.showError(StringUtil.getNetworkError());
                }));
    }
}
