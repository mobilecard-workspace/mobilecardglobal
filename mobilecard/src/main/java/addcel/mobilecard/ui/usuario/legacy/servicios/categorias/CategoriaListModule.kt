package addcel.mobilecard.ui.usuario.legacy.servicios.categorias

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 2019-06-15.
 */
@Module
class CategoriaListModule(val fragment: CategoriaListFragment) {
    @PerFragment
    @Provides
    fun provideActivity(): PagoContainerActivity {
        return fragment.activity as PagoContainerActivity
    }

    @PerFragment
    @Provides
    fun provideCategorias(): List<CatalogoResponse.CategoriaEntity> {
        return fragment.arguments?.getParcelableArrayList("categorias")!!
    }

    @PerFragment
    @Provides
    fun provideAdapter(
            data: List<CatalogoResponse.CategoriaEntity>
    ): CategoriaListAdapter {
        return CategoriaListAdapter(data)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            api: CatalogoService,
            session: SessionOperations
    ): Presenter {
        return CategoriaPresenterImpl(api, session.usuario, fragment, CompositeDisposable())
    }
}