package addcel.mobilecard.ui.usuario.scanpay.manual.monto;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import addcel.mobilecard.ui.usuario.scanpay.manual.confirm.ScanManualConfirmFragment;
import addcel.mobilecard.ui.usuario.wallet.select.custom.WalletSelectLayout;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.StringUtil;
import addcel.mobilecard.validation.ValidationUtils;

/**
 * ADDCEL on 23/08/18.
 */
public final class ScanManualMontoFragment extends Fragment
        implements ScanManualMontoContract.View {

    @Inject
    ScanManualMontoContract.Presenter presenter;
    @Inject
    Validator validator;
    @Inject
    ScanPayActivity activity;
    @NotEmpty(messageResId = R.string.error_cantidad)
    TextInputLayout montoTil;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout conceptoTil;
    private FrameLayout cardContainer;
    private WalletSelectLayout cardLayout;
    private TextInputLayout propinaTil;

    public static synchronized ScanManualMontoFragment get(LcpfEstablecimiento establecimiento) {
        ScanManualMontoFragment fragment = new ScanManualMontoFragment();
        fragment.setArguments(
                new BundleBuilder().putParcelable("establecimiento", establecimiento).build());
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .scanManualMontoSubcomponent(new ScanManualMontoModule(this))
                .inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen_scanpay_manual_monto, container, false);
        cardContainer = v.findViewById(R.id.container_scanpay_manual_monto);
        montoTil = v.findViewById(R.id.til_scanpay_manual_monto_monto);
        propinaTil = v.findViewById(R.id.til_scanpay_manual_monto_propina);
        conceptoTil = v.findViewById(R.id.til_scanpay_manual_monto_concepto);
        v.findViewById(R.id.b_scanpay_manual_monto_continuar)
                .setOnClickListener(view -> clickContinuar());
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setFinished(Boolean.FALSE);
        setEstablecimientoData(view);
        if (cardLayout == null && cardContainer.getChildCount() == 0) {
            cardLayout = (WalletSelectLayout) LayoutInflater.from(view.getContext())
                    .inflate(R.layout.screen_wallet_select, cardContainer, false);
            cardContainer.addView(cardLayout);
        }
    }

    @Override
    public double getMonto() {
        return Double.valueOf(AndroidUtils.getText(montoTil.getEditText()));
    }

    @Override
    public double getPropina() {
        String propinaText = AndroidUtils.getText(propinaTil.getEditText());
        return StringUtil.isDecimalAmount(propinaText) ? Double.parseDouble(propinaText) : 0.0;
    }

    @Override
    public String getConcepto() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(conceptoTil.getEditText()).getText().toString());
    }

    @Override
    public CardEntity getFormaPago() {
        return Objects.requireNonNull(cardLayout).getSelectedCard().getCard();
    }

    @Override
    public String getMsi() {
        return "";
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void setCards(List<CardEntity> cards) {

    }

    @Override
    public void setEstablecimientoData(View view) {
        ((TextView) view.findViewById(R.id.view_scanpay_manual_monto_alias)).setText(
                presenter.getEstablecimiento().getAlias());
        ((TextView) view.findViewById(R.id.view_scanpay_manual_monto_address)).setText(
                presenter.getEstablecimiento().getCorreo());
    }

    @Override
    public void clickContinuar() {
        if (cardLayout.getSelectedCard() == null) {
            showError(getString(R.string.error_formapago_empty));
        } else {
            ValidationUtils.clearViewErrors(montoTil, conceptoTil);
            validator.validate();
        }
    }

    @Override
    public void onValidationSucceeded() {
        if (presenter.validateMonto(
                Objects.requireNonNull(montoTil.getEditText()).getText().toString().trim())) {
            ValidationUtils.clearViewErrors(montoTil, conceptoTil);

            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_scanpay,
                            ScanManualConfirmFragment.get(getMonto(), getPropina(), getConcepto(),
                                    presenter.getEstablecimiento(), getFormaPago()))
                    .hide(this)
                    .addToBackStack(null)
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }
}
