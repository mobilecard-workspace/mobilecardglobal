package addcel.mobilecard.ui.usuario.wallet.select;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.utils.ListUtil;
import addcel.mobilecard.utils.StringUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 09/10/17.
 */
public final class WalletSelectAdapter
        extends RecyclerView.Adapter<WalletSelectAdapter.ViewHolder> {

    private final List<WalletCardSelectModel> models;
    private final int desiredWitdh;
    private final Picasso picasso;

    public WalletSelectAdapter(List<WalletCardSelectModel> models, int desiredWitdh,
                               Picasso picasso) {
        this.models = models;
        this.desiredWitdh = desiredWitdh;
        this.picasso = picasso;
    }

    public void setChecked(int pos) {
        boolean val = models.get(pos).isChecked();
        for (int i = 0, size = models.size(); i < size; i++) {
            WalletCardSelectModel model = models.get(i);
            if (i == pos) {
                model.setChecked(!val);
            } else {
                model.setChecked(false);
            }
        }
        notifyDataSetChanged();
    }

    public int getCheckedCardPosition() {
        int checkedIndex = -1;
        if (ListUtil.notEmpty(models)) {
            for (int i = 0, size = models.size(); i < size; i++) {
                if (models.get(i).isChecked()) {
                    checkedIndex = i;
                }
            }
        }
        return checkedIndex;
    }

    public void update(List<WalletCardSelectModel> models) {
        this.models.clear();
        this.models.addAll(models);
        notifyDataSetChanged();
    }

    public WalletCardSelectModel getItem(int pos) {
        return this.models.get(pos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_wallet_check_card, parent, false), picasso, desiredWitdh);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WalletCardSelectModel model = models.get(position);
        holder.panView.setText(StringUtil.maskCard(AddcelCrypto.decryptHard(model.getCard().getPan())));
        holder.loadFranquiciaBg(model.getCard());

        if (model.isChecked()) {
            holder.check.setVisibility(View.VISIBLE);
        } else {
            holder.check.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final Picasso vhPicasso;
        private final int desiredWidth;

        @BindView(R.id.bg_card)
        ImageView fondoIv;

        @BindView(R.id.pan_card)
        TextView panView;

        @BindView(R.id.img_wallet_card_check)
        ImageView check;

        ViewHolder(View itemView, Picasso picasso, int desiredWidth) {
            super(itemView);
            vhPicasso = picasso;
            this.desiredWidth = desiredWidth;
            ButterKnife.bind(this, itemView);
        }

        int getBgError(CardEntity model) {
            if (model.getTipo() == null) return R.drawable.bg_visa;
            switch (model.getTipo()) {
                case VISA:
                    return R.drawable.bg_visa;
                case MasterCard:
                    return R.drawable.bg_mastercard;
                case AmEx:
                case Amex:
                    return R.drawable.bg_amex;
                default:
                    return R.drawable.bg_carnet;
            }
        }

        int getBgPh(CardEntity model) {
            if (model.getTipo() == null) return R.drawable.bg_visa;
            switch (model.getTipo()) {
                case VISA:
                    return R.drawable.card_visa;
                case MasterCard:
                    return R.drawable.card_master;
                case AmEx:
                case Amex:
                    return R.drawable.card_american;
                default:
                    return R.drawable.card_carnet;
            }
        }

        void loadFranquiciaBg(CardEntity model) {
            this.vhPicasso.load(model.getImgShort())
                    .resize(this.desiredWidth, 0)
                    .centerInside()
                    .error(getBgError(model))
                    .placeholder(getBgPh(model))
                    .into(fondoIv);
        }
    }
}
