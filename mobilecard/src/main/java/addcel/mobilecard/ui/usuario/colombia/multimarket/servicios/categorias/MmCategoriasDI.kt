package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.categorias

import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

/**
 * ADDCEL on 2019-12-20.
 */

@Module
class MmCategoriasModule(val fragment: MmCategoriasFragment) {

    @PerFragment
    @Provides
    fun provideContainer(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideAdapter(): MmCategoriasAdapter {
        return MmCategoriasAdapter()
    }

    @PerFragment
    @Provides
    fun provideAPI(@Named("cacheRetrofit") retrofit: Retrofit): MultimarketAPI {
        return MultimarketAPI.get(retrofit)
    }

    @PerFragment
    @Provides
    fun providePresenter(api: MultimarketAPI): MmCategoriasPresenter {
        return MmCategoriasPresenterImpl(api, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [MmCategoriasModule::class])
interface MmCategoriasSubcomponent {
    fun inject(fragment: MmCategoriasFragment)
}