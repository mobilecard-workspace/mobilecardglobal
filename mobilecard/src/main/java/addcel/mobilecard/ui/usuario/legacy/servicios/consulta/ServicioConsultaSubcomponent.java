package addcel.mobilecard.ui.usuario.legacy.servicios.consulta;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 14/12/16.
 */
@PerFragment
@Subcomponent(modules = ServicioConsultaModule.class)
public interface ServicioConsultaSubcomponent {
    void inject(ServicioConsultaFragment fragment);
}
