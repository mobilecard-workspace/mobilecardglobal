package addcel.mobilecard.ui.usuario.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.appbar.AppBarLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;
import java.util.Stack;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.McConstants;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.CountryNotificationEnvent;
import addcel.mobilecard.data.net.usuarios.NameNotificationEvent;
import addcel.mobilecard.data.net.wallet.AptoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.domain.location.LocationService;
import addcel.mobilecard.domain.sms.SmsUseCase;
import addcel.mobilecard.domain.usuario.bottom.MenuEvent;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.ui.contacto.ContactoActivity;
import addcel.mobilecard.ui.contacto.ContactoModel;
import addcel.mobilecard.ui.login.LoginActivity;
import addcel.mobilecard.ui.login.LoginContract;
import addcel.mobilecard.ui.novedades.NovedadesActivity;
import addcel.mobilecard.ui.novedades.NovedadesModel;
import addcel.mobilecard.ui.sms.SmsActivity;
import addcel.mobilecard.ui.usuario.apto.AptoIntroActivity;
import addcel.mobilecard.ui.usuario.apto.AptoIntroModel;
import addcel.mobilecard.ui.usuario.colombia.multimarket.ColombiaServiciosActivity;
import addcel.mobilecard.ui.usuario.colombia.recargas.ColombiaRecargasActivity;
import addcel.mobilecard.ui.usuario.colombia.soad.ColombiaSoadActivity;
import addcel.mobilecard.ui.usuario.colombia.telepeaje.ColombiaTelepeajeActivity;
import addcel.mobilecard.ui.usuario.edocuenta.EdocuentaActivity;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import addcel.mobilecard.ui.usuario.ingo.IngoActivity;
import addcel.mobilecard.ui.usuario.legacy.CategoriaUseCaseObject;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.RecargaUseCaseObject;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.MobileTagActivity;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.MobileTagModel;
import addcel.mobilecard.ui.usuario.main.favoritos.FavoritosFragment;
import addcel.mobilecard.ui.usuario.main.menu.Menu2Fragment;
import addcel.mobilecard.ui.usuario.main.pais.recarga.RecargaPaisFragment;
import addcel.mobilecard.ui.usuario.main.pais.servicio.PaisFragment;
import addcel.mobilecard.ui.usuario.main.tutorial.MainTutorialFragment;
import addcel.mobilecard.ui.usuario.menu.MenuLayout;
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingModel;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingResult;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormModel;
import addcel.mobilecard.ui.usuario.perfil.Perfil2Activity;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.FragmentUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Lazy;
import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

// import com.shiftpayments.link.sdk.ui.ShiftPlatform;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private final CompositeDisposable permissionCDisposable = new CompositeDisposable();
    @BindView(R.id.appbar_main)
    AppBarLayout appbar;
    @BindView(R.id.frame_main)
    FrameLayout frame;
    @BindView(R.id.progress_main)
    ProgressBar progressBar;
    @BindView(R.id.menu_main)
    MenuLayout bottomMenu;
    @Inject
    Bus bus;
    @Inject
    MainContract.Presenter presenter;
    @Inject
    @Named("fromLogin")
    boolean fromLogin;
    @Inject
    @Named("initialUseCase")
    int initialUseCase;
    @Inject
    @Named("activateDialog")
    Lazy<AlertDialog> activationDialog;
    // ITEMS
    @Inject
    @Named("profileItem")
    PrimaryDrawerItem profile;
    @Inject
    @Named("inicioItem")
    PrimaryDrawerItem inicio;
    @Inject
    @Named("recargasItem")
    PrimaryDrawerItem recargas;
    @Inject
    @Named("serviciosItem")
    PrimaryDrawerItem servicios;
    @Inject
    @Named("serviciosUsItem")
    PrimaryDrawerItem serviciosUs;
    @Inject
    @Named("frecuentesItem")
    PrimaryDrawerItem frecuentes;
    @Inject
    @Named("walletItem")
    PrimaryDrawerItem wallet;
    @Inject
    @Named("checkItem")
    PrimaryDrawerItem cheques; //USA
    @Inject
    @Named("transferItem")
    PrimaryDrawerItem transferencias; //USA
    @Inject
    @Named("edocuentaItem")
    PrimaryDrawerItem edocuenta;
    @Inject
    @Named("scanItem")
    PrimaryDrawerItem scan;
    @Inject
    @Named("escaneaItem")
    PrimaryDrawerItem escaneaPeru;
    @Inject
    @Named("mobileTagItem")
    PrimaryDrawerItem mobileTag;
    @Inject
    @Named("peajeColItem")
    PrimaryDrawerItem peajeCol;
    @Inject
    @Named("soatColItem")
    PrimaryDrawerItem soatCol;
    @Inject
    @Named("prepagoColItem")
    PrimaryDrawerItem prepagoCol;
    @Inject
    @Named("taeColItem")
    PrimaryDrawerItem taeCol;
    @Inject
    @Named("myMcItem")
    PrimaryDrawerItem myMc;
    @Inject
    @Named("dividerItem")
    DividerDrawerItem divider;
    @Inject
    @Named("contactoItem")
    SecondaryDrawerItem contacto;
    @Inject
    @Named("logoutItem")
    SecondaryDrawerItem logout;
    @Inject
    @Named("versionItem")
    SecondaryDrawerItem version;
    @Inject
    Toolbar toolbar;
    @Inject
    Drawer drawer;
    @Inject
    Stack<Long> selectionStack;
    @Inject
    Stack<CharSequence> titleStack;
    private MainSubcomponent subcomponent;
    private RxPermissions permissions;
    private UseCase bottomMenuUseCase = UseCase.HOME;

    public static synchronized Intent get(Context context, boolean fromLogin) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("fromLogin", fromLogin);
        bundle.putInt("initialUseCase", MainContract.UseCase.INICIO);
        return new Intent(context, MainActivity.class).putExtras(bundle);
    }

    public static synchronized Intent get(Context context, int useCase, boolean fromLogin) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("fromLogin", fromLogin);
        bundle.putInt("initialUseCase", useCase);
        return new Intent(context, MainActivity.class).putExtras(bundle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        subcomponent = Mobilecard.get().getNetComponent().mainSubcomponent(new MainModule(this));
        subcomponent.inject(this);
        permissions = new RxPermissions(this);


        permissionCDisposable.add(
                permissions.request(MainContract.MAIN_PERMISSIONS)
                        .subscribe(aBoolean -> {
                            if (aBoolean) launchLocationService();
                        })
        );

        if (savedInstanceState == null) {
            if (initialUseCase == MainContract.UseCase.FRECUENTES) {
                FragmentUtil.addFragment(getSupportFragmentManager(), R.id.frame_main, FavoritosFragment.get());
                setSelection(MainContract.UseCase.FRECUENTES);
                selectionStack.push((long) MainContract.UseCase.FRECUENTES);
            } else {
                FragmentUtil.addFragment(getSupportFragmentManager(), R.id.frame_main, new Menu2Fragment());
                setSelection(MainContract.UseCase.INICIO);
                selectionStack.push((long) MainContract.UseCase.INICIO);
            }
        }
        presenter.setDrawerByCountry();


        //        FragmentUtil.addFragment(getSupportFragmentManager(), R.id.frame_main, MainTutorialFragment.Companion.get(presenter.getIdPais()));

        if (!presenter.isMenuLaunched()) {
            presenter.setMenuLaunched(false);

            NovedadesModel novedadesModel = new NovedadesModel(presenter.getIdPais(), "USUARIO", false);
            startActivityForResult(NovedadesActivity.Companion.get(this, novedadesModel), NovedadesActivity.REQUEST_CODE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
        bottomMenu.unblock();

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Timber.e(task.getException(), "Error al obtener instanceId");
                return;
            }

            if (task.getResult() != null) {
                Timber.d("Token push: %s", task.getResult().getToken());
                presenter.updateToken(task.getResult().getToken());
            }

        });

    }

    @Override
    protected void onPause() {
        bottomMenu.block();
        bus.unregister(this);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        stopService(new Intent(this, LocationService.class));
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        presenter.onRestart();
        try {
            bus.post(new NameNotificationEvent(presenter.getUsuarioName()));
            bus.post(new CountryNotificationEnvent(presenter.getIdPais()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        presenter.loadProfile();
    }

    @Override
    protected void onDestroy() {
        presenter.clearDisposables();
        permissionCDisposable.clear();
        super.onDestroy();
    }

    @Override
    public void setToolbarTitle(int titleResource) {
        try {
            Objects.requireNonNull(getSupportActionBar()).setTitle(titleResource);
        } catch (NullPointerException e) {
            Timber.e(e, "en setToolbarTitle");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == R.id.action_drawer) {
            if (!drawer.isDrawerOpen()) drawer.openDrawer();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void checkLocationSettings(int view) {
        try {
            presenter.launchOnLocationChecked(view, AndroidUtils.isLocationAvailable(this));
        } catch (NullPointerException e) {
            showError(getString(R.string.error_location_device));
        }
    }

    @Override
    public void goToLocationSettings() {
        try {
            showLocationDialog();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void logout() {
        showSuccess(getString(R.string.txt_nav_logout));
        final Intent intent = LoginActivity.Companion.get(this, LoginContract.UseCase.PAIS, null, null)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);
    }

    @Override
    public void startFragmentFromMain(int clave, int title) {
        titleStack.push(toolbar.getTitle());
        selectionStack.push((long) clave);
        toolbar.setTitle(title);
        switch (clave) {
            case MainContract.UseCase.FRECUENTES:
                FragmentUtil.replaceFragment(getSupportFragmentManager(), R.id.frame_main,
                        FavoritosFragment.get());
                break;
            case MainContract.UseCase.SERVICIOS:
                FragmentUtil.replaceFragment(getSupportFragmentManager(), R.id.frame_main,
                        PaisFragment.get(PaisFragment.SERVICIO_MODULE));
                break;
            default:
                FragmentUtil.replaceFragment(getSupportFragmentManager(), R.id.frame_main,
                        new Menu2Fragment());
                break;
        }
    }

    @Override
    public void launchLocationService() {
        new Handler().postDelayed(() -> {
            try {
                boolean location = AndroidUtils.isLocationAvailable(MainActivity.this);
                if (location) {
                    startService(new Intent(MainActivity.this, LocationService.class));
                } else {
                    goToLocationSettings();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
                showError(getString(R.string.error_location_device));
            }
        }, 500);

    }

    @Override
    public void launchSmsactivity(long idUsuario, int idPais) {
        startActivity(SmsActivity.get(this, SmsUseCase.U_MENU, idUsuario, idPais));
    }

    @Override
    public void launchProfileActivity() {
        startActivity(Perfil2Activity.get(this));
    }

    @Override
    public void launchContainerActivityRecargasMX(List<CatalogoResponse.RecargaEntity> recargas) {
        if (presenter.getIdPais() == McConstants.PAIS_ID_PE) {
            showError("Próximamente disponible. Enero 2020");
        } else {
            Intent intent = PagoContainerActivity.get(this, new RecargaUseCaseObject(recargas, null));
            startActivity(intent);
        }
    }

    @Override
    public void launchContainerActivityServicios(List<CatalogoResponse.CategoriaEntity> categorias) {
   /* if (presenter.getIdPais() == 1) {

    } else {
      startFragmentFromMain(MainContract.UseCase.SERVICIOS, R.string.txt_menu_servicios);
    }*/
        if (presenter.getIdPais() == 1 || presenter.getIdPais() == 3) {
            startActivity(PagoContainerActivity.get(this, new CategoriaUseCaseObject(categorias)));
        } else {
            showError("Próximamente disponible. Enero 2020");
        }
    }

    @Override
    public void launchContainerActivityRecargasUS(List<PaisResponse.PaisEntity> paises) {
        titleStack.push(toolbar.getTitle());
        selectionStack.push((long) MainContract.UseCase.RECARGAS);
        toolbar.setTitle(R.string.txt_nav_recargas_us);
        FragmentUtil.replaceFragment(getFragmentManagerFromActivity(), R.id.frame_main,
                RecargaPaisFragment.get(paises));
    }

    @Override
    public void launchContactoActivity(long idUsuario, String email, int idPais) {
        ContactoModel model = new ContactoModel(idUsuario, email, idPais);
        startActivity(ContactoActivity.Companion.get(this, model));
    }

    @Override
    public void launchWalletActivity() {
        startActivity(WalletContainerActivity.get(this));
    }

    @Override
    public void launchEdoCuentaActivity() {
        startActivity(EdocuentaActivity.get(this));
    }

    @Override
    public void launchIngoActivity() {
        if (AndroidUtils.isGooglePlayServicesAvailable(this, GoogleApiAvailability.getInstance())) {
            startActivity(IngoActivity.create(this));
        }
    }

    @Override
    public void launchVmActivity() {

        //SE BLOQUEA TEMPORALMENTE AL SER UNA VERSION OBSOLETA DE LA IMPLEMENTACION... SE HARÁ MERGE
        // CON PROD CUANDO TENGAMOS VoBo DE RAFAEL TELLEZ, APTO y RMF
        showError("Próximamente disponible. Febrero 2020");

        /*if (BuildConfig.DEBUG) {
            startActivity(VmActivity.Companion.get(this));
        } else {
            showError("Próximamente disponible. Enero 2020");
        }*/
    }

    @Override
    public void launchHTHActivity() {
        startActivity(MxTransferActivity.get(this));
    }

    @Override
    public void launchMobileTagActivity() {
        if (presenter.getIdPais() == PaisResponse.PaisEntity.MX) {
            startActivity(MobileTagActivity.Companion.get(this, new MobileTagModel(presenter.getIdPais(), presenter.getEmail())));
        } else {
            showError("Próximamente disponible. Enero 2020");
        }
    }

    @Override
    public void launchScanPayActivity() {
        if (presenter.getIdPais() == 2 || presenter.getIdPais() == 3) {
            showError("Próximamente disponible. Enero 2020");
        } else {
            startActivity(ScanPayActivity.get(this, presenter.getIdPais()));
        }
    }

    @Override
    public void launchColombiaActivity() {
        startActivity(ColombiaRecargasActivity.get(this));
    }


    @Override
    public void launchColombiaTelepeaje() {
        startActivity(ColombiaTelepeajeActivity.Companion.get(this));
    }

    @Override
    public void launchColombiaSoad() {
        if (BuildConfig.DEBUG) {
            startActivity(ColombiaSoadActivity.Companion.get(this));
        } else {
            showError("Próximamente disponible. Febrero 2020");
        }
    }

    @Override
    public void launchColombiaServicios(int useCase) {
        if (useCase == MainContract.UseCase.COL_PREPAGOS) {
            ColombiaServiciosActivity.Companion.start(this, ColombiaServiciosActivity.USE_CASE_PRODUCTOS);
        } else {
            ColombiaServiciosActivity.Companion.start(this, ColombiaServiciosActivity.USE_CASE_SERVICIOS);
        }
    }

    @Override
    public void launchMyMcActivity() {
        if (presenter.getIdPais() != PaisResponse.PaisEntity.CO) { //&& presenter.getIdPais() != PaisResponse.PaisEntity.PE
            showProgress();
            startActivityForResult(MyMcRoutingActivity.Companion.get(
                    this,
                    new MyMcRoutingModel(presenter.getIdUsuario(), presenter.getIdPais())), MyMcRoutingActivity.REQUEST_CODE);
        } else
            showError("Próximamente disponible. Enero 2020");
    }

    @Override
    public Toolbar getMainToolbar() {
        return toolbar;
    }

    @Override
    public void setSelection(long view) {

        boolean isRecargasUS = (view == MainContract.UseCase.RECARGAS && presenter.getIdPais() == 3);

        if (view == MainContract.UseCase.INICIO
                || view == MainContract.UseCase.SERVICIOS
                || view == MainContract.UseCase.FRECUENTES
                || isRecargasUS) {

            drawer.setSelection(view, false);
        }
    }

    @Override
    public UseCase getBottomMenuUseCase() {
        return bottomMenuUseCase;
    }

    @Override
    public void setBottomMenuUseCase(UseCase useCase) {
        this.bottomMenuUseCase = useCase;
    }

    @Override
    public FragmentManager getFragmentManagerFromActivity() {
        return getSupportFragmentManager();
    }

    @Override
    public void goTo(int view) {
        if (drawer.getCurrentSelection() != view) {
            setSelection(view);
        }
        if (view == MainContract.UseCase.LOGOUT) {
            showLogouDialog();
        } else {
            presenter.verifyAndLaunchUseCase(view);
        }
    }

    @Override
    public void setMexicoDrawer() {
        drawer.removeAllItems();
        drawer.addItem(profile);
        drawer.addItem(divider);
        drawer.addItem(inicio);
        if (BuildConfig.DEBUG) drawer.addItem(mobileTag);
        drawer.addItem(scan);
        drawer.addItem(transferencias);
        drawer.addItem(servicios);
        drawer.addItem(recargas);
        drawer.addItem(myMc);
        drawer.addItem(wallet);
        drawer.addItem(frecuentes);
        drawer.addItem(edocuenta);
        drawer.addItem(divider);
        drawer.addItem(contacto);
        drawer.addItem(logout);
        drawer.addItem(divider);
        drawer.addItem(version);
    }

    @Override
    public void setUsaDrawer(boolean hasIngo) {
        drawer.removeAllItems();
        drawer.addItem(profile);
        drawer.addItem(divider);
        drawer.addItem(inicio);
        //drawer.addItem(scan);
        drawer.addItem(cheques);
        drawer.addItem(transferencias);
        drawer.addItem(serviciosUs);
        drawer.addItem(recargas);
        drawer.addItem(myMc);
        drawer.addItem(wallet);
        drawer.addItem(frecuentes);
        drawer.addItem(edocuenta);
        drawer.addItem(divider);
        drawer.addItem(contacto);
        drawer.addItem(logout);
        drawer.addItem(divider);
        drawer.addItem(version);
    }

    @Override
    public void setColombiaDrawer() {
        drawer.removeAllItems();
        drawer.addItem(profile);
        drawer.addItem(divider);
        drawer.addItem(inicio);
        drawer.addItem(soatCol);
        drawer.addItem(servicios);
        drawer.addItem(prepagoCol);
        drawer.addItem(taeCol);

        //QUITAR COMMENT PARA SIGUIENTE RELEASE
        //drawer.addItem(soatCol);
        //drawer.addItem(peajeCol);


        drawer.addItem(myMc);
        drawer.addItem(wallet);
        drawer.addItem(frecuentes);
        drawer.addItem(edocuenta);
        drawer.addItem(divider);
        drawer.addItem(contacto);
        drawer.addItem(logout);
        drawer.addItem(divider);
        drawer.addItem(version);
    }

    @Override
    public void setPeruDrawer() {
        drawer.removeAllItems();
        drawer.addItem(profile);
        drawer.addItem(divider);
        drawer.addItem(inicio);
        drawer.addItem(escaneaPeru);
        //drawer.addItem(pagoEfectivo);
        drawer.addItem(transferencias);
        drawer.addItem(servicios);
        drawer.addItem(recargas);
        drawer.addItem(myMc);
        drawer.addItem(wallet);
        drawer.addItem(frecuentes);
        drawer.addItem(edocuenta);
        drawer.addItem(divider);
        drawer.addItem(contacto);
        drawer.addItem(logout);
        drawer.addItem(divider);
        drawer.addItem(version);
    }

    @Override
    public void setNameOnDrawer(@NonNull String name) {
        profile.withName(name);
    }

    @Override
    public void showLogouDialog() {
        try {
            new AlertDialog.Builder(this).setMessage(R.string.txt_nav_logout)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> presenter.logout())
                    .setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss())
                    .show();
        } catch (Throwable t) {
            presenter.onLogout();
        }
    }

    @Override
    public void showLocationDialog() {
        new AlertDialog.Builder(this).setTitle(android.R.string.dialog_alert_title)
                .setMessage(getString(R.string.txt_location_msg))
                .setPositiveButton(getString(R.string.txt_enable), (dialog1, which) -> {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                })
                .setNegativeButton(getString(android.R.string.cancel), (dialog12, which) -> dialog12.dismiss())
                .show();
    }

    @Override
    public void showIngoDialog(int idPais, long idUsuario, String msg) {
        new AlertDialog.Builder(this).setTitle(android.R.string.dialog_alert_title)
                .setMessage(msg)
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                    dialog.dismiss();
                    showError(MainActivity.this.getString(R.string.txt_menu_ingo_warning));
                })
                .setPositiveButton(R.string.txt_tarjeta_obtener, (dialog, which) -> {
                    dialog.dismiss();
                    startActivity(MyMobilecardActivity.get(MainActivity.this));
                })
                .show();
    }

    @Override
    public void showResendDialog() {
        if (!activationDialog.get().isShowing()) activationDialog.get().show();
    }

    @Override
    public void hideResendDialog() {
        if (activationDialog.get().isShowing()) activationDialog.get().dismiss();
    }

    @Override
    public void showActivationDialog(String msg) {
        View view = View.inflate(this, R.layout.view_activation_msg, null);
        ((TextView) view.findViewById(R.id.text_activation_msg)).setText(msg);

        new AlertDialog.Builder(this).setTitle(android.R.string.dialog_alert_title)
                .setView(view)
                .setPositiveButton(android.R.string.ok, (d, which) -> d.dismiss())
                .show();
    }

    @Override
    public void showError(@NonNull String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        Toasty.success(this, msg).show();
    }

    @Override
    public void showProgress() {
        if (progressBar.getVisibility() == View.GONE) progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (progressBar.getVisibility() == View.VISIBLE) progressBar.setVisibility(View.GONE);
    }

    public MainSubcomponent getSubcomponent() {
        return subcomponent;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
            if (titleStack.size() > 0) {
                toolbar.setTitle(titleStack.pop());
                selectionStack.pop();
                setSelection(selectionStack.peek());
            }
        }
    }

    public MainContract.Presenter getPresenter() {
        return presenter;
    }

    @Subscribe
    @Override
    public void onMenuEventReceived(@NotNull MenuEvent event) {
        bottomMenu.unblock();
        presenter.processMenuEvent(event.getResult(), event.getUseCase());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MyMcRoutingActivity.REQUEST_CODE) {
            if (data != null) {
                onMyMcRoutingActivityResult(resultCode, data);
            } else {
                showError("Ocurrió un error al consultar la tarjeta MobileCard");
            }
        } else if (requestCode == NovedadesActivity.REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    int useCase = data.getIntExtra(NovedadesActivity.DATA_USE_CASE, -1);
                    NovedadesActivity.Companion.processUsuario(this, useCase, presenter.getIdUsuario(), presenter.getIdPais());
                }
            } else {
                FragmentUtil.addFragment(getSupportFragmentManager(), R.id.frame_main, MainTutorialFragment.Companion.get(presenter.getIdPais()));
            }
        }
    }

    @Override
    public void showOnboardingErrorDialog(long idUsuario) {
    /*
    final View view = View.inflate(this, R.layout.screen_jumio_error, null);

    Dialog dialog = new AlertDialog.Builder(this).setView(view).create();

    final TextView text = view.findViewById(R.id.text_jumio_error);
    final Button button = view.findViewById(R.id.b_jumio_error);
    text.setText(R.string.error_jumio_netverify);
    button.setOnClickListener(view1 -> {
      if (dialog != null) {
        initSDKInvocation(this, credentials.getToken(), credentials.getSecret(),
                credentials.getDataCenter(),
                JumioUtil.Companion.getUsuarioReferenceWithPrefix(idUsuario));
        dialog.dismiss();
      }
    });

    dialog.show();

     */
    }

    @Override
    public void launchOnBoardingInfo(long idUsuario) {

    }

    @Override
    public void onMyMcRoutingActivityResult(int resultCode, @NotNull Intent data) {
        hideProgress();
        if (resultCode == RESULT_OK) {
            MyMcRoutingResult result = data.getParcelableExtra(MyMcRoutingActivity.RESULT_DATA);
            if (result != null) {
                switch (result.getIdPais()) {
                    case McConstants.PAIS_ID_MX:
                        onPrevivale(result.getMobileCard());
                        break;
                    case McConstants.PAIS_ID_CO:
                        onPowie(result.getMobileCard());
                        break;
                    case McConstants.PAIS_ID_USA:
                        onApto(result.getUsaAptoInfo(), result.getMobileCard());
                        break;
                    case McConstants.PAIS_ID_PE:
                        onTebca(result.getMobileCard());
                        break;
                }
            } else {
                showError("No se consultó la tarjeta MobileCard");
            }
        } else {
            showError("No se consultó la tarjeta MobileCard");
        }
    }

    @Override
    public void onPrevivale(@Nullable CardEntity card) {
        if (card != null)
            startActivity(MyMobilecardActivity.get(this, card));
        else
            startActivity(MyMobilecardActivity.get(this));
    }

    @Override
    public void onPowie(@Nullable CardEntity card) {
        showError("Próximamente disponible. Enero 2020");
    }

    @Override
    public void onApto(@org.jetbrains.annotations.Nullable AptoResponse aptoInfo, @org.jetbrains.annotations.Nullable CardEntity card) {
        startActivity(AptoIntroActivity.Companion.get(this, new AptoIntroModel(card, aptoInfo)));
    }

    @Override
    public void onTebca(@Nullable CardEntity card) {
        if (card != null) {
            startActivity(MyMobilecardActivity.get(this, card));
        } else {
            TebcaFormModel model1 = new TebcaFormModel(
                    presenter.getUsuario().getEMail(),
                    presenter.getUsuario().getIdeUsuario(),
                    presenter.getUsuario().getUsrTelefono(),
                    presenter.getUsuario().getUsrNombre(),
                    presenter.getUsuario().getUsrApellido(),
                    presenter.getUsuario().getUsrMaterno(), "",
                    "", "", "", "",
                    TipoTarjetaEntity.CREDITO);

            startActivityForResult(TebcaFormActivity.Companion.get(this, model1), TebcaFormActivity.REQUEST_CODE);
        }
    }

    @Override
    public void onTebcaFormResult(int resultCode, @org.jetbrains.annotations.Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Timber.d("Success");
        }
    }
}
