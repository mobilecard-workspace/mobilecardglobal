package addcel.mobilecard.ui.usuario.endtoend.result

import addcel.mobilecard.R
import addcel.mobilecard.data.net.hth.ReceiptEntity
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.screen_h2h_result.*
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 2019-05-13.
 */

class H2HResultFragment : Fragment() {

    companion object {

        fun get(result: ReceiptEntity): H2HResultFragment {
            val args = BundleBuilder().putParcelable("result", result).build()
            val fragment = H2HResultFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var receipt: ReceiptEntity
    lateinit var transferActivity: MxTransferActivity
    val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        receipt = arguments?.getParcelable("result")!!
        transferActivity = activity as MxTransferActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_h2h_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        transferActivity.setStreenStatus(MxTransferActivity.STATUS_FINISHED)

        b_scanpay_result_ok.setOnClickListener { transferActivity.finish() }

        b_scanpay_result_qr.visibility = View.GONE
        view_scanpay_result_title.text = receipt.message
        val code = receipt.code
        if (code == 0) {
            img_scanpay_result.setImageResource(R.drawable.ic_historial_success)
            view_scanpay_result_msg.text = buildMsg()
            transferActivity.findViewById<FloatingActionButton>(R.id.b_success_share).visibility =
                    View.VISIBLE
        } else {
            img_scanpay_result.setImageResource(R.drawable.ic_historial_error)
            view_scanpay_result_msg.visibility = View.GONE
            transferActivity.findViewById<FloatingActionButton>(R.id.b_success_share).visibility =
                    View.GONE
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) transferActivity.setStreenStatus(MxTransferActivity.STATUS_FINISHED)
    }

    private fun buildMsg(): String {
        return "Autorización: " + receipt.authNumber + "\n" + "Folio: " + receipt.idTransaccion + "\n" + "Monto: " + NumberFormat.getCurrencyInstance(
                Locale.getDefault()
        ).format(
                receipt.amount
        ) + "\n" + "Forma de pago: " + receipt.maskedPAN + "\n" + "Fecha y hora: " + formatDateForMsg(
                receipt.dateTime
        )
    }

    private fun formatDateForMsg(millis: Long): String {
        val dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault())
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
        return formatter.format(dateTime)
    }
}
