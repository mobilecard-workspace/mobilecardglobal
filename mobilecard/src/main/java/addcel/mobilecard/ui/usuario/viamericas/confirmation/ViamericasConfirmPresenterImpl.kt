package addcel.mobilecard.ui.usuario.viamericas.confirmation

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.R
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.CreditTransactionConfirmationModel
import addcel.mobilecard.data.net.viamericas.model.OrderFeeRequest
import addcel.mobilecard.data.net.wallet.FranquiciaEntity
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * ADDCEL on 25/05/17.
 */

interface ViamericasConfirmPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getTdcFranquiciaDrawable(franquicia: FranquiciaEntity): Int

    fun getTipoTarjetaByIdioma(tipoTarjeta: TipoTarjetaEntity): String

    fun getOrderFees(request: OrderFeeRequest)

    fun checkWhiteList(idPais: Int, idUsuario: Long)

    fun confirmOrder(confirmation: CreditTransactionConfirmationModel)
}


class ViamericasConfirmPresenterImpl internal constructor(
        private val viamericasService: ViamericasService,
        private val tokenizer: TokenizerAPI,
        private val disposables: CompositeDisposable,
        private val view: VmConfirmView
) : ViamericasConfirmPresenter {
    override fun checkWhiteList(idPais: Int, idUsuario: Long) {
        view.showProgress()
        val wlDisp = tokenizer.isOnWhiteList(
                BuildConfig.ADDCEL_APP_ID,
                idPais,
                StringUtil.getCurrentLanguage(),
                idUsuario
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.hideProgress()
                            if (it.code == 0) view.onWhiteList() else view.notOnWhiteList()
                        },
                        {
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })

        addToDisposables(wlDisp)
    }

    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getTdcFranquiciaDrawable(franquicia: FranquiciaEntity): Int {
        return when (franquicia) {
            FranquiciaEntity.VISA -> R.drawable.logo_visa
            FranquiciaEntity.MasterCard -> R.drawable.logo_master
            FranquiciaEntity.Amex, FranquiciaEntity.AmEx -> R.drawable.logo_american
            else -> R.drawable.logo_visa
        }
    }

    override fun getTipoTarjetaByIdioma(tipoTarjeta: TipoTarjetaEntity): String {
        return if (StringUtil.getCurrentLanguage() == "en")
            tipoTarjeta.name.replace("O", "")
        else
            tipoTarjeta.name
    }

    override fun getOrderFees(request: OrderFeeRequest) {
        view.showProgress()
        val ofDisp = viamericasService.getOrderFees(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                JsonUtil.toJson(request)
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.hideProgress()
                            if (it.idError == 0) view.setFees(it.orders[0])
                        }
                        , {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })

        addToDisposables(ofDisp)
    }

    override fun confirmOrder(confirmation: CreditTransactionConfirmationModel) {
        view.showProgress()
        val confStr = JsonUtil.toJson(confirmation)
        Timber.d(confStr)
        val cDisp = viamericasService.confirmTransactionCredit(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(), confStr
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.hideProgress()
                            if (it.idError == 0) view.postOrderConfirmed(it)
                            else view.showError(it.responseMessage)
                        },
                        {
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        }
                )
        addToDisposables(cDisp)
    }
}
