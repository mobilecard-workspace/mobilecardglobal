package addcel.mobilecard.ui.usuario.colombia.recargas.operadores

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.OperadorRecarga
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

/**
 * ADDCEL on 9/19/19.
 */

class ColombiaRecargaOperadorAdapter(val picasso: Picasso) :
        RecyclerView.Adapter<ColombiaRecargaOperadorAdapter.ViewHolder>() {

    private val operadores: MutableList<OperadorRecarga> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_menu_with_img, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return operadores.size
    }

    fun getItem(pos: Int): OperadorRecarga {
        return operadores[pos]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {/*val cat = ColombiaCategoria.values()[position]
    holder.logo.setImageResource(cat.logo)
    holder.text.setText(cat.text)*/
        val op = operadores[position]
        holder.logo.setBackgroundColor(holder.itemView.resources.getColor(R.color.colorAccent))
        holder.logo.setPadding(16, 16, 16, 16)
        picasso.load(op.imgWhite).placeholder(R.drawable.flag_colombia)
                .error(R.drawable.flag_colombia)
                .into(holder.logo)
    }

    fun update(operadores: List<OperadorRecarga>) {
        if (this.operadores.isNotEmpty()) this.operadores.clear()
        this.operadores.addAll(operadores)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val logo: ImageView = itemView.findViewById(R.id.icon_cat)
    }
}