package addcel.mobilecard.ui.usuario.billpocket

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroModel
import addcel.mobilecard.ui.usuario.main.MainActivity
import addcel.mobilecard.utils.JsonUtil
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.os.PersistableBundle
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.google.common.base.Charsets
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_bill_pocket_secure.*
import mx.mobilecard.crypto.AddcelCrypto
import org.jsoup.Jsoup
import timber.log.Timber
import javax.inject.Inject


enum class BillPocketUseCase {
    USUARIO, NEGOCIO
}

@Parcelize
data class BillPocketSecureModel(
        val id: Long,
        val card: CardEntity,
        val token: TokenEntity,
        val request: String,
        val useCase: BillPocketUseCase = BillPocketUseCase.USUARIO
) : Parcelable

interface BillPocketSecureCallback {
    fun onBillPocketSecureResult(resultCode: Int, data: Intent?)
}

interface BillPocketSecureView : ScreenView {

    fun fetchModel(): BillPocketSecureModel

    fun configBrowser(client: WebViewClient, manager: CookieManager)

    fun setCurrentUrl(url: String)

    fun evalJS(script: String)

    fun logHtml(dom: String)

    fun setCardData()

    fun goToMainMenu()

    fun goToPreviousPage()

    fun showWarningExitDialog()

    fun onUrlProcessed(url: String, processedUrl: String)

    fun showDebugHandlerDialog(handler: SslErrorHandler, error: SslError)

    fun onJsonResult(json: String)
}

class BillPocketSecureActivity : AppCompatActivity(), BillPocketSecureView {

    @Inject
    lateinit var model: BillPocketSecureModel
    @Inject
    lateinit var presenter: BillPocketSecurePresenter
    @Inject
    lateinit var client: WebViewClient

    private var url: String = ""

    companion object {

        const val REQUEST_CODE = 425
        const val RESULT_DATA = "result"

        fun getCardDataInjectionScript(card: CardEntity): String {

            val holder = card.nombre!!
            val pan = AddcelCrypto.decryptHard(card.pan)
            //StringUtil.splitCard(AddcelCrypto.decryptHard(card.pan), " - ")
            val exp = AddcelCrypto.decryptHard(card.vigencia).replace("/", "")
            val cvv = AddcelCrypto.decryptHard(card.codigo)

            /*
                  return "document.getElementsByName('cc_name')[0].value='"
            + cardEntity.getNombre()
            + "';"
            + "document.getElementsByName('cc_number')[0].value='"
            + pan
            + "';"
            + "document.getElementsByName('cc_type')[0].value='"
            + type
            + "';"
            + "document.getElementsByName('cc_cvv2')[0].value='"
            + ct
            + "';"
            + "document.getElementsByName('_cc_expmonth')[0].value='"
            + Strings.nullToEmpty(mes)
            + "';"
            + "document.getElementsByName('_cc_expyear')[0].value="
            + indexAnio
            + ";"

            // SETTEAMOS A READONLY LOS CAMPOS DEL FORM DE PROCOM DESPUES DE LLENARLOS
            + "document.getElementsByName('cc_name')[0].readOnly=true;"
            + "document.getElementsByName('cc_number')[0].readOnly=true;"
            + "document.getElementsByName('cc_type')[0].readOnly=true;"
            // EL CAMPO DE CVV SOLO SE SETTEA COMO READONLY SI la tarjeta TIENE ASIGNADO EL CVV
            + (!Strings.isNullOrEmpty(ct) ? "document.getElementsByName('cc_cvv2')[0].readOnly=true;"
            : "")
            + "document.getElementsByName('_cc_expmonth')[0].readOnly=true;"
            + "document.getElementsByName('_cc_expyear')[0].readOnly=true;";
             */

            return ("document.getElementsByName('name')[0].value='$holder';"
                    + "document.getElementsByName('card')[0].value='$pan';"
                    + "document.getElementsByName('period')[0].value='$exp';"
                    + "document.getElementsByName('cvv')[0].value='$cvv';"
                    + "document.getElementsByName('name')[0].readOnly=true;"
                    + "document.getElementsByName('card')[0].readOnly=true;"
                    + "document.getElementsByName('period')[0].readOnly=true;"
                    + "document.getElementsByName('cvv')[0].readOnly=true;"

                    /*+
                    "document.getElementsByClassName('v-text-field__slot')[0].getElementsByTagName('input')[0].readOnly=true;" +
                    "document.getElementsByClassName('v-text-field__slot')[1].getElementsByTagName('input')[0].readOnly=true;" +
                    "document.getElementsByClassName('v-text-field__slot')[2].getElementsByTagName('input')[0].readOnly=true;"
                    +
                    if (cvv.isNotBlank())
                    "document.getElementsByClassName('v-text-field__slot')[3].getElementsByTagName('input')[0].readOnly=true;"
                    else ""*/)
        }

        fun get(context: Context, model: BillPocketSecureModel): Intent {
            return Intent(context, BillPocketSecureActivity::class.java).putExtra("model", model)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("BillPocketSecureActivity - onCreate()")

        Mobilecard.get().netComponent.billPocketSecureSubcomponent(BillPocketSecureModule(this))
                .inject(this)

        setContentView(R.layout.activity_bill_pocket_secure)

        window.setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
        )

        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        configBrowser(client, CookieManager.getInstance())

        try { // presenter.processUrl(model.token.paths?.start!!, model.request, model.token.token)
            web_bp_secure.loadUrl(
                    model.token.paths?.start,
                    mapOf("Authorization" to model.token.token)
            )
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        if (web_bp_secure != null) {
            web_bp_secure.onResume()
            web_bp_secure.resumeTimers()
        }
    }

    override fun onPause() {
        if (web_bp_secure != null) {
            web_bp_secure.stopLoading()
            web_bp_secure.onPause()
            web_bp_secure.pauseTimers()
        }
        super.onPause()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        web_bp_secure.destroy()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (url == model.token.paths?.start!! || url.contains(model.token.paths?.form!!, true)) {
            goToPreviousPage()
        } else if (url.contains(model.token.paths?.success!!, true) || url.contains(model.token.paths?.error!!, true) || url.contains(
                        model.token.paths?.authError!!,
                        true
                )
        ) {
            goToMainMenu()
        } else {
            showWarningExitDialog()
        }
    }

    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    override fun onSaveInstanceState(
            outState: Bundle, outPersistentState: PersistableBundle
    ) {
        Timber.d("onSaveInstanceState")
        try {
            super.onSaveInstanceState(outState, outPersistentState)
            web_bp_secure.saveState(outState)
        } catch (t: Throwable) {
            Timber.e(t)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        Timber.d("onRestoreInstanceState")
        try {
            web_bp_secure.restoreState(savedInstanceState)
        } catch (t: Throwable) {
            Timber.e(t)
        }
    }

    override fun fetchModel(): BillPocketSecureModel {
        return model
    }

    @SuppressLint("SetJavaScriptEnabled", "JavascriptInterface")
    override fun configBrowser(
            client: WebViewClient, manager: CookieManager
    ) {
        web_bp_secure.webViewClient = client
        web_bp_secure.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        web_bp_secure.requestFocus(View.FOCUS_DOWN)

        web_bp_secure.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                if (!v.hasFocus()) {
                    v.requestFocus()
                }
            }
            false
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager.setAcceptThirdPartyCookies(web_bp_secure, true)
            manager.removeAllCookies(null)
            web_bp_secure.clearCache(true)
        } else {
            manager.removeAllCookie()
        }

        WebView.setWebContentsDebuggingEnabled(true)

        web_bp_secure.settings.javaScriptEnabled = true
        web_bp_secure.settings.javaScriptCanOpenWindowsAutomatically = true
        web_bp_secure.addJavascriptInterface(this, "HTMLOUT")
    }

    override fun setCurrentUrl(url: String) {
        this.url = url
    }

    override fun evalJS(script: String) {
        web_bp_secure.evaluateJavascript(script, null)
    }

    @JavascriptInterface
    override fun logHtml(dom: String) {
        if (BuildConfig.DEBUG && url.contains(BuildConfig.BASE_URL)) Timber.d(dom)

        if (url.contains(model.token.paths?.authError!!) || url.contains(model.token.paths?.error!!) || url.contains(
                        model.token.paths?.success!!
                )
        ) processPaymentResponse(dom)
    }

    private fun processPaymentResponse(html: String) {

        val dom = Jsoup.parse(html)
        val body = dom.body()
        val json = body.text()

        Timber.d("Body json: %s", json)

        if (JsonUtil.isJson(json)) onJsonResult(json)
    }

    override fun setCardData() {
        val script = getCardDataInjectionScript(model.card)
        Timber.d("Script inyeccion: %s", script)
        evalJS(script)
    }

    override fun goToMainMenu() {
        val intent = if (model.useCase == BillPocketUseCase.NEGOCIO)
            NegocioCobroActivity.get(this, NegocioCobroModel())
        else
            MainActivity.get(this, false)

        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))

        finish()
    }

    override fun goToPreviousPage() {
        super.onBackPressed()
    }

    override fun showWarningExitDialog() {
        AlertDialog.Builder(this).setTitle(android.R.string.dialog_alert_title)
                .setMessage(R.string.txt_pago_warning)
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    goToMainMenu()
                }
                .setNegativeButton(android.R.string.cancel) { d, _ -> d.dismiss() }.show()
    }

    override fun onUrlProcessed(url: String, processedUrl: String) {
        loadUrlAfterProcess(processedUrl, Charsets.UTF_8.displayName())
    }

    private fun loadUrlAfterProcess(html: String, charset: String) {
        web_bp_secure.post {
            web_bp_secure.clearCache(true)
            web_bp_secure.loadDataWithBaseURL(
                    BuildConfig.BASE_URL,
                    html,
                    "text/html",
                    charset,
                    null
            )
        }
    }

    private fun getSslErrorMsg(errorCode: Int): Int {
        return when (errorCode) {
            0 -> R.string.error_ssl_notyetvalid
            1 -> R.string.error_ssl_expired
            2 -> R.string.error_hostname_mismatch
            3 -> R.string.error_ssl_untrusted
            4 -> R.string.error_ssl_date_invalid
            else -> R.string.error_ssl_invalid
        }
    }

    override fun showDebugHandlerDialog(handler: SslErrorHandler, error: SslError) {
        AlertDialog.Builder(this).setTitle("Error " + error.primaryError)
                .setMessage(resources.getString(getSslErrorMsg(error.primaryError), error.url))
                .setNegativeButton(android.R.string.cancel) { _, _ -> handler.cancel() }
                .setPositiveButton(R.string.txt_secure_deseascontinuar) { _, _ -> handler.proceed() }
                .show()
    }

    override fun onJsonResult(json: String) {
        val intent = Intent().putExtra(RESULT_DATA, json)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun showProgress() {
        if (progress_bp_secure.visibility == View.GONE) progress_bp_secure.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        if (progress_bp_secure.visibility == View.VISIBLE) progress_bp_secure.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }
}
