package addcel.mobilecard.ui.usuario.legacy.recargas.tae.select;

import android.content.Context;
import android.location.Location;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.model.MobilecardContact;

/**
 * ADDCEL on 16/11/16.
 */
public interface RecargaContract {

    interface View extends Validator.ValidationListener {

        void showProgress();

        void hideProgress();

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);

        String getImei();

        void setContacts(List<MobilecardContact> contacts);

        boolean validatePhoneNumberConfirmation(EditText original, EditText confirmation);

        void toggleOtroNumeroViews(boolean checked);

        void setCurrency(String currency);

        void onOtroNumeroCaptured(CharSequence s);

        void clickRecarga();

        void clickFav();

        void clickSaveFav();

        void updateFavViewsOnSuccess();

        void setFav(boolean fav);

        void setInitialFav(boolean fav);

        void onTipoCambioReceived(double tipoCambio);
    }

    interface Presenter {

        void clearCompositeDisposable();

        void getContacts(Context context) throws Exception;

        String getContacto(MobilecardContact contact, String telefono);

        PaisResponse.PaisEntity getPaisDestino();

        Location buildLocation();

        void setCurrencyCaption(CatalogoResponse.RecargaEntity recargaModel);

        void saveToFav(CatalogoResponse.RecargaEntity recargaModel,
                       List<CatalogoResponse.MontoEntity> montos, int logo, String etiqueta);

        void getTipoCambio(CatalogoResponse.RecargaEntity recargaModel);
    }
}
