package addcel.mobilecard.ui.usuario.wallet.transactions;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.MovementEntity;

/**
 * ADDCEL on 09/10/17.
 */
final class WalletTransactionsAdapter
        extends RecyclerView.Adapter<WalletTransactionsAdapter.ViewHolder> {
    private final List<MovementEntity> models;
    private final NumberFormat currFormat;

    WalletTransactionsAdapter(List<MovementEntity> models) {
        this.models = models;
        this.currFormat = NumberFormat.getCurrencyInstance(Locale.US);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaction, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final MovementEntity model = models.get(position);

        holder.categoryText.setText(String.valueOf(model.getId()));
        holder.nombreText.setText(model.getTicket());
        holder.montoText.setText(currFormat.format(model.getTotal()));
        holder.dateText.setText(model.getDate());
        holder.shareButton.setOnClickListener(view -> {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, view.getResources()
                    .getString(R.string.txt_wallet_transaction_share_subject, model.getId()));
            sharingIntent.putExtra(Intent.EXTRA_TEXT,
                    model.getTicket() + "\n" + currFormat.format(model.getTotal()) + "\n" + model.getDate());
            view.getContext()
                    .startActivity(Intent.createChooser(sharingIntent,
                            view.getContext().getString(R.string.txt_wallet_transaction_share_selection)));
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView categoryText;
        private final TextView nombreText;
        private final TextView montoText;
        private final TextView dateText;
        private final ImageView shareButton;

        ViewHolder(View itemView) {
            super(itemView);
            categoryText = itemView.findViewById(R.id.text_wallet_transaction_category);
            nombreText = itemView.findViewById(R.id.text_wallet_transaction_info);
            montoText = itemView.findViewById(R.id.text_wallet_transaction_monto);
            dateText = itemView.findViewById(R.id.text_wallet_transaction_date);
            shareButton = itemView.findViewById(R.id.b_wallet_transaction_share);
        }
    }
}
