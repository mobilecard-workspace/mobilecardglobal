package addcel.mobilecard.ui.usuario.legacy.servicios.consulta;

import android.annotation.SuppressLint;

import com.squareup.otto.Bus;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.data.net.transacto.entity.ConsultaRequest;
import addcel.mobilecard.data.net.transacto.entity.SaldoEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.location.McLocationData;
import addcel.mobilecard.domain.transacto.TransactoInteractor;
import addcel.mobilecard.event.impl.ErrorEvent;
import addcel.mobilecard.event.impl.SaldoConsultaEvent;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 19/04/17.
 */
public class ServicioConsultaPresenter implements ServicioConsultaContract.Presenter {
    private final TransactoInteractor interactor;
    private final Bus bus;

    ServicioConsultaPresenter(TransactoInteractor interactor, Bus bus) {
        this.interactor = interactor;
        this.bus = bus;
    }

    @Override
    public void clearDisposables() {
        interactor.getDisposables().clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void consultaReferencia(ServicioResponse.ServicioEntity model, String referencia) {
        final int idPais = interactor.getIdPais() == 1 ? 1 : 2;
        ConsultaRequest request = ConsultaRequest.newBuilder()
                .setEmisor(model.getEmisor())
                .setIdAplicacion(BuildConfig.ADDCEL_APP_ID)
                .setIdioma(StringUtil.getCurrentLanguage())
                .setIdPais(idPais)
                .setOperacion(model.getOperacion())
                .setReferencia(referencia)
                .build();

        interactor.consultaSaldo(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request,
                new InteractorCallback<SaldoEntity>() {
                    @Override
                    public void onSuccess(@NotNull SaldoEntity result) {
                        bus.post(new SaldoConsultaEvent(idPais, result));
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        bus.post(new SaldoConsultaEvent(idPais, new SaldoEntity(-1000, error)));
                    }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void saveToFav(ServicioResponse.ServicioEntity servicio, int logo, String etiqueta) {
        Favorito favorito = new Favorito();
        favorito.setCategoria(Favorito.CAT_SERVICIO);
        favorito.setNombre(servicio.getEmpresa());
        favorito.setDescripcion(etiqueta);
        favorito.setPais(interactor.getIdPais());
        favorito.setLogotipo(logo);
        favorito.setMontos("");
        favorito.setServicio(JsonUtil.toJson(servicio));

        interactor.saveToFavs(favorito, new InteractorCallback<Long>() {
            @Override
            public void onSuccess(@NotNull Long result) {
                if (result > 0) {
                    bus.post(new ErrorEvent(R.string.txt_favoritos_success, "", false));
                } else {
                    bus.post(new ErrorEvent(R.string.error_default,
                            StringUtil.errorOrDefault("", StringUtil.getCurrentLanguage()), false));
                }
            }

            @Override
            public void onError(@NotNull String error) {
                bus.post(new ErrorEvent(R.string.error_default, error, false));
            }
        });
    }

    @Override
    public McLocationData fetchLocation() {
        return interactor.getLocation();
    }
}
