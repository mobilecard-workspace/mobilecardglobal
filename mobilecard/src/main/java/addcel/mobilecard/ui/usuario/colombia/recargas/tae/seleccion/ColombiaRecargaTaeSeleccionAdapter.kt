package addcel.mobilecard.ui.usuario.colombia.recargas.tae.seleccion

import addcel.mobilecard.R
import addcel.mobilecard.ui.custom.adapter.SelectableAdapter
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * ADDCEL on 9/24/19.
 */
class ColombiaRecargaTaeSeleccionAdapter :
        SelectableAdapter<ColombiaRecargaTaeSeleccionAdapter.ViewHolder>() {

    private val montos: MutableList<Double> = ArrayList()
    private val numberFormat = NumberFormat.getCurrencyInstance(Locale("es", "CO"))

    fun update(models: List<Double>) {
        montos.clear()
        montos.addAll(models)
        notifyDataSetChanged()
    }

    fun getItem(pos: Int): Double {
        return montos[pos]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemLayoutView =
                LayoutInflater.from(parent.context).inflate(R.layout.view_monto, parent, false)
        return ViewHolder(itemLayoutView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val monto = montos[position]
        holder.montoText.text = numberFormat.format(monto)
        TextViewCompat.setAutoSizeTextTypeWithDefaults(
                holder.montoText,
                TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM
        )
        if (isSelected(position)) {
            holder.montoText.setBackgroundColor(
                    ContextCompat.getColor(holder.montoText.context, R.color.accent)
            )
            holder.montoText.setTextColor(Color.WHITE)
        } else {
            holder.montoText.background =
                    ContextCompat.getDrawable(holder.montoText.context, R.drawable.sh_rectangle)
            holder.montoText.setTextColor(Color.GRAY)
        }
    }

    override fun getItemCount(): Int {
        return montos.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val montoText: TextView = view.findViewById(R.id.text_monto)
    }
}