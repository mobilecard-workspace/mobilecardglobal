package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.monto

import addcel.mobilecard.McConstants
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.multimarket.Categoria
import addcel.mobilecard.data.net.colombia.multimarket.MmConsultaResponse
import addcel.mobilecard.data.net.colombia.multimarket.Servicio
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.captura.MmServiciosCapturaKeyboardFragment
import addcel.mobilecard.ui.usuario.scanner.ScannerActivity
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.afollestad.vvalidator.form
import com.tbruyelle.rxpermissions2.RxPermissions
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_mm_servicios_monto.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-12-20.
 */

@Parcelize
data class MmServicioMontoModel(val cat: Categoria, val servicio: Servicio) : Parcelable

interface MmServicioMontoView : ScreenView {
    fun configInfo()
    fun onScanSuccess(result: String)
    fun onConsultaSuccess(consultaResponse: MmConsultaResponse)
    fun onConsultaSuccessEditable(consultaResponse: MmConsultaResponse)
    fun onConsultaError()
    fun setSaldoLabel(saldo: Double)
    fun setComisionLabel(comision: Double)
    fun launchWallet(consultaResponse: MmConsultaResponse)
}

class MmServicioMontoFragment : Fragment(), MmServicioMontoView {

    companion object {


        fun get(cat: Categoria, servicio: Servicio): MmServicioMontoFragment {
            val vModel = MmServicioMontoModel(cat, servicio)
            val args = BundleBuilder().putParcelable("model", vModel).build()
            val frag = MmServicioMontoFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var cActivity: ContainerActivity
    @Inject
    lateinit var model: MmServicioMontoModel
    @Inject
    lateinit var presenter: MmServicioMontoPresenter

    private lateinit var rxPermission: RxPermissions
    private var consultaResponse: MmConsultaResponse? = null


    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.mmServicioMontoSubcomponent(MmServicioMontoModule(this)).inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_mm_servicios_monto, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configInfo()
        form {
            inputLayout(R.id.til_mm_servicios_numservicio) {
                isNotEmpty().description(R.string.error_campo_generico)
            }
            submitWith(R.id.b_mm_servicio_consultar) {
                presenter.consultaSaldo(AndroidUtils.getText(til_mm_servicios_numservicio.editText), model.servicio.productId, false)
            }
        }

        b_mm_servicios_info.setOnClickListener {
            AlertDialog.Builder(view.context).setTitle("Nota")
                    .setMessage(R.string.txt_mm_servicio_consultar_info)
                    .setPositiveButton("Aceptar") { d, _ -> d.dismiss() }
                    .setOnCancelListener { d -> d.dismiss() }
                    .show()
        }

        b_mm_servicios_escanear.setOnClickListener {
            rxPermission = RxPermissions(this@MmServicioMontoFragment)
            compositeDisposable.add(rxPermission.request(Manifest.permission.CAMERA).subscribe({
                if (it) {
                    startActivityForResult(ScannerActivity.get(view.context), ScannerActivity.REQUEST_CODE)
                } else {
                    Toasty.info(view.context, "Se requiere autorización para utilizar la cámara del dispositivo.").show()
                }
            }, { Toasty.error(view.context, "Ocurrió un error en la solicitud de permisos.").show() }))
        }
        b_mm_servicios_continuar.setOnClickListener {
            if (consultaResponse != null) {
                launchWallet(consultaResponse!!)
            } else {
                showError("Consulte el saldo en su factura")
            }
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun configInfo() {
        title_mm_servicios_monto.text = model.servicio.name
        b_mm_servicios_continuar.isEnabled = false
    }

    override fun onScanSuccess(result: String) {
        presenter.consultaSaldo(result, model.servicio.productId, true)
    }

    override fun onConsultaSuccess(consultaResponse: MmConsultaResponse) {
        setSaldoLabel(consultaResponse.monto)
        setComisionLabel(consultaResponse.comision)
        this.consultaResponse = consultaResponse
        b_mm_servicios_continuar.isEnabled = true
    }

    override fun onConsultaSuccessEditable(consultaResponse: MmConsultaResponse) {

        onConsultaError()

        AlertDialog.Builder(cActivity).setTitle("Información").setMessage("Por favor ingresa el valor de tu factura.")
                .setNegativeButton("Cancelar") { di, _ -> di.dismiss() }
                .setPositiveButton(R.string.txt_continuar) { di, _ ->
                    cActivity.fragmentManagerLazy.commit {
                        add(R.id.frame_colombia_servicios, MmServiciosCapturaKeyboardFragment.get(model.cat, model.servicio, consultaResponse))
                        hide(this@MmServicioMontoFragment)
                        addToBackStack(null)
                        setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    }
                    di.dismiss()
                }.show()
    }

    override fun onConsultaError() {
        view_mm_servicios_saldo.text = ""
        view_mm_servicios_saldo.visibility = View.GONE
        view_mm_servicios_comision.text = ""
        view_mm_servicios_comision.visibility = View.GONE
        this.consultaResponse = null
        b_mm_servicios_continuar.isEnabled = false
    }

    override fun setSaldoLabel(saldo: Double) {
        val formatSaldo = McConstants.CO_CURR_FORMAT.format(saldo)
        view_mm_servicios_saldo.text = getString(R.string.txt_mm_servicio_saldo, formatSaldo)
        view_mm_servicios_saldo.visibility = View.VISIBLE
    }

    override fun setComisionLabel(comision: Double) {
        val formatComision = McConstants.CO_CURR_FORMAT.format(comision)
        view_mm_servicios_comision.text = getString(R.string.txt_mm_servicio_comision, formatComision)
        view_mm_servicios_comision.visibility = View.VISIBLE
    }

    override fun launchWallet(consultaResponse: MmConsultaResponse) {
        val bundle = BundleBuilder().putParcelable("categoria", model.cat)
                .putParcelable("servicio", model.servicio)
                .putParcelable("saldo", consultaResponse)
                .build()

        cActivity.fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, WalletSelectFragment.get(WalletSelectFragment.SERVICIOS_COL, bundle))
            hide(this@MmServicioMontoFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    override fun showSuccess(msg: String) {
        if (isVisible) {
            cActivity.showSuccess(msg)
        }
    }

    override fun showError(msg: String) {
        if (isVisible) {
            cActivity.showError(msg)
        }
    }

    override fun hideProgress() {
        if (isVisible) {
            cActivity.hideProgress()
        }
    }

    override fun showProgress() {
        if (isVisible) {
            cActivity.showProgress()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ScannerActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val result = data.getStringExtra(ScannerActivity.RESULT_DATA)
                    if (result != null) {
                        onScanSuccess(result)
                    } else {
                        onConsultaError()
                        showError("Ocurrió un error al escanear su factura.")
                    }
                } else {
                    onConsultaError()
                    showError("Ocurrió un error al escanear su factura.")
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                onConsultaError()
                showError("Escaneo cancelado por el usuario.")
            }
        }
    }

}