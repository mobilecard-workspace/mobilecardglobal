package addcel.mobilecard.ui.usuario.wallet.select;

import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 16/10/17.
 */
public final class WalletCardSelectModel {
    private final CardEntity card;
    private boolean checked;

    public WalletCardSelectModel(CardEntity card, boolean checked) {
        this.card = card;
        this.checked = checked;
    }

    public CardEntity getCard() {
        return card;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
