package addcel.mobilecard.ui.usuario.apto.cards.created

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.apto.accounts.AccountsAPI
import addcel.mobilecard.data.net.apto.accounts.AptoCardData
import addcel.mobilecard.data.net.apto.accounts.CardIssuer
import addcel.mobilecard.data.net.wallet.AptoCardRequest
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.ui.usuario.apto.cards.activation.CardActivationActivity
import addcel.mobilecard.ui.usuario.apto.cards.activation.CardActivationModel
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_card_created_activity.*
import javax.inject.Inject


@Parcelize
data class CardCreatedModel(val userToken: String) : Parcelable

class CardCreatedActivity : AppCompatActivity() {

    companion object {
        fun get(context: Context, model: CardCreatedModel): Intent {
            return Intent(context, CardCreatedActivity::class.java).putExtra("model", model)
        }
    }

    val api = AccountsAPI.get(BuildConfig.APTO_VAULT_URL)
    val compositeDisposable = CompositeDisposable()

    lateinit var model: CardCreatedModel
    @Inject
    lateinit var sessionOperations: SessionOperations
    @Inject
    lateinit var wallet: WalletAPI
    lateinit var createdCard: AptoCardData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_created_activity)

        Mobilecard.get().netComponent.inject(this)

        model = intent?.getParcelableExtra("model")!!

        label_creation_activate_1.movementMethod = LinkMovementMethod.getInstance()

        b_issue_card_continue.setOnClickListener {
            compositeDisposable.add(issueCard(model.userToken))
        }

        apto_card_create_retry.findViewById<Button>(R.id.b_retry).setOnClickListener {
            onRetry()
        }
    }

    private fun issueCard(userToken: String): Disposable {
        hideRetry()
        val issuer = CardIssuer()
        return api.issueCard("Bearer $userToken", issuer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    createdCard = it
                    val status = createdCard.kycStatus
                    if (status == "passed") {
                        compositeDisposable.add(saveCardToMobilecardDB())
                    } else {
                        val alertDialog = AlertDialog.Builder(this).setTitle("¡Aviso!")
                                .setMessage("Se le ha enviado un correo electrónico de verificación ya que algunos campos no pudieron ser validados.")
                                .setPositiveButton("OK") { d, _ -> d.dismiss() }
                        alertDialog.show()
                    }
                }, {
                    val code = ErrorUtil.getFormattedHttpErrorCode(it)
                    val msg =
                            if (code == 400) "La petición no es válida" else ErrorUtil.getFormattedHttpErrorMsg(
                                    it
                            )
                    Toasty.error(
                            this@CardCreatedActivity, msg
                    ).show()
                })
    }

    private fun showRetry() {
        apto_card_create_retry.visibility = View.VISIBLE
    }

    private fun hideRetry() {
        apto_card_create_retry.visibility = View.GONE
    }

    private fun onRetry() {
        if (::createdCard.isInitialized) {
            compositeDisposable.add(saveCardToMobilecardDB())
        }
    }

    private fun saveCardToMobilecardDB(): Disposable {
        val request = AptoCardRequest(
                createdCard.accountId,
                sessionOperations.usuario.ideUsuario,
                model.userToken
        )

        return wallet.saveInfoApto(
                BuildConfig.ADDCEL_APP_ID,
                sessionOperations.usuario.idPais,
                StringUtil.getCurrentLanguage(),
                request
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.idError == 0) {
                        hideRetry()
                        Toasty.success(this@CardCreatedActivity, "La tarjeta se emitió exitosamente")
                                .show()
                        startActivity(
                                CardActivationActivity.get(
                                        this@CardCreatedActivity,
                                        CardActivationModel(model.userToken, createdCard.accountId)
                                )
                        )
                        finish()
                    } else {
                        showRetry()
                        Toasty.error(this@CardCreatedActivity, it.mensajeError).show()
                    }
                }, {
                    showRetry()
                    Toasty.error(this@CardCreatedActivity, ErrorUtil.getFormattedHttpErrorMsg(it))
                            .show()
                })
    }

}
