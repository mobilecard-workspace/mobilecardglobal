package addcel.mobilecard.ui.usuario.colombia.recargas.datos

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.Categoria
import addcel.mobilecard.data.net.catalogo.OperadorRecarga
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.recargas.ColombiaRecargasActivity
import addcel.mobilecard.ui.usuario.colombia.recargas.categorias.ColombiaCategoria
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.seleccion.ColombiaRecargaPaquetesSeleccionFragment
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.seleccion.ColombiaRecargaPaquetesSeleccionModel
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.view_categoria.*
import javax.inject.Inject

/**
 * ADDCEL on 9/21/19.
 */

@Parcelize
data class ColombiaRecargaPaquetesDatosModel(
        val categoria: ColombiaCategoria,
        val operador: OperadorRecarga
) : Parcelable

interface ColombiaRecargaPaquetesDatosView : ScreenView {
    fun onGetPaquetes(paquetes: List<Categoria>)

    fun onGetPaquetesError()

    fun showRetry()

    fun hideRetry()

    fun clickRetry()

    fun clickPaquete(paquete: Categoria)

    fun clickContinuar()
}

class ColombiaRecargaPaquetesDatosFragment : Fragment(), ColombiaRecargaPaquetesDatosView {

    companion object {
        fun get(model: ColombiaRecargaPaquetesDatosModel): ColombiaRecargaPaquetesDatosFragment {
            val frag = ColombiaRecargaPaquetesDatosFragment()
            frag.arguments = BundleBuilder().putParcelable("model", model).build()
            return frag
        }
    }

    @Inject
    lateinit var model: ColombiaRecargaPaquetesDatosModel
    @Inject
    lateinit var adapter: ColombiaRecargaPaquetesDatosAdapter
    @Inject
    lateinit var presenter: ColombiaRecargaPaquetesDatosPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.colombiaRecargaPaqueteDatosSucomponent(
                ColombiaRecargaPaquetesDatosModule(this)
        ).inject(this)

        presenter.getPaquetes(model.operador)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_categoria, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_categoria.adapter = adapter
        recycler_categoria.layoutManager = LinearLayoutManager(view.context)
        ItemClickSupport.addTo(recycler_categoria).setOnItemClickListener { rv, position, v ->
            val vh = rv.getChildViewHolder(v)
            if (vh is ColombiaRecargaPaquetesDatosAdapter.ItemViewHolder) clickPaquete(
                    adapter.getItem(position)
            )
        }
        screen_retry_categoria.findViewById<Button>(R.id.b_retry)
                .setOnClickListener { clickRetry() }
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_categoria)
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun onGetPaquetes(paquetes: List<Categoria>) {
        hideRetry()
        adapter.update(paquetes)
    }

    override fun onGetPaquetesError() {
        showRetry()
    }

    override fun showRetry() {
        if (screen_retry_categoria.visibility == View.GONE) screen_retry_categoria.visibility =
                View.VISIBLE
    }

    override fun hideRetry() {
        if (screen_retry_categoria.visibility == View.VISIBLE) screen_retry_categoria.visibility =
                View.GONE
    }

    override fun clickRetry() {
        presenter.getPaquetes(model.operador)
    }

    override fun clickPaquete(paquete: Categoria) {

        val sModel = ColombiaRecargaPaquetesSeleccionModel(model.categoria, model.operador, paquete)

        (activity as ColombiaRecargasActivity).fragmentManagerLazy.commit {
            add(R.id.frame_colombia, ColombiaRecargaPaquetesSeleccionFragment.get(sModel))
            hide(this@ColombiaRecargaPaquetesDatosFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    override fun clickContinuar() {

    }

    override fun showProgress() {
        (activity as ColombiaRecargasActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ColombiaRecargasActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ColombiaRecargasActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ColombiaRecargasActivity).showSuccess(msg)
    }
}