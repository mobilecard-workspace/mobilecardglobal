package addcel.mobilecard.ui.usuario.colombia.recargas.operadores

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.ui.usuario.colombia.recargas.categorias.ColombiaCategoria
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 9/21/19.
 */
interface ColombiaRecargaOperadorPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getOperadores(categoria: ColombiaCategoria)
}

class ColombiaRecargaOperadorPresenterImpl(
        val api: CatalogoAPI,
        val disposables: CompositeDisposable, val view: ColombiaRecargaOperadoresView
) :
        ColombiaRecargaOperadorPresenter {

    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getOperadores(categoria: ColombiaCategoria) {
        view.showProgress()
        val method =
                if (categoria == ColombiaCategoria.TAE) CatalogoAPI.OPERADORES_RECARGA else CatalogoAPI.OPERADORES_PAQUETES

        val opDisp =
                api.getOperadoresPuntored(
                        BuildConfig.ADDCEL_APP_ID, 2, StringUtil.getCurrentLanguage(),
                        method
                ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ r ->
                            view.hideProgress()
                            if (r.idError == 0) view.onGetOperadores(r.operadores)
                            else {
                                view.onGetOperadoresError()
                                view.showError(r.mensajeError)
                            }
                        }, { t ->
                            view.hideProgress()
                            view.onGetOperadoresError()
                            view.showError(t.localizedMessage
                                    ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                        })

        addToDisposables(opDisp)
    }
}