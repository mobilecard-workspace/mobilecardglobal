package addcel.mobilecard.ui.usuario.menu

import addcel.mobilecard.di.scope.PerView
import dagger.Subcomponent

/**
 * ADDCEL on 2019-06-14.
 */
@PerView
@Subcomponent(modules = [BottomMenuModule::class])
interface BottomMenuSubcomponent {
    fun inject(view: MenuLayout)
}