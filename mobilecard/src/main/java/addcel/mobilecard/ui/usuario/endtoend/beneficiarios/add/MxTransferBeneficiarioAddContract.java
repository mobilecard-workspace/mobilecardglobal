package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add;

import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import addcel.mobilecard.data.net.hth.AccountResponse;
import addcel.mobilecard.data.net.hth.BankEntity;

/**
 * ADDCEL on 09/11/17.
 */
public interface MxTransferBeneficiarioAddContract {
    interface View extends Validator.ValidationListener {

        String RFC_PATTERN =
                "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\\d]{3})$";

        String getAlias();

        String getNombre();

        String getApellidoPaterno();

        String getApellidoMaterno();

        String getEmail();

        String getNumTelefono();

        BankEntity getBank();

        int getTipoCuentaSelectedPos();

        String getCuentaOrClable();

        void setBankCodes(List<BankEntity> bankCodes);

        void onBankSelected(int pos);

        void onTipoCuentaSelected(int pos);

        void clickSave(android.view.View view);

        void showProgress();

        void hideProgress();

        String getErrorFromSelectionPos(int pos);

        void showError(String msg);

        void showErrorByTipoCuenta();

        void onSave(AccountResponse accountResponse);

        void showRetry();

        void hideRetry();

        void clickRetry();
    }

    interface Presenter {
        String CLABE_PATTERN = "^\\d{18}$";
        String ACCOUNT_PATTERN = "^\\d{10,11}$";
        String ACT = "ACT";
        String CLABE = "CLABE";
        String CERO = "$0.00";
        String BANORTE_KEY = "072";
        String IXE_KEY = "032";

        void getBankCodes();

        String generateRfc();

        int getBanorteSelectedPos(BankEntity bank);

        void save();

        boolean isCLABE();

        boolean isAccount();

        boolean validateAccountOrClabe();
    }
}
