package addcel.mobilecard.ui.usuario.scanpay.manual.select;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;

/**
 * ADDCEL on 13/09/18.
 */
public class ScanManualSelectEstablecimientoAdapter
        extends RecyclerView.Adapter<ScanManualSelectEstablecimientoAdapter.ViewHolder> {

    private List<LcpfEstablecimiento> data;

    ScanManualSelectEstablecimientoAdapter(List<LcpfEstablecimiento> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_scanpay_manual_select_establecimiento, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final LcpfEstablecimiento item = data.get(i);
        viewHolder.setName(item.getAlias());
        viewHolder.setLocation(item.getCorreo());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void update(List<LcpfEstablecimiento> name) {
        this.data = name;
        notifyDataSetChanged();
    }

    public LcpfEstablecimiento getItem(int pos) {
        return data.get(pos);
    }

    public void clear() {
        if (data != null) {
            this.data.clear();
            notifyDataSetChanged();
        }
        data = Lists.newArrayList();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameView;
        private final TextView locationView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameView = itemView.findViewById(R.id.view_scanpay_manual_select_name);
            locationView = itemView.findViewById(R.id.view_scanpay_manual_select_location);
        }

        void setName(String name) {
            nameView.setText(name);
        }

        void setLocation(String location) {
            locationView.setText(location);
        }
    }
}
