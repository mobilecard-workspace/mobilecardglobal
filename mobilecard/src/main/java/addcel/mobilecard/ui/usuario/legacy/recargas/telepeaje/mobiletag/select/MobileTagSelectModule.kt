package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.select

import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.MontoAdapter
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.MobileTagActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import java.text.NumberFormat
import java.util.*
import javax.inject.Named

/**
 * ADDCEL on 30/10/19.
 */
@Module
class MobileTagSelectModule(val fragment: MobileTagSelectFragment) {

    @PerFragment
    @Provides
    fun provideActivity(): MobileTagActivity {
        return fragment.activity as MobileTagActivity
    }

    @PerFragment
    @Provides
    fun provideModel(): MobileTagSelectModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun providePresenter(catalogoService: CatalogoService): MobileTagSelectPresenter {
        return MobileTagSelectPresenterImpl(catalogoService, CompositeDisposable(), fragment)
    }

    @Named("countryFormat")
    @PerFragment
    @Provides
    fun provideNumberFormat(model: MobileTagSelectModel, format: NumberFormat): NumberFormat {
        return when (model.idPais) {
            PaisResponse.PaisEntity.MX -> return NumberFormat.getCurrencyInstance(
                    Locale(
                            "es",
                            "MX"
                    )
            )
            PaisResponse.PaisEntity.CO -> NumberFormat.getCurrencyInstance(
                    Locale(
                            "es",
                            "CO"
                    )
            )
            PaisResponse.PaisEntity.US -> NumberFormat.getCurrencyInstance(Locale.US)
            PaisResponse.PaisEntity.PE -> NumberFormat.getCurrencyInstance(
                    Locale(
                            "es",
                            "PE"
                    )
            )
            else -> format
        }
    }

    @PerFragment
    @Provides
    fun provideAdapter(@Named("countryFormat") format: NumberFormat): MontoAdapter {
        return MontoAdapter(format)
    }
}

@PerFragment
@Subcomponent(modules = [MobileTagSelectModule::class])
interface MobileTagSelectSubcomponent {
    fun inject(fragment: MobileTagSelectFragment)
}