package addcel.mobilecard.ui.usuario.legacy.servicios.consulta;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.google.common.base.Preconditions;
import com.squareup.otto.Bus;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.data.net.transacto.TransactoService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.transacto.TransactoInteractor;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 14/12/16.
 */
@Module
public final class ServicioConsultaModule {
    private final ServicioConsultaFragment fragment;

    ServicioConsultaModule(ServicioConsultaFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    PagoContainerActivity provideActivity() {
        return (PagoContainerActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    @Named("colorLogoMap")
    Map<Integer, Integer> provideColorIcons() {
        final Map<Integer, Integer> map = new LinkedHashMap<>();
        map.put(1, R.drawable.logo_cfe);
        map.put(3, R.drawable.logo_sky);
        map.put(4, R.drawable.logo_agua_mty);
        map.put(5, R.drawable.logo_agua_oaxaca);
        map.put(6, R.drawable.logo_multimedios);
        map.put(7, R.drawable.logo_maxcom);
        map.put(8, R.drawable.logo_telenor);
        map.put(9, R.drawable.logo_fenosa);
        map.put(10, R.drawable.logo_dish);
        map.put(11, R.drawable.logo_aguakan);
        map.put(12, R.drawable.logo_siapa);
        map.put(13, R.drawable.logo_multimedios);
        map.put(14, R.drawable.logo_axtel);
        map.put(15, R.drawable.logo_global_card);
        map.put(16, R.drawable.logo_jumapa);
        map.put(17, R.drawable.logo_adt);
        map.put(18, R.drawable.logo_cmapas);
        map.put(19, R.drawable.logo_paynet);
        map.put(20, R.drawable.logo_cdmx); // TODO falta icono blanco
        map.put(21, R.drawable.logo_edomex);
        map.put(23, R.drawable.logo_citi);
        map.put(24, R.drawable.logo_gob_michoacan);
        map.put(39, R.drawable.logo_gob_jalisco);
        map.put(45, R.drawable.logo_izzi);
        map.put(46, R.drawable.logo_interjet);
        map.put(47, R.drawable.logo_megacable);
        map.put(48, R.drawable.logo_telmex);
        map.put(49, R.drawable.logo_izzi);
        map.put(50, R.drawable.logo_megacable);
        map.put(51, R.drawable.logo_telcel_1);
        map.put(52, R.drawable.logo_att); // NEXTEL
        map.put(53, R.drawable.logo_aguas_saltillo);
        map.put(54, R.drawable.logo_jad);
        map.put(55, R.drawable.logo_jmas);
        map.put(56, R.drawable.logo_transpais);
        map.put(57, R.drawable.logo_rednovo);
        map.put(58, R.drawable.logo_infonavit);
        map.put(59, R.drawable.logo_copsis);
        map.put(60, R.drawable.logo_sencorp); // TODO falta icono blanco
        return map;
    }

    @PerFragment
    @Provides
    ServicioResponse.ServicioEntity provideServicio() {
        return Objects.requireNonNull(fragment.getArguments()).getParcelable("servicio");
    }

    @PerFragment
    @Provides
    boolean provideIsFav() {
        return Objects.requireNonNull(fragment.getArguments()).getBoolean("fav", false);
    }

    @PerFragment
    @Provides
    TransactoInteractor provideInteractor(TransactoService api,
                                          FavoritoDaoRx dao, SessionOperations session) {
        return new TransactoInteractor(api, dao, session,
                Preconditions.checkNotNull(fragment.getActivity()).getContentResolver(),
                new CompositeDisposable());
    }

    @PerFragment
    @Provides
    ServicioConsultaContract.Presenter providePresenter(
            TransactoInteractor interactor, Bus bus) {
        return new ServicioConsultaPresenter(interactor, bus);
    }
}
