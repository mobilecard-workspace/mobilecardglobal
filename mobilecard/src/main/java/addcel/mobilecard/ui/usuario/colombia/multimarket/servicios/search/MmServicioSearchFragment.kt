package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.data.net.colombia.multimarket.Categoria
import addcel.mobilecard.data.net.colombia.multimarket.Servicio
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.parcel.Parcelize
import addcel.mobilecard.R
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.monto.MmServicioMontoFragment
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search.notsearchable.MmServicioNoSearchFragment
import addcel.mobilecard.utils.BundleBuilder
import android.os.Handler
import android.widget.Button
import androidx.fragment.app.commit
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.screen_searchable_list.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * ADDCEL on 2019-12-20.
 */


@Parcelize
data class MmServicioSearchModel(val categoria: Categoria) : Parcelable

interface MmServicioSearchView : ScreenView {
    fun launchServicios()

    fun showRetry()

    fun hideRetry()

    fun clickRetry()

    fun configServicioList(view: View)

    fun configSearchText(view: View)

    fun onSearchable(servicios: List<Servicio>)

    fun onNotSearchable(servicios: List<Servicio>)

    fun clickServicio(pos: Int)
}

class MmServicioSearchFragment : Fragment(), MmServicioSearchView {

    companion object {
        fun get(categoria: Categoria): MmServicioSearchFragment {
            val args = BundleBuilder().putParcelable("model", MmServicioSearchModel(categoria)).build()
            val frag = MmServicioSearchFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var cActivity: ContainerActivity
    @Inject
    lateinit var presenter: MmServicioSearchPresenter
    @Inject
    lateinit var model: MmServicioSearchModel

    private lateinit var adapter: MmServiciosSearchAdapter
    private lateinit var viewModel: MmServicioSeleccionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.mmServicioSearchSubcomponent(MmServicioSearchModule(this)).inject(this)
        viewModel = ViewModelProviders.of(this).get(MmServicioSeleccionViewModel::class.java)
        adapter = MmServiciosSearchAdapter(viewModel.oldFilteredCards)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_searchable_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cActivity.retry?.findViewById<Button>(R.id.b_retry)?.setOnClickListener {
            clickRetry()
        }

        configServicioList(view)
        configSearchText(view)
        launchServicios()
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_searchable_screen_list)
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun launchServicios() {
        if (adapter.itemCount == 0)
            presenter.getServicios(model.categoria)
    }

    override fun showRetry() {
        cActivity.showRetry()
    }

    override fun hideRetry() {
        cActivity.hideRetry()
    }

    override fun clickRetry() {
        launchServicios()
    }

    override fun configServicioList(view: View) {
        recycler_searchable_screen_list.adapter = adapter
        recycler_searchable_screen_list.layoutManager = LinearLayoutManager(view.context)
        recycler_searchable_screen_list.addItemDecoration(
                DividerItemDecoration(
                        view.context,
                        DividerItemDecoration.VERTICAL
                )
        )
        ItemClickSupport.addTo(recycler_searchable_screen_list).setOnItemClickListener { _, position, _ ->
            clickServicio(position)
        }

    }

    override fun configSearchText(view: View) {
        text_searchable_screen_search
                .textChanges()
                .debounce(200, TimeUnit.MILLISECONDS)
                .subscribe {
                    viewModel
                            .search(it.toString())
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe {
                                val diffResult = DiffUtil.calculateDiff(
                                        MmServicioDiffUtilCallback(
                                                viewModel.oldFilteredCards,
                                                viewModel.filteredCards
                                        )
                                )
                                viewModel.oldFilteredCards.clear()
                                viewModel.oldFilteredCards.addAll(viewModel.filteredCards)
                                diffResult.dispatchUpdatesTo(recycler_searchable_screen_list.adapter as MmServiciosSearchAdapter)
                            }
                            .addTo(presenter.getDisposables())
                }.addTo(presenter.getDisposables())
    }

    override fun onSearchable(servicios: List<Servicio>) {
        if (servicios.isNotEmpty()) {
            viewModel.originalCards.addAll(servicios)
            viewModel.oldFilteredCards.addAll(viewModel.originalCards)
            adapter.notifyDataSetChanged()
        } else {
            showError("Servicios no disponibles")
            Handler().postDelayed({
                cActivity.onBackPressed()
            }, 250)
        }
    }

    override fun onNotSearchable(servicios: List<Servicio>) {

        text_searchable_screen_search.visibility = View.GONE
        onSearchable(servicios)

        /*cActivity.fragmentManagerLazy.popBackStack()

        cActivity.fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, MmServicioNoSearchFragment.get(model.categoria, servicios))
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }*/
    }

    override fun clickServicio(pos: Int) {
        cActivity.fragmentManagerLazy.commit {
            add(R.id.frame_colombia_servicios, MmServicioMontoFragment.get(model.categoria, adapter.getItem(pos)))
            hide(this@MmServicioSearchFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    override fun showSuccess(msg: String) {
        cActivity.showSuccess(msg)
    }

    override fun showError(msg: String) {
        cActivity.showError(msg)
    }

    override fun hideProgress() {
        cActivity.hideProgress()
    }

    override fun showProgress() {
        cActivity.showProgress()
    }


    /*



    companion object {
        fun get(context: Context): Intent {
            return Intent(context, RecargaSeleccionActivity::class.java)
        }
    }

    val api = TppAPI.get(TppApp.get().retrofit)
    val repo = TppApp.get().userRepository

    lateinit var adapter: RecargaSeleccionAdapter
    lateinit var viewModel: RecargaSeleccionViewModel
    lateinit var presenter: RecargaSeleccionPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recarga_seleccion)
        presenter = RecargaSeleccionPresenterImpl(api = api, repository = repo, view = this)

        setSupportActionBar(toolbar_recarga_seleccion)

        viewModel = ViewModelProviders.of(this).get(RecargaSeleccionViewModel::class.java)
        adapter = RecargaSeleccionAdapter(viewModel.oldFilteredCards)

        recycler_recarga_seleccion.adapter = adapter
        recycler_recarga_seleccion.layoutManager = LinearLayoutManager(this)
        recycler_recarga_seleccion.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )


        text_seleccion_search
            .textChanges()
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel
                    .search(it.toString())
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        val diffResult = DiffUtil.calculateDiff(
                            CardiffUtilCallback(
                                viewModel.oldFilteredCards,
                                viewModel.filteredCards
                            )
                        )
                        viewModel.oldFilteredCards.clear()
                        viewModel.oldFilteredCards.addAll(viewModel.filteredCards)
                        diffResult.dispatchUpdatesTo(recycler_recarga_seleccion.adapter as RecyclerView.Adapter<*>)
                    }.addTo(presenter.compositeDisposable)
            }.addTo(presenter.compositeDisposable)


        ItemClickSupport.addTo(recycler_recarga_seleccion)
            .setOnItemClickListener { rv, position, v ->
                val vh = rv.getChildViewHolder(v)
                if (vh is RecargaSeleccionAdapter.ItemViewHolder) {
                    val card = adapter.getItem(position)
                    clickCard(card)
                }
            }
        presenter.getCards()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun addEntity(tebcaCardEntity: TebcaCardEntity) {

    }

    override fun updateEntities(tebcaCardEntities: List<TebcaCardEntity>) {

    }

    override fun onLoadFinished() {

    }

    override fun setCards(cards: List<TebcaCardEntity>) {
        if (cards.isNotEmpty()) {
            viewModel.originalCards.addAll(cards)
            viewModel.oldFilteredCards.addAll(viewModel.originalCards)
            adapter.notifyDataSetChanged()
        } else {
            onError("No hay tarjetas registradas")
            Handler().postDelayed({
                this@RecargaSeleccionActivity.finish()
            }, 250)
        }
    }


     */

}