package addcel.mobilecard.ui.usuario.scanpay.manual.confirm;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 23/08/18.
 */
@PerFragment
@Subcomponent(modules = ScanManualConfirmModule.class)
public interface ScanManualConfirmSubcomponent {
    void inject(ScanManualConfirmFragment fragment);
}
