package addcel.mobilecard.ui.usuario.scanner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.common.base.Strings;
import com.google.zxing.Result;

import addcel.mobilecard.R;
import es.dmoral.toasty.Toasty;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import timber.log.Timber;

public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    public static final int REQUEST_CODE = 434;
    public static final String RESULT_DATA = "result";

    private ZXingScannerView mScannerView;

    public static synchronized Intent get(Context context) {
        return new Intent(context, ScannerActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera(); // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera(); // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        if (rawResult != null && !Strings.isNullOrEmpty(rawResult.getText())) {
            Timber.d("Resultado scan: %s", rawResult);
            Bundle bundle = new Bundle();
            bundle.putString("result", rawResult.getText());

            Intent intent = new Intent();
            intent.putExtras(bundle);

            setResult(RESULT_OK, intent);
            finish();
        } else {
            Toasty.error(this, getString(R.string.error_scan_capture)).show();
            mScannerView.resumeCameraPreview(this);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
