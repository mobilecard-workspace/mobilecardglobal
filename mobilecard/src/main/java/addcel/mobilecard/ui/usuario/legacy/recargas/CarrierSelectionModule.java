package addcel.mobilecard.ui.usuario.legacy.recargas;

import android.os.Bundle;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 13/12/16.
 */
@Module
public final class CarrierSelectionModule {
    private final CarrierSelectionFragment fragment;

    CarrierSelectionModule(CarrierSelectionFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    PagoContainerActivity provideActivity() {
        return (PagoContainerActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    Bundle provideArgs() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    CatalogoResponse.RecargaEntity provideCategoria(Bundle args) {
        return args.getParcelable("categoria");
    }

    @PerFragment
    @Provides
    @Nullable
    PaisResponse.PaisEntity providePais(Bundle args) {
        return args.getParcelable("pais");
    }

    @PerFragment
    @Provides
    List<CatalogoResponse.RecargaEntity> provideCarriers(Bundle args) {
        return args.getParcelableArrayList("carriers");
    }

    @PerFragment
    @Provides
    CarrierSelectionAdapter provideAdapter(
            @Named("recargaMap") Map<Integer, Integer> map,
            List<CatalogoResponse.RecargaEntity> carriers) {
        return new CarrierSelectionAdapter(carriers, map);
    }

    @PerFragment
    @Provides
    CarrierSelectionContract.Presenter providePresenter(
            CatalogoService service, SessionOperations session,
            @Nullable PaisResponse.PaisEntity paisModel, CatalogoResponse.RecargaEntity categoria) {
        return new CarrierSelectionPresenter(service, session.getUsuario(), categoria, paisModel,
                fragment);
    }
}
