package addcel.mobilecard.ui.usuario.scanpay.secure;

import androidx.annotation.NonNull;

import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;

/**
 * ADDCEL on 08/11/17.
 */
public interface ScanSecureContract {
    interface View {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void log(String msg);

        void onPagoStarted(@NonNull String html);

        void processResult(SPReceiptEntity response);
    }

    interface Presenter {

        void clearDisposables();

        SPReceiptEntity createResponse(String msg);

        void pago(SPPagoEntity data);
    }
}
