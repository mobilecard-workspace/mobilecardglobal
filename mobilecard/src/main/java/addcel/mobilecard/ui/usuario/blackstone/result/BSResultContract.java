package addcel.mobilecard.ui.usuario.blackstone.result;

import addcel.mobilecard.data.net.blackstone.model.BSPagoResponseEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 11/07/18.
 */
public interface BSResultContract {
    interface View extends ScreenView {
        void setError(BSPagoResponseEntity result);

        void setSuccess(BSPagoResponseEntity result);

        void clickShare();

        void clickFinish();
    }

    interface Presenter {

        BSPagoResponseEntity getResult();

        CharSequence buildSuccessMsg(String base);

        String buildScreenShotName();
    }
}
