package addcel.mobilecard.ui.usuario.colombia.recargas.monto;

import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import addcel.mobilecard.data.net.colombia.entity.ColombiaRecargaCarrierEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaTiendaEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 10/17/18.
 */
public interface ColombiaRecargaMontoContract {
    String KEY_RECARGA = "recarga";
    String KEY_CARRIER = "carrier";
    String KEY_MONTO = "monto";
    String KEY_CAMPOS = "campos";

    interface View extends ScreenView {
        void configRecyclerView();

        void clickContinuar();
    }

    interface Presenter {

        void fillContainer(ViewGroup group);

        TextInputLayout buildDataTilLayout(@NonNull Context context,
                                           ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity campo);

        int getCampoInputType(String tipo);

        boolean evalForm(ViewGroup group);

        void clearErrorsInForm(ViewGroup group);

        InputFilter[] buildFilter(int maxLen);

        int getSelectedMontoPosition(List<Integer> selectedItems);

        List<ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity> getFilledCampos(
                ViewGroup container);

        Bundle buildPagoBundle(List<ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity> campos,
                               ColombiaRecargaCarrierEntity.MontoEntity monto);
    }
}
