package addcel.mobilecard.ui.usuario.scanpay.menu;

import android.Manifest;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import addcel.mobilecard.ui.usuario.scanpay.manual.select.ScanManualSelectEstablecimientoFragment;
import addcel.mobilecard.ui.usuario.scanpay.qr.ScanQrFragment;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 08/08/18.
 */
public final class ScanMenuFragment extends Fragment implements ScanMenuContract.View {

    private static final String[] PERMISSION = {Manifest.permission.CAMERA};
    private final CompositeDisposable disposable = new CompositeDisposable();
    @Inject
    ScanMenuAdapter adapter;
    @Inject
    ScanPayActivity activity;
    private RecyclerView menuView;
    private RxPermissions permissions;

    public static synchronized ScanMenuFragment get() {
        return new ScanMenuFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().scanMenuSubcomponent(new ScanMenuModule(this)).inject(this);
        permissions = new RxPermissions(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen_scanpay_menu, container, false);
        menuView = v.findViewById(R.id.recycler_scanpay_menu);
        menuView.setAdapter(adapter);
        menuView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        ItemClickSupport.addTo(menuView).setOnItemClickListener((recyclerView, position, v1) -> {
            ScanMenuContract.Tipo tipo = adapter.getItem(position);
            clickItem(tipo);
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setFinished(Boolean.FALSE);
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(menuView);
        menuView.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        disposable.clear();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void clickItem(ScanMenuContract.Tipo t) {
        if (t == ScanMenuContract.Tipo.QR) {
            launchScanner();
        } else {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_scanpay, ScanManualSelectEstablecimientoFragment.get())
                    .hide(ScanMenuFragment.this)
                    .addToBackStack(null)
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .commit();
        }
    }

    @Override
    public void launchScanner() {
        disposable.add(
                permissions.request(PERMISSION)
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                activity.getSupportFragmentManager()
                                        .beginTransaction()
                                        .add(R.id.frame_scanpay, ScanQrFragment.get())
                                        .hide(ScanMenuFragment.this)
                                        .addToBackStack(null)
                                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                        .commit();
                            }
                        })
        );
    }
}
