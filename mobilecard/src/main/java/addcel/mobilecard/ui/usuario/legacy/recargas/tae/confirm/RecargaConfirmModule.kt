package addcel.mobilecard.ui.usuario.legacy.recargas.tae.confirm

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.PaisResponse.PaisEntity
import addcel.mobilecard.data.net.pago.BPApi
import addcel.mobilecard.data.net.pago.BPApi.Companion.get
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.token.TokenizerAPI.Companion.provideTokenizerAPI
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.domain.billpocket.BillPocketInteractor
import addcel.mobilecard.domain.billpocket.BillPocketInteractorImpl
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import java.text.NumberFormat

/**
 * ADDCEL on 19/10/17.
 */
@Module
class RecargaConfirmModule(private val fragment: RecargaConfirmFragment) {
    @PerFragment
    @Provides
    fun provideActivity(): PagoContainerActivity {
        return fragment.activity as PagoContainerActivity
    }

    @PerFragment
    @Provides
    fun provideService(retrofit: Retrofit): BPApi {
        return get(retrofit)
    }

    @PerFragment
    @Provides
    fun provideTokenizer(retrofit: Retrofit): TokenizerAPI {
        return provideTokenizerAPI(retrofit)
    }

    @PerFragment
    @Provides
    fun provideInteractor(api: BPApi, tokenizer: TokenizerAPI,
                          session: SessionOperations): BillPocketInteractor {
        return BillPocketInteractorImpl(api, tokenizer, session.usuario,
                session.minimalLocationData, CompositeDisposable())
    }

    @PerFragment
    @Provides
    fun providePresenter(
            interactor: BillPocketInteractor, numberFormat: NumberFormat): RecargaConfirmPresenter {

        val monto: CatalogoResponse.MontoEntity? = fragment.arguments?.getParcelable("monto")
        val paisModel: PaisEntity? = fragment.arguments?.getParcelable("pais")

        val referencia = fragment.arguments?.getString("referencia")!!
        val cardEntity: CardEntity = fragment.arguments?.getParcelable("card")!!
        val tipoCambio = fragment.arguments?.getDouble("tipoCambio")!!

        return RecargaConfirmPresenterImpl(interactor, monto, paisModel, numberFormat, cardEntity, referencia, fragment, tipoCambio)
    }

}

@PerFragment
@Subcomponent(modules = [RecargaConfirmModule::class])
interface RecargaConfirmSubcomponent {
    fun inject(fragment: RecargaConfirmFragment)
}