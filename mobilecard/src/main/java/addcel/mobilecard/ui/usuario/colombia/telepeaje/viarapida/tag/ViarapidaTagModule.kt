package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.tag

import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.colombia.VrTag
import addcel.mobilecard.di.scope.PerFragment
import android.widget.ArrayAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 9/26/19.
 */
@Module
class ViarapidaTagModule(val fragment: ViarapidaTagFragment) {

    @PerFragment
    @Provides
    fun provideMiddleware(r: Retrofit): ColombiaMiddleware {
        return ColombiaMiddleware.get(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            middleware: ColombiaMiddleware
    ): ViarapidaTagPresenter {
        return ViarapidaTagPresenterImpl(middleware, CompositeDisposable(), fragment)
    }

    @PerFragment
    @Provides
    fun provideAdapter(): ArrayAdapter<VrTag> {
        val adapter = ArrayAdapter<VrTag>(fragment.activity!!, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }
}

@PerFragment
@Subcomponent(modules = [ViarapidaTagModule::class])
interface ViarapidaTagSubcomponent {
    fun inject(fragment: ViarapidaTagFragment)
}