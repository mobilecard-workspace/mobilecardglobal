package addcel.mobilecard.ui.usuario.scanpay.secure;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 08/11/17.
 */
@PerFragment
@Subcomponent(modules = ScanSecureModule.class)
public interface ScanSecureSubcomponent {
    void inject(ScanSecureFragment fragment);
}
