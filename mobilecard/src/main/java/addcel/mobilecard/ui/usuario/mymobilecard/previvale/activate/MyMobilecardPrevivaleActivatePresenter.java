package addcel.mobilecard.ui.usuario.mymobilecard.previvale.activate;

import com.google.common.base.Strings;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.previvale.activate.PrevivaleActivateInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 11/16/18.
 */
class MyMobilecardPrevivaleActivatePresenter
        implements MyMobilecardPrevivaleActivateContract.Presenter {
    private final PrevivaleActivateInteractor interactor;
    private final MyMobilecardPrevivaleActivateContract.View view;

    MyMobilecardPrevivaleActivatePresenter(PrevivaleActivateInteractor interactor,
                                           MyMobilecardPrevivaleActivateContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        interactor.getCompositeDisposable().clear();
    }

    @Override
    public String getNombre() {
        return StringUtil.removeAcentosMultiple(interactor.getUsuario().getUsrNombre());
    }

    @Override
    public String getPaterno() {
        return StringUtil.removeAcentosMultiple(interactor.getUsuario().getUsrApellido());
    }

    @Override
    public String getMaterno() {
        return StringUtil.removeAcentosMultiple(
                Strings.nullToEmpty(interactor.getUsuario().getUsrMaterno()));
    }

    @Override
    public void getEstados() {
        view.showProgress();
        interactor.getEstados(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<EstadoResponse.EstadoEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<EstadoResponse.EstadoEntity> result) {
                        view.hideProgress();
                        view.setEstados(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                        view.finishActivity();
                    }
                });
    }

    @Override
    public void onClickActivate(PrevivaleUserRequest request) {
        view.showProgress();
        interactor.activate(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request,
                new InteractorCallback<CardEntity>() {
                    @Override
                    public void onSuccess(@NotNull CardEntity result) {
                        view.hideProgress();
                        view.onActivated(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }
}
