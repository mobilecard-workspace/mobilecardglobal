package addcel.mobilecard.ui.usuario.ingo;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 19/07/18.
 */
@PerActivity
@Subcomponent(modules = IngoModule.class)
public interface IngoSubcomponent {
    void inject(IngoActivity activity);
}
