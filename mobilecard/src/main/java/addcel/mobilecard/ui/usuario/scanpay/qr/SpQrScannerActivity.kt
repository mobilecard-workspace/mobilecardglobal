package addcel.mobilecard.ui.usuario.scanpay.qr

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.utils.JsonUtil
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.common.base.Strings
import com.google.zxing.Result
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_negocio_registro_pe_qr.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber
import java.util.regex.Pattern

@Parcelize
data class SpQrScannerModel(val idPais: Int = PaisResponse.PaisEntity.MX, var result: String = "") :
        Parcelable

interface SpQrScannerView : ZXingScannerView.ResultHandler {
    fun processQrByCountry(result: String)

    fun processMx(result: String)

    fun processUsa(result: String)

    fun processPe(result: String)
}

class SpQrScannerActivity : AppCompatActivity(), SpQrScannerView {
    companion object {
        const val REQUEST_CODE = 429
        const val RESULT_DATA = "data"

        fun get(context: Context, model: SpQrScannerModel): Intent {
            return Intent(context, SpQrScannerActivity::class.java).putExtra("model", model)
        }
    }

    private lateinit var scanner: ZXingScannerView
    private lateinit var model: SpQrScannerModel

    override fun handleResult(p0: Result?) {
        if (p0 != null) {
            Timber.d("Resultado Scan: %s", p0.text)
            processQrByCountry(p0.text)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_registro_pe_qr)
        scanner = scanner_pe_negocio_registro
        model = intent?.getParcelableExtra("model")!!

        Timber.d("SpQrScannerActivity -> Pais seleccionado: %d", model.idPais)
    }

    override fun onResume() {
        super.onResume()
        scanner.setResultHandler(this)
        scanner.startCamera()
    }

    public override fun onPause() {
        super.onPause()
        scanner.stopCamera()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, Intent())
        finish()
    }

    override fun processQrByCountry(result: String) {
        when (model.idPais) {
            PaisResponse.PaisEntity.MX -> processMx(result)
            PaisResponse.PaisEntity.US -> processUsa(result)
            PaisResponse.PaisEntity.PE -> processPe(result)
        }
    }

    override fun processMx(result: String) {
        if (Strings.isNullOrEmpty(result)) {
            Toast.makeText(
                    this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                    Toast.LENGTH_SHORT
            ).show()
            scanner.resumeCameraPreview(this)
        } else {
            try {
                val cleanQR = AddcelCrypto.decryptHard(result)

                Timber.d("Codido MX limpio: %s", cleanQR)

                if (JsonUtil.isJson(cleanQR)) {
                    model.result = cleanQR
                    val intent = Intent().putExtra(RESULT_DATA, model)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else {
                    Toast.makeText(
                            this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                            Toast.LENGTH_SHORT
                    ).show()
                    scanner.resumeCameraPreview(this)
                }


            } catch (e: IllegalStateException) {
                Toast.makeText(
                        this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                        Toast.LENGTH_SHORT
                ).show()
                scanner.resumeCameraPreview(this)
            }

        }
    }

    override fun processUsa(result: String) {
        if (Strings.isNullOrEmpty(result)) {
            Toast.makeText(
                    this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                    Toast.LENGTH_SHORT
            ).show()
            scanner.resumeCameraPreview(this)
        } else {
            try {
                val cleanQR = AddcelCrypto.decryptHard(result)

                Timber.d("Codido US limpio: %s", cleanQR)

                if (JsonUtil.isJson(cleanQR)) {
                    model.result = cleanQR
                    val intent = Intent().putExtra(RESULT_DATA, model)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else {
                    Toast.makeText(
                            this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                            Toast.LENGTH_SHORT
                    ).show()
                    scanner.resumeCameraPreview(this)
                }
            } catch (e: IllegalStateException) {
                Toast.makeText(
                        this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                        Toast.LENGTH_SHORT
                ).show()
                scanner.resumeCameraPreview(this)
            }

        }
    }

    override fun processPe(result: String) {
        if (result.isNotEmpty() && Pattern.matches("-?[0-9a-fA-F]+", result)) {
            Timber.d("Scan result: %s", result)
            model.result = result
            val intent = Intent().putExtra(RESULT_DATA, model)
            setResult(Activity.RESULT_OK, intent)
            finish()
        } else {
            Toast.makeText(
                    this, "El código escaneado no es válido. Intentalo de nuevo, por favor.",
                    Toast.LENGTH_SHORT
            ).show()
            scanner.resumeCameraPreview(this)
        }
    }
}
