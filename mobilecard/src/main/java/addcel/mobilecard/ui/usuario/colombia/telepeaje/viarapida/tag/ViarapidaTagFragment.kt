package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.tag

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.colombia.VrTag
import addcel.mobilecard.data.net.colombia.VrTagRequest
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.colombia.telepeaje.ColombiaTelepeajeActivity
import addcel.mobilecard.ui.usuario.colombia.telepeaje.menu.ColombiaTelepeajeModel
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.seleccion.ViarapidaSeleccionFragment
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.seleccion.ViarapidaSeleccionModel
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.google.common.collect.Lists
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_viarapida_tag.*
import javax.inject.Inject

/**
 * ADDCEL on 9/25/19.
 */

@Parcelize
data class ViarapidaTagModel(val proveedor: ColombiaTelepeajeModel) : Parcelable

interface ViarapidaTagView : ScreenView, TextWatcher {

    fun onListTags(tags: List<VrTag>)

    fun onOtroTagOn(on: Boolean)

    fun onIdCaptured(editable: Editable)

    fun onPlacaCaptured(editable: Editable)

    fun onAliasCaptured(editable: Editable)

    fun setSaveEnable(id: String, placa: String, alias: String)

    fun clickDelete()

    fun onTagDeleted(tag: VrTag)

    fun clickSaved()

    fun onTagSaved(tag: VrTag)

    fun historial()

    fun recargas()
}

class ViarapidaTagFragment : Fragment(), ViarapidaTagView {

    companion object {
        fun get(model: ViarapidaTagModel): ViarapidaTagFragment {
            val fragment = ViarapidaTagFragment()
            fragment.arguments = BundleBuilder().putParcelable("model", model).build()
            return fragment
        }
    }

    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var adapter: ArrayAdapter<VrTag>
    @Inject
    lateinit var presenter: ViarapidaTagPresenter
    lateinit var model: ViarapidaTagModel

    private var saveEnabled: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.viarapidaTagSubcomponent(ViarapidaTagModule(this))
                .inject(this)
        model = arguments?.getParcelable("model")!!
        presenter.getTags(session.usuario.ideUsuario)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_viarapida_tag, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinner_tags_viarapida_elige.adapter = adapter
        img_logo_tags_viarapida.setImageResource(model.proveedor.logoColor)
        switch_tags_viarapida_otro.setOnCheckedChangeListener { p0, p1 ->
            if (p0 != null) {
                onOtroTagOn(p1)
            }
        }
        text_tags_viarapida_id.addTextChangedListener(this)
        text_tags_viarapida_placa.addTextChangedListener(this)
        text_tags_viarapida_alias.addTextChangedListener(this)
        b_tags_viarapida_eliminar.setOnClickListener { clickDelete() }
        b_tags_viarapida_guardar.setOnClickListener { clickSaved() }
        b_tags_viarapida_recargas.setOnClickListener { recargas() }
        switch_tags_viarapida_otro.isChecked = false
        onOtroTagOn(false)
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun onListTags(tags: List<VrTag>) {
        adapter.addAll(tags)
    }

    override fun onOtroTagOn(on: Boolean) {
        val visibility = if (on) View.VISIBLE else View.GONE
        container_tags_viarapida_otro.visibility = visibility
        text_tags_viarapida_id.setText("")
        text_tags_viarapida_placa.setText("")
        text_tags_viarapida_alias.setText("")
    }

    override fun onIdCaptured(editable: Editable) {
        if (editable.isEmpty()) {
            text_tags_viarapida_id.error = getText(R.string.error_campo_generico)
        } else {
            text_tags_viarapida_id.error = null
        }

        setSaveEnable(
                editable.toString(), text_tags_viarapida_placa.text.toString(),
                text_tags_viarapida_alias.text.toString()
        )
    }

    override fun onPlacaCaptured(editable: Editable) {
        if (editable.isEmpty()) {
            text_tags_viarapida_placa.error = getText(R.string.error_campo_generico)
        } else {
            text_tags_viarapida_placa.error = null
        }

        setSaveEnable(
                text_tags_viarapida_id.text.toString(), editable.toString(),
                text_tags_viarapida_alias.text.toString()
        )
    }

    override fun onAliasCaptured(editable: Editable) {
        if (editable.isEmpty()) {
            text_tags_viarapida_alias.error = getText(R.string.error_campo_generico)
        } else {
            text_tags_viarapida_alias.error = null
        }

        setSaveEnable(
                text_tags_viarapida_id.text.toString(), text_tags_viarapida_id.text.toString(),
                editable.toString()
        )
    }

    override fun setSaveEnable(id: String, placa: String, alias: String) {
        b_tags_viarapida_guardar.isEnabled =
                id.isNotBlank() && placa.isNotBlank() && alias.isNotBlank()
    }

    override fun clickDelete() {
        if (!adapter.isEmpty)
            presenter.deleteTag(
                    session.usuario.ideUsuario,
                    spinner_tags_viarapida_elige.selectedItem as VrTag
            )
        else
            (activity as ContainerActivity).showError("Agregue una placa")
    }

    override fun onTagDeleted(tag: VrTag) {
        adapter.remove(tag)
    }

    override fun clickSaved() {
        val request =
                VrTagRequest(
                        text_tags_viarapida_alias.text.toString(), BuildConfig.ADDCEL_APP_ID, 2,
                        session.usuario.ideUsuario, StringUtil.getCurrentLanguage(),
                        text_tags_viarapida_placa.text.toString(), text_tags_viarapida_id.text.toString()
                )

        presenter.saveTag(request)
    }

    override fun onTagSaved(tag: VrTag) {
        if (adapter.isEmpty) {
            adapter.addAll(Lists.newArrayList(tag))
        } else {
            adapter.add(tag)
        }
    }

    override fun historial() {
    }

    override fun recargas() {
        if (adapter.isEmpty) {
            (activity as ContainerActivity).showError(
                    "Agregue una placa para iniciar el proceso de recarga"
            )
        } else {
            val sModel = ViarapidaSeleccionModel(
                    model.proveedor,
                    spinner_tags_viarapida_elige.selectedItem as VrTag
            )
            (activity as ContainerActivity).fragmentManagerLazy.commit {
                add(R.id.frame_colombia_telepeaje, ViarapidaSeleccionFragment.get(sModel))
                hide(this@ViarapidaTagFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }
    }

    override fun showProgress() {
        (activity as ColombiaTelepeajeActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ColombiaTelepeajeActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ColombiaTelepeajeActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ColombiaTelepeajeActivity).showSuccess(msg)
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            when {
                text_tags_viarapida_id.hasFocus() -> onIdCaptured(p0)
                text_tags_viarapida_placa.hasFocus() -> onPlacaCaptured(p0)
                text_tags_viarapida_alias.hasFocus() -> onAliasCaptured(p0)
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}