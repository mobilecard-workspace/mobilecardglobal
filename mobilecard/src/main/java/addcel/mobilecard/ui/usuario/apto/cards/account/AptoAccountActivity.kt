package addcel.mobilecard.ui.usuario.apto.cards.account

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.accounts.AccountsAPI
import addcel.mobilecard.data.net.apto.accounts.AptoCardBalanceEntity
import addcel.mobilecard.data.net.apto.accounts.AptoCardData
import addcel.mobilecard.data.net.apto.accounts.Data
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.cvv.MobilecardCvvTimerActivity
import addcel.mobilecard.ui.cvv.MobilecardCvvTimerModel
import addcel.mobilecard.ui.usuario.apto.cards.detail.AptoAccountDetailActivity
import addcel.mobilecard.ui.usuario.apto.cards.detail.AptoAccountDetailModel
import addcel.mobilecard.ui.usuario.apto.cards.settings.SettingsActivity
import addcel.mobilecard.ui.usuario.apto.cards.settings.SettingsModel
import addcel.mobilecard.utils.ErrorUtil
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_account.*
import mx.mobilecard.crypto.AddcelCrypto
import java.text.NumberFormat
import java.util.*

@Parcelize
data class AptoAccountModel(val userToken: String, val cardEntity: CardEntity) : Parcelable

@Parcelize
data class TransactionModel(
        val concept: String, val amount: String, val date: String,
        val status: String, val category: String, val deviceType: String, val transactionType: String,
        val transactionId: String, val toYourAccount: Double, val id: String, val exchangeRate: String,
        val fromAccount: Double, val fromId: String
) : Parcelable

data class TransactionList(val date: String, val models: List<TransactionModel>)

class AptoAccountActivity : AppCompatActivity() {

    companion object {
        fun get(context: Context, model: AptoAccountModel): Intent {
            return Intent(context, AptoAccountActivity::class.java).putExtra("model", model)
        }

        val USA_FORMAT: NumberFormat = NumberFormat.getCurrencyInstance(Locale.US)
    }

    val api = AccountsAPI.get(BuildConfig.APTO_URL)
    val compositeDisposable = CompositeDisposable()
    lateinit var adapter: AptoAccountAdapter
    lateinit var model: AptoAccountModel
    lateinit var card: AptoCardData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_account)
        model = intent?.getParcelableExtra("model")!!
        adapter = AptoAccountAdapter(mutableListOf(), NumberFormat.getCurrencyInstance(Locale.US))

        val logo: View = card_img.findViewById(R.id.card_logo)
        logo.visibility = View.GONE

        apto_account_transactions.adapter = adapter
        apto_account_transactions.layoutManager = LinearLayoutManager(this)
        ItemClickSupport.addTo(apto_account_transactions).setOnItemClickListener { _, position, _ ->
            val o = adapter.getItem(position)
            clickMovement(o)
        }

        card_img.setOnClickListener { showCvv() }
        b_apto_account_cvv.setOnClickListener { showCvv() }

        setCardInfo(model.cardEntity)
        compositeDisposable.add(getBalance())
        compositeDisposable.add(getTransactions())
    }

    private fun showCvv() {
        AlertDialog.Builder(this@AptoAccountActivity).setTitle("Aviso")
                .setMessage("¿Mostrar el CVV de la tarjeta?")
                .setPositiveButton(
                        "Mostrar"
                ) { d, _ ->
                    startActivity(
                            MobilecardCvvTimerActivity.get(
                                    this@AptoAccountActivity,
                                    MobilecardCvvTimerModel(model.cardEntity.codigo!!)
                            )
                    )
                    d.dismiss()
                }.setNegativeButton("Cancelar") { d, _ -> d.dismiss() }.show()
    }

    private fun setBalanceInfo(balance: AptoCardBalanceEntity) {
        val balanceText =
                USA_FORMAT.format(balance.balance.amount) + " " + balance.balance.currency
        view_apto_account_curr_balance.text = balanceText

        val spendableText =
                USA_FORMAT.format(balance.amountSpendable.amount) + " " + balance.amountSpendable.currency
        view_apto_account_spendable.text = spendableText
    }

    private fun clickMovement(entity: Any) {
        if (entity is Data) {
            val pModel = AptoAccountDetailModel(entity)
            startActivity(AptoAccountDetailActivity.get(this, pModel))
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_apto_card, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        compositeDisposable.add(getAccountInfo())
        return super.onOptionsItemSelected(item)
    }

    private fun setCardInfo(cardEntity: CardEntity) {
        view_apto_account_curr_balance.text = cardEntity.balance.toString()
        view_apto_account_spendable.text = cardEntity.balance.toString()
        card_img.cardNumber = AddcelCrypto.decryptHard(cardEntity.pan)
        card_img.cardName = cardEntity.nombre
        card_img.expiryDate = AddcelCrypto.decryptHard(cardEntity.vigencia)
        card_img.cardFrontBackground = R.drawable.tarjeta_fondo
        val logo: View = card_img.findViewById(R.id.card_logo)
        logo.visibility = View.GONE
    }

    private fun setCardInfo(fullCard: AptoCardData) {
        card = fullCard
        val sModel = SettingsModel(card, model.cardEntity, model.userToken)
        startActivity(SettingsActivity.get(this, sModel))
    }

    private fun getAccountInfo(): Disposable {
        showProgress()
        return api.getAccount(
                "Bearer " + model.userToken,
                model.cardEntity.accountId!!
        ).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
        ).subscribe({
            hideProgress()
            setCardInfo(it)
        },
                {
                    hideProgress()
                    val code = ErrorUtil.getFormattedHttpErrorCode(it)
                    if (code == 404) {
                        Toasty.error(this@AptoAccountActivity, "La tarjeta no existe").show()
                    } else {
                        Toasty.error(this@AptoAccountActivity, ErrorUtil.getFormattedHttpErrorMsg(it))
                                .show()
                    }
                })
    }

    private fun getTransactions(): Disposable {
        showProgress()
        return api.transactions(
                "Bearer " + model.userToken,
                model.cardEntity.accountId!!
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            hideProgress()
                            if (!it.tData.isNullOrEmpty())
                                adapter.update(it.tData)
                        }, {
                    hideProgress()
                    Toasty.error(this, ErrorUtil.getFormattedHttpErrorMsg(it)).show()
                }
                )
    }

    private fun getBalance(): Disposable {
        showProgress()
        return api.getBalance("Bearer " + model.userToken, model.cardEntity.accountId!!)
                .subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({
                    hideProgress()
                    setBalanceInfo(it)
                }, {
                    hideProgress()
                    Toasty.error(
                            this@AptoAccountActivity, ErrorUtil.getFormattedHttpErrorMsg(it)
                    )
                })
    }

    private fun showProgress() {
        if (progress_account.visibility == View.GONE) progress_account.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        if (progress_account.visibility == View.VISIBLE) progress_account.visibility = View.GONE
    }
}
