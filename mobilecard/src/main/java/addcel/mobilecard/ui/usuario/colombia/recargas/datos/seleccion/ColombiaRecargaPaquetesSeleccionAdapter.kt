package addcel.mobilecard.ui.usuario.colombia.recargas.datos.seleccion

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.Paquete
import addcel.mobilecard.ui.custom.adapter.SelectableAdapter
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * ADDCEL on 9/23/19.
 */
class ColombiaRecargaPaquetesSeleccionAdapter :
        SelectableAdapter<ColombiaRecargaPaquetesSeleccionAdapter.ViewHolder>() {

    private val montos: MutableList<Paquete> = ArrayList()
    private val numberFormat = NumberFormat.getCurrencyInstance(Locale("es", "CO"))

    fun update(models: List<Paquete>) {
        montos.clear()
        montos.addAll(models)
        notifyDataSetChanged()
    }

    fun getItem(pos: Int): Paquete {
        return montos[pos]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemLayoutView =
                LayoutInflater.from(parent.context).inflate(R.layout.view_monto, parent, false)
        return ViewHolder(itemLayoutView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val monto = montos[position]
        holder.montoText.text = "${monto.descripcion} - ${numberFormat.format(monto.valor.toInt())}"
        holder.montoText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0F)
        // TextViewCompat.setAutoSizeTextTypeWithDefaults(holder.montoText,
        //   TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM)
        if (isSelected(position)) {
            holder.montoText.setBackgroundColor(
                    ContextCompat.getColor(holder.montoText.context, R.color.accent)
            )
            holder.montoText.setTextColor(Color.WHITE)
        } else {
            holder.montoText.background =
                    ContextCompat.getDrawable(holder.montoText.context, R.drawable.sh_rectangle)
            holder.montoText.setTextColor(Color.GRAY)
        }
    }

    override fun getItemCount(): Int {
        return montos.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val montoText: TextView = view.findViewById(R.id.text_monto)
    }
}