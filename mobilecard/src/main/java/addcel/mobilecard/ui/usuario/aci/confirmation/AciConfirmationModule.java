package addcel.mobilecard.ui.usuario.aci.confirmation;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.aci.confirmation.AciConfirmationInteractor;
import addcel.mobilecard.domain.aci.confirmation.AciConfirmationInteractorImpl;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 17/10/17.
 */
@Module
public final class AciConfirmationModule {
    private final AciConfirmationFragment fragment;

    AciConfirmationModule(AciConfirmationFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    AciActivity provideActivity() {
        return (AciActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    AciService provideAciService(Retrofit retrofit) {
        return retrofit.create(AciService.class);
    }

    @PerFragment
    @Provides
    AciConfirmationInteractor provideInteractor(AciService service,
                                                SessionOperations session) {
        return new AciConfirmationInteractorImpl(service, session);
    }

    @PerFragment
    @Provides
    AciConfirmationContract.Presenter providePresenter(
            AciConfirmationInteractor interactor) {

        boolean withData =
                fragment.getArguments() != null && fragment.getArguments().getBoolean("withData");

        AciConfirmationPresenter.Builder builder =
                new AciConfirmationPresenter.Builder(interactor, fragment).setAccount(
                        fragment.getArguments().getString("referencia"))
                        .setAddress(fragment.getArguments().getParcelable("address"))
                        .setCard(fragment.getArguments().getParcelable("card"))
                        .setFee(fragment.getArguments().getDouble("comision"))
                        .setMonto(fragment.getArguments().getDouble("amount"))
                        .setServiceModel(fragment.getArguments().getParcelable("service"))
                        .setTotal(0)
                        .setWithData(withData);

        if (!withData) {
            builder.setCiudad(fragment.getArguments().getString("ciudad"))
                    .setCvv(fragment.getArguments().getString("cvv"))
                    .setDireccion(fragment.getArguments().getString("direccion"))
                    .setEstado(fragment.getArguments().getParcelable("estado"))
                    .setZipCode(fragment.getArguments().getString("zipCode"));
        }

        return builder.build();
    }
}
