package addcel.mobilecard.ui.usuario.edocuenta.list

import addcel.mobilecard.McConstants
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.domain.historial.HistorialInteractor
import addcel.mobilecard.domain.historial.HistorialInteractorImpl
import addcel.mobilecard.ui.usuario.edocuenta.EdocuentaActivity
import com.google.common.collect.Lists
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import java.text.NumberFormat
import javax.inject.Named

/**
 * ADDCEL on 2019-06-12.
 */
@Module
class EdocuentaListModule(val fragment: HistorialListFragment) {

    @PerFragment
    @Provides
    fun provideActivity(): EdocuentaActivity {
        return fragment.activity as EdocuentaActivity
    }

    @PerFragment
    @Provides
    fun provideInteractor(
            api: WalletAPI,
            session: SessionOperations
    ): HistorialInteractor {
        return HistorialInteractorImpl(api, session.usuario, CompositeDisposable())
    }

    @PerFragment
    @Provides
    fun provideAdapter(): EdocuentaListAdapter {
        return EdocuentaListAdapter(Lists.newArrayList())
    }

    @PerFragment
    @Provides
    fun providePresenter(interactor: HistorialInteractor): Presenter {
        return HistorialListPresenter(interactor, fragment)
    }
}
