package addcel.mobilecard.ui.usuario.main.menu;

import java.util.Arrays;
import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.ui.usuario.main.MainContract;

/**
 * ADDCEL on 04/10/17.
 */
enum MenuModel {


    /**
     * Acceso a creación / datos My bg_card_mobilecard
     */
    MOBILE_TAG(MainContract.UseCase.MOBILE_TAG, R.drawable.mobiletag, R.string.nav_mobiletag),


    /**
     * Acceso a scan&pay
     */
    SCANPAY(MainContract.UseCase.SCAN, R.drawable.scanpay, R.string.nav_scanpay),

    ESCANEA(MainContract.UseCase.SCAN, R.drawable.scanpay, R.string.nav_escanea),

    /**
     * Acceso a casheo de cheques
     */
    CHECKS(MainContract.UseCase.CHEQUE, R.drawable.cheques, R.string.nav_cashyourcheck),

    /**
     * Acceso a transferencias Viamericas
     */
    TRANSFERS(MainContract.UseCase.TRANSFERENCIAS, R.drawable.transferencias, R.string.nav_transfer),

    /**
     * Acceso a transferencias H2H
     */
    TRANSFERS_MX(MainContract.UseCase.TRANSFERENCIAS, R.drawable.transferencias,
            R.string.nav_transfer_mx),

    /**
     * Acceso a pago de servicios
     */
    BILLING(MainContract.UseCase.SERVICIOS, R.drawable.servicios, R.string.nav_billpayments),

    /**
     * Acceso a pago de servicios
     */
    BILLING_US(MainContract.UseCase.SERVICIOS, R.drawable.servicios, R.string.nav_billpayments_mx),

    /**
     * Acceso a recargas tae & peaje
     */
    TOPUP(MainContract.UseCase.RECARGAS, R.drawable.recargas, R.string.nav_topups),

    /**
     * Acceso a Wallet
     */
    WALLET(MainContract.UseCase.WALLET, R.drawable.cartera, R.string.nav_wallet),

    /**
     * Acceso a favoritos
     */
    FAVORITES(MainContract.UseCase.FRECUENTES, R.drawable.favoritos, R.string.nav_favorites),

    /*
    MENU COLOMBIA
     */
    COL_SOAT(MainContract.UseCase.COL_SOAT, R.drawable.col_soat, R.string.nav_col_soat),

    COL_SERVICIOS(MainContract.UseCase.COL_SERVICIOS, R.drawable.servicios, R.string.nav_billpayments),

    COL_PREPAGO(MainContract.UseCase.COL_PREPAGOS, R.drawable.servicios, R.string.nav_col_prepago),

    COL_TAE(MainContract.UseCase.COL_TAE, R.drawable.col_tae, R.string.nav_col_tae),

    COL_PEAJE(MainContract.UseCase.COL_PEAJE, R.drawable.col_tag, R.string.nav_col_peaje),


    /**
     * Acceso a creación / datos My bg_card_mobilecard
     */
    MY_MC(MainContract.UseCase.MY_MC, R.drawable.mi_mobilecard, R.string.txt_menu_mymobilecard);

    final int useCase;
    final int icon;
    final int desc;

    MenuModel(int useCase, int icon, int desc) {
        this.useCase = useCase;
        this.icon = icon;
        this.desc = desc;
    }

    public List<MenuModel> toList() {
        return Arrays.asList(values());
    }
}
