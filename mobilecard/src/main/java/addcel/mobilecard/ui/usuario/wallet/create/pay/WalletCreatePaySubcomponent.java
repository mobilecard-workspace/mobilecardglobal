package addcel.mobilecard.ui.usuario.wallet.create.pay;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 10/10/17.
 */
@PerFragment
@Subcomponent(modules = WalletCreatePayModule.class)
public interface WalletCreatePaySubcomponent {
    void inject(WalletCreatePayFragment fragment);
}
