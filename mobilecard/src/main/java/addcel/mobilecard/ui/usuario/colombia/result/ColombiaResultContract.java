package addcel.mobilecard.ui.usuario.colombia.result;

import addcel.mobilecard.data.net.colombia.entity.ColombiaPagoResponseEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 10/29/18.
 */
public interface ColombiaResultContract {

    interface View extends ScreenView {

        void clickOk();
    }

    interface Presenter {
        boolean isSuccessful();

        String getTitle();

        String getMsg();


        SPReceiptEntity getReceipt();
    }
}
