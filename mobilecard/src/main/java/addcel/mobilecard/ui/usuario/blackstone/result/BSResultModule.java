package addcel.mobilecard.ui.usuario.blackstone.result;

import android.os.Bundle;

import java.text.NumberFormat;
import java.util.Objects;

import addcel.mobilecard.data.net.blackstone.model.BSPagoResponseEntity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 11/07/18.
 */
@Module
public final class BSResultModule {
    private final BSResultFragment fragment;

    BSResultModule(BSResultFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    BlackstoneActivity provideActivity() {
        return (BlackstoneActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Bundle provideBundle() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    BSPagoResponseEntity provideResult(Bundle bundle) {
        return bundle.getParcelable("result");
    }

    @PerFragment
    @Provides
    BSResultContract.Presenter providePresenter(BSPagoResponseEntity pago,
                                                NumberFormat format) {
        return new BSResultPresenter(pago, format);
    }
}
