package addcel.mobilecard.ui.usuario.aci.list;

import androidx.annotation.Nullable;

import java.util.List;

import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.event.impl.AciServiceInsertEvent;
import addcel.mobilecard.event.impl.AciServiceUpdateEvent;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 1/23/19.
 */
public interface AciListContract {
    interface View extends ScreenView {

        void onItemsUpdated(List<ServiceModel> models);

        void onLoadFinished();

        void onItemClicked(int pos);
    }

    interface Presenter {

        void clearCompositeDisposable();

        void register();

        void unregister();

        void downloadItems();

        void onDownload(AciServiceInsertEvent event);

        void filterItems(@Nullable String pattern);

        void onFilter(AciServiceUpdateEvent event);
    }
}
