package addcel.mobilecard.ui.usuario.mymobilecard.previvale.activate;

import android.widget.ArrayAdapter;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.previvale.activate.PrevivaleActivateInteractor;
import addcel.mobilecard.domain.previvale.activate.PrevivaleActivateInteractorImpl;
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 11/16/18.
 */
@Module
public class MyMobilecardPrevivaleActivateModule {
    private final MyMobilecardPrevivaleActivateFragment fragment;

    public MyMobilecardPrevivaleActivateModule(MyMobilecardPrevivaleActivateFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MyMobilecardActivity provideActivity() {
        return (MyMobilecardActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    ArrayAdapter<EstadoResponse.EstadoEntity> provideEstados(
            MyMobilecardActivity activity) {
        ArrayAdapter<EstadoResponse.EstadoEntity> adapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter.setNotifyOnChange(true);
        return adapter;
    }

    @PerFragment
    @Provides
    PrevivaleActivateInteractor provideInteractor(WalletAPI api,
                                                  CatalogoService catalogo, SessionOperations session) {
        return new PrevivaleActivateInteractorImpl(api, catalogo, session.getUsuario());
    }

    @PerFragment
    @Provides
    MyMobilecardPrevivaleActivateContract.Presenter providePresenter(
            PrevivaleActivateInteractor interactor) {
        return new MyMobilecardPrevivaleActivatePresenter(interactor, fragment);
    }
}
