package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.confirm

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.pago.BPApi
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 31/10/19.
 */
@Module
class MobileTagConfirmModule(val fragment: MobileTagConfirmFragment) {

    @PerFragment
    @Provides
    fun provideActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideViewModel(): MobileTagConfirmModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideCard(): CardEntity {
        return fragment.arguments?.getParcelable("card")!!
    }

    @PerFragment
    @Provides
    fun providePagoApi(retrofit: Retrofit): BPApi {
        return BPApi.get(retrofit)
    }

    @PerFragment
    @Provides
    fun provideTokenizer(retrofit: Retrofit): TokenizerAPI {
        return TokenizerAPI.provideTokenizerAPI(retrofit)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            bp: BPApi,
            token: TokenizerAPI,
            session: SessionOperations
    ): MobileTagConfirmPresenter {
        return MobileTagConfirmPresenterImpl(bp, token, session, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [MobileTagConfirmModule::class])
interface MobileTagConfirmSubcomponent {
    fun inject(fragment: MobileTagConfirmFragment)
}