package addcel.mobilecard.ui.usuario.colombia.recargas.datos.seleccion

import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.di.scope.PerFragment
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 9/23/19.
 */
@Module
class ColombiaRecargaPaquetesSeleccionModule(
        val fragment: ColombiaRecargaPaquetesSeleccionFragment
) {

    @PerFragment
    @Provides
    fun provideModel(): ColombiaRecargaPaquetesSeleccionModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideAdapter(): ColombiaRecargaPaquetesSeleccionAdapter {
        return ColombiaRecargaPaquetesSeleccionAdapter()
    }

    @PerFragment
    @Provides
    fun providePresenter(
            catalogoAPI: CatalogoAPI,
            picasso: Picasso
    ): ColombiaRecargaPaquetesSeleccionPresenter {
        return ColombiaRecargaPaquetesSeleccionPresenterImpl(
                catalogoAPI, picasso,
                CompositeDisposable(), fragment
        )
    }
}

@PerFragment
@Subcomponent(modules = [ColombiaRecargaPaquetesSeleccionModule::class])
interface ColombiaRecargaPaquetesSeleccionSubcomponent {
    fun inject(fragment: ColombiaRecargaPaquetesSeleccionFragment)
}