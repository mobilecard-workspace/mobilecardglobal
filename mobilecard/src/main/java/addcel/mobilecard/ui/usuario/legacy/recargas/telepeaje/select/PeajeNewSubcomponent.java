package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/08/17.
 */
@PerFragment
@Subcomponent(modules = PeajeNewModule.class)
public interface PeajeNewSubcomponent {
    void inject(PeajeNewFragment fragment);
}
