package addcel.mobilecard.ui.usuario.billpocket

import addcel.mobilecard.BuildConfig
import android.graphics.Bitmap
import android.net.http.SslError
import android.webkit.*
import com.google.common.base.Strings
import timber.log.Timber

/**
 * ADDCEL on 2019-09-09.
 */
class BillPocketSecureClient(
        val presenter: BillPocketSecurePresenter,
        val view: BillPocketSecureView
) : WebViewClient() {

    override fun onPageStarted(webView: WebView, url: String, favicon: Bitmap?) {
        try {
            view.showProgress()
            if (BuildConfig.DEBUG) Timber.d("onPageStarted: %s", url)
        } catch (e: KotlinNullPointerException) {
            e.printStackTrace()
        }
    }

    override fun onPageFinished(webView: WebView, url: String) {
        view.hideProgress()
        if (BuildConfig.DEBUG) Timber.d("onPageFinished: %s", url)
        view.setCurrentUrl(url)
        presenter.getCardDataForPeaje(url, view.fetchModel().token.paths?.form!!)
        webView.evaluateJavascript(JS_LOG_FUN, null)
    }

    override fun onReceivedSslError(webView: WebView, handler: SslErrorHandler, error: SslError) {
        view.hideProgress()
        if (BuildConfig.DEBUG) {
            handler.proceed()
        } else {
            if (error.url.contains(BuildConfig.BASE_URL)) {
                view.showError(Strings.nullToEmpty(error.toString()))
            } else {
                view.showDebugHandlerDialog(handler, error)
            }
        }
    }

    override fun onReceivedHttpError(
            webView: WebView, request: WebResourceRequest,
            errorResponse: WebResourceResponse
    ) {
        view.hideProgress()
        if (errorResponse.statusCode != 404 && errorResponse.statusCode != 403) {
            view.showError(errorResponse.reasonPhrase)
        }
    }

    companion object {

        private const val JS_LOG_FUN =
                ("javascript:window.HTMLOUT.logHtml(" + "'<html>'" + "+document.getElementsByTagName('html')[0].innerHTML+" + "'</html>'" + ");")
    }
}