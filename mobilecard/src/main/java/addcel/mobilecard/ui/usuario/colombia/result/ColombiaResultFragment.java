package addcel.mobilecard.ui.usuario.colombia.result;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.common.base.Strings;
import com.squareup.phrase.Phrase;

import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.ui.ContainerActivity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.utils.BundleBuilder;

/**
 * ADDCEL on 10/29/18.
 */
public final class ColombiaResultFragment extends Fragment implements ColombiaResultContract.View {
    private static final NumberFormat CURR_FORMAT =
            NumberFormat.getCurrencyInstance(new Locale("es", "CO"));
    @Inject
    ContainerActivity activity;
    @Inject
    ColombiaResultContract.Presenter presenter;
    private ImageView resultImg;
    private TextView titleView;
    private TextView msgView;
    private Button okButton;

    public static synchronized ColombiaResultFragment get(SPReceiptEntity result) {
        ColombiaResultFragment fragment = new ColombiaResultFragment();
        Bundle arguments = new BundleBuilder().putParcelable("recargaResult", result).build();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .colombiaResultSubcomponent(new ColombiaResultModule(this))
                .inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_colombia_result, container, false);
        resultImg = view.findViewById(R.id.img_colombia_result);
        titleView = view.findViewById(R.id.view_colombia_result_title);
        msgView = view.findViewById(R.id.view_colombia_result_msg);
        okButton = view.findViewById(R.id.b_colombia_result_ok);
        okButton.setOnClickListener(v -> clickOk());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleView.setText(presenter.getTitle());
        if (presenter.isSuccessful()) {
            activity.setStreenStatus(ContainerScreenView.STATUS_FINISHED);
            resultImg.setImageResource(R.drawable.ic_historial_success);
            msgView.setVisibility(View.VISIBLE);
            msgView.setText(buildDesc());
            okButton.setVisibility(View.VISIBLE);
        } else {
            activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
            resultImg.setImageResource(R.drawable.ic_historial_error);
            msgView.setText(presenter.getMsg());
            okButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void clickOk() {
        activity.finish();
    }

    private CharSequence buildDesc() {

        return Phrase.from(activity, R.string.txt_colombia_confirm_invoice)
                .put("monto", CURR_FORMAT.format(presenter.getReceipt().getAmount()))
                .put("comision", CURR_FORMAT.format(0.0))
                .put("fecha",
                        DateFormat.format("dd/MM/yyyy HH:mm:ss", presenter.getReceipt().getDateTime()))
                .put("integrador", String.valueOf(presenter.getReceipt().getOpId()))
                .put("transaccion", String.valueOf(presenter.getReceipt().getIdTransaccion()))
                .put("autorizador", String.valueOf(presenter.getReceipt().getOpId()))
                .put("aprobacion", Strings.nullToEmpty(presenter.getReceipt().getAuthNumber()))
                .format();

    }
}
