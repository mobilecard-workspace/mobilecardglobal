package addcel.mobilecard.ui.usuario.viamericas.recipient.create

import addcel.mobilecard.R
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.VmCityModel
import addcel.mobilecard.data.net.viamericas.model.VmStateModel
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog
import addcel.mobilecard.ui.custom.extension.DetachableClickListener
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Build
import android.widget.ArrayAdapter
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Named

/**
 * ADDCEL on 27/03/17.
 */

@Module
class ViamericasRecipientCreateModule(private val activity: ViamericasRecipientCreateActivity) {


    @PerActivity
    @Provides
    fun provideInfoDialog(): AlertDialog {
        return AlertDialog.Builder(activity).setTitle(
                android.R.string.dialog_alert_title
        )
                .setMessage(R.string.info_viamericas_2)
                .setPositiveButton(
                        android.R.string.ok,
                        DetachableClickListener.wrap(DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() })
                )
                .create()
    }

    @PerActivity
    @Provides
    fun providePicker(): DatePickerDialog {
        val dpDialog = if (Build.VERSION.SDK_INT != 24) {
            DatePickerDialog(
                    activity, R.style.CustomDatePickerDialogTheme,
                    activity, Calendar.getInstance().get(Calendar.YEAR) - 18,
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            )
        } else {
            FixedHoloDatePickerDialog(
                    activity, activity,
                    Calendar.getInstance().get(Calendar.YEAR) - 18,
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            )
        }
        dpDialog.setTitle(R.string.txt_fecha_nacimiento)
        return dpDialog
    }

    @PerActivity
    @Provides
    fun provideValidator(adapter: ViewDataAdapter<TextInputLayout, String>): Validator {
        val validator = Validator(activity)
        validator.registerAdapter(TextInputLayout::class.java, adapter)
        validator.setValidationListener(activity)
        return validator
    }

    @PerActivity
    @Provides
    fun provideModel(): ViamericasRecipientCreateModel {
        return activity.intent.getParcelableExtra("model")!!
    }


    @PerActivity
    @Provides
    fun provideEstadoAdapter(): ArrayAdapter<VmStateModel> {
        val estadoArrayAdapter =
                ArrayAdapter<VmStateModel>(activity, android.R.layout.simple_spinner_item)
        estadoArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        estadoArrayAdapter.setNotifyOnChange(true)
        return estadoArrayAdapter
    }

    @PerActivity
    @Provides
    @Named("viamericaCityAdapter")
    fun provideCityAdapter(): ArrayAdapter<VmCityModel> {
        val cityArrayAdapter =
                ArrayAdapter<VmCityModel>(activity, android.R.layout.simple_spinner_item)
        cityArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        cityArrayAdapter.setNotifyOnChange(true)
        return cityArrayAdapter
    }

    @PerActivity
    @Provides
    fun providePresenter(service: ViamericasService): ViamericasRecipientCreatePresenter {
        return ViamericasRecipientCreatePresenterImpl(service, CompositeDisposable(), activity)
    }
}

@PerActivity
@Subcomponent(modules = [ViamericasRecipientCreateModule::class])
interface ViamericasRecipientCreateSubcomponent {
    fun inject(activity: ViamericasRecipientCreateActivity)
}

