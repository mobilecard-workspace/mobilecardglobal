package addcel.mobilecard.ui.usuario.main.contacto;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import addcel.mobilecard.R;

/**
 * ADDCEL on 22/11/16.
 */
public final class ContactoAdapter extends RecyclerView.Adapter<ContactoAdapter.ViewHolder> {

    private static final int[] IMGS = {
            R.drawable.contacto_llamanos, R.drawable.contacto_mail,
            R.drawable.contacto_web, R.drawable.contacto_whtasapp,
    };
    private static final int[] TEXTS = {
            R.string.txt_contacto_marcanos, R.string.txt_contacto_escribenos,
            R.string.txt_contacto_mobilecar, R.string.txt_contacto_whatsapp,
    };

    @Override
    public ContactoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_img_text_no_ratio, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.icon.setImageResource(IMGS[position]);
        holder.contactoText.setText(TEXTS[position]);
    }

    @Override
    public int getItemCount() {
        return TEXTS.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView icon;
        private final TextView contactoText;

        ViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.img_menu_item_icon);
            contactoText = itemView.findViewById(R.id.text_menu_item_name);
        }
    }
}
