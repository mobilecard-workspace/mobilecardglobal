package addcel.mobilecard.ui.usuario.main.pais.servicio;

import com.google.common.base.Preconditions;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import dagger.Module;
import dagger.Provides;

@Module
public final class PaisModule {
    private final PaisFragment fragment;

    PaisModule(PaisFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    int provideModule() {
        return Preconditions.checkNotNull(fragment.getArguments()).getInt("module");
    }

    @PerFragment
    @Provides
    MainActivity provideActivity() {
        return (MainActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    PaisAdapter provideAdapter(SessionOperations session) {
        return new PaisAdapter(Pais.values(), session.getUsuario().getIdPais());
    }

    @PerFragment
    @Provides
    PaisContract.Presenter providesPresenter(CatalogoService service,
                                             SessionOperations session) {
        return new PaisPresenter(service, session.getUsuario(), fragment);
    }
}
