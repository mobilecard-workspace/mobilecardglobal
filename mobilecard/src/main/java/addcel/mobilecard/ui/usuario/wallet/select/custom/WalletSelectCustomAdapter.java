package addcel.mobilecard.ui.usuario.wallet.select.custom;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.ui.usuario.wallet.select.WalletCardSelectModel;
import addcel.mobilecard.utils.ListUtil;
import addcel.mobilecard.utils.StringUtil;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 09/10/17.
 */
public final class WalletSelectCustomAdapter
        extends RecyclerView.Adapter<WalletSelectCustomAdapter.ViewHolder> {

    private final List<WalletCardSelectModel> models;

    public WalletSelectCustomAdapter(List<WalletCardSelectModel> models) {
        this.models = models;
    }

    public void setChecked(int pos) {
        boolean val = models.get(pos).isChecked();
        for (int i = 0, size = models.size(); i < size; i++) {
            WalletCardSelectModel model = models.get(i);
            if (i == pos) {
                model.setChecked(!val);
            } else {
                model.setChecked(false);
            }
        }
        notifyDataSetChanged();
    }

    int getCheckedCardPosition() {
        int checkedIndex = -1;
        if (ListUtil.notEmpty(models)) {
            for (int i = 0, size = models.size(); i < size; i++) {
                if (models.get(i).isChecked()) {
                    checkedIndex = i;
                }
            }
        }
        return checkedIndex;
    }

    public void update(List<WalletCardSelectModel> models) {
        this.models.clear();
        this.models.addAll(models);
        notifyDataSetChanged();
    }

    public WalletCardSelectModel getItem(int pos) {
        return this.models.get(pos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_select_horizontal, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WalletCardSelectModel model = models.get(position);
        String data = String.format("%s\n%s\n%s", model.getCard().getNombre(),
                StringUtil.maskCard(AddcelCrypto.decryptHard(model.getCard().getPan())),
                model.getCard().getTipo().getDescription());
        holder.nameText.setText(data);
        if (model.isChecked()) {
            holder.check.setVisibility(View.VISIBLE);
        } else {
            holder.check.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_wallet_card_data)
        TextView nameText;

        @BindView(R.id.img_wallet_card_check)
        ImageView check;

        @BindColor(R.color.colorPrimaryDark)
        int grey;

        @BindColor(R.color.colorAccent)
        int orange;

        @BindColor(R.color.colorBackground)
        int greyLight;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
