package addcel.mobilecard.ui.usuario.mymobilecard.tebca

import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.domain.wallet.tebca.TebcaFormInteractor
import addcel.mobilecard.domain.wallet.tebca.TebcaFormInteractorImpl
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 2019-08-15.
 */
@Module
class TebcaFormModule(val activity: TebcaFormActivity) {

    @PerActivity
    @Provides
    fun provideModel(): TebcaFormModel {
        return activity.intent?.getParcelableExtra("model")!!
    }

    @PerActivity
    @Provides
    fun provideInteractor(
            wallet: WalletAPI, catalogo: CatalogoAPI
    ): TebcaFormInteractor {
        return TebcaFormInteractorImpl(wallet, catalogo, CompositeDisposable())
    }

    @PerActivity
    @Provides
    fun providePresenter(interactor: TebcaFormInteractor): TebcaFormPresenter {
        return TebcaFormPresenterImpl(interactor, activity)
    }
}

@PerActivity
@Subcomponent(modules = [TebcaFormModule::class])
interface TebcaFormSubcomponent {
    fun inject(activity: TebcaFormActivity)
}