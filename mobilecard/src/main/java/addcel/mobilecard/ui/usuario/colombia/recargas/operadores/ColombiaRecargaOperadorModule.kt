package addcel.mobilecard.ui.usuario.colombia.recargas.operadores

import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.di.scope.PerFragment
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 9/21/19.
 */
@Module
class ColombiaRecargaOperadorModule(val fragment: ColombiaRecargaOperadoresFragment) {
    @PerFragment
    @Provides
    fun provideAdapter(picasso: Picasso): ColombiaRecargaOperadorAdapter {
        return ColombiaRecargaOperadorAdapter(picasso)
    }

    @PerFragment
    @Provides
    fun provideModel(): ColombiaRecargaOperadoresModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun providePresenter(api: CatalogoAPI): ColombiaRecargaOperadorPresenter {
        return ColombiaRecargaOperadorPresenterImpl(api, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [ColombiaRecargaOperadorModule::class])
interface ColombiaRecargaOperadorSubcomponent {
    fun inject(fragment: ColombiaRecargaOperadoresFragment)
}