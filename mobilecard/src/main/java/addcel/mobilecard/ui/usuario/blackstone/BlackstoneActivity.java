package addcel.mobilecard.ui.usuario.blackstone;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import org.jetbrains.annotations.Nullable;

import addcel.mobilecard.R;
import addcel.mobilecard.ui.ContainerActivity;
import addcel.mobilecard.ui.usuario.blackstone.carriers.BSCarrierFragment;

public class BlackstoneActivity extends ContainerActivity {
    private ProgressBar progressBar;
    private Toolbar toolbar;

    public static synchronized Intent get(Context context) {
        return new Intent(context, BlackstoneActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blackstone);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_blackstone, BSCarrierFragment.get("USA"))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
        toolbar = findViewById(R.id.toolbar_blackstone);
    }

    private ProgressBar getProgressBar() {
        if (progressBar == null) progressBar = findViewById(R.id.progress_blackstone);
        return progressBar;
    }

    @Override
    public void showProgress() {
        getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void setAppToolbarTitle(int title) {
        toolbar.setTitle(title);
    }

    @Override
    public void setAppToolbarTitle(@NonNull String title) {
        toolbar.setTitle(title);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public @Nullable View getRetry() {
        return null;
    }
}
