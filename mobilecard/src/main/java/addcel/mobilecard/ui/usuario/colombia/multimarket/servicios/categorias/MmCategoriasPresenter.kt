package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.categorias

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.McConstants
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-12-19.
 */
interface MmCategoriasPresenter {
    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getCategorias()
}

class MmCategoriasPresenterImpl(val multiMarket: MultimarketAPI,
                                val compositeDisposable: CompositeDisposable,
                                val view: MmCategoriasView) : MmCategoriasPresenter {
    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun getCategorias() {
        view.showProgress()
        val cDisp = multiMarket.getCategoriasServicio(BuildConfig.ADDCEL_APP_ID, McConstants.PAIS_ID_CO, StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.codigo == 0) {
                        view.hideRetry()
                        view.setCategorias(it.categorias)
                    } else {
                        view.showRetry()
                        view.showError(it.mensaje)
                    }
                }, {
                    view.hideProgress()
                    view.showRetry()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })

        addToDisposables(cDisp)
    }

}