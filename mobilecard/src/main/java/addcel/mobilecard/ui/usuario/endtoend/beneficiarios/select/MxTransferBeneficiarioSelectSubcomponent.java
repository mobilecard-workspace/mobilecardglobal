package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 08/11/17.
 */
@PerFragment
@Subcomponent(modules = MxTransferBeneficiarioSelectModule.class)
public interface MxTransferBeneficiarioSelectSubcomponent {
    void inject(MxTransferBeneficiarioSelectFragment fragment);
}
