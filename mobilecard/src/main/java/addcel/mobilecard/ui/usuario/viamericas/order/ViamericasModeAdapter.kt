package addcel.mobilecard.ui.usuario.viamericas.order


import addcel.mobilecard.R
import addcel.mobilecard.data.net.viamericas.model.PaymentModeModel
import addcel.mobilecard.ui.custom.adapter.SelectableAdapter
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

/**
 * ADDCEL on 09/05/17.
 */

class ViamericasModeAdapter(private val modes: MutableList<PaymentModeModel> = ArrayList()) :
        SelectableAdapter<ViamericasModeAdapter.ViewHolder>() {

    fun update(modes: List<PaymentModeModel>) {
        this.modes.clear()
        this.modes.addAll(modes)
        notifyDataSetChanged()
    }

    fun getItem(pos: Int): PaymentModeModel {
        return modes[pos]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemLayoutView =
                LayoutInflater.from(parent.context).inflate(R.layout.view_monto, parent, false)
        return ViewHolder(itemLayoutView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mode = modes[position]
        holder.modeText.text = mode.paymentModeName
        if (isSelected(position)) {
            holder.modeText.setBackgroundColor(
                    ContextCompat.getColor(holder.modeText.context, R.color.accent)
            )
            holder.modeText.setTextColor(Color.WHITE)
        } else {
            holder.modeText.background =
                    ContextCompat.getDrawable(holder.modeText.context, R.drawable.sh_rectangle)
            holder.modeText.setTextColor(Color.GRAY)
        }
    }

    override fun getItemCount(): Int {
        return modes.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var modeText: TextView = view.findViewById(R.id.text_monto)

    }
}
