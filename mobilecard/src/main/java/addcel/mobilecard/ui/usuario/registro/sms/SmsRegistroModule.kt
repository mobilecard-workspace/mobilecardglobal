package addcel.mobilecard.ui.usuario.registro.sms

import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.net.registro.RegistroAPI
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.domain.sms.registro.SmsRegistroInteractor
import addcel.mobilecard.domain.sms.registro.SmsRegistroInteractorImpl
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-07-23.
 */

@PerActivity
@Subcomponent(modules = [SmsRegistroModule::class])
interface SmsRegistroSubcomponent {
    fun inject(activity: SmsRegistroActivity)
}

@Module
class SmsRegistroModule(val activity: SmsRegistroActivity) {

    @PerActivity
    @Provides
    fun provideModel(): SmsRegistroModel {
        return activity.intent.extras?.getParcelable("model")!!
    }

    @PerActivity
    @Provides
    fun provideApi(retrofit: Retrofit): RegistroAPI {
        return RegistroAPI.get(retrofit)
    }

    @PerActivity
    @Provides
    fun provideInteractor(
            api: RegistroAPI, state: StateSession,
            model: SmsRegistroModel
    ): SmsRegistroInteractor {
        return SmsRegistroInteractorImpl(api, state, model.useCase, CompositeDisposable())
    }

    @PerActivity
    @Provides
    fun providePresenter(
            interactor: SmsRegistroInteractor,
            model: SmsRegistroModel
    ): SmsRegistroPresenter {
        return SmsRegistroPresenterImpl(interactor, model, activity)
    }
}