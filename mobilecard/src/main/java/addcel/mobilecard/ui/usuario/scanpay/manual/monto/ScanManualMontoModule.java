package addcel.mobilecard.ui.usuario.scanpay.manual.monto;

import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public final class ScanManualMontoModule {
    private final ScanManualMontoFragment fragment;

    public ScanManualMontoModule(ScanManualMontoFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    Bundle provideArguments() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    LcpfEstablecimiento provideEstablecimiento(Bundle bundle) {
        return bundle.getParcelable("establecimiento");
    }

    @PerFragment
    @Provides
    ScanPayActivity provideActivity() {
        return (ScanPayActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    ScanPayService provideService(Retrofit retrofit) {
        return retrofit.create(ScanPayService.class);
    }

    @PerFragment
    @Provides
    ScanManualMontoContract.Presenter providePresenter(ScanPayService service,
                                                       SessionOperations session, LcpfEstablecimiento establecimiento) {
        return new ScanManualMontoPresenter(service, session.getUsuario(), establecimiento, fragment);
    }
}
