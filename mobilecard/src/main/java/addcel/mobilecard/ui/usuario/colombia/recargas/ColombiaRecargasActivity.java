package addcel.mobilecard.ui.usuario.colombia.recargas;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import org.jetbrains.annotations.Nullable;

import addcel.mobilecard.R;
import addcel.mobilecard.ui.ContainerActivity;
import addcel.mobilecard.ui.usuario.colombia.recargas.categorias.ColombiaRecargaCategoriaFragment;

public class ColombiaRecargasActivity extends ContainerActivity {


    private Toolbar toolbar;
    private ProgressBar progressBar;
    private View retry;

    public static synchronized Intent get(Context context) {
        return new Intent(context, ColombiaRecargasActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colombia);
        setAppToolbarTitle(R.string.txt_nav_recargas);

        getFragmentManagerLazy().beginTransaction()
                .add(R.id.frame_colombia, ColombiaRecargaCategoriaFragment.Companion.get())
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
                .commit();

    }

    private ProgressBar getProgressBar() {
        if (progressBar == null) progressBar = findViewById(R.id.progress_colombia);
        return progressBar;
    }

    private Toolbar getToolbar() {
        if (toolbar == null) toolbar = findViewById(R.id.toolbar_colombia);
        return toolbar;
    }

    @Override
    public void showProgress() {
        if (getProgressBar().getVisibility() == View.GONE)
            getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (getProgressBar().getVisibility() == View.VISIBLE)
            getProgressBar().setVisibility(View.GONE);
    }


    @Override
    public void setAppToolbarTitle(int title) {
        getToolbar().setTitle(title);
    }

    @Override
    public void setAppToolbarTitle(@NonNull String title) {
        getToolbar().setTitle(title);
    }

    @Override
    public void showRetry() {
        if (getRetry() != null && getRetry().getVisibility() == View.GONE)
            getRetry().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        if (getRetry() != null && getRetry().getVisibility() == View.VISIBLE)
            getRetry().setVisibility(View.GONE);
    }

    @Override
    public @Nullable View getRetry() {
        if (retry == null)
            retry = findViewById(R.id.retry_colombia);

        return retry;
    }
}
