package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.add;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.AccountResponse;
import addcel.mobilecard.data.net.hth.BankEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import addcel.mobilecard.validation.ValidationUtils;
import addcel.mobilecard.validation.annotation.Celular;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * ADDCEL on 08/11/17.
 */
public class MxTransferBeneficiarioAddFragment extends Fragment
        implements MxTransferBeneficiarioAddContract.View {

    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_transfermx_beneficiario_add_alias)
    TextInputLayout tilAlias;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_transfermx_beneficiario_add_nombre)
    TextInputLayout tilNombre;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_transfermx_beneficiario_add_apellidos)
    TextInputLayout tilApellido;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_transfermx_beneficiario_add_apellidos_materno)
    TextInputLayout tilMaterno;

    @Email(messageResId = R.string.error_email)
    @BindView(R.id.til_transfermx_beneficiario_add_email)
    TextInputLayout tilEmail;

    @Celular(messageResId = R.string.error_celular)
    @BindView(R.id.til_transfermx_beneficiario_add_phone)
    TextInputLayout tilPhone;

    @BindView(R.id.spinner_transfermx_beneficiario_add_banco)
    Spinner bancoSpinner;

    @Length(min = 18, max = 18, messageResId = R.string.error_beneficiario_add_cuentaclabe_empty)
    @BindView(R.id.til_transfermx_beneficiario_add_clabe)
    TextInputLayout tilClabe;

    @BindView(R.id.b_transfermx_beneficiarios_add_continue)
    Button continueButton;

    @Inject
    MxTransferActivity activity;
    @Inject
    boolean fromKeyboard;
    @Inject
    ArrayAdapter<BankEntity> adapter;
    @Inject
    Validator validator;
    @Inject
    MxTransferBeneficiarioAddContract.Presenter presenter;
    private Unbinder unbinder;

    public static MxTransferBeneficiarioAddFragment getForKeyboard(boolean fromKeyboard) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("fromKeyboard", fromKeyboard);
        MxTransferBeneficiarioAddFragment fragment = new MxTransferBeneficiarioAddFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .mxTransferBeneficiarioAddSubcomponent(new MxTransferBeneficiarioAddModule(this))
                .inject(this);
        presenter.getBankCodes();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_mx_transfer_beneficiario_add, container, false);
        unbinder = ButterKnife.bind(this, view);
        bancoSpinner.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);

        if (activity.getRetry() != null)
            activity.getRetry().findViewById(R.id.retry_transfermx).setOnClickListener(v -> clickRetry());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public String getAlias() {
        return Strings.nullToEmpty(Objects.requireNonNull(tilAlias.getEditText()).getText().toString());
    }

    @Override
    public String getNombre() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(tilNombre.getEditText()).getText().toString());
    }

    @Override
    public String getApellidoPaterno() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(tilApellido.getEditText()).getText().toString());
    }

    @Override
    public String getApellidoMaterno() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(tilMaterno.getEditText()).getText().toString());
    }

    @Override
    public String getEmail() {
        return Strings.nullToEmpty(Objects.requireNonNull(tilEmail.getEditText()).getText().toString());
    }

    @Override
    public String getNumTelefono() {
        return Strings.nullToEmpty(Objects.requireNonNull(tilPhone.getEditText()).getText().toString());
    }

    @Override
    public BankEntity getBank() {
        return (BankEntity) bancoSpinner.getSelectedItem();
    }

    @Override
    public int getTipoCuentaSelectedPos() {
        return 1;
    }

    @Override
    public String getCuentaOrClable() {
        return Strings.nullToEmpty(Objects.requireNonNull(tilClabe.getEditText()).getText().toString());
    }

    @Override
    public void setBankCodes(List<BankEntity> bankCodes) {
        adapter.addAll(bankCodes);
    }

    @Override
    public void onBankSelected(int pos) {
    }

    @Override
    public void onTipoCuentaSelected(int pos) {
        if (getView() != null) {
            if (pos == 0) {
                tilClabe.setHint(getText(R.string.txt_transfermx_beneficiario_add_cuenta_cuenta_label));
                tilClabe.setCounterMaxLength(11);
            } else {
                tilClabe.setHint(getText(R.string.txt_transfermx_beneficiario_add_cuenta_clabe_label));
                tilClabe.setCounterMaxLength(18);
            }
        }
    }

    @OnClick(R.id.b_transfermx_beneficiarios_add_continue)
    @Override
    public void clickSave(View view) {
        clearAfterSuccess();
        validator.validate();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public String getErrorFromSelectionPos(int pos) {
        return pos == 0 ? getString(R.string.error_beneficiario_add_cuenta)
                : getString(R.string.error_beneficiario_add_clabe);
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showErrorByTipoCuenta() {
        if (isVisible()) {
            if (getTipoCuentaSelectedPos() == 0) {
                showError(getString(R.string.error_beneficiario_add_cuenta));
            }
            if (getTipoCuentaSelectedPos() == 1) {
                showError(getString(R.string.error_beneficiario_add_clabe));
            }
        }
    }

    @Override
    public void onSave(AccountResponse accountResponse) {
        if (fromKeyboard) {
            activity.finish();
        } else {
            activity.showSuccess(accountResponse.getMensajeError());
            activity.updateBeneficiarios(accountResponse.getAccounts());
        }
    }

    @Override
    public void showRetry() {
        activity.showRetry();
    }

    @Override
    public void hideRetry() {
        activity.hideRetry();
    }

    @Override
    public void clickRetry() {
        presenter.getBankCodes();
    }

    @Override
    public void onValidationSucceeded() {
        if (presenter.validateAccountOrClabe()) {
            clearAfterSuccess();
            presenter.save();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    private boolean validateIfShouldContinueOrSave(Boolean cont) {
        return (cont != null && cont);
    }

    private boolean validateTotal(String total) {
        return !Strings.isNullOrEmpty(total) && Double.valueOf(total) > 0;
    }

    /**
     * Evalua la posici&oacute;n seleccionada en el spinner de tipo cuenta, o la longitud de la cadena
     * capturada en n&uacute;mero de cuenta.
     *
     * @return true en caso de que la posición sea 0 o la cadena sean 18 d&iacute;gitos
     */
    private boolean validateIfValidAccountOrClabe() {
        return getTipoCuentaSelectedPos() == 0 && presenter.isAccount()
                || getTipoCuentaSelectedPos() == 1 && presenter.isCLABE();
    }

    private void clearAfterSuccess() {
        if (isVisible()) {
            ValidationUtils.clearViewErrors(tilAlias, tilNombre, tilApellido, tilMaterno, tilEmail,
                    tilPhone, tilClabe);
        }
    }
}
