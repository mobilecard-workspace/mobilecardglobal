package addcel.mobilecard.ui.usuario.legacy.recargas.tae.select;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;

import androidx.annotation.Nullable;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.model.MobilecardContact;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.transacto.TransactoInteractor;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 16/11/16.
 */
public class RecargaPresenterImpl implements RecargaContract.Presenter {

    private final TransactoInteractor interactor;
    private final PaisResponse.PaisEntity paisDestino;
    private final RecargaContract.View view;

    RecargaPresenterImpl(TransactoInteractor interactor, @Nullable PaisResponse.PaisEntity paisDestino,
                         RecargaContract.View view) {
        this.interactor = interactor;
        this.paisDestino = paisDestino;
        this.view = view;
    }

    @Override
    public void clearCompositeDisposable() {
        interactor.clearDisposables();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getContacts(Context context) {
        view.showProgress();
        interactor.fetchContacts(new InteractorCallback<List<MobilecardContact>>() {
            @Override
            public void onSuccess(@NotNull List<MobilecardContact> result) {
                view.hideProgress();
                view.setContacts(result);
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.setContacts(Lists.newArrayList());
                view.showError(error);
            }
        });
    }

    @Override
    public String getContacto(MobilecardContact contact, String telefono) {
        if (Strings.isNullOrEmpty(telefono)) {
            if (contact != null) {
                return StringUtil.trimPhone(contact.getTelefono());
            } else {
                throw new IllegalArgumentException("El contacto no puede ser null");
            }
        } else {
            return telefono;
        }
    }

    @Override
    public PaisResponse.PaisEntity getPaisDestino() {
        return paisDestino;
    }

    @Override
    public Location buildLocation() {
        Location location = new Location("");
        location.setLatitude(interactor.getLocation().getLat());
        location.setLongitude(interactor.getLocation().getLon());
        return location;
    }

    @Override
    public void setCurrencyCaption(CatalogoResponse.RecargaEntity recargaModel) {
        if (paisDestino != null) {
            if (interactor.getIdPais() == 1 || paisDestino.getId() == 7) {
                view.setCurrency("*MXN");
            } else {
                view.setCurrency("*USD");
            }
        } else {
            view.setCurrency("*MXN");
        }
    }

    @SuppressLint("CheckResult")
    @Override
    public void saveToFav(CatalogoResponse.RecargaEntity recargaModel,
                          List<CatalogoResponse.MontoEntity> montos, int logo, String etiqueta) {
        view.showProgress();
        Favorito favorito = new Favorito();
        favorito.setCategoria(Favorito.CAT_RECARGA);
        favorito.setNombre(recargaModel.getDescripcion());
        favorito.setPais(interactor.getIdPais());
        favorito.setDescripcion(etiqueta);
        favorito.setLogotipo(logo);
        favorito.setMontos(JsonUtil.toJson(montos));
        favorito.setServicio(JsonUtil.toJson(recargaModel));
        interactor.saveToFavs(favorito, new InteractorCallback<Long>() {
            @Override
            public void onSuccess(@NotNull Long result) {
                view.hideProgress();
                if (result > 0) {
                    view.updateFavViewsOnSuccess();
                    view.showSuccess("");
                } else {
                    view.showError("");
                }
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError("");
            }
        });
    }

    @Override
    public void getTipoCambio(CatalogoResponse.RecargaEntity recargaModel) {
        if (paisDestino != null) {
            if (interactor.getIdPais() == 1 || (paisDestino.getId() != 7 && recargaModel.getId() == 2)) {
                view.onTipoCambioReceived(0.0);
            } else {
                view.showProgress();
                interactor.getTipoCambio(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                        new InteractorCallback<Double>() {
                            @Override
                            public void onSuccess(@NotNull Double result) {
                                view.hideProgress();
                                view.onTipoCambioReceived(result);
                            }

                            @Override
                            public void onError(@NotNull String error) {
                                view.hideProgress();
                                view.showError(error);
                            }
                        });
            }
        } else {
            view.onTipoCambioReceived(0.0);
        }
    }
}
