package addcel.mobilecard.ui.usuario.scanpay.secure;

import com.google.common.base.Strings;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.scanpay.secure.ScanPaySecureInteractor;
import addcel.mobilecard.ui.login.tipo.LoginTipoContract;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 09/11/17.
 */
class ScanSecurePresenter implements ScanSecureContract.Presenter {
    private final ScanPaySecureInteractor interactor;
    private final boolean withIdBitacora;
    private final ScanSecureContract.View view;

    ScanSecurePresenter(ScanPaySecureInteractor interactor, boolean withIdBitacora,
                        ScanSecureContract.View view) {
        this.interactor = interactor;
        this.withIdBitacora = withIdBitacora;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposable();
    }

    @Override
    public SPReceiptEntity createResponse(String msg) {
        if (!Strings.isNullOrEmpty(msg)) {
            return JsonUtil.fromJson(msg, SPReceiptEntity.class);
        } else {
            return new SPReceiptEntity(-9999, "Error");
        }
    }

    @Override
    public void pago(SPPagoEntity data) {
        view.showProgress();

        InteractorCallback<String> callback = new InteractorCallback<String>() {
            @Override
            public void onSuccess(@NotNull String result) {
                view.hideProgress();
                view.onPagoStarted(result);
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError(error);
            }
        };

        if (withIdBitacora) {
            interactor.paymentById(BuildConfig.ADDCEL_APP_ID, interactor.getIdPais(),
                    StringUtil.getCurrentLanguage(), data, callback);
        } else {
            interactor.payment(BuildConfig.ADDCEL_APP_ID, interactor.getIdPais(),
                    StringUtil.getCurrentLanguage(), LoginTipoContract.Tipo.USUARIO, data, callback);
        }
    }
}
