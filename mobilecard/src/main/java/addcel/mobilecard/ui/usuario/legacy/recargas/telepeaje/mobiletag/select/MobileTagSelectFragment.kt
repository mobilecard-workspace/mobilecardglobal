package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.select

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.MontoAdapter
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.MobileTagActivity
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.confirm.MobileTagConfirmModel
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_mobiletag_select.*
import javax.inject.Inject

/**
 * ADDCEL on 30/10/19.
 */

@Parcelize
data class MobileTagSelectModel(val idPais: Int, val emailPrellenado: String) : Parcelable

interface MobileTagSelectView : ScreenView, TextWatcher {
    fun launchMontos()

    fun setMontos(montos: List<CatalogoResponse.MontoEntity>)

    fun showRetry()

    fun hideRetry()

    fun clickRetry()

    fun clickContinuar()
}

class MobileTagSelectFragment : Fragment(), MobileTagSelectView {

    companion object {


        private const val PROVEEDOR_MOBILETAG = 800

        fun get(model: MobileTagSelectModel): MobileTagSelectFragment {
            val args = BundleBuilder().putParcelable("model", model).build()
            val frag = MobileTagSelectFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var containerActivity: MobileTagActivity
    @Inject
    lateinit var model: MobileTagSelectModel
    @Inject
    lateinit var presenter: MobileTagSelectPresenter
    @Inject
    lateinit var selectAdapter: MontoAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_mobiletag_select, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.mobileTagSelectSubcomponent(MobileTagSelectModule(this))
                .inject(this)

        launchMontos()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)

        containerActivity.retry?.findViewById<Button>(R.id.b_retry)?.setOnClickListener {
            clickRetry()
        }

        b_recarga_pagar.setOnClickListener { clickContinuar() }

        text_mobiletag_user.addTextChangedListener(this)

        recycler_montos.adapter = selectAdapter
        recycler_montos.layoutManager = GridLayoutManager(view.context, 2)
        recycler_montos.setHasFixedSize(true)
        ItemClickSupport.addTo(recycler_montos).setOnItemClickListener { _, position, _ ->
            selectAdapter.toggleSelection(position)
            b_recarga_pagar.isChecked =
                    AndroidUtils.getText(til_mobiletag_user.editText).isNotEmpty()
        }

        AndroidUtils.setText(til_mobiletag_user.editText, model.emailPrellenado)

    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_montos)
        text_mobiletag_user.removeTextChangedListener(this)
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun launchMontos() {
        //{"idAplicacion":1,"idCategoria":0,"idRecarga":8,"idServicio":8,"idioma":"es","tipoConsulta":0}
        val request = ServiciosRequest.Builder().setIdAplicacion(BuildConfig.ADDCEL_APP_ID)
                .setIdCategoria(0)
                .setIdRecarga(PROVEEDOR_MOBILETAG)
                .setIdServicio(PROVEEDOR_MOBILETAG)
                .setIdioma(StringUtil.getCurrentLanguage())
                .setTipoConsulta(0)
                .build()

        presenter.getMontos(model.idPais, request)
    }

    override fun setMontos(montos: List<CatalogoResponse.MontoEntity>) {
        selectAdapter.update(montos)
        selectAdapter.clearSelection()
    }

    override fun showRetry() {
        containerActivity.showRetry()
    }

    override fun hideRetry() {
        containerActivity.hideRetry()
    }

    override fun clickRetry() {
        launchMontos()
    }

    override fun clickContinuar() {
        if (b_recarga_pagar.isChecked) {
            val user = AndroidUtils.getText(til_mobiletag_user.editText)
            val monto = selectAdapter.getItem(selectAdapter.selectedItems[0])
            val mtcModel = MobileTagConfirmModel(user, monto)
            val args = BundleBuilder().putParcelable("model", mtcModel).build()

            containerActivity.fragmentManagerLazy.commit {
                add(
                        R.id.frame_mobiletag,
                        WalletSelectFragment.get(WalletSelectFragment.MOBILE_TAG, args)
                )
                hide(this@MobileTagSelectFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        } else {
            showError("Ingresa tu destinatario y selecciona el monto a recargar")
        }
    }

    override fun showProgress() {
        containerActivity.showProgress()
    }

    override fun hideProgress() {
        containerActivity.hideProgress()
    }

    override fun showError(msg: String) {
        containerActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerActivity.showSuccess(msg)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (til_mobiletag_user.hasFocus()) {
                if (p0.isNullOrEmpty()) {
                    til_mobiletag_user.error = "Ingresa el nombre del destinatario"
                    b_recarga_pagar.isChecked = false
                } else {
                    til_mobiletag_user.error = null
                    b_recarga_pagar.isChecked = selectAdapter.selectedItemCount > 0
                }
            }
        }
    }

}