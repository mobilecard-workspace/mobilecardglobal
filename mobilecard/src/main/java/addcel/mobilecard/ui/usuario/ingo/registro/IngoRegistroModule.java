package addcel.mobilecard.ui.usuario.ingo.registro;

import android.app.DatePickerDialog;
import android.os.Build;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.ingo.model.IngoEstadoResponse;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.ingo.registro.IngoRegistroInteractor;
import addcel.mobilecard.domain.ingo.registro.IngoRegistroInteractorImpl;
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog;
import addcel.mobilecard.ui.usuario.ingo.IngoActivity;
import dagger.Module;
import dagger.Provides;

@Module
public final class IngoRegistroModule {
    private final IngoRegistroFragment fragment;

    IngoRegistroModule(IngoRegistroFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    IngoActivity provideActivity() {
        return (IngoActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    DateTimeFormatter provideFormatter() {
        return DateTimeFormatter.ofPattern(IngoRegistroContract.DOB_FORMAT);
    }

    @PerFragment
    @Provides
    ArrayAdapter<IngoEstadoResponse.EstadoEntity> provideAdapter(
            IngoActivity activity) {
        ArrayAdapter<IngoEstadoResponse.EstadoEntity> arrayAdapter =
                new ArrayAdapter<>(activity, R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        arrayAdapter.setNotifyOnChange(true);
        return arrayAdapter;
    }

    @PerFragment
    @Provides
    IngoRegistroInteractor provideInteractor(IngoActivity activity,
                                             SessionOperations session) {
        return new IngoRegistroInteractorImpl(activity.getService(), session.getUsuario());
    }

    @PerFragment
    @Provides
    IngoRegistroContract.Presenter providePresenter(
            IngoRegistroInteractor interactor, DateTimeFormatter formatter) {
        return new IngoRegistroPresenter(interactor, formatter, fragment);
    }

    @PerFragment
    @Provides
    DatePickerDialog providesNacimientoDialog(IngoActivity activity) {

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                LocalDateTime time = LocalDateTime.of(i, i1 + 1, i2, 0, 0);
                fragment.setDob(time);
            }
        };

        DatePickerDialog dpDialog;
        if (Build.VERSION.SDK_INT == 24) {
            dpDialog = new FixedHoloDatePickerDialog(activity, listener,
                    Calendar.getInstance().get(Calendar.YEAR) - 18,
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        } else {
            if (Build.VERSION.SDK_INT >= 21) {
                dpDialog = new DatePickerDialog(activity, R.style.CustomDatePickerDialogTheme, listener,
                        Calendar.getInstance().get(Calendar.YEAR) - 18,
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            } else {
                dpDialog =
                        new DatePickerDialog(activity, listener, Calendar.getInstance().get(Calendar.YEAR) - 18,
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                dpDialog.getDatePicker().setCalendarViewShown(false);
            }
        }
        dpDialog.setTitle(R.string.txt_mobilecard_add_dob);
        return dpDialog;
    }
}
