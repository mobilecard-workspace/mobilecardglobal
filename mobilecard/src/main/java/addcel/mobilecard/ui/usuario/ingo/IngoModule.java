package addcel.mobilecard.ui.usuario.ingo;

import com.google.gson.Gson;
import com.ingomoney.ingosdk.android.manager.GoogleAnalyticsHelper;
import com.ingomoney.ingosdk.android.manager.IovationHelper;
import com.ingomoney.ingosdk.android.manager.JsonDeserializer;
import com.ingomoney.ingosdk.android.manager.JsonSerializer;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.ingo.IngoAliveAPI;
import addcel.mobilecard.data.net.ingo.IngoService;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.ingo.GoogleAnalyticsHelperImpl;
import addcel.mobilecard.domain.ingo.IngoInteractor;
import addcel.mobilecard.domain.ingo.IngoInteractorImpl;
import addcel.mobilecard.domain.ingo.IovationHelperImpl;
import addcel.mobilecard.domain.ingo.JsonDeserializerImpl;
import addcel.mobilecard.domain.ingo.JsonSerializerImpl;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 19/07/18.
 */
@Module
public final class IngoModule {
    private final IngoActivity activity;

    IngoModule(IngoActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    IngoService provideService(Retrofit retrofit) {
        return retrofit.create(IngoService.class);
    }

    @Provides
    @PerActivity
    GoogleAnalyticsHelper provideHelper() {
        return new GoogleAnalyticsHelperImpl();
    }

    @Provides
    @PerActivity
    IovationHelper provideIovationHelper() {
        return new IovationHelperImpl();
    }

    @Provides
    @PerActivity
    JsonDeserializer provideJsonDeserializer(Gson gson) {
        return new JsonDeserializerImpl(gson);
    }

    @Provides
    @PerActivity
    JsonSerializer provideJsonSerializer(Gson gson) {
        return new JsonSerializerImpl(gson);
    }


    @PerActivity
    @Provides
    @Named("IngoAliveRetrofit")
    Retrofit provideIngoRetrofit(Retrofit retrofit) {
        return retrofit.newBuilder().baseUrl("https://api.spykemobile.net/").build();
    }

    @PerActivity
    @Provides
    IngoAliveAPI provideIngoAliveApi(
            @Named("IngoAliveRetrofit") Retrofit retrofit) {
        return IngoAliveAPI.Companion.get(retrofit);
    }

    @Provides
    @PerActivity
    IngoInteractor provideInteractor(IngoService ingoService, IngoAliveAPI ingoAliveAPI,
                                     SessionOperations session) {
        return new IngoInteractorImpl(ingoService, ingoAliveAPI, session.getUsuario().getIdeUsuario());
    }

    @PerActivity
    @Provides
    IngoContract.Presenter providePresenter(IngoInteractor interactor) {
        return new IngoPresenter(interactor, activity);
    }
}
