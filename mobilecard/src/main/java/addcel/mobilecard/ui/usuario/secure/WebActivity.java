package addcel.mobilecard.ui.usuario.secure;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.common.base.Charsets;
import com.squareup.otto.Bus;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.HTHService;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;

public final class WebActivity extends AppCompatActivity implements WebContract.View {

    @Inject
    @Named("titulo")
    String titulo;
    @Inject
    @Named("startUrl")
    String startUrl;
    @Inject
    @Named("formEndpoint")
    String formEndpoint;
    @Inject
    @Named("finishEndpoint")
    String finishEndpoint;
    @Inject
    @Named("errorEndpoint")
    String errorEndpoint;
    @Inject
    @Named("currentUrl")
    String currentUrl;
    @Inject
    @Named("token")
    String token;
    @Inject
    byte[] data;
    @Inject
    AlertDialog warningDialog;
    @Inject
    WebView browser;
    @Inject
    Bus bus;
    @Inject
    WebContract.Presenter presenter;

    private FrameLayout container;
    private ProgressBar progressBar;

    /**
     * @param context - Context de donde se lanza la Activity
     * @param titulo  - Titulo de la ventana
     * @param start   - Url que se lanza al iniciar la vista
     * @param form    - Url donde se cargarán los formularios
     * @param finish  - Url donde termina el flujo de pago en caso de exito
     * @param error   - Url donde termina el flujo de pago en caso de error
     * @param data    - Parametros formateados como byte[] que se cargara en el navegador
     * @param card    - Tarjeta con la que se hace el pago
     *                reporteo de errores
     */
    public static synchronized Intent get(Context context, String titulo, String start, String form,
                                          String finish, String error, @Nullable CardEntity card, byte[] data) {
        return get(context, titulo, start, form, finish, error, card, data, "");
    }

    public static synchronized Intent get(Context context, String titulo, String start, String form,
                                          String finish, String error, @Nullable CardEntity card, byte[] data, String token) {
        Bundle bundle = new Bundle();
        bundle.putString("titulo", titulo);
        bundle.putString("startUrl", start);
        bundle.putString("formEndpoint", form);
        bundle.putString("finishEndpoint", finish);
        bundle.putString("errorEndpoint", error);
        bundle.putParcelable("card", card);
        bundle.putByteArray("data", data);
        bundle.putString("token", token);
        return new Intent(context, WebActivity.class).putExtras(bundle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secure);
        container = findViewById(R.id.web_secure_container);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        final WebModule module = new WebModule(this);
        Mobilecard.get().getNetComponent().webSubcomponent(module).inject(this);
        container.addView(browser);
        presenter.processUrl(startUrl, data, token);
    }

    @Override
    public void showError(int resId, boolean destroy) {
        showError(getString(resId), destroy);
    }

    @Override
    public void showError(String charSequence, boolean destroy) {
        int toastLen = destroy ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
        Toasty.error(this, charSequence, toastLen).show();
        if (destroy) finish();
    }

    @Override
    public void showSuccess(String charSequence) {
        Toasty.success(this, charSequence, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startProgress(int StringId) {
        if (getProgressBar().getVisibility() == View.GONE)
            getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void stopProgress() {
        if (getProgressBar().getVisibility() == View.VISIBLE)
            getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void startActivity(Class<?> cl, Bundle bundle) {
        Intent intent = new Intent(this, cl);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void startActivity(Class<?> cl) {
        Intent intent = new Intent(this, cl);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void setCurrentUrl(String url) {
        this.currentUrl = url;
    }

    @Override
    public void evalJS(String fun) {
        browser.evaluateJavascript(fun, null);
    }

    @JavascriptInterface
    @Override
    public void logHtml(String dom) {
        if (BuildConfig.DEBUG) {
            Timber.d(dom);
        }
    }

    @Override
    public void setCardData() {
        final String fun = presenter.buildLoadDataFun();
        evalJS(fun);
    }

    @Override
    public void goToMainMenu() {
        startActivity(MainActivity.get(this, false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }

    @Override
    public void goToPreviousPage() {
        super.onBackPressed();
    }

    @Override
    public void showWarningExitDialog() {
        if (!warningDialog.isShowing()) warningDialog.show();
    }

    @Override
    public void onUrlProcessed(String url, String processedUrl) {
        loadUrlAfterProcess(processedUrl, Charsets.UTF_8.displayName());
    }

    private void loadUrlAfterProcess(String html, String charset) {
        browser.post(() -> {
            browser.clearCache(true);
            browser.loadDataWithBaseURL(BuildConfig.BASE_URL, html, "text/html", charset, null);
        });
    }

    private int getSslErrorMsg(int errorCode) {
        switch (errorCode) {
            case 0:
                return R.string.error_ssl_notyetvalid;
            case 1:
                return R.string.error_ssl_expired;
            case 2:
                return R.string.error_hostname_mismatch;
            case 3:
                return R.string.error_ssl_untrusted;
            case 4:
                return R.string.error_ssl_date_invalid;
            default:
                return R.string.error_ssl_invalid;
        }
    }

    @Override
    public void showDebugHandlerDialog(SslErrorHandler handler, SslError error) {
        new AlertDialog.Builder(this).setTitle("Error " + error.getPrimaryError())
                .setMessage(
                        getResources().getString(getSslErrorMsg(error.getPrimaryError()), error.getUrl()))
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> handler.cancel())
                .setPositiveButton(R.string.txt_secure_deseascontinuar,
                        (dialog, which) -> handler.proceed())
                .show();
    }

    @Override
    public void onBackPressed() {
        if (currentUrl.equals(startUrl) || currentUrl.contains(formEndpoint)) {
            goToPreviousPage();
        } else if (currentUrl.contains(finishEndpoint)
                || currentUrl.contains(errorEndpoint)
                || currentUrl.contains(HTHService.PROCESS_SECURED)) {
            goToMainMenu();
        } else {
            showWarningExitDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (browser != null) {
            browser.onResume();
            browser.resumeTimers();
        }
    }

    @Override
    protected void onPause() {
        if (browser != null) {
            browser.stopLoading();
            browser.onPause();
            browser.pauseTimers();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        presenter.clearDisposables();
        container.removeAllViews();
        browser.destroy();
        super.onDestroy();
    }

    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        Timber.d("onSaveInstanceState");
        try {
            super.onSaveInstanceState(outState, outPersistentState);
            browser.saveState(outState);
        } catch (Throwable t) {
            Timber.e(t);
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        Timber.d("onRestoreInstanceState");
        try {
            browser.restoreState(savedInstanceState);
        } catch (Throwable t) {
            Timber.e(t);
        }
    }

    private ProgressBar getProgressBar() {
        if (progressBar == null) {
            progressBar = findViewById(R.id.progress_secure);
        }
        return progressBar;
    }
}
