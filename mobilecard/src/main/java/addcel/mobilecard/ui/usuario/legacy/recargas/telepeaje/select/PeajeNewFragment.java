package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.phrase.Phrase;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.telepeaje.model.Tag;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.custom.view.CheckableButton;
import addcel.mobilecard.ui.custom.view.CheckableImageButton;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.MontoAdapter;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.utils.MapUtils;
import addcel.mobilecard.utils.StringUtil;
import addcel.mobilecard.validation.ValidationUtils;
import addcel.mobilecard.validation.annotation.PeajeTag;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import dagger.Lazy;

/**
 * ADDCEL on 14/12/16.
 */
public final class PeajeNewFragment extends Fragment implements PeajeContract.View {

    @BindString(R.string.txt_peaje_tag_numero_add)
    String addTagDialogMessage;

    @BindView(R.id.view_recarga_header)
    ImageView header;

    @BindView(R.id.spinner_peaje_eligenumero)
    Spinner tagSpinner;

    @BindView(R.id.view_peaje_tag_saldo)
    TextView saldoView;

    @BindView(R.id.switch_peaje_otronumero)
    Switch otroNumeroSwitch;

    @PeajeTag(messageResId = R.string.error_tag_valido)
    @BindView(R.id.text_peaje_tag)
    EditText
            tagText;

    @NotEmpty(messageResId = R.string.error_tag_dv)
    @BindView(R.id.text_peaje_dv)
    EditText pinText;

    @BindView(R.id.recycler_montos)
    RecyclerView montosRecycler;

    @BindView((R.id.b_recarga_pagar))
    CheckableButton pagarButton;

    @BindView(R.id.b_peaje_fav)
    CheckableImageButton favButton;

    @BindView(R.id.til_peaje_favorito)
    TextInputLayout favTil;

    @BindView(R.id.view_peaje_favorito)
    View favView;

    @Inject
    PagoContainerActivity activity;
    @Inject
    Validator validator;
    @Inject
    PeajeNewAdapter tagAdapter;
    @Inject
    MontoAdapter peajeMontoAdapter;
    @Inject
    DetachableClickListener detachableClickListener;
    @Inject
    Lazy<AlertDialog> aliasDialog;
    @Inject
    PeajeContract.Presenter presenter;

    @Inject
    CatalogoResponse.RecargaEntity servicio;
    @Inject
    List<CatalogoResponse.MontoEntity> montos;
    @Inject
    boolean fav;

    @Inject
    @Named("colorLogoMap")
    Map<Integer, Integer> headerMap;

    @Inject
    @Named("recargaMap")
    Map<Integer, Integer> whiteMap;

    private Unbinder unbinder;

    public PeajeNewFragment() {
        // Required empty public constructor
    }

    public static PeajeNewFragment newInstance(CatalogoResponse.RecargaEntity servicio,
                                               List<CatalogoResponse.MontoEntity> montos) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", servicio);
        bundle.putParcelableArrayList("montos", (ArrayList<? extends Parcelable>) montos);
        PeajeNewFragment fragment = new PeajeNewFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static PeajeNewFragment newInstance(CatalogoResponse.RecargaEntity servicio,
                                               List<CatalogoResponse.MontoEntity> montos, boolean fav) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", servicio);
        bundle.putParcelableArrayList("montos", (ArrayList<? extends Parcelable>) montos);
        bundle.putBoolean("fav", fav);
        PeajeNewFragment fragment = new PeajeNewFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().peajeNewSubcomponent(new PeajeNewModule(this)).inject(this);
        if (tagAdapter.getCount() == 0) presenter.getTags(servicio);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_peaje, container, false);
        unbinder = ButterKnife.bind(this, v);
        aliasDialog.get().setOnShowListener(this);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        detachableClickListener.clearOnDetach(aliasDialog.get());
        activity.setStreenStatus(PagoContainerActivity.STATUS_NORMAL);
        activity.setAppToolbarTitle("TELEPEAJE");
        header.setImageResource(MapUtils.getOrDefault(headerMap, servicio.getId(), R.drawable.logo_pase));
        tagSpinner.setAdapter(tagAdapter);
        RecyclerView.LayoutManager manager = new GridLayoutManager(getContext(), 2);
        montosRecycler.setLayoutManager(manager);
        montosRecycler.setAdapter(peajeMontoAdapter);
        montosRecycler.setHasFixedSize(true);

        ItemClickSupport.addTo(montosRecycler).setOnItemClickListener((recyclerView, position, v) -> {
            peajeMontoAdapter.toggleSelection(position);
            if (!isOtroNumeroChecked()) {
                pagarButton.setChecked(true);
            } else {
                if (validateTagCaptured(AndroidUtils.getText(tagText), AndroidUtils.getText(pinText))) {
                    pagarButton.setChecked(true);
                }
            }
        });
        setInitialFav(fav);
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(montosRecycler);
        montosRecycler.setAdapter(null);
        aliasDialog.get().setOnShowListener(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearCompositeDisposables();
        super.onDestroy();
    }

    @Override
    public Tag getPeajeTag() {
        return (Tag) tagSpinner.getSelectedItem();
    }

    @Override
    public String getNumeroTag() {
        return Strings.nullToEmpty(tagText.getText().toString());
    }

    @Override
    public String getPin() {
        return Strings.nullToEmpty(pinText.getText().toString());
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(activity);
    }

    @Override
    public boolean isOtroNumeroChecked() {
        return otroNumeroSwitch.isChecked();
    }

    @OnCheckedChanged(R.id.switch_peaje_otronumero)
    @Override
    public void checkOtroNumeroSwitch(boolean checked) {
        final int visibility = checked ? View.VISIBLE : View.GONE;
        saldoView.setVisibility(checked ? View.GONE : View.VISIBLE);
        tagSpinner.setEnabled(!checked);
        tagText.setVisibility(visibility);
        tagText.setText("");
        tagText.setEnabled(checked);
        pinText.setVisibility(visibility);
        pinText.setEnabled(checked);
        pinText.setText("");
        if (getView() == null) throw new NullPointerException(getString(R.string.error_default));
        getView().findViewById(R.id.b_peaje_add_tag).setVisibility(visibility);
        if (checked) {
            pagarButton.setChecked(
                    validateTagCaptured(AndroidUtils.getText(tagText), AndroidUtils.getText(pinText)));
        }
    }

    @OnTextChanged(value = R.id.text_peaje_tag, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void onTagCaptured(CharSequence sequence) {
        if (isOtroNumeroChecked()) {
            if (validateTagCaptured(StringUtil.fromSequence(sequence), AndroidUtils.getText(pinText)) && (
                    peajeMontoAdapter.getSelectedItemCount()
                            > 0)) {
                pagarButton.setChecked(true);
            } else {
                pagarButton.setChecked(false);
            }
        }
    }

    @OnTextChanged(value = R.id.text_peaje_dv, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void onDvCaptured(CharSequence sequence) {
        if (isOtroNumeroChecked()) {
            if (validateTagCaptured(AndroidUtils.getText(tagText), StringUtil.fromSequence(sequence)) && (
                    peajeMontoAdapter.getSelectedItemCount()
                            > 0)) {
                pagarButton.setChecked(true);
            } else {
                pagarButton.setChecked(false);
            }
        }
    }

    @Override
    public boolean validateTagCaptured(String tag, String dv) {
        return (TextUtils.getTrimmedLength(tag) >= 8) && !Strings.isNullOrEmpty(dv);
    }

    @Override
    public void notifyNoTags() {
        if (getView() != null && isVisible()) {
            otroNumeroSwitch.setChecked(true);
            otroNumeroSwitch.setEnabled(false);
            checkOtroNumeroSwitch(true);
        }
    }

    @Override
    public void setTags(List<Tag> tags) {
        if (isVisible()) {
            clearAliasDialog();
            tagAdapter.update(tags);
            if (tagAdapter.getCount() > 0) onTagSelected(0);
            otroNumeroSwitch.setChecked(tagAdapter.getCount() == 0);
            otroNumeroSwitch.setEnabled(tagAdapter.getCount() != 0);
            checkOtroNumeroSwitch(tagAdapter.getCount() == 0);
        }
    }

    @OnItemSelected(value = R.id.spinner_peaje_eligenumero, callback = OnItemSelected.Callback.ITEM_SELECTED)
    @Override
    public void onTagSelected(int pos) {
        Tag tag = (Tag) tagAdapter.getItem(pos);
        presenter.checkAndShowSaldo(tag);
    }

    @OnItemSelected(value = R.id.spinner_peaje_eligenumero, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    @Override
    public void onTagNotSelected() {
        if (isVisible()) {
            saldoView.setText(
                    presenter.buildSaldoText(Phrase.from(activity, R.string.saldo_tag_saldo), 0));
            saldoView.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.b_peaje_fav)
    @Override
    public void clickFav() {
        setFav(!favButton.isChecked());
    }

    @OnClick(R.id.b_peaje_fav_add)
    @Override
    public void clickSaveFav() {
        if (!Strings.isNullOrEmpty(Objects.requireNonNull(favTil.getEditText()).getText().toString())) {
            Integer logo = MapUtils.getOrDefault(whiteMap, servicio.getId(), R.drawable.pase);
            presenter.saveToFav(servicio, montos, logo, favTil.getEditText().getText().toString().trim());
        } else {
            favTil.setError(getText(R.string.error_default));
        }
    }

    @Override
    public void setFav(boolean fav) {
        favButton.setChecked(fav);
        new Handler().postDelayed(() -> favView.setVisibility(fav ? View.VISIBLE : View.GONE), 250);
    }

    @Override
    public void setInitialFav(boolean fav) {
        favButton.setChecked(fav);
        favButton.setEnabled(!fav);
        if (!fav) favButton.requestFocus();
    }

    @Override
    public void updateFavViewsOnSuccess() {
        favButton.setEnabled(false);
        favView.setVisibility(View.GONE);
    }

    @OnClick(R.id.b_peaje_add_tag)
    @Override
    public void clickAdd() {
        String tag = AndroidUtils.getText(tagText);
        String dv = AndroidUtils.getText(pinText);
        if (presenter.validateTagInput(tag, dv)) {
            showAliasDialog(tag, dv);
        } else {
            showError(getString(R.string.error_tag_valido));
        }
    }

    @OnClick(R.id.b_peaje_tag_delete)
    @Override
    public void clickDelete() {
        if (tagSpinner.isEnabled() && tagSpinner.getAdapter().getCount() > 0) {
            presenter.removeTag(servicio, (Tag) tagSpinner.getSelectedItem());
        }
    }

    @Override
    public void showAliasDialog(String tag, String dv) {
        final String dialogMsg =
                presenter.buildAliasText(Phrase.from(activity, R.string.txt_peaje_tag_numero_add), tag, dv);
        aliasDialog.get().setMessage(dialogMsg);
        aliasDialog.get().show();
    }

    @Override
    public void clearAliasDialog() {
        if (aliasDialog.get().isShowing()) aliasDialog.get().dismiss();
    }

    @OnClick(R.id.b_recarga_pagar)
    @Override
    public void clickRecarga() {
        validator.validate();
    }

    @Override
    public void showSuccess(int resId) {
        showSuccess(getString(resId));
    }

    @Override
    public void onSaldoTagChecked(double saldoTag) {
        if (getView() != null) {

            boolean shouldGetTagFromModel = tagAdapter.getCount() > 0 && !isOtroNumeroChecked();

            final String[] aTag = shouldGetTagFromModel ? presenter.getTagFromModel(getPeajeTag())
                    : presenter.getTagFromInput(getNumeroTag(), getPin());

            Bundle bundle = new Bundle();
            bundle.putDouble("saldoTag", saldoTag);
            bundle.putParcelable("monto",
                    peajeMontoAdapter.getItem(peajeMontoAdapter.getSelectedItems().get(0)));
            bundle.putString("tag", aTag[0]);
            bundle.putString("pin", aTag[1]);
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.activity_pago_container,
                            WalletSelectFragment.get(WalletSelectFragment.PEAJE, bundle))
                    .hide(this)
                    .addToBackStack(null)
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .commit();
        }
    }

    @Override
    public void hideSaldo() {
        if (isVisible()) saldoView.setVisibility(View.GONE);
    }

    @Override
    public void showSaldo(double saldo) {
        if (isVisible()) {
            saldoView.setVisibility(View.VISIBLE);
            AndroidUtils.setText(saldoView,
                    presenter.buildSaldoText(Phrase.from(activity, R.string.saldo_tag_saldo), saldo));
        }
    }

    @Override
    public void onValidationSucceeded() {
        if (isVisible()) {
            if (!pagarButton.isChecked()) {
                showError(getString(R.string.error_monto_select));
            } else {
                boolean shouldGetTagFromModel = tagAdapter.getCount() > 0 && !isOtroNumeroChecked();

                final String[] aTag = shouldGetTagFromModel ? presenter.getTagFromModel(getPeajeTag())
                        : presenter.getTagFromInput(getNumeroTag(), getPin());

                final String etiqueta =
                        tagAdapter.getCount() == 0 ? "" : ((Tag) tagSpinner.getSelectedItem()).getEtiqueta();
                CatalogoResponse.MontoEntity montoModel =
                        peajeMontoAdapter.getItem(peajeMontoAdapter.getSelectedItems().get(0));
                presenter.checkSaldoTag(Strings.nullToEmpty(aTag[1]), etiqueta,
                        Strings.nullToEmpty(aTag[0]), montoModel);
            }
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationUtils.onValidationFailed(activity, errors);
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull String charSequence) {
        activity.showSuccess(charSequence);
    }

    @Override
    public void onShow(DialogInterface dialog) {
        final String tag = AndroidUtils.getText(tagText);
        final String dv = AndroidUtils.getText(pinText);
        final EditText etiqueta = ((AlertDialog) dialog).findViewById(R.id.text_add_tag_etiqueta);
        etiqueta.setError(null);
        ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v -> {
            String sEtiqueta = AndroidUtils.getText(etiqueta);
            if (!Strings.isNullOrEmpty(sEtiqueta)) {
                presenter.addTag(servicio, tag, dv, sEtiqueta);
            } else {
                etiqueta.setError(getString(R.string.error_tag_alias_add));
            }
        });
    }
}
