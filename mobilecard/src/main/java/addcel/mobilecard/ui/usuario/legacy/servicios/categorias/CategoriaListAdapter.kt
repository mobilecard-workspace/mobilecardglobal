package addcel.mobilecard.ui.usuario.legacy.servicios.categorias

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.utils.MapUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

/**
 * ADDCEL on 2019-06-15.
 */
class CategoriaListAdapter(val data: List<CatalogoResponse.CategoriaEntity>) :
        RecyclerView.Adapter<CategoriaListAdapter.ViewHolder>() {

    /*
    [{"id":1,"descripcion":"LUZ"},
    {"id":2,"descripcion":"GOBIERNOS"},
    {"id":5,"descripcion":"TELEFONIA FIJA"},
    {"id":6,"descripcion":"TELEVISION DE PAGA"},
    {"id":7,"descripcion":"GAS NATURAL"},
    {"id":8,"descripcion":"SISTEMAS DE AGUAS"},
    {"id":10,"descripcion":"TIENDAS ON-LINE"},
    {"id":12,"descripcion":"TELEFONIA MOVIL"},
    {"id":13,"descripcion":"TRANSPORTE"},
    {"id":14,"descripcion":"VIVIENDA"},
    {"id":15,"descripcion":"SEGUROS"}]
     */
    companion object {
        fun getIcons(): Map<Int, Int> {
            return mapOf(
                    1 to R.drawable.b_cat_luz,
                    2 to R.drawable.b_cat_gobierno,
                    5 to R.drawable.b_cat_telefono,
                    6 to R.drawable.b_cat_television,
                    7 to R.drawable.b_cat_gas,
                    8 to R.drawable.b_cat_agua,
                    10 to R.drawable.b_cat_tiendas,
                    12 to R.drawable.b_cat_tae,
                    13 to R.drawable.b_cat_transporte,
                    14 to R.drawable.b_cat_vivienda,
                    15 to R.drawable.b_cat_seguros
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_categoria, parent, false)
        )
    }

    fun getItem(position: Int): CatalogoResponse.CategoriaEntity {
        return data[position]
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entity = data[position]
        holder.icon.setImageResource(
                MapUtils.getOrDefault(
                        getIcons(),
                        entity.id,
                        R.drawable.logo_cfe
                )
        )
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val icon: ImageView = view.findViewById(R.id.icon_cat)
    }
}