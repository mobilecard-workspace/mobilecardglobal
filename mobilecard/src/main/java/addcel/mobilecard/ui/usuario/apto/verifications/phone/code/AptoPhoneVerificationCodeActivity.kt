package addcel.mobilecard.ui.usuario.apto.verifications.phone.code

import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.verification.Secret
import addcel.mobilecard.data.net.apto.verification.VerificationAPI
import addcel.mobilecard.data.net.apto.verification.VerificationResponse
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.apto.verifications.email.capture.AptoEmailVerificationCaptureActivity
import addcel.mobilecard.ui.usuario.apto.verifications.email.capture.AptoEmailVerificationCaptureModel
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.Retrofit2HttpErrorHandling
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import com.dpizarro.pinview.library.PinView
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_phone_verification_code.*

@Parcelize
data class AptoPhoneVerificationCodeModel(
        val phone: String,
        val verification: VerificationResponse
) : Parcelable

interface AptoPhoneVerificationCodeView : ScreenView, PinView.OnCompleteListener {
    fun onVerification(result: VerificationResponse)
}

class AptoPhoneVerificationCodeActivity : AppCompatActivity(), AptoPhoneVerificationCodeView {

    val api = VerificationAPI.get()
    val compositeDisposable = CompositeDisposable()
    lateinit var model: AptoPhoneVerificationCodeModel

    companion object {
        fun get(context: Context, model: AptoPhoneVerificationCodeModel): Intent {
            return Intent(context, AptoPhoneVerificationCodeActivity::class.java).putExtra(
                    "model",
                    model
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_phone_verification_code)
        model = intent?.getParcelableExtra("model")!!
        pin_shift_phone_verification_code.setOnCompleteListener(this)
        view_shift_phone_verification_code_title.text =
                getString(R.string.txt_shift_code_send_title, model.phone)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun onComplete(completed: Boolean, pinResults: String?) {
        if (completed) {
            compositeDisposable.add(
                    api.finish(model.verification.verificationId, Secret(pinResults!!))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    { r -> onVerification(r) },
                                    { t ->
                                        val errorRes = Retrofit2HttpErrorHandling.getBodyFromErrorBody(
                                                t,
                                                VerificationResponse::class.java
                                        )
                                        if (errorRes != null)
                                            onVerification(errorRes)
                                        else
                                            showError(
                                                    t.localizedMessage
                                                            ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)
                                            )
                                    })
            )
        }
    }

    override fun onVerification(result: VerificationResponse) {
        if (result.status == VerificationAPI.STATUS_PASSED) {
            showSuccess(result.status)
            val param = AptoEmailVerificationCaptureModel(model.phone, result)
            startActivity(AptoEmailVerificationCaptureActivity.get(this, param))
        } else {
            showError("${result.type} ${result.status}")
        }
    }
}
