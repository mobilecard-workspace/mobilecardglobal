package addcel.mobilecard.ui.usuario.blackstone.products;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/07/18.
 */
@PerFragment
@Subcomponent(modules = BSProductModule.class)
public interface BSProductSubcomponent {
    void inject(BSProductFragment fragment);
}
