package addcel.mobilecard.ui.usuario.main.contacto;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.domain.usuario.bottom.UseCase;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.ui.usuario.main.MainContract;
import addcel.mobilecard.utils.BundleBuilder;

/**
 * ADDCEL on 24/08/16.
 */
public final class ContactoFragment extends Fragment implements ContactoContract.View {
    @Inject
    MainActivity activity;
    @Inject
    ContactoAdapter adapter;
    @Inject
    @Named("idPais")
    int idPais;
    @Inject
    ContactoContract.Presenter presenter;

    private RecyclerView recyclerContacto;

    public ContactoFragment() {
    }

    public static ContactoFragment get(int idPais) {
        ContactoFragment fragment = new ContactoFragment();
        fragment.setArguments(new BundleBuilder().putInt("idPais", idPais).build());
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().contactoSubcomponent(new ContactoModule(this)).inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_contacto, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUI(view);
        activity.setSelection(MainContract.UseCase.CONTACTO);
        activity.setBottomMenuUseCase(UseCase.CONTACTO);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            activity.setSelection(MainContract.UseCase.CONTACTO);
            activity.setBottomMenuUseCase(UseCase.CONTACTO);
        }
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(recyclerContacto);
        super.onDestroyView();
    }

    @Override
    public void setUI(@NonNull View v) {
        recyclerContacto = v.findViewById(R.id.recycler_contacto);
        configRecyclerView(recyclerContacto);
        setClicks(v);
    }

    @Override
    public void configRecyclerView(@NonNull RecyclerView view) {
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(view)
                .setOnItemClickListener((recyclerView, position, v) -> clickLanzaAccion(position));
    }

    @Override
    public void setClicks(@NonNull View v) {
        v.findViewById(R.id.b_contacto_fb).setOnClickListener(v1 -> clickFb());
        v.findViewById(R.id.b_contacto_tw).setOnClickListener(v1 -> clickTw());
    }

    @Override
    public void clickFb() {
        if (isVisible()) lanzaFacebook();
    }

    @Override
    public void clickTw() {
        if (isVisible()) lanzaTwitter();
    }

    @Override
    public void clickLanzaAccion(int pos) {
        if (isVisible()) presenter.lanzaAccion(pos);
    }

    @Override
    public void llamanos() {
        String tel = (idPais == 4) ? "080080102" : ContactoContract.Constants.TEL_CONTACTO;
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + tel));
        launchIntent(intent);
    }

    @Override
    public void escribenos() {
        final String[] recMex = {ContactoContract.Constants.EMAIL_CONTACTO};
        final String[] recPe = {"contacto@mobilecardpe.com"};
        final Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(android.content.Intent.EXTRA_EMAIL, idPais == 4 ? recPe : recMex);
        i.putExtra(android.content.Intent.EXTRA_SUBJECT, presenter.buildEmailSubject());
        i.putExtra(android.content.Intent.EXTRA_TEXT,
                getString(R.string.txt_contacto_escribenos_email_footer));
        launchIntent(i);
    }

    @Override
    public void llamame() {
    }

    @Override
    public void lanzaMobilecard() {
        Intent browserIntent =
                new Intent(Intent.ACTION_VIEW, Uri.parse(ContactoContract.Constants.WEB));
        launchIntent(browserIntent);
    }

    @Override
    public void lanzaFacebook() {
        Intent browserIntent =
                new Intent(Intent.ACTION_VIEW, Uri.parse(ContactoContract.Constants.FACEBOOK));
        launchIntent(browserIntent);
    }

    @Override
    public void lanzaTwitter() {
        Intent browserIntent =
                new Intent(Intent.ACTION_VIEW, Uri.parse(ContactoContract.Constants.TWITTER));
        launchIntent(browserIntent);
    }

    @Override
    public void lanzaGPlus() {
    }

    @Override
    public void lanzaYoutube() {
    }

    @Override
    public void lanzaLinkedIn() {
    }

    @Override
    public void launchIntent(Intent intent) {
        if (isVisible()) startActivity(intent);
    }
}
