package addcel.mobilecard.ui.usuario.scanpay.qr;

import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public final class ScanQrModule {
    private final ScanQrFragment fragment;

    ScanQrModule(ScanQrFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ScanPayActivity provideActivity() {
        return (ScanPayActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    ScanPayService provideService(Retrofit retrofit) {
        return retrofit.create(ScanPayService.class);
    }

    @PerFragment
    @Provides
    ScanQrContract.Presenter providePresenter(ScanPayService service,
                                              SessionOperations session) {
        return new ScanQrPresenter(service, session.getUsuario(), fragment);
    }
}
