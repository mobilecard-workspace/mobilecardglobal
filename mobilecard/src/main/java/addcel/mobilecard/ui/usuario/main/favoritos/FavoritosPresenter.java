package addcel.mobilecard.ui.usuario.main.favoritos;

import android.annotation.SuppressLint;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.JsonUtil;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * ADDCEL on 26/10/17.
 */
class FavoritosPresenter implements FavoritosContract.Presenter {
    private static final Type montoListType =
            new TypeToken<ArrayList<CatalogoResponse.MontoEntity>>() {
            }.getType();
    private final FavoritoDaoRx dao;
    private final SessionOperations session;
    private final FavoritosContract.View view;
    private final CompositeDisposable compositeDisposable;

    FavoritosPresenter(FavoritoDaoRx dao, SessionOperations session, FavoritosContract.View view) {
        this.dao = dao;
        this.session = session;
        this.compositeDisposable = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearCompositeDisposable() {
        compositeDisposable.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void getAll() {
        view.showProgress();
        compositeDisposable.add(dao.getAll(session.getUsuario().getIdPais())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Favorito>>() {
                    @Override
                    public void accept(List<Favorito> favoritos) throws Exception {
                        view.hideProgress();
                        view.populate(favoritos);
                    }
                }));
    }

    @SuppressLint("CheckResult")
    @Override
    public void removeFavorite(int pos, Favorito favorito) {
        view.showProgress();
        compositeDisposable.add(Single.fromCallable(() -> dao.delete(favorito))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aInt -> {
                    view.hideProgress();
                    if (aInt > 0) {
                        view.notifyRemoval(pos);
                    } else {
                        view.showError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.OPERATION));
                    }
                }));
    }

    @Override
    public void restoreFavorite(int pos, Favorito favorito) {
    }

    @Override
    public CatalogoResponse.RecargaEntity getRecargaModel(Favorito favorito) {
        return JsonUtil.fromJson(favorito.getServicio(), CatalogoResponse.RecargaEntity.class);
    }

    @Override
    public ArrayList<CatalogoResponse.MontoEntity> getMontos(Favorito favorito) {
        return JsonUtil.fromJson(favorito.getMontos(), montoListType);
    }

    @Override
    public ServicioResponse.ServicioEntity getServicioModel(Favorito favorito) {
        return JsonUtil.fromJson(favorito.getServicio(), ServicioResponse.ServicioEntity.class);
    }

    @Override
    public ServiceModel getServiceModel(Favorito favorito) {
        String fav = favorito.getServicio();
        Timber.d("JSON ACI: %s", fav);
        return JsonUtil.fromJson(favorito.getServicio(), ServiceModel.class);
    }
}
