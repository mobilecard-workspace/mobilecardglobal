package addcel.mobilecard.ui.usuario.wallet.create;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 10/10/17.
 */
@PerFragment
@Subcomponent(modules = WalletCreateModule.class)
public interface WalletCreateSubcomponent {
    void inject(WalletCreateFragment fragment);
}
