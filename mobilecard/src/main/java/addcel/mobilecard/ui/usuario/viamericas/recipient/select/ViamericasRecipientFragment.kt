package addcel.mobilecard.ui.usuario.viamericas.recipient.select

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.viamericas.fees.VmFeeViewModel
import addcel.mobilecard.ui.usuario.viamericas.recipient.create.ViamericasRecipientCreateActivity
import addcel.mobilecard.ui.usuario.viamericas.recipient.create.ViamericasRecipientCreateModel
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.BundleBuilder
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.view_viamericas_recipient.*
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/**
 * ADDCEL on 27/03/17.
 */


@Parcelize
data class ViamericasRecipientModel(
        val idSender: String,
        val pais: VmCountryModel,
        val currencies: List<CurrencyModel>,
        val paymentMode: PaymentModeModel,
        val location: LocationModel,
        val montoEnvio: Double,
        val exchangeRate: ExchangeRateModel
) : Parcelable


interface VmRecipientView : ScreenView {
    fun clickAdd()

    fun clickContinue()

    fun postRecipients(recipients: List<RecipientResponse>)

    fun launchBankDepositFragment(recipient: RecipientResponse, fees: OrderFeeModel)

    fun launchNonBankDepositFragment(recipient: RecipientResponse, fees: OrderFeeModel)
}

class ViamericasRecipientFragment : Fragment(), VmRecipientView {

    //DEPENDENCIAS
    @Inject
    lateinit var containerActivity: ContainerActivity
    @Inject
    lateinit var model: ViamericasRecipientModel
    @field:[Inject Named("viamericasRecipientAdapter")]
    lateinit var recipientAdapter: ArrayAdapter<RecipientResponse>
    @Inject
    lateinit var presenter: ViamericasRecipientPresenter

    private lateinit var unbinder: Unbinder
    private var recipientsFirstLoad = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get()
                .netComponent
                .viamericasRecipientSubcomponent(ViamericasRecipientModule(this))
                .inject(this)

        if (recipientAdapter.count == 0)
            presenter.getRecipients(model.idSender, model.pais)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return LayoutInflater.from(context)
                .inflate(R.layout.view_viamericas_recipient, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
        unbinder = ButterKnife.bind(this, view)
        spinner_viamericas_recipient_frequent.adapter = recipientAdapter
        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
    }

    override fun onDestroyView() {
        unbinder.unbind()
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun showProgress() {
        containerActivity.showProgress()
        if (view != null && isVisible) {
            spinner_viamericas_recipient_frequent.isEnabled = false
            b_viamericas_recipient_add.isEnabled = false
            b_viamericas_recipient_continuar.isEnabled = false
        }
    }

    override fun hideProgress() {
        containerActivity.hideProgress()
        if (view != null && isVisible) {
            spinner_viamericas_recipient_frequent.isEnabled = true
            b_viamericas_recipient_add.isEnabled = true
            b_viamericas_recipient_continuar.isEnabled = true
        }
    }

    override fun showError(msg: String) {
        containerActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerActivity.showSuccess(msg)
    }

    @OnClick(R.id.b_viamericas_recipient_add)
    override fun clickAdd() {
        val vrcModel = ViamericasRecipientCreateModel(model.idSender, model.pais)

        startActivityForResult(
                ViamericasRecipientCreateActivity.get(containerActivity, vrcModel),
                ViamericasRecipientCreateActivity.REQUEST_CODE
        )
    }

    @OnClick(R.id.b_viamericas_recipient_continuar)
    override fun clickContinue() {
        if (recipientAdapter.count == 0) {
            showError(getString(R.string.error_viamericas_recipient_empty))
        } else {
            val recipient =
                    recipientAdapter.getItem(spinner_viamericas_recipient_frequent.selectedItemPosition)
            if (recipient != null) {
                launchWalletFrament(recipient)
            }
        }
    }

    private fun launchWalletFrament(recipient: RecipientResponse) {
        val fViewModel = VmFeeViewModel(
                recipient,
                model.currencies,
                model.paymentMode,
                model.location,
                model.montoEnvio,
                model.exchangeRate
        )

        Timber.d("Paymentmode: %s", model.paymentMode)

        val args = BundleBuilder().putParcelable("model", fViewModel).build()
        if (model.paymentMode.paymentModeId.equals("C", true)) {
            containerActivity.fragmentManagerLazy.commit {
                add(
                        R.id.frame_viamericas,
                        WalletSelectFragment.get(WalletSelectFragment.TRANSFER_US_CARD, args)
                )
                hide(this@ViamericasRecipientFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            }
        } else {
            containerActivity.fragmentManagerLazy.commit {
                add(
                        R.id.frame_viamericas,
                        WalletSelectFragment.get(WalletSelectFragment.TRANSFER_US_CASH, args)
                )
                addToBackStack(null)
                hide(this@ViamericasRecipientFragment)
                setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            }
        }
    }


    override fun postRecipients(recipients: List<RecipientResponse>) {
        if (isVisible) {
            recipientsFirstLoad = false
            recipientAdapter.addAll(recipients)
            if (recipients.isEmpty()) {
                Timber.i("No hay recipients")
                spinner_viamericas_recipient_frequent.isEnabled = false
            } else {
                Timber.i("Hay recipients")
                spinner_viamericas_recipient_frequent.isEnabled = true
            }
        }
    }

    override fun launchBankDepositFragment(recipient: RecipientResponse, fees: OrderFeeModel) {
        /*ViamericasCardFragment cardFragment =
                ViamericasCardFragment.get(recipient, currencies, paymentMode, location, montoEnvio,
                        exchangeRate, null);
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_viamericas, cardFragment)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();*/
    }

    override fun launchNonBankDepositFragment(recipient: RecipientResponse, fees: OrderFeeModel) {
        /* ViamericasCashFragment cashFragment =
                ViamericasCashFragment.get(recipient, currencies, paymentMode, location, montoEnvio,
                        exchangeRate, null);
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_viamericas, cashFragment)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ViamericasRecipientCreateActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val recipientResp: RecipientResponse =
                            data.getParcelableExtra(ViamericasRecipientCreateActivity.RESULT_DATA)!!
                    recipientAdapter.add(recipientResp)
                    Handler().postDelayed({
                        launchWalletFrament(recipientResp)
                    }, 250)
                }
            } else {
                containerActivity.showError("Cancelado por el usuario")
            }
        }
    }

    companion object {
        fun get(model: ViamericasRecipientModel): ViamericasRecipientFragment {
            val bundle = BundleBuilder().putParcelable("model", model).build()
            val fragment = ViamericasRecipientFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
