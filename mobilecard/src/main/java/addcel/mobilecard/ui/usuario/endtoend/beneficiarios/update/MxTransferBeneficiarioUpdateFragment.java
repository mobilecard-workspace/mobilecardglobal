package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.update;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.AccountResponse;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import addcel.mobilecard.validation.ValidationUtils;
import addcel.mobilecard.validation.annotation.Celular;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * ADDCEL on 08/11/17.
 */
public class MxTransferBeneficiarioUpdateFragment extends Fragment
        implements MxTransferBeneficiarioUpdateContract.View {

    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_transfermx_beneficiario_update_alias)
    TextInputLayout tilAlias;

    @Email(messageResId = R.string.error_email)
    @BindView(R.id.til_transfermx_beneficiario_update_email)
    TextInputLayout tilEmail;

    @Celular(messageResId = R.string.error_celular)
    @BindView(R.id.til_transfermx_beneficiario_update_phone)
    TextInputLayout tilPhone;

    @BindView(R.id.b_transfermx_beneficiarios_update_continue)
    Button continueButton;

    @Inject
    MxTransferActivity activity;
    @Inject
    Validator validator;
    @Inject
    MxTransferBeneficiarioUpdateContract.Presenter presenter;
    private Unbinder unbinder;

    public static MxTransferBeneficiarioUpdateFragment get(AccountEntity account) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("beneficiario", account);
        MxTransferBeneficiarioUpdateFragment fragment = new MxTransferBeneficiarioUpdateFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .mxTransferBeneficiarioUpdateSubcomponent(new MxTransferBeneficiarioUpdateModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_mx_transfer_beneficiario_update, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        activity.findViewById(R.id.b_success_share).setVisibility(View.GONE);
        Objects.requireNonNull(tilAlias.getEditText()).setText(presenter.getAccount().getAlias());
        Objects.requireNonNull(tilEmail.getEditText()).setText(presenter.getAccount().getCorreo());
        Objects.requireNonNull(tilPhone.getEditText()).setText(presenter.getAccount().getTelefono());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public String getAlias() {
        return Strings.nullToEmpty(Objects.requireNonNull(tilAlias.getEditText()).getText().toString());
    }

    @Override
    public String getEmail() {
        return Strings.nullToEmpty(Objects.requireNonNull(tilEmail.getEditText()).getText().toString());
    }

    @Override
    public String getNumTelefono() {
        return Strings.nullToEmpty(Objects.requireNonNull(tilPhone.getEditText()).getText().toString());
    }

    @OnClick(R.id.b_transfermx_beneficiarios_update_continue)
    @Override
    public void clickSave(View view) {
        clearAfterSuccess();
        validator.validate();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void onSave(AccountResponse accountResponse) {
        activity.showSuccess(accountResponse.getMensajeError());
        activity.updateBeneficiarios(accountResponse.getAccounts());
    }

    @Override
    public void onValidationSucceeded() {
        clearAfterSuccess();
        presenter.save();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    private void clearAfterSuccess() {
        if (isVisible()) ValidationUtils.clearViewErrors(tilAlias, tilEmail, tilPhone);
    }
}
