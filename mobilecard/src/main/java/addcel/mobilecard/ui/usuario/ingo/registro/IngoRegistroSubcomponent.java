package addcel.mobilecard.ui.usuario.ingo.registro;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 18/07/18.
 */
@PerFragment
@Subcomponent(modules = IngoRegistroModule.class)
public interface IngoRegistroSubcomponent {
    void inject(IngoRegistroFragment fragment);
}
