package addcel.mobilecard.ui.usuario.scanpay.qr.mx.concepto

import addcel.mobilecard.R
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.validation.ValidationUtils
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_cobro_create_form.*
import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 2019-11-19.
 */

@Parcelize
data class SpMxConceptoQrViewModel(val pago: SPPagoEntity) : Parcelable

class SpMxConceptoQrFragment : Fragment() {

    companion object {

        private val PESO_FORMAT = NumberFormat.getCurrencyInstance(Locale("es", "MX"))

        fun get(model: SpMxConceptoQrViewModel): SpMxConceptoQrFragment {
            val args = BundleBuilder().putParcelable("model", model)
                    .build()
            val frag = SpMxConceptoQrFragment()
            frag.arguments = args
            return frag
        }
    }

    lateinit var model: SpMxConceptoQrViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_negocio_cobro_create_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ScanPayActivity).setFinished(false)

        b_negocio_cobro_create_form_continuar.setOnClickListener { clickContinuar() }

        AndroidUtils.setText(
                til_negocio_cobro_create_form_monto,
                PESO_FORMAT.format(model.pago.amount)
        )
    }

    fun clickContinuar() {
        clearErrors()

        val concepto = AndroidUtils.getText(til_negocio_cobro_create_form_concepto.editText)
        val propina = AndroidUtils.getText(til_negocio_cobro_create_form_propina.editText)

        if (concepto.isEmpty()) {
            til_negocio_cobro_create_form_concepto.error = "Ingresa el concepto de tu compra."
        } else if (propina.isNotEmpty() && !propina.isDigitsOnly()) {
            til_negocio_cobro_create_form_propina.error = "Ingresa un monto válido."
        } else {
            clearErrors()
            val propinaD =
                    if (propina.isNotEmpty() && propina.isDigitsOnly()) propina.toDouble() else 0.0
            goToCards(propinaD, concepto)
        }

    }

    private fun clearErrors() {
        if (isVisible) ValidationUtils.clearViewErrors(
                til_negocio_cobro_create_form_concepto,
                til_negocio_cobro_create_form_propina
        )
    }

    private fun buildWalletBundle(spPagoEntity: SPPagoEntity): Bundle {
        return BundleBuilder().putParcelable("pago", spPagoEntity).build()
    }

    private fun goToCards(propina: Double, concepto: String) {
        val resultEntity = model.pago.copy(propina = propina, concept = concepto)


        (activity as ScanPayActivity).supportFragmentManager.commit {
            add(
                    R.id.frame_scanpay,
                    WalletSelectFragment.get(
                            WalletSelectFragment.SCANPAY_QR_MX,
                            buildWalletBundle(resultEntity)
                    )
            ).hide(
                    this@SpMxConceptoQrFragment
            ).addToBackStack(null)
                    .setCustomAnimations(
                            android.R.anim.slide_in_left,
                            android.R.anim.slide_out_right
                    )
        }

    }

}