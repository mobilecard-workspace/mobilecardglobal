package addcel.mobilecard.ui.usuario.scanpay.manual.monto;

import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 23/08/18.
 */
public interface ScanManualMontoContract {
    interface View extends Validator.ValidationListener {

        double getMonto();

        double getPropina();

        String getConcepto();

        CardEntity getFormaPago();

        String getMsi();

        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void setCards(List<CardEntity> cards);

        void setEstablecimientoData(android.view.View view);

        void clickContinuar();
    }

    interface Presenter {
        LcpfEstablecimiento getEstablecimiento();

        void getCards();

        boolean validateMonto(String monto);
    }
}
