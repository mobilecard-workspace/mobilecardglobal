package addcel.mobilecard.ui.usuario.scanpay.qr.confirm

import addcel.mobilecard.McConstants
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.FranquiciaEntity
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureActivity
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureActivity.Companion.get
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureModel
import addcel.mobilecard.ui.usuario.billpocket.BillPocketUseCase
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import addcel.mobilecard.ui.usuario.scanpay.result.ScanResultFragment.Companion.get
import addcel.mobilecard.utils.*
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.common.base.Strings
import com.squareup.otto.Bus
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.screen_scanpay_qr_confirm.*
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 11/09/18.
 */
class ScanQrConfirmFragment : Fragment(), ScanQrConfirmView {

    @Inject
    lateinit var cActivity: ScanPayActivity
    @Inject
    lateinit var bus: Bus
    @Inject
    lateinit var picasso: Picasso
    @Inject
    lateinit var presenter: ScanQrConfirmPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get()
                .netComponent
                .scanQrConfirmSubcomponent(ScanQrConfirmModule(this))
                .inject(this)
        bus.register(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.screen_scanpay_qr_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b_scanpay_qr_confirm_continuar.setOnClickListener { clickPurchaseButton() }
        setUiData()

        if (isDebito(presenter.formaPago) || isAmex(presenter.formaPago)) {
            spinner_scanpay_card_msi.setSelection(0)
            spinner_scanpay_card_msi.isEnabled = false
        } else {
            spinner_scanpay_card_msi.isEnabled = true
        }

        if (presenter.idPais == McConstants.PAIS_ID_MX || presenter.idPais == McConstants.PAIS_ID_USA) {
            til_scanpay_email.visibility = View.VISIBLE
        } else {
            til_scanpay_email.visibility = View.GONE
        }

        picasso.load(presenter.formaPago.imgShort)
                .placeholder(R.drawable.bg_visa)
                .placeholder(R.drawable.bg_visa)
                .fit()
                .into(view.findViewById(R.id.bg_scan_card), object : Callback {
                    override fun onSuccess() {
                        (view.findViewById<View>(R.id.pan_scan_card) as TextView).text = StringUtil.maskCard(AddcelCrypto.decryptHard(presenter.formaPago.pan))
                    }

                    override fun onError(e: Exception) {
                        (view.findViewById<View>(R.id.pan_scan_card) as TextView).text = StringUtil.maskCard(AddcelCrypto.decryptHard(presenter.formaPago.pan))
                    }
                })
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!isHidden) enablePurchaseButton()
    }

    override fun onDestroy() {
        bus.unregister(this)
        super.onDestroy()
    }

    override fun showProgress() {
        cActivity.showProgress()
    }

    override fun hideProgress() {
        cActivity.hideProgress()
    }

    override fun showError(msg: String) {
        cActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        cActivity.showSuccess(msg)
    }

    override fun getEmail(): String {
        return AndroidUtils.getText(til_scanpay_email.editText)
    }

    override fun setUiData() {
        val invoice: String = if (presenter.comision == 0.0) {
            if (presenter.idPais == 4) {
                getString(R.string.txt_scanpay_manual_confirm_invoice_no_fee_peru,
                        presenter.formattedMonto, presenter.formattedPropina,
                        presenter.formattedComision, presenter.concepto,
                        presenter.referenciaNegocio)
            } else {
                getString(R.string.txt_scanpay_manual_confirm_invoice_no_fee,
                        presenter.formattedMonto, presenter.formattedPropina, presenter.concepto,
                        presenter.referenciaNegocio)
            }
        } else {
            getString(R.string.txt_scanpay_manual_confirm_invoice, presenter.formattedMonto,
                    presenter.formattedPropina, presenter.formattedComision,
                    presenter.concepto, presenter.referenciaNegocio)
        }
        view_scanpay_qr_confirm_data.text = invoice
        view_scanpay_qr_confirm_total.text = String.format("Total: %s", presenter.formattedTotal)
    }

    override fun onWhiteList() {
        val kPlaceholder = Calendar.getInstance().timeInMillis.toString()
        presenter.getToken(AddcelCrypto.encryptHard(kPlaceholder))
    }

    override fun notOnWhiteList() {
        showError("No estás autorizado para realizar esta operación. Contacta a soporte.")
    }

    override fun launchGetToken(profile: String) {
        presenter.getToken(profile)
    }

    override fun onTokenReceived(tokenEntity: TokenEntity) {
        if (tokenEntity.secure) {
            Timber.d("ScanQrConfirmFragment - %s", tokenEntity.secure)
            val request = presenter.buildRequest()
            val model = BillPocketSecureModel(presenter.fetchIdUsuario(), presenter.formaPago, tokenEntity,
                    presenter.buildRequestString(request), BillPocketUseCase.USUARIO)
            try {
                startActivityForResult(get(cActivity, model), BillPocketSecureActivity.REQUEST_CODE)
            } catch (t: Throwable) {
                t.printStackTrace()
            }
        } else {
            launchPago(tokenEntity)
        }
    }

    override fun launchPago(tokenEntity: TokenEntity) {
        presenter.launchPago(tokenEntity, presenter.buildRequest())
    }

    override fun onPagoSuccess(resultEntity: SPReceiptEntity) {
        b_scanpay_qr_confirm_continuar.isEnabled = true
        // FragmentUtil.addFragmentHidingCurrent(cActivity.getSupportFragmentManager(),
// R.id.frame_scanpay, this, ScanSecureFragment.get(presenter.buildRequest()));
        FragmentUtil.addFragmentHidingCurrent(cActivity.supportFragmentManager, R.id.frame_scanpay,
                this, get(resultEntity, presenter.idPais, false))

        enablePurchaseButton()
    }

    override fun isPurchaseButtonEnabled(): Boolean {
        return isVisible && b_scanpay_qr_confirm_continuar.isEnabled
    }

    override fun disablePurchaseButton() {
        if (isVisible) b_scanpay_qr_confirm_continuar.isEnabled = false
    }

    override fun enablePurchaseButton() {
        if (isVisible) b_scanpay_qr_confirm_continuar.isEnabled = true
    }

    override fun clickPurchaseButton() {
        if (isPurchaseButtonEnabled()) {

            disablePurchaseButton()

            til_scanpay_email.error = null
            if (Strings.isNullOrEmpty(getEmail())) {
                presenter.isOnWhiteList()
            } else {
                if (Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches()) {
                    presenter.isOnWhiteList()
                } else {
                    til_scanpay_email.error = getString(R.string.error_email)
                }
            }
        }
    }

    override fun getMsi(): String {
        return spinner_scanpay_card_msi.selectedItem as String
    }

    private fun isDebito(card: CardEntity): Boolean {
        return card.tipoTarjeta == TipoTarjetaEntity.DEBITO
    }

    private fun isAmex(card: CardEntity): Boolean {
        return if (card.tipo != null) {
            when (card.tipo) {
                FranquiciaEntity.AmEx, FranquiciaEntity.Amex -> true
                else -> false
            }
        } else false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BillPocketSecureActivity.REQUEST_CODE) {
            onBillPocketSecureResult(resultCode, data)
        }
    }

    override fun onBillPocketSecureResult(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val resultData = data.getStringExtra(BillPocketSecureActivity.RESULT_DATA)
                if (JsonUtil.isJson(resultData)) {
                    val response = JsonUtil.fromJson(resultData, SPReceiptEntity::class.java)
                    onPagoSuccess(response)
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            showError("Pago cancelado por el usuario.")
        }
    }

    companion object {

        fun get(arguments: Bundle): ScanQrConfirmFragment {
            val fragment = ScanQrConfirmFragment()
            fragment.arguments = arguments
            return fragment
        }

        fun get(pagoEntity: SPPagoEntity): ScanQrConfirmFragment {
            val bundle = BundleBuilder().putParcelable("pago", pagoEntity).build()
            val fragment = ScanQrConfirmFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}