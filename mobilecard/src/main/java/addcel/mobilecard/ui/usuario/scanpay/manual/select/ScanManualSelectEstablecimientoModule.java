package addcel.mobilecard.ui.usuario.scanpay.manual.select;

import com.squareup.otto.Bus;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoInteractor;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoInteractorImpl;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

@Module
public final class ScanManualSelectEstablecimientoModule {
    private final ScanManualSelectEstablecimientoFragment fragment;

    public ScanManualSelectEstablecimientoModule(ScanManualSelectEstablecimientoFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ScanPayActivity provideActivity() {
        return (ScanPayActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    ScanPayService provideAPI(@Named("longClient") OkHttpClient client) {
        final Retrofit retrofit = new Retrofit.Builder().client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
        return retrofit.create(ScanPayService.class);
    }

    @PerFragment
    @Provides
    ScanManualSelectEstablecimientoInteractor provideInteractor(
            ScanPayService service, SessionOperations session, Bus bus) {
        return new ScanManualSelectEstablecimientoInteractorImpl(service, session, bus);
    }

    @PerFragment
    @Provides
    ScanManualSelectEstablecimientoAdapter provideAdapter(
            ScanManualSelectEstablecimientoInteractor interactor) {
        return new ScanManualSelectEstablecimientoAdapter(interactor.getItems());
    }

    @PerFragment
    @Provides
    ScanManualSelectEstablecimientoContract.Presenter providePresenter(
            ScanManualSelectEstablecimientoInteractor interactor) {
        return new ScanManualSelectEstablecimientoPresenter(interactor, fragment);
    }
}
