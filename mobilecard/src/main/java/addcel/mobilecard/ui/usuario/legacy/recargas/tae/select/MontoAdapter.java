package addcel.mobilecard.ui.usuario.legacy.recargas.tae.select;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.text.NumberFormat;
import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.ui.custom.adapter.SelectableAdapter;

/**
 * ADDCEL on 17/11/16.
 */
public final class MontoAdapter extends SelectableAdapter<MontoAdapter.ViewHolder> {

    private final List<CatalogoResponse.MontoEntity> montos;
    private final NumberFormat numberFormat;

    public MontoAdapter(NumberFormat numberFormat) {
        this.montos = Lists.newArrayList();
        this.numberFormat = numberFormat;
    }

    public MontoAdapter(List<CatalogoResponse.MontoEntity> montos, NumberFormat numberFormat) {
        this.montos = montos;
        this.numberFormat = numberFormat;
    }

    public void update(List<CatalogoResponse.MontoEntity> models) {
        montos.clear();
        montos.addAll(models);
        notifyDataSetChanged();
    }

    public CatalogoResponse.MontoEntity getItem(int pos) {
        return montos.get(pos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.view_monto, parent, false);
        return new MontoAdapter.ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CatalogoResponse.MontoEntity monto = montos.get(position);

        if (monto.getMonto() > 0) {
            holder.montoText.setText(numberFormat.format(monto.getMonto()));
        } else {
            holder.montoText.setText(Strings.nullToEmpty(monto.getDescripcion()));
        }

        if (isSelected(position)) {
            holder.montoText.setBackgroundColor(
                    ContextCompat.getColor(holder.montoText.getContext(), R.color.accent));
            holder.montoText.setTextColor(Color.WHITE);
        } else {
            holder.montoText.setBackground(
                    ContextCompat.getDrawable(holder.montoText.getContext(), R.drawable.sh_rectangle));
            holder.montoText.setTextColor(Color.GRAY);
        }
    }

    @Override
    public int getItemCount() {
        return montos.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView montoText;

        ViewHolder(View view) {
            super(view);
            this.montoText = view.findViewById(R.id.text_monto);
        }
    }
}
