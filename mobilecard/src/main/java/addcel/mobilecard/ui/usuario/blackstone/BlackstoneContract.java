package addcel.mobilecard.ui.usuario.blackstone;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

/**
 * ADDCEL on 06/07/18.
 */
interface BlackstoneContract {
    interface View {
        void showProgress();

        void hideProgress();

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);

        Toolbar getToolbarFromActivity();

        void setFinished(boolean finished);
    }

    interface Presenter {

    }
}
