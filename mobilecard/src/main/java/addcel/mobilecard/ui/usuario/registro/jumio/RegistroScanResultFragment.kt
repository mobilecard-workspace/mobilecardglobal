package addcel.mobilecard.ui.usuario.registro.jumio

import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * ADDCEL on 2019-07-09.
 */

@Parcelize
data class RegistroResultFragmentData(
        val email: String, val reference: String,
        val pais: PaisResponse.PaisEntity, val message: String, val buttonText: String
) : Parcelable

