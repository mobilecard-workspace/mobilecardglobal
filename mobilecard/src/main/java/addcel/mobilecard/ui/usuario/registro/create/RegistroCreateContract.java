package addcel.mobilecard.ui.usuario.registro.create;

import com.mobsandgeeks.saripaar.Validator;

/**
 * ADDCEL on 28/09/17.
 */
interface RegistroCreateContract {
    interface View extends Validator.ValidationListener {
        void showProgress();

        void hideProgress();

        void showError(int res);

        void showError(String msg);

        void onPrivacy(String privacy);

        String getEmail();

        String getPassword();

        void clickInfo();

        void clickPrivacy();

        void clickContinue();
    }

    interface Presenter {
        void getPrivacy();
    }
}
