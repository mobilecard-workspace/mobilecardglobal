package addcel.mobilecard.ui.usuario.scanpay.result.qr;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

/**
 * ADDCEL on 11/09/18.
 */
public interface ScanResultQrContract {
    interface View {
        void setQrCode(Bitmap bitmap);

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);
    }

    interface Presenter {

        void clearCompositeDisposable();

        void createQrCode(int sideLen, int fgColor, int bgColor);

        Bitmap getBitmapOfRootView(android.view.View view);

        String buildScreenShotName();

        void createImage(Bitmap bm);

        CharSequence buildMsg(String base);
    }
}
