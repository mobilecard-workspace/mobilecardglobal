package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.compra

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.ContainerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-12-19.
 */
@Module
class MmProductosConfirmModule(val fragment: MmProductosConfirmFragment) {

    @PerFragment
    @Provides
    fun provideActivity(): ContainerActivity {
        return fragment.activity as ContainerActivity
    }

    @PerFragment
    @Provides
    fun provideViewModel(): MmProductosConfirmViewModel {
        return MmProductosConfirmViewModel(
                fragment.arguments?.getParcelable("producto")!!,
                fragment.arguments?.getParcelable("valor")!!,
                fragment.arguments?.getDouble("amount", 0.0)!!,
                fragment.arguments?.getParcelable("card")!!
        )
    }

    @PerFragment
    @Provides
    fun provideMultimarket(r: Retrofit): MultimarketAPI {
        return MultimarketAPI.get(r)
    }

    @PerFragment
    @Provides
    fun provideUsuario(session: SessionOperations): Usuario {
        return session.usuario
    }

    @PerFragment
    @Provides
    fun providePresenter(multimarket: MultimarketAPI, usuario: Usuario): MmProductosConfirmPresenter {
        return MmProductosConfirmPresenterImpl(multimarket, usuario, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [MmProductosConfirmModule::class])
interface MmProductosConfirmSubcomponent {
    fun inject(fragment: MmProductosConfirmFragment)
}