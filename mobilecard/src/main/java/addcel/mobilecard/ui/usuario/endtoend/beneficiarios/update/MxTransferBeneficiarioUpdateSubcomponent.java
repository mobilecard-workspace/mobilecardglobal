package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.update;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/11/17.
 */
@PerFragment
@Subcomponent(modules = MxTransferBeneficiarioUpdateModule.class)
public interface MxTransferBeneficiarioUpdateSubcomponent {
    void inject(MxTransferBeneficiarioUpdateFragment fragment);
}
