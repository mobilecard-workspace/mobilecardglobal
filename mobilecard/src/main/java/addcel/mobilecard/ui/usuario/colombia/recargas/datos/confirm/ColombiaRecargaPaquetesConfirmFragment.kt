package addcel.mobilecard.ui.usuario.colombia.recargas.datos.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.Categoria
import addcel.mobilecard.data.net.catalogo.OperadorRecarga
import addcel.mobilecard.data.net.catalogo.Paquete
import addcel.mobilecard.data.net.colombia.PaqueteRequest
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.PurchaseScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.colombia.recargas.ColombiaRecargasActivity
import addcel.mobilecard.ui.usuario.colombia.recargas.categorias.ColombiaCategoria
import addcel.mobilecard.ui.usuario.colombia.result.ColombiaResultFragment
import addcel.mobilecard.utils.DeviceUtil
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_scanpay_qr_confirm.*
import mx.mobilecard.crypto.AddcelCrypto
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 9/24/19.
 */

@Parcelize
data class ColombiaRecargaPaquetesConfirmModel(
        val usuario: Usuario,
        val colombiaCategoria: ColombiaCategoria, val operadorRecarga: OperadorRecarga,
        val paquete: Categoria, val monto: Paquete, val destino: String
) : Parcelable

interface ColombiaRecargaPaquetesConfirmView : PurchaseScreenView {
    fun setInvoiceLabel()
    fun setCardInfo()
    fun setMontoLabel()
    fun launchPago()
    fun onPagoSuccess(receipt: SPReceiptEntity)
}

class ColombiaRecargaPaquetesConfirmFragment : Fragment(), ColombiaRecargaPaquetesConfirmView {


    companion object {

        private val COL_FORMAT = NumberFormat.getCurrencyInstance(Locale("es", "CO"))

        fun get(arguments: Bundle): ColombiaRecargaPaquetesConfirmFragment {
            val frag = ColombiaRecargaPaquetesConfirmFragment()
            frag.arguments = arguments
            return frag
        }
    }

    @Inject
    lateinit var presenter: ColombiaRecargaPaquetesConfirmPresenter

    private lateinit var card: CardEntity
    private lateinit var model: ColombiaRecargaPaquetesConfirmModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get().netComponent.colombiaRecargaPaquetesConfirmSubcomponent(
                ColombiaRecargaPaquetesConfirmModule(this)
        ).inject(this)

        card = arguments?.getParcelable("card")!!
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_scanpay_qr_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b_scanpay_qr_confirm_continuar.setOnClickListener { clickPurchaseButton() }
        setInvoiceLabel()
        setMontoLabel()
        setCardInfo()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) enablePurchaseButton()
    }

    override fun setInvoiceLabel() {
        val invoice = getString(
                R.string.txt_scanpay_recarga_confirm_invoice_co,
                COL_FORMAT.format(model.monto.valor.toInt()), COL_FORMAT.format(0.0),
                model.monto.descripcion, model.destino
        )
        view_scanpay_qr_confirm_data.text = invoice
    }

    override fun setCardInfo() {
        Picasso.get().load(card.imgShort).placeholder(R.drawable.bg_visa)
                .placeholder(R.drawable.bg_visa).fit().into(bg_scan_card, object : Callback {
                    override fun onSuccess() {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }

                    override fun onError(e: Exception) {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }
                })
    }

    override fun setMontoLabel() {
        val total = model.monto.valor.toInt()
        view_scanpay_qr_confirm_total.text = COL_FORMAT.format(total)
    }

    override fun onWhiteList() {
        if (activity != null) launchPago()
    }

    override fun notOnWhiteList() {
        showError("No está autorizado para realizar esta operación. Contacte a soporte.")
    }

    override fun launchPago() {
        val request = PaqueteRequest(
                model.monto.descripcion, BuildConfig.ADDCEL_APP_ID, 2,
                model.monto.id.toString(), card.idTarjeta, model.usuario.ideUsuario,
                StringUtil.getCurrentLanguage(), DeviceUtil.getDeviceId(activity!!), model.destino
        )

        presenter.launchPago(request)
    }

    override fun onPagoSuccess(receipt: SPReceiptEntity) {
        (activity as ColombiaRecargasActivity).fragmentManagerLazy.commit {
            add(R.id.frame_colombia, ColombiaResultFragment.get(receipt))
            hide(this@ColombiaRecargaPaquetesConfirmFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
        enablePurchaseButton()
    }

    override fun isPurchaseButtonEnabled(): Boolean {
        return b_scanpay_qr_confirm_continuar.isEnabled
    }

    override fun disablePurchaseButton() {
        if (isVisible) b_scanpay_qr_confirm_continuar.isEnabled = false
    }

    override fun enablePurchaseButton() {
        if (isVisible) b_scanpay_qr_confirm_continuar.isEnabled = true
    }

    override fun clickPurchaseButton() {
        if (isPurchaseButtonEnabled()) {
            if (activity != null) {
                disablePurchaseButton()
                presenter.isOnWhiteList(model.usuario.ideUsuario, model.usuario.idPais)
            }
        }
    }

    override fun showProgress() {
        (activity as ColombiaRecargasActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ColombiaRecargasActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ColombiaRecargasActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ColombiaRecargasActivity).showSuccess(msg)
    }
}