package addcel.mobilecard.ui.usuario.legacy.recargas.tae.select;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.transacto.TransactoService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.transacto.TransactoInteractor;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 15/12/16.
 */
@Module
public final class RecargaModule {
    private final RecargaFragment fragment;

    RecargaModule(RecargaFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    @Named("colorLogoMap")
    Map<Integer, Integer> provideRecargaLabelMap() {
        final Map<Integer, Integer> map = new LinkedHashMap<>();
        map.put(1, R.drawable.logo_telcel);
        map.put(2, R.drawable.logo_movistar);
        map.put(3, R.drawable.logo_att);
        map.put(4, R.drawable.logo_nextel);
        map.put(5, R.drawable.logo_att);
        map.put(6, R.drawable.logo_id);
        map.put(7, R.drawable.logo_todito);
        map.put(8, R.drawable.logo_pase);
        return map;
    }

    @PerFragment
    @Provides
    PagoContainerActivity provideActivity() {
        return (PagoContainerActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    CatalogoResponse.RecargaEntity provideServicio() {
        return Objects.requireNonNull(fragment.getArguments()).getParcelable("servicio");
    }

    @PerFragment
    @Provides
    List<CatalogoResponse.MontoEntity> montos() {
        return Objects.requireNonNull(fragment.getArguments()).getParcelableArrayList("montos");
    }

    @PerFragment
    @Provides
    @Nullable
    PaisResponse.PaisEntity providePais() {
        return Objects.requireNonNull(fragment.getArguments()).getParcelable("pais");
    }

    @PerFragment
    @Provides
    boolean fav() {
        return Objects.requireNonNull(fragment.getArguments()).getBoolean("fav");
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    MontoAdapter provideAdapter(List<CatalogoResponse.MontoEntity> montos,
                                NumberFormat numberFormat) {
        return new MontoAdapter(montos, numberFormat);
    }

    @PerFragment
    @Provides
    TransactoInteractor provideInteractor(TransactoService api,
                                          FavoritoDaoRx dao, SessionOperations session) {
        return new TransactoInteractor(api, dao, session,
                Preconditions.checkNotNull(fragment.getActivity()).getContentResolver(),
                new CompositeDisposable());
    }

    @PerFragment
    @Provides
    RecargaContract.Presenter providePresenter(TransactoInteractor interactor,
                                               @Nullable PaisResponse.PaisEntity paisModel) {
        return new RecargaPresenterImpl(interactor, paisModel, fragment);
    }
}
