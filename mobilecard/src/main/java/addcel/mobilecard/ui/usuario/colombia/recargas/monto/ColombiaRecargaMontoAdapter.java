package addcel.mobilecard.ui.usuario.colombia.recargas.monto;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.base.Strings;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.colombia.entity.ColombiaRecargaCarrierEntity;
import addcel.mobilecard.ui.custom.adapter.SelectableAdapter;
import addcel.mobilecard.utils.ListUtil;

/**
 * ADDCEL on 17/11/16.
 */
public final class ColombiaRecargaMontoAdapter
        extends SelectableAdapter<ColombiaRecargaMontoAdapter.ViewHolder> {

    private final List<ColombiaRecargaCarrierEntity.MontoEntity> montos;

    ColombiaRecargaMontoAdapter(List<ColombiaRecargaCarrierEntity.MontoEntity> montos) {
        this.montos = montos;
    }

    public void update(List<ColombiaRecargaCarrierEntity.MontoEntity> models) {
        montos.clear();
        montos.addAll(models);
        notifyDataSetChanged();
    }

    private ColombiaRecargaCarrierEntity.MontoEntity getItem(int pos) {
        return montos.get(pos);
    }

    public ColombiaRecargaCarrierEntity.MontoEntity getSelectedItem() {
        List<Integer> selected = getSelectedItems();
        return getItem(ListUtil.getFirstElement(selected));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.view_monto, parent, false);
        return new ColombiaRecargaMontoAdapter.ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ColombiaRecargaCarrierEntity.MontoEntity monto = montos.get(position);
        holder.montoText.setText(Strings.nullToEmpty(monto.getMonto()));
        if (isSelected(position)) {
            holder.montoText.setBackgroundColor(
                    ContextCompat.getColor(holder.montoText.getContext(), R.color.accent));
            holder.montoText.setTextColor(Color.WHITE);
        } else {
            holder.montoText.setBackground(
                    ContextCompat.getDrawable(holder.montoText.getContext(), R.drawable.sh_rectangle));
            holder.montoText.setTextColor(Color.GRAY);
        }
    }

    @Override
    public int getItemCount() {
        return montos.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView montoText;

        ViewHolder(View view) {
            super(view);
            this.montoText = view.findViewById(R.id.text_monto);
        }
    }
}
