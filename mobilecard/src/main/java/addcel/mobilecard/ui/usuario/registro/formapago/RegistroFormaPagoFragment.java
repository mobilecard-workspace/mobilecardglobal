package addcel.mobilecard.ui.usuario.registro.formapago;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.CreditCard;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.otto.Bus;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.usuarios.model.RegistroResponse;
import addcel.mobilecard.data.net.usuarios.model.TerminosResponse;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.domain.sms.SmsUseCase;
import addcel.mobilecard.ui.sms.SmsActivity;
import addcel.mobilecard.ui.usuario.registro.RegistroActivity2;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import commons.validator.routines.CreditCardValidator;
import dagger.Lazy;
import es.dmoral.toasty.Toasty;

/**
 * ADDCEL on 27/09/17.
 */
public final class RegistroFormaPagoFragment extends Fragment
        implements RegistroFormaPagoContract.View {

    @BindColor(R.color.colorDeny)
    int errorColor;

    @BindColor(R.color.colorBlack)
    int textColor;

    @BindDimen(R.dimen.widget_vertical_margin)
    int drawablePadding;

    @CreditCard(cardTypes = {
            CreditCard.Type.AMEX, CreditCard.Type.MASTERCARD, CreditCard.Type.VISA,
    }, messageResId = R.string.error_tarjeta)
    @BindView(R.id.til_registro_add_card_tarjeta)
    TextInputLayout panTil;

    @NotEmpty(messageResId = R.string.error_nombre)
    @BindView(R.id.til_registro_add_card_nombre)
    TextInputLayout nombreTil;

    @NotEmpty(messageResId = R.string.error_vigencia_mm)
    @BindView(R.id.til_registro_add_card_vigencia)
    TextInputLayout vigenciaTil;

    @NotEmpty(messageResId = R.string.error_cvv)
    @BindView(R.id.til_registro_add_card_codigo)
    TextInputLayout cvvTil;

    @NotEmpty(messageResId = R.string.error_domicilio)
    @BindView(R.id.til_registro_add_card_amex_dom)
    TextInputLayout direccionTil;

    @NotEmpty(messageResId = R.string.error_cp)
    @BindView(R.id.til_registro_add_card_amex_cp)
    TextInputLayout cpTil;

    @Checked(messageResId = R.string.error_tipotarjeta)
    @BindView(R.id.group_registro_add_card_cardtype)
    RadioGroup tipoGroup;

    @Checked(messageResId = R.string.error_terms_conditions)
    @BindView(R.id.check_registro_add_card_terms)
    CheckBox terminosCheck;

    @Inject
    Lazy<AlertDialog> infoDialog;
    @Inject
    Lazy<DatePickerDialog> vigenciaPicker;

    @Inject
    RegistroActivity2 containerActivity;
    @Inject
    Validator validator;
    @Inject
    RegistroFormaPagoContract.Presenter presenter;
    @Inject
    Bus bus;

    private Unbinder unbinder;

    public static RegistroFormaPagoFragment get(String email, String password, String nombre,
                                                String apellido, String celular, int idPais, String cedula, int tipoCedula) {
        Bundle bundle = new Bundle();
        bundle.putString("email", email);
        bundle.putString("password", password);
        bundle.putString("nombre", nombre);
        bundle.putString("apellido", apellido);
        bundle.putString("celular", celular);
        bundle.putInt("pais", idPais);
        bundle.putString("cedula", cedula);
        bundle.putInt("tipoCedula", tipoCedula);
        RegistroFormaPagoFragment formaPagoFragment = new RegistroFormaPagoFragment();
        formaPagoFragment.setArguments(bundle);
        return formaPagoFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .registroFormaPagoSubcomponent(new RegistroFormaPagoModule(this))
                .inject(this);
        bus.register(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.view_registro_add_card, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        containerActivity.setLocked(false);
        Button privacyButton = view.findViewById(R.id.b_registro_add_card_privacy);
        Button termsButton = view.findViewById(R.id.b_registro_add_card_terms);
        privacyButton.setPaintFlags(privacyButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        termsButton.setPaintFlags(privacyButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Objects.requireNonNull(nombreTil.getEditText())
                .setText(String.format("%s %s", presenter.getNombre(), presenter.getApellido()));
        Objects.requireNonNull(vigenciaTil.getEditText()).setOnFocusChangeListener((view1, b) -> {
            if (view1 != null && view1.hasFocus()) {
                vigenciaTil.setHint(view1.getResources().getText(R.string.txt_registro_vigencia));
            } else {
                vigenciaTil.setHint(Objects.requireNonNull(view1)
                        .getResources()
                        .getText(R.string.txt_registro_vigencia_hint));
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        containerActivity.setLocked(false);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @OnTextChanged(value = R.id.text_regitro_add_card_tarjeta, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void validateTarjetaOnCapture(CharSequence s) {
        if (!Strings.isNullOrEmpty(s.toString())) {
            if (CreditCardValidator.VISA_VALIDATOR.isValid(s.toString())) {
                Objects.requireNonNull(panTil.getEditText())
                        .setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.card_visa, 0);
                panTil.getEditText().setCompoundDrawablePadding(drawablePadding);
                panTil.getEditText().setTextColor(textColor);
            } else if (CreditCardValidator.MASTERCARD_VALIDATOR.isValid(s.toString())) {
                Objects.requireNonNull(panTil.getEditText())
                        .setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.card_master, 0);
                panTil.getEditText().setCompoundDrawablePadding(drawablePadding);
                panTil.getEditText().setTextColor(textColor);
            } else if (CreditCardValidator.AMEX_VALIDATOR.isValid(s.toString())) {
                Objects.requireNonNull(panTil.getEditText())
                        .setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.card_american, 0);
                panTil.getEditText().setCompoundDrawablePadding(drawablePadding);
                panTil.getEditText().setTextColor(textColor);
            } else {
                Objects.requireNonNull(panTil.getEditText()).setTextColor(errorColor);
                panTil.getEditText().setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
            }
        } else {
            Objects.requireNonNull(panTil.getEditText()).setTextColor(textColor);
            panTil.getEditText().setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    @OnClick(value = R.id.text_regitro_add_card_vigencia)
    @Override
    public void validateVigenciaOnCapture() {
        vigenciaPicker.get().show();
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(containerActivity);
    }

    @Override
    public String getPan() {
        return Strings.nullToEmpty(Objects.requireNonNull(panTil.getEditText()).getText().toString());
    }

    @Override
    public String getHolderName() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(nombreTil.getEditText()).getText().toString());
    }

    @Override
    public String getVigencia() {
        return presenter.getVigenciaFormatted();
    }

    @Override
    public String getCvv() {
        return Strings.nullToEmpty(Objects.requireNonNull(cvvTil.getEditText()).getText().toString());
    }

    @Override
    public String getDomicilio() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(direccionTil.getEditText()).getText().toString());
    }

    @Override
    public String getZip() {
        return Strings.nullToEmpty(Objects.requireNonNull(cpTil.getEditText()).getText().toString());
    }

    @Override
    public TipoTarjetaEntity getTipoTarjeta() {
        return tipoGroup.getCheckedRadioButtonId() == R.id.radio_registro_add_card_credito
                ? TipoTarjetaEntity.CREDITO : TipoTarjetaEntity.DEBITO;
    }

    @Override
    public boolean areTerminosChecked() {
        return false;
    }

    @OnClick(R.id.b_registro_add_card_privacy)
    @Override
    public void clickPrivacy() {
        presenter.getPrivacy();
    }

    @OnClick(R.id.b_registro_add_card_terms)
    @Override
    public void clickTerminos() {
        presenter.getTerminos();
    }

    @OnClick(R.id.b_registro_add_card_info)
    @Override
    public void clickInfo() {
        infoDialog.get().show();
    }

    @OnClick(R.id.b_registro_add_card_continue)
    @Override
    public void clickContinue() {
        validator.validate();
    }

    @Override
    public void showProgress() {
        containerActivity.getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        containerActivity.getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showError(int resId) {
        if (isVisible()) {
            Objects.requireNonNull(getView())
                    .findViewById(R.id.b_registro_add_card_continue)
                    .setEnabled(Boolean.TRUE);
            Toasty.error(Objects.requireNonNull(getContext()), getString(resId)).show();
        }
    }

    @Override
    public void showError(String msg) {
        if (isVisible()) {
            Toasty.error(Objects.requireNonNull(getContext()), msg).show();
            Objects.requireNonNull(getView())
                    .findViewById(R.id.b_registro_add_card_continue)
                    .setEnabled(Boolean.TRUE);
        }
    }

    @Override
    public void showSuccess(String msg) {
        if (isVisible()) {
            Objects.requireNonNull(getView())
                    .findViewById(R.id.b_registro_add_card_continue)
                    .setEnabled(Boolean.TRUE);
            Toasty.success(Objects.requireNonNull(getContext()), msg).show();
        }
    }

    @Override
    public void onRegistroSuccess(RegistroResponse response) {
        containerActivity.startActivity(
                SmsActivity.get(containerActivity, SmsUseCase.U_REGISTRO, response.getIdUsuario(),
                        response.getIdPais()));
        containerActivity.finish();
    }

    @Override
    public void finish() {
        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onPrivacy(String privacy) {
        if (isVisible()) {
            new AlertDialog.Builder(getContext()).setTitle(R.string.txt_registro_privacy)
                    .setMessage(privacy)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                    .show();
        }
    }

    @Override
    public void showTerminosDialog(TerminosResponse terminosResponse) {
        if (isVisible()) {
            new AlertDialog.Builder(getContext()).setTitle(R.string.txt_terms_and_conditions_title)
                    .setMessage(terminosResponse.getTerminos())
                    .setPositiveButton(android.R.string.yes,
                            (dialog, which) -> terminosCheck.setChecked(true))
                    .setNegativeButton(android.R.string.no,
                            (dialog, which) -> terminosCheck.setChecked(false))
                    .show();
        }
    }

    @Override
    public void onValidationSucceeded() {
        Objects.requireNonNull(getView())
                .findViewById(R.id.b_registro_add_card_continue)
                .setEnabled(Boolean.FALSE);
        ValidationUtils.clearViewErrors(panTil);
        presenter.saveUser("");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(containerActivity, errors);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        presenter.updateCalendarVigencia(i, i1);
        Objects.requireNonNull(vigenciaTil.getEditText()).setText(getVigencia());
    }
}
