package addcel.mobilecard.ui.usuario.ingo.processing;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 19/07/18.
 */
@PerFragment
@Subcomponent(modules = IngoProcessingModule.class)
public interface IngoProcessingSubcomponent {
    void inject(IngoProcessingFragment fragment);
}
