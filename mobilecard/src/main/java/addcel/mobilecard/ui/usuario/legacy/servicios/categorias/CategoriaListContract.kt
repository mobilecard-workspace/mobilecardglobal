package addcel.mobilecard.ui.usuario.legacy.servicios.categorias

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import addcel.mobilecard.ui.usuario.legacy.servicios.consulta.ServicioConsultaFragment
import addcel.mobilecard.ui.usuario.legacy.servicios.directo.ServicioFragment
import addcel.mobilecard.ui.usuario.legacy.servicios.list.ServicioSelectionFragment
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.screen_producto_list.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * ADDCEL on 2019-06-15.
 */
class CategoriaListFragment : Fragment(), View {
    companion object {
        fun get(recargas: List<CatalogoResponse.CategoriaEntity>): CategoriaListFragment {
            val args = BundleBuilder().putParcelableArrayList(
                    "categorias",
                    recargas as ArrayList<out Parcelable>
            ).build()
            val fragment = CategoriaListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var containerActivity: PagoContainerActivity
    @Inject
    lateinit var adapter: CategoriaListAdapter
    @Inject
    lateinit var recargas: MutableList<CatalogoResponse.CategoriaEntity>
    @Inject
    lateinit var presenter: Presenter

    private var isLocked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.categoriaListSubcomponent(CategoriaListModule(this))
                .inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): android.view.View? {
        return inflater.inflate(R.layout.screen_producto_list, container, false)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            containerActivity.setAppToolbarTitle(R.string.txt_nav_servicios)
            unLockList()
        }
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unLockList()
        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
        containerActivity.setAppToolbarTitle(R.string.txt_nav_servicios)
        recycler_productos.adapter = adapter
        recycler_productos.layoutManager = GridLayoutManager(view.context, 2)
        ItemClickSupport.addTo(recycler_productos).setOnItemClickListener { _, position, _ ->
            if (!isListLocked()) clickCategoria(adapter.getItem(position))
        }
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_productos)
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun clickCategoria(entity: CatalogoResponse.CategoriaEntity) {
        lockList()
        presenter.getProductos(entity)
        Handler().postDelayed({
            if (isVisible) unLockList()
        }, TimeUnit.SECONDS.toMillis(1))
    }

    override fun isListLocked(): Boolean {
        return isLocked
    }

    override fun lockList() {
        isLocked = true
    }

    override fun unLockList() {
        isLocked = false
    }

    override fun showProgress() {
        containerActivity.showProgress()
    }

    override fun hideProgress() {
        containerActivity.hideProgress()
    }

    override fun showError(msg: String) {
        containerActivity.showError(msg)
    }

    override fun onCategoriaClicked(
            categoria: CatalogoResponse.CategoriaEntity,
            productos: List<ServicioResponse.ServicioEntity>
    ) {
        if (productos.size == 1) {
            onSingleServicio(productos[0])
        } else {
            onServicios(categoria, productos)
        }
    }

    private fun onSingleServicio(servicio: ServicioResponse.ServicioEntity) {
        val frag = if (servicio.consultaSaldo) ServicioConsultaFragment.get(
                servicio
        ) else ServicioFragment.get(servicio)

        containerActivity.fragmentManagerLazy.commit {
            add(R.id.activity_pago_container, frag)
            hide(this@CategoriaListFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    private fun onServicios(
            categoria: CatalogoResponse.CategoriaEntity,
            servicios: List<ServicioResponse.ServicioEntity>
    ) {
        containerActivity.fragmentManagerLazy.commit {
            add(R.id.activity_pago_container, ServicioSelectionFragment.get(categoria, servicios))
            hide(this@CategoriaListFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }
}

interface View {

    fun isListLocked(): Boolean

    fun lockList()

    fun unLockList()

    fun showProgress()

    fun hideProgress()

    fun showError(msg: String)

    fun clickCategoria(entity: CatalogoResponse.CategoriaEntity)

    fun onCategoriaClicked(
            categoria: CatalogoResponse.CategoriaEntity,
            productos: List<ServicioResponse.ServicioEntity>
    )
}