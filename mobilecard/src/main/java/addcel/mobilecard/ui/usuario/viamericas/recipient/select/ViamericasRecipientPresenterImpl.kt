package addcel.mobilecard.ui.usuario.viamericas.recipient.select

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.VmCountryModel
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * ADDCEL on 29/03/17.
 */

interface ViamericasRecipientPresenter {

    fun addToDisposables(d: Disposable)

    fun clearDisposables()

    fun getRecipients(idSender: String, country: VmCountryModel)
}

class ViamericasRecipientPresenterImpl(
        private val service: ViamericasService,
        private val disposables: CompositeDisposable,
        private val view: VmRecipientView
) : ViamericasRecipientPresenter {

    override fun getRecipients(idSender: String, country: VmCountryModel) {
        view.showProgress()
        val json = "{\"idSender\":\"" + idSender + "\",\"idCountry\":\"" + country.idCountry + "\"}"
        Timber.d("getRecipients - json: %s", json)


        val recDisp =
                service.getRecipients(
                        ViamericasService.AUTH,
                        BuildConfig.ADDCEL_APP_ID,
                        StringUtil.getCurrentLanguage(),
                        json
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            view.hideProgress()
                            if (it.idError == 0) {
                                //filterNotNull()
                                view.postRecipients(it.recipients)
                            } else {
                                view.postRecipients(ArrayList())
                            }
                        }, {
                            view.hideProgress()
                            view.postRecipients(ArrayList())
                        })

        addToDisposables(recDisp)

    }

    override fun addToDisposables(d: Disposable) {
        disposables.add(d)
    }

    override fun clearDisposables() {
        disposables.clear()
    }
}
