package addcel.mobilecard.ui.usuario.perfil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.squareup.otto.Bus;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.PhotoAPI;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.domain.usuario.perfil.PerfilInteractor;
import addcel.mobilecard.domain.usuario.perfil.PerfilInteractorImpl;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import butterknife.Setter;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 06/10/17.
 */
@Module
public final class Perfil2Module {
    private final Perfil2Activity activity;

    Perfil2Module(Perfil2Activity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(activity);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(activity);
        return validator;
    }

    @PerActivity
    @Provides
    Setter<View, Boolean> provideEditRule() {
        return new Setter<View, Boolean>() {
            @Override
            public void set(@NonNull View view, Boolean value, int index) {
                if (view instanceof Button) {
                    view.setVisibility(value ? View.VISIBLE : View.GONE);
                } else {
                    view.setEnabled(value);
                }
            }
        };
    }

    @PerActivity
    @Provides
    ArrayAdapter<PaisResponse.PaisEntity> providePaisAdapter() {
        ArrayAdapter<PaisResponse.PaisEntity> adapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter.setNotifyOnChange(Boolean.TRUE);
        return adapter;
    }

    @PerActivity
    @Provides
    PhotoAPI provideApi(Retrofit r) {
        return PhotoAPI.Companion.get(r);
    }

    @PerActivity
    @Provides
    PerfilInteractor provideInteractor(UsuariosService service,
                                       PhotoAPI photoAPI, Bus bus, CatalogoService catalogoService, SessionOperations session) {
        return new PerfilInteractorImpl(service, catalogoService, photoAPI, bus, session);
    }

    @PerActivity
    @Provides
    PerfilContract.Presenter providePresenter(PerfilInteractor interactor) {
        return new PerfilPresenter(interactor, activity);
    }

    @PerActivity
    @Provides
    AlertDialog providePasswordDialog(Resources resources) {

        final View view = View.inflate(activity, R.layout.view_perfil_password, null);

        return new AlertDialog.Builder(activity).setTitle(
                resources.getString(R.string.txt_perfil_password_title))
                .setView(view)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        if (dialog1 != null) dialog1.dismiss();
                    }
                })
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }
}
