package addcel.mobilecard.ui.usuario.registro.sms

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.registro.SmsActivationEntity
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.sms.registro.SmsRegistroInteractor
import addcel.mobilecard.utils.StringUtil

/**
 * ADDCEL on 2019-07-23.
 */
interface SmsRegistroPresenter {

    fun clearDisposables()

    fun fetchModel(): SmsRegistroModel

    fun requires9Digits(): Boolean

    fun getStoredPhone(): String

    fun storePhone(phone: String)

    fun sendSms(phone: String, imei: String)

    fun validateSms(code: String, phone: String, imei: String)
}

class SmsRegistroPresenterImpl(
        val interactor: SmsRegistroInteractor, val model: SmsRegistroModel,
        val view: SmsRegistroView
) : SmsRegistroPresenter {
    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun fetchModel(): SmsRegistroModel {
        return model
    }

    override fun requires9Digits(): Boolean {
        return model.pais.id == PaisResponse.PaisEntity.PE
    }

    override fun getStoredPhone(): String {
        return interactor.fetchPhone()
    }

    override fun storePhone(phone: String) {
        interactor.storePhone(phone)
    }

    override fun sendSms(phone: String, imei: String) {
        view.showProgress()
        interactor.launchSms(BuildConfig.ADDCEL_APP_ID,
                model.pais.id,
                StringUtil.getCurrentLanguage(),
                phone,
                imei,
                object : InteractorCallback<SmsActivationEntity> {
                    override fun onSuccess(result: SmsActivationEntity) {
                        view.hideProgress()
                        when (result.code) {
                            1 -> {
                                view.onMsg(result)
                                view.showSuccess(result.message)
                            }
                            2 -> {
                                view.onPreviouslyValidated(phone)
                                view.showSuccess(result.message)
                            }
                            else -> view.showError(result.message)
                        }
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun validateSms(code: String, phone: String, imei: String) {
        view.showProgress()
        interactor.verifySms(BuildConfig.ADDCEL_APP_ID,
                model.pais.id,
                StringUtil.getCurrentLanguage(),
                code,
                phone,
                imei,
                object : InteractorCallback<SmsActivationEntity> {
                    override fun onSuccess(result: SmsActivationEntity) {
                        view.hideProgress()
                        if (result.code == 1) {
                            view.showSuccess(result.message)
                            view.launchStorage(result.data?.phoneNumber!!)
                            view.onVerification(result)
                        } else view.showError(result.message)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }
}