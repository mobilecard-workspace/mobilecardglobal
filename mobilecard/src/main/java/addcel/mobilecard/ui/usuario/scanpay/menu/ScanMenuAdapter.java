package addcel.mobilecard.ui.usuario.scanpay.menu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import addcel.mobilecard.R;

/**
 * ADDCEL on 21/08/18.
 */
public class ScanMenuAdapter extends RecyclerView.Adapter<ScanMenuAdapter.ViewHolder> {

    ScanMenuAdapter() {
    }

    ScanMenuContract.Tipo getItem(int pos) {
        return ScanMenuContract.Tipo.values()[pos];
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_menu, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ScanMenuContract.Tipo tipo = ScanMenuContract.Tipo.values()[i];
        viewHolder.icon.setImageResource(tipo.getIcon());
        viewHolder.name.setText(tipo.getStr());
    }

    @Override
    public int getItemCount() {
        return ScanMenuContract.Tipo.values().length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final ImageView icon;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.img_menu_item_icon);
            name = itemView.findViewById(R.id.text_menu_item_name);
        }
    }
}
