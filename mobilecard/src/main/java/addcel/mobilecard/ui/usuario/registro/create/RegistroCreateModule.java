package addcel.mobilecard.ui.usuario.registro.create;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.registro.RegistroActivity2;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 28/09/17.
 */
@Module
public class RegistroCreateModule {
    private final RegistroCreateFragment fragment;

    RegistroCreateModule(RegistroCreateFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    RegistroActivity2 provideActivity2() {
        return (RegistroActivity2) Preconditions.checkNotNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    RegistroCreateContract.Presenter providePresenter(
            UsuariosService service) {
        return new RegistroCreatePresenter(service, fragment);
    }
}
