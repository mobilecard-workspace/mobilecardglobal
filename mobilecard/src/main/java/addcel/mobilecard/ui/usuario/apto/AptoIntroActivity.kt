package addcel.mobilecard.ui.usuario.apto

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.accounts.AccountsAPI
import addcel.mobilecard.data.net.wallet.AptoResponse
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.apto.cards.account.AptoAccountActivity
import addcel.mobilecard.ui.usuario.apto.cards.account.AptoAccountModel
import addcel.mobilecard.ui.usuario.apto.cards.created.CardCreatedActivity
import addcel.mobilecard.ui.usuario.apto.cards.created.CardCreatedModel
import addcel.mobilecard.ui.usuario.apto.verifications.phone.capture.AptoPhoneVerificationCaptureActivity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_apto_intro.*

@Parcelize
data class AptoIntroModel(val cardEntity: CardEntity?, val info: AptoResponse?) : Parcelable

class AptoIntroActivity : AppCompatActivity(), ScreenView {

    companion object {
        fun get(context: Context, model: AptoIntroModel): Intent {
            return Intent(context, AptoIntroActivity::class.java).putExtra("model", model)
        }
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    lateinit var model: AptoIntroModel
    val api = AccountsAPI.get(BuildConfig.APTO_URL)
    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apto_intro)
        model = intent.getParcelableExtra("model")!!

        b_shift_intro_get.setOnClickListener {
            startActivity(
                    Intent(this@AptoIntroActivity, AptoPhoneVerificationCaptureActivity::class.java)
            )
        }

        if (model.info != null) {
            val aptoInfo = model.info!!

            if (aptoInfo.token.isNotEmpty()) {
                if (model.cardEntity != null) {
                    val acModel = AptoAccountModel(aptoInfo.token, model.cardEntity!!)
                    startActivity(AptoAccountActivity.get(this, acModel))
                    finish()
                } else {
                    val ccModel = CardCreatedModel(aptoInfo.token)
                    startActivity(CardCreatedActivity.get(this, ccModel))
                }
                finish()
            }
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
