package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.colombia.VrRecargaRequest
import addcel.mobilecard.data.net.colombia.VrTagDetalle
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.colombia.result.ColombiaResultFragment
import addcel.mobilecard.ui.usuario.colombia.telepeaje.menu.ColombiaTelepeajeModel
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.DeviceUtil
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_viarapida_confirm.*
import mx.mobilecard.crypto.AddcelCrypto
import java.text.NumberFormat
import javax.inject.Inject

/**
 * ADDCEL on 9/26/19.
 */

@Parcelize
data class ViarapidaConfirmModel(
        val usuario: Usuario,
        val proveedor: ColombiaTelepeajeModel, val tag: VrTagDetalle, val monto: Double,
        val colFormat: NumberFormat
) : Parcelable

interface ViarapidaConfirmView : ScreenView, Authenticable {
    fun setCardInfo()
    fun clickPagar()
    fun launchPagar()
    fun onRecarga(receipt: SPReceiptEntity)
}

class ViarapidaConfirmFragment : Fragment(), ViarapidaConfirmView {

    companion object {
        fun get(arguments: Bundle): ViarapidaConfirmFragment {
            val frag = ViarapidaConfirmFragment()
            frag.arguments = arguments
            return frag
        }
    }

    @Inject
    lateinit var presenter: ViarapidaConfirmPresenter

    private lateinit var model: ViarapidaConfirmModel
    private lateinit var card: CardEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get().netComponent.viarapidaConfirmSubcomponent(ViarapidaConfirmModule(this))
                .inject(this)

        model = arguments?.getParcelable("model")!!
        card = arguments?.getParcelable("card")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_viarapida_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b_peaje_confirm_pagar.setOnClickListener { clickPagar() }
        view_peaje_confirm_referencia.text = StringUtil.parsePlaca(model.tag.placa, "-")
        AndroidUtils.setText(
                til_peaje_confirm_saldo.editText,
                model.colFormat.format(model.tag.saldo)
        )
        AndroidUtils.setText(
                til_peaje_confirm_monto_recarga.editText,
                model.colFormat.format(model.monto)
        )
        AndroidUtils.setText(til_peaje_confirm_fee.editText, model.colFormat.format(0.0))
        AndroidUtils.setText(
                til_peaje_confirm_total.editText,
                model.colFormat.format(model.monto + 0.0)
        )
        setCardInfo()
    }

    override fun setCardInfo() {
        Picasso.get().load(card.imgShort).placeholder(R.drawable.bg_visa)
                .placeholder(R.drawable.bg_visa).fit().into(bg_scan_card, object : Callback {
                    override fun onSuccess() {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }

                    override fun onError(e: Exception) {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }
                })
    }

    override fun clickPagar() {
        if (activity != null)
            presenter.isOnWhiteList(model.usuario.ideUsuario, model.usuario.idPais)
    }

    override fun launchPagar() {

        if (activity != null) {

            val request =
                    VrRecargaRequest(
                            BuildConfig.ADDCEL_APP_ID,
                            model.usuario.idPais,
                            card.idTarjeta,
                            model.usuario.ideUsuario,
                            StringUtil.getCurrentLanguage(),
                            DeviceUtil.getDeviceId(activity!!),
                            model.monto,
                            model.tag.placa,
                            model.tag.cuenta
                    )

            presenter.recargaTag(request)
        }
    }

    override fun onRecarga(receipt: SPReceiptEntity) {
        if (activity != null) {
            (activity as ContainerActivity).fragmentManagerLazy.commit {
                add(R.id.frame_colombia_telepeaje, ColombiaResultFragment.get(receipt))
                hide(this@ViarapidaConfirmFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }
    }

    override fun showProgress() {
        (activity as ContainerActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ContainerActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ContainerActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ContainerActivity).showSuccess(msg)
    }

    override fun onWhiteList() {
        launchPagar()
    }

    override fun notOnWhiteList() {
        showError("No estás autorizado para realizar esta operacion. Contacta a soporte.")
    }
}
