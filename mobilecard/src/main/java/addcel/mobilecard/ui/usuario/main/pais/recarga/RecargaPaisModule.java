package addcel.mobilecard.ui.usuario.main.pais.recarga;

import com.google.common.base.Preconditions;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 11/12/17.
 */
@Module
public final class RecargaPaisModule {
    private final RecargaPaisFragment fragment;

    RecargaPaisModule(RecargaPaisFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MainActivity provideActivity() {
        return (MainActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    List<PaisResponse.PaisEntity> providePaises() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelableArrayList("paises");
    }

    /*
   [
          {
              "codigPais": "000502",
              "id": 1,
              "nombrePais": "Guatemala"
          },
          {
              "codigPais": "000598",
              "id": 2,
              "nombrePais": "Uruguay"
          },
          {
              "codigPais": "000057",
              "id": 3,
              "nombrePais": "Colombia"
          },
          {
              "codigPais": "000593",
              "id": 4,
              "nombrePais": "Ecuador"
          },
          {
              "codigPais": "000051",
              "id": 5,
              "nombrePais": "Peru"
          },
          {
              "codigPais": "000506",
              "id": 6,
              "nombrePais": "Costa Rica"
          },
          {
              "codigPais": "000052",
              "id": 7,
              "nombrePais": "Mexico"
          },
          {
              "codigPais": "000507",
              "id": 8,
              "nombrePais": "Panama"
          },
          {
              "codigPais": "000503",
              "id": 9,
              "nombrePais": "El Salvador"
          },
          {
              "codigPais": "000505",
              "id": 10,
              "nombrePais": "Nicaragua"
          }
      ]
   */
    @PerFragment
    @Provides
    @Named("flagMap")
    Map<Integer, Integer> provideFlagMap() {
        final Map<Integer, Integer> flagMap = new LinkedHashMap<>();
        flagMap.put(1, R.drawable.flag_guatemala);
        flagMap.put(2, R.drawable.flag_uruguay);
        flagMap.put(3, R.drawable.flag_colombia);
        flagMap.put(4, R.drawable.flag_ecuador);
        flagMap.put(5, R.drawable.flag_peru);
        flagMap.put(6, R.drawable.flag_costarica);
        flagMap.put(7, R.drawable.flag_mex);
        flagMap.put(8, R.drawable.flag_panama);
        flagMap.put(9, R.drawable.flag_salvador);
        flagMap.put(10, R.drawable.flag_nicaragua);
        flagMap.put(11, R.drawable.flag_usa);
        return flagMap;
    }

    @PerFragment
    @Provides
    RecargaPaisAdapter provideAdapter(List<PaisResponse.PaisEntity> paises,
                                      @Named("flagMap") Map<Integer, Integer> flagMap) {
        return new RecargaPaisAdapter(paises, flagMap);
    }

    @PerFragment
    @Provides
    RecargaPaisContract.Presenter providePresenter(
            CatalogoService catalogoService, SessionOperations session) {
        return new RecargaPaisPresenter(catalogoService, session.getUsuario(), fragment);
    }
}
