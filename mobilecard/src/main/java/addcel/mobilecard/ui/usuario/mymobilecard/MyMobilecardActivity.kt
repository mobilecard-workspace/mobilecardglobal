package addcel.mobilecard.ui.usuario.mymobilecard

import addcel.mobilecard.McConstants
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.usuario.main.MainActivity
import addcel.mobilecard.ui.usuario.mymobilecard.display.MyMobilecardDisplayFragment
import addcel.mobilecard.ui.usuario.mymobilecard.previvale.activate.MyMobilecardPrevivaleActivateFragment
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormActivity
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormActivity.Companion.get
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormModel
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.Fragment
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_my_mobilecard.*
import javax.inject.Inject

/*import com.shiftpayments.link.sdk.api.vos.Card;
import com.shiftpayments.link.sdk.api.vos.datapoints.DataPointList;
import com.shiftpayments.link.sdk.api.vos.responses.config.ConfigResponseVo;
import com.shiftpayments.link.sdk.ui.ShiftPlatform;
import com.shiftpayments.link.sdk.ui.storages.UIStorage;
import com.shiftpayments.link.sdk.ui.vos.ShiftSdkOptions;*/


@Parcelize
data class MyMobileCardViewModel(val cardEntity: CardEntity? = null, val restartMenu: Boolean = false) : Parcelable

class MyMobilecardActivity : ContainerActivity(), MyMobilecardContract.View {
    //@BindString(R.string.shift_environment) String environment;
// @BindString(R.string.shift_developer_key) String devKey;
// @BindString(R.string.shift_project_token) String projToken;

    @Inject
    lateinit var usuario: Usuario
    @Inject
    lateinit var catalogoService: CatalogoService
    @Inject
    lateinit var viewModel: MyMobileCardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.slide_out_right)
        Mobilecard.get().netComponent.myMobilecardSubcomponent(MyMobilecardModule(this)).inject(this)
        setContentView(R.layout.activity_my_mobilecard)
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        setSupportActionBar(toolbar_mymobilecard)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (viewModel.cardEntity != null) {
            onHasPrevivale(viewModel.cardEntity!!)
        } else {
            onNoPrevivale()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showProgress() {
        if (progress_mymobilecard.visibility == View.GONE)
            progress_mymobilecard.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        if (progress_mymobilecard.visibility == View.VISIBLE)
            progress_mymobilecard.visibility = View.GONE
    }


    override fun setAppToolbarTitle(title: Int) {
        val bar = supportActionBar
        bar?.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        val bar = supportActionBar
        if (bar != null) bar.title = title
    }

    override fun showRetry() {
        if (retry_mymobilecard.visibility == View.GONE) retry_mymobilecard.visibility = View.VISIBLE
    }

    override fun hideRetry() {
        if (retry_mymobilecard.visibility == View.VISIBLE) retry_mymobilecard.visibility = View.GONE
    }

    override fun getRetry(): View? {
        return retry_mymobilecard
    }

    override fun getCatalogo(): CatalogoService {
        return catalogoService
    }

    override fun onHasPrevivale(model: CardEntity) {
        val display: Fragment = MyMobilecardDisplayFragment.get(model)

        /* if (usuario.idPais == McConstants.PAIS_ID_MX) {
     PrevivaleUserDisplayFragment.get(model)
        } else {
     MyMobilecardDisplayFragment.get(model)
         }*/

        supportFragmentManager.beginTransaction()
                .add(R.id.frame_mymobilecard, display)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit()
    }

    override fun onNoPrevivale() {
        when (usuario.idPais) {
            McConstants.PAIS_ID_MX -> supportFragmentManager.beginTransaction()
                    .add(R.id.frame_mymobilecard, MyMobilecardPrevivaleActivateFragment())
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit()
            McConstants.PAIS_ID_USA -> {
            }
            McConstants.PAIS_ID_PE -> {
                val model1 = TebcaFormModel(
                        usuario.eMail,
                        usuario.ideUsuario,
                        usuario.usrTelefono,
                        usuario.usrNombre,
                        usuario.usrApellido,
                        usuario.usrMaterno, "",
                        "", "", "", "",
                        TipoTarjetaEntity.CREDITO)
                startActivityForResult(get(this, model1), TebcaFormActivity.REQUEST_CODE)
            }
            else -> showError("Próximamente disponible. Enero 2020")
        }
    }

    override fun onBackPressed() {
        if (viewModel.restartMenu) {
            startActivity(MainActivity.get(this, false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            finish()
        } else {
            super.onBackPressed()
        }
    }


    override fun onTebcaFormResult(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val resultCards: List<CardEntity>? = data.getParcelableArrayListExtra(TebcaFormActivity.DATA_CARD_LIST)
                if (resultCards != null) {
                    onBackPressed()
                }
            }
        }
    }

    companion object {
        @JvmStatic
        @Synchronized
        operator fun get(context: Context): Intent {
            val vm = MyMobileCardViewModel()
            return Intent(context, MyMobilecardActivity::class.java).putExtra("model", vm)
        }
        @JvmStatic
        @Synchronized
        operator fun get(context: Context, relaunchMenu: Boolean): Intent {
            val vm = MyMobileCardViewModel(restartMenu = relaunchMenu)
            return Intent(context, MyMobilecardActivity::class.java).putExtra("model", vm)
        }

        @JvmStatic
        @Synchronized
        operator fun get(context: Context, cardEntity: CardEntity): Intent {
            val vm = MyMobileCardViewModel(cardEntity = cardEntity)
            return Intent(context, MyMobilecardActivity::class.java).putExtra("model", vm)
        }
    }
}