package addcel.mobilecard.ui.usuario.mymobilecard.tebca

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.DeptoEntity
import addcel.mobilecard.data.net.catalogo.DistritoEntity
import addcel.mobilecard.data.net.catalogo.ProvinciaEntity
import addcel.mobilecard.data.net.wallet.CardResponse
import addcel.mobilecard.data.net.wallet.TebcaAltaRequest
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.wallet.tebca.TebcaFormInteractor
import addcel.mobilecard.utils.StringUtil

/**
 * ADDCEL on 2019-08-15.
 */
interface TebcaFormPresenter {
    fun clearDisposables()

    fun loadDepartamentos()

    fun loadProvincias(depto: DeptoEntity)

    fun loadDistritos(provincia: ProvinciaEntity)

    fun saveCard(body: TebcaAltaRequest)
}

class TebcaFormPresenterImpl(val interactor: TebcaFormInteractor, val view: TebcaFormView) :
        TebcaFormPresenter {

    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun loadDepartamentos() {
        view.showProgress()
        interactor.loadDepartamentos(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                object : InteractorCallback<List<DeptoEntity>> {
                    override fun onSuccess(result: List<DeptoEntity>) {
                        view.hideProgress()
                        view.onDeptosLoaded(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun loadProvincias(depto: DeptoEntity) {
        view.showProgress()
        interactor.loadProvincias(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                depto.codigo, object : InteractorCallback<List<ProvinciaEntity>> {
            override fun onSuccess(result: List<ProvinciaEntity>) {
                view.hideProgress()
                view.onProvinciasLoaded(result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
            }
        })
    }

    override fun loadDistritos(provincia: ProvinciaEntity) {
        view.showProgress()
        interactor.loadDistritos(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                provincia.codigo, object : InteractorCallback<List<DistritoEntity>> {
            override fun onSuccess(result: List<DistritoEntity>) {
                view.hideProgress()
                view.onDistritosLoaded(result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
            }
        })
    }

    override fun saveCard(body: TebcaAltaRequest) {
        view.showProgress()
        interactor.saveCard(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), body,
                object : InteractorCallback<CardResponse> {
                    override fun onSuccess(result: CardResponse) {
                        view.hideProgress()
                        if (result.idError == 0) {
                            view.showSuccess(result.mensajeError)
                            view.onCardSaved(result.tarjetas)
                        } else {
                            view.showError(result.mensajeError)
                        }
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }
}