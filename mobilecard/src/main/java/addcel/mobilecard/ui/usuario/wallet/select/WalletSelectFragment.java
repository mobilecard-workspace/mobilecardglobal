package addcel.mobilecard.ui.usuario.wallet.select;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.aci.processing.AciProcessingFragment;
import addcel.mobilecard.ui.usuario.blackstone.confirm.BSConfirmFragment;
import addcel.mobilecard.ui.usuario.colombia.multimarket.productos.compra.MmProductosConfirmFragment;
import addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.compra.MmServiciosConfirmFragment;
import addcel.mobilecard.ui.usuario.colombia.recargas.datos.confirm.ColombiaRecargaPaquetesConfirmFragment;
import addcel.mobilecard.ui.usuario.colombia.recargas.tae.confirm.ColombiaRecargaTaeConfirmFragment;
import addcel.mobilecard.ui.usuario.colombia.soad.processing.ColombiaSoadProcessingFragment;
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.confirm.ViarapidaConfirmFragment;
import addcel.mobilecard.ui.usuario.endtoend.processing.MxTransferProcessingFragment;
import addcel.mobilecard.ui.usuario.ingo.processing.IngoProcessingFragment;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.confirm.RecargaConfirmFragment;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.confirm.PeajeConfirmFragment;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.confirm.MobileTagConfirmFragment;
import addcel.mobilecard.ui.usuario.legacy.servicios.confirm.ServicioConfirmFragment;
import addcel.mobilecard.ui.usuario.scanpay.qr.confirm.ScanQrConfirmFragment;
import addcel.mobilecard.ui.usuario.scanpay.qr.id.ScanQrIdFragment;
import addcel.mobilecard.ui.usuario.viamericas.fees.bank.ViamericasCardFragment;
import addcel.mobilecard.ui.usuario.viamericas.fees.cash.ViamericasCashFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;

/**
 * ADDCEL on 13/10/17.
 */
public class WalletSelectFragment extends Fragment implements WalletSelectContract.View {

    public static final int RECARGAS_MX = 1;
    public static final int PEAJE = 2;
    public static final int SERVICIOS_MX = 3;
    public static final int SERVICIOS_US = 4;
    public static final int TRANSFER_MX = 5;
    public static final int TRANSFER_US_CARD = 6;
    public static final int TRANSFER_US_CASH = 7;
    public static final int BLACKSTONE = 9;
    public static final int INGO = 10;
    public static final int BONOS_COL = 13;
    public static final int SCANPAY_QR_MX = 14;
    public static final int SCANPAY_QR_ID_MX = 15;
    public static final int SCANPAY_MANUAL_MX = 16;
    public static final int SCANPAY_QR_PE = 17;
    public static final int RECARGAS_PAQUETES_COL = 18;
    public static final int RECARGAS_COL = 19;
    public static final int SERVICIOS_COL = 20;
    public static final int PEAJE_COL = 21;
    public static final int SOAD_COL = 22;
    public static final int PREPAGO_COL = 23; //LOL
    public static final int MOBILE_TAG = 24;

    @BindView(R.id.recycler_wallet_cards)
    RecyclerView cardsView;

    @BindView(R.id.progress_wallet_select)
    ProgressBar progressBar;

    @BindView(R.id.b_wallet_select_refresh)
    Button retryButton;

    @Inject
    WalletSelectAdapter adapter;
    @Inject
    WalletSelectContract.Presenter presenter;
    private Unbinder unbinder;

    public WalletSelectFragment() {
    }

    public static WalletSelectFragment get(int module, Bundle bundle) {
        WalletSelectFragment fragment = new WalletSelectFragment();
        bundle.putInt("module", module);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .walletSelectSubcomponent(new WalletSelectModule(this))
                .inject(this);

        if (adapter.getItemCount() == 0) {
            int module = Objects.requireNonNull(getArguments()).getInt("module");
            if (module == INGO) {
                presenter.getCards(); // presenter.getDebitCards();
            } else {
                presenter.getCards();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_wallet_cards_select, container, false);
        unbinder = ButterKnife.bind(this, view);
        cardsView.setAdapter(adapter);
        cardsView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(cardsView)
                .setOnItemClickListener((recyclerView, position, v) -> clickSelect(position));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (adapter.getItemCount() == 0) showProgress();
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(cardsView);
        cardsView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showRetry() {
        if (isVisible()) retryButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        if (isVisible()) retryButton.setVisibility(View.GONE);
    }

    @OnClick(R.id.b_wallet_select_refresh)
    @Override
    public void clickRetry() {
        hideRetry();
        int module = Objects.requireNonNull(getArguments()).getInt("module");
        if (module == INGO) {
            presenter.getCards();
        } else {
            presenter.getCards();
        }
    }

    @Override
    public void showProgress() {
        if (isVisible() && getView() != null) progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (isVisible() && getView() != null) progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String msg) {
        if (isVisible()) Toasty.error(Objects.requireNonNull(getContext()), msg).show();
    }

    @Override
    public void showError(int resId) {
        showError(getString(resId));
    }

    @Override
    public void showSuccess(String msg) {
        if (isVisible()) Toasty.success(Objects.requireNonNull(getContext()), msg).show();
    }

    @Override
    public void clickSelect(int pos) {
        adapter.setChecked(pos);
    }

    @Override
    public void onCardsUpdated(List<CardEntity> models) {
        if (getView() != null && isVisible()) {
            if (!models.isEmpty()) {
                List<WalletCardSelectModel> selectModels = Lists.newArrayList();
                for (CardEntity model : models) {
                    selectModels.add(new WalletCardSelectModel(model, model.getDeterminada()));
                }
                adapter.update(selectModels);
                cardsView.setVisibility(View.VISIBLE);
            } else {
                new Handler().postDelayed(() -> {
                    Activity act = getActivity();
                    if (act != null) act.onBackPressed();
                }, 500);
            }
        }
    }

    @OnClick(R.id.b_wallet_cards_select_continue)
    @Override
    public void goToNext() {
        try {
            int checkedPos = adapter.getCheckedCardPosition();
            if (adapter.getCheckedCardPosition() == -1) {
                showError(getString(R.string.error_wallet_card_select));
            } else {
                Objects.requireNonNull(getArguments())
                        .putParcelable("card", adapter.getItem(checkedPos).getCard());
                int module = getArguments().getInt("module");

                FragmentManager supportFragmentManager =
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager();

                switch (module) {
                    case RECARGAS_MX:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.activity_pago_container, RecargaConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case PEAJE:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.activity_pago_container, PeajeConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case SERVICIOS_MX:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.activity_pago_container, ServicioConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case SERVICIOS_US:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_aci, AciProcessingFragment.get(getArguments()), "processingACI")
                                .commit();
                        break;
                    case TRANSFER_MX:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_transfermx, MxTransferProcessingFragment.get(getArguments()),
                                        "processingH2H")
                                .commit();
                        break;
                    case BLACKSTONE:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_blackstone, BSConfirmFragment.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case INGO:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_ingo, IngoProcessingFragment.get(getArguments()), "processingIngo")
                                .commit();
                        break;
                    case RECARGAS_COL:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_colombia,
                                        ColombiaRecargaTaeConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case RECARGAS_PAQUETES_COL:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_colombia,
                                        ColombiaRecargaPaquetesConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case PEAJE_COL:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_colombia_telepeaje,
                                        ViarapidaConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case SOAD_COL:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_colombia_soad,
                                        ColombiaSoadProcessingFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .commit();
                        break;
                    case SERVICIOS_COL:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_colombia_servicios, MmServiciosConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case PREPAGO_COL:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_colombia_servicios, MmProductosConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case BONOS_COL:
                        showSuccess("BONOS COL");
                        break;
                    case SCANPAY_QR_MX:
                    case SCANPAY_QR_PE:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_scanpay, ScanQrConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case SCANPAY_QR_ID_MX:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_scanpay, ScanQrIdFragment.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case TRANSFER_US_CASH:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_viamericas, ViamericasCashFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case TRANSFER_US_CARD:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_viamericas, ViamericasCardFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case MOBILE_TAG:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_mobiletag, MobileTagConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    default:
                        break;
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
}
