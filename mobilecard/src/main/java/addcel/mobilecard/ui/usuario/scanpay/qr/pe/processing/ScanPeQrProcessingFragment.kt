package addcel.mobilecard.ui.usuario.scanpay.qr.pe.processing

import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.ScreenView
import androidx.fragment.app.Fragment

/**
 * ADDCEL on 2019-07-17.
 */

data class ScanPeQrProcessingFragmentData(val cadena: String)

interface View : ScreenView, Authenticable {
    fun launchPago(pago: SPPagoEntity)

    fun onPago(receipt: SPReceiptEntity)
}

class ScanPeQrProcessingFragment : Fragment(), View {
    override fun onWhiteList() {
    }

    override fun notOnWhiteList() {
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(msg: String) {
    }

    override fun showSuccess(msg: String) {
    }

    override fun onPago(receipt: SPReceiptEntity) {
    }

    override fun launchPago(pago: SPPagoEntity) {
    }
}