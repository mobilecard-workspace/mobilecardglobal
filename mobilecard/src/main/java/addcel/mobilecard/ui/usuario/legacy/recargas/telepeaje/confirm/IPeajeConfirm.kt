package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.confirm

import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.ScreenView
import io.reactivex.disposables.Disposable

/**
 * ADDCEL on 2020-01-15.
 */
interface PeajeConfirmView : ScreenView, Authenticable {
    fun getImei(): String

    fun updateUiOnIdPais()

    fun setSaldoActualTag(saldo: String)

    fun onErrorSaldo(msg: String)

    fun clickPagar()

    fun showError(resId: Int)
}

interface PeajeConfirmPresenter {
    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getSaldoTag()

    fun getJumioRef(): String

    fun getTag(): String

    fun getVerificador(): String

    fun getFormattedTag(): String

    fun getSaldoActual(): String

    fun getBalance(): String

    fun getCard(): CardEntity

    fun getFee(): String

    fun getTotal(): String

    fun getStartUrl(): String

    fun getSuccessUrl(): String

    fun getErrorUrl(): String

    fun getFormUrl(): String

    fun getIdPais(): Int

    fun checkWhiteList()

    fun buildPagoRequest(): ByteArray
}