package addcel.mobilecard.ui.usuario.aci.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.base.Preconditions;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.aci.AciActivity;
import addcel.mobilecard.ui.usuario.aci.pago.AciPagoFragment;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public final class AciListFragment extends Fragment implements AciListContract.View {

    @BindString(R.string.txt_menu_servicios)
    String title;

    @BindView(R.id.text_aci_search)
    EditText searchText;

    @BindView(R.id.recycler_aci)
    RecyclerView servicesRecycler;

    @Inject
    AciListAdapter adapter;
    @Inject
    AciListContract.Presenter presenter;

    private AciActivity aciActivity;
    private Unbinder unbinder;

    public AciListFragment() {
        // Required empty public constructor
    }

    public static AciListFragment get() {
        return new AciListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aciActivity = (AciActivity) Preconditions.checkNotNull(getActivity());
        aciActivity.getSubcomponent().aciServiceSubcomponent(new AciListModule(this)).inject(this);
        presenter.register();
        presenter.downloadItems();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aci_services, container, false);
        unbinder = ButterKnife.bind(this, view);
        servicesRecycler.addItemDecoration(
                new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL));
        servicesRecycler.setAdapter(adapter);
        servicesRecycler.setLayoutManager(new LinearLayoutManager(view.getContext()));
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        servicesRecycler.setItemAnimator(itemAnimator);
        ItemClickSupport.addTo(servicesRecycler)
                .setOnItemClickListener((recyclerView, position, v) -> onItemClicked(position));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        aciActivity.setAppToolbarTitle(title);
        aciActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(servicesRecycler);
        servicesRecycler.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearCompositeDisposable();
        presenter.unregister();
        super.onDestroy();
    }

    @OnTextChanged(value = R.id.text_aci_search, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void filterServicios(CharSequence sequence) {
        presenter.filterItems(sequence.toString());
    }

    @Override
    public void showProgress() {
        if (isVisible()) {
            searchText.setHint(R.string.progress);
            searchText.setEnabled(false);
            aciActivity.showProgress();
        }
    }

    @Override
    public void hideProgress() {
        if (isVisible()) {
            searchText.setHint(android.R.string.search_go);
            searchText.setEnabled(true);
            aciActivity.hideProgress();
        }
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        aciActivity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        aciActivity.showSuccess(msg);
    }

    @Override
    public void onItemsUpdated(List<ServiceModel> models) {
        adapter.update(models);
    }

    @Override
    public void onLoadFinished() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(int pos) {
        ServiceModel item = adapter.getItemAtPosition(pos);
        aciActivity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.frame_aci, AciPagoFragment.get(item))
                .hide(this)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .addToBackStack(null)
                .commit();
    }
}
