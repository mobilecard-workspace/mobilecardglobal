package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.colombia.VrRecargaRequest
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 9/30/19.
 */
interface ViarapidaConfirmPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun isOnWhiteList(idUsuario: Long, idPais: Int)

    fun recargaTag(request: VrRecargaRequest)
}

class ViarapidaConfirmPresenterImpl(
        val middleware: ColombiaMiddleware,
        val token: TokenizerAPI,
        val compositeDisposable: CompositeDisposable, val view: ViarapidaConfirmView
) :
        ViarapidaConfirmPresenter {
    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun isOnWhiteList(idUsuario: Long, idPais: Int) {
        view.showProgress()
        val wDisp = token.isOnWhiteList(
                BuildConfig.ADDCEL_APP_ID, idPais, StringUtil.getCurrentLanguage(), idUsuario
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.code == 0) view.onWhiteList() else view.notOnWhiteList()
                }, {
                    view.hideProgress()
                    view.notOnWhiteList()
                })
        addToDisposables(wDisp)
    }

    override fun recargaTag(request: VrRecargaRequest) {
        val rDisp =
                middleware.recargaTag(ColombiaMiddleware.getAuth(), request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                            view.hideProgress()
                            view.onRecarga(SPReceiptEntity(r.codigo, r.codigo, r.mensaje))
                        }, { t ->
                            view.hideProgress()
                            view.showError(t.localizedMessage
                                    ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                        })

        addToDisposables(rDisp)
    }
}