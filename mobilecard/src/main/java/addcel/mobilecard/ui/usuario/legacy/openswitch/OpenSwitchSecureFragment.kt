package addcel.mobilecard.ui.usuario.legacy.openswitch

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.common.base.Strings
import com.squareup.phrase.Phrase
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_generic_result.*
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.text.NumberFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */

@Parcelize
data class OpenSwitchSecureModel(val result: PagoResponse, val title: Int, val idPais: Int) :
        Parcelable

class OpenSwitchSecureFragment : Fragment() {
    companion object {

        private lateinit var CURR_FORMAT: NumberFormat

        private const val TEL_MX = "018009255001"
        private const val TEL_CO = "+5715800822"
        //private const val TEL_CO = "018005184868"
        private const val TEL_US = "+12134230411"
        private const val TEL_PE = "080080102"


        fun getLocale(idPais: Int): Locale {
            return when (idPais) {
                PaisResponse.PaisEntity.MX -> Locale("es", "MX")
                PaisResponse.PaisEntity.CO -> Locale("es", "CO")
                PaisResponse.PaisEntity.US -> Locale.US
                PaisResponse.PaisEntity.PE -> Locale("es", "PE")
                else -> Locale.US
            }
        }

        fun get(model: OpenSwitchSecureModel): OpenSwitchSecureFragment {
            CURR_FORMAT = NumberFormat.getCurrencyInstance(getLocale(model.idPais))

            val fragment = OpenSwitchSecureFragment()
            val bundle = Bundle()
            bundle.putParcelable("model", model)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var pcActivity: ContainerActivity
    lateinit var model: OpenSwitchSecureModel
    lateinit var permission: RxPermissions
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pcActivity = activity as ContainerActivity
        permission = RxPermissions(this)
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_generic_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pcActivity.setStreenStatus(ContainerScreenView.STATUS_FINISHED)
        pcActivity.setAppToolbarTitle(model.title)
        b_generic_result_aceptar.setOnClickListener { pcActivity.onBackPressed() }
        b_generic_result_home.setOnClickListener { pcActivity.finish() }
        /*b_pago_screenshot_share.setOnClickListener {
            disposables.add(
                permission.request(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                    .subscribe {
                        if (it) {
                            if (isVisible) {
                                val intent =
                                    ScreenshotUtil.shareScreenshot(
                                        activity,
                                        getString(R.string.txt_transaction_share)
                                    )
                                shareScreenshot(intent)
                            } else {
                                if (isVisible) Toasty.error(
                                    this.activity!!,
                                    getString(R.string.txt_ingo_permission_retry)
                                ).show()
                            }
                        }
                    }
            )
        }*/

        title_generic_result.text = getTitleSpan(model.result)
        msg_generic_result.text = buildResultMsg(model.result)
        icon_generic_result.setImageResource(getStatusIcon(model.result.code))
        view_generic_result_aclaraciones.text = buildAclaraciones(model.result.code, model.idPais)

        if (model.result.code == 0) {
            pcActivity.setStreenStatus(ContainerScreenView.STATUS_FINISHED)
            b_generic_result_aceptar.visibility = View.GONE
        } else {
            pcActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
            b_generic_result_aceptar.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }


    private fun formatDateForMsg(millis: Long): String {
        val dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault())
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
        return formatter.format(dateTime)
    }

    private fun buildResultMsg(result: PagoResponse): CharSequence {
        val res = resources
        return if (result.code == 0) {
            Phrase.from(res, R.string.txt_confirmacion_pago_msg)
                    .put("mensaje", Strings.nullToEmpty(result.message))
                    .put("idbitacora", result.idTransaccion.toString())
                    .put("auth", Strings.nullToEmpty(result.authNumber))
                    .put("tdc", Strings.nullToEmpty(result.maskedPAN))
                    .put("date", formatDateForMsg(result.dateTime))
                    .put("monto", CURR_FORMAT.format(result.amount)).format()
        } else {
            Phrase.from(res, R.string.txt_confirmacion_pago_error)
                    .put("error", result.code.toString())
                    .put("mensaje", Strings.nullToEmpty(result.message)).format()
        }
    }

    private fun getStatusIcon(code: Int): Int {
        return if (code == 0) R.drawable.ic_historial_success else R.drawable.ic_historial_error
    }

    private fun getStatusText(code: Int): String {
        return if (code == 0) "EXITOSA" else "FALLIDA"
    }

    private fun getStatusColor(status: Int): Int {
        return if (status == 0) R.color.colorAccept else R.color.colorDeny
    }

    private fun getTitleSpan(receipt: PagoResponse): CharSequence {
        val firstPart = SpannableString("OPERACIÓN ")
        val lastPart = SpannableString(getStatusText(receipt.code))

        firstPart.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.colorBlack)), 0,
                firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context!!, getStatusColor(receipt.code))), 0,
                lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, lastPart)
    }

    private fun buildAclaraciones(code: Int, idPais: Int): String {
        return if (code == 0) {
            "Servicio operado por Plataforma MobileCard. El pago quedará aplicado a tu servicio en las próximas horas.\n\n" +
                    "Para aclaraciones marque " + getPhoneByCountry(idPais)
        } else {
            "Para aclaraciones marque \n" + getPhoneByCountry(idPais)
        }
    }

    private fun getPhoneByCountry(idPais: Int): String {
        return when (idPais) {
            PaisResponse.PaisEntity.MX -> TEL_MX
            PaisResponse.PaisEntity.CO -> TEL_CO
            PaisResponse.PaisEntity.US -> TEL_US
            PaisResponse.PaisEntity.PE -> TEL_MX
            else -> TEL_US
        }
    }
}
