package addcel.mobilecard.ui.usuario.aci.confirmation;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 17/10/17.
 */
@PerFragment
@Subcomponent(modules = AciConfirmationModule.class)
public interface AciConfirmationSubcomponent {
    void inject(AciConfirmationFragment fragment);
}
