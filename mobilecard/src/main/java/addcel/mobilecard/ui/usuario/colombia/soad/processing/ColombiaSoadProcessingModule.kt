package addcel.mobilecard.ui.usuario.colombia.soad.processing

import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.di.scope.PerFragment
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 10/4/19.
 */
@Module
class ColombiaSoadProcessingModule(val fragment: ColombiaSoadProcessingFragment) {

    @PerFragment
    @Provides
    fun provideMiddleware(retrofit: Retrofit): ColombiaMiddleware {
        return ColombiaMiddleware.get(retrofit)
    }

    @PerFragment
    @Provides
    fun provideTokenizer(r: Retrofit): TokenizerAPI {
        return TokenizerAPI.provideTokenizerAPI(r)
    }


    @PerFragment
    @Provides
    fun providePresenter(
            api: ColombiaMiddleware, tokenizerAPI: TokenizerAPI
    ): ColombiaSoadProcessingPresenter {
        return ColombiaSoadProcessingPresenterImpl(
                api,
                tokenizerAPI,
                CompositeDisposable(),
                fragment
        )
    }
}

@PerFragment
@Subcomponent(modules = [ColombiaSoadProcessingModule::class])
interface ColombiaSoadProcessingSubcomponent {
    fun inject(fragment: ColombiaSoadProcessingFragment)
}