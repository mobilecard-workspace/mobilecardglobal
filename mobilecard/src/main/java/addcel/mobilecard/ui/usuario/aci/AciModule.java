package addcel.mobilecard.ui.usuario.aci;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 22/03/17.
 */
@Module
public final class AciModule {
    private final AciActivity activity;

    AciModule(AciActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    AciView provideAciView() {
        return activity;
    }
}
