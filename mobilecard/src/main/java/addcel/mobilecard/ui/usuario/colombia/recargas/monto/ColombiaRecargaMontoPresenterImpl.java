package addcel.mobilecard.ui.usuario.colombia.recargas.monto;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.colombia.entity.ColombiaRecargaCarrierEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaTiendaEntity;
import addcel.mobilecard.utils.BundleBuilder;
import timber.log.Timber;

/**
 * ADDCEL on 10/17/18.
 */
class ColombiaRecargaMontoPresenterImpl implements ColombiaRecargaMontoContract.Presenter {

    private final ColombiaTiendaEntity.RecargaEntity recarga;
    private final ColombiaRecargaCarrierEntity carrier;

    ColombiaRecargaMontoPresenterImpl(ColombiaTiendaEntity.RecargaEntity recarga,
                                      ColombiaRecargaCarrierEntity carrier) {
        this.recarga = recarga;
        this.carrier = carrier;
    }

    @Override
    public void fillContainer(ViewGroup group) {
        for (ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity c : recarga.getCampos()) {
            Timber.d("Adding campo: %s", c.getNombre());
            group.addView(buildDataTilLayout(group.getContext(), c));
        }
        group.refreshDrawableState();
    }

    @Override
    public TextInputLayout buildDataTilLayout(@NonNull Context context,
                                              ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity campo) {
        TextInputLayout layout = new TextInputLayout(context);
        TextInputEditText text = new TextInputEditText(context);
        text.setInputType(getCampoInputType(campo.getTipo()));
        text.setFilters(buildFilter(campo.getLen()));
        text.setText(campo.getValor());
        layout.addView(text);
        layout.setCounterEnabled(true);
        layout.setCounterMaxLength(campo.getLen());
        layout.setHint(campo.getNombre());
        layout.setTag(campo);
        return layout;
    }

    @Override
    public int getCampoInputType(String tipo) {
        switch (tipo) {
            case "email":
                return InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
            // email, string, number
            case "string":
                return InputType.TYPE_CLASS_TEXT;
            case "number":
                return InputType.TYPE_CLASS_PHONE;
            default:
                return InputType.TYPE_CLASS_TEXT;
        }
    }

    @Override
    public boolean evalForm(ViewGroup group) {
        for (int i = 0, size = group.getChildCount(); i < size; i++) {
            View v = group.getChildAt(i);
            if (v instanceof TextInputLayout) {
                if (Strings.isNullOrEmpty(
                        Objects.requireNonNull(((TextInputLayout) v).getEditText()).getText().toString())) {
                    ((TextInputLayout) v).setError(v.getResources().getString(R.string.error_campo_generico));
                    return Boolean.FALSE;
                }
            }
        }
        return Boolean.TRUE;
    }

    @Override
    public void clearErrorsInForm(ViewGroup group) {
        for (int i = 0, size = group.getChildCount(); i < size; i++) {
            View v = group.getChildAt(i);
            if (v instanceof TextInputLayout) {
                ((TextInputLayout) v).setError(null);
            }
        }
    }

    @Override
    public InputFilter[] buildFilter(int maxLen) {
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLen);
        return fArray;
    }

    @Override
    public int getSelectedMontoPosition(List<Integer> selectedItems) {
        return selectedItems.get(0);
    }

    @Override
    public List<ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity> getFilledCampos(
            ViewGroup group) {
        List<ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity> campos = Lists.newArrayList();
        for (int i = 0, size = group.getChildCount(); i < size; i++) {
            View v = group.getChildAt(i);
            ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity campo = getFromView(v);
            fillCampoValorFromViewData(campo, v);
            campos.add(campo);
        }
        return campos;
    }

    @Override
    public Bundle buildPagoBundle(List<ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity> campos,
                                  ColombiaRecargaCarrierEntity.MontoEntity monto) {
        return new BundleBuilder().putParcelable(ColombiaRecargaMontoContract.KEY_RECARGA, recarga)
                .putParcelable(ColombiaRecargaMontoContract.KEY_CARRIER, carrier)
                .putParcelableArrayList(ColombiaRecargaMontoContract.KEY_CAMPOS,
                        (ArrayList<? extends Parcelable>) campos)
                .putParcelable(ColombiaRecargaMontoContract.KEY_MONTO, monto)
                .build();
    }

    private ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity getFromView(View view) {
        return (ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity) view.getTag();
    }

    private void fillCampoValorFromViewData(
            ColombiaTiendaEntity.RecargaEntity.CampoRecargaEntity campo, View view) {
        if (view instanceof TextInputLayout) {
            campo.setValor(Strings.nullToEmpty(
                    Objects.requireNonNull(((TextInputLayout) view).getEditText()).getText().toString()));
        }
        if (view instanceof EditText) {
            campo.setValor(Strings.nullToEmpty(((EditText) view).getText().toString()));
        }
    }
}
