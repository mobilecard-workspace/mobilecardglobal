package addcel.mobilecard.ui.usuario.viamericas.order

import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.ui.ScreenView
import com.mobsandgeeks.saripaar.Validator

/**
 * ADDCEL on 17/05/17.
 */

interface VmOrderView : ScreenView, Validator.ValidationListener {

    fun clickInfo()

    fun postCountries(countries: List<VmCountryModel>)

    fun postCurrencies(currencies: List<CurrencyModel>)

    fun postModes(paymentModes: List<PaymentModeModel>)

    fun postBranches(locations: List<LocationModel>)

    fun postExchangeRate(exchangeRateResponse: ExchangeRateResponse)

    fun disableInitialTexts()

    fun enableTexts()

    fun disableOnCallStarted()

    fun enableOnCallFinished()
}
