package addcel.mobilecard.ui.usuario.aci.pago;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 23/03/17.
 */
@PerFragment
@Subcomponent(modules = AciPagoModule.class)
public interface AciPagoSubcomponent {
    void inject(AciPagoFragment fragment);
}
