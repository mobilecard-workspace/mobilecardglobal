package addcel.mobilecard.ui.usuario.colombia.multimarket.servicios.search

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.McConstants
import addcel.mobilecard.data.net.colombia.multimarket.Categoria
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-12-20.
 */
interface MmServicioSearchPresenter {

    fun getDisposables(): CompositeDisposable

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun getServicios(categoria: Categoria)
}

class MmServicioSearchPresenterImpl(val multimarket: MultimarketAPI, val compositeDisposable: CompositeDisposable, val view: MmServicioSearchView) : MmServicioSearchPresenter {
    override fun getDisposables(): CompositeDisposable {
        return compositeDisposable
    }

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun getServicios(categoria: Categoria) {
        view.showProgress()
        val cDisp = multimarket.getServicios(
                BuildConfig.ADDCEL_APP_ID, McConstants.PAIS_ID_CO, StringUtil.getCurrentLanguage(), categoria.nombre)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.codigo == 0) {
                        view.hideRetry()
                        if (it.searchable) view.onSearchable(it.servicios)
                        else view.onNotSearchable(it.servicios)
                    } else {
                        view.showError(it.mensaje)
                        view.showRetry()
                    }
                },
                        {
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                            view.showRetry()
                        })
        addToDisposables(cDisp)
    }

}