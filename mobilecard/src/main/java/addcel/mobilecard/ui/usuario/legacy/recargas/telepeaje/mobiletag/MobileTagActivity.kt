package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag

import addcel.mobilecard.R
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.select.MobileTagSelectFragment
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.select.MobileTagSelectModel
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_mobile_tag.*

@Parcelize
data class MobileTagModel(val idPais: Int, val emailPrellenado: String) : Parcelable

class MobileTagActivity : ContainerActivity() {

    companion object {
        fun get(context: Context, model: MobileTagModel): Intent {
            return Intent(context, MobileTagActivity::class.java).putExtra("model", model)
        }
    }

    lateinit var model: MobileTagModel

    override fun showProgress() {
        if (progress_mobiletag.visibility == View.GONE) progress_mobiletag.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        if (progress_mobiletag.visibility == View.VISIBLE) progress_mobiletag.visibility = View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_mobiletag.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_mobiletag.title = title
    }

    override fun getRetry(): View? {
        return retry_mobile_tag
    }

    override fun showRetry() {
        if (retry_mobile_tag.visibility == View.GONE) retry_mobile_tag.visibility = View.VISIBLE
    }

    override fun hideRetry() {
        if (retry_mobile_tag.visibility == View.VISIBLE) retry_mobile_tag.visibility = View.GONE
    }

    override fun onBackPressed() {
        hideRetry()
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_tag)

        model = intent.getParcelableExtra("model")!!

        fragmentManagerLazy.commit {
            add(
                    R.id.frame_mobiletag,
                    MobileTagSelectFragment.get(MobileTagSelectModel(model.idPais, model.emailPrellenado))
            )
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }
}
