package addcel.mobilecard.ui.usuario.aci.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.aci.model.ServiceModel;

/**
 * ADDCEL on 22/03/17.
 */
public final class AciListAdapter extends RecyclerView.Adapter<AciListAdapter.ViewHolder> {

    private List<ServiceModel> models;

    AciListAdapter(List<ServiceModel> models) {
        this.models = models;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_aci_servicio, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ServiceModel model = models.get(position);
        holder.nameView.setText(model.getName());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void update(List<ServiceModel> name) {
        this.models = name;
        notifyDataSetChanged();
    }

    public void clear() {
        if (models != null) {
            this.models.clear();
            notifyDataSetChanged();
        }
        models = Lists.newArrayList();
    }

    ServiceModel getItemAtPosition(int position) {
        return models.get(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameView;

        ViewHolder(View itemView) {
            super(itemView);
            nameView = itemView.findViewById(R.id.textAci);
            nameView.setTextColor(0xff424242);
        }
    }
}
