package addcel.mobilecard.ui.usuario.wallet.select.custom;

import com.google.common.collect.Lists;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerView;
import addcel.mobilecard.domain.wallet.select.WalletSelectInteractor;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ADDCEL on 24/08/18.
 */
@Module
public final class WalletSelectCustomModule {
    private final WalletSelectLayout layout;

    WalletSelectCustomModule(WalletSelectLayout layout) {
        this.layout = layout;
    }

    @PerView
    @Provides
    WalletSelectCustomAdapter provideAdapter() {
        return new WalletSelectCustomAdapter(Lists.newArrayList());
    }

    @PerView
    @Provides
    WalletSelectInteractor provideInteractor(WalletAPI api,
                                             SessionOperations session) {
        return new WalletSelectInteractor(api, session.getUsuario(), new CompositeDisposable());
    }

    @PerView
    @Provides
    WalletSelectCustomContract.Presenter providePresenter(
            WalletSelectInteractor interactor) {
        return new WalletSelectCustomPresenter(interactor, layout);
    }
}
