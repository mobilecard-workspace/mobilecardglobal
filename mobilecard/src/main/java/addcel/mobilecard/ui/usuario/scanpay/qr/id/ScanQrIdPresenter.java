package addcel.mobilecard.ui.usuario.scanpay.qr.id;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.scanpay.SPApi;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.token.TokenBaseResponse;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 28/08/18.
 */
public class ScanQrIdPresenter implements ScanQrIdContract.Presenter {
    private final ScanPayOpenInteractor interactor;
    private final ScanQrIdContract.View view;
    private Object data;

    ScanQrIdPresenter(ScanPayOpenInteractor interactor, ScanQrIdContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void clearData() {
        this.data = null;
    }

    @Override
    public Object getData() {
        return this.data;
    }

    @Override
    public void setData(Object object) {
        this.data = object;
    }

    @Override
    public boolean hasData() {
        return this.data != null;
    }

    @Override
    public SPPagoEntity buildRequest() {
        return new SPPagoEntity(0, 0, "", "", 0, "", Integer.parseInt(view.getScanPayId()),
                view.getcard().getIdTarjeta(), interactor.getIdUsuario(), "",
                interactor.getLocation().getLat(), interactor.getLocation().getLon(), "",
                Integer.parseInt(view.getMsi()), 0.0, "", "", "", "", "", "", "", "", "", "");
    }

    @Override
    public String getJumioRef() {
        return interactor.getScanReference();
    }

    @Override
    public void checkWhiteList() {
        view.showProgress();
        interactor.isOnWhiteList(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), interactor.getIdPais(SPApi.TIPO_USUARIO), new InteractorCallback<TokenBaseResponse>() {
            @Override
            public void onSuccess(@NotNull TokenBaseResponse result) {
                view.hideProgress();
                if (result.getCode() == 0) view.onWhiteList();
                else view.notOnWhiteList();
            }

            @Override
            public void onError(@NotNull String error) {
                view.hideProgress();
                view.showError(error);
            }
        });
    }

    @Override
    public void getToken(String profile) {
        view.showProgress();

        interactor.getToken(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                interactor.getIdPais(SPApi.TIPO_USUARIO), requestToString(buildRequest()), profile,
                new InteractorCallback<TokenEntity>() {
                    @Override
                    public void onSuccess(@NotNull TokenEntity result) {
                        view.hideProgress();
                        view.onTokenReceived(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void pago(TokenEntity tokenEntity, SPPagoEntity request) {
        view.showProgress();
        interactor.pagoBPById(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                tokenEntity.getToken(), tokenEntity.getAccountId(), buildRequest(),
                new InteractorCallback<SPReceiptEntity>() {
                    @Override
                    public void onSuccess(@NotNull SPReceiptEntity result) {
                        view.hideProgress();
                        view.onPagoReceived(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    private String requestToString(SPPagoEntity request) {
        return AddcelCrypto.encryptSensitive(JsonUtil.toJson(request));
    }
}
