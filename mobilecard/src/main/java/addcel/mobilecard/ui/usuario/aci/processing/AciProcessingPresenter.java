package addcel.mobilecard.ui.usuario.aci.processing;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import addcel.mobilecard.data.net.aci.AciService;
import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.CheckDataRequest;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardEntity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 16/10/17.
 */
class AciProcessingPresenter implements AciProcessingContract.Presenter {
    private final AciService service;
    private final ServiceModel serviceModel;
    private final CardEntity cardEntity;
    private final String referencia;
    private final double monto;
    private final double comision;
    private final Address address;
    private final Usuario usuario;
    private final CompositeDisposable disposables;
    private final AciProcessingContract.View view;

    AciProcessingPresenter(AciService service, ServiceModel serviceModel, CardEntity cardEntity,
                           String referencia, double monto, double comision, Address address, Usuario usuario,
                           AciProcessingContract.View view) {
        this.service = service;
        this.serviceModel = serviceModel;
        this.cardEntity = cardEntity;
        this.referencia = referencia;
        this.monto = monto;
        this.comision = comision;
        this.address = address;
        this.usuario = usuario;
        this.disposables = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @Override
    public ServiceModel getServiceModel() {
        return serviceModel;
    }

    @Override
    public CardEntity getCardEntity() {
        return cardEntity;
    }

    @Override
    public double getMonto() {
        return monto;
    }

    @Override
    public double getComision() {
        return comision;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public String getReferencia() {
        return referencia;
    }

    @SuppressLint("CheckResult")
    @Override
    public void checkData() {
        view.showProgress();
        CheckDataRequest request =
                new CheckDataRequest(usuario.getIdeUsuario(), cardEntity.getIdTarjeta());
        disposables.add(service.checkUser(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mcResponse -> {
                    view.hideProgress();
                    if (mcResponse.getIdError() == 0) {
                        view.onHasData();
                    } else {
                        view.onNoData();
                    }
                }, throwable -> {
                    view.hideProgress();
                    view.showError(Strings.nullToEmpty(throwable.getLocalizedMessage()));
                }));
    }
}
