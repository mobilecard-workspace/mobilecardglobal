package addcel.mobilecard.ui.usuario.blackstone.result;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 11/07/18.
 */
@PerFragment
@Subcomponent(modules = BSResultModule.class)
public interface BSResultSubcomponent {
    void inject(BSResultFragment fragment);
}
