package addcel.mobilecard.ui.usuario.legacy.servicios.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse
import addcel.mobilecard.data.net.pago.PagoEntity
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.data.net.token.TokenBaseResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.transacto.TransactoService
import addcel.mobilecard.data.net.transacto.entity.SaldoEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.billpocket.BillPocketInteractor
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import android.os.Build
import com.google.common.base.Strings
import mx.mobilecard.crypto.AddcelCrypto
import okio.ByteString.Companion.encodeUtf8
import java.text.NumberFormat

/**
 * ADDCEL on 19/10/17.
 */
class ServicioConfirmPresenterImpl(private val interactor: BillPocketInteractor,
                                   private val serviceModel: ServicioResponse.ServicioEntity, private val cardEntity: CardEntity, private val saldoEntity: SaldoEntity,
                                   private val numberFormat: NumberFormat, private val view: ServicioConfirmView) : ServicioConfirmPresenter {
    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun fetchIdUsuario(): Long {
        return interactor.getIdUsuario()
    }

    override fun fetchCardEntity(): CardEntity {
        return cardEntity
    }

    override fun getReferenciaName(): String {
        return serviceModel.nombreReferencia
    }

    override fun getReferenciaValue(): String {
        return serviceModel.referencia
    }

    override fun getBalance(): String {
        return formatAmount(saldoEntity.montoMxn, "MXN")
    }

    override fun getBalanceUSD(): String {
        return formatAmount(saldoEntity.montoUsd, currencyPostFix)
    }

    override fun getFee(): String {
        val fee = if (interactor.getIdPais() == PaisResponse.PaisEntity.US) saldoEntity.comisionUsd else saldoEntity.comisionMxn
        return formatAmount(fee, currencyPostFix)
    }

    override fun getTotal(): String {
        val total = if (interactor.getIdPais() == PaisResponse.PaisEntity.US) saldoEntity.totalUsd else saldoEntity.totalMxn
        return formatAmount(total, currencyPostFix)
    }

    override fun getExchangeRate(): String {
        return numberFormat.format(saldoEntity.tipoCambio)
    }

    override fun getIdPais(): Int {
        return interactor.getIdPais()
    }

    override fun getJumioReference(): String {
        return interactor.getJumioRef()
    }

    override fun checkWhiteList() {
        view.showProgress()
        interactor.checkWhiteList(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), object : InteractorCallback<TokenBaseResponse> {
            override fun onSuccess(result: TokenBaseResponse) {
                view.hideProgress()
                if (result.code == 0) {
                    view.onWhiteList()
                } else {
                    view.notOnWhiteList()
                    view.enablePurchaseButton()
                }
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
                view.enablePurchaseButton()
            }
        })
    }

    override fun getToken(profile: String) {
        val entity = buildPagoRequest()
        view.showProgress()
        interactor.getToken(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), profile,
                buildPagoRequestAsString(entity), object : InteractorCallback<TokenEntity> {
            override fun onSuccess(result: TokenEntity) {
                view.hideProgress()
                view.onTokenReceived(result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
                view.enablePurchaseButton()
            }
        })
    }

    override fun buildPagoRequest(): PagoEntity {
        val idPais = interactor.getIdPais()
        val emisor = Integer.valueOf(serviceModel.emisor)
        val monto = if (getIdPais() == PaisResponse.PaisEntity.MX) saldoEntity.montoMxn else saldoEntity.montoUsd
        val comision = if (getIdPais() == PaisResponse.PaisEntity.MX) saldoEntity.comisionMxn else saldoEntity.comisionUsd
        val software = Build.VERSION.SDK_INT.toString()
        return PagoEntity("",
                comision,
                Strings.nullToEmpty(saldoEntity.concepto),
                BuildConfig.DEBUG, emisor,
                BuildConfig.ADDCEL_APP_ID,
                idPais,
                serviceModel.emisor,
                cardEntity.idTarjeta,
                interactor.getIdUsuario(),
                view.getImei(),
                interactor.getLat(),
                interactor.getLon(),
                monto,
                serviceModel.operacion,
                saldoEntity.referencia,
                software,
                Build.MANUFACTURER)
    }

    override fun buildPagoRequestAsString(entity: PagoEntity): String {
        return AddcelCrypto.encryptSensitive(JsonUtil.toJson(entity))
    }

    override fun buildPagoRequestForBrowser(entity: PagoEntity): ByteArray {
        return ("json=" + buildPagoRequestAsString(entity)).encodeUtf8().toByteArray()
    }

    override fun executePagoBP(token: TokenEntity, entity: PagoEntity) {
        view.lockScreen()
        view.showProgress()
        interactor.pay(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), token, entity,
                object : InteractorCallback<PagoResponse> {
                    override fun onSuccess(result: PagoResponse) {
                        view.hideProgress()
                        view.unlockScreen()
                        view.onBpSuccess(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.unlockScreen()
                        view.showError(error)
                        view.enablePurchaseButton()
                    }
                })
    }

    override fun getStartUrl(accountId: String): String {
        val base = BuildConfig.BASE_URL + TransactoService.PAGO
        return base.replace("{idApp}", BuildConfig.ADDCEL_APP_ID.toString())
                .replace("{idioma}", StringUtil.getCurrentLanguage())
                .replace("{accountId}", accountId)
    }

    override fun getSuccessUrl(): String {
        return "payworks2RecRespuesta"
    }

    override fun getErrorUrl(): String {
        return "Transacto/"
    }

    override fun getFormUrl(): String {
        return "https://www.procom.prosa.com.mx/eMerchant/7627488_ADCL_Diestel.jsp"
    }

    private val currencyPostFix: String
        get() {
            when (interactor.getIdPais()) {
                PaisResponse.PaisEntity.MX -> return "MXN"
                PaisResponse.PaisEntity.CO -> return "COP"
                PaisResponse.PaisEntity.US -> return "USD"
                PaisResponse.PaisEntity.PE -> return "PEN"
            }
            return "MXN"
        }

    private fun formatAmount(amount: Double, currency: String): String {
        return String.format("%s %s", numberFormat.format(amount), currency)
    }

}