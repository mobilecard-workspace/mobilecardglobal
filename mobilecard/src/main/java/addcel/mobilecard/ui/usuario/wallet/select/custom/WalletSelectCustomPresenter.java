package addcel.mobilecard.ui.usuario.wallet.select.custom;

import android.annotation.SuppressLint;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.wallet.select.WalletSelectInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 24/08/18.
 */
public class WalletSelectCustomPresenter implements WalletSelectCustomContract.Presenter {
    private final WalletSelectInteractor interactor;
    private final WalletSelectCustomContract.View view;

    WalletSelectCustomPresenter(WalletSelectInteractor interactor,
                                WalletSelectCustomContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCards() {
        interactor.list(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<CardEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<CardEntity> result) {
                        view.setCards(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                    }
                });
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }
}
