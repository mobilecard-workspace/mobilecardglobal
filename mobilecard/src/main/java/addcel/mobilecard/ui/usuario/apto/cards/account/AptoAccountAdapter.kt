package addcel.mobilecard.ui.usuario.apto.cards.account

/**
 * ADDCEL on 2019-08-05.
 */

import addcel.mobilecard.R
import addcel.mobilecard.data.net.apto.accounts.Data
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.text.NumberFormat
import java.util.*

/** ADDCEL on 23/06/17.  */

class AptoAccountAdapter(val data: MutableList<Data>, val currFormat: NumberFormat) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private val NUM_FORMAT = NumberFormat.getCurrencyInstance(Locale.US)
        private val DATE_FORMAT = DateTimeFormatter.ofPattern("MMMM dd, h:mm a - yyyy")
    }

    fun update(data: List<Data>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun insert(pos: Int, tData: Data) {
        this.data.add(pos, tData)
        notifyItemInserted(pos)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return IViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.item_historial,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val transaction = data[position]
        (holder as IViewHolder).ticketView.text = transaction.description
        holder.totalView.text = NUM_FORMAT.format(transaction.localAmount.amount)
        holder.dateView.text = formatDate(transaction.createdAt.toLong())
    }

    private fun formatDate(timestamp: Long): String {
        val date = Instant.ofEpochSecond(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime()
        return date.format(DATE_FORMAT)

    }

    /*
       ((IViewHolder) holder).shareButton.setOnClickListener(view -> {
         Intent sharingIntent = new Intent(Intent.ACTION_SEND);
         sharingIntent.setType("text/plain");
         sharingIntent.putExtra(Intent.EXTRA_SUBJECT, view.getResources()
             .getString(R.string.txt_wallet_transaction_share_subject, Integer.parseInt(t.getId())));
         sharingIntent.putExtra(Intent.EXTRA_TEXT,
             t.getTicket() + "\n" + currFormat.format(t.getTotal()) + "\n" + t.getDate());
         view.getContext()
             .startActivity(Intent.createChooser(sharingIntent,
                 view.getContext().getString(R.string.txt_wallet_transaction_share_selection)));
       });
    */

    override fun getItemCount(): Int {
        return data.size
    }

    fun getItem(pos: Int): Any {
        return data[pos]
    }

    internal class IViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val totalView: TextView = itemView.findViewById(R.id.view_historial_monto)
        val ticketView: TextView = itemView.findViewById(R.id.view_historial_concepto)
        val dateView: TextView = itemView.findViewById(R.id.view_historial_fecha)
        val statusView: ImageView = itemView.findViewById(R.id.img_historial_status)

        fun setStatusResource(status: String) {
            var res: Int = if (status == "Complete") R.drawable.ic_historial_list_success
            else R.drawable.ic_historial_list_error
            statusView.setImageResource(res)
        }
    }

}
