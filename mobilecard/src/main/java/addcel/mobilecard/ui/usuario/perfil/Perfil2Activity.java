package addcel.mobilecard.ui.usuario.perfil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.PhotoResponseEvent;
import addcel.mobilecard.ui.usuario.perfil.photo.PerfilPhotoActivity;
import addcel.mobilecard.ui.usuario.perfil.photo.PerfilPhotoModel;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Setter;
import butterknife.ViewCollections;
import dagger.Lazy;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

public class Perfil2Activity extends AppCompatActivity implements PerfilContract.View {

    @NotEmpty(messageResId = R.string.error_nombre)
    @BindView(R.id.til_profile_nombre)
    TextInputLayout
            nombreTil;

    @NotEmpty(messageResId = R.string.error_apellido)
    @BindView(R.id.til_profile_apellido)
    TextInputLayout apellidoTil;

    @BindView(R.id.til_profile_email)
    TextInputLayout emailTil;

    @BindView(R.id.til_profile_password)
    TextInputLayout passwordTil;

    @BindView(R.id.spinner_profile_pais)
    Spinner paisSpinner;

    @NotEmpty(messageResId = R.string.error_celular)
    @BindView(R.id.til_profile_celular)
    TextInputLayout celularTil;

    @BindView(R.id.progress_profile)
    ProgressBar progressBar;

    @BindViews({
            R.id.til_profile_nombre, R.id.til_profile_apellido, R.id.spinner_profile_pais,
            R.id.b_profile_password, R.id.b_profile_save
    })
    List<View> modifiableView;

    @BindView(R.id.b_profile_camera)
    CircleImageView profileImgButton;

    @BindView(R.id.screen_retry)
    ConstraintLayout retryScreen;

    @Inject
    Validator validator;
    @Inject
    Bus bus;
    @Inject
    Setter<View, Boolean> updateViewsWhenClickingEditing;
    @Inject
    PerfilContract.Presenter presenter;
    @Inject
    ArrayAdapter<PaisResponse.PaisEntity> adapter;
    @Inject
    Lazy<AlertDialog> passwordDialog;

    public static synchronized Intent get(Context context) {
        return new Intent(context, Perfil2Activity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().perfil2Subcomponent(new Perfil2Module(this)).inject(this);
        setContentView(R.layout.activity_perfil2);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_perfil);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Button pgOlvidarButton = findViewById(R.id.b_profile_password);
        pgOlvidarButton.setPaintFlags(pgOlvidarButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Button pgEditarButton = findViewById(R.id.b_profile_edit);
        pgEditarButton.setPaintFlags(pgEditarButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Button retryButton = retryScreen.findViewById(R.id.b_retry);
        retryButton.setOnClickListener(v -> presenter.getPaises(true));

        emailTil.setEnabled(false);
        passwordTil.setEnabled(false);
        celularTil.setEnabled(false);
        paisSpinner.setEnabled(false);
        setTextValues();
        paisSpinner.setAdapter(adapter);
        if (adapter.getCount() == 0) presenter.getPaises(false);
        ViewCollections.set(modifiableView, updateViewsWhenClickingEditing, false);
        presenter.loadImage();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
        passwordDialog.get().setOnShowListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        passwordDialog.get().setOnShowListener(null);
        bus.unregister(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    private void setTextValues() {
        AndroidUtils.setText(nombreTil.getEditText(), presenter.getNombre());
        AndroidUtils.setText(apellidoTil.getEditText(), presenter.getApellido());
        AndroidUtils.setText(emailTil.getEditText(), presenter.getEmail());
        AndroidUtils.setText(passwordTil.getEditText(), presenter.getPassword());
        AndroidUtils.setText(celularTil.getEditText(), presenter.getCelular());
    }

    @Override
    public String getNombre() {
        return AndroidUtils.getText(nombreTil.getEditText());
    }

    @Override
    public String getApellido() {
        return AndroidUtils.getText(apellidoTil.getEditText());
    }

    @Override
    public String getEmail() {
        return AndroidUtils.getText(emailTil.getEditText());
    }

    @Override
    public int getPais() {
        return AndroidUtils.getSelectedItem(paisSpinner, PaisResponse.PaisEntity.class).getId();
    }

    @Override
    public void setPais(int idPais) {
        for (int i = 0; i < adapter.getCount(); i++) {
            PaisResponse.PaisEntity p = Preconditions.checkNotNull(adapter.getItem(i));
            if (p.getId() == idPais) paisSpinner.setSelection(i);
        }
    }

    @Override
    public String getCelular() {
        return AndroidUtils.getText(celularTil.getEditText());
    }

    @OnClick(R.id.b_profile_camera)
    @Override
    public void clickCamera() {
        startActivityForResult(
                PerfilPhotoActivity.Companion.get(this, new PerfilPhotoModel(presenter.getIdUsuario())),
                PerfilPhotoActivity.REQUEST_CODE);
    }

    @OnClick(R.id.b_profile_edit)
    @Override
    public void clickEdit() {
        passwordTil.setVisibility(View.GONE);
        ViewCollections.set(modifiableView, updateViewsWhenClickingEditing, true);
        if (paisSpinner.getAdapter().isEmpty()) presenter.getPaises(true);
    }

    @OnClick(R.id.b_profile_password)
    @Override
    public void clickPassword() {
        if (!passwordDialog.get().isShowing()) passwordDialog.get().show();
    }

    @OnClick(R.id.b_profile_save)
    @Override
    public void clickSave() {
        validator.validate();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(int resId) {
        onError(getString(resId));
    }

    @Override
    public void onError(String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void onSuccess(String msg) {
        Toasty.success(this, msg).show();
    }

    @Override
    public void notifyUpdate() {
        AndroidUtils.setText(nombreTil.getEditText(), presenter.getNombre());
        AndroidUtils.setText(apellidoTil.getEditText(), presenter.getApellido());
        setPais(presenter.getPais());
        AndroidUtils.setText(celularTil.getEditText(), presenter.getCelular());
        ViewCollections.set(modifiableView, updateViewsWhenClickingEditing, false);
        passwordTil.setVisibility(View.VISIBLE);
    }

    @Override
    public void notifyPasswordUpdate() {
        ViewCollections.set(modifiableView, updateViewsWhenClickingEditing, false);
        AndroidUtils.setText(passwordTil.getEditText(), presenter.getPassword());
        passwordTil.setVisibility(View.VISIBLE);
    }

    @Subscribe
    @Override
    public void onImageLoaded(Bitmap bitmap) {
        hideProgress();

        try {
            profileImgButton.setImageBitmap(bitmap);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Subscribe
    @Override
    public void updatePhoto(PhotoResponseEvent event) {
        hideProgress();

        if (event.getData().getIdError() == 0) {
            onSuccess(event.getData().getMensajeError());
            presenter.loadImage();
        } else {
            onError(event.getData().getMensajeError());
            profileImgButton.setImageDrawable(null);
        }
    }

    @Override
    public void notifyPaises(List<PaisResponse.PaisEntity> paises) {
        if (paises.size() > 0 && retryScreen.getVisibility() == View.VISIBLE) {
            retryScreen.setVisibility(View.GONE);
            paisSpinner.setEnabled(true);
        }

        adapter.addAll(paises);

        if (adapter.isEmpty()) {
            paisSpinner.setEnabled(false);
        } else {
            setPais(presenter.getPais());
        }
    }

    @Override
    public void notifyPaisesError(String msg) {
        adapter.clear();
        paisSpinner.setEnabled(false);
        retryScreen.setVisibility(View.VISIBLE);
        onError(msg);
    }

    @Override
    public void onValidationSucceeded() {
        clearErrors();
        presenter.save();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationUtils.onValidationFailed(this, errors);
    }

    private void clearErrors() {
        ValidationUtils.clearViewErrors(nombreTil, apellidoTil);
    }

    @Override
    public void onShow(DialogInterface dialog) {
        final TextInputLayout lyPass = ((AlertDialog) dialog).findViewById(R.id.til_perfil_password);
        final TextInputLayout lyConfPass =
                ((AlertDialog) dialog).findViewById(R.id.til_perfile_password_conf);
        final Button updateButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);

        lyPass.setError(null);
        lyConfPass.setError(null);
        AndroidUtils.setText(lyPass.getEditText(), "");
        AndroidUtils.setText(lyConfPass.getEditText(), "");

        updateButton.setOnClickListener(v -> {
            if (AndroidUtils.validatePasswordWindow(lyPass, lyConfPass, R.string.error_password,
                    R.string.error_conf_password)) {
                presenter.updatePassword(AndroidUtils.getText(lyPass.getEditText()));
                lyPass.setError(null);
                lyConfPass.setError(null);
                AndroidUtils.setText(lyPass.getEditText(), "");
                AndroidUtils.setText(lyConfPass.getEditText(), "");
                dialog.dismiss();
            }
        });
    }

    private void setPic(ImageView view, String filePath) {
        // Get the dimensions of the View
        int targetW = view.getWidth();
        int targetH = view.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);

        view.setImageBitmap(bitmap);

        presenter.updateProfile(filePath);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PerfilPhotoActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    setPic(profileImgButton, data.getStringExtra(PerfilPhotoActivity.DATA_PATH));
                }
            }
        }
    }
}
