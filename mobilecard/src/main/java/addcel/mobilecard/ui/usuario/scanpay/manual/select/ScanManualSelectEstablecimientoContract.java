package addcel.mobilecard.ui.usuario.scanpay.manual.select;

import androidx.annotation.StringRes;

import java.util.List;

import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoDownloadEvent;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoErrorEvent;
import addcel.mobilecard.domain.scanpay.manual.select.ScanManualSelectEstablecimientoFilterEvent;

/**
 * ADDCEL on 22/08/18.
 */
public interface ScanManualSelectEstablecimientoContract {
    interface View {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void onEstablecimientosDownloaded();

        void onEstablecimientosError(@StringRes int errorRes);

        void filter(CharSequence pattern);

        void setEstablecimientos(List<LcpfEstablecimiento> models); //TODO definir API Scan&Pay
    }

    interface Presenter {

        void clearCompositeDisposable();

        void register();

        void unregister();

        void downloadEstablecimientos();

        void onEstablecimientosDownloaded(ScanManualSelectEstablecimientoDownloadEvent event);

        void onEstablecimientosError(ScanManualSelectEstablecimientoErrorEvent event);

        void filterList(String pattern);

        void onEstablecimientosFiltered(ScanManualSelectEstablecimientoFilterEvent event);
    }
}
