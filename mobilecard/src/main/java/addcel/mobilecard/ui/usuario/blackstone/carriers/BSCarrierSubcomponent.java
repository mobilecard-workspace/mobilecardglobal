package addcel.mobilecard.ui.usuario.blackstone.carriers;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 09/07/18.
 */
@PerFragment
@Subcomponent(modules = BSCarrierModule.class)
public interface BSCarrierSubcomponent {
    void inject(BSCarrierFragment fragment);
}
