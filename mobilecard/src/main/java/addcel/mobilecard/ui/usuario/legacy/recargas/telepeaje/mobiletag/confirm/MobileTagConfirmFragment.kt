package addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.mobiletag.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.pago.PagoEntity
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.*
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureActivity
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureCallback
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureModel
import addcel.mobilecard.ui.usuario.legacy.openswitch.OpenSwitchSecureFragment
import addcel.mobilecard.ui.usuario.legacy.openswitch.OpenSwitchSecureModel
import addcel.mobilecard.utils.DeviceUtil
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_scanpay_qr_confirm.*
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber
import javax.inject.Inject

/**
 * ADDCEL on 31/10/19.
 */

@Parcelize
data class MobileTagConfirmModel(
        val usuario: String,
        val monto: CatalogoResponse.MontoEntity
) : Parcelable


interface MobileTagConfirmView : PurchaseScreenView, BillPocketSecureCallback {

    fun setInfo()

    fun setCardInfo()

    fun launchToken(transactionRef: String)

    fun onToken(token: TokenEntity)

    fun launchPagoSecure(token: TokenEntity, request: PagoEntity)

    fun launchPagoOpen(token: TokenEntity, request: PagoEntity)

    fun onPagoSuccess(pagoResponse: PagoResponse)
}

class MobileTagConfirmFragment : Fragment(), MobileTagConfirmView {


    companion object {
        fun get(arguments: Bundle): MobileTagConfirmFragment {
            val frag = MobileTagConfirmFragment()
            frag.arguments = arguments
            return frag
        }
    }

    @Inject
    lateinit var containerActivity: ContainerActivity
    @Inject
    lateinit var model: MobileTagConfirmModel
    @Inject
    lateinit var card: CardEntity
    @Inject
    lateinit var presenter: MobileTagConfirmPresenter
    @Inject
    lateinit var picasso: Picasso

    private lateinit var locationAndImeiPermissions: RxPermissions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.mobileTagConfirmSubcomponent(MobileTagConfirmModule(this))
                .inject(this)
        locationAndImeiPermissions = RxPermissions(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_scanpay_qr_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.addToDisposables(
                locationAndImeiPermissions.request(
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ).subscribe()
        )

        setInfo()
        setCardInfo()

        b_scanpay_qr_confirm_continuar.setOnClickListener {
            clickPurchaseButton()
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) enablePurchaseButton()
    }

    override fun setInfo() {
        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)

        val idPais = presenter.fetchUsuario().idPais

        val invoice = getString(
                R.string.txt_mobiletag_confirm_invoice,
                MobileTagConfirmPresenter.formatAmount(model.monto.monto, idPais),
                MobileTagConfirmPresenter.formatAmount(model.monto.comision, idPais),
                model.monto.concepto
        )

        view_scanpay_qr_confirm_data.text = invoice

        view_scanpay_qr_confirm_total.text = String.format(
                "Total: %s",
                MobileTagConfirmPresenter.formatAmount(
                        model.monto.monto + model.monto.comision,
                        idPais
                )
        )
    }

    override fun setCardInfo() {
        picasso.load(card.imgShort)
                .placeholder(R.drawable.bg_visa)
                .placeholder(R.drawable.bg_visa)
                .fit()
                .into(bg_scan_card, object : Callback {
                    override fun onSuccess() {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }

                    override fun onError(e: Exception) {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }
                })
    }

    override fun onWhiteList() {
        launchToken("")
    }

    override fun notOnWhiteList() {
        showError("No está autorizado para realizar esta operacion. Contacte a soporte.")
    }

    override fun launchToken(transactionRef: String) {

        val request = PagoEntity(
                cargo = model.monto.monto,
                comision = model.monto.comision,
                concepto = model.monto.concepto,
                idCard = card.idTarjeta,
                idUser = presenter.fetchUsuario().ideUsuario,
                imei = DeviceUtil.getDeviceId(containerActivity),
                lat = presenter.fetchLocation().lat,
                lon = presenter.fetchLocation().lon,
                modelo = Build.MODEL,
                referencia = model.usuario,
                software = Build.VERSION.SDK_INT.toString(),
                debug = BuildConfig.DEBUG,
                emisor = model.monto.emisor.toInt(),
                idAplicacion = BuildConfig.ADDCEL_APP_ID,
                idProveedor = model.monto.emisor,
                operacion = model.monto.operacion,
                idPais = presenter.fetchUsuario().idPais
        )


        val requestStr = JsonUtil.toJson(request)

        Timber.d("Objeto petición token: %s", requestStr)

        presenter.getToken(AddcelCrypto.encryptSensitive(requestStr), transactionRef)
    }

    override fun onToken(token: TokenEntity) {

        val request = PagoEntity(
                cargo = model.monto.monto,
                comision = model.monto.comision,
                concepto = model.monto.concepto,
                idCard = card.idTarjeta,
                idUser = presenter.fetchUsuario().ideUsuario,
                imei = DeviceUtil.getDeviceId(containerActivity),
                lat = presenter.fetchLocation().lat,
                lon = presenter.fetchLocation().lon,
                modelo = Build.MODEL,
                referencia = model.usuario,
                software = Build.VERSION.SDK_INT.toString(),
                debug = BuildConfig.DEBUG,
                emisor = model.monto.emisor.toInt(),
                idAplicacion = BuildConfig.ADDCEL_APP_ID,
                idProveedor = model.monto.emisor,
                operacion = model.monto.operacion,
                idPais = presenter.fetchUsuario().idPais
        )

        if (token.secure) {
            launchPagoSecure(token, request)
        } else {
            launchPagoOpen(token, request)
        }
    }

    override fun launchPagoSecure(token: TokenEntity, request: PagoEntity) {
        enablePurchaseButton()

        val model = BillPocketSecureModel(
                presenter.fetchUsuario().ideUsuario, card, token,
                AddcelCrypto.encryptSensitive(JsonUtil.toJson(request))
        )

        try {
            startActivityForResult(
                    BillPocketSecureActivity.get(containerActivity, model),
                    BillPocketSecureActivity.REQUEST_CODE
            )
        } catch (t: Throwable) {
            t.printStackTrace()
        }

    }

    override fun launchPagoOpen(token: TokenEntity, request: PagoEntity) {
        presenter.pagar(token, request)
    }

    override fun onPagoSuccess(pagoResponse: PagoResponse) {
        containerActivity.fragmentManagerLazy
                .beginTransaction()
                .add(
                        R.id.frame_mobiletag,
                        OpenSwitchSecureFragment.get(
                                OpenSwitchSecureModel(
                                        pagoResponse,
                                        R.string.nav_mobiletag,
                                        presenter.fetchUsuario().idPais
                                )
                        )
                )
                .hide(this)
                .addToBackStack(null)
                .commit()

        if (isVisible) enablePurchaseButton()
    }

    override fun isPurchaseButtonEnabled(): Boolean {
        return b_scanpay_qr_confirm_continuar.isEnabled
    }

    override fun disablePurchaseButton() {
        b_scanpay_qr_confirm_continuar.isEnabled = false
    }

    override fun enablePurchaseButton() {
        b_scanpay_qr_confirm_continuar.isEnabled = true
    }

    override fun clickPurchaseButton() {
        if (isPurchaseButtonEnabled()) {
            disablePurchaseButton()
            presenter.checkWhitelist()
        }
    }

    override fun showProgress() {
        containerActivity.showProgress()
    }

    override fun hideProgress() {
        containerActivity.hideProgress()
    }

    override fun showError(msg: String) {
        containerActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerActivity.showSuccess(msg)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BillPocketSecureActivity.REQUEST_CODE) {
            onBillPocketSecureResult(resultCode, data)
        }
    }

    override fun onBillPocketSecureResult(resultCode: Int, data: Intent?) {
        enablePurchaseButton()
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val resultData = data.getStringExtra(BillPocketSecureActivity.RESULT_DATA)
                if (JsonUtil.isJson(resultData)) {
                    val response = JsonUtil.fromJson(resultData, PagoResponse::class.java)
                    onPagoSuccess(response)
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            showError("Pago cancelado por el usuario.")
        }
    }

}