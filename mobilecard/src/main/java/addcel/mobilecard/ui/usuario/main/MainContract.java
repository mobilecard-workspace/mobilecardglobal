package addcel.mobilecard.ui.usuario.main;

import android.Manifest;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.ingo.IngoAliveResponse;
import addcel.mobilecard.data.net.usuarios.model.UserValidation;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.usuario.menu.MenuValidationEvent;
import addcel.mobilecard.ui.usuario.menu.BottomMenuEventListener;
import addcel.mobilecard.ui.usuario.mymobilecard.routing.MyMcRoutingCallback;
import addcel.mobilecard.ui.usuario.mymobilecard.tebca.TebcaFormCallback;
import io.reactivex.annotations.NonNull;

/**
 * ADDCEL on 05/07/18.
 */
public interface MainContract {

    String[] MAIN_PERMISSIONS = {
            Manifest.permission.READ_CONTACTS, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.CHANGE_WIFI_STATE
    };

    interface UseCase {
        int PROFILE = 1;
        int INICIO = 2;
        int RECARGAS = 3;
        int SERVICIOS = 4;
        int WALLET = 5;
        int EDOCUENTA = 6;
        int CHEQUE = 7;
        int TRANSFERENCIAS = 8;
        int FRECUENTES = 9;
        int SCAN = 10;
        int CONTACTO = 11;
        int LOGOUT = 12;
        int DIVIDER = 13;
        int VERSION = 14;
        int MY_MC = 15;
        int COL_PEAJE = 16;
        int COL_SOAT = 17;
        int COL_TAE = 18;
        int COL_PREPAGOS = 19; //LOL
        int COL_SERVICIOS = 20;
        int MOBILE_TAG = 21;
    }

    interface View extends BottomMenuEventListener, MyMcRoutingCallback, TebcaFormCallback {

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);

        void showProgress();

        void hideProgress();

        void setSelection(long view);

        addcel.mobilecard.domain.usuario.bottom.UseCase getBottomMenuUseCase();

        void setBottomMenuUseCase(addcel.mobilecard.domain.usuario.bottom.UseCase useCase);

        FragmentManager getFragmentManagerFromActivity();

        void goTo(int view);

        void setMexicoDrawer();

        void setUsaDrawer(boolean hasIngo);

        void setColombiaDrawer();

        void setPeruDrawer();

        void setNameOnDrawer(@NonNull String name);

        void showLogouDialog();

        void showLocationDialog();

        void showIngoDialog(int idPais, long idUsuario, String msg);

        void showResendDialog();

        void hideResendDialog();

        void showActivationDialog(String msg);

        void setToolbarTitle(int titleResource);

        void checkLocationSettings(int view);

        void goToLocationSettings();

        void logout();

        void startFragmentFromMain(int clave, int title);

        void launchLocationService();

        void launchSmsactivity(long idUsuario, int idPais);

        void launchProfileActivity();

        void launchContainerActivityRecargasMX(List<CatalogoResponse.RecargaEntity> recargas);

        void launchContainerActivityServicios(List<CatalogoResponse.CategoriaEntity> categorias);

        void launchContainerActivityRecargasUS(List<PaisResponse.PaisEntity> paises);

        void launchContactoActivity(long idUsuario, String email, int idPais);

        void launchWalletActivity();

        void launchEdoCuentaActivity();

        void launchIngoActivity();

        void launchVmActivity();

        void launchHTHActivity();

        void launchMobileTagActivity();

        void launchScanPayActivity();

        void launchColombiaActivity();

        void launchColombiaTelepeaje();

        void launchColombiaSoad();

        void launchColombiaServicios(int useCase);

        void launchMyMcActivity();

        Toolbar getMainToolbar();

        void showOnboardingErrorDialog(long idUsuario);

        void launchOnBoardingInfo(long idUsuario);
    }

    interface Presenter {

        void clearDisposables();

        String getPaisFormatJUMIO(int idPais);

        boolean isMenuLaunched();

        void setMenuLaunched(boolean launched);

        void onRestart();

        void loadProfile();

        void launchOnLocationChecked(int useCase, boolean locationEnabled);

        void checkIfIngoAlive();

        void checkIfIngoAlive(InteractorCallback<IngoAliveResponse> interactorCallback);

        Usuario getUsuario();

        long getIdUsuario();

        String getUsuarioName() throws NullPointerException;

        String getUsuarioApellidos() throws NullPointerException;

        String getUsuarioProfile();

        String getEmail();

        int getIdPais();

        void verifyAndLaunchUseCase(int useCase);

        void setDrawerByCountry();

        void onLogged(int useCase);

        void onEmailAuthRequired(UserValidation validation);

        void resendActivationLink();

        void onSmsAuthRequired(UserValidation validation);

        void evalJumioUser(MenuValidationEvent validation);

        void evalUser(MenuValidationEvent validationEvent);

        void getRecargas();

        void getServicios();

        void updateReference(String reference);

        void logout();

        void onLogout();

        void processMenuEvent(UserValidation result,
                              addcel.mobilecard.domain.usuario.bottom.UseCase useCase);

        void evalJumioUserBottom(UserValidation result,
                                 addcel.mobilecard.domain.usuario.bottom.UseCase useCase);

        void updateToken(String token);

        void deleteToken(String token);

    }
}
