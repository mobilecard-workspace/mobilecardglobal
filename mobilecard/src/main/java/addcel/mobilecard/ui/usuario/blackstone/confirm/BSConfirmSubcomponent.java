package addcel.mobilecard.ui.usuario.blackstone.confirm;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 10/07/18.
 */
@PerFragment
@Subcomponent(modules = BSConfirmModule.class)
public interface BSConfirmSubcomponent {
    void inject(BSConfirmFragment fragment);
}
