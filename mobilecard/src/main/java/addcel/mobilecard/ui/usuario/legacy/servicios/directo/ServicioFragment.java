package addcel.mobilecard.ui.usuario.legacy.servicios.directo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.McConstants;
import addcel.mobilecard.McRegexPatterns;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.data.net.transacto.entity.SaldoEntity;
import addcel.mobilecard.ui.custom.view.CheckableButton;
import addcel.mobilecard.ui.custom.view.CheckableImageButton;
import addcel.mobilecard.ui.scanner.NewScannerActivity;
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.servicios.HideTutorialEvent;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment;
import addcel.mobilecard.utils.ActivityResultEvent;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.utils.MapUtils;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import dagger.Lazy;
import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * ADDCEL on 22/11/16.
 */
public final class ServicioFragment extends Fragment implements ServicioContract.View {
    private final CompositeDisposable uiDisposable = new CompositeDisposable();
    @BindView(R.id.view_servicio_header)
    ImageView header;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    @BindView(R.id.til_servicio_numerorecibo)
    TextInputLayout labelReciboTil;
    @BindView(R.id.img_recibo)
    ImageView reciboView;
    @BindView(R.id.text_servicio_numerorecibo)
    TextInputEditText referenciaText;
    @BindView(R.id.b_servicio_escanear)
    Button escanearButton;
    @BindView(R.id.b_float_servicio_scan)
    FloatingActionButton scanFloatButton;
    @BindView(R.id.b_servicio_fav)
    CheckableImageButton favButton;
    @BindView(R.id.til_servicio_favorito)
    TextInputLayout favTil;
    @BindView(R.id.view_servicio_favorito)
    View favView;
    @NotEmpty(messageResId = R.string.error_cantidad)
    @BindView(R.id.til_servicio_otracantidad)
    TextInputLayout cantidadTil;
    @BindView(R.id.text_servicio_otracantidad)
    TextInputEditText cantidadText;
    @Checked(messageResId = R.string.error_servicio_directo)
    @BindView(R.id.b_servicio_pagar)
    CheckableButton pagarButton;
    @Inject
    PagoContainerActivity activity;
    @Inject
    @Named("colorLogoMap")
    Map<Integer, Integer> headerMap;
    @Inject
    @Named("iconMap")
    Map<Integer, Integer> whiteMap;
    @Inject
    boolean fav;
    @Inject
    ServicioResponse.ServicioEntity servicio;
    @Inject
    Validator validator;
    @Inject
    ServicioContract.Presenter presenter;
    @Inject
    Bus bus;

    private AlertDialog infoDialog;
    private Unbinder unbinder;

    public ServicioFragment() {
    }

    public static ServicioFragment get(ServicioResponse.ServicioEntity servicio) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", servicio);
        ServicioFragment fragment = new ServicioFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static ServicioFragment get(ServicioResponse.ServicioEntity servicio, boolean fav) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", servicio);
        bundle.putBoolean("fav", fav);
        ServicioFragment fragment = new ServicioFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().servicioSubcomponent(new ServicioModule(this)).inject(this);
        bus.register(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_servicio, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        infoDialog = AndroidUtils.createInformationDialog(activity, android.R.string.dialog_alert_title, R.string.info_servicios_1, android.R.string.ok);

        activity.setStreenStatus(PagoContainerActivity.STATUS_NORMAL);
        activity.setAppToolbarTitle(servicio.getEmpresa());
        header.setImageResource(
                MapUtils.getOrDefault(headerMap, servicio.getId(), R.drawable.logo_cfe));
        labelReciboTil.setHint(servicio.getNombreReferencia());
        escanearButton.setText(Phrase.from(view, R.string.txt_servicio_scan)
                .put("referencia", Strings.nullToEmpty(servicio.getNombreReferencia()))
                .format());
        clear();
        setInitialFav(fav);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) activity.setAppToolbarTitle(servicio.getEmpresa());
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void onValidationSucceeded() {
        presenter.getTipoCambio(servicio,
                Objects.requireNonNull(referenciaText.getText()).toString().trim(),
                Double.parseDouble(Objects.requireNonNull(cantidadText.getText()).toString().trim()));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(activity);
    }

    @OnClick({R.id.b_servicio_escanear, R.id.b_float_servicio_scan})
    @Override
    public void scanReferencia() {
        RxPermissions permissions = new RxPermissions(this);
        uiDisposable.add(permissions.request(Manifest.permission.CAMERA).subscribe(
                aBoolean -> {
                    if (aBoolean) {
                        if (getContext() != null)
                            NewScannerActivity.Companion.startForResult(ServicioFragment.this);
                    } else {
                        Toasty.info(activity, "MobileCard requiere autorización para utilizar la cámara del dispositivo").show();
                    }
                }
                , throwable -> activity.showError(Objects.requireNonNull(throwable.getLocalizedMessage()))));
    }

    @OnTextChanged(value = R.id.text_servicio_numerorecibo, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void cambiaReferencia(CharSequence seq) {
        if (Strings.isNullOrEmpty(seq.toString())) {
            pagarButton.setChecked(false);
        } else {
            if (!Strings.isNullOrEmpty(Objects.requireNonNull(cantidadText.getText()).toString())
                    && TextUtils.isDigitsOnly(cantidadText.getText())) {
                pagarButton.setChecked(true);
            }
        }
    }

    @Override
    public void postReferencia(ActivityResultEvent event) {
        Timber.d("result scan: %s", event.toString());
        if (NewScannerActivity.REQUEST_CODE == event.getRequestCode()) {
            if (Activity.RESULT_OK == event.getResultCode()) {
                final String referencia = event.getData().getStringExtra(NewScannerActivity.RESULT_DATA);
                if (!TextUtils.isEmpty(referencia)) {
                    referenciaText.setError(null);
                    referenciaText.clearFocus();
                    referenciaText.setText(referencia);
                }
            }
        }
    }

    @OnClick(R.id.b_servicio_help)
    @Override
    public void showReciboImage() {
        if (infoDialog != null) infoDialog.show();
    }

    @OnClick(R.id.b_servicio_fav)
    @Override
    public void clickFav() {
        setFav(!favButton.isChecked());
    }

    @OnClick(R.id.b_servicio_fav_add)
    @Override
    public void clickSaveFav() {
        if (!Strings.isNullOrEmpty(Objects.requireNonNull(favTil.getEditText()).getText().toString())) {
            favTil.setError(null);
            presenter.saveToFav(servicio,
                    MapUtils.getOrDefault(whiteMap, servicio.getId(), R.drawable.servicios),
                    favTil.getEditText().getText().toString().trim());
        } else {
            favTil.setError(getText(R.string.error_default));
        }
    }

    @Override
    public void updateFavViewsOnSuccess() {
        favButton.setEnabled(false);
        favView.setVisibility(View.GONE);
    }

    @Override
    public void setFav(boolean fav) {
        favButton.setChecked(fav);
        new Handler().postDelayed(() -> favView.setVisibility(fav ? View.VISIBLE : View.GONE), 250);
    }

    @Override
    public void setInitialFav(boolean fav) {
        favButton.setChecked(fav);
        favButton.setEnabled(!fav);
        if (!fav) favButton.requestFocus();
    }

    @Subscribe
    @Override
    public void hideReciboImage(HideTutorialEvent event) {
        if (reciboView.getVisibility() == View.VISIBLE) {
            new Handler().postDelayed(() -> {
                reciboView.setVisibility(View.GONE);
                activity.setShowingServicioHelp(false);
            }, 200);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("onActivityResult(requestCode: %d, resultCode: %d)", requestCode, resultCode);
        postReferencia(new ActivityResultEvent(requestCode, resultCode, data));
    }

    @OnTextChanged(value = R.id.text_servicio_otracantidad, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    @Override
    public void cambiaCantidad(CharSequence seq) {
        if (Pattern.matches(McRegexPatterns.CURRENCY_AMOUNT, seq)) {
            if (!Strings.isNullOrEmpty(Objects.requireNonNull(referenciaText.getText()).toString())) {
                pagarButton.setChecked(true);
            }
        } else {
            pagarButton.setChecked(false);
        }
    }

    @OnClick(R.id.b_servicio_pagar)
    @Override
    public void launchPago() {
        validator.validate();
    }

    @Override
    public void clear() {
        referenciaText.setText("");
        cantidadText.setText("");
        cambiaCantidad("");
    }

    @Override
    public void showSuccess(int resId) {
        showSuccess(getString(resId));
    }

    @Override
    public void setTipoCambio(ServicioResponse.ServicioEntity model, String referencia, double monto,
                              double tipoCambio) {
        SaldoEntity saldoEntity =
                new SaldoEntity.Builder().setmComision(monto * model.getPorcentajeComision())
                        .setmComisionMxn(monto * model.getPorcentajeComision())
                        .setmComisionUsd(tipoCambio == 0.0 ? 0.0 : (monto / tipoCambio) * model.getPorcentajeComision())
                        .setmConcepto(String.format("%s %s", getString(R.string.nav_billpayments), model.getEmpresa()))
                        .setmMonto(monto)
                        .setmMontoMxn(monto)
                        .setmMontoUsd(tipoCambio == 0.0 ? 0.0 : monto / tipoCambio)
                        .setmTipoCambio(tipoCambio)
                        .setmTotalMxn(monto + (monto * model.getPorcentajeComision()))
                        .setmTotalUsd(tipoCambio == 0.0 ? 0.0 : (monto / tipoCambio) + ((monto / tipoCambio) * model.getPorcentajeComision()))
                        .build();

        saldoEntity.setReferencia(referencia);
        model.setReferencia(referencia);

        Bundle bundle = new Bundle();
        bundle.putParcelable("servicio", model);
        bundle.putParcelable("saldo", saldoEntity);

        activity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.activity_pago_container,
                        WalletSelectFragment.get(WalletSelectFragment.SERVICIOS_MX, bundle))
                .hide(this)
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .commit();
    }
}
