package addcel.mobilecard.ui.usuario.wallet.update;

import android.app.DatePickerDialog;

import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.FranquiciaEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;

/**
 * ADDCEL on 10/10/17.
 */
public interface WalletUpdateContract {
    interface View extends Validator.ValidationListener, DatePickerDialog.OnDateSetListener {

        boolean isPrimary();

        String getCardHolderName();

        void setCardField();

        String getCard();

        String getCvv();

        void clickVigencia();

        String getVigencia();

        TipoTarjetaEntity getTipoTarjeta();

        String getAddress1();

        String getAddress2();

        String getZipCode();

        void clickSave();

        void showProgress();

        void hideProgress();

        void showExpirationDialog();

        void dismissExpirationDialog();

        void showError(int resId);

        void showError(String msg);

        void showSuccess(String msg);

        void setValuesOnError();

        void updateCards(List<CardEntity> models);

        void updateCardsAndFinish(List<CardEntity> models);

        void lockIfMobilecard();

        int getSelectedState();

        void updateStates(List<EstadoResponse.EstadoEntity> states);

        String getDirUsa();

        String getCityUsa();

        String getZipUsa();
    }

    interface Presenter {
        String getAlias(CardEntity entity);

        boolean isPrimary(CardEntity entity);

        boolean isMobilecard(CardEntity entity);

        String getCardHolderName(CardEntity entity);

        String getCard(CardEntity entity);

        FranquiciaEntity getFranquicia(CardEntity entity);

        String getCvv(CardEntity entity);

        String[] getVigencia(CardEntity entity);

        TipoTarjetaEntity getTipoTarjeta(CardEntity entity);

        String getDireccion1(CardEntity entity);

        String getDireccion2(CardEntity entity);

        String getZipCode(CardEntity entity);

        void update(CardEntity entity);

        void delete(CardEntity entity);

        int getIdPais();

        void getEstados();

        void clearDisposables();
    }
}
