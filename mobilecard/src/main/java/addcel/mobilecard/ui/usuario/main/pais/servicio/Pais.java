package addcel.mobilecard.ui.usuario.main.pais.servicio;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.R;

/**
 * ADDCEL on 01/03/17.
 */
public enum Pais {
    US(3, "USA", R.drawable.flag_usa, R.string.txt_us), MX(1, "México", R.drawable.flag_mex,
            R.string.txt_mx);
    // COL(2, "Colombia", R.drawable.flag_colombia, R.string.txt_col);

    private final int id;
    private final String description;
    private final int flagIcon;
    private final int nameRes;

    Pais(int id, String description, int flagIcon, int nameRes) {
        this.id = id;
        this.description = description;
        this.flagIcon = flagIcon;
        this.nameRes = nameRes;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public int getFlagIcon() {
        return flagIcon;
    }

    public int getNameRes() {
        return nameRes;
    }

    @NotNull
    @Override
    public String toString() {
        return description;
    }
}
