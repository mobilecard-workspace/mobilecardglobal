package addcel.mobilecard.ui.usuario.colombia.multimarket.productos.compra

import addcel.mobilecard.McConstants
import addcel.mobilecard.data.net.colombia.multimarket.MmCompraProductoEntity
import addcel.mobilecard.data.net.colombia.multimarket.MultimarketAPI
import addcel.mobilecard.data.net.colombia.multimarket.Valor
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.utils.ErrorUtil
import androidx.core.text.isDigitsOnly
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-12-19.
 */
interface MmProductosConfirmPresenter {
    fun addToDisposables(disposable: Disposable)
    fun clearDisposables()
    fun getIdUsuario(): Long
    fun comprarProducto(request: MmCompraProductoEntity)
    fun getMonto(valor: Valor, amount: Double): Double
    fun getComision(valor: Valor, amount: Double): Double
}

class MmProductosConfirmPresenterImpl(
        val multimarket: MultimarketAPI,
        val usuario: Usuario, val compositeDisposable: CompositeDisposable, val view: MmProductosConfirmView) : MmProductosConfirmPresenter {

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun getIdUsuario(): Long {
        return usuario.ideUsuario
    }

    override fun comprarProducto(request: MmCompraProductoEntity) {
        view.showProgress()
        val cDisp = multimarket.compraProducto(
                request
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    view.onPagar(it)
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                    view.enablePurchaseButton()
                })
        addToDisposables(cDisp)
    }

    override fun getMonto(valor: Valor, amount: Double): Double {
        return if (amount > 0.0) {
            amount
        } else {
            if (valor.valor.isDigitsOnly()) {
                valor.valor.toDouble()
            } else {
                0.0
            }
        }
    }

    override fun getComision(valor: Valor, amount: Double): Double {
        return if (amount > 0.0) {
            amount * McConstants.DEFAULT_FEE_PCT
        } else {
            if (valor.valor.isDigitsOnly()) {
                valor.valor.toDouble() * McConstants.DEFAULT_FEE_PCT
            } else {
                0.0
            }
        }
    }

}