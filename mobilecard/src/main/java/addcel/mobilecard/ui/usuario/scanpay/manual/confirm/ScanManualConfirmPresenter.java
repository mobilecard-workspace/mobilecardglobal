package addcel.mobilecard.ui.usuario.scanpay.manual.confirm;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.scanpay.SPApi;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 24/08/18.
 */
public class ScanManualConfirmPresenter implements ScanManualConfirmContract.Presenter {
    private final ScanPayOpenInteractor interactor;
    private final double monto;
    private final double propina;
    private final String concepto;
    private final LcpfEstablecimiento establecimiento;
    private final CardEntity formaPago;
    private final NumberFormat currFormat;
    private final ScanManualConfirmContract.View view;

    public ScanManualConfirmPresenter(ScanPayOpenInteractor interactor, double monto, double propina,
                                      String concepto, LcpfEstablecimiento establecimiento, CardEntity formaPago,
                                      NumberFormat currFormat, ScanManualConfirmContract.View view) {
        this.interactor = interactor;
        this.monto = monto;
        this.propina = propina;
        this.concepto = concepto;
        this.establecimiento = establecimiento;
        this.formaPago = formaPago;
        this.currFormat = currFormat;
        this.view = view;
    }

    @Override
    public double getMonto() {
        return monto;
    }

    @Override
    public String getConcepto() {
        return StringUtil.removeAcentosMultiple(concepto);
    }

    @Override
    public LcpfEstablecimiento getEstablecimiento() {
        return establecimiento;
    }

    @Override
    public CardEntity getFormaPago() {
        return formaPago;
    }

    @Override
    public String getFormattedFormaPago() {
        return StringUtil.maskCard(AddcelCrypto.decryptHard(formaPago.getPan()));
    }

    @Override
    public String getFormattedMonto() {
        return currFormat.format(monto);
    }

    @Override
    public String getFormattedPropina() {
        return currFormat.format(propina);
    }

    @Override
    public String getFormattedComision() {
        return currFormat.format(calculaComision());
    }

    @Override
    public SPPagoEntity buildRequest() {
        return new SPPagoEntity(getMonto(), calculaComision(),
                StringUtil.removeAcentosMultiple(concepto), "", establecimiento.getId(), "", 0,
                formaPago.getIdTarjeta(), interactor.getIdUsuario(), "", interactor.getLocation().getLat(),
                interactor.getLocation().getLon(), "", Integer.parseInt(view.getMsi()), propina,
                StringUtil.removeAcentosMultiple(establecimiento.getAlias()), "", "", "", "", "", "", "",
                "", "");
    }

    @Override
    public boolean isMsiDisabled() {
        return isDebito(formaPago) || isAmex(formaPago);
    }

    @Override
    public void getToken(String profile) {
        view.showProgress();
        interactor.getToken(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                interactor.getIdPais(SPApi.TIPO_USUARIO),
                AddcelCrypto.encryptSensitive(JsonUtil.toJson(buildRequest())), profile,
                new InteractorCallback<TokenEntity>() {
                    @Override
                    public void onSuccess(@NotNull TokenEntity result) {
                        view.hideProgress();
            /*if (result.getSecure()) {
              view.launchSecure(buildRequest(), result);
            } else {
              launchOpenPago(result);
            }*/
                        launchOpenPago(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void launchOpenPago(TokenEntity token) {
        view.showProgress();
        interactor.pagoBP(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), token.getToken(),
                SPApi.TIPO_USUARIO, token.getAccountId(), buildRequest(),
                new InteractorCallback<SPReceiptEntity>() {
                    @Override
                    public void onSuccess(@NotNull SPReceiptEntity result) {
                        view.hideProgress();
                        view.launchOpen(result, token);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    private boolean isDebito(CardEntity model) {
        return model.getTipoTarjeta().equals(TipoTarjetaEntity.DEBITO);
    }

    private boolean isAmex(CardEntity model) {
        switch (model.getTipo()) {
            case AmEx:
            case Amex:
                return true;
            default:
                return false;
        }
    }

    private double calculaComision() {
        return establecimiento.getComisionFija() + ((monto + establecimiento.getComisionFija())
                * establecimiento.getComisionPorcentaje());
    }
}
