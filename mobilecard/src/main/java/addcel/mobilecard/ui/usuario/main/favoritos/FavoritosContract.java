package addcel.mobilecard.ui.usuario.main.favoritos;

import java.util.ArrayList;
import java.util.List;

import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import addcel.mobilecard.data.net.aci.model.ServiceModel;
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.event.impl.PostPaisEvent;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 02/10/17.
 */
public interface FavoritosContract {
    interface View extends ScreenView {

        void notifyRemoval(int pos);

        void notifyUpdate(int update, Favorito favorito);

        void populate(List<Favorito> favoritos);

        void onPaisUpdate(PostPaisEvent event);

        void clickEdit();

        void onEdit();

        void clickFinishEdit();

        void onFinishEdit();

        void clickFavorito(int pos);

        void clickDelete(int pos, Favorito fav);

        void showDeleteDialog(int pos, Favorito fav);
    }

    interface Presenter {

        void clearCompositeDisposable();

        void getAll();

        CatalogoResponse.RecargaEntity getRecargaModel(Favorito favorito);

        ArrayList<CatalogoResponse.MontoEntity> getMontos(Favorito favorito);

        ServicioResponse.ServicioEntity getServicioModel(Favorito favorito);

        ServiceModel getServiceModel(Favorito favorito);

        void removeFavorite(int pos, Favorito favorito);

        void restoreFavorite(int pos, Favorito favorito);
    }
}
