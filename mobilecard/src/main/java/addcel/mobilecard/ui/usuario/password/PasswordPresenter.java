package addcel.mobilecard.ui.usuario.password;

import android.annotation.SuppressLint;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 22/12/16.
 */
public final class PasswordPresenter implements PasswordContract.Presenter {
    private final SessionOperations session;
    private final UsuariosService service;
    private final Usuario usuario;
    private final CompositeDisposable disposables;
    private final PasswordContract.View view;

    PasswordPresenter(PasswordContract.View view, SessionOperations session, UsuariosService service,
                      Usuario usuario) {
        this.session = session;
        this.service = service;
        this.usuario = usuario;
        this.disposables = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        disposables.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void setPassword(String oldPass, String newPass) {
        view.showProgress();
        disposables.add(
                service.updatePassword(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                        usuario.getIdeUsuario(), AddcelCrypto.encryptHard(oldPass),
                        AddcelCrypto.encryptHard(newPass))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(mcResponse -> {
                            view.hideProgress();
                            if (mcResponse.getIdError() == 0) {
                                saveUsuario(newPass, 1);
                                view.onPasswordChanged(mcResponse.getMensajeError());
                            } else {
                                view.showError(mcResponse.getMensajeError());
                            }
                        }, throwable -> {
                            view.hideProgress();
                            view.showError(StringUtil.getNetworkError());
                        }));
    }

    @Override
    public void saveUsuario(String newPass, int newStatus) {
        usuario.setUsrPwd(AddcelCrypto.encryptHard(newPass));
        usuario.setIdUsrStatus(newStatus);
        session.setUsuario(usuario);
        session.setUsuarioLogged(true);
    }
}
