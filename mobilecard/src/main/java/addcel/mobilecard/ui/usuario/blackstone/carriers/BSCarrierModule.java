package addcel.mobilecard.ui.usuario.blackstone.carriers;

import android.os.Bundle;

import com.google.common.base.Preconditions;

import addcel.mobilecard.data.net.blackstone.BlackstoneService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.usuario.blackstone.BlackstoneActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 09/07/18.
 */
@Module
public class BSCarrierModule {
    private final BSCarrierFragment fragment;

    BSCarrierModule(BSCarrierFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    BlackstoneActivity provideActivity() {
        return (BlackstoneActivity) Preconditions.checkNotNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Bundle provideArguments() {
        return Preconditions.checkNotNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    BlackstoneService provideService(Retrofit retrofit) {
        return retrofit.create(BlackstoneService.class);
    }

    @PerFragment
    @Provides
    BSCarrierAdapter provideAdapter() {
        return new BSCarrierAdapter();
    }

    @PerFragment
    @Provides
    BSCarrierContract.Presenter providePresenter(BlackstoneService service,
                                                 Bundle bundle) {
        return new BSCarrierPresenter(service, bundle.getString("countryCode"), fragment);
    }
}
