package addcel.mobilecard.ui.usuario.scanpay.qr.pe.processing

import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-07-17.
 */
@Module
class ScanPeQrProcessingModule(val fragment: ScanPeQrProcessingFragment) {
    @PerFragment
    @Provides
    fun provideActivity(): ScanPayActivity {
        return fragment.activity as ScanPayActivity
    }

    @PerFragment
    @Provides
    fun providePago(): SPPagoEntity {
        return fragment.arguments?.getParcelable("pago")!!
    }

    @PerFragment
    @Provides
    fun provideApi(r: Retrofit): SPApi {
        return SPApi.provide(r)
    }

    @PerFragment
    @Provides
    fun provideTokenizer(r: Retrofit): TokenizerAPI {
        return TokenizerAPI.provideTokenizerAPI(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            api: SPApi,
            r: TokenizerAPI
    ): ScanPeQrProcessingPresenter {
        return ScanPeQrProcessingPresenterImpl(api, r, CompositeDisposable(), fragment)
    }
}

@PerFragment
@Subcomponent(modules = [ScanPeQrProcessingModule::class])
interface ScanPeQrProcessingSubcomponent {
    fun inject(fragment: ScanPeQrProcessingFragment)
}