package addcel.mobilecard.ui.usuario.wallet.create;

import android.app.DatePickerDialog;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;
import android.widget.ArrayAdapter;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Calendar;
import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.wallet.add.WalletCreateInteractor;
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public final class WalletCreateModule {
    private final WalletCreateFragment fragment;

    WalletCreateModule(WalletCreateFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    WalletContainerActivity provideActivity() {
        return (WalletContainerActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    Calendar provideVigenciaCalendar() {
        return Calendar.getInstance();
    }

    @PerFragment
    @Provides
    DatePickerDialog datePickerDialog(WalletContainerActivity activity,
                                      Calendar calendar) {
        DatePickerDialog datePickerDialog;
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            datePickerDialog =
                    new FixedHoloDatePickerDialog(activity, fragment, calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog =
                        new DatePickerDialog(activity, R.style.CustomDatePickerDialogTheme, fragment,
                                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                calendar.get(Calendar.DAY_OF_MONTH));
            } else {
                // R.style.CustomDatePickerDialogTheme
                datePickerDialog = new DatePickerDialog(activity, fragment, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            }
        }

        datePickerDialog.getDatePicker()
                .findViewById(Resources.getSystem().getIdentifier("day", "id", "android"))
                .setVisibility(View.GONE);
        return datePickerDialog;
    }


    @PerFragment
    @Provides
    ArrayAdapter<EstadoResponse.EstadoEntity> provideAdapter(WalletContainerActivity activity) {
        ArrayAdapter<EstadoResponse.EstadoEntity> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter.setNotifyOnChange(true);

        return adapter;
    }

    @PerFragment
    @Provides
    WalletCreateInteractor provideInteractor(WalletAPI api, CatalogoService catalogo,
                                             SessionOperations session) {
        return new WalletCreateInteractor(api, catalogo, session.getUsuario(), new CompositeDisposable());
    }

    @PerFragment
    @Provides
    WalletCreateContract.Presenter providePresenter(
            WalletCreateInteractor interactor) {
        return new WalletCreatePresenter(interactor, fragment);
    }
}
