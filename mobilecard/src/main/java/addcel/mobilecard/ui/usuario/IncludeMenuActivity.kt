package addcel.mobilecard.ui.usuario

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.domain.usuario.bottom.MenuEvent
import addcel.mobilecard.domain.usuario.bottom.UseCase
import addcel.mobilecard.ui.usuario.edocuenta.EdocuentaActivity
import addcel.mobilecard.ui.usuario.main.MainActivity
import addcel.mobilecard.ui.usuario.main.MainContract
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.otto.Bus
import com.squareup.otto.Subscribe
import es.dmoral.toasty.Toasty
import javax.inject.Inject

class IncludeMenuActivity : AppCompatActivity() {

    @Inject
    lateinit var bus: Bus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_include_menu)
        Mobilecard.get().netComponent.inject(this)
    }

    override fun onResume() {
        super.onResume()
        bus.register(this)
    }

    override fun onPause() {
        bus.unregister(this)
        super.onPause()
    }

    @Subscribe
    fun postMenuEvent(event: MenuEvent) {
        when (event.result.idUsrStatus) {
            100 -> Toasty.info(this, "SMS").show()
            99 -> Toasty.info(this, "EMAIL").show()
            1 -> {
                when {
                    event.useCase == UseCase.HOME -> startActivity(
                            MainActivity.get(this, MainContract.UseCase.INICIO, false)
                    )
                    event.useCase == UseCase.WALLET -> startActivity(
                            WalletContainerActivity.get(
                                    this
                            )
                    )
                    event.useCase == UseCase.FAVORITOS -> startActivity(
                            MainActivity.get(this, MainContract.UseCase.FRECUENTES, false)
                    )
                    event.useCase == UseCase.HISTORIAL -> startActivity(EdocuentaActivity.get(this))
                }
            }
        }
    }
}
