package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.select;

import java.util.List;

import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.event.BusEvent;

/**
 * ADDCEL on 24/11/17.
 */
public class MxTransferBeneficiarioSelectEvent implements BusEvent {
    private final List<AccountEntity> accounts;

    public MxTransferBeneficiarioSelectEvent(List<AccountEntity> accounts) {
        this.accounts = accounts;
    }

    public List<AccountEntity> getAccounts() {
        return accounts;
    }
}
