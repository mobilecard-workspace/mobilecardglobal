package addcel.mobilecard.ui.usuario.colombia.recargas.tae.confirm

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.OperadorRecarga
import addcel.mobilecard.data.net.colombia.RecargaRequest
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.PurchaseScreenView
import addcel.mobilecard.ui.usuario.colombia.recargas.ColombiaRecargasActivity
import addcel.mobilecard.ui.usuario.colombia.recargas.categorias.ColombiaCategoria
import addcel.mobilecard.ui.usuario.colombia.result.ColombiaResultFragment
import addcel.mobilecard.utils.DeviceUtil
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_scanpay_qr_confirm.*
import mx.mobilecard.crypto.AddcelCrypto
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 9/24/19.
 */

@Parcelize
data class ColombiaRecargaTaeConfirmModel(
        val usuario: Usuario,
        val colombiaCategoria: ColombiaCategoria, val operadorRecarga: OperadorRecarga,
        val monto: Double, val destino: String
) : Parcelable

interface ColombiaRecargaTaeConfirmView : PurchaseScreenView {
    fun setInvoiceLabel()
    fun setCardInfo()
    fun setMontoLabel()
    fun launchPago()
    fun onPagoSuccess(receipt: SPReceiptEntity)
}

class ColombiaRecargaTaeConfirmFragment : Fragment(), ColombiaRecargaTaeConfirmView {

    companion object {

        private val COL_FORMAT = NumberFormat.getCurrencyInstance(Locale("es", "CO"))

        fun get(arguments: Bundle): ColombiaRecargaTaeConfirmFragment {
            val frag = ColombiaRecargaTaeConfirmFragment()
            frag.arguments = arguments
            return frag
        }
    }

    @Inject
    lateinit var presenter: ColombiaRecargaTaeConfirmPresenter

    private lateinit var card: CardEntity
    private lateinit var model: ColombiaRecargaTaeConfirmModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.colombiaRecargaTaeConfirmSubcomponent(
                ColombiaRecargaTaeConfirmModule(this)
        ).inject(this)

        card = arguments?.getParcelable("card")!!
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_scanpay_qr_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b_scanpay_qr_confirm_continuar.setOnClickListener { clickPurchaseButton() }
        setInvoiceLabel()
        setMontoLabel()
        setCardInfo()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) enablePurchaseButton()
    }

    override fun setInvoiceLabel() {
        val invoice =
                getString(
                        R.string.txt_scanpay_recarga_confirm_invoice_co, COL_FORMAT.format(model.monto),
                        COL_FORMAT.format(0.0), model.operadorRecarga.nombre, model.destino
                )
        view_scanpay_qr_confirm_data.text = invoice
    }

    override fun setCardInfo() {
        Picasso.get().load(card.imgShort).placeholder(R.drawable.bg_visa)
                .placeholder(R.drawable.bg_visa).fit().into(bg_scan_card, object : Callback {
                    override fun onSuccess() {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }

                    override fun onError(e: Exception) {
                        pan_scan_card.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
                    }
                })
    }

    override fun setMontoLabel() {
        val total = model.monto
        view_scanpay_qr_confirm_total.text = COL_FORMAT.format(total)
    }


    override fun onWhiteList() {
        if (activity != null)
            launchPago()
    }

    override fun notOnWhiteList() {
        showError("No está autorizado para realizar esta operacion. Contacte a soporte.")
    }

    override fun launchPago() {
        val request = RecargaRequest(
                model.operadorRecarga.codigo,
                model.operadorRecarga.nombre,
                BuildConfig.ADDCEL_APP_ID,
                2,
                card.idTarjeta,
                model.usuario.ideUsuario,
                StringUtil.getCurrentLanguage(),
                DeviceUtil.getDeviceId(activity!!),
                model.monto,
                model.destino
        )

        presenter.launchPago(request)
    }

    override fun onPagoSuccess(receipt: SPReceiptEntity) {
        (activity as ColombiaRecargasActivity).fragmentManagerLazy.commit {
            add(R.id.frame_colombia, ColombiaResultFragment.get(receipt))
            hide(this@ColombiaRecargaTaeConfirmFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
        enablePurchaseButton()
    }

    override fun isPurchaseButtonEnabled(): Boolean {
        return b_scanpay_qr_confirm_continuar.isEnabled
    }

    override fun disablePurchaseButton() {
        b_scanpay_qr_confirm_continuar.isEnabled = false
    }

    override fun enablePurchaseButton() {
        b_scanpay_qr_confirm_continuar.isEnabled = true
    }

    override fun clickPurchaseButton() {
        if (isPurchaseButtonEnabled()) {
            disablePurchaseButton()
            presenter.isOnWhiteList(model.usuario.ideUsuario, model.usuario.idPais)
        }
    }

    override fun showProgress() {
        (activity as ColombiaRecargasActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ColombiaRecargasActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ColombiaRecargasActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ColombiaRecargasActivity).showSuccess(msg)
    }
}