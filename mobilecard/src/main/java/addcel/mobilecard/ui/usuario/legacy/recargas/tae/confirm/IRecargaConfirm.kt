package addcel.mobilecard.ui.usuario.legacy.recargas.tae.confirm

import addcel.mobilecard.data.net.catalogo.model.PaisResponse.PaisEntity
import addcel.mobilecard.data.net.pago.PagoEntity
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.PurchaseScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureCallback

/**
 * ADDCEL on 2020-01-15.
 */
interface RecargaConfirmView : PurchaseScreenView, BillPocketSecureCallback {
    fun getImei(): String
    fun updateUiOnIdPais()
    fun updateOnIdPaisUS()
    fun lockScreen()
    fun unlockScreen()
    fun setTipoCambio(tipoCambio: Double)
    fun showError(resId: Int)
    fun onTokenReceived(tokenEntity: TokenEntity)
    fun onBpSuccess(response: PagoResponse)
}

interface RecargaConfirmPresenter {
    fun clearDisposables()

    fun getIdUsuario(): Long

    fun getCard(): CardEntity

    fun getReferenciaValue(): String

    fun getBalance(): String

    fun getBalanceUSD(): String

    fun getFee(): String

    fun getFormattedTipoCambio(): String

    fun getTotal(): String

    fun getFeeUSD(): String

    fun getTotalUSD(): String

    fun getStartUrl(accountId: String): String

    fun getSuccessUrl(): String

    fun getErrorUrl(): String

    fun getFormUrl(): String

    fun getJumioRef(): String

    fun getjumioStatus(): String

    fun getIdPais(): Int

    fun getPaisDestino(): PaisEntity?

    fun getTipoCambio(): Double

    fun shouldHideUSAFields(): Boolean

    fun checkWhiteList()

    fun getToken(profile: String)

    fun buildPagoRequest(): PagoEntity

    fun buildPagoRequestAsString(entity: PagoEntity): String

    fun buildPagoRequestForBrowser(entity: PagoEntity): ByteArray

    fun executePagoBP(token: TokenEntity?, entity: PagoEntity)
}