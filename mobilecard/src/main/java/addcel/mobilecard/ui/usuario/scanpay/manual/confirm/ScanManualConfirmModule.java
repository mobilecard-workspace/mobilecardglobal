package addcel.mobilecard.ui.usuario.scanpay.manual.confirm;

import android.os.Bundle;

import java.text.NumberFormat;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.SPApi;
import addcel.mobilecard.data.net.scanpay.model.LcpfEstablecimiento;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor;
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

@Module
public final class ScanManualConfirmModule {
    private final ScanManualConfirmFragment fragment;

    public ScanManualConfirmModule(ScanManualConfirmFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ScanPayActivity provideActivity() {
        return (ScanPayActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Bundle provideArgs() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    double provideMonto(Bundle args) {
        return args.getDouble("monto");
    }

    @PerFragment
    @Provides
    @Named("propina")
    double providePropina(Bundle args) {
        return args.getDouble("propina");
    }

    @PerFragment
    @Provides
    String provideConcepto(Bundle args) {
        return args.getString("concepto");
    }

    @PerFragment
    @Provides
    LcpfEstablecimiento provideEstablecimiento(Bundle args) {
        return args.getParcelable("establecimiento");
    }

    @PerFragment
    @Provides
    CardEntity provideFormaPago(Bundle args) {
        return args.getParcelable("formaPago");
    }

    @PerFragment
    @Provides
    ScanPayOpenInteractor provideInteractor(Retrofit retrofit,
                                            SessionOperations session) {
        return new ScanPayOpenInteractor(SPApi.Companion.provide(retrofit),
                TokenizerAPI.Companion.provideTokenizerAPI(retrofit), session, new CompositeDisposable());
    }

    @PerFragment
    @Provides
    ScanManualConfirmContract.Presenter providePresenter(
            ScanPayOpenInteractor interactor, double monto, @Named("propina") double propina,
            String concepto, LcpfEstablecimiento establecimiento, CardEntity formaPago,
            NumberFormat currFormat) {
        return new ScanManualConfirmPresenter(interactor, monto, propina, concepto, establecimiento,
                formaPago, currFormat, fragment);
    }
}
