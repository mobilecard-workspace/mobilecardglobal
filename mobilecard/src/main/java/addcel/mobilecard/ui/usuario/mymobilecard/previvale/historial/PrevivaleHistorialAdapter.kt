package addcel.mobilecard.ui.usuario.mymobilecard.previvale.historial

import addcel.mobilecard.R
import addcel.mobilecard.data.net.previvale.historial.PrevivaleTransaction
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DiffUtil.DiffResult
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.NumberFormat

/**
 * ADDCEL on 23/06/17.
 */
class PrevivaleHistorialAdapter : RecyclerView.Adapter<PrevivaleHistorialAdapter.IViewHolder>() {

    companion object {
        private const val DEPOSITO = "Deposito"
        private const val CARGO = "Cargo"
        private const val RECHAZADA = "Rechazada"
    }

    private val data = mutableListOf<PrevivaleTransaction>()

    fun update(newData: List<PrevivaleTransaction>): Disposable {
        val callback: DiffUtil.Callback = PrevivaleHistorialDiffCallback(data, newData)
        return Observable.fromCallable { DiffUtil.calculateDiff(callback) }.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { diffResult: DiffResult ->
                    diffResult.dispatchUpdatesTo(this)
                    data.clear()
                    data.addAll(newData)
                }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IViewHolder {
        return IViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_historial, parent, false))
    }

    override fun onBindViewHolder(holder: IViewHolder, position: Int, payloads: List<Any>) {

        var ticket: String?
        var date: String?
        var importe: String?
        var status: String?

        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val o = payloads[0] as Bundle
            for (key in o.keySet()) {
                status = o.getString("status")

                if (key == "establecimiento") {
                    ticket = o.getString(key)
                    holder.ticketView.text = "$status $ticket"
                }
                if (key == "fecha") {
                    date = o.getString(key)
                    holder.dateView.text = date
                }
                if (key == "cantidad") {
                    importe = o.getString(key)
                    holder.totalView.text = importe
                    if (status == CARGO) {
                        holder.totalView.text = "-$importe"
                        holder.totalView.setTextColor(Color.RED)
                    }
                }
                if (key == "status") {
                    status = o.getString(key)
                    status?.let { holder.setStatusResource(it) }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        val transaction = data[position]
        holder.ticketView.text = "${transaction.status} ${transaction.establecimiento}"
        holder.totalView.text = transaction.cantidad
        if (transaction.status == CARGO) {
            holder.totalView.text = "-${transaction.cantidad}"
            holder.totalView.setTextColor(Color.RED)
        }
        holder.dateView.text = transaction.fecha
        holder.setStatusResource(transaction.status)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun getItem(pos: Int): PrevivaleTransaction {
        return data[pos]
    }


    class IViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val totalView: TextView = itemView.findViewById(R.id.view_historial_monto)
        val ticketView: TextView = itemView.findViewById(R.id.view_historial_concepto)
        val dateView: TextView = itemView.findViewById(R.id.view_historial_fecha)
        private val statusView: ImageView = itemView.findViewById(R.id.img_historial_status)


        fun setStatusResource(status: String) {
            var res = R.drawable.ic_historial_list_processing
            if (status == DEPOSITO) res = R.drawable.ic_historial_list_success
            if (status == CARGO) res = R.drawable.ic_historial_list_success
            if (status == RECHAZADA) res = R.drawable.ic_historial_list_error
            statusView.setImageResource(res)
        }
    }
}