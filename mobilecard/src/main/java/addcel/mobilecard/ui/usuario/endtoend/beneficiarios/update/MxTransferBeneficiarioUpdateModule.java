package addcel.mobilecard.ui.usuario.endtoend.beneficiarios.update;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.H2HApi;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.h2h.H2HAccountInteractor;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

/**
 * ADDCEL on 09/11/17.
 */
@Module
public class MxTransferBeneficiarioUpdateModule {
    private final MxTransferBeneficiarioUpdateFragment fragment;

    MxTransferBeneficiarioUpdateModule(MxTransferBeneficiarioUpdateFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    MxTransferActivity provideAdActivity() {
        return (MxTransferActivity) fragment.getActivity();
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    AccountEntity provideAccount() {
        return Preconditions.checkNotNull(fragment.getArguments()).getParcelable("beneficiario");
    }

    @PerFragment
    @Provides
    H2HApi provideApi(Retrofit retrofit) {
        return H2HApi.Companion.get(retrofit);
    }

    @PerFragment
    @Provides
    H2HAccountInteractor provideInteractor(H2HApi api,
                                           SessionOperations session) {
        return new H2HAccountInteractor(api, session, new CompositeDisposable());
    }

    @PerFragment
    @Provides
    MxTransferBeneficiarioUpdateContract.Presenter providePresenter(
            H2HAccountInteractor interactor, AccountEntity account) {
        return new MxTransferBeneficiarioUpdatePresenter(interactor, account, fragment);
    }
}
