package addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.seleccion

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.colombia.ColombiaMiddleware
import addcel.mobilecard.data.net.colombia.VrTag
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 9/27/19.
 */
interface ViarapidaSeleccionPresenter {

    fun addToDisposables(d: Disposable)

    fun clearDisposables()

    fun getDetalleTag(idUsuario: Long, tag: VrTag)
}

class ViarapidaSeleccionPresenterImpl(
        val middleware: ColombiaMiddleware,
        val disposables: CompositeDisposable, val view: ViarapidaSeleccionView
) :
        ViarapidaSeleccionPresenter {
    override fun addToDisposables(d: Disposable) {
        disposables.add(d)
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun getDetalleTag(idUsuario: Long, tag: VrTag) {

        view.showProgress()

        val dDisp = middleware.detalleTag(
                ColombiaMiddleware.getAuth(), BuildConfig.ADDCEL_APP_ID, 2,
                StringUtil.getCurrentLanguage(), tag.idViaTag, idUsuario
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    view.hideProgress()
                    if (r.codigo == 0) view.onGetDetalle(r)
                    else view.showError(r.mensaje)
                }, { t ->
                    view.hideProgress()
                    view.showError(t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK))
                })

        addToDisposables(dDisp)
    }
}