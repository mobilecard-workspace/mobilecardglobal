package addcel.mobilecard.ui.usuario.mymobilecard.previvale.historial

import addcel.mobilecard.data.net.previvale.historial.PrevivaleAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import retrofit2.Retrofit
import javax.inject.Named

/**
 * ADDCEL on 2020-01-27.
 */

@Module
class PrevivaleHistorialModule(private val fragment: PrevivaleHistorialFragment) {

    @PerFragment
    @Provides
    fun provideContainer(): MyMobilecardActivity {
        return fragment.activity as MyMobilecardActivity
    }


    @PerFragment
    @Provides
    fun provideModel(): PrevivaleHistorialModel {
        return fragment.arguments?.getParcelable("model")!!
    }


    @PerFragment
    @Provides
    fun provideApi(@Named("cacheRetrofit") retrofit: Retrofit): PrevivaleAPI {
        return PrevivaleAPI.create(retrofit)
    }


    @PerFragment
    @Provides
    fun providePresenter(api: PrevivaleAPI): PrevivaleHistorialPresenter {
        return PrevivaleHistorialPresenterImpl(api, fragment)
    }
}

@PerFragment
@Subcomponent(modules = [PrevivaleHistorialModule::class])
interface PrevivaleHistorialSubcomponent {
    fun inject(fragment: PrevivaleHistorialFragment)
}