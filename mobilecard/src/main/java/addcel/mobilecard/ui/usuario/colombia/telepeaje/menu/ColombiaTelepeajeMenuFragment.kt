package addcel.mobilecard.ui.usuario.colombia.telepeaje.menu

import addcel.mobilecard.R
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.colombia.telepeaje.ColombiaTelepeajeActivity
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.tag.ViarapidaTagFragment
import addcel.mobilecard.ui.usuario.colombia.telepeaje.viarapida.tag.ViarapidaTagModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.view_categoria.*

/**
 * ADDCEL on 9/25/19.
 */

enum class ColombiaTelepeajeModel(val logoWhite: Int, val logoColor: Int, val text: String) {
    VIA_RAPIDA(R.drawable.viarapida, R.drawable.viarapida_color, "Vía Rápida")
}

class ColombiaTelepeajeMenuFragment : Fragment() {

    val adapter = ColombiaTelepeajeMenuAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_categoria, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_categoria.adapter = adapter
        recycler_categoria.layoutManager = LinearLayoutManager(view.context)
        ItemClickSupport.addTo(recycler_categoria).setOnItemClickListener { _, pos, _ ->
            (activity as ColombiaTelepeajeActivity).fragmentManagerLazy.commit {
                val viaModel =
                        ViarapidaTagModel(
                                adapter.getItem(pos)
                        )
                add(R.id.frame_colombia_telepeaje, ViarapidaTagFragment.get(viaModel))
                hide(this@ColombiaTelepeajeMenuFragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }
    }
}