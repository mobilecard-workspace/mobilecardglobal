package addcel.mobilecard.ui.usuario.main.tutorial

import addcel.mobilecard.R
import addcel.mobilecard.ui.usuario.main.MainActivity
import addcel.mobilecard.ui.usuario.main.menu.Menu2Fragment
import addcel.mobilecard.ui.usuario.menu.MenuLayout
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.screen_main_tutorial.*

/**
 * ADDCEL on 2019-06-07.
 */
class MainTutorialFragment : Fragment() {

    companion object {
        fun get(idPais: Int): MainTutorialFragment {
            val bundle = BundleBuilder().putInt("idPais", idPais).build()
            val fragment = MainTutorialFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var mActivity: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = activity as MainActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_main_tutorial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mActivity.mainToolbar.visibility = View.GONE
        mActivity.findViewById<MenuLayout>(R.id.menu_main).visibility = View.GONE
        arguments!!.getInt("idPais")
        screen_main_tutorial.setImageResource(R.drawable.mx_app_tutorial)
        screen_main_tutorial.setOnClickListener {
            fragmentManager?.commit {
                mActivity.findViewById<MenuLayout>(R.id.menu_main).visibility = View.VISIBLE
                add(R.id.frame_main, Menu2Fragment()).hide(this@MainTutorialFragment)
            }
            mActivity.mainToolbar.visibility = View.VISIBLE
        }
    }
}