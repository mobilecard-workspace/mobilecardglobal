package addcel.mobilecard.ui.usuario.scanpay.result.qr;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 11/09/18.
 */
@PerFragment
@Subcomponent(modules = ScanResultQrModule.class)
public interface ScanResultQrSubcomponent {
    void inject(ScanResultQrFragment fragment);
}
