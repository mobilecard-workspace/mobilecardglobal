package addcel.mobilecard.ui.usuario.legacy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.squareup.otto.Bus;

import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.event.CancelConsultaEvent;
import addcel.mobilecard.ui.ContainerActivity;
import addcel.mobilecard.ui.usuario.legacy.recargas.list.RecargaListFragment;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.RecargaFragment;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select.PeajeNewFragment;
import addcel.mobilecard.ui.usuario.legacy.servicios.HideTutorialEvent;
import addcel.mobilecard.ui.usuario.legacy.servicios.categorias.CategoriaListFragment;
import addcel.mobilecard.ui.usuario.legacy.servicios.consulta.ServicioConsultaFragment;
import addcel.mobilecard.ui.usuario.legacy.servicios.directo.ServicioFragment;
import addcel.mobilecard.utils.BundleBuilder;

public final class PagoContainerActivity extends ContainerActivity {

    @Inject
    Bus bus;

    private Toolbar toolbar;
    private boolean showingServicioHelp = false;
    private boolean showingConsultaData = false;

    public static Intent get(Context context, Parcelable useCaseObject) {
        Bundle bundle = new BundleBuilder().putParcelable("useCase", useCaseObject).build();
        return new Intent(context, PagoContainerActivity.class).putExtras(bundle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().inject(this);
        setContentView(R.layout.activity_pago_container);
        toolbar = findViewById(R.id.toolbar_pago_container);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Fragment initialFragment = provideInitialFragment(
                Objects.requireNonNull(getIntent().getExtras()).getParcelable("useCase"));

        if (savedInstanceState == null) {
            getFragmentManagerLazy().beginTransaction()
                    .add(R.id.activity_pago_container, initialFragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setAppToolbarTitle(int title) {
        toolbar.setTitle(title);
    }

    @Override
    public void setAppToolbarTitle(@NonNull String title) {
        toolbar.setTitle(title);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public @Nullable View getRetry() {
        return null;
    }

    private boolean isShowingServicioHelp() {
        return showingServicioHelp;
    }

    public void setShowingServicioHelp(boolean showingServicioHelp) {
        this.showingServicioHelp = showingServicioHelp;
    }

    private boolean isShowingConsultaData() {
        return showingConsultaData;
    }

    public void setShowingConsultaData(boolean showingConsultaData) {
        this.showingConsultaData = showingConsultaData;
    }

    @Override
    public void onBackPressed() {
        if (isShowingServicioHelp() && isShowingConsultaData()) {
            bus.post(new HideTutorialEvent());
        } else if (isShowingConsultaData()) {
            bus.post(new CancelConsultaEvent());
        } else if (isShowingServicioHelp()) {
            bus.post(new HideTutorialEvent());
        } else {
            super.onBackPressed();
        }
    }

    private Fragment provideInitialFragment(Parcelable useCase) {

        Fragment initialFragment = null;

        if (useCase instanceof RecargaUseCaseObject) {
            initialFragment =
                    RecargaListFragment.Companion.get(((RecargaUseCaseObject) useCase).getRecargas(),
                            ((RecargaUseCaseObject) useCase).getPais());
        } else if (useCase instanceof CategoriaUseCaseObject) {
            initialFragment =
                    CategoriaListFragment.Companion.get(((CategoriaUseCaseObject) useCase).getCategorias());
        } else if (useCase instanceof TaeFavUseCaseObject) {
            initialFragment = RecargaFragment.newInstance(((TaeFavUseCaseObject) useCase).getRecarga(),
                    ((TaeFavUseCaseObject) useCase).getMontos(), true);
        } else if (useCase instanceof PeajeFavUseCaseObject) {
            initialFragment = PeajeNewFragment.newInstance(((PeajeFavUseCaseObject) useCase).getRecarga(),
                    ((PeajeFavUseCaseObject) useCase).getMontos(), true);
        } else if (useCase instanceof ServicioFavUseCaseObject) {
            if (((ServicioFavUseCaseObject) useCase).getServicio().getConsultaSaldo()) {
                initialFragment =
                        ServicioConsultaFragment.get(((ServicioFavUseCaseObject) useCase).getServicio(), true);
            } else {
                initialFragment =
                        ServicioFragment.get(((ServicioFavUseCaseObject) useCase).getServicio(), true);
            }
        }

        return initialFragment;
    }

    @Override
    public void showProgress() {
        findViewById(R.id.progress_pago_container).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        findViewById(R.id.progress_pago_container).setVisibility(View.GONE);
    }
}
