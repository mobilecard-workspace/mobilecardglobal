package addcel.mobilecard.ui.usuario.main.menu;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 04/10/17.
 */
@PerFragment
@Subcomponent(modules = Menu2Module.class)
public interface Menu2Subcomponent {
    void inject(Menu2Fragment fragment);
}
