package addcel.mobilecard.ui.usuario.wallet.select.add;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.aci.processing.AciProcessingFragment;
import addcel.mobilecard.ui.usuario.blackstone.confirm.BSConfirmFragment;
import addcel.mobilecard.ui.usuario.endtoend.processing.MxTransferProcessingFragment;
import addcel.mobilecard.ui.usuario.ingo.processing.IngoProcessingFragment;
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.confirm.RecargaConfirmFragment;
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.confirm.PeajeConfirmFragment;
import addcel.mobilecard.ui.usuario.legacy.servicios.confirm.ServicioConfirmFragment;
import addcel.mobilecard.ui.usuario.wallet.create.pay.WalletCreatePayFragment;
import addcel.mobilecard.ui.usuario.wallet.select.WalletCardSelectModel;
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;

/**
 * ADDCEL on 10/08/18.
 */
public final class WalletSelectAddFragment extends Fragment
        implements WalletSelectAddContract.View {

    public static final int INGO = 10;
    private static final int RECARGAS_MX = 1;
    private static final int PEAJE = 2;
    private static final int SERVICIOS_MX = 3;
    private static final int SERVICIOS_US = 4;
    private static final int TRANSFER_MX = 5;
    private static final int BLACKSTONE = 9;
    @BindView(R.id.recycler_wallet_cards)
    RecyclerView cardsView;

    @BindView(R.id.progress_wallet_select)
    ProgressBar progressBar;

    @BindView(R.id.b_wallet_select_refresh)
    Button retryButton;

    @BindView(R.id.b_wallet_select_add)
    ImageButton addButton;

    @Inject
    WalletSelectAdapter adapter;
    @Inject
    WalletSelectAddContract.Presenter presenter;
    private Unbinder unbinder;

    public WalletSelectAddFragment() {
    }

    public static WalletSelectAddFragment get(int module, Bundle bundle) {
        WalletSelectAddFragment fragment = new WalletSelectAddFragment();
        bundle.putInt("module", module);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .walletSelectAddSubcomponent(new WalletSelectAddModule(this))
                .inject(this);
        if (adapter.getItemCount() == 0) {
            int module = Objects.requireNonNull(getArguments()).getInt("module");
            if (module == INGO) {
                presenter.getDebitCards();
            } else {
                presenter.getCards();
            }
        }
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_wallet_cards_select_add, container, false);
        unbinder = ButterKnife.bind(this, view);
        cardsView.setAdapter(adapter);
        cardsView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(cardsView)
                .setOnItemClickListener((recyclerView, position, v) -> clickSelect(position));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (adapter.getItemCount() == 0) showProgress();
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(cardsView);
        cardsView.setAdapter(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showRetry() {
        if (isVisible()) retryButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        if (isVisible()) retryButton.setVisibility(View.GONE);
    }

    @OnClick(R.id.b_wallet_select_refresh)
    @Override
    public void clickRetry() {
        hideRetry();
        int module = Objects.requireNonNull(getArguments()).getInt("module");
        if (module == INGO) {
            presenter.getDebitCards();
        } else {
            presenter.getCards();
        }
    }

    @Override
    public void showProgress() {
        if (isVisible() && getView() != null) progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (isVisible() && getView() != null) progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String msg) {
        if (isVisible()) Toasty.error(Objects.requireNonNull(getContext()), msg).show();
    }

    @Override
    public void showError(int resId) {
        showError(getString(resId));
    }

    @Override
    public void showSuccess(String msg) {
        if (isVisible()) Toasty.success(Objects.requireNonNull(getContext()), msg).show();
    }

    @Override
    public void clickSelect(int pos) {
        adapter.setChecked(pos);
    }

    @Override
    public void onCardsUpdated(List<CardEntity> models) {
        if (getView() != null && isVisible()) {
            List<WalletCardSelectModel> selectModels = Lists.newArrayList();
            for (CardEntity model : models) {
                selectModels.add(new WalletCardSelectModel(model, model.getDeterminada()));
            }
            adapter.update(selectModels);
            cardsView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.b_wallet_cards_select_continue)
    @Override
    public void goToNext() {
        try {
            int checkedPos = adapter.getCheckedCardPosition();
            if (adapter.getCheckedCardPosition() == -1) {
                showError(getString(R.string.error_wallet_card_select));
            } else {
                Objects.requireNonNull(getArguments())
                        .putParcelable("card", adapter.getItem(checkedPos).getCard());
                int module = getArguments().getInt("module");

                FragmentManager supportFragmentManager =
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager();

                switch (module) {
                    case RECARGAS_MX:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.activity_pago_container, RecargaConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case PEAJE:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.activity_pago_container, PeajeConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case SERVICIOS_MX:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.activity_pago_container, ServicioConfirmFragment.Companion.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case SERVICIOS_US:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_aci, AciProcessingFragment.get(getArguments()), "processing")
                                .commit();
                        break;
                    case TRANSFER_MX:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_transfermx, MxTransferProcessingFragment.get(getArguments()),
                                        "processing")
                                .commit();
                        break;
                    case BLACKSTONE:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_blackstone, BSConfirmFragment.get(getArguments()))
                                .hide(this)
                                .addToBackStack(null)
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .commit();
                        break;
                    case INGO:
                        supportFragmentManager.beginTransaction()
                                .add(R.id.frame_ingo, IngoProcessingFragment.get(getArguments()), "processing")
                                .commit();
                        break;
                    default:
                        break;
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.b_wallet_select_add)
    @Override
    public void clickAdd() {
        if (Objects.requireNonNull(getArguments()).getInt("module") == INGO) {
            Objects.requireNonNull(getFragmentManager())
                    .beginTransaction()
                    .add(R.id.frame_ingo, WalletCreatePayFragment.get(INGO, getArguments()))
                    .hide(this)
                    .addToBackStack(null)
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .commit();
        }
    }
}
