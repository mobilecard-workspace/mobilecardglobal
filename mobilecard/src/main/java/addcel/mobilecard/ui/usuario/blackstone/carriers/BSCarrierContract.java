package addcel.mobilecard.ui.usuario.blackstone.carriers;

import androidx.annotation.NonNull;

import java.util.List;

import addcel.mobilecard.data.net.blackstone.model.BSCarrierEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 09/07/18.
 */
public interface BSCarrierContract {
    interface View extends ScreenView {
        void setCarriers(@NonNull List<BSCarrierEntity> carriers);

        void onCarrierClicked(int pos);
    }

    interface Presenter {
        void getCarriers();
    }
}
