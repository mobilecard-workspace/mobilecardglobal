package addcel.mobilecard.ui.usuario.scanpay.qr;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 21/08/18.
 */
@PerFragment
@Subcomponent(modules = ScanQrModule.class)
public interface ScanQrSubcomponent {
    void inject(ScanQrFragment fragment);
}
