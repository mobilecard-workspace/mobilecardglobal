package addcel.mobilecard.ui.usuario.blackstone.carriers;

import android.annotation.SuppressLint;

import com.google.common.base.Strings;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.blackstone.BlackstoneService;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 09/07/18.
 */
class BSCarrierPresenter implements BSCarrierContract.Presenter {
    private final BlackstoneService service;
    private final String countryCode;
    private final BSCarrierContract.View view;

    BSCarrierPresenter(BlackstoneService service, String countryCode, BSCarrierContract.View view) {
        this.service = service;
        this.countryCode = countryCode;
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCarriers() {
        view.showProgress();
        service.getCarriers(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), countryCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bsCarrierEntities -> {
                    view.hideProgress();
                    view.setCarriers(bsCarrierEntities);
                }, throwable -> {
                    view.hideProgress();
                    view.showError(Strings.nullToEmpty(throwable.getLocalizedMessage()));
                });
    }
}
