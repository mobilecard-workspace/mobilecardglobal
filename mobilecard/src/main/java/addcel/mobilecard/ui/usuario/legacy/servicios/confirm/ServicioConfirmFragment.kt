package addcel.mobilecard.ui.usuario.legacy.servicios.confirm

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse
import addcel.mobilecard.data.net.pago.PagoResponse
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.data.net.transacto.entity.SaldoEntity
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureActivity
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureActivity.Companion.get
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureModel
import addcel.mobilecard.ui.usuario.billpocket.BillPocketUseCase
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import addcel.mobilecard.ui.usuario.legacy.openswitch.OpenSwitchSecureFragment.Companion.get
import addcel.mobilecard.ui.usuario.legacy.openswitch.OpenSwitchSecureModel
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.DeviceUtil.Companion.getDeviceId
import addcel.mobilecard.utils.JsonUtil
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.otto.Bus
import com.squareup.phrase.Phrase
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.view_servicios_confirm.*
import timber.log.Timber
import javax.inject.Inject

/**
 * ADDCEL on 19/10/17.
 */
class ServicioConfirmFragment : Fragment(), ServicioConfirmView {
    private val permDisposables = CompositeDisposable()

    @Inject
    lateinit var cActivity: PagoContainerActivity

    @Inject
    lateinit var bus: Bus

    @Inject
    lateinit var presenter: ServicioConfirmPresenter

    private lateinit var permissions: RxPermissions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get()
                .netComponent
                .servicioConfirmSubcomponent(ServicioConfirmModule(this))
                .inject(this)
        permissions = RxPermissions(this)
        bus.register(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_servicios_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cActivity.setStreenStatus(PagoContainerActivity.STATUS_NORMAL)
        cActivity.setAppToolbarTitle(R.string.nav_billpayments)

        b_servicio_confirm_pagar.setOnClickListener { clickPurchaseButton() }

        updateUiOnIdPais()
        permDisposables.add(
                permissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_PHONE_STATE)
                        .subscribe()
        )
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        permDisposables.clear()
        bus.unregister(this)
        super.onDestroy()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!isHidden) enablePurchaseButton()
    }

    override fun getImei(): String {
        return getDeviceId(cActivity)
    }

    override fun updateUiOnIdPais() {
        val idPais = presenter.getIdPais()
        if (idPais == 1) {
            til_servicio_confirm_balance_usd.visibility = View.GONE
            container_servicio_confirm_montos.visibility = View.GONE
        } else {
            AndroidUtils.setText(til_servicio_confirm_balance_usd.editText, presenter.getBalanceUSD())
            val sequence = Phrase.from(resources, R.string.txt_viamericas_cambio)
                    .put("send_monto", "$1.00")
                    .put("send_curr", "USD")
                    .put("receive_monto", presenter.getExchangeRate())
                    .put("receive_curr", "MXN")
                    .format()
            AndroidUtils.setText(view_servicio_confirm_exchange_rate, sequence)
        }
        AndroidUtils.setText(label_servicio_confirm_referencia, presenter.getReferenciaName())
        AndroidUtils.setText(view_servicio_confirm_referencia, presenter.getReferenciaValue())
        AndroidUtils.setText(til_servicio_confirm_balance.editText, presenter.getBalance())
        AndroidUtils.setText(til_servicio_confirm_fee.editText, presenter.getFee())
        AndroidUtils.setText(til_servicio_confirm_total.editText, presenter.getTotal())
    }


    override fun showProgress() {
        cActivity.showProgress()
    }

    override fun hideProgress() {
        cActivity.hideProgress()
    }

    override fun lockScreen() {
        cActivity.setStreenStatus(PagoContainerActivity.STATUS_BLOCKED)
    }

    override fun unlockScreen() {
        cActivity.setStreenStatus(PagoContainerActivity.STATUS_NORMAL)
    }

    override fun showError(resId: Int) {
        b_servicio_confirm_pagar.isEnabled = true
        cActivity.showError(getString(resId))
    }

    override fun showError(msg: String) {
        b_servicio_confirm_pagar.isEnabled = true
        cActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        cActivity.showSuccess(msg)
    }

    override fun onWhiteList() {
        presenter.getToken("")
    }

    override fun notOnWhiteList() {
        showError("No estás autorizado para realizar esta operación. Contacta a soporte.")
    }

    override fun onTokenReceived(tokenEntity: TokenEntity) {
        val entity = presenter.buildPagoRequest()
        if (tokenEntity.secure) {
            Timber.d("ServicioConfirmFragment - %s", tokenEntity.secure)
            val model = BillPocketSecureModel(presenter.fetchIdUsuario(), presenter.fetchCardEntity(), tokenEntity,
                    presenter.buildPagoRequestAsString(entity), BillPocketUseCase.USUARIO)
            try {
                startActivityForResult(get(cActivity, model), BillPocketSecureActivity.REQUEST_CODE)
            } catch (t: Throwable) {
                t.printStackTrace()
            }
        } else {
            presenter.executePagoBP(tokenEntity, entity)
        }
    }

    override fun onBpSuccess(response: PagoResponse) {
        cActivity.supportFragmentManager
                .beginTransaction()
                .add(R.id.activity_pago_container,
                        get(OpenSwitchSecureModel(response,
                                R.string.txt_nav_servicios, presenter.getIdPais())))
                .hide(this)
                .addToBackStack(null)
                .commit()

        enablePurchaseButton()
    }

    override fun isPurchaseButtonEnabled(): Boolean {
        return isVisible && b_servicio_confirm_pagar.isEnabled
    }

    override fun disablePurchaseButton() {
        if (isVisible) b_servicio_confirm_pagar.isEnabled = false
    }

    override fun enablePurchaseButton() {
        if (isVisible) b_servicio_confirm_pagar.isEnabled = true
    }

    override fun clickPurchaseButton() {
        if (isPurchaseButtonEnabled()) {
            disablePurchaseButton()
            presenter.checkWhiteList()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BillPocketSecureActivity.REQUEST_CODE) {
            onBillPocketSecureResult(resultCode, data)
        }
    }

    override fun onBillPocketSecureResult(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED) {
            showError("Pago cancelado por usuario")
        } else if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val resultData = data.getStringExtra(BillPocketSecureActivity.RESULT_DATA)
                if (JsonUtil.isJson(resultData)) {
                    val response = JsonUtil.fromJson(resultData, PagoResponse::class.java)
                    onBpSuccess(response)
                }
            }
        }
    }

    companion object {
        fun get(bundle: Bundle?): ServicioConfirmFragment {
            val fragment = ServicioConfirmFragment()
            fragment.arguments = bundle
            return fragment
        }

        fun get(tipo: Int, model: ServicioResponse.ServicioEntity?,
                monto: Double): ServicioConfirmFragment {
            val fragment = ServicioConfirmFragment()
            val bundle = Bundle()
            bundle.putInt("tipo", tipo)
            bundle.putParcelable("servicio", model)
            bundle.putDouble("monto", monto)
            fragment.arguments = bundle
            return fragment
        }

        fun get(tipo: Int, model: ServicioResponse.ServicioEntity?,
                saldo: SaldoEntity?): ServicioConfirmFragment {
            val fragment = ServicioConfirmFragment()
            val bundle = Bundle()
            bundle.putInt("tipo", tipo)
            bundle.putParcelable("servicio", model)
            bundle.putParcelable("saldo", saldo)
            fragment.arguments = bundle
            return fragment
        }
    }
}