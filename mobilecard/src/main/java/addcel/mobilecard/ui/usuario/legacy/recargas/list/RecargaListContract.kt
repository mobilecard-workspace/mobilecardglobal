package addcel.mobilecard.ui.usuario.legacy.recargas.list

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import addcel.mobilecard.ui.usuario.legacy.recargas.CarrierSelectionFragment
import addcel.mobilecard.ui.usuario.legacy.recargas.tae.select.RecargaFragment
import addcel.mobilecard.ui.usuario.legacy.recargas.telepeaje.select.PeajeNewFragment
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.screen_producto_list.*
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-06-15.
 */
class RecargaListFragment : Fragment(), View {

    companion object {
        fun get(
                recargas: List<CatalogoResponse.RecargaEntity>,
                pais: PaisResponse.PaisEntity?
        ): RecargaListFragment {
            val args = BundleBuilder().putParcelableArrayList(
                    "categorias",
                    recargas as ArrayList<out Parcelable>
            ).putParcelable("pais", pais).build()
            val fragment = RecargaListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var containerActivity: PagoContainerActivity
    @Inject
    lateinit var adapter: RecargaListAdapter
    @Inject
    lateinit var recargas: MutableList<CatalogoResponse.RecargaEntity>
    @Inject
    @JvmField
    var pais: PaisResponse.PaisEntity? = null
    @Inject
    lateinit var presenter: RecargaListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.recargaListSubcomponent(RecargaListModule(this)).inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): android.view.View? {
        return inflater.inflate(R.layout.screen_producto_list, container, false)
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
        containerActivity.setAppToolbarTitle(R.string.txt_nav_recargas)
        recycler_productos.adapter = adapter
        recycler_productos.layoutManager = LinearLayoutManager(view.context)
        ItemClickSupport.addTo(recycler_productos).setOnItemClickListener { _, position, _ ->
            clickRecarga(adapter.getItem(position))
        }
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_productos)
        super.onDestroyView()
    }

    override fun clickRecarga(entity: CatalogoResponse.RecargaEntity) {
        presenter.getCarriers(entity)
    }

    override fun launchEsAntad(entity: CatalogoResponse.RecargaEntity) {
    }

    override fun launchNoAntad(entity: CatalogoResponse.RecargaEntity) {
    }

    override fun showProgress() {
        containerActivity.showProgress()
    }

    override fun hideProgress() {
        containerActivity.hideProgress()
    }

    override fun showError(msg: String) {
        containerActivity.showError(msg)
    }

    override fun onProductos(
            entity: CatalogoResponse.RecargaEntity,
            productos: List<CatalogoResponse.RecargaEntity>
    ) {
        if (pais != null && pais!!.id != 7) {
            for (ent in productos) {
                if (ent.id == 2 && ent.esAntad) presenter.getMontos(entity, ent)
            }
        } else if (productos.size == 1) {
            presenter.getMontos(entity, productos[0])
        } else {
            containerActivity.fragmentManagerLazy.transaction {
                add(
                        R.id.activity_pago_container,
                        CarrierSelectionFragment.get(entity, productos, pais)
                )
                hide(this@RecargaListFragment)
                addToBackStack(null)
            }
        }
    }

    override fun onMontos(
            entity: CatalogoResponse.RecargaEntity,
            carrier: CatalogoResponse.RecargaEntity, montos: List<CatalogoResponse.MontoEntity>
    ) {

        val frag: Fragment = if (carrier.esAntad) RecargaFragment.newInstance(
                carrier, montos,
                pais
        ) else PeajeNewFragment.newInstance(carrier, montos)

        containerActivity.fragmentManagerLazy.transaction {
            add(R.id.activity_pago_container, frag)
            hide(this@RecargaListFragment)
            addToBackStack(null)
        }
    }
}

interface View {

    fun showProgress()

    fun hideProgress()

    fun showError(msg: String)

    fun clickRecarga(entity: CatalogoResponse.RecargaEntity)

    fun onProductos(
            entity: CatalogoResponse.RecargaEntity,
            productos: List<CatalogoResponse.RecargaEntity>
    )

    fun onMontos(
            entity: CatalogoResponse.RecargaEntity, carrier: CatalogoResponse.RecargaEntity,
            montos: List<CatalogoResponse.MontoEntity>
    )

    fun launchEsAntad(entity: CatalogoResponse.RecargaEntity)

    fun launchNoAntad(entity: CatalogoResponse.RecargaEntity)
}