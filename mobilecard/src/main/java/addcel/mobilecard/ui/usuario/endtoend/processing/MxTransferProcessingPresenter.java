package addcel.mobilecard.ui.usuario.endtoend.processing;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.H2HApi;
import addcel.mobilecard.data.net.hth.HTHService;
import addcel.mobilecard.data.net.hth.PaymentEntity;
import addcel.mobilecard.data.net.hth.ReceiptEntity;
import addcel.mobilecard.data.net.token.TokenBaseResponse;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.h2h.H2HPaymentInteractor;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;
import okio.ByteString;

import static addcel.mobilecard.BuildConfig.ADDCEL_APP_ID;

/**
 * ADDCEL on 22/11/17.
 */
class MxTransferProcessingPresenter implements MxTransferProcessingContract.Presenter {
    private final H2HPaymentInteractor interactor;
    private final AccountEntity account;
    private final CardEntity cardEntity;
    private final double monto;
    private final double comision;
    private final MxTransferProcessingContract.View view;

    MxTransferProcessingPresenter(H2HPaymentInteractor interactor, AccountEntity account,
                                  CardEntity cardEntity, double monto, double comision,
                                  MxTransferProcessingContract.View view) {
        this.interactor = interactor;
        this.account = account;
        this.cardEntity = cardEntity;
        this.monto = monto;
        this.comision = comision;
        this.view = view;
    }

    @Override
    public long fetchIdUsuario() {
        return interactor.getIdUsuario();
    }

    @Override
    public CardEntity fetchCard() {
        return cardEntity;
    }

    @Override
    public String getJumioRef() {
        return interactor.getScanReference();
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @Override
    public void checkWhiteList() {
        view.lockUI();
        view.showProgress();
        interactor.checkWhiteList(ADDCEL_APP_ID, interactor.getIdPais(), StringUtil.getCurrentLanguage(),
                interactor.getIdUsuario(), new InteractorCallback<TokenBaseResponse>() {
                    @Override
                    public void onSuccess(@NotNull TokenBaseResponse result) {
                        view.hideProgress();
                        view.unLockUI();
                        if (result.getCode() == 0) view.onWhiteList();
                        else view.notOnWhiteList();
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.unLockUI();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void getToken(String profile) {
        view.lockUI();
        view.showProgress();

        String auth = AddcelCrypto.encryptSensitive(JsonUtil.toJson(buildRequestModel()));

        interactor.getToken(ADDCEL_APP_ID, interactor.getIdPais(), StringUtil.getCurrentLanguage(),
                auth, profile, new InteractorCallback<TokenEntity>() {
                    @Override
                    public void onSuccess(@NotNull TokenEntity result) {
                        view.hideProgress();
                        view.unLockUI();
                        view.onTokenReceived(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.unLockUI();
                        view.showError(error);
                    }
                });
    }

    @Override
    public PaymentEntity buildRequestModel() {

        return new PaymentEntity(
                interactor.getIdUsuario(),
                cardEntity.getIdTarjeta(),
                account.getId(),
                monto,
                PaymentEntity.CONCEPTO,
                comision,
                PaymentEntity.ID_PROVIDER,
                PaymentEntity.ID_PROVIDER,
                view.getImei(),
                view.getSoftware(),
                view.getModel(),
                interactor.getLocation().getLat(),
                interactor.getLocation().getLon(),
                String.valueOf(account.getId()),
                String.valueOf(PaymentEntity.ID_PROVIDER));
    }

    @Override
    public String buildRequestString(PaymentEntity model) {
        return AddcelCrypto.encryptSensitive(JsonUtil.toJson(model));
    }

    @Override
    public byte[] buildRequestArray(PaymentEntity model) {
        return ByteString.encodeUtf8("json=" + buildRequestString(model)).toByteArray();
    }

    @Override
    public void enqueuePayment(TokenEntity tokenEntity, PaymentEntity paymentEntity) {
        view.lockUI();
        view.showProgress();
        interactor.pagoBP(ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), interactor.getIdPais(),
                tokenEntity.getAccountId(), tokenEntity.getToken(), paymentEntity,
                new InteractorCallback<ReceiptEntity>() {
                    @Override
                    public void onSuccess(@NotNull ReceiptEntity result) {
                        view.hideProgress();
                        view.unLockUI();
                        view.onSuccess(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.unLockUI();
                        view.showError(error);
                    }
                });
    }

    @Override
    public String getStartUrl() {
        return BuildConfig.BASE_URL + HTHService.ENQUEUE_PAYMENT.replace("{idApp}",
                String.valueOf(ADDCEL_APP_ID));
    }

    @Override
    public String getFormUrl() {
        return StringUtil.sha256("WHATEVERTHEHELL");
    }

    @Override
    public String getSuccessUrl() {
        return H2HApi.Companion.PROCESS_FINISHED.replace("{idApp}", String.valueOf(ADDCEL_APP_ID));
    }

    @Override
    public String getErrorUrl() {
        return StringUtil.sha256("WHATEVERTHEHELL");
    }
}
