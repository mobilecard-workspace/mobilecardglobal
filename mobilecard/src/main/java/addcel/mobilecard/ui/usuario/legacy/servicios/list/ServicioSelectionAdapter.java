package addcel.mobilecard.ui.usuario.legacy.servicios.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.utils.MapUtils;

/**
 * ADDCEL on 14/12/16.
 */
public final class ServicioSelectionAdapter
        extends RecyclerView.Adapter<ServicioSelectionAdapter.ViewHolder> {

    private final List<ServicioResponse.ServicioEntity> servicios;
    private final Map<Integer, Integer> iconos;

    ServicioSelectionAdapter(List<ServicioResponse.ServicioEntity> servicios,
                             Map<Integer, Integer> iconos) {
        this.servicios = servicios;
        this.iconos = iconos;
    }

    public void update(List<ServicioResponse.ServicioEntity> servicios) {
        this.servicios.clear();
        this.servicios.addAll(servicios);
        notifyDataSetChanged();
    }

    public void add(ServicioResponse.ServicioEntity servicio, int pos) {
        this.servicios.add(pos, servicio);
        notifyItemInserted(pos);
    }

    public ServicioResponse.ServicioEntity getItem(int pos) {
        return servicios.get(pos);
    }

    @Override
    public ServicioSelectionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_servicio, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ServicioSelectionAdapter.ViewHolder holder, int position) {
        ServicioResponse.ServicioEntity servicio = servicios.get(position);
        holder.text.setText(servicio.toString());
        holder.logo.setImageResource(
                MapUtils.getOrDefault(iconos, servicio.getId(), R.drawable.servicios));
    }

    @Override
    public int getItemCount() {
        return servicios.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView text;
        private final ImageView logo;

        ViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text_servicio);
            logo = itemView.findViewById(R.id.img_servicio_logo);
        }
    }
}
