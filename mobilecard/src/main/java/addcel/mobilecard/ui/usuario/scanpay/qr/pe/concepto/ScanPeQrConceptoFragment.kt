package addcel.mobilecard.ui.usuario.scanpay.qr.pe.concepto

import addcel.mobilecard.data.net.qr.QrHexaEntity
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.listener.NumberKeyboardListener
import addcel.mobilecard.ui.usuario.scanpay.ScanPayActivity
import addcel.mobilecard.ui.usuario.wallet.select.WalletSelectFragment
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.DeviceUtil
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_scanpay_qr_pe_concepto.*

/**
 * ADDCEL on 2019-07-17.
 */

@Parcelize
data class ScanPeQrConceptoData(
        val qrHexa: String, val qrEntity: QrHexaEntity,
        val monto: String
) : Parcelable

interface View : ScreenView {
    fun launchWallet()
}

class ScanPeQrConceptoFragment : Fragment(), View {

    companion object {
        fun get(configData: ScanPeQrConceptoData): ScanPeQrConceptoFragment {
            val args = BundleBuilder().putParcelable("data", configData).build()
            val frag = ScanPeQrConceptoFragment()
            frag.arguments = args

            return frag
        }
    }

    lateinit var configData: ScanPeQrConceptoData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configData = arguments?.getParcelable("data")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): android.view.View? {
        return inflater.inflate(
                addcel.mobilecard.R.layout.screen_scanpay_qr_pe_concepto, container,
                false
        )
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view_scanpay_qr_pe_concepto_establecimiento.text = configData.qrEntity.businessName
        text_scanpay_qr_pe_concepto_monto.text =
                NumberKeyboardListener.getFormattedMonto(configData.monto, 4)
        b_scanpay_qr_pe_concepto_continuar.setOnClickListener {
            launchWallet()
        }
    }

    override fun launchWallet() {

        val dMonto = configData.monto.toDouble() / 100
        val comision = (dMonto * .01)
        val impuesto = comision * .18
        val comisionTotal = 0.0
        //comision + impuesto
        val concepto = AndroidUtils.getText(text_scanpay_qr_pe_concepto_concepto)
        val propina = AndroidUtils.getText(text_scanpay_qr_pe_concepto_propina)
        val dPropina = if (propina.isEmpty() || !propina.isDigitsOnly()) 0.0 else propina.toDouble()
        val refNegocio = configData.qrEntity.businessName

        val pagoEntity =
                SPPagoEntity(
                        dMonto,
                        comisionTotal,
                        concepto,
                        "",
                        0,
                        "",
                        0,
                        0,
                        0,
                        DeviceUtil.getDeviceId(context!!),
                        0.0,
                        0.0,
                        Build.MODEL,
                        0,
                        dPropina,
                        refNegocio,
                        Build.VERSION.SDK_INT.toString(),
                        "",
                        "",
                        "",
                        configData.qrHexa,
                        "",
                        "",
                        "",
                        ""
                )

        fragmentManager?.transaction {
            add(
                    addcel.mobilecard.R.id.frame_scanpay,
                    WalletSelectFragment.get(
                            WalletSelectFragment.SCANPAY_QR_PE,
                            BundleBuilder().putParcelable("pago", pagoEntity).build()
                    )
            )
            hide(this@ScanPeQrConceptoFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    override fun showProgress() {
        (activity as ScanPayActivity).showProgress()
    }

    override fun hideProgress() {
        (activity as ScanPayActivity).hideProgress()
    }

    override fun showError(msg: String) {
        (activity as ScanPayActivity).showError(msg)
    }

    override fun showSuccess(msg: String) {
        (activity as ScanPayActivity).showSuccess(msg)
    }
}