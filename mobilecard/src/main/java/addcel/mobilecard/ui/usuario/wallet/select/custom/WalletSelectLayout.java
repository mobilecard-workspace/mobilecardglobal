package addcel.mobilecard.ui.usuario.wallet.select.custom;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.wallet.select.WalletCardSelectModel;

/**
 * ADDCEL on 24/08/18.
 */
public class WalletSelectLayout extends ConstraintLayout
        implements WalletSelectCustomContract.View {

    @Inject
    WalletSelectCustomAdapter adapter;
    @Inject
    WalletSelectCustomContract.Presenter presenter;
    private RecyclerView cardsView;
    private SelectedListener listener;

    public WalletSelectLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        Mobilecard.get()
                .getNetComponent()
                .walletSelectCustomSubcomponent(new WalletSelectCustomModule(this))
                .inject(this);
    }

    public void setListener(@NonNull SelectedListener listener) {
        this.listener = listener;
    }

    public void removeListener() {
        this.listener = null;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        presenter.getCards();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        cardsView = findViewById(R.id.select_wallet_screen_recycler);
        cardsView.setAdapter(adapter);
        cardsView.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        ItemClickSupport.addTo(cardsView)
                .setOnItemClickListener((recyclerView, position, v) -> clickChecked(position));
    }

    @Override
    protected void onDetachedFromWindow() {
        presenter.clearDisposables();
        if (cardsView != null) {
            ItemClickSupport.removeFrom(cardsView);
            if (cardsView.getAdapter() != null) cardsView.setAdapter(null);
        }
        super.onDetachedFromWindow();
    }

    @Override
    public void clickAddNewCard() {
    }

    @Override
    public void setCards(List<CardEntity> models) {
        List<WalletCardSelectModel> selectModels = Lists.newArrayList();
        for (CardEntity model : models) {
            selectModels.add(new WalletCardSelectModel(model, model.getDeterminada()));
        }
        adapter.update(selectModels);
    }

    @Override
    public void clickChecked(int pos) {
        adapter.setChecked(pos);
        int selectedPos = adapter.getCheckedCardPosition();
        if (listener != null) {
            if (selectedPos > -1) listener.onSelected(adapter.getItem(selectedPos).getCard());
        }
    }

    @Override
    public WalletCardSelectModel getSelectedCard() {
        int selectedCardPos = adapter.getCheckedCardPosition();
        return selectedCardPos == -1 ? null : adapter.getItem(selectedCardPos);
    }

    public interface SelectedListener {
        void onSelected(CardEntity card);
    }
}
