package addcel.mobilecard.ui.usuario.viamericas.confirmation

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.data.net.viamericas.model.*
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.FranquiciaEntity
import addcel.mobilecard.ui.Authenticable
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.usuario.viamericas.ViamericasTransactionElementAdapter
import addcel.mobilecard.ui.usuario.viamericas.result.ViamericasResultFragment
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.os.Parcelable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.google.common.base.Strings
import com.squareup.phrase.Phrase
import kotlinx.android.parcel.Parcelize
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber
import java.text.NumberFormat
import javax.annotation.Nullable
import javax.inject.Inject

/**
 * ADDCEL on 25/05/17.
 */
@Parcelize
data class ViamericasConfirmModel(
        val recipient: RecipientResponse, val sendAmount: Double,
        val currencies: List<CurrencyModel>, val location: LocationModel,
        val paymentMode: PaymentModeModel, val exchangeRate: ExchangeRateModel, val fees: OrderFeeModel,
        val orderResponse: OrderResponse,
        val creditCard: CardEntity
) : Parcelable


interface VmConfirmView : ScreenView, Authenticable {
    fun setRecipientNameText(response: RecipientResponse)

    fun setAmountToSendText(amount: Double, curr: CurrencyModel)

    fun setAmountToReceiveText(amount: Double, curr: CurrencyModel, exchangeRate: ExchangeRateModel)

    fun setReceiveNetworkText(location: LocationModel)

    fun setPaymentMethodText(paymode: PaymentModeModel)

    fun setTarjetaText(card: CardEntity)

    fun setFees(fees: OrderFeeModel)

    fun postOrderConfirmed(confirmationResponse: CreditTransactionConfirmationResponse)

    fun clickSend()

    fun launchWhiteList()
}

class ViamericasConfirmFragment : Fragment(), VmConfirmView {
    override fun onWhiteList() {
        launchEnvio()
    }

    override fun notOnWhiteList() {
        showError("No está autorizado para realizar esta operacion. Contacte a soporte.")
    }

    override fun launchWhiteList() {
        presenter.checkWhiteList(usuario.idPais, usuario.ideUsuario)
    }


    //VIEWS
    @BindView(R.id.view_viamericas_confirm_recipient)
    lateinit var recipientView: TextView
    @BindView(R.id.view_viamericas_confirm_send)
    lateinit var sendAmountView: TextView
    @BindView(R.id.view_viamericas_confirm_receive)
    lateinit var receiveAmountView: TextView
    @BindView(R.id.view_viamericas_confirm_network)
    lateinit var locationView: TextView
    @BindView(R.id.view_viamericas_confirm_paymode)
    lateinit var paymentModeView: TextView
    @BindView(R.id.view_viamericas_confirm_tdc)
    lateinit var tdcView: TextView
    @BindView(R.id.view_viamericas_confirm_tdc_billing)
    lateinit var billingTdcView: TextView
    @BindView(R.id.recycler_viamericas_confirm_detalles)
    lateinit var detallesView: RecyclerView

    @Inject
    lateinit var containerActivity: ContainerActivity
    @Inject
    lateinit var viewModel: ViamericasConfirmModel
    @Inject
    lateinit var currencyFormat: NumberFormat
    @Inject
    lateinit var usuario: Usuario
    @Inject
    lateinit var detallesAdapter: ViamericasTransactionElementAdapter
    @Inject
    lateinit var presenter: ViamericasConfirmPresenter

    private lateinit var unbinder: Unbinder

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get()
                .netComponent
                .viamericasConfirmSubcomponent(ViamericasConfirmModule(this))
                .inject(this)
    }

    @Nullable
    override fun onCreateView(
            inflater: LayoutInflater, @Nullable container: ViewGroup?,
            @Nullable savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_viamericas_confirm, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)

        detallesView.adapter = detallesAdapter
        detallesView.addItemDecoration(
                DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL)
        )
        detallesView.layoutManager = LinearLayoutManager(view.context)

        containerActivity.setStreenStatus(ContainerScreenView.STATUS_NORMAL)
        setRecipientNameText(viewModel.recipient)
        setAmountToSendText(viewModel.sendAmount, viewModel.currencies[0])
        setAmountToReceiveText(
                viewModel.sendAmount,
                viewModel.currencies[viewModel.currencies.size - 1],
                viewModel.exchangeRate
        )
        setReceiveNetworkText(viewModel.location)
        setPaymentMethodText(viewModel.paymentMode)
        setTarjetaText(viewModel.creditCard)

        setFees(viewModel.fees)
    }

    override fun onDestroyView() {
        unbinder.unbind()
        super.onDestroyView()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun showProgress() {
        containerActivity.showProgress()
    }

    override fun hideProgress() {
        containerActivity.hideProgress()
    }

    override fun showError(msg: String) {
        containerActivity.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerActivity.showSuccess(msg)
    }

    override fun setRecipientNameText(response: RecipientResponse) {
        val name = Phrase.from(containerActivity, R.string.txt_viamericas_confirm_recipient)
                .put("recipient_name", response.fName + " " + response.lName)
                .format()

        recipientView.text = name
    }

    override fun setAmountToSendText(amount: Double, curr: CurrencyModel) {
        val amountAndCurr = Phrase.from(containerActivity, R.string.txt_viamericas_confirm_send)
                .put("amount", currencyFormat.format(amount))
                .put("currency", curr.id)
                .format()
        sendAmountView.text = amountAndCurr
    }

    override fun setAmountToReceiveText(
            amount: Double,
            curr: CurrencyModel,
            exchangeRate: ExchangeRateModel
    ) {
        val amountAndCurr = Phrase.from(containerActivity, R.string.txt_viamericas_confirm_receive)
                .put("amount", currencyFormat.format(amount * exchangeRate.exchangeRate))
                .put("currency", curr.id)
                .format()
        receiveAmountView.text = amountAndCurr
    }

    override fun setReceiveNetworkText(location: LocationModel) {
        val network = Phrase.from(containerActivity, R.string.txt_viamericas_confirm_network)
                .put("network", location.nameGroup)
                .format()
        locationView.text = network
    }

    override fun setPaymentMethodText(paymode: PaymentModeModel) {
        val network = Phrase.from(containerActivity, R.string.txt_viamericas_confirm_paymode)
                .put("paymode", paymode.paymentModeName)
                .format()
        paymentModeView.text = network
    }

    override fun setTarjetaText(card: CardEntity) {
        tdcView.text = StringUtil.maskCard(AddcelCrypto.decryptHard(card.pan))
        val franquiciaDrawable = presenter.getTdcFranquiciaDrawable(
                viewModel.creditCard.tipo
                        ?: FranquiciaEntity.VISA
        )
        tdcView.setCompoundDrawablesRelativeWithIntrinsicBounds(franquiciaDrawable, 0, 0, 0)
        tdcView.compoundDrawablePadding = 8

        val cardSeq = Phrase.from(containerActivity, R.string.txt_viamericas_confirm_tdc_billing)
                .put("domicilio", Strings.nullToEmpty(card.domAmex))
                .format()
        billingTdcView.text = cardSeq
    }

    override fun setFees(fees: OrderFeeModel) {
        detallesAdapter.update(
                viewModel.sendAmount,
                fees,
                viewModel.currencies,
                viewModel.exchangeRate,
                currencyFormat
        )
    }

    override fun postOrderConfirmed(confirmationResponse: CreditTransactionConfirmationResponse) {
        Timber.d(confirmationResponse.responseMessage)
        containerActivity.fragmentManagerLazy.commit {
            add(R.id.frame_viamericas, ViamericasResultFragment.get(confirmationResponse))
            hide(this@ViamericasConfirmFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    @OnClick(R.id.b_viamericas_confirm_enviar)
    override fun clickSend() {
        launchWhiteList()
    }

    private fun launchEnvio() {
        val holderInfo = TextUtils.split(viewModel.creditCard.nombre, "\\s+")

        val confirmation = CreditTransactionConfirmationModel.Builder().setIdBitacora(
                viewModel.orderResponse.idBitacora
        )
                .setCardFirstName(holderInfo[0])
                .setCardLastName(holderInfo[1])
                .setCardNumber(AddcelCrypto.decryptHard(viewModel.creditCard.pan))
                .setCvv(
                        if (viewModel.creditCard.codigo.orEmpty().isNotBlank()) AddcelCrypto.decryptHard(
                                viewModel.creditCard.codigo
                        )
                        else viewModel.creditCard.codigo
                )
                .setExpDate(AddcelCrypto.decryptHard(viewModel.creditCard.vigencia).replace("/", ""))
                .setIdReciever(viewModel.orderResponse.idReceiver)
                .setIdSender(viewModel.orderResponse.idSender)
                .setNickName(StringUtil.trimLoginIfEmail(usuario.usrLogin))
                .setPaymentType(viewModel.creditCard.tipoTarjeta?.symbol ?: "K")
                .setIdTarjeta(viewModel.creditCard.idTarjeta)
                .setIdUsuario(usuario.ideUsuario)
                .build()
        presenter.confirmOrder(confirmation)
    }


    companion object {
        fun get(model: ViamericasConfirmModel): ViamericasConfirmFragment {
            val bundle = Bundle()
            bundle.putParcelable("model", model)
            val fragment = ViamericasConfirmFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
