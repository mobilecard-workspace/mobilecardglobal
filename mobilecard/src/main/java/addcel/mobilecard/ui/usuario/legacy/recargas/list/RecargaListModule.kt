package addcel.mobilecard.ui.usuario.legacy.recargas.list

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.usuario.legacy.PagoContainerActivity
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * ADDCEL on 2019-06-15.
 */
@Module
class RecargaListModule(val fragment: RecargaListFragment) {
    @PerFragment
    @Provides
    fun provideActivity(): PagoContainerActivity {
        return fragment.activity as PagoContainerActivity
    }

    @PerFragment
    @Provides
    fun provideCategorias(): List<CatalogoResponse.RecargaEntity> {
        return fragment.arguments?.getParcelableArrayList("categorias")!!
    }

    @PerFragment
    @Provides
    fun providePais(): PaisResponse.PaisEntity? {
        return fragment.arguments?.getParcelable("pais")
    }

    @PerFragment
    @Provides
    fun provideAdapter(
            data: List<CatalogoResponse.RecargaEntity>
    ): RecargaListAdapter {
        return RecargaListAdapter(data)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            api: CatalogoService,
            session: SessionOperations
    ): RecargaListPresenter {
        return RecargaListPresenterImpl(api, session.usuario, CompositeDisposable(), fragment)
    }
}