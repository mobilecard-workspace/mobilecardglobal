package addcel.mobilecard.ui.usuario.viamericas.fees

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.viamericas.ViamericasService
import addcel.mobilecard.data.net.viamericas.model.OrderFeeRequest
import addcel.mobilecard.data.net.viamericas.model.OrderModel
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import android.content.Context
import android.location.Address
import android.location.Geocoder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

/**
 * ADDCEL on 15/05/17.
 */


interface ViamericasFeePresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun buildDefaultAddress(): Address

    fun getOrderFees(request: OrderFeeRequest)

    fun createOrder(order: OrderModel)

    fun getCurrentAddress(context: Context, locationData: McLocationData)

    fun trimLocation(location: Double): String
}


class ViamericasFeePresenterImpl(
        private val viamericasService: ViamericasService,
        private val disposables: CompositeDisposable,
        private val view: VmFeeView
) : ViamericasFeePresenter {


    override fun buildDefaultAddress(): Address {
        val a = Address(Locale.US)
        a.latitude = 0.0
        a.longitude = 0.0
        a.setAddressLine(0, "")
        return a
    }

    override fun getOrderFees(request: OrderFeeRequest) {
        val body = JsonUtil.toJson(request)
        Timber.d(body)
        view.showProgress()
        val oDisp = viamericasService
                .getOrderFees(
                        ViamericasService.AUTH,
                        BuildConfig.ADDCEL_APP_ID,
                        StringUtil.getCurrentLanguage(),
                        body
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) view.setFees(it.orders[0])
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })

        addToDisposables(oDisp)
    }

    override fun createOrder(order: OrderModel) {
        view.showProgress()
        val json = JsonUtil.toJson(order)
        Timber.d("json orden: %s", json)

        val noDisp = viamericasService.createNewOrder(
                ViamericasService.AUTH,
                BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                json
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.hideProgress()
                            if (it.idError == 0) view.postOrderCreated(it)
                            else view.showError(it.mensajeError)
                        },
                        {
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        }
                )
        addToDisposables(noDisp)
    }

    override fun getCurrentAddress(context: Context, locationData: McLocationData) {
        Timber.d("getCurrentAddress")

        val geocoder = Geocoder(context, Locale.US)
        val aDisp = getFromLocation(geocoder, locationData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.isNotEmpty()) view.postAddress(it[0])
                    else view.postAddress(buildDefaultAddress())
                }, {
                    it.printStackTrace()
                    view.postAddress(buildDefaultAddress())
                })

        addToDisposables(aDisp)
    }

    private fun getFromLocation(
            geocoder: Geocoder,
            locationData: McLocationData
    ): Observable<List<Address>> {

        Timber.d("getFromLocation")

        return Observable.fromCallable {
            geocoder.getFromLocation(locationData.lat, locationData.lon, 1)
        }
    }

    override fun trimLocation(location: Double): String {

        Timber.d("trimLocation - entrada: %s", location)

        if (location.toString().contains(".")) {
            val asArr = location.toString().split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()
            return if (asArr[1].length > 8) {
                asArr[0] + "." + asArr[1].substring(0, 8)
            } else asArr[0] + "." + asArr[1]
        }

        val locationS = location.toString()
        Timber.d("trimLocation - salida: %s", locationS)
        return locationS
    }

    override fun addToDisposables(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun clearDisposables() {
        disposables.clear()
    }
}
