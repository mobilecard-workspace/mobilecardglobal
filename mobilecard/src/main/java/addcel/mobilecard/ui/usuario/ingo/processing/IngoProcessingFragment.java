package addcel.mobilecard.ui.usuario.ingo.processing;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.common.base.Strings;
import com.ingomoney.ingosdk.android.IngoSdkManager;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.ui.usuario.ingo.IngoActivity;
import addcel.mobilecard.utils.DeviceUtil;

/**
 * ADDCEL on 19/07/18.
 */
public class IngoProcessingFragment extends Fragment implements IngoProcessingContract.View {

    @Inject
    IngoActivity parentActivity;
    @Inject
    IngoProcessingContract.Presenter presenter;

    public IngoProcessingFragment() {
    }

    public static synchronized IngoProcessingFragment get(Bundle arguments) {
        IngoProcessingFragment fragment = new IngoProcessingFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mobilecard.get()
                .getNetComponent()
                .ingoProcessingSubcomponent(new IngoProcessingModule(this))
                .inject(this);
        parentActivity.setFinished(true);

        presenter.checkWhiteList();
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        parentActivity.showProgress();
    }

    @Override
    public void hideProgress() {
        parentActivity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        parentActivity.getSupportFragmentManager().beginTransaction().remove(this).commit();
        parentActivity.showError(msg);
    }

    @Override
    public void showCountryError(String msg) {
        if (parentActivity != null) {
            new AlertDialog.Builder(parentActivity).setMessage(msg)
                    .setPositiveButton(R.string.action_settings, (dialog, which) -> {
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        startActivity(intent);
                    })
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                        dialog.dismiss();
                        parentActivity.finish();
                    })
                    .show();
        }
    }

    @Override
    public void showSuccess(String msg) {
        parentActivity.showSuccess(msg);
    }

    @Override
    public void onTarjetaAddedOrUpdated() {
        presenter.checkIfIngoAvailable();
    }

    @Override
    public void onTarjetaNotAdded() {
    }

    @Override
    public void launchIngo() {
        if (parentActivity.getIngoSdkManager() != null) {
            launchIngoImpl(parentActivity, parentActivity.getIngoSdkManager(), presenter.getCustomerId(), presenter.getSsoToken());
        } else {
            parentActivity.showError(getString(R.string.txt_ingo_error_compatibility));
        }
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(parentActivity);
    }

    private void launchIngoImpl(@NonNull IngoActivity activity,
                                @NonNull IngoSdkManager ingoSdkManager, String customerId, String ssoToken) {
        try {
            ingoSdkManager.begin(activity, customerId, ssoToken);
        } catch (Throwable t) {
            t.printStackTrace();
            activity.showError(Strings.nullToEmpty(t.getLocalizedMessage()));
            activity.kill();
        }
    }

    @Override
    public void onWhiteList() {
        presenter.addOrUpdateTarjeta();
    }

    @Override
    public void notOnWhiteList() {
        showError("No estás autorizado para realizar esta operación. Contacta a soporte.");
    }
}
