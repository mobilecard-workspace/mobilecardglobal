package addcel.mobilecard.ui.usuario.legacy.recargas.tae.select;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 15/12/16.
 */
@PerFragment
@Subcomponent(modules = RecargaModule.class)
public interface RecargaSubcomponent {
    void inject(RecargaFragment fragment);
}
