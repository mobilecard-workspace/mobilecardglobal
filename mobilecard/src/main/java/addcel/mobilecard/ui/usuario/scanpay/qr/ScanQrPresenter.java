package addcel.mobilecard.ui.usuario.scanpay.qr;

import androidx.annotation.NonNull;

import com.google.common.base.Strings;
import com.google.gson.JsonSyntaxException;

import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.data.net.scanpay.model.SPQrEntity;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.JsonUtil;
import mx.mobilecard.crypto.AddcelCrypto;
import timber.log.Timber;

/**
 * ADDCEL on 21/08/18.
 */
class ScanQrPresenter implements ScanQrContract.Presenter {
    private final ScanQrContract.View view;
    private final Usuario usuario;

    ScanQrPresenter(ScanPayService service, Usuario usuario, ScanQrContract.View view) {
        this.usuario = usuario;
        this.view = view;
    }

    @Override
    public void validateScanResult(@NonNull String result) {
        if (Strings.isNullOrEmpty(result)) {
            view.showError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.OPERATION));
        } else {
            Timber.d("QR Scan %s", result);
            try {
                final String cleanQR = AddcelCrypto.decryptHard(result);
                SPQrEntity qrEntity = JsonUtil.fromJson(cleanQR, SPQrEntity.class);
                view.onScanSuccess(qrEntity);
            } catch (NullPointerException | JsonSyntaxException | IllegalStateException e) {
                view.showError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.OPERATION));
            }
        }
    }

    @Override
    public long getIdUsuario() {
        return usuario.getIdeUsuario();
    }

    @Override
    public int getIdPais() {
        return usuario.getIdPais();
    }
}
