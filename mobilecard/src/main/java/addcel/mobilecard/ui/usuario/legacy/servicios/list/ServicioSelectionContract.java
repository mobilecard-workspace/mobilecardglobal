package addcel.mobilecard.ui.usuario.legacy.servicios.list;

import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 14/12/16.
 */
public interface ServicioSelectionContract {
    interface View extends ScreenView {
        void disableList();

        void enableList();

        void onItemClicked(ServicioResponse.ServicioEntity servicio);
    }
}
