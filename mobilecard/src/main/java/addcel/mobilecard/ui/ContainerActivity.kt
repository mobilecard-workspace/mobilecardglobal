package addcel.mobilecard.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager

import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * ADDCEL on 3/14/19.
 */
abstract class ContainerActivity : AppCompatActivity(), ContainerScreenView {

    private var screenStatus: Int = 0
    private var fragmentManager: FragmentManager? = null
    private val contCompDisposable = CompositeDisposable()

    override fun getScreenStatus(): Int {
        return screenStatus
    }

    override fun setStreenStatus(status: Int) {
        this.screenStatus = status
    }

    override fun getFragmentManagerLazy(): FragmentManager {
        if (fragmentManager == null) {
            fragmentManager = supportFragmentManager
        }
        return fragmentManager!!
    }

    override fun pressBack() {
        hideRetry()

        when (screenStatus) {
            ContainerScreenView.STATUS_NORMAL -> super.onBackPressed()
            ContainerScreenView.STATUS_FINISHED -> finish()
            else -> {
            }
        }
    }

    override fun onDestroy() {
        contCompDisposable.clear()
        super.onDestroy()
    }

    override fun onBackPressed() {
        pressBack()
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun addToDisposables(disposable: Disposable) {
        contCompDisposable.add(disposable)
    }
}
