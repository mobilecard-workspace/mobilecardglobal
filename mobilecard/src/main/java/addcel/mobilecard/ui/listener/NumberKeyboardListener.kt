package addcel.mobilecard.ui.listener

import addcel.mobilecard.McConstants
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.utils.StringUtil
import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 2019-07-12.
 */
interface NumberKeyboardListener {

    companion object {

        fun getFormattedMonto(monto: String, idPais: Int): String {
            return if (monto.isNotEmpty() && StringUtil.isDecimalAmount(monto)) {
                val locale = getLocaleByIdPais(idPais)
                val formatter = NumberFormat.getCurrencyInstance(locale)
                formatter.format(processMonto(monto))
            } else {
                ""
            }
        }

        fun getFormattedMontoDecimal(monto: String, idPais: Int): String {
            val locale = getLocaleByIdPais(idPais)
            val formatter = NumberFormat.getCurrencyInstance(locale)
            return formatter.format(processMontoDecimal(monto))
        }

        private fun getLocaleByIdPais(idPais: Int): Locale {
            return when (idPais) {
                McConstants.PAIS_ID_MX -> McConstants.LOCALE_MX
                McConstants.PAIS_ID_CO -> McConstants.LOCALE_COL
                McConstants.PAIS_ID_USA -> McConstants.LOCALE_USA
                McConstants.PAIS_ID_PE -> McConstants.LOCALE_PE
                else -> {
                    McConstants.LOCALE_USA
                }
            }
        }

        private fun processMonto(monto: String): Double {
            return monto.toDouble() / 100
        }

        private fun processMontoDecimal(monto: String): Double {
            if (monto.isEmpty()) return 0.0
            return monto.toDouble()
        }
    }

    fun onKeyStroke(captured: String)

    fun onDeleteStroke()
}