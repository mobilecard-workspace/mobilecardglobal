package addcel.mobilecard.ui.keyboard.wallet;

import addcel.mobilecard.di.scope.PerView;
import dagger.Subcomponent;

/**
 * ADDCEL on 14/08/18.
 */
@PerView
@Subcomponent(modules = KeyboardWalletModule.class)
public interface KeyboardWalletSubcomponent {
    void inject(KeyboardWalletView view);
}
