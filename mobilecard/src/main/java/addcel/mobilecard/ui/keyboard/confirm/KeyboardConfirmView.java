package addcel.mobilecard.ui.keyboard.confirm;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.squareup.phrase.Phrase;

import java.text.NumberFormat;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 15/08/18.
 */
public final class KeyboardConfirmView extends ConstraintLayout
        implements KeyboardConfirmContract.View {

    @Inject
    KeyboardConfirmContract.Presenter presenter;
    @Inject
    NumberFormat format;

    private AccountEntity beneficiario;
    private double monto;
    private CardEntity card;

    public KeyboardConfirmView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Mobilecard.get()
                    .getNetComponent()
                    .keyboardConfirmSubcomponent(new KeyboardConfirmModule(this))
                    .inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViewById(R.id.b_keyboard_confirm_continuar).setOnClickListener(view -> clickContinuar());
        findViewById(R.id.b_keyboard_confirm_back).setOnClickListener(view -> clickBack());
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {

            ((TextView) findViewById(R.id.text_keyboard_confirm_monto)).setText(format.format(monto));

            CharSequence msg = Phrase.from(this, R.string.txt_transfermx_confirm)
                    .put("card", StringUtil.maskCard(AddcelCrypto.decryptHard(card.getPan()), "*"))
                    .put("nombre", beneficiario.getAlias())
                    .format();
            ((TextView) findViewById(R.id.text_keyboard_confirm_msg)).setText(msg);

            presenter.register();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (presenter != null) presenter.unregister();
        super.onDetachedFromWindow();
    }

    public NumberFormat getFormat() {
        return format;
    }

    @Override
    public AccountEntity getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(AccountEntity beneficiario) {
        this.beneficiario = beneficiario;
    }

    @Override
    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    @Override
    public CardEntity getCard() {
        return card;
    }

    public void setCard(CardEntity card) {
        this.card = card;
    }

    @Override
    public void clickBack() {
        presenter.onBackClick();
    }

    @Override
    public void clickContinuar() {
        presenter.getToken("");
    }

    @Override
    public void showProgress() {
        if (!isInEditMode()) findViewById(R.id.progress_keyboard_success).setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (!isInEditMode()) findViewById(R.id.progress_keyboard_success).setVisibility(GONE);
    }
}
