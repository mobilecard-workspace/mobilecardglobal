package addcel.mobilecard.ui.keyboard.beneficiarios;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.base.Optional;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.H2HApi;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.usuario.endtoend.MxTransferActivity;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 13/08/18.
 */
public class KeyboardBeneficiariosSelectView extends ConstraintLayout {
    @Inject
    H2HApi api;
    @Inject
    Optional<Usuario> usuario;
    @Inject
    KeyboardBeneficiarioSelectAdapter adapter;
    @Inject
    Bus bus;

    public KeyboardBeneficiariosSelectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Mobilecard.get()
                .getNetComponent()
                .keyboardBeneficiariosSelectSubcomponent(new KeyboardBeneficiariosSelectModule())
                .inject(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        RecyclerView accountsView = findViewById(R.id.recycler_keyboard_beneficiarios_select);
        accountsView.setAdapter(adapter);
        accountsView.setLayoutManager(new LinearLayoutManager(getContext()));
        accountsView.addItemDecoration(
                new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        ItemClickSupport.addTo(accountsView)
                .setOnItemClickListener((recyclerView, position, v) -> bus.post(
                        new KeyboardBeneficiariosSelectEvent(adapter.getItem(position))));

        findViewById(R.id.b_keyboard_beneficiarios_select_add).setOnClickListener(
                view -> view.getContext()
                        .startActivity(MxTransferActivity.getforKeyBoard(view.getContext(), Boolean.TRUE)));
    }

    @SuppressLint("CheckResult")
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (adapter.getItemCount() == 0) {
            api.toAccount(BuildConfig.ADDCEL_APP_ID, usuario.or(new Usuario()).getIdeUsuario(),
                    StringUtil.getCurrentLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> adapter.update(response.getAccounts()));
        }
    }
}
