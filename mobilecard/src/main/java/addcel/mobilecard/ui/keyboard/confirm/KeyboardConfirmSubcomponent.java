package addcel.mobilecard.ui.keyboard.confirm;

import addcel.mobilecard.di.scope.PerView;
import dagger.Subcomponent;

/**
 * ADDCEL on 15/08/18.
 */
@PerView
@Subcomponent(modules = KeyboardConfirmModule.class)
public interface KeyboardConfirmSubcomponent {
    void inject(KeyboardConfirmView view);
}
