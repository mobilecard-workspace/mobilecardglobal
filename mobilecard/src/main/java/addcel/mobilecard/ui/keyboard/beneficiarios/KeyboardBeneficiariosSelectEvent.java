package addcel.mobilecard.ui.keyboard.beneficiarios;

import addcel.mobilecard.data.net.hth.AccountEntity;

/**
 * ADDCEL on 14/08/18.
 */
public final class KeyboardBeneficiariosSelectEvent {
    private final AccountEntity account;

    KeyboardBeneficiariosSelectEvent(AccountEntity account) {
        this.account = account;
    }

    public AccountEntity getAccount() {
        return account;
    }
}
