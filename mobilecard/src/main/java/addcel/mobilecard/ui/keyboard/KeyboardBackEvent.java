package addcel.mobilecard.ui.keyboard;

/**
 * ADDCEL on 15/08/18.
 */
public class KeyboardBackEvent {
    private final KeyboardScreen screen;

    public KeyboardBackEvent(KeyboardScreen screen) {
        this.screen = screen;
    }

    public KeyboardScreen getScreen() {
        return screen;
    }
}
