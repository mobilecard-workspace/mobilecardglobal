package addcel.mobilecard.ui.keyboard.confirm;

import addcel.mobilecard.data.net.hth.ReceiptEntity;

/**
 * ADDCEL on 15/08/18.
 */
public final class KeyboardConfirmEvent {
    private final ReceiptEntity receipt;

    KeyboardConfirmEvent(ReceiptEntity receipt) {
        this.receipt = receipt;
    }

    public ReceiptEntity getReceipt() {
        return receipt;
    }
}
