package addcel.mobilecard.ui.keyboard.beneficiarios;

import addcel.mobilecard.di.scope.PerView;
import dagger.Subcomponent;

/**
 * ADDCEL on 13/08/18.
 */
@PerView
@Subcomponent(modules = KeyboardBeneficiariosSelectModule.class)
public interface KeyboardBeneficiariosSelectSubcomponent {
    void inject(KeyboardBeneficiariosSelectView view);
}
