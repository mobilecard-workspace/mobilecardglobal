package addcel.mobilecard.ui.keyboard.secure;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.common.base.Strings;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.R;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import dagger.Module;
import dagger.Provides;
import timber.log.Timber;

/**
 * ADDCEL on 30/08/18.
 */
@Module
public final class KeyboardWebModule {
    private final KeyboardWebActivity activity;

    KeyboardWebModule(KeyboardWebActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Bundle provideExtras() {
        return Objects.requireNonNull(activity.getIntent().getExtras());
    }

    @Provides
    @PerActivity
    @Named("titulo")
    String provideTitulo(Bundle extras) {
        return extras.getString("titulo");
    }

    @Provides
    @PerActivity
    @Named("startUrl")
    String provideStartUrl(Bundle extras) {
        return extras.getString("startUrl");
    }

    @Provides
    @PerActivity
    @Named("finishEndpoint")
    String provideFinishEndpoint(Bundle extras) {
        return extras.getString("finishEndpoint");
    }

    @Provides
    @PerActivity
    AlertDialog provideWarningDialog() {
        DetachableClickListener okWrapper =
                DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                });

        DetachableClickListener cancelWrapper =
                DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog =
                new AlertDialog.Builder(activity).setTitle(android.R.string.dialog_alert_title)
                        .setMessage(R.string.txt_pago_warning)
                        .setPositiveButton(android.R.string.yes, okWrapper)
                        .setNegativeButton(android.R.string.cancel, cancelWrapper)
                        .create();

        okWrapper.clearOnDetach(dialog);
        cancelWrapper.clearOnDetach(dialog);

        return dialog;
    }

    @Provides
    @PerActivity
    @Named("clipboardDialog")
    AlertDialog provideClipboardDialog() {
        DetachableClickListener okWrapper =
                DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        activity.sendTextToClipboard();
                    }
                });

        DetachableClickListener cancelWrapper =
                DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog =
                new AlertDialog.Builder(activity).setTitle(android.R.string.dialog_alert_title)
                        .setMessage(R.string.txt_keyboard_web_clipboard)
                        .setPositiveButton(android.R.string.yes, okWrapper)
                        .setNegativeButton(android.R.string.cancel, cancelWrapper)
                        .create();

        okWrapper.clearOnDetach(dialog);
        cancelWrapper.clearOnDetach(dialog);
        return dialog;
    }

    @Provides
    @PerActivity
    byte[] provideRequest(Bundle extras) {
        return extras.getByteArray("data");
    }

    @Provides
    @PerActivity
    CookieManager provideManager() {
        return CookieManager.getInstance();
    }

    @Provides
    @PerActivity
    KeyboardWebContract.Presenter providePresenter() {
        return new KeyboardWebPresenter(activity);
    }

    @Provides
    @PerActivity
    WebViewClient provideWebClient() {
        return new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                activity.showProgress();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Timber.d("Cargando URL: %s", url);
                activity.hideProgress();
                view.evaluateJavascript(KeyboardWebContract.PROCESS_HTML_FUN, null);
                activity.launchFinishedCheck(url);
                activity.setCurrentUrl(url);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                final String errorStr = Strings.nullToEmpty(TextUtils.substring(error.toString(), 0, 100));
                activity.hideProgress();
                if (BuildConfig.DEBUG) {
                    handler.proceed();
                } else {
                    activity.showError(errorStr);
                    activity.finish();
                }
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request,
                                            WebResourceResponse errorResponse) {
                activity.hideProgress();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (errorResponse.getStatusCode() != 404 && errorResponse.getStatusCode() != 403) {
                        activity.showError(errorResponse.getReasonPhrase());
                        activity.finish();
                    }
                }
            }
        };
    }

    @SuppressLint({"SetJavaScriptEnabled", "ClickableViewAccessibility"})
    @Provides
    @PerActivity
    WebView provideBrowser(WebViewClient client, CookieManager manager) {
        final WebView browser = new WebView(activity);
        browser.setWebViewClient(client);
        browser.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        browser.requestFocus(View.FOCUS_DOWN);
        browser.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!v.hasFocus()) {
                        v.requestFocus();
                    }
                }
                return false;
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager.setAcceptThirdPartyCookies(browser, true);
            manager.removeAllCookies(null);
            browser.clearCache(true);
        } else {
            manager.removeAllCookie();
        }

        browser.getSettings().setUserAgentString(KeyboardWebContract.USER_AGENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        browser.addJavascriptInterface(activity, "HTMLOUT");
        return browser;
    }
}
