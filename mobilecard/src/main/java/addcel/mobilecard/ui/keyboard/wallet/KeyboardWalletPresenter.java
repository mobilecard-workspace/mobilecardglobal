package addcel.mobilecard.ui.keyboard.wallet;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.squareup.otto.Bus;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.ui.keyboard.KeyboardBackEvent;
import addcel.mobilecard.ui.keyboard.KeyboardScreen;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 14/08/18.
 */
public class KeyboardWalletPresenter implements KeyboardWalletContract.Presenter {
    private final WalletAPI service;
    private final Usuario usuario;
    private final Bus bus;
    private final KeyboardWalletContract.View view;

    KeyboardWalletPresenter(WalletAPI service, Usuario usuario, Bus bus,
                            KeyboardWalletContract.View view) {
        this.service = service;
        this.usuario = usuario;
        this.bus = bus;
        this.view = view;
    }

    @SuppressLint("CheckResult")
    public void getCards() {
        service.getTarjetas(BuildConfig.ADDCEL_APP_ID, usuario.getIdeUsuario(),
                StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cardResponse -> {
                    if (cardResponse.getIdError() == 0) view.setCards(cardResponse.getTarjetas());
                });
    }

    @Override
    public void postWalletEvent(@NonNull CardEntity model, @NonNull AccountEntity account,
                                double monto) {
        bus.post(new KeyboardWalletEvent(account, monto, model));
    }

    @Override
    public void clickBack() {
        bus.post(new KeyboardBackEvent(KeyboardScreen.WALLET));
    }
}
