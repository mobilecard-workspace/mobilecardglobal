package addcel.mobilecard.ui.keyboard.monto;

import addcel.mobilecard.di.scope.PerView;
import dagger.Subcomponent;

/**
 * ADDCEL on 03/09/18.
 */
@PerView
@Subcomponent(modules = KeyboardMontoModule.class)
public interface KeyboardMontoSubcomponent {
    void inject(KeyboardMontoView view);
}
