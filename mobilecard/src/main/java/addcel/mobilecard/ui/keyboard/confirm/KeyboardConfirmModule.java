package addcel.mobilecard.ui.keyboard.confirm;

import com.squareup.otto.Bus;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.hth.H2HApi;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.di.scope.PerView;
import addcel.mobilecard.domain.h2h.KeyboardH2HPaymentInteractor;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

/**
 * ADDCEL on 15/08/18.
 */
@Module
public final class KeyboardConfirmModule {
    private final KeyboardConfirmView view;

    KeyboardConfirmModule(KeyboardConfirmView view) {
        this.view = view;
    }

    @PerView
    @Provides
    H2HApi provideService(Retrofit retrofit) {
        return H2HApi.Companion.get(retrofit);
    }

    @PerView
    @Provides
    TokenizerAPI provideTokenizer(Retrofit retrofit) {
        return TokenizerAPI.Companion.provideTokenizerAPI(retrofit);
    }

    @PerView
    @Provides
    KeyboardH2HPaymentInteractor provideInteractor(H2HApi api,
                                                   TokenizerAPI tokenizerAPI, SessionOperations session, Bus bus) {
        return new KeyboardH2HPaymentInteractor(api, tokenizerAPI, session, new CompositeDisposable(),
                bus);
    }

    @PerView
    @Provides
    KeyboardConfirmContract.Presenter providePresenter(
            KeyboardH2HPaymentInteractor interactor, Bus bus) {
        return new KeyboardConfirmPresenter(interactor, bus, view);
    }
}
