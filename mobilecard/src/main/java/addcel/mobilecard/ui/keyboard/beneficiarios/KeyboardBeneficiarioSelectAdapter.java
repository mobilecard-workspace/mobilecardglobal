package addcel.mobilecard.ui.keyboard.beneficiarios;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.utils.ListUtil;

/**
 * ADDCEL on 14/08/18.
 */
public final class KeyboardBeneficiarioSelectAdapter
        extends RecyclerView.Adapter<KeyboardBeneficiarioSelectAdapter.ViewHolder> {

    private final List<AccountEntity> beneficiarios = Lists.newArrayList();

    KeyboardBeneficiarioSelectAdapter() {
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.keyboard_beneficiarios_select_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.name.setText(beneficiarios.get(i).getAlias());
    }

    @Override
    public int getItemCount() {
        return beneficiarios.size();
    }

    public void update(List<AccountEntity> beneficiarios) {
        if (ListUtil.notEmpty(this.beneficiarios)) this.beneficiarios.clear();
        this.beneficiarios.addAll(beneficiarios);
        notifyDataSetChanged();
    }

    public AccountEntity getItem(int pos) {
        return beneficiarios.get(pos);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.text_beneficiarios_select_name);
        }
    }
}
