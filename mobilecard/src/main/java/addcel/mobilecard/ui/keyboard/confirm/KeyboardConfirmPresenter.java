package addcel.mobilecard.ui.keyboard.confirm;

import android.os.Build;

import com.squareup.otto.Bus;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.hth.HTHService;
import addcel.mobilecard.data.net.hth.PaymentEntity;
import addcel.mobilecard.data.net.hth.ReceiptEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.h2h.KeyboardH2HPaymentInteractor;
import addcel.mobilecard.ui.keyboard.KeyboardBackEvent;
import addcel.mobilecard.ui.keyboard.KeyboardScreen;
import addcel.mobilecard.ui.keyboard.error.KeyboardErrorSuccessEvent;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

import static addcel.mobilecard.BuildConfig.ADDCEL_APP_ID;

/**
 * ADDCEL on 15/08/18.
 */
class KeyboardConfirmPresenter implements KeyboardConfirmContract.Presenter {

    private final Bus bus;
    private final KeyboardConfirmContract.View view;
    private KeyboardH2HPaymentInteractor interactor;

    KeyboardConfirmPresenter(KeyboardH2HPaymentInteractor interactor, Bus bus,
                             KeyboardConfirmContract.View view) {
        this.interactor = interactor;
        this.bus = bus;
        this.view = view;
    }

    @Override
    public void register() {
        bus.register(this);
    }

    @Override
    public void unregister() {
        bus.unregister(this);
    }

    @Override
    public double getComision(double monto) {
        return view.getBeneficiario().getComision_fija() + ((monto + view.getBeneficiario()
                .getComision_fija()) * view.getBeneficiario().getComision_porcentaje());
    }

    @Override
    public void getToken(String profile) {
        view.showProgress();
        PaymentEntity request =
                new PaymentEntity(interactor.getIdUsuario(), view.getCard().getIdTarjeta(),
                        view.getBeneficiario().getId(), view.getMonto(), view.getBeneficiario().getAlias(),
                        getComision(view.getMonto()), 0, 0, "", String.valueOf(Build.VERSION.SDK_INT),
                        Build.MODEL, interactor.getLocation().getLat(), interactor.getLocation().getLon(), "",
                        "");

        interactor.getToken(ADDCEL_APP_ID, interactor.getIdPais(), StringUtil.getCurrentLanguage(),
                AddcelCrypto.encryptSensitive(JsonUtil.toJson(request)), profile,
                new InteractorCallback<TokenEntity>() {
                    @Override
                    public void onSuccess(@NotNull TokenEntity result) {
                        view.hideProgress();
                        sendMoney(result, request);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void sendMoney(TokenEntity tokenEntity, PaymentEntity paymentEntity) {
        view.showProgress();
        interactor.pagoBP(ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), interactor.getIdPais(),
                tokenEntity.getAccountId(), tokenEntity.getToken(), paymentEntity,
                new InteractorCallback<ReceiptEntity>() {
                    @Override
                    public void onSuccess(@NotNull ReceiptEntity result) {
                        view.hideProgress();
                        bus.post(new KeyboardConfirmEvent(result));
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        bus.post(new KeyboardErrorSuccessEvent());
                    }
                });
    }

    @Override
    public String getStartUrl() {
        return BuildConfig.BASE_URL + HTHService.ENQUEUE_PAYMENT.replace("{idApp}",
                String.valueOf(ADDCEL_APP_ID));
    }

    @Override
    public String getFormUrl() {
        return StringUtil.sha256("WHATEVERTHEHELL");
    }

    @Override
    public String getSuccessUrl() {
        return HTHService.PROCESS_FINISHED.replace("{idApp}", String.valueOf(ADDCEL_APP_ID));
    }

    @Override
    public String getErrorUrl() {
        return StringUtil.sha256("WHATEVERTHEHELL");
    }

    @Override
    public void onBackClick() {
        bus.post(new KeyboardBackEvent(KeyboardScreen.CONFIRM));
    }
}
