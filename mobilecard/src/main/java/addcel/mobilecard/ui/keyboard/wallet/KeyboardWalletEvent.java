package addcel.mobilecard.ui.keyboard.wallet;

import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 14/08/18.
 */
public class KeyboardWalletEvent {
    private final AccountEntity account;
    private final double monto;
    private final CardEntity card;

    public KeyboardWalletEvent(AccountEntity account, double monto, CardEntity card) {
        this.account = account;
        this.monto = monto;
        this.card = card;
    }

    public AccountEntity getAccount() {
        return account;
    }

    public double getMonto() {
        return monto;
    }

    public CardEntity getCard() {
        return card;
    }
}
