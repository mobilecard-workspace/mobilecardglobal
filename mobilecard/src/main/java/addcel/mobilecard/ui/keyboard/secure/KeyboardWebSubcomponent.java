package addcel.mobilecard.ui.keyboard.secure;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 30/08/18.
 */
@PerActivity
@Subcomponent(modules = KeyboardWebModule.class)
public interface KeyboardWebSubcomponent {
    void inject(KeyboardWebActivity activity);
}
