package addcel.mobilecard.ui.keyboard.wallet;

import java.util.List;

import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 14/08/18.
 */
public interface KeyboardWalletContract {
    interface View {
        void setBeneficiario(AccountEntity setAccount);

        void setMonto(double monto);

        void setCards(List<CardEntity> cards);

        void clickCard(CardEntity card);
    }

    interface Presenter {
        void getCards();

        void postWalletEvent(CardEntity model, AccountEntity account, double monto);

        void clickBack();
    }
}
