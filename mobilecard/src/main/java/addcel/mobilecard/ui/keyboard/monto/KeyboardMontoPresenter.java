package addcel.mobilecard.ui.keyboard.monto;

import com.google.common.base.Strings;

import java.text.NumberFormat;

import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 03/09/18.
 */
public class KeyboardMontoPresenter implements KeyboardMontoContract.Presenter {
    private final NumberFormat format;

    KeyboardMontoPresenter(NumberFormat format) {
        this.format = format;
    }

    @Override
    public String getFormattedMonto(String monto) {
        if (!Strings.isNullOrEmpty(monto) && StringUtil.isDecimalAmount(monto)) {
            return format.format(processMonto(monto));
        } else {
            return "";
        }
    }

    @Override
    public String eraseFormattedMonto(String monto, boolean removeLast) {
        if (!Strings.isNullOrEmpty(monto) && StringUtil.isDecimalAmount(monto)) {
            if (removeLast) monto = StringUtil.removeLast(monto);
            return format.format(Double.valueOf(monto) * 100);
        } else {
            return "";
        }
    }

    private double processMonto(String monto) {
        return Double.valueOf(monto) / 100;
    }
}
