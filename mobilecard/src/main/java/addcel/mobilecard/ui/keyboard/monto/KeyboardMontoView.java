package addcel.mobilecard.ui.keyboard.monto;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.common.base.Strings;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.ui.keyboard.KeyboardBackEvent;
import addcel.mobilecard.ui.keyboard.KeyboardScreen;
import addcel.mobilecard.utils.StringUtil;
import timber.log.Timber;

/**
 * ADDCEL on 14/08/18.
 */
public final class KeyboardMontoView extends ConstraintLayout
        implements KeyboardMontoContract.View {
    private static String montoString = "";
    @Inject
    KeyboardMontoContract.Presenter presenter;
    private TextView inputMonto;
    private AccountEntity account;
    private Bus bus;

    public KeyboardMontoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Mobilecard.get()
                    .getNetComponent()
                    .keyboardMontoSubcomponent(new KeyboardMontoModule(this))
                    .inject(this);
        }
    }

    public void setAccount(AccountEntity account) {
        this.account = account;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        inputMonto = findViewById(R.id.text_keyboard_monto);
        inputMonto.setFocusable(false);
        inputMonto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    findViewById(R.id.b_keyboard_total_clear).setEnabled(Boolean.TRUE);
                    findViewById(R.id.b_keyboard_total_continue).setEnabled(Boolean.TRUE);
                } else {
                    findViewById(R.id.b_keyboard_total_clear).setEnabled(Boolean.FALSE);
                    findViewById(R.id.b_keyboard_total_continue).setEnabled(Boolean.FALSE);
                }
            }
        });
        findViewById(R.id.b_keyboard_total_clear).setEnabled(Boolean.FALSE);
        findViewById(R.id.b_keyboard_total_continue).setEnabled(Boolean.FALSE);

        findViewById(R.id.b_keyboard_total_one).setOnClickListener(view -> setMontoValueInText("1"));
        findViewById(R.id.b_keyboard_total_two).setOnClickListener(view -> setMontoValueInText("2"));
        findViewById(R.id.b_keyboard_total_three).setOnClickListener(view -> setMontoValueInText("3"));
        findViewById(R.id.b_keyboard_total_four).setOnClickListener(view -> setMontoValueInText("4"));
        findViewById(R.id.b_keyboard_total_five).setOnClickListener(view -> setMontoValueInText("5"));
        findViewById(R.id.b_keyboard_total_six).setOnClickListener(view -> setMontoValueInText("6"));
        findViewById(R.id.b_keyboard_total_seven).setOnClickListener(view -> setMontoValueInText("7"));
        findViewById(R.id.b_keyboard_total_eight).setOnClickListener(view -> setMontoValueInText("8"));
        findViewById(R.id.b_keyboard_total_nine).setOnClickListener(view -> setMontoValueInText("9"));
        findViewById(R.id.b_keyboard_total_clear).setOnClickListener(view -> deleteMontoValueInText());
        findViewById(R.id.b_keyboard_total_zero).setOnClickListener(view -> setMontoValueInText("0"));
        findViewById(R.id.b_keyboard_total_continue).setOnClickListener(view -> postMontoEvent());
        findViewById(R.id.b_keyboard_monto_back).setOnClickListener(
                view -> bus.post(new KeyboardBackEvent(KeyboardScreen.MONTO)));
    }

    private void postMontoEvent() {
        Timber.d("Is account null: %b", account == null);
        bus.post(new KeyboardMontoEvent(account, Double.parseDouble(montoString) / 100));
    }

    private void setMontoValueInText(String capture) {
        if (!Strings.isNullOrEmpty(capture) && StringUtil.isDecimalAmount(capture)) {
            montoString += capture;
            inputMonto.setText(presenter.getFormattedMonto(montoString));
        } else {
            montoString = "";
            inputMonto.setText("");
        }
    }

    private void deleteMontoValueInText() {
        montoString = StringUtil.removeLast(montoString);
        inputMonto.setText(presenter.getFormattedMonto(montoString));
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    public void showError(String msg) {
    }
}
