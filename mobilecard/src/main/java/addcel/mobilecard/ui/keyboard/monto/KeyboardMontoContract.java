package addcel.mobilecard.ui.keyboard.monto;

/**
 * ADDCEL on 03/09/18.
 */
public interface KeyboardMontoContract {
    interface View {
        void showProgress();

        void hideProgress();

        void showError(String msg);
    }

    interface Presenter {
        String getFormattedMonto(String monto);

        String eraseFormattedMonto(String monto, boolean removeLast);
    }
}
