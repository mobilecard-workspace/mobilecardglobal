package addcel.mobilecard.ui.keyboard.wallet;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import timber.log.Timber;

/**
 * ADDCEL on 14/08/18.
 */
public class KeyboardWalletView extends ConstraintLayout implements KeyboardWalletContract.View {
    @Inject
    KeyboardWalletContract.Presenter presenter;
    @Inject
    KeyboardWalletAdapter adapter;
    @Inject
    NumberFormat currFormat;
    private AccountEntity account;
    private double monto;
    private TextView montoView;

    public KeyboardWalletView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Mobilecard.get()
                    .getNetComponent()
                    .keyboardWalletSubcomponent(new KeyboardWalletModule(this))
                    .inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        montoView = findViewById(R.id.text_keyboard_wallet_monto);
        RecyclerView cardsView = findViewById(R.id.recycler_keyboard_wallet);
        cardsView.setAdapter(adapter);
        cardsView.setLayoutManager(new LinearLayoutManager(getContext()));
        cardsView.addItemDecoration(
                new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        ItemClickSupport.addTo(cardsView)
                .setOnItemClickListener(
                        (recyclerView, position, v) -> clickCard(adapter.getItem(position)));
        findViewById(R.id.b_keyboard_wallet_back).setOnClickListener(view -> presenter.clickBack());
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        montoView.setText(currFormat.format(monto));
        presenter.getCards();
    }

    @Override
    public void setBeneficiario(AccountEntity setAccount) {
        this.account = setAccount;
    }

    @Override
    public void setMonto(double monto) {
        this.monto = monto;
    }

    @Override
    public void setCards(List<CardEntity> cards) {
        adapter.update(cards);
    }

    @Override
    public void clickCard(CardEntity card) {
        Timber.d("Is account null: %b", account == null);
        presenter.postWalletEvent(card, account, monto);
    }
}
