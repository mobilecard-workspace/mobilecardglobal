package addcel.mobilecard.ui.keyboard.secure;

import android.content.ClipData;
import android.content.ClipboardManager;

import androidx.annotation.NonNull;

import java.util.List;
/*
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;*/

/**
 * ADDCEL on 30/08/18.
 */
class KeyboardWebPresenter implements KeyboardWebContract.Presenter {
    private final KeyboardWebContract.View view;

    public KeyboardWebPresenter(KeyboardWebContract.View view) {
        this.view = view;
    }

    @Override
    public String onHtmlReceived(String html) {
    /*Document document = Jsoup.parse(html);
    Element envelope = document.getElementById("envelope");
    Elements tbody = envelope.getElementsByTag("tbody");
    return parseResultText(tbody.eachText());
    */
        return "";
    }

    @Override
    public void onFinishedCheckLaunch(String url, String finishEndpoint) {
        view.shouldShowFloatingActionButton(url.contains(finishEndpoint));
    }

    @Override
    public void onTextSentToClipboard(ClipboardManager manager, @NonNull String label,
                                      @NonNull String text) {
        ClipData clip = ClipData.newPlainText(label, text);
        manager.setPrimaryClip(clip);
    }

    private String parseResultText(List<String> resultText) {
        StringBuilder resBuilder = new StringBuilder();
        for (String text : resultText) {
            resBuilder.append(text).append("\n");
        }
        return resBuilder.toString();
    }
}
