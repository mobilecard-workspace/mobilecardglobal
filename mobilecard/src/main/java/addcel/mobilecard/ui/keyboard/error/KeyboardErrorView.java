package addcel.mobilecard.ui.keyboard.error;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.squareup.otto.Bus;

import addcel.mobilecard.R;
import addcel.mobilecard.ui.keyboard.KeyboardService;
import timber.log.Timber;

/**
 * ADDCEL on 16/08/18.
 */
public final class KeyboardErrorView extends ConstraintLayout {

    public static final int ERROR_LOGIN = 1;
    public static final int ERROR_PAIS = 2;
    public static final int SUCCESS = 3;
    private int errorType;
    private TextView msgView;
    private Button errorButton;
    private Bus bus;

    public KeyboardErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        msgView = findViewById(R.id.view_keyboard_login_msg);
        errorButton = findViewById(R.id.b_keyboard_login_msg);
        errorButton.setOnClickListener(view -> {
            Timber.d("Tipo error: %d", errorType);
            switch (errorType) {
                case ERROR_LOGIN:
                    bus.post(new KeyboardErrorLoginEvent());
                    break;
                case ERROR_PAIS:
                    bus.post(new KeyboardErrorPaisEvent());
                    break;
                default:
                    bus.post(new KeyboardErrorSuccessEvent());
                    break;
            }
        });
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (errorType == ERROR_PAIS) {
            msgView.setText(R.string.txt_keyboard_error_pais);
            errorButton.setText(R.string.update);
        } else if (errorType == SUCCESS) {
            msgView.setText(KeyboardService.RESULT);
            errorButton.setText(android.R.string.ok);
            KeyboardService.clearTransactionResults();
        }
    }
}
