package addcel.mobilecard.ui.keyboard.monto;

import java.text.NumberFormat;

import addcel.mobilecard.di.scope.PerView;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 03/09/18.
 */
@Module
public final class KeyboardMontoModule {
    private final KeyboardMontoContract.View view;

    KeyboardMontoModule(KeyboardMontoContract.View view) {
        this.view = view;
    }

    @PerView
    @Provides
    KeyboardMontoContract.Presenter providePresenter(NumberFormat format) {
        return new KeyboardMontoPresenter(format);
    }
}
