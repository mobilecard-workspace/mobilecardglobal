package addcel.mobilecard.ui.keyboard.confirm;

import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.PaymentEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 15/08/18.
 */
public interface KeyboardConfirmContract {

    interface View {
        AccountEntity getBeneficiario();

        double getMonto();

        CardEntity getCard();

        void clickBack();

        void clickContinuar();

        void showProgress();

        void hideProgress();
    }

    interface Presenter {

        void register();

        void unregister();

        double getComision(double monto);


        void getToken(String profile);

        void sendMoney(TokenEntity token, PaymentEntity paymentEntity);

        String getStartUrl();

        String getFormUrl();

        String getSuccessUrl();

        String getErrorUrl();

        void onBackClick();
    }
}
