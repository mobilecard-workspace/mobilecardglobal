package addcel.mobilecard.ui.keyboard.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.squareup.otto.Bus;

import addcel.mobilecard.R;

/**
 * ADDCEL on 12/09/18.
 */
public class KeyboardMenuView extends ConstraintLayout {

    private Bus bus;

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public KeyboardMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Button transferButton = findViewById(R.id.b_keyboard_menu_transfer);
        transferButton.setOnClickListener(
                view -> bus.post(new KeyboardMenuLaunchEvent(KeyboardMenuLaunchEvent.TRANSFER)));
    }
}
