package addcel.mobilecard.ui.keyboard.menu;

/**
 * ADDCEL on 12/09/18.
 */
public class KeyboardMenuLaunchEvent {
    private final int caso;
    public static final int TRANSFER = 1;

    public KeyboardMenuLaunchEvent(int caso) {
        this.caso = caso;
    }

    public int getCaso() {
        return caso;
    }
}
