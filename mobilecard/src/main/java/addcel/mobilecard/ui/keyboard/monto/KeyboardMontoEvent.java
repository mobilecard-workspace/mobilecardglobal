package addcel.mobilecard.ui.keyboard.monto;

import addcel.mobilecard.data.net.hth.AccountEntity;

/**
 * ADDCEL on 14/08/18.
 */
public final class KeyboardMontoEvent {
    private final AccountEntity account;
    private final double amount;

    KeyboardMontoEvent(AccountEntity account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    public AccountEntity getAccount() {
        return account;
    }

    public double getAmount() {
        return amount;
    }
}
