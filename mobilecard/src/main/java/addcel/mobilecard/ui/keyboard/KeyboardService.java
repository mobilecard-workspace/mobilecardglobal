package addcel.mobilecard.ui.keyboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.ReceiptEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.keyboard.beneficiarios.KeyboardBeneficiariosSelectEvent;
import addcel.mobilecard.ui.keyboard.confirm.KeyboardConfirmEvent;
import addcel.mobilecard.ui.keyboard.confirm.KeyboardConfirmView;
import addcel.mobilecard.ui.keyboard.error.KeyboardErrorLoginEvent;
import addcel.mobilecard.ui.keyboard.error.KeyboardErrorPaisEvent;
import addcel.mobilecard.ui.keyboard.error.KeyboardErrorSuccessEvent;
import addcel.mobilecard.ui.keyboard.error.KeyboardErrorView;
import addcel.mobilecard.ui.keyboard.menu.KeyboardMenuLaunchEvent;
import addcel.mobilecard.ui.keyboard.menu.KeyboardMenuView;
import addcel.mobilecard.ui.keyboard.monto.KeyboardMontoEvent;
import addcel.mobilecard.ui.keyboard.monto.KeyboardMontoView;
import addcel.mobilecard.ui.keyboard.success.KeyboardSuccessView;
import addcel.mobilecard.ui.keyboard.wallet.KeyboardWalletEvent;
import addcel.mobilecard.ui.keyboard.wallet.KeyboardWalletView;
import addcel.mobilecard.ui.usuario.perfil.Perfil2Activity;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;

/**
 * ADDCEL on 11/08/18.
 */
public final class KeyboardService extends InputMethodService implements KeyboardContract.View {
    public static boolean COMPLETED = false;
    public static String RESULT;
    @Inject
    SessionOperations session;
    @Inject
    Bus bus;
    /*private CardListView cardListView;
    private EnterAmountView enterAmountView;
    private KeyboardCheckerRequirements keyboardCheckerRequirements;*/
    private View loadingView;
    private View menuView;
    private View beneficiariosView;
    private View montosView;
    private View walletView;
    private View confirmView;
    private View successView;
    // private NumberPaymentView numberPaymentView;
    private ViewGroup viewKeyboard;

    public static void setTransactionResults(String data) {
        RESULT = data;
        COMPLETED = Boolean.TRUE;
    }

    public static void clearTransactionResults() {
        RESULT = "";
        COMPLETED = Boolean.FALSE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Mobilecard.get().getNetComponent().inject(this);
        bus.register(this);
    }

    @Override
    public View onCreateInputView() {
        @SuppressLint("InflateParams") View inflate =
                getLayoutInflater().inflate(R.layout.keyboard_container, null);
        if (inflate == null) {
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.viewKeyboard = (ViewGroup) inflate;
        LayoutInflater layoutInflater = getLayoutInflater();
        ViewGroup viewGroup = this.viewKeyboard;
        if (viewGroup == null) {
            throw new NullPointerException("viewKeyboard");
        }
        // inflate = layoutInflater.inflate(R.layout.keyboard_progress_view, viewGroup, false);
        // this.loadingView = inflate;

        inflate = layoutInflater.inflate(R.layout.keyboard_menu_screen, viewGroup, false);
        this.menuView = inflate;
        ((KeyboardMenuView) menuView).setBus(bus);

        Timber.d("OnCreateView");
        ViewGroup viewGroup2 = this.viewKeyboard;
        if (viewGroup2 == null) {
            throw new NullPointerException("viewKeyboard");
        }
        return viewGroup2;
    }

    @Override
    public void onStartInputView(EditorInfo info, boolean restarting) {
        super.onStartInputView(info, restarting);
        // showLoading();
        if (session.isUsuarioLogged()) {
            if (session.getUsuario().getIdPais() != 1) {
                showError("LoginRequerido", KeyboardErrorView.ERROR_PAIS);
            } else {
                if (COMPLETED) {
                    showError(RESULT, KeyboardErrorView.SUCCESS);
                } else {
                    showMenu();
                }
            }
        } else {
            showError("LoginRequerido", KeyboardErrorView.ERROR_LOGIN);
        }
    /*
    info = this.keyboardCheckerRequirements;
    if (info == null) {
     throw new NullPointerException("keyboardCheckerRequirements");
    }
    if (info.needToLogin() != null) {
      editorInfo = this.keyboardCheckerRequirements;
      if (editorInfo == null) {
        throw new NullPointerException("keyboardCheckerRequirements");
      }
      showError(editorInfo.getErrorNotLogged());
    } else {
      editorInfo = this.keyboardCheckerRequirements;
      if (info == null) {
        throw new NullPointerException(("keyboardCheckerRequirements");
      }
      info.init();
    }
    */
        String stringBuilder = "onStartInputView restarting = " + restarting;
        Timber.d("bg_card_mobilecard Keyboard %s", stringBuilder);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    public void showLoading() {
        View view = this.loadingView;
        if (view == null) {
            throw new NullPointerException("loadingView");
        }
        replaceView(view, KeyboardScreen.ERROR);
    }

    @Override
    public void showMenu() {
        View view = this.menuView;
        if (view == null) {
            throw new NullPointerException("menuView");
        }
        replaceView(view, KeyboardScreen.MENU);
    }

    @Subscribe
    @Override
    public void postMenu(KeyboardMenuLaunchEvent event) {
        switch (event.getCaso()) {
            case KeyboardMenuLaunchEvent.TRANSFER:
                showBeneficiarios();
                break;
            default:
                break;
        }
    }

    @Override
    public void showBeneficiarios() {
        if (this.beneficiariosView == null) {
            this.beneficiariosView =
                    getLayoutInflater().inflate(R.layout.keyboard_beneficiarios_select_screen, viewKeyboard,
                            false);
        }
        View view = beneficiariosView;
        if (view == null) {
            throw new NullPointerException("beneficiariosView");
        }
        replaceView(view, KeyboardScreen.BENEFICIARIO);
    }

    @Override
    public void showError(String msg, int errorType) {
        LayoutInflater layoutInflater = getLayoutInflater();
        ViewGroup viewGroup = this.viewKeyboard;
        if (viewGroup == null) {
            throw new NullPointerException("viewKeyboard");
        }
        View inflate = layoutInflater.inflate(R.layout.keyboard_error_screen, viewGroup, false);
        ((KeyboardErrorView) inflate).setErrorType(errorType);
        ((KeyboardErrorView) inflate).setBus(bus);
        replaceView(inflate, KeyboardScreen.ERROR);
    }

    @Subscribe
    @Override
    public void postLoginError(KeyboardErrorLoginEvent event) {
        openMC();
    }

    @Subscribe
    @Override
    public void postPaisError(KeyboardErrorPaisEvent event) {
        startActivity(Perfil2Activity.get(this).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void replaceView(View view, KeyboardScreen screen) {
        ViewGroup viewGroup = this.viewKeyboard;
        if (viewGroup == null) {
            throw new NullPointerException("viewKeyboard");
        }
        viewGroup = viewGroup.findViewById(R.id.keyboard_container);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
    }

    @Subscribe
    @Override
    public void postBack(KeyboardBackEvent event) {
        onBackClicked(event.getScreen());
    }

    @Override
    public void onBackClicked(KeyboardScreen screen) {
        switch (screen) {
            case CONFIRM:
                replaceView(walletView, KeyboardScreen.WALLET);
                break;
            case WALLET:
                replaceView(montosView, KeyboardScreen.MONTO);
                break;
            case MONTO:
                replaceView(beneficiariosView, KeyboardScreen.BENEFICIARIO);
                break;
            case BENEFICIARIO:
                replaceView(menuView, KeyboardScreen.MENU);
                break;
            case SUCCESS:
                replaceView(menuView, KeyboardScreen.MENU);
                break;
            default:
                replaceView(loadingView, KeyboardScreen.ERROR);
                break;
        }
    }

    @Subscribe
    @Override
    public void postBeneficiario(KeyboardBeneficiariosSelectEvent event) {
        showMonto(event.getAccount());
    }

    @Override
    public void showMonto(AccountEntity account) {
        if (this.montosView == null) {
            this.montosView =
                    getLayoutInflater().inflate(R.layout.keyboard_monto_screen, viewKeyboard, false);
        }
        View view = montosView;
        if (view == null) {
            throw new NullPointerException("montosView");
        }
        ((KeyboardMontoView) view).setAccount(account);
        ((KeyboardMontoView) view).setBus(bus);
        replaceView(view, KeyboardScreen.MONTO);
    }

    @Subscribe
    @Override
    public void postMonto(KeyboardMontoEvent event) {
        showWallet(event.getAccount(), event.getAmount());
    }

    @Override
    public void showWallet(AccountEntity account, double monto) {
        if (this.walletView == null) {
            this.walletView =
                    getLayoutInflater().inflate(R.layout.keyboard_wallet_screen, viewKeyboard, false);
        }
        View view = walletView;
        if (view == null) {
            throw new NullPointerException("walletView");
        }
        ((KeyboardWalletView) view).setBeneficiario(account);
        ((KeyboardWalletView) view).setMonto(monto);
        replaceView(view, KeyboardScreen.WALLET);
    }

    @Subscribe
    @Override
    public void postWallet(KeyboardWalletEvent event) {
        showConfirm(event.getAccount(), event.getMonto(), event.getCard());
    }

    @Override
    public void showConfirm(AccountEntity account, double monto, CardEntity card) {
        if (this.confirmView == null) {
            this.confirmView =
                    getLayoutInflater().inflate(R.layout.keyboard_confirm_screen, viewKeyboard, false);
        }
        View view = confirmView;
        if (view == null) {
            throw new NullPointerException("confirmView");
        }
        ((KeyboardConfirmView) view).setBeneficiario(account);
        ((KeyboardConfirmView) view).setMonto(monto);
        ((KeyboardConfirmView) view).setCard(card);
        replaceView(view, KeyboardScreen.CONFIRM);
    }

    @Subscribe
    @Override
    public void postConfirm(KeyboardConfirmEvent event) {
        showSuccess(event.getReceipt());
    }

    @Override
    public void showSuccess(ReceiptEntity receipt) {
        if (this.successView == null) {
            this.successView =
                    getLayoutInflater().inflate(R.layout.keyboard_success_screen, viewKeyboard, false);
        }
        View view = successView;
        if (view == null) {
            throw new NullPointerException("successView");
        }
        ((KeyboardSuccessView) view).setReceipt(receipt);
        ((KeyboardSuccessView) view).setBus(bus);
        replaceView(view, KeyboardScreen.SUCCESS);
    }

    @Subscribe
    @Override
    public void postTransactionFinished(KeyboardErrorSuccessEvent event) {
        clearKeyboard();
    }

    private void openMC() {
        Context applicationContext = getApplicationContext();
        Intent launchIntentForPackage =
                getPackageManager().getLaunchIntentForPackage(applicationContext.getPackageName());
        if (launchIntentForPackage != null) {
            startActivity(launchIntentForPackage);
        }
        if (launchIntentForPackage == null) {
            Toasty.error(this, applicationContext.getString(R.string.error_default)).show();
        }
    }

    private void clearKeyboard() {
        if (session.isUsuarioLogged()) {
            if (session.getUsuario().getIdPais() != 1) {
                showError("LoginRequerido", KeyboardErrorView.ERROR_PAIS);
            } else {
                showMenu();
            }
        } else {
            showError("LoginRequerido", KeyboardErrorView.ERROR_LOGIN);
        }
    }
}
