package addcel.mobilecard.ui.keyboard.wallet;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.utils.ListUtil;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 14/08/18.
 */
public class KeyboardWalletAdapter extends RecyclerView.Adapter<KeyboardWalletAdapter.ViewHolder> {

    private final List<CardEntity> cards = Lists.newArrayList();

    public KeyboardWalletAdapter() {
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.keyboard_wallet_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        CardEntity model = cards.get(i);
        viewHolder.pan.setText(StringUtil.maskCard(AddcelCrypto.decryptHard(model.getPan()), "•"));
        viewHolder.setFranquicia(model);
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public void update(List<CardEntity> cards) {
        if (ListUtil.notEmpty(this.cards)) this.cards.clear();
        this.cards.addAll(cards);
        notifyDataSetChanged();
    }

    public CardEntity getItem(int pos) {
        return cards.get(pos);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView pan;
        private final double dimen;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            pan = itemView.findViewById(R.id.text_keyboard_wallet_pan);
            dimen = itemView.getResources().getDimension(R.dimen.widget_vertical_margin);
        }

        private void setFranquicia(CardEntity model) {
            switch (model.getTipo()) {
                case VISA:
                    pan.setCompoundDrawablesWithIntrinsicBounds(R.drawable.card_visa, 0, 0, 0);
                    break;
                case MasterCard:
                    pan.setCompoundDrawablesWithIntrinsicBounds(R.drawable.card_master, 0, 0, 0);
                    break;
                case AmEx:
                case Amex:
                    pan.setCompoundDrawablesWithIntrinsicBounds(R.drawable.card_american, 0, 0, 0);
                    break;
                default:
                    pan.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mc_alpha, 0, 0, 0);
                    break;
            }
            pan.setCompoundDrawablePadding((int) dimen);
        }
    }
}
