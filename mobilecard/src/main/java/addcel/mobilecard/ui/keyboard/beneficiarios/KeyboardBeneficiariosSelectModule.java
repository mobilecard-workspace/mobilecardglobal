package addcel.mobilecard.ui.keyboard.beneficiarios;

import com.google.common.base.Optional;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.hth.H2HApi;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.di.scope.PerView;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 13/08/18.
 */
@Module
public class KeyboardBeneficiariosSelectModule {

    KeyboardBeneficiariosSelectModule() {
    }

    @PerView
    @Provides
    H2HApi provideService(Retrofit retrofit) {
        return H2HApi.Companion.get(retrofit);
    }

    @PerView
    @Provides
    Optional<Usuario> provideUsuario(SessionOperations session) {
        return Optional.fromNullable(session.getUsuario());
    }

    @PerView
    @Provides
    KeyboardBeneficiarioSelectAdapter provideAdapter() {
        return new KeyboardBeneficiarioSelectAdapter();
    }
}
