package addcel.mobilecard.ui.keyboard.wallet;

import com.google.common.base.Optional;
import com.squareup.otto.Bus;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerView;
import dagger.Module;
import dagger.Provides;

@Module
public final class KeyboardWalletModule {
    private final KeyboardWalletView view;

    KeyboardWalletModule(KeyboardWalletView view) {
        this.view = view;
    }

    @PerView
    @Provides
    KeyboardWalletAdapter provideAdapter() {
        return new KeyboardWalletAdapter();
    }

    @PerView
    @Provides
    Optional<Usuario> provideUsuario(SessionOperations session) {
        return Optional.fromNullable(session.getUsuario());
    }

    @PerView
    @Provides
    KeyboardWalletContract.Presenter providePresenter(WalletAPI service,
                                                      Optional<Usuario> usuario, Bus bus) {
        return new KeyboardWalletPresenter(service, usuario.or(new Usuario()), bus, view);
    }
}
