package addcel.mobilecard.ui.keyboard.success

import addcel.mobilecard.data.net.hth.ReceiptEntity
import addcel.mobilecard.ui.keyboard.KeyboardBackEvent
import addcel.mobilecard.ui.keyboard.KeyboardScreen
import addcel.mobilecard.ui.keyboard.KeyboardService
import addcel.mobilecard.utils.ErrorUtil
import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.otto.Bus
import kotlinx.android.synthetic.main.keyboard_success_screen.view.*
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 2019-06-04.
 */
interface View {

    fun setInfo(receipt: ReceiptEntity)

    fun onBackClicked()

    fun onAcceptClicked()
}

class KeyboardSuccessView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs),
        View {
    lateinit var receipt: ReceiptEntity
    lateinit var bus: Bus

    override fun onFinishInflate() {
        super.onFinishInflate()
        b_keyboard_success_back.setOnClickListener { onBackClicked() }
        b_keyboard_success_continuar.setOnClickListener { onAcceptClicked() }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        if (!isInEditMode) {
            setInfo(receipt)
        }
    }

    override fun setInfo(receipt: ReceiptEntity) {
        text_keyboard_success_msg.text = buildMsg(receipt)
        if (receipt.code == 0) {
            text_keyboard_success_title.text = receipt.message
        } else {
            text_keyboard_success_title.text = ErrorUtil.getErrorMsg(ErrorUtil.OPERATION)
        }
    }

    override fun onBackClicked() {
        KeyboardService.clearTransactionResults()
        bus.post(KeyboardBackEvent(KeyboardScreen.SUCCESS))
    }

    override fun onAcceptClicked() {
        KeyboardService.clearTransactionResults()
        bus.post(KeyboardBackEvent(KeyboardScreen.SUCCESS))
    }

    private fun buildMsg(receipt: ReceiptEntity): String {
        return if (receipt.code == 0) "Autorización: " + receipt.authNumber + "\n" + "Folio: " + receipt.idTransaccion + "\n" + "Monto: " + NumberFormat.getCurrencyInstance(
                Locale.getDefault()
        ).format(
                receipt.amount
        ) + "\n" + "Forma de pago: " + receipt.maskedPAN + "\n" + "Fecha y hora: " + formatDateForMsg(
                receipt.dateTime
        )
        else receipt.message
    }

    private fun formatDateForMsg(millis: Long): String {
        val dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault())
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
        return formatter.format(dateTime)
    }
}