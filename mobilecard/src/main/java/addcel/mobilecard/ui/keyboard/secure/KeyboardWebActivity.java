package addcel.mobilecard.ui.keyboard.secure;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.ui.keyboard.KeyboardService;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;

public class KeyboardWebActivity extends AppCompatActivity implements KeyboardWebContract.View {

    @Inject
    @Named("titulo")
    String titulo;

    @Inject
    @Named("startUrl")
    String startUrl;

    @Inject
    @Named("finishEndpoint")
    String finishEndpoint;

    @Inject
    byte[] data;
    @Inject
    AlertDialog warningDialog;

    @Inject
    @Named("clipboardDialog")
    AlertDialog clipboardDialog;

    @Inject
    WebView browser;
    @Inject
    KeyboardWebContract.Presenter presenter;

    private FrameLayout container;
    private String currentUrl = "";
    private ProgressBar progressBar;
    private FloatingActionButton clipBoardButton;

    public static synchronized Intent get(Context context, String titulo, String start, String finish,
                                          byte[] data) {
        Bundle bundle = new Bundle();
        bundle.putString("titulo", titulo);
        bundle.putString("startUrl", start);
        bundle.putString("finishEndpoint", finish);
        bundle.putByteArray("data", data);
        return new Intent(context, KeyboardWebActivity.class).putExtras(bundle)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard_web);
        container = findViewById(R.id.frame_keyboard_web);
        clipBoardButton = findViewById(R.id.b_keyboard_web_copy);
        clipBoardButton.setOnClickListener(v -> {
            if (!clipboardDialog.isShowing()) clipboardDialog.show();
        });
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        final KeyboardWebModule module = new KeyboardWebModule(this);
        Mobilecard.get().getNetComponent().keyboardWebSubcomponent(module).inject(this);
        container.addView(browser);
        postUrl(savedInstanceState, browser, startUrl, data);
    }

    @Override
    public void onBackPressed() {
        if (currentUrl.contains(finishEndpoint)) {
            finish();
            // android.os.Process.killProcess(android.os.Process.myPid());
        } else {
            showWarningExitDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (browser != null) {
            browser.onResume();
            browser.resumeTimers();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (browser != null) {
            browser.stopLoading();
            browser.onPause();
            browser.pauseTimers();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        container.removeAllViews();
        browser.destroy();
    }

    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        Timber.d("onSaveInstanceState");
        try {
            super.onSaveInstanceState(outState, outPersistentState);
            browser.saveState(outState);
        } catch (Throwable t) {
            Timber.e(t);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Timber.d("onRestoreInstanceState");
        try {
            browser.restoreState(savedInstanceState);
        } catch (Throwable t) {
            Timber.e(t);
        }
    }

    @Override
    public void showProgress() {
        if (getProgressBar().getVisibility() == View.GONE)
            getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (getProgressBar().getVisibility() == View.VISIBLE)
            getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showError(@NonNull String msg) {
        Toasty.error(this, msg).show();
        finish();
    }

    @Override
    public void setCurrentUrl(@NonNull String url) {
        currentUrl = url;
    }

    @JavascriptInterface
    @Override
    public void receiveHtml(String html) {
        if (BuildConfig.DEBUG) {
            Timber.d(html);
        }
        if (currentUrl.contains(finishEndpoint)) {
            String data = presenter.onHtmlReceived(html);
            KeyboardService.setTransactionResults(data);
        }
    }

    @Override
    public void showWarningExitDialog() {
        if (!warningDialog.isShowing()) warningDialog.show();
    }

    @Override
    public void launchFinishedCheck(String url) {
        presenter.onFinishedCheckLaunch(url, finishEndpoint);
    }

    @Override
    public void shouldShowFloatingActionButton(boolean show) {
        clipBoardButton.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void sendTextToClipboard() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        presenter.onTextSentToClipboard(clipboard, getString(R.string.txt_transaction_success),
                KeyboardService.RESULT);
    /*
     En caso de que se cambie el feature a compartir usar siguiente fragmento de código

     /*String shareBody = KeyboardService.RESULT;
     Intent sharingIntent = new Intent(Intent.ACTION_SEND);
     sharingIntent.setType("text/plain");
     sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.nav_transfer_mx));
     sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
     startActivity(
         Intent.createChooser(sharingIntent, getResources().getString(R.string.txt_share)));
    */
    }

    private ProgressBar getProgressBar() {
        if (progressBar == null) progressBar = findViewById(R.id.progress_keyboard_web);
        return progressBar;
    }

    private void postUrl(Bundle savedInstanceState, WebView view, String startUrl, byte[] bytes) {
        if (savedInstanceState == null) {
            new Handler().postDelayed(() -> view.postUrl(startUrl, bytes), 400);
        }
    }
}
