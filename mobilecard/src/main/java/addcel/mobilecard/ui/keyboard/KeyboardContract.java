package addcel.mobilecard.ui.keyboard;

import addcel.mobilecard.data.net.hth.AccountEntity;
import addcel.mobilecard.data.net.hth.ReceiptEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.keyboard.beneficiarios.KeyboardBeneficiariosSelectEvent;
import addcel.mobilecard.ui.keyboard.confirm.KeyboardConfirmEvent;
import addcel.mobilecard.ui.keyboard.error.KeyboardErrorLoginEvent;
import addcel.mobilecard.ui.keyboard.error.KeyboardErrorPaisEvent;
import addcel.mobilecard.ui.keyboard.error.KeyboardErrorSuccessEvent;
import addcel.mobilecard.ui.keyboard.menu.KeyboardMenuLaunchEvent;
import addcel.mobilecard.ui.keyboard.monto.KeyboardMontoEvent;
import addcel.mobilecard.ui.keyboard.wallet.KeyboardWalletEvent;

/**
 * ADDCEL on 11/08/18.
 */
interface KeyboardContract {
    interface View {
        void showLoading();

        void showMenu();

        void postMenu(KeyboardMenuLaunchEvent event);

        void showBeneficiarios();

        void showError(String msg, int errorType);

        void postLoginError(KeyboardErrorLoginEvent event);

        void postPaisError(KeyboardErrorPaisEvent event);

        void replaceView(android.view.View view, KeyboardScreen screen);

        void postBack(KeyboardBackEvent event);

        void onBackClicked(KeyboardScreen screen);

        void postBeneficiario(KeyboardBeneficiariosSelectEvent event);

        void showMonto(AccountEntity account);

        void postMonto(KeyboardMontoEvent event);

        void showWallet(AccountEntity account, double monto);

        void postWallet(KeyboardWalletEvent event);

        void showConfirm(AccountEntity account, double monto, CardEntity card);

        void postConfirm(KeyboardConfirmEvent event);

        void showSuccess(ReceiptEntity receipt);

        void postTransactionFinished(KeyboardErrorSuccessEvent event);
    }

    interface Presenter {
    }
}
