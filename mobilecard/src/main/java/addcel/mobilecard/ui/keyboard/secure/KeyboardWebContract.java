package addcel.mobilecard.ui.keyboard.secure;

import android.content.ClipboardManager;

import androidx.annotation.NonNull;

/**
 * ADDCEL on 30/08/18.
 */
public interface KeyboardWebContract {
    String USER_AGENT =
            "User-Agent:Mozilla/5.0 (Linux; Android 4.4.2; SM-T230 Build/KOT49H) AppleWebKit/537.36"
                    + "(KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Safari/537.36";

    String PROCESS_HTML_FUN = "javascript:window.HTMLOUT.receiveHtml("
            + "'<html>'"
            + "+document.getElementsByTagName('html')[0].innerHTML+"
            + "'</html>'"
            + ");";

    interface View {

        void showProgress();

        void hideProgress();

        void showError(@NonNull String msg);

        void setCurrentUrl(@NonNull String url);

        void receiveHtml(String html);

        void showWarningExitDialog();

        void launchFinishedCheck(String url);

        void shouldShowFloatingActionButton(boolean show);

        void sendTextToClipboard();
    }

    interface Presenter {
        String onHtmlReceived(String html);

        void onFinishedCheckLaunch(String url, String finishEndpoint);

        void onTextSentToClipboard(ClipboardManager manager, @NonNull String label,
                                   @NonNull String text);
    }
}
