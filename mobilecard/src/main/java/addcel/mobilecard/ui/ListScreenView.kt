package addcel.mobilecard.ui

/**
 * ADDCEL on 2020-02-11.
 */
interface ListScreenView : ScreenView {
    fun lockList()
    fun unLockList()
    fun clickElement(pos: Int)
}