package addcel.mobilecard.ui

/**
 * ADDCEL on 2019-12-27.
 */
interface Authenticable {

    fun onWhiteList()

    fun notOnWhiteList()
}