package addcel.mobilecard.ui.sms;

import androidx.annotation.NonNull;

import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 11/29/18.
 */
public interface SmsContract {
    interface View extends ScreenView {

        void onCodeCaptured(@NonNull String code);

        void clickResend();

        void onUsuarioSplash();

        void onNegocioSplash();

        void onUsuarioRegistro();

        void onNegocioRegistro();

        void onUsuarioLogin();

        void onNegocioLogin();

        void onUsuarioMenu();

        void onNegocioMenu();
    }

    interface Presenter {
        int getIdPais();

        void validateCode(String code);

        void resendSms(boolean firstLaunch);

        void clearDisposables();
    }
}
