package addcel.mobilecard.ui.sms;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 11/29/18.
 */
@PerActivity
@Subcomponent(modules = SmsModule.class)
public interface SmsSubcomponent {
    void inject(SmsActivity activity);
}
