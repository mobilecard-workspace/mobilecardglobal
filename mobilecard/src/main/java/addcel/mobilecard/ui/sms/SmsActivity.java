package addcel.mobilecard.ui.sms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.dpizarro.pinview.library.PinView;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroModel;
import addcel.mobilecard.ui.negocio.pe.menu.NegocioMenuPeActivity;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.utils.BundleBuilder;
import es.dmoral.toasty.Toasty;

public class SmsActivity extends AppCompatActivity implements SmsContract.View {

    @Inject
    @Named("email")
    String email;

    @Inject
    @Named("password")
    String password;

    @Inject
    @Named("imei")
    String imei;

    @Inject
    StateSession state;

    @Inject
    SmsContract.Presenter presenter;

    private ProgressBar progressBar;

    public static synchronized Intent get(Context context, int useCase, long id, int idPais) {
        return get(context, useCase, id, idPais, "", "", "");
    }

    public static synchronized Intent get(Context context, int useCase, long id, int idPais,
                                          String email, String pass, String imei) {
        Bundle extras = new BundleBuilder().putInt("useCase", useCase)
                .putLong("id", id)
                .putInt("idPais", idPais)
                .putString("email", email)
                .putString("password", pass)
                .putString("imei", imei)
                .build();
        return new Intent(context, SmsActivity.class).putExtras(extras);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().smsSubcomponent(new SmsModule(this)).inject(this);
        setContentView(R.layout.activity_sms);
        PinView pinView = findViewById(R.id.pinView);
        progressBar = findViewById(R.id.progress_sms);
        findViewById(R.id.tv_resend_bttn).setOnClickListener(v -> clickResend());

        pinView.setOnCompleteListener((completed, pinResults) -> {
            if (completed) {
                presenter.validateCode(pinResults);
            }
        });
    }

    @Override
    protected void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (progressBar.getVisibility() == View.GONE) progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (progressBar.getVisibility() == View.VISIBLE) progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        Toasty.success(this, msg).show();
    }

    @Override
    public void onCodeCaptured(@NonNull String code) {
        presenter.validateCode(code);
    }

    @Override
    public void clickResend() {
        presenter.resendSms(Boolean.FALSE);
    }

    @Override
    public void onUsuarioSplash() {
        startActivity(MainActivity.get(this, Boolean.FALSE));
        finish();
    }

    @Override
    public void onNegocioSplash() {
        startActivity(NegocioCobroActivity.Companion.get(this,
                new NegocioCobroModel(NegocioCobroActivity.USE_CASE_MENU)));
        finish();
    }

    @Override
    public void onUsuarioRegistro() {
        finish();
    }

    @Override
    public void onNegocioRegistro() {
        finish();
    }

    @Override
    public void onUsuarioLogin() {
        startActivity(MainActivity.get(this, Boolean.TRUE));
        finish();
    }

    @Override
    public void onNegocioLogin() {
        if (presenter.getIdPais() == 1) {
            startActivity(NegocioCobroActivity.Companion.get(this,
                    new NegocioCobroModel(NegocioCobroActivity.USE_CASE_MENU))
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        } else {
            startActivity(
                    NegocioMenuPeActivity.Companion.get(this, true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
        finish();
    }

    @Override
    public void onUsuarioMenu() {
        finish();
    }

    @Override
    public void onNegocioMenu() {
        finish();
    }
}
