package addcel.mobilecard.ui.sms;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.sms.SmsEvent;
import addcel.mobilecard.domain.sms.SmsInteractor;
import addcel.mobilecard.domain.sms.SmsUseCase;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 11/29/18.
 */
class SmsPresenter implements SmsContract.Presenter {
    private final SmsInteractor interactor;
    private final SmsContract.View view;

    SmsPresenter(SmsInteractor interactor, SmsContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public int getIdPais() {
        return interactor.getIdpais();
    }

    @Override
    public void validateCode(String code) {
        view.showProgress();
        interactor.validateCode(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), code,
                new InteractorCallback<SmsEvent>() {
                    @Override
                    public void onSuccess(@NotNull SmsEvent result) {
                        view.hideProgress();
                        view.showSuccess(result.getMensaje());
                        switch (result.getUseCase()) {
                            case SmsUseCase.U_SPLASH:
                                view.onUsuarioSplash();
                                break;
                            case SmsUseCase.U_REGISTRO:
                                view.onUsuarioRegistro();
                                break;
                            case SmsUseCase.U_LOGIN:
                                view.onUsuarioLogin();
                                break;
                            case SmsUseCase.N_SPLASH:
                                view.onNegocioSplash();
                                break;
                            case SmsUseCase.N_REGISTRO:
                                view.onNegocioRegistro();
                                break;
                            case SmsUseCase.N_LOGIN:
                                view.onNegocioLogin();
                                break;
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void resendSms(boolean firstLaunch) {

        boolean condition = !firstLaunch || (interactor.getUseCase() != SmsUseCase.U_REGISTRO
                && interactor.getUseCase() != SmsUseCase.N_REGISTRO);

        if (condition) {
            view.showProgress();
            interactor.resendSms(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                    new InteractorCallback<DefaultResponse>() {
                        @Override
                        public void onSuccess(@NotNull DefaultResponse result) {
                            view.hideProgress();
                            view.showSuccess(result.getMensajeError());
                        }

                        @Override
                        public void onError(@NotNull String error) {
                            view.hideProgress();
                            view.showError(error);
                        }
                    });
        }
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }
}
