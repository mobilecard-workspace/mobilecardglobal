package addcel.mobilecard.ui.sms;

import com.google.common.base.Preconditions;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.domain.sms.SmsInteractor;
import addcel.mobilecard.domain.sms.SmsInteractorImpl;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 11/29/18.
 */
@Module
public final class SmsModule {
    private final SmsActivity activity;

    SmsModule(SmsActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    @Named("useCase")
    int provideUseCase() {
        return Preconditions.checkNotNull(activity.getIntent().getExtras()).getInt("useCase");
    }

    @PerActivity
    @Provides
    @Named("id")
    long provideId() {
        return Preconditions.checkNotNull(activity.getIntent().getExtras()).getLong("id");
    }

    @PerActivity
    @Provides
    @Named("idPais")
    int provideIdPais() {
        return Preconditions.checkNotNull(activity.getIntent().getExtras()).getInt("idPais");
    }

    @PerActivity
    @Provides
    @Named("email")
    String provideEmail() {
        return Preconditions.checkNotNull(activity.getIntent().getExtras()).getString("email");
    }

    @PerActivity
    @Provides
    @Named("password")
    String providePassword() {
        return Preconditions.checkNotNull(activity.getIntent().getExtras()).getString("password");
    }

    @PerActivity
    @Provides
    @Named("imei")
    String provideImei() {
        return Preconditions.checkNotNull(activity.getIntent().getExtras()).getString("imei");
    }

    @PerActivity
    @Provides
    SmsInteractor provideInteractor(UsuariosService api,
                                    SessionOperations session, @Named("useCase") int useCase, @Named("id") long id,
                                    @Named("idPais") int idPais) {
        return new SmsInteractorImpl(api, session, useCase, id, idPais);
    }

    @PerActivity
    @Provides
    SmsContract.Presenter providePresenter(SmsInteractor interactor) {
        return new SmsPresenter(interactor, activity);
    }
}
