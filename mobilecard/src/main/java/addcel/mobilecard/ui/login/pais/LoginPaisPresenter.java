package addcel.mobilecard.ui.login.pais;

import android.annotation.SuppressLint;

import com.google.common.collect.Lists;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 20/08/18.
 */
class LoginPaisPresenter implements LoginPaisContract.Presenter {
    private final CatalogoService service;
    private final LoginPaisContract.View view;
    private final CompositeDisposable disposable;

    LoginPaisPresenter(CatalogoService service, LoginPaisContract.View view) {
        this.service = service;
        this.disposable = new CompositeDisposable();
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getPaises() {
        disposable.add(service.getPaises(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::setPaises, throwable -> {
                    view.setPaises(Lists.newArrayList());
                    view.showError(ErrorUtil.Companion.getFormattedHttpErrorMsg(throwable));
                }));
    }

    @Override
    public void clearDisposable() {
        disposable.clear();
    }
}
