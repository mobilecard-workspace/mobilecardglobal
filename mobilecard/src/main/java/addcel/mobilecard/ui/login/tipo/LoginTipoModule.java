package addcel.mobilecard.ui.login.tipo;

import com.squareup.picasso.Picasso;

import java.util.Objects;

import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.login.LoginActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 20/08/18.
 */
@Module
public final class LoginTipoModule {
    private final LoginTipoFragment fragment;

    LoginTipoModule(LoginTipoFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    LoginActivity provideActivity() {
        return (LoginActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    PaisResponse.PaisEntity providePais() {
        return Objects.requireNonNull(fragment.getArguments()).getParcelable("paisModel");
    }

    @PerFragment
    @Provides
    LoginTipoAdapter provideAdapter(Picasso picasso) {
        return new LoginTipoAdapter(picasso);
    }
}
