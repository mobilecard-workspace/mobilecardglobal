package addcel.mobilecard.ui.login

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse.PaisEntity
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.login.inicio.LoginInicioFragment
import addcel.mobilecard.ui.login.negocio.LoginNegocioFragment
import addcel.mobilecard.ui.login.pais.LoginPaisFragment
import addcel.mobilecard.ui.login.tipo.LoginTipoContract
import addcel.mobilecard.ui.login.tipo.LoginTipoFragment
import addcel.mobilecard.ui.login.usuario.LoginUsuarioFragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_login.*


@Parcelize
data class LoginViewModel(
        val useCase: LoginContract.UseCase = LoginContract.UseCase.PAIS,
        val pais: PaisEntity? = null,
        val perfil: LoginTipoContract.Tipo? = null
) : Parcelable


class LoginActivity : ContainerActivity(), LoginView {

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        setContentView(R.layout.activity_login)
        viewModel = intent?.getParcelableExtra("model")!!
        launchScreen(viewModel.useCase)
    }

    override fun getRetry(): View? {
        return null
    }

    override fun launchUsuarioScreen() {
        fragmentManagerLazy.beginTransaction()
                .add(R.id.frame_login, LoginUsuarioFragment.get(viewModel.pais))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit()
    }

    override fun launchNegocioScreen() {
        fragmentManagerLazy.beginTransaction()
                .add(R.id.frame_login, LoginNegocioFragment.get(viewModel.pais))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit()
    }

    override fun launchScreen(useCase: LoginContract.UseCase) {
        when (useCase) {
            LoginContract.UseCase.TIPO -> launchTipoScreen()
            LoginContract.UseCase.INICIO -> launchInicioScreen()
            LoginContract.UseCase.USUARIO -> launchUsuarioScreen()
            LoginContract.UseCase.NEGOCIO -> launchNegocioScreen()
            else -> launchPaisScreen()
        }
    }

    override fun launchPaisScreen() {
        fragmentManagerLazy.beginTransaction()
                .add(R.id.frame_login, LoginPaisFragment.get())
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit()
    }

    override fun launchTipoScreen() {
        fragmentManagerLazy.beginTransaction()
                .add(R.id.frame_login, LoginTipoFragment.get(viewModel.pais))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit()
    }

    override fun launchInicioScreen() {
        if (viewModel.pais != null) {
            fragmentManagerLazy.beginTransaction()
                    .add(R.id.frame_login, LoginInicioFragment.get(viewModel.pais!!, viewModel.perfil))
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit()
        }
    }

    override fun showProgress() {
        if (progress_login.visibility == View.GONE) progress_login.visibility = View.VISIBLE
    }

    override fun hideRetry() {

    }

    override fun hideProgress() {
        if (progress_login.visibility == View.VISIBLE) progress_login.visibility = View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {}
    override fun setAppToolbarTitle(title: String) {}
    override fun showRetry() {

    }


    companion object {
        fun get(
                context: Context,
                useCase: LoginContract.UseCase = LoginContract.UseCase.PAIS,
                pais: PaisEntity? = null,
                perfil: LoginTipoContract.Tipo? = null
        ): Intent {
            val viewModel = LoginViewModel(useCase, pais, perfil)
            return Intent(context, LoginActivity::class.java).putExtra("model", viewModel)
        }
    }
}