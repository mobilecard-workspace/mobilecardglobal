package addcel.mobilecard.ui.login.tipo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Maps;
import com.squareup.picasso.Picasso;

import java.util.Map;

import addcel.mobilecard.R;
import addcel.mobilecard.utils.MapUtils;

/**
 * ADDCEL on 20/08/18.
 */
public final class LoginTipoAdapter extends RecyclerView.Adapter<LoginTipoAdapter.ViewHolder> {
    private static final Map<LoginTipoContract.Tipo, Integer> map = Maps.newLinkedHashMap();

    static {
        map.put(LoginTipoContract.Tipo.USUARIO, R.drawable.icon_user);
        map.put(LoginTipoContract.Tipo.NEGOCIO, R.drawable.icon_establecimiento);
    }

    private final Picasso picasso;

    LoginTipoAdapter(Picasso picasso) {
        this.picasso = picasso;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_usuario_tipo, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        LoginTipoContract.Tipo tipo = LoginTipoContract.Tipo.values()[i];
        viewHolder.name.setText(tipo.equals(LoginTipoContract.Tipo.NEGOCIO) ? "COMERCIO" : tipo.name());
        viewHolder.label.setText("Ingresa como: ");
        viewHolder.icon.setImageResource(MapUtils.getOrDefault(map, tipo, R.drawable.icon_user));

    /*picasso.load(MapUtils.getOrDefault(map, tipo, R.drawable.icon_user))
    .placeholder(R.drawable.icon_user)
    .error(R.drawable.icon_user)
    .resize(200, 117)
    .centerInside()
    .into(viewHolder.icon);*/
    }

    public LoginTipoContract.Tipo getItem(int pos) {
        return LoginTipoContract.Tipo.values()[pos];
    }

    @Override
    public int getItemCount() {
        return LoginTipoContract.Tipo.values().length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView icon;
        private final TextView name;
        private final TextView label;

        ViewHolder(View itemView) {
            super(itemView);
            label = itemView.findViewById(R.id.label_usuario_tipo_info);
            icon = itemView.findViewById(R.id.img_menu_item_icon);
            name = itemView.findViewById(R.id.text_menu_item_name);
        }
    }
}
