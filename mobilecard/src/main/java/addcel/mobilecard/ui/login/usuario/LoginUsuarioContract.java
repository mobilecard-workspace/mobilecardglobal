package addcel.mobilecard.ui.login.usuario;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;

/**
 * ADDCEL on 05/09/18.
 */
public interface LoginUsuarioContract {
    interface View extends Validator.ValidationListener {

        void login();

        void clearSignInValues();

        void showReset();

        void finishView();

        void showProgress();

        void hideProgress();

        void launchOnBoardingInfo(long idUsuario);

        void launchPassword(Usuario usuario);

        void launchSmsView(Usuario usuario);

        void launchMenu();

        void launchRegistro();

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);

        void onResetResult(int resultCode, @Nullable Intent data);

        void onNovedadesResult(int resultCode, @Nullable Intent data);
    }

    interface Presenter {

        void clearDisposables();

        void login(String login, String password, String imei);

        void resetPassword(String login);

        void updateReference(String login, String reference);

        PaisResponse.PaisEntity getPais();

    }
}
