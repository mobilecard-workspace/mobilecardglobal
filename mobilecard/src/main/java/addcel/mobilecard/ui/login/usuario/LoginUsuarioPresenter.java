package addcel.mobilecard.ui.login.usuario;

import android.annotation.SuppressLint;
import android.os.Build;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.model.LoginRequest;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.login.UsuarioStatus;
import addcel.mobilecard.domain.login.usuario.LoginUsuarioInteractor;
import addcel.mobilecard.ui.login.tipo.LoginTipoContract;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 29/11/16.
 */
public class LoginUsuarioPresenter implements LoginUsuarioContract.Presenter {

    private final LoginUsuarioInteractor interactor;
    private final PaisResponse.PaisEntity pais;
    private final LoginUsuarioContract.View view;

    LoginUsuarioPresenter(LoginUsuarioInteractor interactor, PaisResponse.PaisEntity pais,
                          LoginUsuarioContract.View view) {
        this.interactor = interactor;
        this.view = view;
        this.pais = pais;
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @SuppressLint("CheckResult")
    @Override
    public void login(String login, String password, String imei) {
        view.showProgress();
        LoginRequest request = new LoginRequest.Builder().setImei(imei)
                .setManufacturer(Build.MANUFACTURER)
                .setOs(String.valueOf(Build.VERSION.SDK_INT))
                .setPlatform("ANDROID")
                .setUsrLoginOrEmail(login)
                .setUsrPwd(AddcelCrypto.encryptHard(password))
                .setTipoUsuario(LoginTipoContract.Tipo.USUARIO)
                .build();

        interactor.login(BuildConfig.ADDCEL_APP_ID, pais.getId(), StringUtil.getCurrentLanguage(),
                request, new InteractorCallback<Usuario>() {
                    @Override
                    public void onSuccess(@NotNull Usuario result) {
                        view.hideProgress();
                        int status = result.getIdUsrStatus();
                        switch (status) {
                            case UsuarioStatus.ACTIVO:
                            case UsuarioStatus.EMAIL_VERIFICATION:
                            case UsuarioStatus.SMS_VERIFICATION:
                                processUser(result);
                                break;
                            default:
                                view.showError(result.getMensajeError());
                                break;
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void resetPassword(String login) {
        view.showProgress();
        interactor.reset(BuildConfig.ADDCEL_APP_ID, pais.getId(), StringUtil.getCurrentLanguage(),
                login, new InteractorCallback<McResponse>() {
                    @Override
                    public void onSuccess(@NotNull McResponse result) {
                        view.hideProgress();
                        view.showSuccess(result.getMensajeError());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void updateReference(String login, String reference) {
        view.showProgress();
        interactor.updateReference(BuildConfig.ADDCEL_APP_ID, pais.getId(),
                StringUtil.getCurrentLanguage(), login, reference, new InteractorCallback<McResponse>() {
                    @Override
                    public void onSuccess(@NotNull McResponse result) {
                        view.hideProgress();
                        if (result.getIdError() == 0) {
                            view.showSuccess(result.getMensajeError());
                        } else {
                            view.showError(result.getMensajeError());
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(ErrorUtil.Companion.getErrorMsg(ErrorUtil.NETWORK));
                    }
                });
    }

    @Override
    public PaisResponse.PaisEntity getPais() {
        return pais;
    }

    private void processUser(Usuario usuario) {
        if (usuario.getIdUsrStatus() == UsuarioStatus.ACTIVO || usuario.getIdUsrStatus() == UsuarioStatus.EMAIL_VERIFICATION) {

            view.clearSignInValues();
            view.showSuccess(usuario.getMensajeError());
            view.launchMenu();

        } else if (usuario.getIdUsrStatus() == UsuarioStatus.SMS_VERIFICATION) {
            view.launchSmsView(usuario);
        }
    }
}
