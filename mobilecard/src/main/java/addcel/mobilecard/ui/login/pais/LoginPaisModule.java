package addcel.mobilecard.ui.login.pais;

import com.google.common.collect.Lists;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.login.LoginActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 20/08/18.
 */
@Module
public final class LoginPaisModule {
    private final LoginPaisFragment fragment;

    LoginPaisModule(LoginPaisFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    LoginActivity provideActivity() {
        return (LoginActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    LoginPaisAdapter provideAdapter(Picasso picasso, LoginActivity activity) {
        return new LoginPaisAdapter(picasso,
                activity.getWindow().getWindowManager().getDefaultDisplay().getWidth(),
                Lists.newArrayList());
    }

    @PerFragment
    @Provides
    LoginPaisContract.Presenter providePresenter(CatalogoService service) {
        return new LoginPaisPresenter(service, fragment);
    }
}
