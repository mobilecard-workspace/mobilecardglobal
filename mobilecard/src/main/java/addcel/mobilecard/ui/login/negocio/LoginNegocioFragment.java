package addcel.mobilecard.ui.login.negocio;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.domain.sms.SmsUseCase;
import addcel.mobilecard.ui.login.LoginActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroModel;
import addcel.mobilecard.ui.negocio.mx.password.NegocioPasswordActivity;
import addcel.mobilecard.ui.negocio.pe.menu.NegocioMenuPeActivity;
import addcel.mobilecard.ui.password.reset.ResetPasswordActivity;
import addcel.mobilecard.ui.password.reset.ResetPasswordModel;
import addcel.mobilecard.ui.sms.SmsActivity;
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroActivity;
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroModel;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.validation.ValidationUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * ADDCEL on 05/09/18.
 */
public final class LoginNegocioFragment extends Fragment implements LoginNegocioContract.View {
    private static final String[] PERMISSIONS = {
            Manifest.permission.READ_CONTACTS, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };
    @Email(messageResId = R.string.error_email)
    @BindView(R.id.input_login)
    TextInputLayout emailTil;
    @Password(scheme = Password.Scheme.ANY, messageResId = R.string.error_password, min = 8)
    @BindView(R.id.input_password)
    TextInputLayout passwordTil;

    @Inject
    SessionOperations session;
    @Inject
    StateSession state;
    @Inject
    LoginActivity activity;
    @Inject
    Validator validator;
    @Inject
    LoginNegocioPresenter presenter;

    private Unbinder unbinder;

    public LoginNegocioFragment() {
    }

    public static synchronized LoginNegocioFragment get(PaisResponse.PaisEntity paisModel) {
        LoginNegocioFragment fragment = new LoginNegocioFragment();
        fragment.setArguments(new BundleBuilder().putParcelable("paisModel", paisModel).build());
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .loginNegocioSubcomponent(new LoginNegocioModule(this))
                .inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_login_negocio, container, false);
        unbinder = ButterKnife.bind(this, view);
        Button pgOlvidarButton = view.findViewById(R.id.b_login_olvidar);
        pgOlvidarButton.setPaintFlags(pgOlvidarButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        pgOlvidarButton.setOnClickListener(v -> clickReset());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.logout();
        final RxPermissions permissions = new RxPermissions(this);
        presenter.addToDisposables(permissions.request(PERMISSIONS).subscribe(aBoolean -> {
            if (!aBoolean) {
                showError("Requerimos tu autorización para utilizar MobileCard al maximo");
            }
        }));
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposable();
        super.onDestroy();
    }

    @Override
    public void clearSignInValues() {
        state.setCapturedPhone("");
        state.setCapturedEmail("");
    }

    @Override
    public String getEmail() {
        return Strings.nullToEmpty(Objects.requireNonNull(emailTil.getEditText()).getText().toString());
    }

    @Override
    public String getPassword() {
        return Strings.nullToEmpty(
                Objects.requireNonNull(passwordTil.getEditText()).getText().toString());
    }

    @Override
    public String getImei() {
        return DeviceUtil.Companion.getDeviceId(activity);
    }

    @OnClick(R.id.b_login_login)
    @Override
    public void clickLogin() {
        ValidationUtils.clearViewErrors(emailTil, passwordTil);
        validator.validate();
    }

    @Override
    public void clickReset() {

        final ResetPasswordModel rpModel =
                new ResetPasswordModel(ResetPasswordActivity.USE_CASE_NEGOCIO, presenter.fetchPais().getId());

        startActivityForResult(ResetPasswordActivity.Companion.get(activity, rpModel),
                ResetPasswordActivity.REQUEST_CODE);
    }

    @OnClick(R.id.b_login_registrate)
    @Override
    public void clickRegistar() {
        activity.startActivity(SmsRegistroActivity.Companion.get(activity,
                new SmsRegistroModel(presenter.fetchPais(), SmsRegistroActivity.USE_CASE_NEGOCIO)));
    }

    @Override
    public void launchPassword(NegocioEntity negocio) {
        AndroidUtils.getEditText(emailTil).setText("");
        AndroidUtils.getEditText(passwordTil).setText("");
        activity.startActivity(NegocioPasswordActivity.get(activity, negocio, Boolean.FALSE));
        activity.finish();
    }

    @Override
    public void launchEmail(NegocioEntity entity) {
        presenter.resendActivation(entity);
    }

    @Override
    public void onEmailResend(DefaultResponse response) {
        if (response.getIdError() == 0) {
            showSuccess(response.getMensajeError());
        } else {
            showError(response.getMensajeError());
        }
    }

    @Override
    public void launchSms(NegocioEntity entity) {
        activity.startActivity(
                SmsActivity.get(activity, SmsUseCase.N_LOGIN, entity.getIdEstablecimiento(),
                        presenter.fetchPais().getId()));
        activity.finish();
    }


    @Override
    public void launchMenu(int idPais) {
        if (idPais == 1) {
            activity.startActivity(NegocioCobroActivity.Companion.get(activity,
                    new NegocioCobroModel(NegocioCobroActivity.USE_CASE_MENU))
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        } else if (idPais == 4) {
            activity.startActivity(NegocioMenuPeActivity.Companion.get(activity, state.isMenuLaunched())
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
        activity.finish();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void showResendDialog(NegocioEntity entity) {
        if (isVisible()) {
            new AlertDialog.Builder(activity).setTitle(android.R.string.dialog_alert_title)
                    .setMessage(R.string.txt_account_activation)
                    .setPositiveButton(R.string.txt_account_activation_btn,
                            (dialogInterface, i) -> launchEmail(entity))
                    .setNegativeButton(android.R.string.cancel,
                            (dialogInterface, i) -> dialogInterface.dismiss())
                    .show();
        }
    }

    @Override
    public void showResetDialog() {

    }

    @Override
    public void hideResetDialog() {

    }


    @Override
    public void onResetResult(int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            String message = "";
            if (data != null) {
                message = data.getStringExtra(ResetPasswordActivity.DATA_MESSAGE);
            }
            if (!Strings.isNullOrEmpty(message)) {
                showSuccess(message);
            }
        }
    }

    @Override
    public void onValidationSucceeded() {
        presenter.login();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ResetPasswordActivity.REQUEST_CODE) {
            onResetResult(resultCode, data);
        }
    }
}
