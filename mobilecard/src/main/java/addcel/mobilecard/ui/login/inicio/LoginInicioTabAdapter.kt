package addcel.mobilecard.ui.login.inicio

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter

/**
 * ADDCEL on 2019-05-29.
 */
class LoginInicioTabAdapter(private val layouts: Array<Int>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = ImageView(container.context)
        view.scaleType = ImageView.ScaleType.CENTER_INSIDE
        view.setImageResource(layouts[position])
        container.addView(view)

        return view
    }

    override fun getCount(): Int {
        return layouts.size
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val view = `object` as View
        container.removeView(view)
    }
}

