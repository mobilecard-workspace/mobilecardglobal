package addcel.mobilecard.ui.login

/**
 * ADDCEL on 2019-12-11.
 */
interface LoginView {

    fun launchScreen(useCase: LoginContract.UseCase)

    fun launchPaisScreen()

    fun launchTipoScreen()

    fun launchInicioScreen()

    fun launchUsuarioScreen()

    fun launchNegocioScreen()


}