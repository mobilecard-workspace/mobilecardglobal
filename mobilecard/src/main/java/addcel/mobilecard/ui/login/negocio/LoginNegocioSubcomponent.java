package addcel.mobilecard.ui.login.negocio;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 29/11/16.
 */
@PerFragment
@Subcomponent(modules = LoginNegocioModule.class)
public interface LoginNegocioSubcomponent {
    void inject(LoginNegocioFragment fragment);
}
