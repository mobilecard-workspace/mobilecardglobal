package addcel.mobilecard.ui.login.usuario;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 29/11/16.
 */
@PerFragment
@Subcomponent(modules = LoginUsuarioModule.class)
public interface LoginUsuarioSubcomponent {
    void inject(LoginUsuarioFragment fragment);
}
