package addcel.mobilecard.ui.login.tipo;

/**
 * ADDCEL on 20/08/18.
 */
public interface LoginTipoContract {
    enum Tipo {
        USUARIO, NEGOCIO
    }

    interface View {
        void showProgress();

        void hideProgress();

        void showError(String msg);

        void showSuccess(String msg);

        void clickTipo(int pos);
    }

    interface Presenter {
    }
}
