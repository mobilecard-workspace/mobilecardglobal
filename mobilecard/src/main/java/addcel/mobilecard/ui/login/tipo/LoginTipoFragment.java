package addcel.mobilecard.ui.login.tipo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.login.LoginActivity;
import addcel.mobilecard.ui.login.inicio.LoginInicioFragment;
import addcel.mobilecard.ui.login.usuario.LoginUsuarioFragment;

/**
 * ADDCEL on 20/08/18.
 */
public class LoginTipoFragment extends Fragment implements LoginTipoContract.View {
    @Inject
    LoginActivity activity;
    @Inject
    PaisResponse.PaisEntity paisModel;
    @Inject
    SessionOperations session;
    @Inject
    LoginTipoAdapter adapter;
    private RecyclerView tiposView;

    public static synchronized LoginTipoFragment get(PaisResponse.PaisEntity paisModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("paisModel", paisModel);
        LoginTipoFragment fragment = new LoginTipoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().usuarioSubcomponent(new LoginTipoModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_launch_usuario, container, false);
        tiposView = view.findViewById(R.id.recycler_login_tipo);
        tiposView.setAdapter(adapter);
        tiposView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(tiposView)
                .setOnItemClickListener((recyclerView, position, v) -> clickTipo(position));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        session.logout();
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(tiposView);
        tiposView.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void clickTipo(int pos) {
        LoginTipoContract.Tipo tipo = adapter.getItem(pos);

        if (tipo == LoginTipoContract.Tipo.NEGOCIO) {
            launchLoginNegocioScreen(paisModel, tipo);
        } else {
            launchLoginUsuarioScreen(paisModel);
        }
    }

    private void launchLoginUsuarioScreen(PaisResponse.PaisEntity paisModel) {
        if (isVisible()) {

            Fragment nextFragment = paisModel.getId() == 3 ? LoginUsuarioFragment.get(paisModel)
                    : LoginInicioFragment.Companion.get(paisModel, LoginTipoContract.Tipo.USUARIO);

            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_login, nextFragment)
                    .addToBackStack(null)
                    .hide(this)
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        }
    }

    private void launchLoginNegocioScreen(PaisResponse.PaisEntity paisModel,
                                          LoginTipoContract.Tipo tipo) {
        if (isVisible()) {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_login, LoginInicioFragment.Companion.get(paisModel, tipo))
                    .addToBackStack(null)
                    .hide(this)
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        }
    }
}
