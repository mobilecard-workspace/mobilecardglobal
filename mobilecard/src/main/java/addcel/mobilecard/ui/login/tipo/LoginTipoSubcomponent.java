package addcel.mobilecard.ui.login.tipo;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 20/08/18.
 */
@PerFragment
@Subcomponent(modules = LoginTipoModule.class)
public interface LoginTipoSubcomponent {
    void inject(LoginTipoFragment fragment);
}
