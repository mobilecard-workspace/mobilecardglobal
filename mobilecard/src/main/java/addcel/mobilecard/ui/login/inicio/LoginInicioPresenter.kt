package addcel.mobilecard.ui.login.inicio

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.ui.login.tipo.LoginTipoContract

/**
 * ADDCEL on 2019-07-09.
 */
interface LoginInicioPresenter {

    fun getImgArray(idPais: Int, perfil: LoginTipoContract.Tipo): Array<Int>
}

class LoginInicioPresenterImpl : LoginInicioPresenter {

    //SE OCULTAN PANELES QUE INVOLUCREN RECONOCIMIENTO FACIAL

    companion object {
        val mxUsuarioViews: Array<Int> =
                arrayOf(
                        R.drawable.car_mex_01, R.drawable.car_mex_02, R.drawable.car_mex_03,
                        R.drawable.car_mex_04, R.drawable.car_mex_05,
                        //R.drawable.car_mex_06,
                        R.drawable.car_mex_07, R.drawable.car_mex_08
                )

        val peUsuarioViews: Array<Int> =
                arrayOf(
                        R.drawable.car_per_01, R.drawable.car_per_02, R.drawable.car_per_03,
                        R.drawable.car_per_04
                )

        val peNegocioViews: Array<Int> =
                arrayOf(
                        R.drawable.car_neg_pe_01, R.drawable.car_neg_pe_02, R.drawable.car_neg_pe_03,
                        R.drawable.car_neg_pe_04
                        //, R.drawable.car_neg_pe_05
                )
    }

    override fun getImgArray(idPais: Int, perfil: LoginTipoContract.Tipo): Array<Int> {
        if (idPais == PaisResponse.PaisEntity.MX) {
            return if (perfil == LoginTipoContract.Tipo.USUARIO) {
                mxUsuarioViews
            } else {
                peNegocioViews
            }
        } else if (idPais == PaisResponse.PaisEntity.PE) {
            return if (perfil == LoginTipoContract.Tipo.USUARIO) {
                peUsuarioViews
            } else {
                peNegocioViews
            }
        }
        return mxUsuarioViews
    }
}