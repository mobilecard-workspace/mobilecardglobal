package addcel.mobilecard.ui.login.usuario;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.sms.SmsUseCase;
import addcel.mobilecard.ui.login.LoginActivity;
import addcel.mobilecard.ui.novedades.NovedadesActivity;
import addcel.mobilecard.ui.password.reset.ResetPasswordActivity;
import addcel.mobilecard.ui.password.reset.ResetPasswordModel;
import addcel.mobilecard.ui.sms.SmsActivity;
import addcel.mobilecard.ui.usuario.main.MainActivity;
import addcel.mobilecard.ui.usuario.mymobilecard.MyMobilecardActivity;
import addcel.mobilecard.ui.usuario.password.PasswordActivity;
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroActivity;
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroModel;
import addcel.mobilecard.ui.usuario.wallet.WalletContainerActivity;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.validation.ValidationUtils;
import addcel.mobilecard.validation.annotation.Login;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * ADDCEL on 05/09/18.
 */
public final class LoginUsuarioFragment extends Fragment implements LoginUsuarioContract.View {

    private static final String[] PERMISSIONS = {
            Manifest.permission.READ_CONTACTS, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.CHANGE_WIFI_STATE
    };
    private final CompositeDisposable permissionDisposables = new CompositeDisposable();
    @Login
    @BindView(R.id.input_login)
    TextInputLayout loginTil;
    @Password(messageResId = R.string.error_password, min = 8, scheme = Password.Scheme.ANY)
    @BindView(R.id.input_password)
    TextInputLayout passwordTil;
    @Inject
    SessionOperations session;
    @Inject
    StateSession state;
    @Inject
    Validator validator;
    @Inject
    LoginActivity activity;
    @Inject
    LoginUsuarioContract.Presenter presenter;

    private Unbinder unbinder;
    private RxPermissions permissions;

    public static synchronized LoginUsuarioFragment get(PaisResponse.PaisEntity pais) {
        Bundle b = new BundleBuilder().putParcelable("paisModel", pais).build();
        LoginUsuarioFragment fragment = new LoginUsuarioFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .loginUsuarioSubcomponent(new LoginUsuarioModule(this))
                .inject(this);

        permissions = new RxPermissions(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_login_usuario, container, false);
        unbinder = ButterKnife.bind(this, view);
        Timber.i("onCreate Login Activity");
        Button pgOlvidarButton = view.findViewById(R.id.b_login_olvidar);
        pgOlvidarButton.setPaintFlags(pgOlvidarButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        session.logout();
        permissionDisposables.add(permissions.request(PERMISSIONS).subscribe());
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        permissionDisposables.clear();
        super.onDestroy();
    }

    @OnClick(R.id.b_login_login)
    @Override
    public void login() {
        ValidationUtils.clearViewErrors(loginTil, passwordTil);
        validator.validate();
    }

    @Override
    public void clearSignInValues() {
        state.setCapturedPhone("");
        state.setCapturedEmail("");
    }

    @OnClick(R.id.b_login_olvidar)
    @Override
    public void showReset() {
        final ResetPasswordModel rpModel =
                new ResetPasswordModel(ResetPasswordActivity.USE_CASE_USUARIO, presenter.getPais().getId());

        startActivityForResult(ResetPasswordActivity.Companion.get(activity, rpModel),
                ResetPasswordActivity.REQUEST_CODE);
    }

    @Override
    public void finishView() {
        activity.finish();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void launchOnBoardingInfo(long idUsuario) {

    }

    @Override
    public void launchPassword(Usuario usuario) {
        activity.startActivity(PasswordActivity.get(activity, usuario));
        activity.finish();
    }

    @Override
    public void launchSmsView(Usuario usuario) {
        startActivity(SmsActivity.get(activity, SmsUseCase.U_LOGIN, usuario.getIdeUsuario(),
                usuario.getIdPais()));
        activity.finish();
    }

    @Override
    public void launchMenu() {
        startActivity(
                MainActivity.get(activity, Boolean.TRUE).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        activity.finish();
    }

    @OnClick(R.id.b_login_registrate)
    @Override
    public void launchRegistro() {
        startActivity(SmsRegistroActivity.Companion.get(activity,
                new SmsRegistroModel(presenter.getPais(), SmsRegistroActivity.USE_CASE_USUARIO)));
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void onValidationSucceeded() {
        presenter.login(Objects.requireNonNull(loginTil.getEditText()).getText().toString().trim(),
                Objects.requireNonNull(passwordTil.getEditText()).getText().toString().trim(),
                DeviceUtil.Companion.getDeviceId(activity));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ResetPasswordActivity.REQUEST_CODE) {
            onResetResult(resultCode, data);
        } else if (requestCode == NovedadesActivity.REQUEST_CODE) {
            onNovedadesResult(resultCode, data);
        }
    }

    @Override
    public void onResetResult(int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            String message = "";
            if (data != null) {
                message = data.getStringExtra(ResetPasswordActivity.DATA_MESSAGE);
            }
            if (!Strings.isNullOrEmpty(message)) {
                showSuccess(message);
            }
        }
    }

    @Override
    public void onNovedadesResult(int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                int result = data.getIntExtra(NovedadesActivity.DATA_USE_CASE, NovedadesActivity.USE_CASE_NO_CARD);
                switch (result) {
                    case NovedadesActivity.USE_CASE_MOBILECARD:
                        startActivity(MyMobilecardActivity.get(activity, true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finishView();
                        break;
                    case NovedadesActivity.USE_CASE_GET_CARD:
                        startActivity(WalletContainerActivity.get(activity, true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finishView();
                        break;
                    case NovedadesActivity.USE_CASE_NO_CARD:
                        startActivity(MainActivity.get(activity, true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finishView();
                        break;
                }
            }
        }
    }
}
