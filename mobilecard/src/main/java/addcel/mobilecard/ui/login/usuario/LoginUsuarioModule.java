package addcel.mobilecard.ui.login.usuario;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Objects;

import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.usuarios.UsuariosService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.login.usuario.LoginUsuarioInteractor;
import addcel.mobilecard.domain.login.usuario.LoginUsuarioInteractorImpl;
import addcel.mobilecard.ui.login.LoginActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 05/09/18.
 */
@Module
public final class LoginUsuarioModule {
    private final LoginUsuarioFragment fragment;

    LoginUsuarioModule(LoginUsuarioFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    LoginActivity provideActivity() {
        return (LoginActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    @Nullable
    Bundle getExtras() {
        return fragment.getArguments();
    }

    @PerFragment
    @Provides
    PaisResponse.PaisEntity providePaisModel(@Nullable Bundle bundle) {
        return bundle != null ? Preconditions.checkNotNull(bundle.getParcelable("paisModel"))
                : PaisResponse.PaisEntity.DEFAULT_VALUE;
    }

    @PerFragment
    @Provides
    LoginUsuarioInteractor provideInteractor(UsuariosService service,
                                             SessionOperations session, StateSession state) {
        return new LoginUsuarioInteractorImpl(service, session, state);
    }

    @PerFragment
    @Provides
    LoginUsuarioContract.Presenter providePresenter(
            LoginUsuarioInteractor interactor, PaisResponse.PaisEntity pais) {
        return new LoginUsuarioPresenter(interactor, pais, fragment);
    }
}
