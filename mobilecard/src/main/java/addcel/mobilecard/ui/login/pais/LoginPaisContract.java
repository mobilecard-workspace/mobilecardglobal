package addcel.mobilecard.ui.login.pais;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.PaisResponse;

/**
 * ADDCEL on 20/08/18.
 */
public interface LoginPaisContract {

    interface View {
        void showProgress();

        void hideProgress();

        void setPaises(List<PaisResponse.PaisEntity> paises);

        void clickRetry();

        void showError(String msg);

        void clickPais(int pos);

        void launchUsuario(PaisResponse.PaisEntity paisModel);

        void launchLoginUsuario(PaisResponse.PaisEntity paisModel);

        int getH();

        int getW();
    }

    interface Presenter {
        void getPaises();

        void clearDisposable();
    }
}
