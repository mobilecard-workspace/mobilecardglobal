package addcel.mobilecard.ui.login.pais;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.login.LoginActivity;
import addcel.mobilecard.ui.login.tipo.LoginTipoFragment;
import addcel.mobilecard.ui.login.usuario.LoginUsuarioFragment;

/**
 * ADDCEL on 20/08/18.
 */
public class LoginPaisFragment extends Fragment implements LoginPaisContract.View {
    @Inject
    LoginActivity activity;
    @Inject
    LoginPaisAdapter adapter;
    @Inject
    LoginPaisContract.Presenter presenter;
    @Inject
    SessionOperations session;
    private RecyclerView paisesView;
    private Button refreshButton;

    public static synchronized LoginPaisFragment get() {
        return new LoginPaisFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get().getNetComponent().paisSubcomponent(new LoginPaisModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_launch_pais, container, false);
        paisesView = view.findViewById(R.id.recycler_launch_pais_paises);
        refreshButton = view.findViewById(R.id.b_launch_pais_refresh);
        refreshButton.setOnClickListener(view1 -> clickRetry());
        paisesView.setAdapter(adapter);
        paisesView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(paisesView)
                .setOnItemClickListener((recyclerView, position, v) -> clickPais(position));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        session.logout();
        if (adapter.getItemCount() == 0) presenter.getPaises();
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(paisesView);
        paisesView.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposable();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void setPaises(List<PaisResponse.PaisEntity> paises) {
        if (isVisible()) {
            refreshButton.setVisibility(paises.isEmpty() ? View.VISIBLE : View.GONE);
            adapter.update(paises);
        }
    }

    @Override
    public void clickRetry() {
        presenter.getPaises();
    }

    @Override
    public void showError(String msg) {
        activity.showError(msg);
    }

    @Override
    public void clickPais(int pos) {
        PaisResponse.PaisEntity item = adapter.getItem(pos);
        if (item.getId() == PaisResponse.PaisEntity.MX || item.getId() == PaisResponse.PaisEntity.PE) {
            launchUsuario(item);
        } else {
            launchLoginUsuario(item);
        }
    }

    @Override
    public void launchUsuario(PaisResponse.PaisEntity paisModel) {
        if (isVisible()) {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_login, LoginTipoFragment.get(paisModel))
                    .addToBackStack(null)
                    .hide(this)
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        }
    }

    @Override
    public void launchLoginUsuario(PaisResponse.PaisEntity paisModel) {
        if (isVisible()) {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_login, LoginUsuarioFragment.get(paisModel))
                    .addToBackStack(null)
                    .hide(this)
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        }
    }

    @Override
    public int getH() {
        return activity.getWindow().getDecorView().getHeight();
    }

    @Override
    public int getW() {
        return activity.getWindow().getDecorView().getWidth();
    }
}
