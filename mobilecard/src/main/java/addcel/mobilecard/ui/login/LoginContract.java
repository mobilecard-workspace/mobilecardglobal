package addcel.mobilecard.ui.login;

/**
 * ADDCEL on 29/06/18.
 */
public interface LoginContract {

    enum UseCase {
        PAIS, TIPO, INICIO, USUARIO, NEGOCIO
    }

    interface View {

        void launchScreen(UseCase useCase);

        void launchPaisScreen();

        void launchTipoScreen();

        void launchInicioScreen();

        void launchUsuarioScreen();

        void launchNegocioScreen();

    }

    interface Presenter {
    }
}
