package addcel.mobilecard.ui.login.negocio

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.data.net.usuarios.model.LoginRequest
import addcel.mobilecard.ui.login.tipo.LoginTipoContract
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.os.Build
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 05/09/18.
 */
class LoginNegocioPresenterImpl(
        private val service: NegocioAPI,
        private val state: StateSession,
        private val session: SessionOperations,
        private val pais: PaisResponse.PaisEntity,
        private val compositeDisposable: CompositeDisposable,
        private val view: LoginNegocioContract.View
) : LoginNegocioPresenter {

    override fun clearDisposable() {
        compositeDisposable.clear()
    }

    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun fetchPais(): PaisResponse.PaisEntity {
        return pais
    }


    override fun resendActivation(negocio: NegocioEntity) {
        view.showProgress()
        compositeDisposable.add(
                service.resendActivationLink(
                        BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                        negocio.idEstablecimiento.toLong()
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            view.hideProgress()
                            view.onEmailResend(it)
                        }, {
                            view.hideProgress()
                            view.showError(
                                    ErrorUtil.getFormattedHttpErrorMsg(it)
                            )
                        })
        )
    }

    override fun login() {
        view.showProgress()
        session.logout()

        val request = LoginRequest.Builder().setImei(view.imei)
                .setManufacturer(Build.MANUFACTURER)
                .setOs(Build.VERSION.SDK_INT.toString())
                .setPlatform("ANDROID")
                .setTipoUsuario(LoginTipoContract.Tipo.NEGOCIO)
                .setUsrLoginOrEmail(view.email)
                .setUsrPwd(StringUtil.sha512(view.password))
                .build()

        compositeDisposable.add(
                service.login(
                        BuildConfig.ADDCEL_APP_ID, pais.id, StringUtil.getCurrentLanguage(),
                        request
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            view.hideProgress()
                            if (it.idError == 0) {
                                storeNegocio(it)
                                evalLogin(it)
                            } else {
                                view.showError(it.mensajeError)
                            }
                        }, {
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })
        )
    }

    override fun resetPassword(email: String) {
        view.showProgress()
        compositeDisposable.add(
                service.resetPasswordAdmin(
                        BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                        email
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            view.hideProgress()
                            if (it.idError == 0) {
                                view.showSuccess(it.mensajeError)
                                view.hideResetDialog()
                            } else {
                                view.showError(it.mensajeError)
                            }
                        }, {
                            view.hideProgress()
                            view.showError(
                                    ErrorUtil.getFormattedHttpErrorMsg(it)
                            )
                        })
        )
    }

    override fun logout() {
        session.logout()
    }


    private fun storeNegocio(negocioEntity: NegocioEntity) {
        session.negocio = negocioEntity
        session.isNegocioLogged = java.lang.Boolean.TRUE
    }

    private fun evalLogin(negocio: NegocioEntity) {
        when (negocio.idUsrStatus) {
            NegocioEntity.STATUS_RESET -> view.launchPassword(negocio)
            NegocioEntity.STATUS_ACTIVE -> evalNegocioActive(negocio)
            NegocioEntity.STATUS_PWD -> evalNegocioPwd(negocio)
            NegocioEntity.STATUS_SMS -> view.launchSms(negocio)
            else -> view.showError(negocio.mensajeError)
        }
    }

    private fun evalNegocioActive(negocio: NegocioEntity) {
        view.clearSignInValues()
        view.showSuccess(negocio.mensajeError)
        view.launchMenu(negocio.idPais)
    }

    private fun evalNegocioPwd(negocio: NegocioEntity) {
        if (negocio.idPais == PaisResponse.PaisEntity.PE) {
            view.showResendDialog(negocio)
        } else {
            view.showSuccess(negocio.mensajeError)
            view.launchMenu(negocio.idPais)
        }
    }
}
