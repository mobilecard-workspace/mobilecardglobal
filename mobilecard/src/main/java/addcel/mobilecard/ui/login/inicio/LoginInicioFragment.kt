package addcel.mobilecard.ui.login.inicio

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.ui.login.negocio.LoginNegocioFragment
import addcel.mobilecard.ui.login.tipo.LoginTipoContract
import addcel.mobilecard.ui.login.usuario.LoginUsuarioFragment
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroActivity
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroModel
import addcel.mobilecard.utils.BundleBuilder
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_inicio_mx.*
import timber.log.Timber

/**
 * ADDCEL on 2019-06-21.
 */
class LoginInicioFragment : Fragment() {
    companion object {
        fun get(
                pais: PaisResponse.PaisEntity,
                perfil: LoginTipoContract.Tipo?
        ): LoginInicioFragment {
            Timber.d("Pais login: %d", pais.id)
            val args = BundleBuilder().putParcelable("pais", pais)
            if (perfil != null) args.putSerializable("perfil", perfil)
            val frag = LoginInicioFragment()
            frag.arguments = args.build()
            return frag
        }
    }

    private lateinit var dots: Array<ImageView?>
    private lateinit var pais: PaisResponse.PaisEntity
    private lateinit var perfil: LoginTipoContract.Tipo
    private lateinit var adapter: LoginInicioTabAdapter

    private val presenter: LoginInicioPresenter = LoginInicioPresenterImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pais = arguments?.getParcelable("pais")!!
        perfil = arguments?.getSerializable("perfil")!! as LoginTipoContract.Tipo
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_inicio_mx, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val views = presenter.getImgArray(pais.id, perfil)
        dots = arrayOfNulls(views.size)
        adapter = LoginInicioTabAdapter(views)

        pager_mx_inicio_tutorial.adapter = adapter
        pager_mx_inicio_tutorial.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                addBottomDots(
                        view.context,
                        position
                ) // changing the next button text 'NEXT' / 'GOT IT'
            }
        })
        b_mx_inicio_registro.setOnClickListener { launchRegistro() }
        b_mx_inicio_login.setOnClickListener { launchLogin() }
        addBottomDots(view.context, 0)
    }

    private fun addBottomDots(
            context: Context,
            currentPage: Int
    ) { //val colorsActive = resources.obtainTypedArray(R.array.array_dot_inicio_active)
        //val colorsInactive = resources.obtainTypedArray(R.array.array_dot_inicio_inactive)

        layout_mx_inicio_dots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = ImageView(context)
            dots[i]?.setImageResource(
                    R.drawable.car_dot_w
            ) //dots[i]?.setImageResource(colorsInactive.getResourceId(currentPage, -1))
            layout_mx_inicio_dots.addView(dots[i])
        }

        //colorsInactive.recycle()

        if (dots.isNotEmpty()) { //dots[currentPage]?.setImageResource(colorsActive.getResourceId(currentPage, -1))
            dots[currentPage]?.setImageResource(R.drawable.car_dot_o) //colorsActive.recycle()
        }
    }

    private fun launchLogin() {
        if (perfil == LoginTipoContract.Tipo.NEGOCIO) {
            fragmentManager?.commit {
                add(R.id.frame_login, LoginNegocioFragment.get(pais))
                hide(this@LoginInicioFragment)
                addToBackStack(null)
            }
        } else {
            fragmentManager?.commit {
                add(R.id.frame_login, LoginUsuarioFragment.get(pais))
                hide(this@LoginInicioFragment)
                addToBackStack(null)
            }
        }
    }

    private fun launchRegistro() {
        if (perfil == LoginTipoContract.Tipo.NEGOCIO) {
            val model = SmsRegistroModel(pais, SmsRegistroActivity.USE_CASE_NEGOCIO)
            startActivity(SmsRegistroActivity.get(activity!!, model))
        } else {
            val model = SmsRegistroModel(pais, SmsRegistroActivity.USE_CASE_USUARIO)
            startActivity(SmsRegistroActivity.get(activity!!, model))
        }
        activity!!.finish()
    }
}