package addcel.mobilecard.ui.login.negocio;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;

/**
 * ADDCEL on 05/09/18.
 */
public interface LoginNegocioContract {
    interface View extends Validator.ValidationListener {

        void clearSignInValues();

        String getEmail();

        String getPassword();

        String getImei();

        void clickLogin();

        void clickReset();

        void clickRegistar();

        void launchPassword(NegocioEntity negocio);

        void launchEmail(NegocioEntity entity);

        void onEmailResend(DefaultResponse response);

        void launchSms(NegocioEntity entity);

        void launchMenu(int idPais);

        void showProgress();

        void hideProgress();

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);

        void showResendDialog(NegocioEntity entity);

        void showResetDialog();

        void hideResetDialog();

        void onResetResult(int resultCode, @Nullable Intent data);
    }
}
