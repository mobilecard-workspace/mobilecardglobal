package addcel.mobilecard.ui.login.negocio

import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import io.reactivex.disposables.Disposable

/**
 * ADDCEL on 2019-11-06.
 */


interface LoginNegocioPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposable()

    fun fetchPais(): PaisResponse.PaisEntity

    fun resendActivation(negocio: NegocioEntity)

    fun login()

    fun resetPassword(email: String)

    fun logout()
}