package addcel.mobilecard.ui.login.pais;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 20/08/18.
 */
@PerFragment
@Subcomponent(modules = LoginPaisModule.class)
public interface LoginPaisSubcomponent {
    void inject(LoginPaisFragment fragment);
}
