package addcel.mobilecard.ui.login.pais;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.utils.ListUtil;

/**
 * ADDCEL on 20/08/18.
 */
public final class LoginPaisAdapter extends RecyclerView.Adapter<LoginPaisAdapter.ViewHolder> {

    private final Picasso picasso;
    private final int targetWidth;
    private final List<PaisResponse.PaisEntity> paises;

    LoginPaisAdapter(Picasso picasso, int targetWidth, List<PaisResponse.PaisEntity> paises) {
        this.picasso = picasso;
        this.targetWidth = targetWidth;
        this.paises = paises;
    }

    @Override
    public LoginPaisAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LoginPaisAdapter.ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pais, parent, false));
    }

    @Override
    public void onBindViewHolder(LoginPaisAdapter.ViewHolder holder, int position) {
        PaisResponse.PaisEntity pais = paises.get(position);

        holder.nameText.setText(pais.getNombre());
        picasso.load(pais.getUrl())
                .placeholder(R.drawable.ic_pais)
                .error(R.drawable.ic_pais)
                .resize(targetWidth / 3, 0)
                .centerInside()
                .into(holder.flagImg);
    }

    @Override
    public int getItemCount() {
        return paises.size();
    }

    public void update(List<PaisResponse.PaisEntity> paises) {
        if (ListUtil.notEmpty(this.paises)) this.paises.clear();
        this.paises.addAll(paises);
        notifyDataSetChanged();
    }

    public PaisResponse.PaisEntity getItem(int pos) {
        return paises.get(pos);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView flagImg;
        private final TextView nameText;

        ViewHolder(View itemView) {
            super(itemView);
            flagImg = itemView.findViewById(R.id.img_pais_item_flag);
            nameText = itemView.findViewById(R.id.text_pais_item_name);
        }
    }
}
