package addcel.mobilecard.ui.login.negocio;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Preconditions;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Objects;

import addcel.mobilecard.data.local.shared.state.StateSession;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.login.LoginActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

/**
 * ADDCEL on 05/09/18.
 */
@Module
public final class LoginNegocioModule {
    private final LoginNegocioFragment fragment;

    LoginNegocioModule(LoginNegocioFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    LoginActivity provideActivity() {
        return (LoginActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    @Nullable
    Bundle getExtras() {
        return fragment.getArguments();
    }

    @PerFragment
    @Provides
    PaisResponse.PaisEntity providePaisModel(@Nullable Bundle bundle) {
        return bundle == null ? PaisResponse.PaisEntity.DEFAULT_VALUE
                : Preconditions.checkNotNull(bundle.getParcelable("paisModel"));
    }

    @PerFragment
    @Provides
    NegocioAPI provideApi(Retrofit retrofit) {
        return retrofit.create(NegocioAPI.class);
    }

    @PerFragment
    @Provides
    LoginNegocioPresenter providePresenter(NegocioAPI api,
                                           StateSession state, SessionOperations session, PaisResponse.PaisEntity pais) {
        return new LoginNegocioPresenterImpl(api, state, session, pais, new CompositeDisposable(), fragment);
    }
}
