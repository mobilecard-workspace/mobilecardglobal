package addcel.mobilecard.ui

/**
 * ADDCEL on 2020-02-13.
 */
interface PurchaseScreenView : ScreenView, Authenticable {
    fun isPurchaseButtonEnabled(): Boolean
    fun disablePurchaseButton()
    fun enablePurchaseButton()
    fun clickPurchaseButton()
}