package addcel.mobilecard.ui.launch

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity
import addcel.mobilecard.data.net.usuarios.model.UserValidation
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.domain.sms.SmsUseCase
import addcel.mobilecard.ui.keyboard.KeyboardService
import addcel.mobilecard.ui.login.LoginActivity
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroModel
import addcel.mobilecard.ui.negocio.mx.password.NegocioPasswordActivity
import addcel.mobilecard.ui.negocio.pe.menu.NegocioMenuPeActivity
import addcel.mobilecard.ui.sms.SmsActivity
import addcel.mobilecard.ui.usuario.main.MainActivity
import addcel.mobilecard.ui.usuario.password.PasswordActivity
import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Handler
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import timber.log.Timber
import javax.inject.Inject

class LaunchActivity : AppCompatActivity(), LaunchView {

    @Inject
    lateinit var presenter: LaunchPresenter
    @Inject
    lateinit var connectivityManager: ConnectivityManager
    @Inject
    lateinit var state: StateSession
    private lateinit var permissions: RxPermissions

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        Mobilecard.get().netComponent.splashSubcomponent(LaunchModule(this)).inject(this)

        state.setMenuLaunched(true)

        //LIMPIAMOS VALORES TRANSACCION TECLADO
        KeyboardService.RESULT = ""
        KeyboardService.COMPLETED = java.lang.Boolean.FALSE

        setContentView(R.layout.activity_launch)

        permissions = RxPermissions(this)

        val pDisp = permissions.request(Manifest.permission.ACCESS_NETWORK_STATE)
                .subscribe {
                    if (it) {
                        checkConnection(connectivityManager.activeNetworkInfo)
                    } else {
                        presenter.verify(java.lang.Boolean.FALSE)
                    }
                }


        presenter.addToDisposables(pDisp)

        //LottieAnimationView splash = findViewById(R.id.splash);

        /*splash.addAnimatorListener(new AnimatorListenerAdapter() {
          @Override public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            NetworkInfo connInfo = connectivityManager.getActiveNetworkInfo();
            if (connInfo != null && connInfo.isConnectedOrConnecting()) {
              presenter.verify(Boolean.TRUE);
            } else {
              Timber.w("No Network");
              presenter.verify(Boolean.FALSE);
            }
          }
        });*/
    }

    private fun checkConnection(info: NetworkInfo?) {
        Handler().postDelayed({
            if (info != null && info.isConnectedOrConnecting) {
                presenter.verify(java.lang.Boolean.TRUE)
            } else {
                Timber.w("No Network")
                presenter.verify(java.lang.Boolean.FALSE)
            }
        }, 2000)
    }

    public override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun goToLogin(isUsa: Boolean) {
        startActivity(LoginActivity.get(this))
    }

    override fun goToUsuarioSms(usuario: UserValidation) {
        startActivity(
                SmsActivity.get(
                        this,
                        SmsUseCase.U_SPLASH,
                        usuario.idUSuario,
                        usuario.idPais!!
                )
        )
    }

    override fun goToNegocioSms(negocio: ValidationEntity) {
        startActivity(SmsActivity.get(this, SmsUseCase.N_SPLASH, negocio.id.toLong(), 1))
    }

    override fun goToPassword(usuario: Usuario) {
        startActivity(PasswordActivity.get(this, usuario))
    }

    override fun goToNegocioPassword(entity: NegocioEntity) {
        startActivity(NegocioPasswordActivity.get(this, entity, false))
    }

    override fun goToMenu() {
        startActivity(MainActivity.get(this, false))
    }

    override fun goToNegocioMenu(pais: Int) {
        if (pais == PaisResponse.PaisEntity.PE) {
            startActivity(NegocioMenuPeActivity.get(this, true))
        } else {
            startActivity(
                    NegocioCobroActivity.get(
                            this,
                            NegocioCobroModel(NegocioCobroActivity.USE_CASE_MENU)
                    ).addFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP
                    )
            )
        }
    }
}
