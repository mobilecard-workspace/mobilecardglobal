package addcel.mobilecard.ui.launch

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.usuarios.UsuariosService
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.domain.launch.SplashInteractor
import addcel.mobilecard.domain.launch.SplashInteractorImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * ADDCEL on 18/08/17.
 */

@Module
class LaunchModule(private val activity: LaunchActivity) {

    @PerActivity
    @Provides
    fun provideApi(retrofit: Retrofit): NegocioAPI {
        return retrofit.create(NegocioAPI::class.java)
    }

    @PerActivity
    @Provides
    fun provideInteractor(
            service: UsuariosService,
            api: NegocioAPI, session: SessionOperations
    ): SplashInteractor {
        return SplashInteractorImpl(service, api, session)
    }

    @PerActivity
    @Provides
    fun providePresenter(interactor: SplashInteractor): LaunchPresenter {
        return LaunchPresenterImpl(interactor, activity)
    }
}
