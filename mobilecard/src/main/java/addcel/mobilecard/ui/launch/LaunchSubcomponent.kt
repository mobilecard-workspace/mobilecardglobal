package addcel.mobilecard.ui.launch

import addcel.mobilecard.di.scope.PerActivity
import dagger.Subcomponent

/**
 * ADDCEL on 18/08/17.
 */

@PerActivity
@Subcomponent(modules = [LaunchModule::class])
interface LaunchSubcomponent {
    fun inject(activity: LaunchActivity)
}
