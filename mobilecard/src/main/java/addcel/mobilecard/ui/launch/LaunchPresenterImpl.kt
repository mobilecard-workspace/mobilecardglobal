package addcel.mobilecard.ui.launch

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity
import addcel.mobilecard.data.net.usuarios.model.McResponse
import addcel.mobilecard.data.net.usuarios.model.UserValidation
import addcel.mobilecard.data.net.usuarios.model.Usuario
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.launch.SplashInteractor
import addcel.mobilecard.domain.login.UsuarioStatus
import addcel.mobilecard.utils.StringUtil
import io.reactivex.disposables.Disposable

/** ADDCEL on 18/08/17.  */
internal class LaunchPresenterImpl(
        private val interactor: SplashInteractor,
        private val view: LaunchView
) : LaunchPresenter {

    companion object {
        fun userToValidationEntity(usuario: Usuario): UserValidation {
            return UserValidation.Builder()
                    .setIdError(usuario.idError)
                    .setIdPais(usuario.idPais)
                    .setIdUsrStatus(usuario.idUsrStatus)
                    .setIdUSuario(usuario.ideUsuario)
                    .setMensajeError(usuario.mensajeError)
                    .setScanReference(usuario.scanReference)
                    .setUsrApellido(usuario.usrApellido)
                    .setUsrJumio(usuario.usrJumio)
                    .setUsrNombre(usuario.usrNombre).build()

        }

        fun negocioToValidationEntity(negocio: NegocioEntity): ValidationEntity {
            return ValidationEntity.Builder()
                    .setIdError(negocio.idError)
                    .setIdPais(negocio.idPais)
                    .setEstatus(negocio.idUsrStatus)
                    .setId(negocio.idEstablecimiento)
                    .setIdError(negocio.idError)
                    .setJumioReference(negocio.scanReference)
                    .setJumioStatus(negocio.jumioStatus)
                    .setMensajeError(negocio.mensajeError)
                    .setTelefonoContacto(negocio.telefono)
                    .build()

        }
    }


    override fun addToDisposables(disposable: Disposable) {
        interactor.addToDisposable(disposable)
    }

    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun verify(onLine: Boolean) {
        val usuario = interactor.usuario
        val negocio = interactor.negocio
        if (usuario == null && negocio == null) {
            view.goToLogin(false)
        } else if (usuario != null) {
            if (onLine)
                onVerifyUser(onLine)
            else
                verifyUsuarioOffLine(usuario)
        } else {
            if (onLine)
                onVerifyNegocio(onLine)
            else
                verifyNegocioOffLine(negocio!!)
        }
    }

    override fun updateReference(userValidation: UserValidation, reference: String) {
        interactor.updateReference(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                userValidation.idPais!!, "", reference, object : InteractorCallback<McResponse> {
            override fun onSuccess(result: McResponse) {
                view.goToLogin(false)
            }

            override fun onError(error: String) {
                view.goToLogin(false)
            }
        })
    }

    private fun onVerifyUser(online: Boolean) {
        interactor.verifyUser(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), online,
                object : InteractorCallback<UserValidation> {
                    override fun onSuccess(result: UserValidation) {
                        when (result.idUsrStatus) {
                            UsuarioStatus.ACTIVO, UsuarioStatus.EMAIL_VERIFICATION -> view.goToMenu()
                            UsuarioStatus.SMS_VERIFICATION -> view.goToUsuarioSms(result)
                            else -> view.goToLogin(false)
                        }
                    }

                    override fun onError(error: String) {
                        view.goToLogin(false)
                    }
                })
    }

    private fun onVerifyNegocio(onLine: Boolean) {
        interactor.verifyNegocio(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), onLine,
                object : InteractorCallback<ValidationEntity> {
                    override fun onSuccess(result: ValidationEntity) {
                        when (result.estatus) {
                            UsuarioStatus.ACTIVO, UsuarioStatus.EMAIL_VERIFICATION -> view.goToNegocioMenu(
                                    result.idPais
                            )
                            UsuarioStatus.SMS_VERIFICATION -> view.goToNegocioSms(result)
                            else -> view.goToLogin(false)
                        }
                    }

                    override fun onError(error: String) {
                        view.goToLogin(false)
                    }
                })
    }

    private fun verifyUsuarioOffLine(usuario: Usuario) {
        when (usuario.idUsrStatus) {
            UsuarioStatus.ACTIVO, UsuarioStatus.EMAIL_VERIFICATION -> view.goToMenu()
            UsuarioStatus.SMS_VERIFICATION -> view.goToUsuarioSms(userToValidationEntity(usuario))
            else -> view.goToLogin(false)
        }
    }

    private fun verifyNegocioOffLine(negocio: NegocioEntity) {
        when (negocio.idUsrStatus) {
            UsuarioStatus.ACTIVO, UsuarioStatus.EMAIL_VERIFICATION -> view.goToNegocioMenu(
                    negocio.idPais
            )
            UsuarioStatus.SMS_VERIFICATION -> view.goToNegocioSms(negocioToValidationEntity(negocio))
            else -> view.goToLogin(false)
        }
    }
}
