package addcel.mobilecard.ui.launch

import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity
import addcel.mobilecard.data.net.usuarios.model.UserValidation
import addcel.mobilecard.data.net.usuarios.model.Usuario
import io.reactivex.disposables.Disposable

/**
 * ADDCEL on 18/08/17.
 */

interface LaunchView {

    fun goToLogin(isUsa: Boolean)

    fun goToUsuarioSms(usuario: UserValidation)

    fun goToNegocioSms(negocio: ValidationEntity)

    fun goToPassword(usuario: Usuario)

    fun goToNegocioPassword(entity: NegocioEntity)

    fun goToMenu()

    fun goToNegocioMenu(pais: Int)
}

interface LaunchPresenter {

    fun addToDisposables(disposable: Disposable)

    fun clearDisposables()

    fun verify(onLine: Boolean)

    fun updateReference(userValidation: UserValidation, reference: String)
}

