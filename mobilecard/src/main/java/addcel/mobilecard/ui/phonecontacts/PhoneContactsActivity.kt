package addcel.mobilecard.ui.phonecontacts

import addcel.mobilecard.R
import addcel.mobilecard.data.net.usuarios.model.MobilecardContact
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.utils.StringUtil
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.common.base.Strings
import com.google.common.collect.ImmutableSet
import com.google.common.collect.Lists
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_device_contacts.*

interface PhoneContactsView {
    fun setContacts(contacts: List<MobilecardContact>)
    fun clickContact(contact: MobilecardContact)
}

class PhoneContactsAdapter : RecyclerView.Adapter<PhoneContactsAdapter.ViewHolder>() {

    private val contacts: MutableList<MobilecardContact> = ArrayList()

    fun update(uContacts: List<MobilecardContact>) {
        if (contacts.isNotEmpty()) {
            contacts.clear()
        }
        contacts.addAll(uContacts)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        android.R.layout.simple_selectable_list_item,
                        parent, false
                )
        )
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    fun getItem(pos: Int): MobilecardContact {
        return contacts[pos]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contacts[position]
        holder.text.text = contact.nombre
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val text: TextView = view.findViewById(android.R.id.text1)
    }
}

class PhoneContactsActivity : AppCompatActivity(), PhoneContactsView {

    companion object {
        const val REQUEST_CODE = 426
        const val DATA_CONTACT = "contacto"

        private const val SELECTION_QUERY =
                ContactsContract.Contacts.IN_VISIBLE_GROUP + " = 1" + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER + " = 1"
        private const val SORT_ORDER =
                ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC"

        fun get(context: Context): Intent {
            return Intent(context, PhoneContactsActivity::class.java)
        }
    }

    private val disposables = CompositeDisposable()
    val adapter = PhoneContactsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_contacts)
        recycler_contacts.adapter = adapter
        recycler_contacts.layoutManager = LinearLayoutManager(this)
        recycler_contacts.addItemDecoration(
                DividerItemDecoration(
                        this,
                        DividerItemDecoration.VERTICAL
                )
        )
        ItemClickSupport.addTo(recycler_contacts)
                .setOnItemClickListener { _, position, _ -> clickContact(adapter.getItem(position)) }

        getContacts()
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    override fun setContacts(contacts: List<MobilecardContact>) {
        adapter.update(contacts)
    }

    override fun clickContact(contact: MobilecardContact) {
        val intent = Intent().putExtra(DATA_CONTACT, contact)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    //PRESENTER
    private fun getContacts() {
        fetchContacts(object : InteractorCallback<List<MobilecardContact>> {
            override fun onSuccess(result: List<MobilecardContact>) {
                setContacts(result)
            }

            override fun onError(error: String) {
                setContacts(Lists.newArrayList<MobilecardContact>())
            }
        })
    }

    //INTERACTOR
    private fun getContactCursor(): Cursor? = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, SELECTION_QUERY, null, SORT_ORDER
    )

    fun clearDisposables() {
        disposables.clear()
    }

    private fun fetchContacts(callback: InteractorCallback<List<MobilecardContact>>) {
        val cursor: Cursor? = getContactCursor()

        if (cursor != null) {
            disposables.add(contactsObservable(cursor).subscribeOn(Schedulers.io()).observeOn(
                    AndroidSchedulers.mainThread()
            ).map { ImmutableSet.copyOf(it).asList() }.subscribe(
                    { callback.onSuccess(it) }, {
                callback.onError(Strings.nullToEmpty(StringUtil.getNetworkError()))
            })
            )
        } else {
            callback.onError(Strings.nullToEmpty("Contacts not available"))
        }
    }

    private fun contactsObservable(cursor: Cursor): Observable<List<MobilecardContact>> {
        return Observable.fromCallable { fetchContacts(cursor) }
    }

    private fun fetchContacts(cursor: Cursor): List<MobilecardContact> {
        val contacts = Lists.newArrayList<MobilecardContact>()
        while (cursor.moveToNext()) {
            val contactName = cursor.getString(
                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
            )
            val phNumber = cursor.getString(
                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
            )
            if (!TextUtils.isEmpty(phNumber)) {
                contacts.add(MobilecardContact(contactName, phNumber))
            }
        }
        cursor.close()
        return contacts
    }
}
