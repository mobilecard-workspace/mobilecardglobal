package addcel.mobilecard.ui.password.reset

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_reset_password.*
import javax.inject.Inject

@Parcelize
data class ResetPasswordModel(val useCase: Int, val idPais: Int) : Parcelable

interface ResetPasswordView : ScreenView, TextWatcher {
    fun clickReset()

    fun onReset(code: Int, message: String)
}

class ResetPasswordActivity : AppCompatActivity(), ResetPasswordView {

    companion object {
        const val REQUEST_CODE = 417
        const val USE_CASE_USUARIO = 1
        const val USE_CASE_NEGOCIO = 2
        const val DATA_MODEL = "model"
        const val DATA_MESSAGE = "message"

        fun get(context: Context, resetPasswordModel: ResetPasswordModel): Intent {
            return Intent(context, ResetPasswordActivity::class.java).putExtras(
                    BundleBuilder().putParcelable(DATA_MODEL, resetPasswordModel).build()
            )
        }
    }

    @Inject
    lateinit var presenter: ResetPasswordPresenter
    @Inject
    lateinit var model: ResetPasswordModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        setContentView(R.layout.activity_reset_password)
        Mobilecard.get().netComponent.resetPasswordSubcomponent(ResetPasswordModule(this))
                .inject(this)

        AndroidUtils.getEditText(til_reset_password_email).addTextChangedListener(this)
        b_reset_password.setOnClickListener { clickReset() }
        b_reset_password.isEnabled = false
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun clickReset() {
        val email = AndroidUtils.getText(til_reset_password_email.editText)
        presenter.reset(email, model.idPais)
    }

    override fun onReset(code: Int, message: String) {
        val intent = Intent().putExtra(DATA_MESSAGE, message)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun showProgress() {
        progress_reset_password.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_reset_password.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (Patterns.EMAIL_ADDRESS.matcher(p0).matches()) {
                til_reset_password_email.error = null
                b_reset_password.isEnabled = true
            } else {
                til_reset_password_email.error = getText(R.string.error_email)
                b_reset_password.isEnabled = false
            }
        } else {
            b_reset_password.isEnabled = false
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}
