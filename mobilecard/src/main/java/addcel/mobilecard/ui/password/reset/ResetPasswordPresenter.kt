package addcel.mobilecard.ui.password.reset

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.usuarios.model.McResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.password.reset.ResetPasswordInteractor
import addcel.mobilecard.utils.StringUtil

/**
 * ADDCEL on 2019-07-19.
 */
interface ResetPasswordPresenter {

    fun clearDisposables()

    fun reset(email: String, idPais: Int)
}

class ResetPasswordPresenterImpl(
        val interactor: ResetPasswordInteractor,
        val view: ResetPasswordView
) : ResetPasswordPresenter {
    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun reset(email: String, idPais: Int) {
        view.showProgress()
        interactor.reset(BuildConfig.ADDCEL_APP_ID, idPais, StringUtil.getCurrentLanguage(), email,
                object : InteractorCallback<McResponse> {
                    override fun onSuccess(result: McResponse) {
                        view.hideProgress()
                        if (result.idError == 0) {
                            view.onReset(result.idError, result.mensajeError)
                        } else {
                            view.showError(result.mensajeError)
                        }
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }
}