package addcel.mobilecard.ui.password.reset

import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.usuarios.UsuariosService
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.domain.password.reset.ResetPasswordInteractor
import addcel.mobilecard.domain.password.reset.ResetPasswordNegocioInteractorImpl
import addcel.mobilecard.domain.password.reset.ResetPasswordUsuarioInteractorImpl
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-07-19.
 */

@PerActivity
@Subcomponent(modules = [ResetPasswordModule::class])
interface ResetPasswordSubcomponent {
    fun inject(activity: ResetPasswordActivity)
}

@Module
class ResetPasswordModule(val activity: ResetPasswordActivity) {

    @PerActivity
    @Provides
    fun provideModel(): ResetPasswordModel {
        return activity.intent.extras?.getParcelable(ResetPasswordActivity.DATA_MODEL)!!
    }

    @PerActivity
    @Provides
    fun provideNegocioApi(r: Retrofit): NegocioAPI {
        return r.create(NegocioAPI::class.java)
    }

    @PerActivity
    @Provides
    fun provideInteractor(
            userApi: UsuariosService, negocioAPI: NegocioAPI,
            model: ResetPasswordModel
    ): ResetPasswordInteractor {
        return if (model.useCase == ResetPasswordActivity.USE_CASE_NEGOCIO) {
            ResetPasswordNegocioInteractorImpl(negocioAPI, CompositeDisposable())
        } else {
            ResetPasswordUsuarioInteractorImpl(userApi, CompositeDisposable())
        }
    }

    @PerActivity
    @Provides
    fun providePresenter(
            interactor: ResetPasswordInteractor
    ): ResetPasswordPresenter {
        return ResetPasswordPresenterImpl(interactor, activity)
    }
}