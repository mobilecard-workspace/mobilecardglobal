package addcel.mobilecard.ui.password.update

import addcel.mobilecard.R
import addcel.mobilecard.data.net.usuarios.model.McResponse
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_password.*

interface UpdatePasswordView : ScreenView, TextWatcher {

    fun enableUpdateButton()

    fun clickUpdate(id: Long, oldPass: String, newPass: String)

    fun onUpdate(newPass: String, result: McResponse)
}

@Parcelize
data class UpdatePasswordModel(val id: Long, val useCase: Int) : Parcelable

class UpdatePasswordActivity : AppCompatActivity(), UpdatePasswordView {
    companion object {
        const val REQUEST_CODE = 418
        const val USE_CASE_USUARIO = 1
        const val USE_CASE_NEGOCIO = 2
        const val DATA_MODEL = "model"
        const val DATA_MESSAGE = "message"
        const val DATA_NEW_PASS = "newPass"

        fun get(context: Context, updatePasswordModel: UpdatePasswordModel): Intent {
            return Intent(context, UpdatePasswordActivity::class.java).putExtras(
                    BundleBuilder().putParcelable(DATA_MODEL, updatePasswordModel).build()
            )
        }
    }

    lateinit var model: UpdatePasswordModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        setContentView(R.layout.activity_password)

        AndroidUtils.getEditText(til_password_old).addTextChangedListener(this)
        AndroidUtils.getEditText(til_password_new).addTextChangedListener(this)
        AndroidUtils.getEditText(til_password_conf).addTextChangedListener(this)

        b_password_actualizar.setOnClickListener {
            clickUpdate(
                    model.id, AndroidUtils.getText(til_password_old.editText),
                    AndroidUtils.getText(til_password_new.editText)
            )
        }

        b_password_actualizar.isEnabled = false
    }

    override fun clickUpdate(id: Long, oldPass: String, newPass: String) {
    }

    override fun onUpdate(newPass: String, result: McResponse) {
        val intent =
                Intent().putExtra(DATA_MESSAGE, result.mensajeError).putExtra(DATA_NEW_PASS, newPass)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun showProgress() {
        progress_password.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_password.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun enableUpdateButton() {
        b_password_actualizar.isEnabled =
                til_password_old.error == null && til_password_new.error == null && til_password_conf.error == null
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (til_password_old.hasFocus()) {
                if (p0.isEmpty() || p0.length < 6) {
                    til_password_old.error = getString(R.string.error_password)
                } else {
                    til_password_old.error = null
                }
            } else if (til_password_new.hasFocus()) {
                if (p0.isEmpty() || p0.length < 6) {
                    til_password_new.error = getString(R.string.error_password)
                } else {
                    til_password_new.error = null
                }
            } else if (til_password_conf.hasFocus()) {
                if (p0.isEmpty() || (p0.toString() != AndroidUtils.getText(til_password_new.editText))) {
                    til_password_conf.error = getString(R.string.error_conf_password)
                } else {
                    til_password_conf.error = null
                }
            }
            enableUpdateButton()
        } else {
            b_password_actualizar.isEnabled = false
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}
