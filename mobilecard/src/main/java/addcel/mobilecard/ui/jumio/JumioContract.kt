package addcel.mobilecard.ui.jumio

import addcel.mobilecard.McConstants
import android.app.Activity
import com.google.common.base.Strings

/*import com.jumio.MobileSDK
import com.jumio.auth.AuthenticationDeallocationCallback
import com.jumio.core.enums.JumioDataCenter
import com.jumio.core.exceptions.MissingPermissionException
import com.jumio.core.exceptions.PlatformNotSupportedException
import com.jumio.nv.NetverifyDeallocationCallback*/

/**
 * ADDCEL on 2019-06-29.
 */

/*data class JumioCredentials(
        val token: String = "6623acf1-641c-43af-b62a-44ab4eb878ad",
        val secret: String = "gYjTrv8IMH2UaUIw5b0mSXFKubLSkvXj",
        val dataCenter: JumioDataCenter = JumioDataCenter.US
) {
    companion object {
        fun get(): JumioCredentials {
            return JumioCredentials()
        }
    }
}*/

class JumioUtil {
    companion object {


        fun isUserEnrolled(status: String?): Boolean {
            return McConstants.JUMIO_STATUS_ENROLLED == Strings.nullToEmpty(status)
        }

        fun checkPermissions(activity: Activity, requestCode: Int): Boolean {
            /*return if (!MobileSDK.hasAllRequiredPermissions(activity)) { // Acquire missing permissions.
                val mp = MobileSDK.getMissingPermissions(activity)

                ActivityCompat.requestPermissions(
                        activity, mp,
                        requestCode
                ) // The result is received in onRequestPermissionsResult.
                false
            } else {
                true
            }*/
            return true
        }

        fun getUsuarioReferenceWithPrefix(idUsuario: Long): String {
            return "U-$idUsuario"
        }

    }
}

interface Onboardable {
    // :NetverifyDeallocationCallback

    companion object {
        const val REQUEST_CODE = 303
    }

    /*@Throws(PlatformNotSupportedException::class, NullPointerException::class)
    fun initSdk(
            act: Activity, token: String, secret: String, dCenter: JumioDataCenter, reference: String
    )

    @Throws(MissingPermissionException::class)
    fun launchOnboarding(reference: String)

    fun onOnboardingResult(resultCode: Int, data: Intent?)*/
}

interface Authenticable {
    //: AuthenticationDeallocationCallback {

    companion object {
        const val REQUEST_CODE = 304
    }

    fun onWhiteList()

    fun notOnWhiteList()

    /*@Throws(
            PlatformNotSupportedException::class, NullPointerException::class,
            MissingPermissionException::class, IllegalArgumentException::class
    )
    fun launchAuthentication(
            activity: Activity, token: String, secret: String, dataCenter: JumioDataCenter
    )

    fun onAuthenticationResult(resultCode: Int, data: Intent?)

    fun showAuthenticationErrorDialog()

     */
}