package addcel.mobilecard.ui.jumio

import addcel.mobilecard.R
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_jumio_message.*

class JumioMessageActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE = 310
        const val DATA_REFERENCE = "reference"

        fun get(context: Context, reference: Long): Intent {
            return Intent(context, JumioMessageActivity::class.java).putExtra(
                    DATA_REFERENCE,
                    reference
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jumio_message)
        img_jumio_msg.setOnClickListener {
            Handler().postDelayed({
                setResult(Activity.RESULT_OK, intent)
                finish()
            }, 200)
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}