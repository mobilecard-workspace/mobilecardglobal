package addcel.mobilecard.ui.custom.extension;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.ViewTreeObserver;
import android.view.Window;

/**
 * ADDCEL on 07/02/18.
 */
public final class DetachableClickListener implements DialogInterface.OnClickListener {

    private DialogInterface.OnClickListener delegateOrNull;

    private DetachableClickListener(DialogInterface.OnClickListener delegateOrNull) {
        this.delegateOrNull = delegateOrNull;
    }

    public static DetachableClickListener wrap(DialogInterface.OnClickListener delegate) {
        return new DetachableClickListener(delegate);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (delegateOrNull != null) {
            delegateOrNull.onClick(dialogInterface, i);
        }
    }

    public void clearOnDetach(Dialog dialog) {
        Window window = dialog.getWindow();
        if (window != null) {
            window.getDecorView()
                    .getViewTreeObserver()
                    .addOnWindowAttachListener(new ViewTreeObserver.OnWindowAttachListener() {
                        @Override
                        public void onWindowAttached() {
                        }

                        @Override
                        public void onWindowDetached() {
                            delegateOrNull = null;
                        }
                    });
        }
    }
}
