package addcel.mobilecard.ui.custom.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import addcel.mobilecard.R;

/**
 * ADDCEL on 07/12/16.
 */
public final class ClickableImageText extends RelativeLayout {

    private ImageView buttonImage;
    private TextView buttonText;

    public ClickableImageText(Context context) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_clickable_image_text, this);
        loadViews();
    }

    public ClickableImageText(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_clickable_image_text, this);
        loadViews();
    }

    private void loadViews() {
        buttonImage = findViewById(R.id.img_button);
        buttonText = findViewById(R.id.text_button);
    }

    private void setButtonImage(int drawable) {
        buttonImage.setImageDrawable(ContextCompat.getDrawable(getContext(), drawable));
    }

    private void setButtonText(int text) {
        buttonText.setText(text);
    }

    public void loadButtonData(int drawableId, int textId) {
        setButtonImage(drawableId);
        setButtonText(textId);
    }
}
