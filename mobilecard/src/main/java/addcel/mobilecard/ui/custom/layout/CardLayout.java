package addcel.mobilecard.ui.custom.layout;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.FranquiciaEntity;
import addcel.mobilecard.utils.StringUtil;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 18/11/16.
 */
public final class CardLayout extends LinearLayout {

    private static final int[] CARD_ICONS = {
            R.drawable.logo_visa, R.drawable.logo_master, R.drawable.logo_american,
    };
    private static final String[] CARD_TYPES = {
            "Visa", "MasterCard", "American Express",
    };

    private CardEntity model;
    private TextView typeView;
    private TextView numberView;

    public CardLayout(Context context) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.layout_card, this);
        loadViews();
    }

    public CardLayout(Context context, CardEntity card) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.layout_card, this);
        loadViews();
        setCard(card);
    }

    public CardLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.layout_card, this);
        loadViews();
    }

    private void loadViews() {
        typeView = findViewById(R.id.text_card_type);
        numberView = findViewById(R.id.text_card_number);
    }

    public CardEntity getModel() {
        return model;
    }

    private void setCard(CardEntity model) {
        this.model = model;
        setType(model.getTipo());
        setPan(StringUtil.maskCard(AddcelCrypto.decryptHard(model.getPan())));
    }

    private void setType(FranquiciaEntity franquicia) {
        typeView.setTextColor(Color.GRAY);
        float dimen = getResources().getDimension(R.dimen.widget_vertical_margin);
        if (franquicia.equals(FranquiciaEntity.VISA)) {
            typeView.setText(CARD_TYPES[0]);
            typeView.setCompoundDrawablesRelativeWithIntrinsicBounds(CARD_ICONS[0], 0, 0, 0);
            typeView.setCompoundDrawablePadding((int) dimen);
        }
        if (franquicia.equals(FranquiciaEntity.MasterCard)) {
            typeView.setText(CARD_TYPES[1]);
            typeView.setCompoundDrawablesRelativeWithIntrinsicBounds(CARD_ICONS[1], 0, 0, 0);
            typeView.setCompoundDrawablePadding((int) dimen);
        }
        if (franquicia.equals(FranquiciaEntity.Amex) || franquicia.equals(FranquiciaEntity.AmEx)) {
            typeView.setText(CARD_TYPES[2]);
            typeView.setCompoundDrawablesRelativeWithIntrinsicBounds(CARD_ICONS[2], 0, 0, 0);
            typeView.setCompoundDrawablePadding((int) dimen);
        }
    }

    private void setPan(String card) {
        // numberView.setText(maskPan(card));
        numberView.setText(card);
    }
}
