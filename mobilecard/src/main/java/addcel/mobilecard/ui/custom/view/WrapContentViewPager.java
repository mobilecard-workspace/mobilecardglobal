package addcel.mobilecard.ui.custom.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

/**
 * ADDCEL on 2019-06-06.
 */
public final class WrapContentViewPager extends ViewPager {

    public int height;

    public WrapContentViewPager(Context context) {
        super(context);
    }

    public WrapContentViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WrapContentViewPager(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        for (int i = 0; i < this.getChildCount(); ++i) {
            View child = this.getChildAt(i);
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight();
            if (h > this.height) {
                this.height = h;
            }
        }

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(this.height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
