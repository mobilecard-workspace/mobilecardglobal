package addcel.mobilecard.ui.scanner

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.R
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_new_scanner.*
import timber.log.Timber

class NewScannerActivity : AppCompatActivity() {


    companion object {
        const val REQUEST_CODE = 434
        const val RESULT_DATA = "result"

        fun get(context: Context): Intent {
            return Intent(context, NewScannerActivity::class.java)
        }

        fun startForResult(activity: Activity) {
            Timber.d("llamando startForResult - Activity")
            val intent = Intent(activity, NewScannerActivity::class.java)
            activity.startActivityForResult(intent, REQUEST_CODE)
        }

        fun startForResult(fragment: Fragment) {
            Timber.d("llamando startForResult - Fragment")
            val intent = Intent(fragment.context, NewScannerActivity::class.java)
            fragment.startActivityForResult(intent, REQUEST_CODE)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_scanner)


        lifecycle.addObserver(view_barcode)


        view_barcode.barcodes.observe(this, Observer {
            if (BuildConfig.DEBUG) Toasty.info(this@NewScannerActivity, it.toString()).show()

            Timber.d("Resultado scan: %s", it.toString())
            Timber.d("Resultado toSTring: %s", it[0].toString())
            Timber.d("Resultado raw: %s", it[0].rawValue)

            val intent = Intent().putExtra(RESULT_DATA, it[0].rawValue)

            setResult(Activity.RESULT_OK, intent)
            finish()
        })
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}
