package addcel.mobilecard.ui;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import org.jetbrains.annotations.Nullable;

import io.reactivex.disposables.Disposable;

/**
 * ADDCEL on 3/13/19.
 */
public interface ContainerScreenView extends ScreenView {
    int STATUS_NORMAL = 0;
    int STATUS_BLOCKED = 1;
    int STATUS_FINISHED = 2;

    void pressBack();

    int getScreenStatus();

    void setStreenStatus(int status);

    void setAppToolbarTitle(int title);

    void setAppToolbarTitle(@NonNull String title);

    void showRetry();

    void hideRetry();

    @Nullable View getRetry();

    FragmentManager getFragmentManagerLazy();

    void addToDisposables(@NonNull Disposable disposable);
}
