package addcel.mobilecard.ui.notification

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_result_notification.*
import java.util.concurrent.TimeUnit

@Parcelize
data class ResultNotifictionViewModel(val useCase: Int = ResultNotificationActivity.USE_CASE_COMERCIO_REGISTRO_MC_SUCCESS,
                                      val idPais: Int = PaisResponse.PaisEntity.MX,
                                      val title: Int,
                                      val msg: String,
                                      val icon: Int,
                                      val buttonText: Int) : Parcelable

class ResultNotificationActivity : AppCompatActivity() {

    companion object {

        const val REQUEST_CODE = 433
        const val DATA = "addcel.mobilecard.ui.notification.data"

        const val USE_CASE_USUARIO_REGISTRO = 1
        const val USE_CASE_USUARIO_LOGIN = 2
        const val USE_CASE_USUARIO_MENU = 3
        const val USE_CASE_USUARIO_WALLET = 4

        const val USE_CASE_COMERCIO_REGISTRO_MC_SUCCESS = 5
        const val USE_CASE_COMERCIO_REGISTRO_MC_ERROR = 6
        const val USE_CASE_COMERCIO_LOGIN = 7
        const val USE_CASE_COMERCIO_MENU = 8
        const val USE_CASE_COMERCIO_WALLET = 9

        fun get(context: Context, model: ResultNotifictionViewModel): Intent {
            return Intent(context, ResultNotificationActivity::class.java).putExtra("model", model)
        }

        fun startForResult(context: Context, model: ResultNotifictionViewModel) {
            (context as Activity).startActivityForResult(get(context, model), REQUEST_CODE)
        }
    }

    private lateinit var viewModel: ResultNotifictionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = intent?.getParcelableExtra("model")!!
        setContentView(R.layout.activity_result_notification)

        title_result_notification.setText(viewModel.title)
        msg_result_notification.text = viewModel.msg
        img_result_notification.setImageResource(viewModel.icon)
        b_result_notification.setText(viewModel.buttonText)

        b_result_notification.setOnClickListener {
            clickButton(viewModel.useCase)
        }
    }


    private fun clickButton(useCase: Int) {
        Handler().postDelayed({
            finishWithResult(useCase)
        }, TimeUnit.SECONDS.toMillis(1))

    }

    private fun finishWithResult(useCase: Int) {
        intent.putExtra(DATA, useCase)
        setResult(RESULT_OK, intent)
        finish()

    }

    override fun onBackPressed() {
        clickButton(viewModel.useCase)
    }
}
