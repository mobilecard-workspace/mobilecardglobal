package addcel.mobilecard.ui.negocio.pe.registro.third

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.negocios.entity.BankEntity
import addcel.mobilecard.data.net.negocios.entity.NegocioAltaRequest
import addcel.mobilecard.data.net.negocios.entity.NegocioRegistroResponse
import addcel.mobilecard.data.net.usuarios.model.TerminosResponse
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.negocio.pe.menu.NegocioMenuPeActivity
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeActivity
import addcel.mobilecard.ui.negocio.pe.registro.persona.PePersonaFiscal
import addcel.mobilecard.ui.negocio.pe.registro.second.NegocioRegistroPE2FragmentData
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import addcel.mobilecard.validation.ValidationUtils
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_pe_negocio_registro_3.*
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-07-10.
 */

@Parcelize
data class NegocioRegistroPE3FragmentData(
        val configData2: NegocioRegistroPE2FragmentData, val nacimiento: String,
        val nacionalidad: String, val direccion: String, val distrito: String, val provincia: String,
        val departamento: String, val celular: String, val email: String, val password: String
) :
        Parcelable

interface View : ScreenView {

    fun updateUiOnTipoPersona(persona: PePersonaFiscal)

    fun setBancos(result: List<BankEntity>)

    fun setTerminos(result: TerminosResponse)

    fun setContrato(result: TerminosResponse)

    fun clickGuardar()

    fun onRegistroSuccess(result: NegocioRegistroResponse)

    fun onLoginSuccess()

    fun showRetry()

    fun hideRetry()
}

class NegocioRegistroPe3Fragment : Fragment(), addcel.mobilecard.ui.negocio.pe.registro.third.View {

    companion object {
        fun get(configData: NegocioRegistroPE3FragmentData): NegocioRegistroPe3Fragment {
            val args = BundleBuilder().putParcelable("data", configData).build()
            val frag = NegocioRegistroPe3Fragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var containerAct: NegocioRegistroPeActivity
    @Inject
    lateinit var configData: NegocioRegistroPE3FragmentData
    @Inject
    lateinit var adapter: ArrayAdapter<BankEntity>
    @Inject
    lateinit var state: StateSession
    @Inject
    lateinit var presenter: NegocioRegistroPe3Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioRegistroPe3Subcomponent(NegocioRegistroPe3Module(this))
                .inject(this)

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_pe_negocio_registro_3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        check_pe_negocio_registro_burocrata.setOnCheckedChangeListener { _, p1 ->
            til_pe_negocio_registro_burocrata.isEnabled = p1
        }
        containerAct.getRetryView().findViewById<Button>(R.id.b_retry).setOnClickListener {
            presenter.getBanks()
        }

        til_pe_negocio_registro_burocrata.isEnabled = false
        b_pe_negocio_registro_terms.setOnClickListener { presenter.getTerminos() }
        b_pe_negocio_registro_contrato.setOnClickListener { presenter.getContrato() }
        b_pe_negocio_registro_3_guardar.setOnClickListener { clickGuardar() }
        spinner_pe_negocio_registro_banco.adapter = adapter
        updateUiOnTipoPersona(configData.configData2.configData1.persona)

        if (adapter.isEmpty) presenter.getBanks()
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun onRegistroSuccess(result: NegocioRegistroResponse) {

    }

    override fun onLoginSuccess() {
        startActivity(
                NegocioMenuPeActivity.get(
                        containerAct,
                        false
                ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        )
        containerAct.finish()
    }

    override fun updateUiOnTipoPersona(persona: PePersonaFiscal) {
        val visibility = if (persona == PePersonaFiscal.JURIDICA) View.GONE else View.VISIBLE

        til_pe_negocio_registro_centro_laboral.visibility = visibility
        til_pe_negocio_registro_ocupacion.visibility = visibility
        label_pe_negocio_registro_burocrata.visibility = visibility
        check_pe_negocio_registro_burocrata.visibility = visibility
        til_pe_negocio_registro_burocrata.visibility = visibility
    }

    override fun setBancos(result: List<BankEntity>) {
        if (adapter.count > 0) adapter.clear()
        adapter.addAll(result)
    }

    override fun setTerminos(result: TerminosResponse) {
        if (result.idError == 0) {
            AlertDialog.Builder(containerAct).setTitle(result.mensajeError)
                    .setMessage(result.terminos)
                    .setPositiveButton(android.R.string.ok) { p0, _ ->
                        check_pe_negocio_registro_terms.isChecked = true; p0.dismiss()
                    }.setNegativeButton(android.R.string.cancel) { p0, _ ->
                        check_pe_negocio_registro_terms.isChecked = false; p0.dismiss()
                    }.show()
        } else {
            AlertDialog.Builder(containerAct).setMessage(result.mensajeError)
                    .setPositiveButton(android.R.string.ok) { p0, _ ->
                        check_pe_negocio_registro_terms.isChecked = false; p0.dismiss()
                    }.show()
        }
    }

    override fun setContrato(result: TerminosResponse) {
        if (result.idError == 0) {
            AlertDialog.Builder(containerAct).setTitle(result.mensajeError)
                    .setMessage(result.terminos)
                    .setPositiveButton(android.R.string.ok) { p0, _ ->
                        check_pe_negocio_registro_contrato.isChecked = true; p0.dismiss()
                    }.setNegativeButton(android.R.string.cancel) { p0, _ ->
                        check_pe_negocio_registro_contrato.isChecked = false; p0.dismiss()
                    }.show()
        } else {
            AlertDialog.Builder(containerAct).setMessage(result.mensajeError)
                    .setPositiveButton(android.R.string.ok) { p0, _ ->
                        check_pe_negocio_registro_contrato.isChecked = false; p0.dismiss()
                    }.show()
        }
    }

    override fun clickGuardar() {
        ValidationUtils.clearViewErrors(
                til_pe_negocio_registro_centro_laboral,
                til_pe_negocio_registro_ocupacion, til_pe_negocio_registro_burocrata,
                til_pe_negocio_registro_clabe
        )
        if (validate()) {
            presenter.altaUsuario(buildRequest())
        }
    }

    private fun validate(): Boolean {
        var isValid = true

        if (til_pe_negocio_registro_centro_laboral.visibility == View.VISIBLE && AndroidUtils.getText(
                        til_pe_negocio_registro_centro_laboral.editText
                ).isEmpty()
        ) {
            til_pe_negocio_registro_centro_laboral.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (til_pe_negocio_registro_ocupacion.visibility == View.VISIBLE && AndroidUtils.getText(
                        til_pe_negocio_registro_ocupacion.editText
                ).isEmpty()
        ) {
            til_pe_negocio_registro_ocupacion.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (til_pe_negocio_registro_burocrata.visibility == View.VISIBLE && til_pe_negocio_registro_burocrata.isEnabled && AndroidUtils.getText(
                        til_pe_negocio_registro_burocrata.editText
                ).isEmpty()
        ) {
            til_pe_negocio_registro_burocrata.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (configData.configData2.configData1.persona == PePersonaFiscal.JURIDICA && AndroidUtils.getText(
                        til_pe_negocio_registro_clabe.editText
                ).isEmpty() || AndroidUtils.getText(
                        til_pe_negocio_registro_clabe.editText
                ).length < 20
        ) {
            til_pe_negocio_registro_clabe.error = "Ingresa un CCI válido"
            isValid = false
        }


        if (!check_pe_negocio_registro_contrato.isChecked) {
            containerAct.showError(getString(R.string.error_viamericas_privacy))
            isValid = false
        }

        if (!check_pe_negocio_registro_terms.isChecked) {
            containerAct.showError(getString(R.string.error_terms_conditions))
            isValid = false
        }

        if (adapter.count == 0) {
            containerAct.showError("Selecciona tu institución bancaria")
            isValid = false

            presenter.getBanks()
        }

        return isValid
    }

    private fun buildRequest(): NegocioAltaRequest {

        val bank = spinner_pe_negocio_registro_banco.selectedItem as BankEntity


        return NegocioAltaRequest(
                idAplicacion = BuildConfig.ADDCEL_APP_ID
                ,
                idEstablecimiento = configData.configData2.configData1.idEstablecimiento
                ,
                rfc = configData.configData2.ruc
                ,
                razonSocial = configData.configData2.razonSocial
                ,
                nombreEstablecimiento = configData.configData2.nombreComercio
                ,
                numeroDocumento = configData.configData2.numeroDni
                ,
                tipoDocumento = "1"
                ,
                nombre = configData.configData2.nombre
                ,
                paterno = configData.configData2.paterno
                ,
                materno = configData.configData2.materno
                ,
                fechaNacimiento = configData.nacimiento
                ,
                nacionalidad = configData.nacionalidad
                ,
                direccionEstablecimiento = configData.configData2.direccionComercio
                ,
                distrito = configData.distrito
                ,
                provincia = configData.provincia
                ,
                idPais = PaisResponse.PaisEntity.PE
                ,
                departamento = configData.departamento
                ,
                telefonoContacto = configData.celular
                ,
                emailContacto = configData.email
                ,
                usuario = configData.email
                ,
                pass = StringUtil.sha512(configData.password)
                ,
                centroLaboral = AndroidUtils.getText(til_pe_negocio_registro_centro_laboral.editText)
                ,
                ocupacion =
                StringUtil.removeAcentosMultiple(
                        AndroidUtils.getText(til_pe_negocio_registro_ocupacion.editText)
                )
                ,
                cargo = AndroidUtils.getText(til_pe_negocio_registro_burocrata.editText)
                ,
                negocio = true
                ,
                idBanco = bank.id.toString()
                ,
                codigoBanco = bank.clave ?: bank.id.toString()
                ,
                imgDomicilio = ""
                ,
                imgIne = ""
                ,
                cci = AndroidUtils.getText(til_pe_negocio_registro_clabe.editText)
                ,
                cuentaClabe = AndroidUtils.getText(til_pe_negocio_registro_clabe.editText)
                ,
                digitoVerificador = configData.configData2.digitoVerificador
                ,
                direccion = configData.direccion
                ,
                tipoPersona = configData.configData2.configData1.persona.name.toLowerCase(
                        Locale(
                                "es",
                                "PE"
                        )
                )
        )

    }

    override fun showProgress() {
        containerAct.showProgress()
    }

    override fun hideProgress() {
        containerAct.hideProgress()
    }

    override fun showError(msg: String) {
        containerAct.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerAct.showSuccess(msg)
    }

    override fun showRetry() {
        containerAct.showRetry()
    }

    override fun hideRetry() {
        containerAct.hideRetry()
    }
}