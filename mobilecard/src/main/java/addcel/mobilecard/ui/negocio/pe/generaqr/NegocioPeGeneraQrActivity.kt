package addcel.mobilecard.ui.negocio.pe.generaqr

import addcel.mobilecard.R
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.negocio.pe.generaqr.monto.NegocioPeGeneraQrKeyboardFragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_negocio_pe_genera_qr.*

class NegocioPeGeneraQrActivity : ContainerActivity() {

    companion object {
        fun get(context: Context): Intent {
            return Intent(context, NegocioPeGeneraQrActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_pe_genera_qr)

        setAppToolbarTitle(R.string.txt_menu_scan)

        fragmentManagerLazy.commit {
            add(
                    R.id.frame_negocio_negocio_pe_genera_qr,
                    NegocioPeGeneraQrKeyboardFragment.get()
            ).setCustomAnimations(
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
            )
        }
    }

    override fun getRetry(): View? {
        return null
    }

    override fun showProgress() {
        progress_negocio_negocio_pe_genera_qr.visibility = View.VISIBLE
    }

    override fun hideRetry() {

    }

    override fun hideProgress() {
        progress_negocio_negocio_pe_genera_qr.visibility = View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_negocio_negocio_pe_genera_qr.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_negocio_negocio_pe_genera_qr.title = title
    }

    override fun showRetry() {

    }
}
