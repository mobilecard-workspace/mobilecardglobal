package addcel.mobilecard.ui.negocio.mx.historial;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.collect.Lists;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.List;
import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.model.SPCuentaEntity;
import addcel.mobilecard.utils.ListUtil;

/**
 * ADDCEL on 11/14/18.
 */
public class NegocioHistorialAdapter
        extends RecyclerView.Adapter<NegocioHistorialAdapter.ViewHolder> {
    private final List<SPCuentaEntity> SPCuentaEntitys;
    private final NumberFormat currFormat;

    NegocioHistorialAdapter(NumberFormat currFormat) {
        this.SPCuentaEntitys = Lists.newArrayList();
        this.currFormat = currFormat;
    }

    public void update(List<SPCuentaEntity> SPCuentaEntitys) {
        if (ListUtil.notEmpty(this.SPCuentaEntitys)) this.SPCuentaEntitys.clear();
        this.SPCuentaEntitys.addAll(SPCuentaEntitys);
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_negocio_cuenta, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        SPCuentaEntity SPCuentaEntity = SPCuentaEntitys.get(position);
        holder.referenciaView.setText(SPCuentaEntity.getReferenciaNegocio());
        Objects.requireNonNull(holder.authTil.getEditText())
                .setText(String.format(
                        holder.authTil.getContext().getString(R.string.txt_negocio_item_cuenta_pagada),
                        SPCuentaEntity.getCodigoAut()));
        Objects.requireNonNull(holder.fechaTil.getEditText()).setText(SPCuentaEntity.getFecha());
        holder.totalView.setText(String.format(
                holder.totalView.getContext().getString(R.string.txt_negocio_item_cuenta_totalpagado),
                currFormat.format(SPCuentaEntity.getTotal())));
        Objects.requireNonNull(holder.importeTil.getEditText())
                .setText(currFormat.format(SPCuentaEntity.getImporte()));
        Objects.requireNonNull(holder.propinaTil.getEditText())
                .setText(currFormat.format(SPCuentaEntity.getPropina()));
        Objects.requireNonNull(holder.comisionTil.getEditText())
                .setText(currFormat.format(SPCuentaEntity.getComision()));
        holder.detallesButton.setOnClickListener(
                view -> holder.setDetalleVisible(holder.isDetallesVisible() ? View.GONE : View.VISIBLE));
    }

    @Override
    public int getItemCount() {
        return SPCuentaEntitys.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView referenciaView;
        private final TextInputLayout authTil;
        private final TextInputLayout fechaTil;
        private final Button detallesButton;
        private final TextView totalView;
        private final LinearLayout detalleLayout;
        private final TextInputLayout importeTil;
        private final TextInputLayout propinaTil;
        private final TextInputLayout comisionTil;

        ViewHolder(View itemView) {
            super(itemView);
            referenciaView = itemView.findViewById(R.id.view_cuenta_referencia);
            authTil = itemView.findViewById(R.id.til_cuenta_auth);
            fechaTil = itemView.findViewById(R.id.til_cuenta_fecha);
            detallesButton = itemView.findViewById(R.id.b_cuenta_detalles);
            totalView = itemView.findViewById(R.id.view_cuenta_total);
            detalleLayout = itemView.findViewById(R.id.layout_cuenta_detalle);
            importeTil = itemView.findViewById(R.id.til_cuenta_importe);
            propinaTil = itemView.findViewById(R.id.til_cuenta_propina);
            comisionTil = itemView.findViewById(R.id.til_cuenta_comision);
        }

        void setDetalleVisible(int visibility) {
            detalleLayout.setVisibility(visibility);
            detallesButton.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0,
                    visibility == View.VISIBLE ? R.drawable.negocio_desplegable_arriba
                            : R.drawable.negocio_desplegable_abajo, 0);
        }

        boolean isDetallesVisible() {
            return detalleLayout.getVisibility() == View.VISIBLE;
        }
    }
}
