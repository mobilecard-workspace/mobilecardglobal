package addcel.mobilecard.ui.negocio.pe.generaqr.qr

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.data.net.qr.QREntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.listener.NumberKeyboardListener
import addcel.mobilecard.ui.negocio.pe.generaqr.NegocioPeGeneraQrActivity
import addcel.mobilecard.utils.BundleBuilder
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_pe_genera_qr.*
import okio.ByteString.Companion.decodeBase64
import javax.inject.Inject

@Parcelize
data class NegocioPeGeneraQrQRFragmentData(
        val monto: String, val concepto: String,
        val propina: String
) : Parcelable

interface View : ScreenView {
    fun onQrSuccess(result: QREntity)
}

/**
 * ADDCEL on 2019-07-15.
 */
class NegocioPeGeneraQrQRFragment : Fragment(), addcel.mobilecard.ui.negocio.pe.generaqr.qr.View {

    companion object {
        fun get(confData: NegocioPeGeneraQrQRFragmentData): NegocioPeGeneraQrQRFragment {
            val args = BundleBuilder().putParcelable("data", confData).build()
            val frag = NegocioPeGeneraQrQRFragment()
            frag.arguments = args

            return frag
        }
    }

    @Inject
    lateinit var containerAct: NegocioPeGeneraQrActivity
    @Inject
    lateinit var configData: NegocioPeGeneraQrQRFragmentData
    @Inject
    lateinit var presenter: NegocioPeGeneraQrPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get()
                .netComponent.negocioPeGeneraQrQrSubcomponent(NegocioPeGeneraQrQrModule(this))
                .inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                addcel.mobilecard.R.layout.screen_negocio_pe_genera_qr, container,
                false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.generaQR(configData.monto, configData.concepto, configData.propina)
    }

    override fun onQrSuccess(result: QREntity) {
        if (isVisible) {
            val arr = result.qrBase64.decodeBase64()?.toByteArray()
            val bmp = BitmapFactory.decodeByteArray(
                    result.qrBase64.decodeBase64()?.toByteArray(), 0,
                    arr?.size!!
            )
            view_negocio_cobro_create_qr_code.setImageBitmap(bmp)
            setMessage()
        }
    }

    override fun showProgress() {
        containerAct.showProgress()
    }

    override fun hideProgress() {
        containerAct.hideProgress()
    }

    override fun showError(msg: String) {
        containerAct.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerAct.showSuccess(msg)
    }

    fun setMessage() {
        val monto = NumberKeyboardListener.getFormattedMonto(configData.monto, 4)
        val propina = NumberKeyboardListener.getFormattedMontoDecimal(configData.propina, 4)
        val comision = NumberKeyboardListener.getFormattedMontoDecimal(
                presenter.getComision(configData.monto.toDouble() / 100).toString(), 4
        )
        val concepto = configData.concepto
        val establecimiento = presenter.getIdEstablecimiento().toString()

        val propinaD = if (configData.propina.isEmpty()) 0.0 else configData.propina.toDouble()
        val totalD =
                configData.monto.toDouble() / 100 + propinaD + presenter.getComision(configData.monto.toDouble() / 100)

        val total =
                "Total: " + NumberKeyboardListener.getFormattedMontoDecimal(totalD.toString(), 4)

        val msg =
                "Monto: $monto\n\nPropina: $propina\n\nComisión por uso de plataforma: $comision\n\nConcepto: $concepto\n\nEstablecimiento: $establecimiento"

        view_negocio_cobro_create_qr_info.text = msg
        view_negocio_cobro_create_qr_total.text = total
        b_negocio_cobro_create_qr_confirmar.setOnClickListener {
            containerAct.finish()
        }
    }
}