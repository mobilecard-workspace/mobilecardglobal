package addcel.mobilecard.ui.negocio.mx.cobro.secure;

import androidx.annotation.NonNull;

import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 08/11/17.
 */
public interface NegocioCobroSecureContract {
    interface View extends ScreenView {

        void log(String msg);

        void onPagoStarted(@NonNull String html);

        void processHthResponse(SPReceiptEntity response);
    }

    interface Presenter {
        SPReceiptEntity createResponse(String msg);

        void pago(@NonNull SPPagoEntity data);
    }
}
