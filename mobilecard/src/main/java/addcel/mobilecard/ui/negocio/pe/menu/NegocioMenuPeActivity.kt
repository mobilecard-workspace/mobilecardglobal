package addcel.mobilecard.ui.negocio.pe.menu

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.usuarios.model.PushApi
import addcel.mobilecard.data.net.usuarios.model.PushTokenRequest
import addcel.mobilecard.domain.usuario.bottom.MenuEvent
import addcel.mobilecard.domain.usuario.bottom.UseCase
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.contacto.ContactoActivity
import addcel.mobilecard.ui.contacto.ContactoModel
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.login.LoginActivity
import addcel.mobilecard.ui.negocio.mx.historial.NegocioHistorialActivity
import addcel.mobilecard.ui.negocio.pe.escaneaqr.result.NegocioPeEscaneaQrResultActivity
import addcel.mobilecard.ui.negocio.pe.escaneaqr.scanner.NegocioPeEscaneaQrActivity
import addcel.mobilecard.ui.negocio.pe.generaqr.NegocioPeGeneraQrActivity
import addcel.mobilecard.ui.negocio.pe.id.NegocioPeIdQRActivity
import addcel.mobilecard.ui.negocio.pe.id.NegocioPeIdQrModel
import addcel.mobilecard.ui.negocio.pe.registro.persona.PePersonaFiscal
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.NegocioPeTarjetaPresenteActivity
import addcel.mobilecard.ui.negocio.pe.wallet.NegocioPeWalletActivity
import addcel.mobilecard.ui.negocio.pe.wallet.NegocioPeWalletModel
import addcel.mobilecard.ui.novedades.NovedadesActivity
import addcel.mobilecard.ui.novedades.NovedadesModel
import addcel.mobilecard.ui.usuario.menu.BottomMenuEventListener
import addcel.mobilecard.ui.usuario.menu.MenuLayout
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.iid.FirebaseInstanceId
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem
import com.squareup.otto.Bus
import com.squareup.otto.Subscribe
import com.tbruyelle.rxpermissions2.RxPermissions
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_negocio_menu_pe.*
import retrofit2.Retrofit
import timber.log.Timber
import java.util.*
import javax.inject.Inject

enum class NegocioPeruMenuItems(val icon: Int, val text: Int) {
    TARJETA_PRESENTE(R.drawable.tarjeta, R.string.s_menu_negocio_tarjeta_presente), GENERA_QR(
            R.drawable.generar,
            R.string.s_menu_negocio_genera_qr
    ) //ESCANEA_QR(R.drawable.escanear, R.string.s_menu_negocio_escanea_qr)
}

interface NegocioMenuPeView : ScreenView, BottomMenuEventListener {

    fun getNombreNegocio(): String

    fun launchProfile()

    fun configBottomLayout()
}

class NegocioMenuPeActivity : AppCompatActivity(), NegocioMenuPeView {

    companion object {

        private const val PERFIL = 0L
        private const val INICIO = 6L
        private const val TARJETA = 1L
        private const val GENERA = 2L
        private const val ESCANEA = 3L
        private const val CONTACTO = 8L
        private const val LOGOUT = 5L
        private const val VERSION = 7L

        const val PCT_COMISION_IGV = 0.18

        fun get(context: Context, tutorialAlreadyShown: Boolean): Intent {
            return Intent(context, NegocioMenuPeActivity::class.java).putExtras(
                    BundleBuilder().putBoolean("tutorialAlreadyShown", tutorialAlreadyShown).build()
            )
        }
    }

    lateinit var adapter: NegocioMenuPeAdapter
    val compositeDisposable = CompositeDisposable()

    lateinit var perfilItem: PrimaryDrawerItem
    lateinit var inicioItem: PrimaryDrawerItem
    lateinit var tarjetaItem: PrimaryDrawerItem
    lateinit var generaQrItem: PrimaryDrawerItem
    // lateinit var escaneQrItem: PrimaryDrawerItem
    lateinit var dividerItem: DividerDrawerItem
    lateinit var contactoItem: SecondaryDrawerItem
    lateinit var logoutItem: SecondaryDrawerItem
    lateinit var versionItem: SecondaryDrawerItem

    lateinit var drawer: Drawer
    lateinit var rxPermission: RxPermissions


    @Inject
    lateinit var retrofit: Retrofit
    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var state: StateSession
    @Inject
    lateinit var bus: Bus
    private lateinit var push: PushApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.inject(this)

        push = PushApi.get(retrofit)

        setContentView(R.layout.activity_negocio_menu_pe)

        adapter = NegocioMenuPeAdapter(NegocioPeruMenuItems.values().asList(), this)

        drawer = setDrawerLayout()
        recycler_negocio_peru_menu.adapter = adapter
        recycler_negocio_peru_menu.layoutManager = LinearLayoutManager(this)
        rxPermission = RxPermissions(this)
        ItemClickSupport.addTo(recycler_negocio_peru_menu)
                .setOnItemClickListener { rv, position, v ->
                    val vh = rv.getChildViewHolder(v)
                    if (vh is NegocioMenuPeAdapter.ItemViewHolder) {
                        when (adapter.getItem(position)) {
                            NegocioPeruMenuItems.TARJETA_PRESENTE -> startActivity(
                                    NegocioPeTarjetaPresenteActivity.get(this@NegocioMenuPeActivity)
                            )
                            NegocioPeruMenuItems.GENERA_QR -> startActivity(
                                    NegocioPeGeneraQrActivity.get(
                                            this@NegocioMenuPeActivity
                                    )
                            ) //NegocioPeruMenuItems.ESCANEA_QR -> launchScanQr(rxPermission)
                        }
                    }
                }

        tutorial_negocio_peru_menu.setOnClickListener {
            tutorial_negocio_peru_menu.visibility = View.GONE
        }

        initItems()
        setItemsInDrawer()
        configBottomLayout()

        drawer.setSelection(INICIO)

        if (!state.isMenuLaunched()) {
            state.setMenuLaunched(true)

            tutorial_negocio_peru_menu.visibility = View.VISIBLE

            val persona = session.negocio.tipoPersona

            startActivityForResult(
                    NovedadesActivity.get(
                            this, NovedadesModel(
                            PaisResponse.PaisEntity.PE,
                            "NEGOCIO",
                            persona == PePersonaFiscal.JURIDICA.name.toLowerCase(
                                    Locale.getDefault()
                            )
                    )
                    ),
                    NovedadesActivity.REQUEST_CODE
            )
        } else {
            tutorial_negocio_peru_menu.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()
        bus.register(this)

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (!it.isSuccessful) {
                Timber.e(it.exception, "Error al obtener instanceId")
                return@addOnCompleteListener
            }

            val token = it.result?.token
            Timber.d("Token push: %s", token)

            if (token != null) {

                val idApp = BuildConfig.ADDCEL_APP_ID
                val idEstablecimiento = session.negocio.idEstablecimiento.toLong()
                val idPais = session.negocio.idPais
                val idioma = StringUtil.getCurrentLanguage()

                compositeDisposable.add(
                        push.saveToken(
                                PushApi.PUSH_AUTH,
                                PushTokenRequest(
                                        idApp,
                                        idPais,
                                        idEstablecimiento,
                                        idioma,
                                        "ESTABLECIMIENTO",
                                        token
                                )
                        )
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({}, { t ->
                                    t.printStackTrace()
                                })
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()
        bus.unregister(this)
    }

    override fun onRestart() {
        super.onRestart()
        drawer.setSelection(INICIO)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_drawer) {
            if (!drawer.isDrawerOpen) drawer.openDrawer()
            return true
        } else if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showProgress() {
        progress_negocio_peru_menu.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_negocio_peru_menu.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen) {
            drawer.closeDrawer()
        } else {
            super.onBackPressed()
        }
    }

    private fun setDrawerLayout(): Drawer {

        setSupportActionBar(toolbar_negocio_peru_menu)

        return DrawerBuilder().withActivity(this).withToolbar(toolbar_negocio_peru_menu)
                .withSliderBackgroundColorRes(R.color.colorAccent).withActionBarDrawerToggle(false)
                .withHeader(R.layout.item_drawer_header).withDrawerGravity(Gravity.END)
                .withOnDrawerItemClickListener { _, _, drawerItem ->
                    clickDrawer(drawerItem.identifier)
                    false
                }.build()
    }

    private fun initItems() {
        perfilItem = buildProfileItem()
        inicioItem = buildInicioItem()
        tarjetaItem = buildPresenteItem()
        generaQrItem = buildGeneraItem() // escaneQrItem = buildEscaneaItem()
        dividerItem = buildDivider()
        contactoItem = buildContactoItem()
        logoutItem = buildLogoutItem()
        versionItem = buildVersionItem()
    }

    private fun setItemsInDrawer() { //drawer.addItem(perfilItem)
        //drawer.addItem(dividerItem)
        drawer.addItem(inicioItem)
        drawer.addItem(tarjetaItem)
        drawer.addItem(generaQrItem) // drawer.addItem(escaneQrItem)
        drawer.addItem(dividerItem)
        drawer.addItem(contactoItem)
        drawer.addItem(logoutItem)
        drawer.addItem(versionItem)
    }

    private fun buildProfileItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(PERFIL).withName(R.string.txt_perfil_title)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground).withIcon(R.drawable.icon_settings)
    }

    private fun buildInicioItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(INICIO).withName(R.string.nav_home)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
    }

    private fun buildPresenteItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(TARJETA)
                .withName(NegocioPeruMenuItems.TARJETA_PRESENTE.text)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
    }

    private fun buildGeneraItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(GENERA)
                .withName(NegocioPeruMenuItems.GENERA_QR.text)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
    }

    /*private fun buildEscaneaItem(): PrimaryDrawerItem {
      return PrimaryDrawerItem().withIdentifier(ESCANEA)
          .withName(NegocioPeruMenuItems.ESCANEA_QR.text).withTextColorRes(R.color.colorBackground)
          .withSelectedColorRes(R.color.colorAccentDark)
          .withSelectedTextColorRes(R.color.colorBackground)
    }*/

    private fun buildDivider(): DividerDrawerItem {
        return DividerDrawerItem().withIdentifier(4)
    }

    private fun buildContactoItem(): SecondaryDrawerItem {
        return SecondaryDrawerItem().withIdentifier(CONTACTO)
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withName(getString(R.string.nav_contactus)).withIcon(R.drawable.icon_info_white)
    }

    private fun buildLogoutItem(): SecondaryDrawerItem {
        return SecondaryDrawerItem().withIdentifier(LOGOUT)
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withName(getString(R.string.nav_logout))
                .withIcon(R.drawable.icon_logout)
    }

    private fun buildVersionItem(): SecondaryDrawerItem {
        return SecondaryDrawerItem().withIdentifier(VERSION).withEnabled(false)
                .withDisabledTextColorRes(R.color.colorBackground)
                .withName(getString(R.string.version, BuildConfig.VERSION_NAME).toUpperCase())
    }

    private fun clickDrawer(id: Long) {
        when (id) {
            TARJETA -> startActivity(NegocioPeTarjetaPresenteActivity.get(this))
            GENERA -> startActivity(NegocioPeGeneraQrActivity.get(this))
            ESCANEA -> launchScanQr(rxPermission)
            CONTACTO -> launchContacto()
            LOGOUT -> logout()
            else -> {

            }
        }
    }

    override fun getNombreNegocio(): String {
        return session.negocio.nombreEstablecimiento
    }

    override fun launchProfile() {
        startActivity(
                NegocioPeIdQRActivity.get(
                        this,
                        NegocioPeIdQrModel(
                                qrBase64 = session.negocio.qrBase64,
                                negocioName = session.negocio.nombreEstablecimiento
                        )
                )
        )
    }

    override fun configBottomLayout() {
        val favs: ImageButton = menu_negocio_peru_menu.findViewById(R.id.b_menu_frecuentes)
        favs.visibility = View.GONE
    }

    private fun logout() {
        AlertDialog.Builder(this).setMessage("¿Deseas cerrar sesión MobileCard?")
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    session.logout()
                    startActivity(
                            LoginActivity.get(this@NegocioMenuPeActivity).addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                            )
                    )
                    finish()
                }.setNegativeButton(android.R.string.no) { p0, _ -> p0?.dismiss() }.show()
    }

    private fun launchContacto() {
        startActivity(
                ContactoActivity.get(
                        this,
                        ContactoModel(session.negocio.idEstablecimiento.toLong(), session.negocio.email, 4)
                )
        )
    }

    private fun launchScanQr(rxPermission: RxPermissions) {
        compositeDisposable.add(rxPermission.request(Manifest.permission.CAMERA).subscribe { accept ->
            if (accept) {
                startActivityForResult(
                        NegocioPeEscaneaQrActivity.get(this@NegocioMenuPeActivity),
                        NegocioPeEscaneaQrActivity.REQUEST_CODE
                )
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == NegocioPeEscaneaQrActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val result: SPReceiptEntity = data?.getParcelableExtra("result")!!
                startActivity(NegocioPeEscaneaQrResultActivity.get(this, result))
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toasty.error(this, "Cancelado por el usuario").show()
            }
        } else if (requestCode == NovedadesActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val useCase = data.getIntExtra(NovedadesActivity.DATA_USE_CASE, -1)
                    NovedadesActivity.processNegocio(
                            this, useCase, session.negocio.idEstablecimiento,
                            PaisResponse.PaisEntity.PE, session.negocio.tipoPersona
                    )
                }
            }
        }
    }

    @Subscribe
    override fun onMenuEventReceived(event: MenuEvent) {
        (menu_negocio_peru_menu as MenuLayout).unblock()

        when (event.useCase) {
            UseCase.WALLET -> startActivity(
                    NegocioPeWalletActivity.get(
                            this,
                            NegocioPeWalletModel(
                                    session.negocio.idEstablecimiento,
                                    session.negocio.tipoPersona
                            )
                    )
            )
            UseCase.HISTORIAL -> startActivity(NegocioHistorialActivity.get(this, 4))
            else -> Timber.d("In progress")
        }
    }
}
