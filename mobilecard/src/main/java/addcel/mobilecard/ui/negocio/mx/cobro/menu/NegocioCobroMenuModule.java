package addcel.mobilecard.ui.negocio.mx.cobro.menu;

import java.util.Arrays;
import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.negocio.cobro.menu.NegocioCobroMenuInteractor;
import addcel.mobilecard.domain.negocio.cobro.menu.NegocioCobroMenuInteractorImpl;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 07/09/18.
 */
@Module
public final class NegocioCobroMenuModule {
    private final NegocioCobroMenuFragment fragment;

    public NegocioCobroMenuModule(NegocioCobroMenuFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    NegocioCobroActivity provideActivity() {
        return (NegocioCobroActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    NegocioAPI provideApi(Retrofit retrofit) {
        return retrofit.create(NegocioAPI.class);
    }

    @PerFragment
    @Provides
    NegocioCobroMenuInteractor provideInteractor(NegocioAPI api,
                                                 SessionOperations session) {
        return new NegocioCobroMenuInteractorImpl(api, session);
    }

    @PerFragment
    @Provides
    NegocioCobroMenuContract.Presenter providePresenter(
            NegocioCobroMenuInteractor interactor) {
        return new NegocioCobroMenuPresenter(interactor, fragment);
    }

    @PerFragment
    @Provides
    NegocioCobroMenuAdapter provideAdapter() {
        return new NegocioCobroMenuAdapter(Arrays.asList(NegocioCobroMenuContract.TipoCobro.values()), fragment);
    }
}
