package addcel.mobilecard.ui.negocio.mx.cobro.scan;

import java.util.Objects;

import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 13/09/18.
 */
@Module
public final class NegocioCobroQrScanModule {
    private final NegocioCobroQrScanFragment fragment;

    NegocioCobroQrScanModule(NegocioCobroQrScanFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    NegocioCobroActivity provideActivity() {
        return (NegocioCobroActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    ScanPayService provideService(Retrofit retrofit) {
        return retrofit.create(ScanPayService.class);
    }

    @PerFragment
    @Provides
    NegocioCobroQrScanContract.Presenter providePresenter(
            ScanPayService service) {
        return new NegocioCobroQrScanPresenter(service, fragment);
    }
}
