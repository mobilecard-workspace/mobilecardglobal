package addcel.mobilecard.ui.negocio.pe.tarjetapresente.concepto

import addcel.mobilecard.R
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.NegocioPeTarjetaPresenteActivity
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.tarjeta.NegocioPeTarjetaPresenteTarjetaData
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.tarjeta.NegocioPeTarjetaPresenteTarjetaFragment
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_pe_tarjeta_presente_concepto.*

/**
 * ADDCEL on 2019-07-13.
 */

@Parcelize
data class NegocioPeTarjetaPresenteConceptoData(val monto: String) : Parcelable

class NegocioPeTarjetaPresenteConceptoFragment : Fragment() {
    companion object {
        fun get(
                configData: NegocioPeTarjetaPresenteConceptoData
        ): NegocioPeTarjetaPresenteConceptoFragment {
            val args = BundleBuilder().putParcelable("data", configData).build()
            val fragment = NegocioPeTarjetaPresenteConceptoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var configData: NegocioPeTarjetaPresenteConceptoData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configData = arguments?.getParcelable("data")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.screen_negocio_pe_tarjeta_presente_concepto,
                container,
                false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b_negocio_pe_tarjeta_presente_continuar.setOnClickListener {
            val concepto = AndroidUtils.getText(text_negocio_pe_tarjeta_presente_concepto)
            if (concepto.isNotEmpty()) {
                val tarjetaData = NegocioPeTarjetaPresenteTarjetaData(configData.monto, concepto)
                (activity as NegocioPeTarjetaPresenteActivity).fragmentManagerLazy.commit {
                    add(
                            R.id.frame_negocio_negocio_pe_tarjeta_presente,
                            NegocioPeTarjetaPresenteTarjetaFragment.get(tarjetaData)
                    )
                    hide(this@NegocioPeTarjetaPresenteConceptoFragment)
                    addToBackStack(null)
                    setCustomAnimations(
                            android.R.anim.slide_in_left,
                            android.R.anim.slide_out_right
                    )
                }
            } else {
                (activity as NegocioPeTarjetaPresenteActivity).showError(
                        getString(R.string.error_campo_generico)
                )
            }
        }
    }
}