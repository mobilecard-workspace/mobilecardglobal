package addcel.mobilecard.ui.negocio.mx.registro

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.negocio.mx.registro.domicilio.NegocioRegistroDomicilioFragment
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroActivity
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroActivity.Companion.get
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroModel
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_negocio_registro.*


@Parcelize
data class NegocioRegistroViewModel(
        val phone: String = "",
        val email: String = ""
) : Parcelable

class NegocioRegistroActivity : ContainerActivity(), NegocioRegistroView {

    lateinit var viewModel: NegocioRegistroViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_registro)
        viewModel = intent?.getParcelableExtra("model")!!
        if (savedInstanceState == null) addInfoFragment()
    }

    override fun showProgress() {
        if (progress_negocio_registro.visibility == View.GONE)
            progress_negocio_registro.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        if (progress_negocio_registro.visibility == View.VISIBLE)
            progress_negocio_registro.visibility = View.GONE
    }

    override fun clearDomImg() {

    }

    private fun addInfoFragment() {
        fragmentManagerLazy.commit {
            add(
                    R.id.frame_negocio_registro,
                    NegocioRegistroDomicilioFragment.get(viewModel.phone)
            )
            setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        }
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_negocio_registro.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_negocio_registro.title = title
    }

    override fun getRetry(): View? {
        return retry_negocio_registro
    }

    override fun showRetry() {
        if (retry_negocio_registro.visibility == View.GONE) retry_negocio_registro.visibility =
                View.VISIBLE
    }

    override fun hideRetry() {
        if (retry_negocio_registro.visibility == View.VISIBLE) retry_negocio_registro.visibility =
                View.GONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in fragmentManagerLazy.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onBackPressed() {
        hideRetry()
        if (fragmentManagerLazy.backStackEntryCount == 0) {
            startActivity(
                    get(
                            this, SmsRegistroModel(
                            PaisResponse.PaisEntity(1, "MX", "MEXICO"),
                            SmsRegistroActivity.USE_CASE_NEGOCIO)
                    )
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            finish()
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        fun get(context: Context, phone: String, email: String): Intent {
            val vm = NegocioRegistroViewModel(phone, email)
            return Intent(context, NegocioRegistroActivity::class.java).putExtra("model", vm)
        }
    }
}
