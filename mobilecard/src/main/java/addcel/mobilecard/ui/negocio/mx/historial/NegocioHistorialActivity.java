package addcel.mobilecard.ui.negocio.mx.historial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Lists;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.model.SPCuentaEntity;
import es.dmoral.toasty.Toasty;

public class NegocioHistorialActivity extends AppCompatActivity
        implements NegocioHistorialContract.View {

    @Inject
    NegocioHistorialAdapter adapter;
    @Inject
    @Named("idPais")
    int idPais;
    @Inject
    NegocioHistorialContract.Presenter presenter;

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private Button reintentarButton;

    public static synchronized Intent get(Context context, int idPais) {
        return new Intent(context, NegocioHistorialActivity.class).putExtra("idPais", idPais);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .negocioHistorialSubcomponent(new NegocioHistorialModule(this))
                .inject(this);
        setContentView(R.layout.activity_negocio_historial);
        Toolbar toolbar = findViewById(R.id.toolbar_negocio_historial);
        progressBar = findViewById(R.id.progress_negocio_historial);
        recyclerView = findViewById(R.id.view_negocio_historial);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        reintentarButton = findViewById(R.id.b_negocio_historial_refresh);
        reintentarButton.setOnClickListener(v -> clickReintentar());

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        presenter.getCuentas(idPais);
    }

    @Override
    protected void onDestroy() {
        presenter.clearDisposables();
        recyclerView.setAdapter(null);
        super.onDestroy();
    }

    @Override
    public void showError(@NotNull String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void showSuccess(@NotNull String msg) {
        Toasty.success(this, msg).show();
    }

    @Override
    public void setCuentas(List<SPCuentaEntity> cuentas) {
        adapter.update(cuentas);
        recyclerView.setVisibility(View.VISIBLE);
        reintentarButton.setVisibility(View.GONE);
    }

    @Override
    public void notCuentas() {
        adapter.update(Lists.newArrayList());
        recyclerView.setVisibility(View.GONE);
        reintentarButton.setVisibility(View.VISIBLE);
        showError(getString(R.string.txt_no_transaciones));
    }

    @Override
    public void serverError() {
        adapter.update(Lists.newArrayList());
        recyclerView.setVisibility(View.GONE);
        reintentarButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void clickReintentar() {
        presenter.getCuentas(idPais);
    }

    @Override
    public void showProgress() {
        if (progressBar.getVisibility() == View.GONE) progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (progressBar.getVisibility() == View.VISIBLE) progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
