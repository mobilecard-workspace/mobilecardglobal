package addcel.mobilecard.ui.negocio.pe.registro.second

import addcel.mobilecard.McRegexPatterns
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.DeptoEntity
import addcel.mobilecard.data.net.catalogo.DistritoEntity
import addcel.mobilecard.data.net.catalogo.ProvinciaEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeActivity
import addcel.mobilecard.ui.negocio.pe.registro.first.NegocioRegistroPE1FragmentData
import addcel.mobilecard.ui.negocio.pe.registro.third.NegocioRegistroPE3FragmentData
import addcel.mobilecard.ui.negocio.pe.registro.third.NegocioRegistroPe3Fragment
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.afollestad.vvalidator.form
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_registro_domicilio.*
import kotlinx.android.synthetic.main.screen_pe_negocio_registro_2.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-07-10.
 */

@Parcelize
data class NegocioRegistroPE2FragmentData(
        val configData1: NegocioRegistroPE1FragmentData, val ruc: String = "", val razonSocial: String,
        val nombreComercio: String, val direccionComercio: String, val numeroDni: String,
        val digitoVerificador: String, val tipoDni: String, val nombre: String, val paterno: String,
        val materno: String
) : Parcelable

interface View : ScreenView {

    fun setInitialValues(
            deptos: List<DeptoEntity>,
            provincias: List<ProvinciaEntity>,
            distritos: List<DistritoEntity>
    )

    fun showRetry()

    fun hideRetry()

    fun setDeptos(deptos: List<DeptoEntity>)

    fun setProvincias(provincias: List<ProvinciaEntity>, currDeptoPos: Int)

    fun setDistritos(distritos: List<DistritoEntity>, currProvinciaPos: Int)

    fun clickGuardar()
}

class NegocioRegistroPe2Fragment : Fragment(),
        addcel.mobilecard.ui.negocio.pe.registro.second.View {

    companion object {

        private val dobFormatterView: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        private val dobFormatterApi: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

        fun get(configData: NegocioRegistroPE2FragmentData): NegocioRegistroPe2Fragment {
            val args = BundleBuilder().putParcelable("data", configData).build()
            val fragment = NegocioRegistroPe2Fragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var containerAct: NegocioRegistroPeActivity
    @Inject
    lateinit var configData: NegocioRegistroPE2FragmentData
    @Inject
    lateinit var deptoAdapter: ArrayAdapter<DeptoEntity>
    @Inject
    lateinit var provinciaAdapter: ArrayAdapter<ProvinciaEntity>
    @Inject
    lateinit var distritoAdapter: ArrayAdapter<DistritoEntity>
    @Inject
    lateinit var presenter: NegocioRegistroPe2Presenter

    lateinit var datePickerDialog: DatePickerDialog
    private var fechaNacimientoDueno: LocalDateTime = LocalDateTime.now()

    //ESTADO ACTUAL DEPARTAMENTO y PROVINCIAS para no volver a llamar en caso de seleccionar el mismo
    var currDeptoPos: Int = -1
    var currProvinciaPos: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioRegistroPe2Subcomponent(NegocioRegistroPe2Module(this))
                .inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_pe_negocio_registro_2, container, false)
    }

    private fun configSpinners() {
        spinner_pe_negocio_registro_departamento.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        provinciaAdapter.clear()
                        distritoAdapter.clear()
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        if (isVisible) {
                            if (p2 != currDeptoPos) {
                                val it = deptoAdapter.getItem(p2)
                                if (it != null) {
                                    provinciaAdapter.clear()
                                    distritoAdapter.clear()
                                    presenter.getProvincias(it.codigo, currDeptoPos)
                                }
                            }
                        }
                    }
                }

        spinner_pe_negocio_registro_provincia.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        distritoAdapter.clear()
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        if (isVisible) {
                            if (p2 != currProvinciaPos) {
                                val it = provinciaAdapter.getItem(p2)
                                if (it != null) {
                                    distritoAdapter.clear()
                                    presenter.getDistritos(it.codigo, currProvinciaPos)
                                }
                            }
                        }
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        AndroidUtils.setText(til_pe_negocio_registro_celular.editText, containerAct.getPhone())
        til_pe_negocio_registro_celular.isEnabled = false

        spinner_pe_negocio_registro_departamento.adapter = deptoAdapter
        spinner_pe_negocio_registro_provincia.adapter = provinciaAdapter
        spinner_pe_negocio_registro_distrito.adapter = distritoAdapter
        configSpinners()
        buildDatePickerDialog()

        containerAct.getRetryView().findViewById<Button>(R.id.b_retry).setOnClickListener {
            presenter.getInitialData()
        }

        b_pe_negocio_registro_nacimiento.setOnClickListener {
            datePickerDialog.show()
        }

        form {
            inputLayout(R.id.til_pe_negocio_registro_direccion) {
                isNotEmpty().description(R.string.error_campo_generico)
            }
            inputLayout(R.id.til_pe_negocio_registro_email) {
                isEmail().description(R.string.error_email)
            }
            inputLayout(R.id.til_pe_negocio_registro_email_conf) {
                assert("Confirmacion email") { til ->
                    AndroidUtils.getText(til_pe_negocio_registro_email.editText) == AndroidUtils.getText(til.editText)
                }.description(R.string.error_conf_password)
            }
            inputLayout(R.id.til_pe_negocio_registro_password) {
                matches(McRegexPatterns.PASSWORD).description(R.string.error_password)
            }
            inputLayout(R.id.til_pe_negocio_registro_password_conf) {
                matches(McRegexPatterns.PASSWORD).description(R.string.error_conf_password)
                contains(AndroidUtils.getText(til_pe_negocio_registro_password.editText)).description(R.string.error_conf_password)
            }
            submitWith(R.id.b_pe_negocio_registro_2_guardar) {
                clickGuardar()
            }
        }

        if (deptoAdapter.count > 0 && provinciaAdapter.count > 0 && distritoAdapter.count > 0) {
            if (!datePickerDialog.isShowing) datePickerDialog.show()
        } else {
            presenter.getInitialData()
        }
    }

    override fun setInitialValues(
            deptos: List<DeptoEntity>,
            provincias: List<ProvinciaEntity>,
            distritos: List<DistritoEntity>
    ) {
        Timber.d("setInitialValues -> Deptos: %s", deptos.toString())
        Timber.d("setInitialValues -> Provincias: %s", provincias.toString())
        Timber.d("setInitialValues -> Distritos: %s", distritos.toString())

        deptoAdapter.addAll(deptos)
        provinciaAdapter.addAll(provincias)
        distritoAdapter.addAll(distritos)

        if (!datePickerDialog.isShowing) datePickerDialog.show()
    }

    override fun setDeptos(deptos: List<DeptoEntity>) {
        if (deptoAdapter.count > 0) deptoAdapter.clear()
        deptoAdapter.addAll(deptos)
        if (deptoAdapter.count > 0) {
            this.currDeptoPos = 0
            presenter.getProvincias(deptoAdapter.getItem(currDeptoPos)!!.codigo, currDeptoPos)
        }
    }

    override fun setProvincias(provincias: List<ProvinciaEntity>, currDeptoPos: Int) {
        if (provinciaAdapter.count > 0) provinciaAdapter.clear()
        provinciaAdapter.addAll(provincias)
        if (provinciaAdapter.count > 0) {
            this.currDeptoPos = currDeptoPos
            currProvinciaPos = 0
            presenter.getDistritos(
                    provinciaAdapter.getItem(currProvinciaPos)!!.codigo,
                    currProvinciaPos
            )
        }
    }

    override fun setDistritos(distritos: List<DistritoEntity>, currProvinciaPos: Int) {
        if (distritoAdapter.count > 0) distritoAdapter.clear()
        distritoAdapter.addAll(distritos)
        if (distritoAdapter.count > 0) {
            this.currProvinciaPos = currProvinciaPos
        }
    }

    override fun clickGuardar() {
        if (AndroidUtils.getText(b_pe_negocio_registro_nacimiento).isNullOrEmpty()) {
            showError(getString(R.string.error_fecha_nacimiento))
        } else {
            val data =
                    NegocioRegistroPE3FragmentData(
                            configData, dobFormatterApi.format(fechaNacimientoDueno),
                            AndroidUtils.getText(til_pe_negocio_registro_nacionalidad.editText),
                            AndroidUtils.getText(til_pe_negocio_registro_direccion.editText),
                            AndroidUtils.getSelectedItem(
                                    spinner_pe_negocio_registro_distrito,
                                    DistritoEntity::class.java
                            ).codigo,
                            AndroidUtils.getSelectedItem(
                                    spinner_pe_negocio_registro_provincia,
                                    ProvinciaEntity::class.java
                            ).codigo,
                            AndroidUtils.getSelectedItem(
                                    spinner_pe_negocio_registro_departamento,
                                    DeptoEntity::class.java
                            ).codigo,
                            AndroidUtils.getText(til_pe_negocio_registro_celular.editText),
                            AndroidUtils.getText(til_pe_negocio_registro_email.editText),
                            AndroidUtils.getText(til_pe_negocio_registro_password.editText)
                    )

            (activity as NegocioRegistroPeActivity).fragmentManagerLazy.commit {
                add(R.id.frame_pe_negocio_registro, NegocioRegistroPe3Fragment.get(data))
                hide(this@NegocioRegistroPe2Fragment)
                addToBackStack(null)
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            }
        }
    }

    override fun showProgress() {
        containerAct.showProgress()
    }

    override fun hideProgress() {
        containerAct.hideProgress()
    }

    override fun showError(msg: String) {
        containerAct.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerAct.showSuccess(msg)
    }

    override fun showRetry() {
        containerAct.showRetry()
    }

    override fun hideRetry() {
        containerAct.hideRetry()
    }

    private fun buildDatePickerDialog() {
        val listener = DatePickerDialog.OnDateSetListener { _, i, i1, i2 ->
            val time = LocalDateTime.of(i, i1 + 1, i2, 0, 0)
            fechaNacimientoDueno = time
            b_pe_negocio_registro_nacimiento.text = dobFormatterView.format(fechaNacimientoDueno)

        }

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            datePickerDialog = FixedHoloDatePickerDialog(
                    activity, listener,
                    Calendar.getInstance().get(Calendar.YEAR) - 18,
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            )
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog =
                        DatePickerDialog(
                                activity!!, R.style.CustomDatePickerDialogTheme, listener,
                                Calendar.getInstance().get(Calendar.YEAR) - 18,
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                        )
            } else {
                datePickerDialog =
                        DatePickerDialog(
                                activity!!, listener, Calendar.getInstance().get(Calendar.YEAR) - 18,
                                Calendar.getInstance().get(Calendar.MONTH),
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                        )
                datePickerDialog.datePicker.calendarViewShown = false
            }
        }
        datePickerDialog.setTitle(R.string.txt_mobilecard_add_dob)
    }
}