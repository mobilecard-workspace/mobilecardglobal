package addcel.mobilecard.ui.negocio.mx.cobro.scan;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 13/09/18.
 */
@PerFragment
@Subcomponent(modules = NegocioCobroQrScanModule.class)
public interface NegocioCobroQrScanSubcomponent {
    void inject(NegocioCobroQrScanFragment fragment);
}
