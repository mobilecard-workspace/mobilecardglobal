package addcel.mobilecard.ui.negocio.mx.wallet.registro;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 12/11/18.
 */
@PerFragment
@Subcomponent(modules = NegocioWalletRegistroModule.class)
public interface NegocioWalletRegistroSubcomponent {
    void inject(NegocioWalletRegistroFragment fragment);
}
