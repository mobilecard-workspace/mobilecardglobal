package addcel.mobilecard.ui.negocio.mx.cobro.menu;

import androidx.annotation.Nullable;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 31/08/18.
 */
public interface NegocioCobroMenuContract {
    enum TipoCobro {
        TDC(R.string.s_menu_negocio_tarjeta_presente), QR_NEGOCIO(
                R.string.s_menu_negocio_genera_qr), QR_CLIENTE(R.string.s_menu_negocio_escanea_qr);

        private final int desc;

        TipoCobro(int desc) {
            this.desc = desc;
        }

        public int getDesc() {
            return desc;
        }
    }

    interface View extends ScreenView {
        void launchCreateFragment();

        void launchScanFragment();

        void launchCardFragment();

        void launchWalletActivity(@Nullable CardEntity card);

        void launchSmsActivity(ValidationEntity entity);

        void launchProfile();

        void launchQr();

        String getNegocioName();

        void showActivationDialog();

        void dismissActivationDialog();

        void onItemClicked(int pos);
    }

    interface Presenter {
        void validate(TipoCobro tipoCobro);

        NegocioEntity getNegocio();

        String getComercioName();

        void resendActivation();

        boolean hasIdAccount();
    }
}
