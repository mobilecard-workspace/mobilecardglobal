package addcel.mobilecard.ui.negocio.pe.registro.persona

/**
 * ADDCEL on 2019-07-09.
 */
import addcel.mobilecard.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

enum class PePersonaFiscal(val icon: Int, val titleRes: Int, val subRes: Int) {
    JURIDICA(
            R.drawable.persona, R.string.s_registro_negocio_juridica,
            R.string.s_registro_negocio_juridica_sub
    ),
    NATURAL(
            R.drawable.persona, R.string.s_registro_negocio_natural,
            R.string.s_registro_negocio_natural_sub
    )
}

class NegocioPeRegistroPersonaAdapter :
        RecyclerView.Adapter<NegocioPeRegistroPersonaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.item_peru_registro_menu, parent,
                        false
                )
        )
    }

    fun getItem(position: Int): PePersonaFiscal {
        return PePersonaFiscal.values()[position]
    }

    override fun getItemCount(): Int {
        return PePersonaFiscal.values().size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val tipo = PePersonaFiscal.values()[position]

        holder.icon.setCompoundDrawablesRelativeWithIntrinsicBounds(tipo.icon, 0, 0, 0)
        holder.icon.setText(tipo.titleRes)
        holder.subTitle.setText(tipo.subRes)
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val subTitle: TextView = v.findViewById(R.id.subtitle_negocio_pe_registro_menu)
        val icon: TextView = v.findViewById(R.id.b_negocio_pe_registro_menu)
    }
}