package addcel.mobilecard.ui.negocio.mx.wallet.update.account

import addcel.mobilecard.R
import addcel.mobilecard.ui.negocio.mx.wallet.update.account.form.NegocioMxWalletAccountFormFragment
import addcel.mobilecard.ui.negocio.mx.wallet.update.account.form.NegocioMxWalletAccountFormModel
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_wallet_account.*

/**
 * ADDCEL on 2019-08-16.
 */

@Parcelize
data class NegocioMxWalletAccountModel(val establecimientoId: Int) : Parcelable

class NegocioMxWalletAccountFragment : Fragment() {

    companion object {
        fun get(model: NegocioMxWalletAccountModel): NegocioMxWalletAccountFragment {

            val args = BundleBuilder().putParcelable("model", model).build()
            val frag = NegocioMxWalletAccountFragment()
            frag.arguments = args
            return frag
        }
    }

    lateinit var model: NegocioMxWalletAccountModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_negocio_wallet_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        b_negocio_wallet_account_add.setOnClickListener {
            fragmentManager?.commit {
                replace(
                        R.id.frame_tebca,
                        NegocioMxWalletAccountFormFragment.get(
                                NegocioMxWalletAccountFormModel(
                                        model.establecimientoId,
                                        null
                                )
                        )
                )
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }
    }
}