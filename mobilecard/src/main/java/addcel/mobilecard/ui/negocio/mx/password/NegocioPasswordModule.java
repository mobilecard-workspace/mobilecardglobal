package addcel.mobilecard.ui.negocio.mx.password;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.di.scope.PerActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 13/09/18.
 */
@Module
public final class NegocioPasswordModule {
    private final NegocioPasswordActivity activity;

    NegocioPasswordModule(NegocioPasswordActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    NegocioEntity provideNegocio() {
        return Objects.requireNonNull(activity.getIntent().getExtras()).getParcelable("negocio");
    }

    @PerActivity
    @Provides
    boolean provideLoggedIn() {
        return Objects.requireNonNull(activity.getIntent().getExtras()).getBoolean("loggedIn");
    }

    @PerActivity
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(activity);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(activity);
        return validator;
    }

    @PerActivity
    @Provides
    NegocioAPI provideApi(Retrofit retrofit) {
        return retrofit.create(NegocioAPI.class);
    }

    @PerActivity
    @Provides
    NegocioPasswordContract.Presenter providePresenter(NegocioAPI api,
                                                       SessionOperations session, NegocioEntity newNegocio) {
        return new NegocioPasswordPresenter(api, session, newNegocio, activity);
    }
}
