package addcel.mobilecard.ui.negocio.mx.historial;

import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.domain.negocio.historial.NegocioHistorialInteractor;
import addcel.mobilecard.domain.negocio.historial.NegocioHistorialInteractorImpl;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 11/14/18.
 */
@Module
public final class NegocioHistorialModule {
    private final NegocioHistorialActivity activity;

    NegocioHistorialModule(NegocioHistorialActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    @Named("idPais")
    int provideIdPais() {
        return activity.getIntent().getIntExtra("idPais", 1);
    }

    @PerActivity
    @Provides
    NegocioEntity provideNegocio(SessionOperations session) {
        return session.getNegocio();
    }

    @PerActivity
    @Provides
    @Named("customCurrFormat")
    NumberFormat provideFormat(
            @Named("idPais") int idPais) {

        switch (idPais) {
            case 1:
                return NumberFormat.getCurrencyInstance(new Locale("es", "MX"));
            case 2:
                return NumberFormat.getCurrencyInstance(new Locale("es", "CO"));
            case 4:
                return NumberFormat.getCurrencyInstance(new Locale("es", "PE"));
            default:
                return NumberFormat.getCurrencyInstance(Locale.US);
        }
    }

    @PerActivity
    @Provides
    NegocioHistorialAdapter provideAdapter(
            @Named("customCurrFormat") NumberFormat format) {
        return new NegocioHistorialAdapter(format);
    }

    @PerActivity
    @Provides
    ScanPayService provideService(Retrofit retrofit) {
        return retrofit.create(ScanPayService.class);
    }

    @PerActivity
    @Provides
    NegocioHistorialInteractor provideInteractor(ScanPayService service,
                                                 NegocioEntity negocioEntity) {
        return new NegocioHistorialInteractorImpl(service, negocioEntity);
    }

    @PerActivity
    @Provides
    NegocioHistorialContract.Presenter providePresenter(
            NegocioHistorialInteractor interactor) {
        return new NegocioHistorialPresenter(interactor, activity);
    }
}
