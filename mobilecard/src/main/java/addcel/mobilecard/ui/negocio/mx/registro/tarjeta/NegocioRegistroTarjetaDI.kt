package addcel.mobilecard.ui.negocio.mx.registro.tarjeta

import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.di.scope.PerFragment
import android.widget.ArrayAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-12-06.
 */

@Module
class NegocioRegistroTarjetaModule(val fragment: NegocioRegistroTarjetaFragment) {

    @PerFragment
    @Provides
    fun provideViewModel(): NegocioRegistroTarjetaModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @PerFragment
    @Provides
    fun provideAdapter(): ArrayAdapter<EstadoResponse.EstadoEntity> {
        val adapter = ArrayAdapter<EstadoResponse.EstadoEntity>(
                fragment.context!!,
                android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }

    @PerFragment
    @Provides
    fun provideNegocioApi(r: Retrofit): NegocioAPI {
        return NegocioAPI.create(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            api: NegocioAPI,
            catalogoService: CatalogoService, session: SessionOperations, state: StateSession
    ): NegocioRegistroTarjetaPresenter {
        return NegocioRegistroTarjetaPresenterImpl(
                api,
                catalogoService,
                session, state,
                CompositeDisposable(),
                fragment
        )
    }
}

@PerFragment
@Subcomponent(modules = [NegocioRegistroTarjetaModule::class])
interface NegocioRegistroTarjetaSubcomponent {
    fun inject(fragment: NegocioRegistroTarjetaFragment)
}