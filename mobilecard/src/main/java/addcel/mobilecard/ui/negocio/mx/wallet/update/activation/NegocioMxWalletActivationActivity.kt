package addcel.mobilecard.ui.negocio.mx.wallet.update.activation

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.McRegexPatterns
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.negocio.mx.wallet.NegocioWalletActivity
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.vvalidator.form
import com.google.common.base.Strings
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_mx_previvale_activate.*
import mx.mobilecard.crypto.AddcelCrypto
import javax.inject.Inject

@Parcelize
data class NegocioMxWalletActivationModel(
        val idComercio: Int
) : Parcelable

interface NegocioMxWalletActivationCallback {
    fun onMxWalletActivationResult(resultCode: Int, data: Intent?)
}

interface NegocioMxWalletActivationView : ScreenView,
        TextWatcher {

    fun configRetry()

    fun configPanTil()

    fun configEstadoSpinner()

    fun launchEstados()

    fun showRetry()

    fun hideRetry()

    fun clickRetry()

    fun setEstados(estados: List<EstadoResponse.EstadoEntity>)

    fun clickCurp()

    fun clickActivate()

    fun onActivate(result: CardEntity)

    fun onActivate(result: CommerceAccountsResponse)
}

class NegocioMxWalletActivationActivity : AppCompatActivity(), NegocioMxWalletActivationView {

    override fun afterTextChanged(p0: Editable?) {
        val pan = Strings.nullToEmpty(p0.toString())
        if (pan.matches(McRegexPatterns.PREVIVALE.toRegex())) {
            AndroidUtils.getEditText(til_negocio_mx_previvale_activation_card).setTextColor(Color.BLACK)
        } else {
            AndroidUtils.getEditText(til_negocio_mx_previvale_activation_card).setTextColor(Color.RED)
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun configRetry() {
        retry_negocio_mx_previvale_activate.findViewById<Button>(R.id.b_retry).setOnClickListener {
            clickRetry()
        }
    }

    override fun configPanTil() {
        text_negocio_mx_previvale_activation_card.addTextChangedListener(this)
    }

    override fun configEstadoSpinner() {
        spinner_negocio_mx_previvale_activation_estado.adapter = adapter
    }


    override fun launchEstados() {
        if (adapter.count == 0)
            presenter.getEstados()
    }

    override fun showRetry() {
        form_negocio_mx_previvale_activate.isEnabled = false
        if (retry_negocio_mx_previvale_activate.visibility == View.GONE) retry_negocio_mx_previvale_activate.visibility =
                View.VISIBLE
    }

    override fun hideRetry() {
        form_negocio_mx_previvale_activate.isEnabled = true
        if (retry_negocio_mx_previvale_activate.visibility == View.VISIBLE) retry_negocio_mx_previvale_activate.visibility =
                View.GONE
    }

    override fun clickRetry() {
        launchEstados()
    }

    override fun setEstados(estados: List<EstadoResponse.EstadoEntity>) {
        adapter.clear()
        adapter.addAll(estados)
    }

    override fun clickCurp() {
        try {
            val myIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(CURP_URL)
            )
            startActivity(myIntent)
        } catch (e: ActivityNotFoundException) {
            showError(e.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.OPERATION))
            e.printStackTrace()
        }
    }

    override fun showProgress() {
        progress_negocio_mx_previvale_activation.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_negocio_mx_previvale_activation.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    companion object {
        const val REQUEST_CODE = 423
        const val DATA_CARD = "card"
        const val RESULT_DATA = "addcel.mobilecard.ui.negocio.mx.wallet.data"
        private const val CURP_URL = "https://www.gob.mx/curp/"

        fun get(context: Context, model: NegocioMxWalletActivationModel): Intent {
            return Intent(context, NegocioMxWalletActivationActivity::class.java).putExtra(
                    "model",
                    model
            )
        }
    }

    @Inject
    lateinit var model: NegocioMxWalletActivationModel
    @Inject
    lateinit var presenter: NegocioMxWalletActivationPresenter
    @Inject
    lateinit var adapter: ArrayAdapter<EstadoResponse.EstadoEntity>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioMxWalletActivationSubcomponent(
                NegocioMxWalletActivationModule(this)
        ).inject(this)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.slide_out_right)
        setContentView(R.layout.screen_negocio_mx_previvale_activate)

        configRetry()
        configPanTil()
        configEstadoSpinner()

        b_negocio_mx_previvale_activation_curp.setOnClickListener {
            clickCurp()
        }

        form {
            inputLayout(R.id.til_negocio_mx_previvale_activation_card) {
                matches(McRegexPatterns.PREVIVALE).description(R.string.error_tarjeta)
            }
            inputLayout(R.id.til_negocio_mx_previvale_activation_direccion) {
                isNotEmpty().description(R.string.error_campo_generico)
                length().atLeast(10).description(R.string.error_domicilio)
            }
            inputLayout(R.id.til_negocio_mx_previvale_activation_direccion) {
                isNotEmpty().description(R.string.error_campo_generico)
                length().atLeast(4).description(R.string.error_campo_generico)
            }
            inputLayout(R.id.til_negocio_mx_previvale_activation_ciudad) {
                isNotEmpty().description(R.string.error_campo_generico)
                length().atLeast(4).description(R.string.error_campo_generico)
            }
            inputLayout(R.id.til_negocio_mx_previvale_activation_cp) {
                isNotEmpty().description(R.string.error_campo_generico)
                length().atLeast(5).description(R.string.error_cp)
            }
            inputLayout(R.id.til_negocio_mx_previvale_activation_curp) {
                matches(McRegexPatterns.CURP).description(R.string.error_rfc_curp)
            }
            submitWith(R.id.b_negocio_mx_previvale_activation_activate) {
                clickActivate()
            }
        }

        launchEstados()
    }

    override fun clickActivate() {
        val direccion = AndroidUtils.getText(til_negocio_mx_previvale_activation_direccion.editText)
        val colonia = AndroidUtils.getText(til_negocio_mx_previvale_activation_colonia.editText)
        val ciudad = AndroidUtils.getText(til_negocio_mx_previvale_activation_ciudad.editText)
        val cp = AndroidUtils.getText(til_negocio_mx_previvale_activation_cp.editText)
        val estado =
                (spinner_negocio_mx_previvale_activation_estado.selectedItem as EstadoResponse.EstadoEntity).id
        val curp = AndroidUtils.getText(til_negocio_mx_previvale_activation_curp.editText)
        val pan = AndroidUtils.getText(til_negocio_mx_previvale_activation_card.editText)

        val request = PrevivaleUserRequest(
                model.idComercio.toLong(),
                "",
                "",
                "",
                direccion,
                colonia,
                ciudad,
                cp,
                estado,
                StringUtil.generarRFC(presenter.getFullName()),
                curp,
                BuildConfig.ADDCEL_APP_ID,
                AddcelCrypto.encryptHard(pan)
        )

        presenter.activate(request)
    }

    override fun onActivate(result: CardEntity) {
        val intent = Intent()
        intent.putExtra(NegocioWalletActivity.RESULT_DATA, model)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onActivate(result: CommerceAccountsResponse) {
        if (result.idError != 0) {
            showError(result.mensajeError)
        } else {
            Handler().postDelayed({
                val intent = Intent().putExtra(DATA_CARD, result)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }, 200)
        }
    }
}
