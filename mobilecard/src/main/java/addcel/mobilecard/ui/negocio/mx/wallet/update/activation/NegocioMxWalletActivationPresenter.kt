package addcel.mobilecard.ui.negocio.mx.wallet.update.activation

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-08-29.
 */
interface NegocioMxWalletActivationPresenter {
    fun addToDisposables(disposable: Disposable)
    fun clearDisposables()
    fun getEstados()
    fun getFullName(): String
    fun activate(request: PrevivaleUserRequest)
}

class NegocioMxWalletActivationPresenterImpl(
        val wallet: WalletAPI,
        val catalogo: CatalogoService,
        val session: SessionOperations,
        val compositeDisposable: CompositeDisposable = CompositeDisposable(),
        val view: NegocioMxWalletActivationView
) : NegocioMxWalletActivationPresenter {
    override fun addToDisposables(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun getEstados() {
        view.showProgress()
        val eDisp = catalogo.getEstados(
                BuildConfig.ADDCEL_APP_ID,
                PaisResponse.PaisEntity.MX,
                StringUtil.getCurrentLanguage()
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) {
                        view.hideRetry()
                        view.setEstados(it.estados)
                    } else {
                        view.showRetry()
                        view.showError(it.mensajeError)
                    }
                },
                        {
                            view.showRetry()
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        })
        addToDisposables(eDisp)
    }

    override fun getFullName(): String {
        return session.negocio.representanteLegal
    }

    override fun activate(request: PrevivaleUserRequest) {
        view.showProgress()
        val aDisp = wallet.getPrevivaleCard(
                BuildConfig.ADDCEL_APP_ID,
                PaisResponse.PaisEntity.MX,
                StringUtil.getCurrentLanguage(),
                WalletAPI.NEGOCIO,
                request.idUsuario,
                request
        ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    if (it.idError == 0) view.onActivate(it.card)
                    else
                        view.showError(it.mensajeError)
                }, {
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })

        addToDisposables(aDisp)
    }
}