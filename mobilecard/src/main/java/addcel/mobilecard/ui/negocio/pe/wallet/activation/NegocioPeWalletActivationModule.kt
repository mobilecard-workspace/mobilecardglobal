package addcel.mobilecard.ui.negocio.pe.wallet.activation

import addcel.mobilecard.data.net.tebca.CommerceAccountsAPI
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.domain.peru.NegocioPeWalletActivationInteractor
import addcel.mobilecard.domain.peru.NegocioPeWalletActivationInteractorImpl
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-08-30.
 */
@Module
class NegocioPeWalletActivationModule(
        private val activity: NegocioPeWalletActivationActivity
) {
    @PerActivity
    @Provides
    fun provideModel(): NegocioPeWalletActivationModel {
        return activity.intent.getParcelableExtra("model")!!
    }

    @PerActivity
    @Provides
    fun provideApi(retrofit: Retrofit): CommerceAccountsAPI {
        return CommerceAccountsAPI.get(retrofit)
    }

    @PerActivity
    @Provides
    fun provideInteractor(
            api: CommerceAccountsAPI,
            model: NegocioPeWalletActivationModel
    ): NegocioPeWalletActivationInteractor {
        return NegocioPeWalletActivationInteractorImpl(api, CompositeDisposable())
    }

    @PerActivity
    @Provides
    fun providePresenter(
            interactor: NegocioPeWalletActivationInteractor
    ): NegocioPeWalletActivationPresenter {
        return NegocioPeWalletActivationPresenterImpl(interactor, activity)
    }
}

@PerActivity
@Subcomponent(modules = [NegocioPeWalletActivationModule::class])
interface NegocioPeWalletActivationSubcomponent {
    fun inject(activity: NegocioPeWalletActivationActivity)
}