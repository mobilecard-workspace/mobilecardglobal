package addcel.mobilecard.ui.negocio.pe.generaqr.concepto

import addcel.mobilecard.R
import addcel.mobilecard.ui.negocio.pe.generaqr.NegocioPeGeneraQrActivity
import addcel.mobilecard.ui.negocio.pe.generaqr.qr.NegocioPeGeneraQrQRFragment
import addcel.mobilecard.ui.negocio.pe.generaqr.qr.NegocioPeGeneraQrQRFragmentData
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_pe_tarjeta_presente_concepto.*

/**
 * ADDCEL on 2019-07-13.
 */

@Parcelize
data class NegocioPeGeneraQrConceptoData(val monto: String) : Parcelable

class NegocioPeGeneraQrConceptoFragment : Fragment() {
    companion object {
        fun get(configData: NegocioPeGeneraQrConceptoData): NegocioPeGeneraQrConceptoFragment {
            val args = BundleBuilder().putParcelable("data", configData).build()
            val fragment = NegocioPeGeneraQrConceptoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var configData: NegocioPeGeneraQrConceptoData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configData = arguments?.getParcelable("data")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.screen_negocio_pe_tarjeta_presente_concepto,
                container,
                false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        label_negocio_pe_tarjeta_presente_propina.visibility = View.VISIBLE
        text_negocio_pe_tarjeta_presente_propina.visibility = View.VISIBLE
        b_negocio_pe_tarjeta_presente_continuar.setText(R.string.txt_generar_qr)

        b_negocio_pe_tarjeta_presente_continuar.setOnClickListener {
            val concepto = AndroidUtils.getText(text_negocio_pe_tarjeta_presente_concepto)
            val propina = AndroidUtils.getText(text_negocio_pe_tarjeta_presente_propina)
            if (concepto.isNotEmpty()) {
                val confData = NegocioPeGeneraQrQRFragmentData(configData.monto, concepto, propina)
                (activity as NegocioPeGeneraQrActivity).fragmentManagerLazy.transaction {
                    add(
                            R.id.frame_negocio_negocio_pe_genera_qr,
                            NegocioPeGeneraQrQRFragment.get(confData)
                    )
                    hide(this@NegocioPeGeneraQrConceptoFragment)
                    addToBackStack(null)
                    setCustomAnimations(
                            android.R.anim.slide_in_left,
                            android.R.anim.slide_out_right
                    )
                }
            } else {
                (activity as NegocioPeGeneraQrActivity).showError(getString(R.string.error_campo_generico))
            }
        }
    }
}