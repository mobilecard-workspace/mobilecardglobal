package addcel.mobilecard.ui.negocio.pe.wallet

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.tebca.Cci
import addcel.mobilecard.data.net.tebca.CommerceAccountsAPI
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.data.net.tebca.TebcaCard
import addcel.mobilecard.domain.usuario.bottom.MenuEvent
import addcel.mobilecard.domain.usuario.bottom.UseCase
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.cvv.MobilecardCvvTimerActivity
import addcel.mobilecard.ui.cvv.MobilecardCvvTimerModel
import addcel.mobilecard.ui.negocio.mx.historial.NegocioHistorialActivity
import addcel.mobilecard.ui.negocio.pe.menu.NegocioMenuPeActivity
import addcel.mobilecard.ui.negocio.pe.registro.persona.PePersonaFiscal
import addcel.mobilecard.ui.negocio.pe.wallet.account.NegocioPeWalletAccountFragment
import addcel.mobilecard.ui.negocio.pe.wallet.account.NegocioPeWalletAccountModel
import addcel.mobilecard.ui.negocio.pe.wallet.account.display.NegocioPeWalletAccountDisplayFragment
import addcel.mobilecard.ui.negocio.pe.wallet.account.display.NegocioPeWalletAccountDisplayModel
import addcel.mobilecard.ui.negocio.pe.wallet.activation.NegocioPeWalletActivationActivity
import addcel.mobilecard.ui.negocio.pe.wallet.activation.NegocioPeWalletActivationCallback
import addcel.mobilecard.ui.negocio.pe.wallet.activation.NegocioPeWalletActivationModel
import addcel.mobilecard.ui.usuario.menu.BottomMenuEventListener
import addcel.mobilecard.ui.usuario.menu.MenuLayout
import addcel.mobilecard.utils.CardLoadingUtils
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.transaction
import com.squareup.otto.Bus
import com.squareup.otto.Subscribe
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_negocio_pe_wallet.*
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber
import java.util.*
import javax.inject.Inject

@Parcelize
data class NegocioPeWalletModel(val idEstablecimiento: Int, val tipoPersona: String) :
        Parcelable

data class CommerceAccountEvent(val accounts: CommerceAccountsResponse)

data class ProgressEvent(val showProgress: Boolean)

interface NegocioPeWalletView : ScreenView, NegocioPeWalletActivationCallback,
        BottomMenuEventListener {

    fun postCommerceAccountEvent(event: CommerceAccountEvent)

    fun postProgressEvent(event: ProgressEvent)

    fun onAccount(cci: Cci)

    fun onAccountNull()

    fun onTebcaAccount(card: TebcaCard)

    fun onTebcaAccountNull()

    fun builcActivatedCard(card: TebcaCard)

    fun clickActivate()

    fun clickRequest()

    fun clickBlock()

    fun clickReplace()

    fun clickRetry()

    fun clickFav(type: Int)

    fun loadBmp(card: TebcaCard)

    fun hideRetry()

    fun showRetry()

    fun configRetry()

    fun showBlockDialog(card: TebcaCard)

    fun showBlockWarningDialog()

    fun showTebcaActivatedOptionsDialog()

    fun showTebcaActivateDialog()

    fun launchActivate()

    fun getFavResource(fav: Boolean): Int

    @Throws(Throwable::class)
    fun flipCard()
}

class NegocioPeWalletActivity : AppCompatActivity(), NegocioPeWalletView {

    override fun configRetry() {
        screen_retry.findViewById<Button>(R.id.b_retry).setOnClickListener {
            clickRetry()
        }
    }

    override fun hideRetry() {
        screen_retry.visibility = View.GONE
    }

    override fun clickRetry() {
        presenter.getAccountInfo()
    }

    override fun clickFav(type: Int) {
        presenter.favCard(type)
    }

    override fun showRetry() {
        screen_retry.visibility = View.VISIBLE
    }

    @Subscribe
    override fun postCommerceAccountEvent(event: CommerceAccountEvent) {
        val result = event.accounts
        presenter.processResult(result)
    }

    @Subscribe
    override fun postProgressEvent(event: ProgressEvent) {
        if (event.showProgress) showProgress() else hideProgress()
    }

    override fun onAccount(cci: Cci) {
        personalAccount = cci

        b_cci_fav.visibility = View.VISIBLE
        b_cci_fav.setImageResource(getFavResource(cci.favorito))

        supportFragmentManager.transaction {
            add(
                    R.id.frame_tebca, NegocioPeWalletAccountDisplayFragment.get(
                    NegocioPeWalletAccountDisplayModel(
                            personalAccount!!,
                            model.idEstablecimiento
                    )
            )
            ).setCustomAnimations(
                    android.R.anim.fade_in,
                    android.R.anim.slide_out_right
            )
        }
    }

    override fun onAccountNull() {
        personalAccount = null

        b_cci_fav.visibility = View.GONE

        supportFragmentManager.transaction {
            add(
                    R.id.frame_tebca, NegocioPeWalletAccountFragment.get(
                    NegocioPeWalletAccountModel(model.idEstablecimiento)
            )
            ).setCustomAnimations(
                    android.R.anim.fade_in, android.R.anim.slide_out_right
            )
        }
    }

    override fun onTebcaAccount(card: TebcaCard) {
        tebcaCard = card
        updateUiOnTebcaCard(card)
        builcActivatedCard(card)
    }

    override fun onTebcaAccountNull() {
        tebcaCard = null
        updateUiOnTebcaCard(null)
    }

    override fun builcActivatedCard(card: TebcaCard) {
        card_tebca_negocio_activated.isEditable = false
        card_tebca_negocio_activated.background =
                ContextCompat.getDrawable(this, R.drawable.full_mobilecard_h)
        card_tebca_negocio_activated.cardName = card.nombre!!
        card_tebca_negocio_activated.cardNameTextColor = resources.getColor(R.color.colorBackground)
        card_tebca_negocio_activated.cardNumber = AddcelCrypto.decryptHard(card.pan!!)
        card_tebca_negocio_activated.cardNumberTextColor =
                resources.getColor(R.color.colorBackground)
        card_tebca_negocio_activated.expiryDate = AddcelCrypto.decryptHard(card.vigencia!!)
        card_tebca_negocio_activated.expiryDateTextColor =
                resources.getColor(R.color.colorBackground)
        try {
            card_tebca_negocio_activated.findViewById<EditText>(R.id.cvv_et)
                    .setText(AddcelCrypto.decryptHard(card.codigo!!))

            card_tebca_negocio_activated.findViewById<View>(R.id.card_logo)
                    .setBackgroundResource(R.drawable.logo_visa)
            card_tebca_negocio_activated.invalidate()
            card_tebca_negocio_activated.requestLayout() // setVisibility(View.INVISIBLE);
        } catch (t: Throwable) {
            t.printStackTrace()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) card_tebca_negocio_activated.elevation =
                4.0.toFloat()
        card_tebca_negocio_activated.putChip(false)

        loadBmp(card)
    }

    override fun loadBmp(card: TebcaCard) {

        val cleanCard = AddcelCrypto.decryptHard(card.pan)

        picasso.load(card.imgFull!!)
                .error(CardLoadingUtils.getCardImg(cleanCard, CardLoadingUtils.FULL))
                .placeholder(CardLoadingUtils.getCardImg(cleanCard, CardLoadingUtils.SHORT))
                .into(object : Target {
                    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                        card_tebca_negocio_activated.background =
                                BitmapDrawable(card_tebca_negocio_activated.resources, bitmap)
                    }

                    override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {
                        e.printStackTrace()
                        card_tebca_negocio_activated.background = errorDrawable
                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable) {
                        card_tebca_negocio_activated.background = placeHolderDrawable
                    }
                })
    }

    override fun showProgress() {
        progress_negocio_peru_wallet.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_negocio_peru_wallet.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    companion object {
        private val localePer = Locale("es", "PE")

        fun get(context: Context, model: NegocioPeWalletModel): Intent {
            return Intent(context, NegocioPeWalletActivity::class.java).putExtra("model", model)
        }
    }

    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var model: NegocioPeWalletModel
    @Inject
    lateinit var presenter: NegocioPeWalletPresenter
    @Inject
    lateinit var bus: Bus
    @Inject
    lateinit var picasso: Picasso

    lateinit var activatedOptionsDialog: AlertDialog
    lateinit var notActivatedDialog: AlertDialog

    private var tebcaCard: TebcaCard? = null
    private var personalAccount: Cci? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioPeWalletSubcomponent(NegocioPeWalletModule(this))
                .inject(this)
        setContentView(R.layout.activity_negocio_pe_wallet)

        card_tebca_negocio.setOnClickListener {
            clickActivate()
        }

        b_tebca_fav.setOnClickListener { clickFav(CommerceAccountsAPI.TYPE_TEBCA) }

        b_cci_fav.setOnClickListener { clickFav(CommerceAccountsAPI.TYPE_CCI) }

        card_dot_indicator.setOnClickListener { clickActivate() }

        b_tebca_digital_solicitar.setOnClickListener {
            clickRequest()
        }


        b_tebca_bloquear.setOnClickListener {
            clickBlock()
        }

        b_tebca_reposicion.setOnClickListener {
            clickReplace()
        }

        card_tebca_negocio_activated.setOnClickListener { showTebcaActivatedOptionsDialog() }

        card_activated_dot_indicator.setOnClickListener { showTebcaActivatedOptionsDialog() }


        configRetry()
        configBottomLayout()
        updateUiOnTipoPersona(model.tipoPersona)
        presenter.getAccountInfo()
    }

    override fun onResume() {
        super.onResume()
        bus.register(this)
    }

    override fun onPause() {
        bus.unregister(this)
        super.onPause()
    }

    private fun configBottomLayout() {
        val favs: ImageButton = menu_negocio_peru_wallet.findViewById(R.id.b_menu_frecuentes)
        favs.visibility = View.GONE
    }

    private fun updateUiOnTipoPersona(tipo: String) {
        val tipoCompare = PePersonaFiscal.JURIDICA.name.toLowerCase(localePer)
        val visibility = if (tipo == tipoCompare) View.GONE else View.VISIBLE

        wallet_tebca_layout.visibility = visibility
        divider_tebca.visibility = visibility
        b_cci_fav.visibility = visibility
        b_cci_fav.isEnabled = tipo != tipoCompare

        if (wallet_tebca_layout.visibility == View.VISIBLE) updateUiOnTebcaCard(null)
    }

    override fun clickActivate() {
        showTebcaActivateDialog()
    }

    override fun clickRequest() {
        presenter.requestCard()
    }

    override fun clickBlock() {
        if (tebcaCard != null) showBlockDialog(tebcaCard!!)
    }

    override fun clickReplace() {
        if (tebcaCard != null) {
            if (presenter.evalIfBlocked(tebcaCard!!.estatus)) presenter.replaceCard()
            else showBlockWarningDialog()
        }
    }

    override fun showBlockDialog(card: TebcaCard) {
        AlertDialog.Builder(this).setTitle("Aviso")
                .setMessage(presenter.getBlockMessage(card.estatus))
                .setPositiveButton("Aceptar") { _, _ -> presenter.blockCard(card.estatus) }
                .setNegativeButton("Cancelar") { d, _ -> d.dismiss() }.show()
    }

    override fun showBlockWarningDialog() {
        AlertDialog.Builder(this).setTitle("Aviso")
                .setMessage("Debes bloquear tu cuenta antes de solicitar una reposición")
                .setPositiveButton("Aceptar") { d, _ -> d.dismiss() }.show()
    }

    private fun updateUiOnTebcaCard(tebca: TebcaCard?) {
        if (tebca == null) {
            label_tebca_fisica.visibility = View.VISIBLE
            label_tebca_digital.visibility = View.VISIBLE
            b_tebca_digital_solicitar.visibility = View.VISIBLE
            card_tebca_negocio.visibility = View.VISIBLE
            card_dot_indicator.visibility = View.VISIBLE
            card_tebca_negocio_activated.visibility = View.GONE
            card_activated_dot_indicator.visibility = View.GONE
            b_tebca_fav.visibility = View.GONE
            label_tebca_saldo.visibility = View.GONE
            view_tebca_saldo.visibility = View.GONE
            b_tebca_bloquear.visibility = View.GONE
            b_tebca_reposicion.visibility = View.GONE
        } else {
            label_tebca_fisica.visibility = View.GONE
            label_tebca_digital.visibility = View.GONE
            b_tebca_digital_solicitar.visibility = View.GONE
            card_tebca_negocio.visibility = View.GONE
            card_dot_indicator.visibility = View.GONE
            card_tebca_negocio_activated.visibility = View.VISIBLE
            b_tebca_fav.visibility = View.VISIBLE
            b_tebca_fav.setImageResource(getFavResource(tebca.favorito))
            card_activated_dot_indicator.visibility = View.VISIBLE
            label_tebca_saldo.visibility = View.VISIBLE
            view_tebca_saldo.visibility = View.VISIBLE
            view_tebca_saldo.text = presenter.formatSaldo(tebca.balance)
            b_tebca_bloquear.visibility = View.VISIBLE
            b_tebca_bloquear.text = if (tebca.estatus == 1) "BLOQUEAR" else "DESBLOQUEAR"
            b_tebca_reposicion.visibility = View.VISIBLE
        }
    }

    override fun showTebcaActivateDialog() {
        if (!::notActivatedDialog.isInitialized) {
            notActivatedDialog =
                    AlertDialog.Builder(this)
                            .setMessage("¿Deseas activar tu tarjeta física MobileCard?")
                            .setPositiveButton("Sí") { _, _ -> launchActivate() }
                            .setNegativeButton("No") { d, _ -> d.dismiss() }.create()
        }

        if (!notActivatedDialog.isShowing) notActivatedDialog.show()
    }

    override fun showTebcaActivatedOptionsDialog() {
        if (!::activatedOptionsDialog.isInitialized) {
            activatedOptionsDialog = AlertDialog.Builder(this).setTitle("Mi MobileCard")
                    .setSingleChoiceItems(
                            resources.getTextArray(R.array.tebca_options_array),
                            0
                    ) { p0, p1 ->
                        if (p1 == 0) {
                            clickFav(CommerceAccountsAPI.TYPE_TEBCA)
                        } else if (p1 == 1) {
                            try {
                                flipCard() //card_tebca_negocio_activated.security
                            } catch (t: Throwable) {
                                t.printStackTrace()
                            }
                        }
                        p0?.dismiss()
                    }.create()
        }

        if (!activatedOptionsDialog.isShowing) activatedOptionsDialog.show()
    }

    override fun launchActivate() {
        startActivityForResult(
                NegocioPeWalletActivationActivity.get(
                        this@NegocioPeWalletActivity,
                        NegocioPeWalletActivationModel(model.idEstablecimiento)
                ),
                NegocioPeWalletActivationActivity.REQUEST_CODE
        )
    }

    override fun getFavResource(fav: Boolean): Int {
        return if (fav) R.drawable.ic_account_star else R.drawable.icon_fav_gray
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == NegocioPeWalletActivationActivity.REQUEST_CODE) {
            onPeWalletActivationResult(resultCode, data)
        }
    }

    override fun onPeWalletActivationResult(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val result: CommerceAccountsResponse? =
                        data.getParcelableExtra(NegocioPeWalletActivationActivity.DATA_CARD)
                if (result != null) {
                    presenter.processResult(result)
                }
            }
        }
    }

    @Subscribe
    override fun onMenuEventReceived(event: MenuEvent) {
        (menu_negocio_peru_wallet as MenuLayout).unblock()

        when (event.useCase) {
            UseCase.HOME -> {
                startActivity(
                        NegocioMenuPeActivity.get(this, true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                )
                finish()
            }
            UseCase.HISTORIAL -> startActivity(NegocioHistorialActivity.get(this, 4))
            else -> Timber.d("In progress")
        }
    }

    @Throws(Throwable::class)
    override fun flipCard() {
        if (tebcaCard != null) {
            val model = MobilecardCvvTimerModel(tebcaCard!!.codigo!!)
            startActivity(MobilecardCvvTimerActivity.get(this, model))
        }
    }
}
