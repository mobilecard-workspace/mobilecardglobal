package addcel.mobilecard.ui.negocio.pe.registro.third

import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.negocios.entity.BankEntity
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.domain.peru.PeruNegocioRegistroInteractor
import addcel.mobilecard.domain.peru.PeruNegocioRegistroInteractorImpl
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeActivity
import android.widget.ArrayAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-07-11.
 */

@PerFragment
@Subcomponent(modules = [NegocioRegistroPe3Module::class])
interface NegocioRegistroPe3Subcomponent {
    fun inject(fragment: NegocioRegistroPe3Fragment)
}

@Module
class NegocioRegistroPe3Module(val fragment: NegocioRegistroPe3Fragment) {

    @Provides
    @PerFragment
    fun provideContainerActivity(): NegocioRegistroPeActivity {
        return fragment.activity as NegocioRegistroPeActivity
    }

    @Provides
    @PerFragment
    fun provideData(): NegocioRegistroPE3FragmentData {
        return fragment.arguments?.getParcelable("data")!!
    }

    @Provides
    @PerFragment
    fun provideAdapter(
            containerActivity: NegocioRegistroPeActivity
    ): ArrayAdapter<BankEntity> {
        val adapter =
                ArrayAdapter<BankEntity>(containerActivity, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }

    @Provides
    @PerFragment
    fun provideApi(retrofit: Retrofit): NegocioAPI {
        return retrofit.create(NegocioAPI::class.java)
    }

    @Provides
    @PerFragment
    fun provideInteractor(api: NegocioAPI): PeruNegocioRegistroInteractor {
        return PeruNegocioRegistroInteractorImpl(api, CompositeDisposable())
    }

    @Provides
    @PerFragment
    fun providePresenter(
            interactor: PeruNegocioRegistroInteractor, session: SessionOperations, state: StateSession
    ): NegocioRegistroPe3Presenter {
        return NegocioRegistroPe3PresenterImpl(interactor, session, state, fragment)
    }
}