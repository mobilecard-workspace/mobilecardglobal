package addcel.mobilecard.ui.negocio.mx.cobro;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 10/09/18.
 */
@PerActivity
@Subcomponent(modules = NegocioCobroModule.class)
public interface NegocioCobroSubcomponent {
    void inject(NegocioCobroActivity activity);
}
