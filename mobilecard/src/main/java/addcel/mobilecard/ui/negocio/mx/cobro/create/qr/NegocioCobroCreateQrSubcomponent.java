package addcel.mobilecard.ui.negocio.mx.cobro.create.qr;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 07/09/18.
 */
@PerFragment
@Subcomponent(modules = NegocioCobroCreateQrModule.class)
public interface NegocioCobroCreateQrSubcomponent {
    void inject(NegocioCobroCreateQrFragment fragment);
}
