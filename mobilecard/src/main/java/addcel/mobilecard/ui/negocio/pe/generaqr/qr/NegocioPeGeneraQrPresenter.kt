package addcel.mobilecard.ui.negocio.pe.generaqr.qr

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.data.net.qr.QrRequest
import addcel.mobilecard.data.net.qr.VisaQrWalletAPI
import addcel.mobilecard.ui.negocio.pe.menu.NegocioMenuPeActivity
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.text.format.DateFormat
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

/**
 * ADDCEL on 2019-07-15.
 */
interface NegocioPeGeneraQrPresenter {

    fun clearDisposables()

    fun getComision(monto: Double): Double

    fun getIdEstablecimiento(): Int

    fun generaQR(monto: String, concepto: String, propina: String)
}

class NegocioPeGeneraQrPresenterImpl(
        val api: VisaQrWalletAPI, val negocio: NegocioEntity,
        val disposables: CompositeDisposable, val view: View
) : NegocioPeGeneraQrPresenter {
    override fun getIdEstablecimiento(): Int {
        return negocio.idEstablecimiento
    }

    override fun getComision(monto: Double): Double {
        Timber.d("Comision pct: %s", negocio.comisionPorcentaje.toString())
        Timber.d("Monto: %s", monto.toString())

        val comision = (monto * negocio.comisionPorcentaje)
        val impuesto = comision * NegocioMenuPeActivity.PCT_COMISION_IGV

        return 0.0 //comision + impuesto
    }

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun generaQR(monto: String, concepto: String, propina: String) {

        view.showProgress()

        val montoD = (monto.toDouble() / 100)

        val propinaD = if (propina.isEmpty()) 0.0 else propina.toDouble()

        val todayPlusTwo = Calendar.getInstance()
        todayPlusTwo.add(Calendar.DATE, 2)

        val body = QrRequest(
                montoD + propinaD, getComision(monto.toDouble() / 100), concepto,
                DateFormat.format("yyyy-MM-dd", todayPlusTwo).toString()
        )

        disposables.add(
                api.generateDynamicQr(
                        BuildConfig.ADDCEL_APP_ID, negocio.idPais,
                        StringUtil.getCurrentLanguage(), -1, negocio.idEstablecimiento, body
                ).subscribeOn(
                        Schedulers.io()
                ).observeOn(AndroidSchedulers.mainThread()).subscribe({ r ->
                    view.hideProgress()
                    view.onQrSuccess(r)
                }, { t ->
                    view.hideProgress()
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(t))
                })
        )
    }
}

