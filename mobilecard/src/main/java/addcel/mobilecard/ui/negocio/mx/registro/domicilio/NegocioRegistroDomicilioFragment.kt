package addcel.mobilecard.ui.negocio.mx.registro.domicilio

import addcel.mobilecard.McRegexPatterns
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.negocios.entity.BankEntity
import addcel.mobilecard.data.net.negocios.entity.NegocioCreateRequest
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroModel
import addcel.mobilecard.ui.negocio.mx.registro.NegocioRegistroActivity
import addcel.mobilecard.ui.negocio.mx.registro.tarjeta.NegocioRegistroTarjetaFragment
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.StringUtil
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.afollestad.vvalidator.field.FieldError
import com.afollestad.vvalidator.form
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_registro_domicilio.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-12-05.
 */


@Parcelize
data class NegocioRegistroDomicilioViewModel(val celular: String) : Parcelable


interface NegocioRegistroDomicilioView : ScreenView {
    fun configAccountCheck(view: View)
    fun configBancoSpinner(view: View)
    fun configMobileCardCheck(view: View)
    fun onHasAccountCheck(checked: Boolean)
    fun onHasMobileCardCheck(checked: Boolean)
    fun launchBancos()
    fun setBancos(bancos: List<BankEntity>)
    fun showTerminos(terminos: String)
    fun showPrivacidad(privacidad: String)
    fun launchRegistro()
    fun launchTarjeta()
    fun onRegistro(negocioEntity: NegocioEntity)
    fun login(negocioEntity: NegocioEntity)
    fun onLoginSuccessful()
    fun showRetry()
    fun hideRetry()
    fun clickRetry()
}

class NegocioRegistroDomicilioFragment : Fragment(), NegocioRegistroDomicilioView {

    companion object {
        fun get(celular: String): NegocioRegistroDomicilioFragment {
            val frag = NegocioRegistroDomicilioFragment()
            val viewModel = NegocioRegistroDomicilioViewModel(celular)
            val args = BundleBuilder().putParcelable("model", viewModel).build()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var presenter: NegocioRegistroDomicilioPresenter

    private lateinit var viewModel: NegocioRegistroDomicilioViewModel
    private lateinit var bancoAdapter: ArrayAdapter<BankEntity>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioRegistroDomicilioSubcomponent(
                NegocioRegistroDomicilioModule(this)
        ).inject(this)
        viewModel = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_negocio_registro_domicilio, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        form {
            inputLayout(R.id.til_negocio_registro_mx_nombrecomercio, name = "Establecimiento") {
                isNotEmpty().description(R.string.error_campo_generico)
            }
            inputLayout(R.id.til_negocio_registro_mx_email, name = "Email") {
                isEmail().description(R.string.error_email)
            }
            inputLayout(R.id.til_negocio_registro_mx_email_conf, name = "Email Conf") {
                assert("Confirmacion email") { til ->
                    AndroidUtils.getText(til_negocio_registro_mx_email.editText) == AndroidUtils.getText(til.editText)
                }.description(R.string.error_conf_password)
            }
            inputLayout(R.id.til_negocio_registro_mx_nombre, name = "Nombre") {
                isNotEmpty().description(R.string.error_campo_generico)
            }
            inputLayout(R.id.til_negocio_registro_mx_paterno, name = "Paterno") {
                isNotEmpty().description(R.string.error_campo_generico)
            }
            inputLayout(R.id.til_negocio_registro_mx_materno, name = "Materno") {
                isNotEmpty().description(R.string.error_campo_generico)
            }
            inputLayout(R.id.til_negocio_registro_mx_password, name = "Password") {
                matches(McRegexPatterns.PASSWORD).description(R.string.error_password)
            }
            inputLayout(R.id.til_negocio_registro_mx_password_conf, name = "Confirmacion Password") {
                matches(McRegexPatterns.PASSWORD).description(R.string.error_conf_password)
                val pass = AndroidUtils.getText(til_negocio_registro_mx_password.editText)
                contains(pass).description(R.string.error_conf_password)
            }
            inputLayout(R.id.til_negocio_registro_mx_banco_clabe, name = "CLABE interbancaria") {
                conditional({ switch_negocio_registro_mx_banco.isChecked }) {
                    isNotEmpty().description(R.string.error_beneficiario_add_clabe)
                    isNumber().description(R.string.error_beneficiario_add_clabe)
                    length().exactly(18).description(R.string.error_beneficiario_add_clabe)
                }
            }
            checkable(R.id.switch_negocio_registro_mx_terminos, name = "Terminos y condiciones") {
                isChecked().description(R.string.error_terms_conditions)
                onErrors { _, errors ->
                    val firstError: FieldError? = errors.firstOrNull()
                    if (firstError != null) showError(firstError.toString())
                }
            }
            submitWith(R.id.b_registro_negocio_mx_registrar) {
                if (switch_negocio_registro_mx_banco.isChecked)
                    launchRegistro()
                else if (switch_negocio_registro_mx_mobilecard.isChecked) {
                    launchTarjeta()
                }
            }
        }

        b_negocio_registro_mx_terminos.setOnClickListener {
            presenter.getTerminos()
        }

        b_negocio_registro_negocio_mx_privacidad.setOnClickListener {
            presenter.getPrivacidad()
        }
        configBancoSpinner(view)
        configAccountCheck(view)
        configMobileCardCheck(view)

        val retry =
                (activity as NegocioRegistroActivity).retry?.findViewById<Button>(R.id.b_retry)
        retry?.setOnClickListener {
            clickRetry()
        }
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun configAccountCheck(view: View) {
        switch_negocio_registro_mx_banco.setOnCheckedChangeListener { _, checked ->
            onHasAccountCheck(checked)
            switch_negocio_registro_mx_mobilecard.isChecked = !checked
            onHasMobileCardCheck(!checked)
        }
    }

    override fun configBancoSpinner(view: View) {
        bancoAdapter = ArrayAdapter(view.context, android.R.layout.simple_spinner_item)
        bancoAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        bancoAdapter.setNotifyOnChange(true)
        spinner_negocio_registro_mx_banco.adapter = bancoAdapter
    }

    override fun configMobileCardCheck(view: View) {
        switch_negocio_registro_mx_mobilecard.setOnCheckedChangeListener { _, checked ->
            onHasMobileCardCheck(checked)
            switch_negocio_registro_mx_banco.isChecked = !checked
            onHasAccountCheck(!checked)
        }
    }

    override fun onHasAccountCheck(checked: Boolean) {
        if (checked) {
            AndroidUtils.setText(til_negocio_registro_mx_banco_clabe.editText, "")
            layout_negocio_registro_mx_banco.visibility = View.VISIBLE
            if (bancoAdapter.count == 0) launchBancos()
            b_registro_negocio_mx_registrar.setText(R.string.txt_negocio_registro_fiscal_finalizar)
        } else {
            AndroidUtils.setText(til_negocio_registro_mx_banco_clabe.editText, "")
            layout_negocio_registro_mx_banco.visibility = View.GONE
        }
        b_registro_negocio_mx_registrar.isEnabled = true
    }

    override fun onHasMobileCardCheck(checked: Boolean) {
        val visibility = if (checked) View.VISIBLE else View.GONE
        if (checked) b_registro_negocio_mx_registrar.setText(R.string.txt_negocio_registro_info_continuar)
        caption_negocio_registro_mx_mobilecard.visibility = visibility
        b_registro_negocio_mx_registrar.isEnabled = true
    }

    override fun launchBancos() {
        presenter.getBancos()
    }

    override fun setBancos(bancos: List<BankEntity>) {
        bancoAdapter.clear()
        bancoAdapter.addAll(bancos)
    }

    override fun showTerminos(terminos: String) {
        val dialog = AlertDialog.Builder(activity!!).setTitle(R.string.txt_terms_and_conditions_title)
                .setMessage(terminos)
                .setPositiveButton("Acepto") { d, _ ->
                    switch_negocio_registro_mx_terminos.isChecked = true
                    d.dismiss()
                }
                .setNegativeButton("No acepto") { d, _ ->
                    switch_negocio_registro_mx_terminos.isChecked = false
                    d.dismiss()
                }.create()
        dialog.show()
    }

    override fun showPrivacidad(privacidad: String) {
        val dialog = AlertDialog.Builder(activity!!).setTitle(R.string.txt_registro_privacy)
                .setMessage(privacidad)
                .setPositiveButton("OK") { d, _ ->
                    d.dismiss()
                }.create()
        dialog.show()
    }

    override fun launchRegistro() {

        val cel = viewModel.celular
        val establecimiento = StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_negocio_registro_mx_nombrecomercio.editText))
        val email = AndroidUtils.getText(til_negocio_registro_mx_email.editText)
        val nombre = StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_negocio_registro_mx_nombre.editText))
        val paterno = StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_negocio_registro_mx_paterno.editText))
        val materno = StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_negocio_registro_mx_materno.editText))
        val password = AndroidUtils.getText(til_negocio_registro_mx_password.editText)
        val clabe = AndroidUtils.getText(til_negocio_registro_mx_banco_clabe.editText)
        val idBanco =
                if (bancoAdapter.count == 0 || clabe.isBlank()) 0 else (spinner_negocio_registro_mx_banco.selectedItem as BankEntity).id

        val request = NegocioCreateRequest(
                celular = cel,
                nombreEstablecimiento = establecimiento,
                email = email,
                password = StringUtil.sha512(password),
                nombre = nombre,
                paterno = paterno,
                materno = materno,
                clabe = clabe,
                idBanco = idBanco
        )

        presenter.createNegocio(request)
    }

    override fun launchTarjeta() {

        val cel = viewModel.celular
        val establecimiento = AndroidUtils.getText(til_negocio_registro_mx_nombrecomercio.editText)
        val email = AndroidUtils.getText(til_negocio_registro_mx_email.editText)
        val nombre = AndroidUtils.getText(til_negocio_registro_mx_nombre.editText)
        val paterno = AndroidUtils.getText(til_negocio_registro_mx_paterno.editText)
        val materno = AndroidUtils.getText(til_negocio_registro_mx_materno.editText)
        val password = AndroidUtils.getText(til_negocio_registro_mx_password.editText)
        val clabe = AndroidUtils.getText(til_negocio_registro_mx_banco_clabe.editText)
        val idBanco =
                if (bancoAdapter.count == 0 || clabe.isBlank()) 0 else (spinner_negocio_registro_mx_banco.selectedItem as BankEntity).id

        val request = NegocioCreateRequest(
                celular = cel,
                nombreEstablecimiento = establecimiento,
                email = email,
                password = StringUtil.sha512(password),
                nombre = nombre,
                paterno = paterno,
                materno = materno,
                clabe = clabe,
                idBanco = idBanco
        )

        activity?.supportFragmentManager?.commit {
            add(R.id.frame_negocio_registro, NegocioRegistroTarjetaFragment.get(request))
            hide(this@NegocioRegistroDomicilioFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    override fun onRegistro(negocioEntity: NegocioEntity) {
        showSuccess(negocioEntity.mensajeError)
        login(negocioEntity)
    }

    override fun login(negocioEntity: NegocioEntity) {
        presenter.loginNegocio(negocioEntity)
    }

    override fun onLoginSuccessful() {
        if (activity != null) {
            startActivity(
                    NegocioCobroActivity.get(
                            activity!!,
                            NegocioCobroModel()
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            )

            activity!!.finish()
        }
    }

    override fun showRetry() {
        screen_negocio_registro_domicilio.isEnabled = false
        (activity as NegocioRegistroActivity).showRetry()
    }

    override fun hideRetry() {
        (activity as NegocioRegistroActivity).hideRetry()
        screen_negocio_registro_domicilio.isEnabled = true
    }

    override fun clickRetry() {
        if (bancoAdapter.count == 0)
            launchBancos()
    }

    override fun showSuccess(msg: String) {
        (activity as NegocioRegistroActivity).showSuccess(msg)
    }

    override fun showError(msg: String) {
        (activity as NegocioRegistroActivity).showError(msg)
    }

    override fun hideProgress() {
        (activity as NegocioRegistroActivity).hideProgress()
    }

    override fun showProgress() {
        (activity as NegocioRegistroActivity).showProgress()
    }
}