package addcel.mobilecard.ui.negocio.mx.cobro.menu;

import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.login.UsuarioStatus;
import addcel.mobilecard.domain.negocio.cobro.menu.NegocioCobroMenuInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 12/3/18.
 */
public class NegocioCobroMenuPresenter implements NegocioCobroMenuContract.Presenter {
    private final NegocioCobroMenuInteractor interactor;
    private final NegocioCobroMenuContract.View view;

    NegocioCobroMenuPresenter(NegocioCobroMenuInteractor interactor,
                              NegocioCobroMenuContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void validate(NegocioCobroMenuContract.TipoCobro tipoCobro) {
        view.showProgress();
        interactor.verify(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<ValidationEntity>() {
                    @Override
                    public void onSuccess(@NotNull ValidationEntity result) {
                        view.hideProgress();
                        switch (result.getEstatus()) {
                            case UsuarioStatus.ACTIVO:
                                onItemClicked(tipoCobro, result.getCard());
                                break;
                            case UsuarioStatus.EMAIL_VERIFICATION:
                                view.showSuccess(result.getMensajeError());
                                view.showActivationDialog();
                                break;
                            case UsuarioStatus.SMS_VERIFICATION:
                                view.showSuccess(result.getMensajeError());
                                view.launchSmsActivity(result);
                                break;
                            default:
                                view.showError(result.getMensajeError());
                                break;
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public NegocioEntity getNegocio() {
        return interactor.getNegocio();
    }

    @Override
    public String getComercioName() {
        return interactor.getComercioName();
    }

    @Override
    public void resendActivation() {
        view.showProgress();
        interactor.resendActivationLink(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                new InteractorCallback<DefaultResponse>() {
                    @Override
                    public void onSuccess(@NotNull DefaultResponse result) {
                        view.hideProgress();
                        view.dismissActivationDialog();
                        view.showSuccess(result.getMensajeError());
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public boolean hasIdAccount() {
        return interactor.getIdAccount() > 0;
    }

    private void onItemClicked(NegocioCobroMenuContract.TipoCobro tipo, @Nullable CardEntity card) {
        switch (tipo) {
            case QR_NEGOCIO:
                view.launchCreateFragment();
                break;
            case QR_CLIENTE:
                view.launchScanFragment();
                break;
            case TDC:
                view.launchCardFragment();
                break;
            default:
                break;
        }
    }
}
