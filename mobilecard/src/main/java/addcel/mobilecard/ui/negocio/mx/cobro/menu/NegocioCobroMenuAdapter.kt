package addcel.mobilecard.ui.negocio.mx.cobro.menu

import addcel.mobilecard.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.hdodenhof.circleimageview.CircleImageView

/**
 * ADDCEL on 31/08/18.
 */
class NegocioCobroMenuAdapter(
        private val items: List<NegocioCobroMenuContract.TipoCobro>,
        private val view: NegocioCobroMenuContract.View
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> {
                val v =
                        LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false)
                ItemViewHolder(v)
            }
            TYPE_HEADER -> {
                val v = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_header_negocio_menu, parent, false)
                HeaderViewHolder(view, v)
            }
            else -> throw RuntimeException(
                    "there is no type that matches the type $viewType + make sure your using types correctly"
            )
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {

        if (viewHolder is ItemViewHolder) {
            val tipoCobro = getItem(i)
            viewHolder.text.setText(tipoCobro.desc)
            when (tipoCobro) {
                NegocioCobroMenuContract.TipoCobro.QR_CLIENTE -> viewHolder.icon.setImageResource(R.drawable.escanear)
                NegocioCobroMenuContract.TipoCobro.TDC -> viewHolder.icon.setImageResource(R.drawable.tarjeta)
                NegocioCobroMenuContract.TipoCobro.QR_NEGOCIO -> viewHolder.icon.setImageResource(R.drawable.generar)
            }
        } else if (viewHolder is HeaderViewHolder) {
            viewHolder.name.text = view.negocioName
        }
    }

    override fun getItemCount(): Int {
        return items.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(pos: Int): NegocioCobroMenuContract.TipoCobro {
        return items[pos - 1]
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val icon: ImageView = view.findViewById(R.id.img_menu_item_icon)
        val text: TextView = view.findViewById(R.id.text_menu_item_name)
    }

    class HeaderViewHolder(menuView: NegocioCobroMenuContract.View, view: View) :
            RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.label_negocio_peru_menu_profile)
        val icon: CircleImageView = view.findViewById(R.id.b_negocio_peru_menu_profile)
        val qr: CircleImageView = view.findViewById(R.id.img_negocio_peru_menu_profile)

        init {
            icon.setOnClickListener {
                menuView.launchProfile()
            }

            qr.setOnClickListener {
                menuView.launchQr()
            }
        }
    }
}
