package addcel.mobilecard.ui.negocio.pe.generaqr.qr

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.qr.VisaQrWalletAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.negocio.pe.generaqr.NegocioPeGeneraQrActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-07-15.
 */

@PerFragment
@Subcomponent(modules = [NegocioPeGeneraQrQrModule::class])
interface NegocioPeGeneraQrQrSubcomponent {
    fun inject(fragment: NegocioPeGeneraQrQRFragment)
}

@Module
class NegocioPeGeneraQrQrModule(val fragment: NegocioPeGeneraQrQRFragment) {

    @PerFragment
    @Provides
    fun provideContainer(): NegocioPeGeneraQrActivity {
        return fragment.activity as NegocioPeGeneraQrActivity
    }

    @PerFragment
    @Provides
    fun provideData(): NegocioPeGeneraQrQRFragmentData {
        return fragment.arguments?.getParcelable("data")!!
    }

    @PerFragment
    @Provides
    fun provideApi(r: Retrofit): VisaQrWalletAPI {
        return VisaQrWalletAPI.get(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            api: VisaQrWalletAPI,
            sessionOperations: SessionOperations
    ): NegocioPeGeneraQrPresenter {
        return NegocioPeGeneraQrPresenterImpl(
                api, sessionOperations.negocio, CompositeDisposable(),
                fragment
        )
    }
}