package addcel.mobilecard.ui.negocio.pe.wallet.account.form

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.negocios.entity.BankEntity
import addcel.mobilecard.data.net.tebca.Cci
import addcel.mobilecard.data.net.tebca.CciRequest
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.negocio.pe.wallet.CommerceAccountEvent
import addcel.mobilecard.ui.negocio.pe.wallet.ProgressEvent
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import com.squareup.otto.Bus
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_wallet_account_form.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-08-19.
 */

@Parcelize
data class NegocioPeWalletAccountFormModel(
        val establecimientoId: Int,
        val cci: Cci? = null
) : Parcelable

interface NegocioWalletAccountFormView : ScreenView, TextWatcher {
    fun getBanks()

    fun setBanks(banks: List<BankEntity>)

    fun addAccount()

    fun onAccountAdded(accounts: CommerceAccountsResponse)
}

class NegocioPeWalletAccountFormFragment : Fragment(), NegocioWalletAccountFormView {

    companion object {
        fun get(model: NegocioPeWalletAccountFormModel): NegocioPeWalletAccountFormFragment {
            val args = BundleBuilder().putParcelable("model", model).build()
            val frag = NegocioPeWalletAccountFormFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var model: NegocioPeWalletAccountFormModel
    @Inject
    lateinit var adapter: ArrayAdapter<BankEntity>
    @Inject
    lateinit var presenter: NegocioPeWalletAccountFormPresenter
    @Inject
    lateinit var bus: Bus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mobilecard.get().netComponent.negocioPeWalletAccountFormSubcomponent(
                NegocioPeWalletAccountFormModule(this)
        ).inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_negocio_wallet_account_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinner_negocio_wallet_account_form_banco.adapter = adapter
        spinner_negocio_wallet_account_form_banco.isEnabled = false
        getBanks()
        if (model.cci != null) AndroidUtils.setText(
                til_negocio_wallet_account_form_cci.editText,
                model.cci!!.cuenta
        )

        AndroidUtils.getEditText(til_negocio_wallet_account_form_cci).addTextChangedListener(this)

        b_negocio_wallet_account_form_continuar.setOnClickListener {

            val cci = AndroidUtils.getText(til_negocio_wallet_account_form_cci.editText)

            if (cci.isEmpty() || !cci.isDigitsOnly() || cci.length != 20) {
                til_negocio_wallet_account_form_cci.error = "Captura un CCI válido"
            } else {
                til_negocio_wallet_account_form_cci.error = null
                addAccount()
            }
        }
    }

    override fun getBanks() {
        presenter.getBanks()
    }

    override fun setBanks(banks: List<BankEntity>) {
        if (banks.isNotEmpty()) {
            if (adapter.isEmpty) adapter.addAll(banks)
            spinner_negocio_wallet_account_form_banco.isEnabled = true
            spinner_negocio_wallet_account_form_banco.setSelection(
                    presenter.setSelectedBankPos(model.cci?.nombreBanco, banks)
            )
        }
    }

    override fun addAccount() {
        val request = CciRequest(
                AndroidUtils.getText(til_negocio_wallet_account_form_cci.editText),
                model.establecimientoId,
                (spinner_negocio_wallet_account_form_banco.selectedItem as BankEntity).id
        )

        presenter.updateAccount(request)
    }

    override fun onAccountAdded(accounts: CommerceAccountsResponse) {
        bus.post(CommerceAccountEvent(accounts))
    }

    override fun showProgress() {
        bus.post(ProgressEvent(true))
    }

    override fun hideProgress() {
        bus.post(ProgressEvent(false))
    }

    override fun showError(msg: String) {
        Toasty.error(activity!!, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(activity!!, msg).show()
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (p0.isEmpty() || !p0.isDigitsOnly() || p0.length != 20) {
                til_negocio_wallet_account_form_cci.error = "Captura un CCI válido"
            } else {
                til_negocio_wallet_account_form_cci.error = null
            }
        }
    }
}