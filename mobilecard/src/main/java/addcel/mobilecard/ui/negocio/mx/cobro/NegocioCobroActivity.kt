package addcel.mobilecard.ui.negocio.mx.cobro

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.usuarios.model.PushApi
import addcel.mobilecard.data.net.usuarios.model.PushTokenRequest
import addcel.mobilecard.data.net.usuarios.model.RemovePushTokenRequest
import addcel.mobilecard.domain.usuario.bottom.MenuEvent
import addcel.mobilecard.domain.usuario.bottom.UseCase
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.contacto.ContactoActivity
import addcel.mobilecard.ui.contacto.ContactoModel
import addcel.mobilecard.ui.custom.extension.DetachableClickListener
import addcel.mobilecard.ui.login.LoginActivity
import addcel.mobilecard.ui.negocio.mx.cobro.create.monto.NegocioMxCreateMontoQrKeyboardFragment
import addcel.mobilecard.ui.negocio.mx.cobro.create.monto.NegocioMxCreateMontoQrKeyboardFragment.Companion.get
import addcel.mobilecard.ui.negocio.mx.cobro.menu.NegocioCobroMenuContract
import addcel.mobilecard.ui.negocio.mx.cobro.menu.NegocioCobroMenuFragment
import addcel.mobilecard.ui.negocio.mx.cobro.scan.NegocioCobroQrScanFragment
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.monto.NegocioMxUsercardMontoQrKeyboardFragment
import addcel.mobilecard.ui.negocio.mx.historial.NegocioHistorialActivity
import addcel.mobilecard.ui.negocio.mx.password.NegocioPasswordActivity
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletActivity
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletModel
import addcel.mobilecard.ui.novedades.NovedadesActivity
import addcel.mobilecard.ui.usuario.menu.BottomMenuEventListener
import addcel.mobilecard.ui.usuario.menu.MenuLayout
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.FragmentUtil
import addcel.mobilecard.utils.StringUtil
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.ProgressBar
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import com.google.firebase.iid.FirebaseInstanceId
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem
import com.squareup.otto.Bus
import com.squareup.otto.Subscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_negocio_cobro.*
import retrofit2.Retrofit
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import javax.inject.Named

@Parcelize
data class NegocioCobroModel(val useCase: String = NegocioCobroActivity.USE_CASE_MENU) :
        Parcelable

class NegocioCobroActivity : ContainerActivity(), BottomMenuEventListener {


    @field:[Inject Named("okListener")]
    lateinit var okListener: DetachableClickListener
    @field:[Inject Named("cancelListener")]
    lateinit var cancelListener: DetachableClickListener

    @Inject
    lateinit var logoutDialog: AlertDialog
    @Inject
    lateinit var model: NegocioCobroModel
    @Inject
    lateinit var bus: Bus
    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var state: StateSession
    @Inject
    lateinit var retrofit: Retrofit

    lateinit var perfilItem: PrimaryDrawerItem
    lateinit var inicioItem: PrimaryDrawerItem
    lateinit var tarjetaItem: PrimaryDrawerItem
    lateinit var generaQrItem: PrimaryDrawerItem
    lateinit var passwordItem: PrimaryDrawerItem
    lateinit var escaneQrItem: PrimaryDrawerItem
    lateinit var dividerItem: DividerDrawerItem
    lateinit var contactoItem: SecondaryDrawerItem
    lateinit var logoutItem: SecondaryDrawerItem
    lateinit var versionItem: SecondaryDrawerItem

    lateinit var drawer: Drawer
    lateinit var push: PushApi
    lateinit var negocioApi: NegocioAPI
    private val disposables = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_cobro)
        NegocioCobroComponentCreator.negocioCobroSubcomponent(
                Mobilecard.get(),
                NegocioCobroModule(this)
        ).inject(this)

        okListener.clearOnDetach(logoutDialog)
        cancelListener.clearOnDetach(logoutDialog)

        push = PushApi.get(retrofit)
        negocioApi = retrofit.create(NegocioAPI::class.java)
        setSupportActionBar(toolbar_negocio_cobro)

        drawer = setDrawerLayout()

        FragmentUtil.addFragment(
                fragmentManagerLazy, R.id.frame_negocio_cobro,
                NegocioCobroMenuFragment.get()
        )

        initItems()
        setItemsInDrawer()
        configBottomLayout()
        drawer.setSelection(INICIO)
        state.setMenuLaunched(true)

        if (model.useCase == USE_CASE_CARD) {
            val wModel = NegocioMxWalletModel(session.negocio.idEstablecimiento, session.negocio.tipoPersona)
            startActivity(NegocioMxWalletActivity.get(this, wModel))
        }
    }

    override fun onResume() {
        super.onResume()
        bus.register(this)

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (!it.isSuccessful) {
                Timber.e(it.exception, "Error al obtener instanceId")
                return@addOnCompleteListener
            }

            val token = it.result?.token
            Timber.d("Token push: %s", token)

            if (token != null) {

                val idApp = BuildConfig.ADDCEL_APP_ID
                val idEstablecimiento = session.negocio.idEstablecimiento.toLong()
                val idPais = session.negocio.idPais
                val idioma = StringUtil.getCurrentLanguage()

                disposables.add(
                        push.saveToken(
                                PushApi.PUSH_AUTH,
                                PushTokenRequest(
                                        idApp,
                                        idPais,
                                        idEstablecimiento,
                                        idioma,
                                        "ESTABLECIMIENTO",
                                        token
                                )
                        )
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({}, { t ->
                                    t.printStackTrace()
                                    Timber.e(t)
                                })
                )
            }
        }
    }


    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun deleteToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (!it.isSuccessful) {
                Timber.e(it.exception, "Error al obtener instanceId")
                processLogout()
                return@addOnCompleteListener
            }

            val token = it.result?.token
            Timber.d("Token push: %s", token)

            if (token != null) {
                val idEstablecimiento = session.negocio.idEstablecimiento.toLong()
                showProgress()
                disposables.add(
                        push.deleteToken(
                                PushApi.PUSH_AUTH,
                                RemovePushTokenRequest(
                                        idEstablecimiento,
                                        "ESTABLECIMIENTO",
                                        token
                                )
                        )
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    hideProgress()
                                    processLogout()
                                }, { t ->
                                    hideProgress()
                                    t.printStackTrace()
                                    processLogout()
                                })
                )
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        drawer.setSelection(INICIO)
    }

    override fun onPause() {
        bus.unregister(this)
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_drawer) {
            if (!drawer.isDrawerOpen) drawer.openDrawer()
            return true
        } else if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getRetry(): View? {
        return null
    }

    override fun showProgress() {
        if (getProgressBar()!!.visibility == View.GONE) getProgressBar()!!.visibility = View.VISIBLE
    }

    override fun hideRetry() {

    }

    override fun hideProgress() {
        if (getProgressBar()!!.visibility == View.VISIBLE) getProgressBar()!!.visibility = View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_negocio_cobro.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_negocio_cobro.title = title
    }

    override fun showRetry() {

    }


    private fun getProgressBar(): ProgressBar? {
        return progress_negocio_cobro
    }

    @Subscribe
    override fun onMenuEventReceived(event: MenuEvent) {
        (findViewById<View>(R.id.menu_negocio_cobro) as MenuLayout).unblock()
        when (event.useCase) {
            UseCase.HOME -> {
                refreshHomeScreen(fragmentManagerLazy)
            }
            UseCase.WALLET -> {
                disposables.add(
                        negocioApi.verify(
                                BuildConfig.ADDCEL_APP_ID,
                                StringUtil.getCurrentLanguage(),
                                session.negocio.idEstablecimiento.toLong()
                        )
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            val wModel =
                                                    NegocioMxWalletModel(
                                                            session.negocio.idEstablecimiento,
                                                            session.negocio.tipoPersona
                                                    )
                                            startActivity(NegocioMxWalletActivity.get(this, wModel))
                                        }, {
                                    showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                                }
                                )
                )
            }
            UseCase.HISTORIAL -> startActivity(NegocioHistorialActivity.get(this, 1))
            else -> {
            }
        }
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen) {
            drawer.closeDrawer()
        } else {
            if (screenStatus == ContainerScreenView.STATUS_FINISHED) {
                refreshHomeScreen(fragmentManagerLazy)
                setStreenStatus(ContainerScreenView.STATUS_NORMAL)
            } else {
                super.onBackPressed()
            }
        }
    }

    private fun setDrawerLayout(): Drawer {

        setSupportActionBar(toolbar_negocio_cobro)

        return DrawerBuilder().withActivity(this).withToolbar(toolbar_negocio_cobro)
                .withSliderBackgroundColorRes(R.color.colorAccent).withActionBarDrawerToggle(false)
                .withHeader(R.layout.item_drawer_header).withDrawerGravity(Gravity.END)
                .withOnDrawerItemClickListener { _, _, drawerItem ->
                    clickDrawer(drawerItem.identifier)
                    false
                }.build()
    }

    private fun initItems() {
        perfilItem = buildProfileItem()
        inicioItem = buildInicioItem()
        tarjetaItem = buildPresenteItem()
        generaQrItem = buildGeneraItem()
        escaneQrItem = buildEscaneaItem()
        passwordItem = buildPasswordItem()
        dividerItem = buildDivider()
        contactoItem = buildContactoItem()
        logoutItem = buildLogoutItem()
        versionItem = buildVersionItem()
    }

    private fun setItemsInDrawer() { //drawer.addItem(perfilItem)
        //drawer.addItem(dividerItem)
        drawer.addItem(inicioItem)
        drawer.addItem(tarjetaItem)
        drawer.addItem(generaQrItem) // drawer.addItem(escaneQrItem)
        drawer.addItem(escaneQrItem)
        drawer.addItem(passwordItem)
        drawer.addItem(dividerItem)
        drawer.addItem(contactoItem)
        drawer.addItem(logoutItem)
        drawer.addItem(versionItem)
    }

    private fun buildProfileItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(PERFIL).withName(R.string.txt_perfil_title)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground).withIcon(R.drawable.icon_settings)
    }

    private fun buildInicioItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(INICIO).withName(R.string.nav_home)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
    }

    private fun buildPresenteItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(TARJETA)
                .withName(NegocioCobroMenuContract.TipoCobro.TDC.desc)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
    }

    private fun buildGeneraItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(GENERA)
                .withName(NegocioCobroMenuContract.TipoCobro.QR_NEGOCIO.desc)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
    }

    private fun buildEscaneaItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(ESCANEA)
                .withName(NegocioCobroMenuContract.TipoCobro.QR_CLIENTE.desc)
                .withTextColorRes(R.color.colorBackground).withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
    }

    private fun buildPasswordItem(): PrimaryDrawerItem {
        return PrimaryDrawerItem().withIdentifier(PASSWORD)
                .withName(R.string.txt_perfil_change_password).withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
    }

    private fun buildDivider(): DividerDrawerItem {
        return DividerDrawerItem().withIdentifier(4)
    }

    private fun buildContactoItem(): SecondaryDrawerItem {
        return SecondaryDrawerItem().withIdentifier(CONTACTO)
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withName(getString(R.string.nav_contactus)).withIcon(R.drawable.icon_info_white)
    }

    private fun buildLogoutItem(): SecondaryDrawerItem {
        return SecondaryDrawerItem().withIdentifier(LOGOUT)
                .withTextColorRes(R.color.colorBackground)
                .withSelectedColorRes(R.color.colorAccentDark)
                .withSelectedTextColorRes(R.color.colorBackground)
                .withName(getString(R.string.nav_logout))
                .withIcon(R.drawable.icon_logout)
    }

    private fun buildVersionItem(): SecondaryDrawerItem {
        return SecondaryDrawerItem().withIdentifier(VERSION).withEnabled(false)
                .withDisabledTextColorRes(R.color.colorBackground)
                .withName(
                        getString(
                                R.string.version,
                                BuildConfig.VERSION_NAME
                        ).toUpperCase(Locale.getDefault())
                )
    }

    private fun clickDrawer(id: Long) {
        when (id) {
            TARJETA -> launchTarjetaPresente()
            GENERA -> launchGeneraQR()
            ESCANEA -> launchEscaneaQR()
            PASSWORD -> startActivity(NegocioPasswordActivity.get(this, session.negocio, true))
            CONTACTO -> startActivity(
                    ContactoActivity.get(
                            this,
                            ContactoModel(
                                    session.negocio.idEstablecimiento.toLong(),
                                    session.negocio.email,
                                    1
                            )
                    )
            )
            LOGOUT -> logout()
            else -> {

            }
        }
    }

    private fun launchTarjetaPresente() {


        /*
        fragmentManager.beginTransaction()
                .add(container, newFragment)
                .addToBackStack(null)
                .hide(currentFragment)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
         */

        fragmentManagerLazy.commit {
            replace(R.id.frame_negocio_cobro, NegocioMxUsercardMontoQrKeyboardFragment.get())
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    private fun launchGeneraQR() {
        fragmentManagerLazy.commit {
            replace(R.id.frame_negocio_cobro, NegocioMxCreateMontoQrKeyboardFragment.get())
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
        }
    }

    private fun launchEscaneaQR() {

        if (drawer.isDrawerOpen) drawer.closeDrawer()

        Handler().postDelayed({
            fragmentManagerLazy.commit {
                replace(R.id.frame_negocio_cobro, NegocioCobroQrScanFragment.get())
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }, 250)
    }

    private fun logout() {
        androidx.appcompat.app.AlertDialog.Builder(this)
                .setMessage("¿Deseas cerrar sesión MobileCard?")
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    deleteToken()
                }.setNegativeButton(android.R.string.no) { p0, _ -> p0?.dismiss() }.show()
    }

    private fun configBottomLayout() {
        val favs = menu_negocio_cobro.findViewById<ImageButton>(R.id.b_menu_frecuentes)
        favs.visibility = View.GONE
    }

    private fun processLogout() {
        session.logout()
        startActivity(LoginActivity.get(this).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    fun refreshHomeScreen(fm: FragmentManager) {
        if (fm.backStackEntryCount > 0) {
            for (count in 0 until fm.backStackEntryCount) {
                fm.popBackStack()
            }
            fm.commit {
                add(R.id.frame_negocio_cobro, NegocioCobroMenuFragment.get())
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == NovedadesActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val useCase = data.getIntExtra(NovedadesActivity.DATA_USE_CASE, -1)
                    NovedadesActivity.processNegocio(this, useCase, session.negocio.idEstablecimiento, 1)
                }
            }
        }
    }


    fun showBottomMenu() {
        if (menu_negocio_cobro.visibility == View.GONE) menu_negocio_cobro.visibility = View.VISIBLE
    }

    fun hideBottomMenu() {
        if (menu_negocio_cobro.visibility == View.VISIBLE) menu_negocio_cobro.visibility = View.GONE
    }

    companion object {

        private const val PERFIL = 0L
        private const val INICIO = 6L
        private const val TARJETA = 1L
        private const val GENERA = 2L
        private const val ESCANEA = 3L
        private const val PASSWORD = 9L
        private const val CONTACTO = 8L
        private const val LOGOUT = 5L
        private const val VERSION = 7L

        const val USE_CASE_CARD = "card"
        const val USE_CASE_MENU = "menu"

        fun get(context: Context, model: NegocioCobroModel): Intent {
            return Intent(context, NegocioCobroActivity::class.java).putExtra("model", model)
        }
    }
}
