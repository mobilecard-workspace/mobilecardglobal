package addcel.mobilecard.ui.negocio.pe.registro.qr

import addcel.mobilecard.R
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.JsonUtil
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.annotations.SerializedName
import com.google.zxing.Result
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_negocio_registro_pe_qr.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber

data class NegocioRegistroPEQrData(@SerializedName("idEstablecimiento") val idEstablecimiento: Int)

class NegocioRegistroPeQrActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    override fun handleResult(p0: Result?) {
        if (p0 != null) {
            val code = decryptQr(p0.text)
            if (code != null) {
                val clearQr = deserializeQr(code)
                if (clearQr != null) {
                    Timber.d("Id establecimiento: %d", clearQr.idEstablecimiento)
                    val intent = Intent().putExtra("id_establecimiento", clearQr.idEstablecimiento)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else {
                    Toasty.error(this, ErrorUtil.getErrorMsg(ErrorUtil.OPERATION)).show()
                    scanner.resumeCameraPreview(this)
                }
            } else {
                Toasty.error(this, ErrorUtil.getErrorMsg(ErrorUtil.OPERATION)).show()
                scanner.resumeCameraPreview(this)
            }
        } else {
            scanner.resumeCameraPreview(this)
        }
    }

    private lateinit var scanner: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_registro_pe_qr)
        scanner = scanner_pe_negocio_registro
    }

    override fun onResume() {
        super.onResume()
        scanner.setResultHandler(this)
        scanner.startCamera()
    }

    public override fun onPause() {
        super.onPause()
        scanner.stopCamera()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, Intent())
        finish()
    }

    private fun decryptQr(text: String): String? {
        val decrypt = AddcelCrypto.decryptHard(text)
        Timber.d("QR Descifrado: %s", decrypt)
        return decrypt
    }

    private fun deserializeQr(clearText: String): NegocioRegistroPEQrData? {

        return try {
            JsonUtil.fromJson(clearText, NegocioRegistroPEQrData::class.java)
        } catch (t: Throwable) {
            t.printStackTrace()
            null
        }
    }
}
