package addcel.mobilecard.ui.negocio.mx.cobro.menu;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.negocios.entity.ValidationEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.sms.SmsUseCase;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import addcel.mobilecard.ui.custom.extension.ItemClickSupport;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroComponentCreator;
import addcel.mobilecard.ui.negocio.mx.cobro.create.monto.NegocioMxCreateMontoQrKeyboardFragment;
import addcel.mobilecard.ui.negocio.mx.cobro.scan.NegocioCobroQrScanFragment;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.monto.NegocioMxUsercardMontoQrKeyboardFragment;
import addcel.mobilecard.ui.negocio.mx.perfil.NegocioPerfilActivity;
import addcel.mobilecard.ui.negocio.pe.id.NegocioPeIdQRActivity;
import addcel.mobilecard.ui.negocio.pe.id.NegocioPeIdQrModel;
import addcel.mobilecard.ui.sms.SmsActivity;
import addcel.mobilecard.utils.FragmentUtil;

/**
 * ADDCEL on 31/08/18.
 */
public final class NegocioCobroMenuFragment extends Fragment
        implements NegocioCobroMenuContract.View {
    @Inject
    NegocioCobroActivity activity;
    @Inject
    NegocioCobroMenuContract.Presenter presenter;
    @Inject
    NegocioCobroMenuAdapter adapter;

    private AlertDialog activateDialog;
    private RecyclerView cobroView;

    private DetachableClickListener positiveListener;
    private DetachableClickListener cancelListener;

    public NegocioCobroMenuFragment() {
    }

    public static synchronized NegocioCobroMenuFragment get() {
        return new NegocioCobroMenuFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NegocioCobroComponentCreator.negocioCobroMenuSubcomponent(Mobilecard.get(),
                new NegocioCobroMenuModule(this)).inject(this);

        positiveListener = DetachableClickListener.wrap((dialog, which) -> presenter.resendActivation());
        cancelListener = DetachableClickListener.wrap((dialog, which) -> dialog.dismiss());

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            activity.setAppToolbarTitle(R.string.txt_negocio_cobro_title);
            activity.showBottomMenu();
        }
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_negocio_cobro_menu, container, false);
        cobroView = view.findViewById(R.id.recycler_negocio_cobro_menu);
        cobroView.setAdapter(adapter);
        cobroView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        ItemClickSupport.addTo(cobroView)
                .setOnItemClickListener(
                        (recyclerView, position, v) -> {
                            final RecyclerView.ViewHolder vh = recyclerView.getChildViewHolder(v);
                            if (vh instanceof NegocioCobroMenuAdapter.ItemViewHolder) {
                                NegocioCobroMenuFragment.this.onItemClicked(position);
                            }
                        });
        return view;


        /*
  ItemClickSupport.addTo(recycler_negocio_peru_menu).setOnItemClickListener { rv, position, v ->
                val vh = rv.getChildViewHolder(v)
                if (vh is NegocioMenuPeAdapter.ItemViewHolder) {
                    when (adapter.getItem(position)) {
                        NegocioPeruMenuItems.TARJETA_PRESENTE -> startActivity(
                            NegocioPeTarjetaPresenteActivity.get(this@NegocioMenuPeActivity)
                        )
                        NegocioPeruMenuItems.GENERA_QR -> startActivity(
                            NegocioPeGeneraQrActivity.get(
                                this@NegocioMenuPeActivity
                            )
                        ) //NegocioPeruMenuItems.ESCANEA_QR -> launchScanQr(rxPermission)
                    }
                }
            }
         */
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        positiveListener.clearOnDetach(getActivationDialog());
        cancelListener.clearOnDetach(getActivationDialog());
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        activity.showBottomMenu();
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(cobroView);
        activateDialog = null;
        cobroView.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void launchCreateFragment() {
        FragmentUtil.addFragmentHidingCurrent(activity.getFragmentManagerLazy(),
                R.id.frame_negocio_cobro, this, NegocioMxCreateMontoQrKeyboardFragment.Companion.get());
    }

    @Override
    public void launchScanFragment() {
        FragmentUtil.addFragmentHidingCurrent(activity.getFragmentManagerLazy(),
                R.id.frame_negocio_cobro, this, NegocioCobroQrScanFragment.get());
    }

    @Override
    public void launchCardFragment() {
        FragmentUtil.addFragmentHidingCurrent(activity.getFragmentManagerLazy(),
                R.id.frame_negocio_cobro, this, NegocioMxUsercardMontoQrKeyboardFragment.Companion.get());
    }

    @Override
    public void launchWalletActivity(@Nullable CardEntity card) {

    }

    @Override
    public void launchSmsActivity(ValidationEntity entity) {
        startActivity(SmsActivity.get(activity, SmsUseCase.N_MENU, entity.getId(), 1));
    }

    @Override
    public void launchProfile() {
        startActivity(NegocioPerfilActivity.get(activity, presenter.getNegocio()));

    }

    @Override
    public void launchQr() {
        final NegocioPeIdQrModel viewModel = new NegocioPeIdQrModel(presenter.getNegocio().getIdEstablecimiento(), presenter.getNegocio().getQrBase64(),
                presenter.getNegocio().getNombreEstablecimiento(), presenter.getNegocio().getIdPais());
        startActivity(NegocioPeIdQRActivity.Companion.get(
                activity, viewModel));
    }

    @Override
    public String getNegocioName() {
        return presenter.getComercioName();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void showActivationDialog() {
        if (isVisible() && !getActivationDialog().isShowing()) getActivationDialog().show();
    }

    @Override
    public void dismissActivationDialog() {
        if (isVisible() && getActivationDialog().isShowing()) getActivationDialog().dismiss();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void onItemClicked(int pos) {
        NegocioCobroMenuContract.TipoCobro item = adapter.getItem(pos);
        presenter.validate(item);
    }

    private AlertDialog getActivationDialog() {
        if (activateDialog == null) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
            dialog.setTitle(android.R.string.dialog_alert_title);
            dialog.setMessage(activity.getString(R.string.txt_account_activation));
            dialog.setPositiveButton(activity.getString(R.string.txt_account_activation_btn), positiveListener);
            dialog.setNegativeButton(activity.getString(android.R.string.cancel), cancelListener);
            activateDialog = dialog.create();
        }
        return activateDialog;
    }
}
