package addcel.mobilecard.ui.negocio.pe.registro.third

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.negocios.entity.BankResponse
import addcel.mobilecard.data.net.negocios.entity.NegocioAltaRequest
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.data.net.usuarios.model.TerminosResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.peru.PeruNegocioRegistroInteractor
import addcel.mobilecard.utils.StringUtil

/**
 * ADDCEL on 2019-07-11.
 */
interface NegocioRegistroPe3Presenter {
    fun clearDisposables()

    fun getBanks()

    fun getTerminos()

    fun getContrato()

    fun altaUsuario(request: NegocioAltaRequest)

    fun login(negocio: NegocioEntity)
}

class NegocioRegistroPe3PresenterImpl(
        val interactor: PeruNegocioRegistroInteractor,
        val sessionOperations: SessionOperations,
        val state: StateSession,
        val view: View
) : NegocioRegistroPe3Presenter {

    override fun clearDisposables() {
        interactor.clear()
    }

    override fun getBanks() {
        view.showProgress()
        interactor.getBanks(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                object : InteractorCallback<BankResponse> {
                    override fun onSuccess(result: BankResponse) {
                        view.hideProgress()
                        if (result.idError == 0L) {
                            view.hideRetry()
                            view.setBancos(result.banks)
                        } else {
                            view.showRetry()
                            view.showError(result.mensajeError)
                        }
                    }

                    override fun onError(error: String) {
                        view.showRetry()
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun getTerminos() {
        view.showProgress()
        interactor.getTerms(BuildConfig.ADDCEL_APP_ID, NegocioAPI.METHOD_TERMS,
                StringUtil.getCurrentLanguage(), object : InteractorCallback<TerminosResponse> {
            override fun onSuccess(result: TerminosResponse) {
                view.hideProgress()
                view.setTerminos(result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
            }
        })
    }

    override fun getContrato() {
        view.showProgress()
        interactor.getTerms(BuildConfig.ADDCEL_APP_ID, NegocioAPI.METHOD_PRIVACY,
                StringUtil.getCurrentLanguage(), object : InteractorCallback<TerminosResponse> {
            override fun onSuccess(result: TerminosResponse) {
                view.hideProgress()
                view.setContrato(result)
            }

            override fun onError(error: String) {
                view.hideProgress()
                view.showError(error)
            }
        })
    }

    override fun altaUsuario(request: NegocioAltaRequest) {
        view.showProgress()
        interactor.saveUser(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request,
                object : InteractorCallback<NegocioEntity> {
                    override fun onSuccess(result: NegocioEntity) {
                        view.hideProgress()
                        if (result.idError == 0) {
                            login(result)
                        } else {
                            view.showError(result.mensajeError)
                        }
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun login(negocio: NegocioEntity) {
        sessionOperations.logout()
        sessionOperations.negocio = negocio
        sessionOperations.isNegocioLogged = true
        state.setCapturedEmail("")
        state.setCapturedPhone("")
        state.setMenuLaunched(false)
        view.onLoginSuccess()
    }
}