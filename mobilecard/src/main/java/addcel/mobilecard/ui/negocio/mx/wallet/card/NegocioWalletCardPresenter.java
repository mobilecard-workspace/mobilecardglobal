package addcel.mobilecard.ui.negocio.mx.wallet.card;

import android.content.Context;
import android.os.Build;

import androidx.core.content.ContextCompat;

import com.google.common.base.Strings;
import com.vinaygaba.creditcardview.CreditCardView;

import java.text.NumberFormat;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.negocio.wallet.card.NegocioWalletCardInteractor;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 12/12/18.
 */
public class NegocioWalletCardPresenter implements NegocioWalletCardContract.Presenter {

    private final NegocioWalletCardInteractor interactor;
    private final NumberFormat numberFormat;

    NegocioWalletCardPresenter(NegocioWalletCardInteractor interactor, NumberFormat numberFormat) {
        this.interactor = interactor;
        this.numberFormat = numberFormat;
    }

    @Override
    public CardEntity getModel() {
        return interactor.getCard();
    }

    @Override
    public String getPan() {
        if (Strings.isNullOrEmpty(getModel().getPan())) {
            return "";
        }
        return AddcelCrypto.decryptHard(getModel().getPan());
    }

    @Override
    public String getHolderName() {
        return Strings.nullToEmpty(getModel().getNombre());
    }

    @Override
    public String getVigencia() {
        if (Strings.isNullOrEmpty(getModel().getVigencia())) {
            return "";
        } else {
            return AddcelCrypto.decryptHard(getModel().getVigencia());
        }
    }

    @Override
    public String getBalance() {
        try {
            double balance = getModel().getBalance();
            return numberFormat.format(balance);
        } catch (NumberFormatException e) {
            return "";
        }
    }

    @Override
    public int getBalanceColor() {
        double balance = getModel().getBalance();
        if (balance < 0) {
            return NegocioWalletCardContract.Constants.COLOR_RED;
        } else if (balance > 0) {
            return NegocioWalletCardContract.Constants.COLOR_GREEN;
        } else {
            return NegocioWalletCardContract.Constants.COLOR_GRAPHITE;
        }
    }

    @Override
    public String buildPhoneMessage() {
        return null;
    }

    @Override
    public CreditCardView buildCardView(Context context) {
        CreditCardView view = new CreditCardView(context);
        view.setIsEditable(false);
        view.setBackground(ContextCompat.getDrawable(context, R.drawable.card_ccv));
        view.setCardName(getHolderName());
        view.setCardNameTextColor(context.getResources().getColor(R.color.colorBackground));
        view.setCardNumber(getPan());
        view.setCardNumberTextColor(context.getResources().getColor(R.color.colorBackground));
        view.setExpiryDate(getVigencia());
        view.setExpiryDateTextColor(context.getResources().getColor(R.color.colorBackground));
        try {
            view.findViewById(R.id.card_logo).setBackgroundResource(R.drawable.logo_carnet);
            view.invalidate();
            view.requestLayout();
            // setVisibility(View.INVISIBLE);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) view.setElevation((float) 4.0);
        view.putChip(false);
        return view;
    }
}
