package addcel.mobilecard.ui.negocio.pe.tarjetapresente.tarjeta

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.negocio.NegocioSignatureActivity
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.NegocioPeTarjetaPresenteActivity
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.result.NegocioPeTarjetaPresenteResultFragment
import addcel.mobilecard.utils.*
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.tbruyelle.rxpermissions2.RxPermissions
import io.card.payment.CardIOActivity
import io.card.payment.CardType
import io.card.payment.CreditCard
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_pe_tarjeta_presente_tarjeta.*
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber
import javax.inject.Inject

/**
 * ADDCEL on 2019-07-13.
 */

@Parcelize
data class NegocioPeTarjetaPresenteTarjetaData(val monto: String, val concepto: String) :
        Parcelable

interface View : ScreenView, TextWatcher {

    fun clickScan()

    fun onToken(token: TokenEntity)

    fun onPagoSuccess(result: SPReceiptEntity)
}

class NegocioPeTarjetaPresenteTarjetaFragment : Fragment(),
        addcel.mobilecard.ui.negocio.pe.tarjetapresente.tarjeta.View {

    companion object {
        const val REQUEST_CODE = 402

        fun get(
                configData: NegocioPeTarjetaPresenteTarjetaData
        ): NegocioPeTarjetaPresenteTarjetaFragment {
            Timber.d("Data tarjeta presente: %s", configData.toString())

            val args = BundleBuilder().putParcelable("data", configData).build()
            val frag = NegocioPeTarjetaPresenteTarjetaFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var containerAct: NegocioPeTarjetaPresenteActivity
    @Inject
    lateinit var presenter: NegocioPeTarjetaPresenteTarjetaPresenter
    @Inject
    lateinit var configData: NegocioPeTarjetaPresenteTarjetaData
    lateinit var permissions: RxPermissions
    private val permissionDisposable = CompositeDisposable()
    private var capturedCard: CreditCard? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioPeTarjetaPresenteSubcomponent(
                NegocioPeTarjetaPresenteTarjetaModule(this)
        ).inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.screen_negocio_pe_tarjeta_presente_tarjeta,
                container,
                false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        permissions = RxPermissions(this@NegocioPeTarjetaPresenteTarjetaFragment)
        AndroidUtils.getEditText(til_negocio_cobro_usercard_card_cardholder_nombre)
                .addTextChangedListener(this)
        AndroidUtils.getEditText(til_negocio_cobro_usercard_card_cardholder_apellido)
                .addTextChangedListener(this)
        AndroidUtils.getEditText(til_usercard_card_email).addTextChangedListener(this)
        b_negocio_cobro_usercard_card_scan.setOnClickListener {
            clickScan()
        }

        b_negocio_cobro_usercard_card_continuar.setOnClickListener {

            this@NegocioPeTarjetaPresenteTarjetaFragment.startActivityForResult(
                    NegocioSignatureActivity.get(view.context), NegocioSignatureActivity.REQUEST_CODE
            )
        }
        disableCardFields()

        Handler().postDelayed({ clickScan() }, 250)
    }

    override fun onDestroy() {
        permissionDisposable.clear()
        presenter.clearDisposables()
        super.onDestroy()
    }

    private fun launchCardScan() {
        val scanIntent =
                Intent(activity, CardIOActivity::class.java).putExtra(
                        CardIOActivity.EXTRA_REQUIRE_EXPIRY,
                        true
                ).putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false)
                        .putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true)
                        .putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false)
                        .putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false)
                        .putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, true)
                        .putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true)
                        .putExtra(
                                CardIOActivity.EXTRA_SCAN_INSTRUCTIONS,
                                getString(R.string.txt_cardio_instruction)
                        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scanIntent.putExtra(
                    CardIOActivity.EXTRA_GUIDE_COLOR,
                    containerAct.resources!!.getColor(R.color.colorAccent, null)
            )
        } else {
            scanIntent.putExtra(
                    CardIOActivity.EXTRA_GUIDE_COLOR,
                    containerAct.resources!!.getColor(R.color.colorAccent)
            )
        }

        startActivityForResult(scanIntent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE) {
            Timber.d("Card.io Request Code: %d", requestCode)
            Timber.d("Card.io Result Code: %d", resultCode)
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                processCard(data)
            }
        } else if (requestCode == NegocioSignatureActivity.REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val firma = data?.getStringExtra("firma")!!
                    launchPago(firma)
                }
                Activity.RESULT_CANCELED -> showError("Cancelado por el usuario")
                else -> showError(ErrorUtil.getErrorMsg(ErrorUtil.OPERATION))
            }
        }
    }

    private fun launchPago(firma: String) {

        if (capturedCard != null) {

            val amount = configData.monto.toDouble() / 100
            val comision = presenter.calculaComision(
                    amount,
                    spinner_negocio_cobro_usercard_card_tipo.selectedItemPosition
            )
            val concepto = configData.concepto
            val idEstablecimiento = presenter.getIdNegocio()
            val imei = DeviceUtil.getDeviceId(containerAct)
            val lat = presenter.getLocation().lat
            val lon = presenter.getLocation().lon
            val modelo = Build.MODEL
            val propina = 0.0
            val software = Build.VERSION.SDK_INT.toString()
            val ct = AddcelCrypto.encryptHard(capturedCard?.cvv!!.trim())
            val tarjeta = AddcelCrypto.encryptHard(capturedCard?.cardNumber!!)
            val vigencia = AddcelCrypto.encryptHard(
                    presenter.formatVigencia(capturedCard?.expiryMonth!!, capturedCard?.expiryYear!!)
            )
            val tipoTarjeta =
                    StringUtil.removeAcentos(spinner_negocio_cobro_usercard_card_tipo.selectedItem as String)

            val nombre =
                    AndroidUtils.getText(til_negocio_cobro_usercard_card_cardholder_nombre.editText)
            val apellido =
                    AndroidUtils.getText(til_negocio_cobro_usercard_card_cardholder_apellido.editText)

            val email = AndroidUtils.getText(til_usercard_card_email.editText)
            val celular = AndroidUtils.getText(til_usercard_card_celular.editText)

            val entity = SPPagoEntity(
                    amount = amount,
                    comision = comision,
                    concept = concepto,
                    ct = ct,
                    establecimientoId = idEstablecimiento,
                    firma = firma,
                    imei = imei,
                    lat = lat,
                    lon = lon,
                    modelo = modelo,
                    propina = propina,
                    software = software,
                    tarjeta = tarjeta,
                    tipoTarjeta = tipoTarjeta,
                    vigencia = vigencia,
                    nombre = nombre,
                    apellido = apellido,
                    email = email,
                    celular = celular
            )

            Timber.d("Tarjeta Presente: %s", entity)


            presenter.getToken("wc874fnw847fchw8m4w9c8g8", entity)
        } else {
            showError("Escanea la tarjeta bancaria del cliente")
        }
    }

    private fun processCard(data: Intent) {
        capturedCard = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT)
        if (capturedCard != null) {
            Timber.d("DATOS TARJETA: %s", capturedCard.toString())
            AndroidUtils.setText(
                    til_negocio_cobro_usercard_card_card.editText,
                    capturedCard?.formattedCardNumber!!
            )
            AndroidUtils.setText(
                    til_negocio_cobro_usercard_card_cvv.editText,
                    capturedCard?.cvv!!
            ) //setValuesFromScan(scanResult)
            setFranquiciaLogo(capturedCard!!)

            val vigencia =
                    capturedCard?.expiryMonth.toString() + "/" + capturedCard?.expiryYear.toString()

            AndroidUtils.setText(til_negocio_cobro_usercard_card_exp.editText, vigencia)
            val capturedCardArr = data.getByteArrayExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE)
            if (capturedCardArr != null) {
                b_negocio_cobro_usercard_card_scan.setImageBitmap(parseImage(capturedCardArr))
            }
        } else {
            showError("Ocurrió un error al obtener los datos de la tarjeta bancaria")
        }
        b_negocio_cobro_usercard_card_continuar.isEnabled = shouldEnableContinuar()
    }

    private fun setFranquiciaLogo(card: CreditCard) {
        when (card.cardType) {
            CardType.VISA -> img_negocio_cobro_usercard_card_franquicia.setImageResource(
                    R.drawable.logo_visa
            )
            CardType.MASTERCARD -> img_negocio_cobro_usercard_card_franquicia.setImageResource(
                    R.drawable.logo_master
            )
            CardType.AMEX -> img_negocio_cobro_usercard_card_franquicia.setImageResource(
                    R.drawable.logo_american
            )
            else -> {
                img_negocio_cobro_usercard_card_franquicia.setImageResource(R.drawable.logo_visa)
            }
        }
    }

    private fun parseImage(bytes: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    }

    override fun clickScan() {
        permissionDisposable.add(permissions.request(Manifest.permission.CAMERA).subscribe { granted ->
            if (granted) {
                launchCardScan()
            } else {
                showPermissionDialog()
            }
        })
    }

    private fun showPermissionDialog() {
        AlertDialog.Builder(containerAct).setTitle("Aviso")
                .setMessage(
                        "Para poder capturar los datos de la tarjeta bancaria, por favor acepta los permisos solicitados por el dispositivo"
                )
                .setPositiveButton("Aceptar") { dialogInterface, _ ->
                    clickScan()
                    dialogInterface.dismiss()
                }.setNegativeButton("Cancelar") { dialogInterface, _ -> dialogInterface.dismiss() }
                .show()
    }

    override fun onToken(token: TokenEntity) {
    }

    override fun onPagoSuccess(result: SPReceiptEntity) {
        containerAct.fragmentManagerLazy.commit {
            add(
                    R.id.frame_negocio_negocio_pe_tarjeta_presente,
                    NegocioPeTarjetaPresenteResultFragment.get(result)
            )
            hide(this@NegocioPeTarjetaPresenteTarjetaFragment)
            addToBackStack(null)
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    private fun disableCardFields() {
        til_negocio_cobro_usercard_card_card.isEnabled = false
        til_negocio_cobro_usercard_card_cvv.isEnabled = false
        til_negocio_cobro_usercard_card_exp.isEnabled = false
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0 != null) {
            if (til_negocio_cobro_usercard_card_cardholder_nombre.hasFocus()) {
                if (p0.isEmpty()) {
                    til_negocio_cobro_usercard_card_cardholder_nombre.error =
                            getString(R.string.error_nombre)
                } else {
                    til_negocio_cobro_usercard_card_cardholder_nombre.error = null
                }
            } else if (til_negocio_cobro_usercard_card_cardholder_apellido.hasFocus()) {
                if (p0.isEmpty()) {
                    til_negocio_cobro_usercard_card_cardholder_apellido.error =
                            getString(R.string.error_apellido)
                } else {
                    til_negocio_cobro_usercard_card_cardholder_apellido.error = null
                }
            } else if (til_usercard_card_email.hasFocus()) {
                if (!Patterns.EMAIL_ADDRESS.matcher(p0).matches()) {
                    til_usercard_card_email.error = getText(R.string.error_email)
                } else {
                    til_usercard_card_email.error = null
                }
            }
        }
        b_negocio_cobro_usercard_card_continuar.isEnabled = shouldEnableContinuar()
    }

    private fun shouldEnableContinuar(): Boolean {
        return AndroidUtils.getText(
                til_negocio_cobro_usercard_card_cardholder_nombre.editText
        ).isNotEmpty() && AndroidUtils.getText(
                til_negocio_cobro_usercard_card_cardholder_apellido.editText
        ).isNotEmpty() && AndroidUtils.getText(
                til_usercard_card_email.editText
        ).matches(
                Patterns.EMAIL_ADDRESS.toRegex()
        ) && capturedCard != null
    }

    override fun showProgress() {
        containerAct.showProgress()
    }

    override fun hideProgress() {
        containerAct.hideProgress()
    }

    override fun showError(msg: String) {
        containerAct.showError(msg)
    }

    override fun showSuccess(msg: String) {
        containerAct.showSuccess(msg)
    }
}