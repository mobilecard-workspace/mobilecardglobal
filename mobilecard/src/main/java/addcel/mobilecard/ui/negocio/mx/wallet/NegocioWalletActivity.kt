package addcel.mobilecard.ui.negocio.mx.wallet

import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.wallet.CardEntity
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.negocio.mx.wallet.card.NegocioWalletCardFragment
import addcel.mobilecard.ui.negocio.mx.wallet.registro.NegocioWalletRegistroFragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.fragment.app.commit
import com.squareup.otto.Bus
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_negocio_wallet.*
import javax.inject.Inject

@Parcelize
data class NegocioWalletModel(val idEstablecimiento: Int) : Parcelable

interface NegocioWalletRequestCallback {
    fun onNegocioWalletRequest(resultCode: Int, data: Intent?)
}

interface NegocioWalletView {
    fun setUi()

    fun launchValidate()

    fun onHasCard(card: CardEntity)

    fun onNoCard()

    fun onError(msg: String)
}

class NegocioWalletActivity : ContainerActivity(), NegocioWalletView {


    @Inject
    lateinit var model: NegocioWalletModel
    @Inject
    lateinit var bus: Bus
    @Inject
    lateinit var presenter: NegocioWalletPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioWalletSubcomponent(NegocioWalletModule(this))
                .inject(this)
        setContentView(R.layout.activity_negocio_wallet)
        setUi()
        launchValidate()
    }

    override fun onResume() {
        super.onResume()
        bus.register(this)
    }

    override fun getRetry(): View? {
        return null
    }

    override fun onPause() {
        bus.unregister(this)
        super.onPause()
    }

    override fun setUi() {
    }

    override fun onDestroy() {
        presenter.clearDisposables()
        super.onDestroy()
    }

    override fun launchValidate() {
        showProgress()
        presenter.onValidateLaunched(model.idEstablecimiento)
    }

    override fun onHasCard(card: CardEntity) {
        hideProgress()
        fragmentManagerLazy.commit {
            add(R.id.frame_negocio_wallet, NegocioWalletCardFragment.get(card))
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    override fun onNoCard() {
        hideProgress()
        fragmentManagerLazy.commit {
            add(R.id.frame_negocio_wallet, NegocioWalletRegistroFragment.get())
            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    override fun onError(msg: String) {
        hideProgress()
        showError(msg)
        onBackPressed()
    }

    override fun showProgress() {
        if (progress_negocio_wallet.visibility == View.GONE) progress_negocio_wallet.visibility =
                View.VISIBLE
    }

    override fun hideRetry() {

    }

    override fun hideProgress() {
        if (progress_negocio_wallet.visibility == View.VISIBLE) progress_negocio_wallet.visibility =
                View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
    }

    override fun setAppToolbarTitle(title: String) {
    }

    override fun showRetry() {

    }


    companion object {

        const val REQUEST_CODE = 432
        const val RESULT_DATA = "addcel.mobilecard.ui.negocio.mx.wallet.data"

        fun get(context: Context, model: NegocioWalletModel): Intent {
            return Intent(context, NegocioWalletActivity::class.java).putExtra("model", model)
        }
    }
}
