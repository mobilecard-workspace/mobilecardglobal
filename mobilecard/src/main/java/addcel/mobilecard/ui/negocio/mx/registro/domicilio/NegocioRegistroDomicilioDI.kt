package addcel.mobilecard.ui.negocio.mx.registro.domicilio

import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.di.scope.PerFragment
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-12-06.
 */

@Module
class NegocioRegistroDomicilioModule(val fragment: NegocioRegistroDomicilioFragment) {

    @PerFragment
    @Provides
    fun provideNegocioApi(r: Retrofit): NegocioAPI {
        return NegocioAPI.create(r)
    }

    @PerFragment
    @Provides
    fun providePresenter(
            api: NegocioAPI,
            catalogoService: CatalogoService, session: SessionOperations, state: StateSession
    ): NegocioRegistroDomicilioPresenter {
        return NegocioRegistroDomicilioPresenterImpl(
                api,
                catalogoService,
                session, state,
                CompositeDisposable(),
                fragment
        )
    }
}

@PerFragment
@Subcomponent(modules = [NegocioRegistroDomicilioModule::class])
interface NegocioRegistroDomicilioSubcomponent {
    fun inject(fragment: NegocioRegistroDomicilioFragment)
}