package addcel.mobilecard.ui.negocio.mx.cobro.scan;

import androidx.annotation.NonNull;

import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.ui.ScreenView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * ADDCEL on 13/09/18.
 */
public interface NegocioCobroQrScanContract {
    interface View extends ScreenView, ZXingScannerView.ResultHandler {

        int getTransactionId();

        void onScanSuccess(SPReceiptEntity resultEntity);

        void onIdReceived(@NonNull String id);

        void clickId();
    }

    interface Presenter {
        void clearDisposable();

        void validateScanResult(@NonNull String result);

        void getResultById();
    }
}
