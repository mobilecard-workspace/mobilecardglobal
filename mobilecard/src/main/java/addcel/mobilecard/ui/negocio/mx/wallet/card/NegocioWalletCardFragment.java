package addcel.mobilecard.ui.negocio.mx.wallet.card;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.common.base.Strings;
import com.vinaygaba.creditcardview.CreditCardView;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.ui.negocio.mx.wallet.NegocioWalletActivity;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.BundleBuilder;

/**
 * ADDCEL on 12/11/18.
 */
public final class NegocioWalletCardFragment extends Fragment
        implements NegocioWalletCardContract.View {

    @Inject
    NegocioWalletActivity activity;
    @Inject
    NegocioWalletCardContract.Presenter presenter;

    private FrameLayout cardContainer;
    private TextView balanceView;
    private TextView clabeView;
    private LinearLayout balanceContainer;

    public NegocioWalletCardFragment() {
    }

    public static synchronized NegocioWalletCardFragment get(@NonNull CardEntity card) {
        Bundle arguments = new BundleBuilder().putParcelable("card", card).build();
        NegocioWalletCardFragment fragment = new NegocioWalletCardFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .negocioWalletCardSubcomponent(new NegocioWalletCardModule(this))
                .inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_my_mobilecard_previvale_display, container, false);
        setUI(v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        launchCardCreation();
        setBalance(presenter.getBalance());
        setClabe(presenter.getModel().getClabe());
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void setUI(@NonNull View v) {
        cardContainer = v.findViewById(R.id.card_container);
        balanceView = v.findViewById(R.id.view_mymobilecard_display_balance);
        clabeView = v.findViewById(R.id.view_mymobilecard_display_clabe);
        clabeView.setOnClickListener(view -> clickSave());
        balanceContainer = v.findViewById(R.id.card_mymobilecard_display_balance);
        v.findViewById(R.id.b_mymc_info)
                .setOnClickListener(view -> new AlertDialog.Builder(activity).setTitle("Aviso")
                        .setMessage(R.string.txt_mymc_info)
                        .setPositiveButton("Aceptar", (dialogInterface, i) -> dialogInterface.dismiss()).show());
    }

    @Override
    public void setBalance(String balance) {
        if (!Strings.isNullOrEmpty(balance)) {
            balanceView.setText(getString(R.string.txt_negocio_card_saldo, balance));
        } else {
            balanceView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setClabe(String clabe) {
        if (!Strings.isNullOrEmpty(clabe)) {
            clabeView.setText(getString(R.string.txt_negocio_card_clabe, clabe));
        } else {
            clabeView.setVisibility(View.GONE);
        }
    }

    @Override
    public void launchCardCreation() {
        CreditCardView cardView = presenter.buildCardView(activity);
        cardContainer.addView(cardView);
    }

    @Override
    public void clickSave() {
        ClipboardManager clipboard =
                (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("MI CUENTA CLABE", AndroidUtils.getText(clabeView));
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
            Toast.makeText(activity, "Copiado en portapapeles", Toast.LENGTH_SHORT).show();
        }
    }
}
