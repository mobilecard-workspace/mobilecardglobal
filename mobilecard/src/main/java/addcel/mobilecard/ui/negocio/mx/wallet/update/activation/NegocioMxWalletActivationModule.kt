package addcel.mobilecard.ui.negocio.mx.wallet.update.activation

import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse
import addcel.mobilecard.data.net.wallet.WalletAPI
import addcel.mobilecard.di.scope.PerActivity
import android.widget.ArrayAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

/**
 * ADDCEL on 2019-08-30.
 */
@Module
class NegocioMxWalletActivationModule(
        private val activity: NegocioMxWalletActivationActivity
) {
    @PerActivity
    @Provides
    fun provideModel(): NegocioMxWalletActivationModel {
        return activity.intent.getParcelableExtra("model")!!
    }

    @PerActivity
    @Provides
    fun provideAdapter(): ArrayAdapter<EstadoResponse.EstadoEntity> {
        val adapter =
                ArrayAdapter<EstadoResponse.EstadoEntity>(activity, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }

    @PerActivity
    @Provides
    fun providePresenter(
            api: WalletAPI, catalogo: CatalogoService, session: SessionOperations
    ): NegocioMxWalletActivationPresenter {
        return NegocioMxWalletActivationPresenterImpl(
                wallet = api,
                catalogo = catalogo,
                session = session,
                view = activity
        )
    }
}

@PerActivity
@Subcomponent(modules = [NegocioMxWalletActivationModule::class])
interface NegocioMxWalletActivationSubcomponent {
    fun inject(activity: NegocioMxWalletActivationActivity)
}