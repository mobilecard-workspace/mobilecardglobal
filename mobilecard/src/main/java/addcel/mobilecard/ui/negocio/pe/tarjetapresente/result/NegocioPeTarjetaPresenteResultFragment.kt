package addcel.mobilecard.ui.negocio.pe.tarjetapresente.result

import addcel.mobilecard.R
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.ui.ContainerScreenView
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.NegocioPeTarjetaPresenteActivity
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.common.base.Strings
import com.squareup.phrase.Phrase
import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 2019-07-15.
 */
interface View {
    fun setUIData()

    fun clickQR()

    fun clickOk()
}

class NegocioPeTarjetaPresenteResultFragment : Fragment(), View {

    private val format = NumberFormat.getCurrencyInstance(Locale("es", "PE"))

    lateinit var presenter: NegocioPeTarjetaPresenteResultPresenter
    private var imgView: ImageView? = null
    private var titleView: TextView? = null
    private var msgView: TextView? = null
    private var qrButton: Button? = null

    companion object {
        fun get(result: SPReceiptEntity): NegocioPeTarjetaPresenteResultFragment {
            val args = BundleBuilder().putParcelable("result", result).build()
            val fragment = NegocioPeTarjetaPresenteResultFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter =
                NegocioPeTarjetaPresenteResultPresenter(arguments?.getParcelable("result")!!, this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): android.view.View? {
        val view = inflater.inflate(R.layout.screen_scanpay_result, container, false)
        imgView = view.findViewById(R.id.img_scanpay_result)
        titleView = view.findViewById(R.id.view_scanpay_result_title)
        msgView = view.findViewById(R.id.view_scanpay_result_msg)
        val okButton = view.findViewById<Button>(R.id.b_scanpay_result_ok)
        okButton.setOnClickListener { clickOk() }
        qrButton = view.findViewById(R.id.b_scanpay_result_qr)
        qrButton!!.setOnClickListener { clickQR() }
        return view
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkIfFromScanOrNegocioAndSetFinished()
        setUIData()
    }

    override fun onResume() {
        super.onResume()
        checkIfFromScanOrNegocioAndSetFinished()
    }

    override fun setUIData() {
        imgView!!.setImageResource(
                if (presenter.spReceiptEntity.code == 0) R.drawable.ic_historial_success else R.drawable.ic_historial_error
        )
        titleView!!.text = presenter.spReceiptEntity.message
        msgView!!.text = buildResultMsg()
        qrButton!!.visibility = android.view.View.GONE
    }

    override fun clickQR() {
    }

    override fun clickOk() {
        (activity as NegocioPeTarjetaPresenteActivity).finish()
    }

    private fun checkIfFromScanOrNegocioAndSetFinished() {
        (activity as NegocioPeTarjetaPresenteActivity).setStreenStatus(
                ContainerScreenView.STATUS_FINISHED
        )
    }

    private fun buildResultMsg(): CharSequence {

        return if (presenter.spReceiptEntity.code == 0) {
            Phrase.from(resources, R.string.txt_confirmacion_pago_msg)
                    .put("mensaje", Strings.nullToEmpty(presenter.spReceiptEntity.message))
                    .put("idbitacora", presenter.spReceiptEntity.idTransaccion.toString())
                    .put("auth", Strings.nullToEmpty(presenter.spReceiptEntity.authNumber))
                    .put("tdc", Strings.nullToEmpty(presenter.spReceiptEntity.maskedPAN))
                    .put(
                            "date",
                            DateFormat.format("dd-MM-yyyy HH:mm:ss", presenter.spReceiptEntity.dateTime)
                    )
                    .put("monto", format.format(presenter.spReceiptEntity.amount)).format()
        } else {
            Phrase.from(resources, R.string.txt_confirmacion_pago_error)
                    .put("error", presenter.spReceiptEntity.code.toString())
                    .put("mensaje", Strings.nullToEmpty(presenter.spReceiptEntity.message)).format()
        }
    }
}

