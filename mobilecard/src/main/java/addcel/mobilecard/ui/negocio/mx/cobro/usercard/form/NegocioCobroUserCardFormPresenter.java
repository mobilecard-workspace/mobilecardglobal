package addcel.mobilecard.ui.negocio.mx.cobro.usercard.form;

import androidx.annotation.NonNull;

import com.google.common.base.Strings;

import java.text.NumberFormat;

import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 07/09/18.
 */
public class NegocioCobroUserCardFormPresenter
        implements NegocioCobroUserCardFormContract.Presenter {
    private final NumberFormat numberFormat;

    NegocioCobroUserCardFormPresenter(NumberFormat numberFormat,
                                      NegocioCobroUserCardFormContract.View view) {
        this.numberFormat = numberFormat;
    }

    @Override
    public double calculateTotal(@NonNull CharSequence monto, @NonNull CharSequence propina) {
        double dMonto =
                StringUtil.isDecimalAmount(Strings.nullToEmpty(monto.toString())) ? Double.valueOf(
                        Strings.nullToEmpty(monto.toString())) : 0.0;
        double dPropina =
                StringUtil.isDecimalAmount(Strings.nullToEmpty(propina.toString())) ? Double.valueOf(
                        Strings.nullToEmpty(propina.toString())) : 0.0;
        return dMonto + dPropina;
    }

    @Override
    public String formatAmount(double cantidad) {
        return numberFormat.format(cantidad);
    }
}
