package addcel.mobilecard.ui.negocio.mx.cobro.create.form;

import com.mobsandgeeks.saripaar.Validator;

/**
 * ADDCEL on 07/09/18.
 */
interface NegocioCobroCreateFormContract {
    interface View extends Validator.ValidationListener {
        double getImporte();

        double getPropina();

        String getConcepto();

        void clickContinuar();
    }

    interface Presenter {

    }
}
