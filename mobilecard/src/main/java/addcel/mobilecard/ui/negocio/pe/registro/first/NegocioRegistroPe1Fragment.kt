package addcel.mobilecard.ui.negocio.pe.registro.first

import addcel.mobilecard.R
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeActivity
import addcel.mobilecard.ui.negocio.pe.registro.menu.TipoRegistro
import addcel.mobilecard.ui.negocio.pe.registro.persona.PePersonaFiscal
import addcel.mobilecard.ui.negocio.pe.registro.second.NegocioRegistroPE2FragmentData
import addcel.mobilecard.ui.negocio.pe.registro.second.NegocioRegistroPe2Fragment
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.validation.ValidationUtils
import android.os.Bundle
import android.os.Parcelable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.transaction
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_pe_negocio_registro_1.*

/**
 * ADDCEL on 2019-07-10.
 */
@Parcelize
data class NegocioRegistroPE1FragmentData(
        val perfil: TipoRegistro,
        val persona: PePersonaFiscal, val idEstablecimiento: Int
) : Parcelable

class NegocioRegistroPe1Fragment : Fragment() {
    companion object {
        fun get(configData: NegocioRegistroPE1FragmentData): NegocioRegistroPe1Fragment {
            val args = BundleBuilder().putParcelable("data", configData).build()
            val fragment = NegocioRegistroPe1Fragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var configData: NegocioRegistroPE1FragmentData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configData = arguments?.getParcelable("data")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_pe_negocio_registro_1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (configData.persona == PePersonaFiscal.NATURAL) {
            til_pe_negocio_registro_ruc.hint = "RUC (Opcional)"
        } else {
            til_pe_negocio_registro_ruc.hint = "RUC"
        }

        b_pe_negocio_registro_1_guardar.setOnClickListener {

            ValidationUtils.clearViewErrors(
                    til_pe_negocio_registro_ruc,
                    til_pe_negocio_registro_razon_social, til_pe_negocio_registro_nombre_comercio,
                    til_pe_negocio_registro_direccion_comercio, til_pe_negocio_registro_dni,
                    til_pe_negocio_registro_verificador, til_pe_negocio_registro_nombres,
                    til_pe_negocio_registro_paterno, til_pe_negocio_registro_materno
            )

            if (validate()) {
                val configData = NegocioRegistroPE2FragmentData(
                        configData,
                        AndroidUtils.getText(til_pe_negocio_registro_ruc.editText),
                        AndroidUtils.getText(til_pe_negocio_registro_razon_social.editText),
                        AndroidUtils.getText(til_pe_negocio_registro_nombre_comercio.editText),
                        AndroidUtils.getText(til_pe_negocio_registro_direccion_comercio.editText),
                        AndroidUtils.getText(til_pe_negocio_registro_dni.editText),
                        AndroidUtils.getText(til_pe_negocio_registro_verificador.editText), "DNI",
                        AndroidUtils.getText(til_pe_negocio_registro_nombres.editText),
                        AndroidUtils.getText(til_pe_negocio_registro_paterno.editText),
                        AndroidUtils.getText(til_pe_negocio_registro_materno.editText)
                )

                (activity as NegocioRegistroPeActivity).fragmentManagerLazy.commit {
                    add(R.id.frame_pe_negocio_registro, NegocioRegistroPe2Fragment.get(configData))
                    hide(this@NegocioRegistroPe1Fragment)
                    addToBackStack(null)
                    setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                }
            }
        }
    }

    private fun validate(): Boolean {
        var isValid = true

        if (configData.persona == PePersonaFiscal.JURIDICA && AndroidUtils.getText(
                        til_pe_negocio_registro_ruc.editText
                ).isEmpty()
        ) {
            til_pe_negocio_registro_ruc.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (configData.persona == PePersonaFiscal.JURIDICA && AndroidUtils.getText(
                        til_pe_negocio_registro_razon_social.editText
                ).isEmpty()
        ) {
            til_pe_negocio_registro_razon_social.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (AndroidUtils.getText(til_pe_negocio_registro_nombre_comercio.editText).isEmpty()) {
            til_pe_negocio_registro_nombre_comercio.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (AndroidUtils.getText(til_pe_negocio_registro_direccion_comercio.editText).isEmpty()) {
            til_pe_negocio_registro_direccion_comercio.error =
                    getString(R.string.error_campo_generico)
            isValid = false
        }

        if (AndroidUtils.getText(til_pe_negocio_registro_dni.editText).isEmpty()) {
            til_pe_negocio_registro_dni.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (AndroidUtils.getText(
                        til_pe_negocio_registro_dni.editText
                ).length < 8 || !TextUtils.isDigitsOnly(
                        AndroidUtils.getText(til_pe_negocio_registro_dni.editText)
                )
        ) {
            til_pe_negocio_registro_dni.error = "El DNI no es válido"
            isValid = false
        }

        if (AndroidUtils.getText(til_pe_negocio_registro_verificador.editText).isEmpty()) {
            til_pe_negocio_registro_verificador.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (AndroidUtils.getText(til_pe_negocio_registro_nombres.editText).isEmpty()) {
            til_pe_negocio_registro_nombres.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (AndroidUtils.getText(til_pe_negocio_registro_paterno.editText).isEmpty()) {
            til_pe_negocio_registro_paterno.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        if (AndroidUtils.getText(til_pe_negocio_registro_materno.editText).isEmpty()) {
            til_pe_negocio_registro_materno.error = getString(R.string.error_campo_generico)
            isValid = false
        }

        return isValid
    }
}