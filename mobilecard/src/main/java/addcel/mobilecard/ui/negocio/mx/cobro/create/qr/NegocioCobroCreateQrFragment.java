package addcel.mobilecard.ui.negocio.mx.cobro.create.qr;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import addcel.mobilecard.McConstants;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroComponentCreator;
import addcel.mobilecard.ui.usuario.scanpay.result.ScanResultFragment;
import addcel.mobilecard.utils.BundleBuilder;

/**
 * ADDCEL on 07/09/18.
 */
public final class NegocioCobroCreateQrFragment extends Fragment
        implements NegocioCobroCreateQrContract.View {
    @Inject
    NegocioCobroActivity activity;
    @Inject
    NegocioCobroCreateQrContract.Presenter presenter;
    private ImageView qrView;
    private TextView idView;

    public NegocioCobroCreateQrFragment() {
    }

    public static synchronized NegocioCobroCreateQrFragment get(double monto, double propina,
                                                                String concepto) {
        Bundle args = new BundleBuilder().putDouble("monto", monto)
                .putDouble("propina", propina)
                .putString("concepto", concepto)
                .build();
        NegocioCobroCreateQrFragment fragment = new NegocioCobroCreateQrFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NegocioCobroComponentCreator.negocioCobroCreateQrSubcomponent(Mobilecard.get(),
                new NegocioCobroCreateQrModule(this)).inject(this);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_negocio_cobro_create_qr, container, false);
        qrView = view.findViewById(R.id.view_negocio_cobro_create_qr_code);
        idView = view.findViewById(R.id.view_negocio_cobro_create_qr_id);
        view.findViewById(R.id.b_negocio_cobro_create_qr_confirmar).setOnClickListener(v -> clickConfirm());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        activity.hideBottomMenu();
        presenter.checkComision();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!isHidden()) {
            activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
            activity.hideBottomMenu();
        }
    }

    @Override
    public void onDestroy() {
        presenter.clearCompositeDisposable();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void onComisionReceived(SPComisionEntity comision) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        presenter.createQrCode(width - (width / 4), Color.BLACK, Color.TRANSPARENT, comision);
    }

    @Override
    public void setQrCode(Bitmap bitmap, SPComisionEntity comision) {
        if (isVisible()) {
            qrView.setImageBitmap(bitmap);
            presenter.getId(comision);
        }
    }

    @Override
    public void setId(int id) {
        if (isVisible()) {
            String idText = getString(R.string.txt_negocio_cobro_create_qr_id, id);
            idView.setText(idText);
            idView.setPaintFlags(idView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            idView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void clickConfirm() {
        if (presenter.isIdSet()) {
            presenter.validateId();
        } else {
            showError(getString(R.string.error_transaction_incomplete));
        }
    }

    @Override
    public void onConfirmId(SPReceiptEntity result) {
        activity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.frame_negocio_cobro, ScanResultFragment.Companion.get(result, McConstants.PAIS_ID_MX, Boolean.TRUE))
                .addToBackStack(null)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .hide(this)
                .commit();
    }
}
