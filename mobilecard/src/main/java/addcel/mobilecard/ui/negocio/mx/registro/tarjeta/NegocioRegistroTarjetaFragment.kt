package addcel.mobilecard.ui.negocio.mx.registro.tarjeta

import addcel.mobilecard.McRegexPatterns
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.negocios.entity.NegocioCreateRequest
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroModel
import addcel.mobilecard.ui.negocio.mx.registro.NegocioRegistroActivity
import addcel.mobilecard.ui.notification.ResultNotificationActivity
import addcel.mobilecard.ui.notification.ResultNotifictionViewModel
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.BundleBuilder
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.fragment.app.Fragment
import com.afollestad.vvalidator.form
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_wallet_registro.*
import javax.inject.Inject

/**
 * ADDCEL on 2019-12-16.
 */


@Parcelize
data class NegocioRegistroTarjetaModel(val altaRequest: NegocioCreateRequest) : Parcelable

interface NegocioRegistroTarjetaView : ScreenView {

    fun configFields()

    fun configEstadoSpinner()

    fun showRetry()

    fun hideRetry()

    fun clickRetry()

    fun launchEstados()

    fun setEstados(estados: List<EstadoResponse.EstadoEntity>)

    fun launchCurp()

    fun launchRegistro()

    fun onLoginSuccess(msg: String, withCard: Boolean)
}

class NegocioRegistroTarjetaFragment : Fragment(), NegocioRegistroTarjetaView {

    companion object {

        private const val CURP_URL = "https://www.gob.mx/curp/"
        private val vowels = listOf('A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u')

        fun get(altaRequest: NegocioCreateRequest): NegocioRegistroTarjetaFragment {
            val args =
                    BundleBuilder().putParcelable("model", NegocioRegistroTarjetaModel(altaRequest)).build()
            val frag = NegocioRegistroTarjetaFragment()
            frag.arguments = args
            return frag
        }
    }

    @Inject
    lateinit var model: NegocioRegistroTarjetaModel
    @Inject
    lateinit var presenter: NegocioRegistroTarjetaPresenter
    @Inject
    lateinit var adapter: ArrayAdapter<EstadoResponse.EstadoEntity>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioRegistroTarjetaSubcomponent(
                NegocioRegistroTarjetaModule(
                        this
                )
        ).inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_negocio_wallet_registro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as NegocioRegistroActivity).retry?.findViewById<Button>(R.id.b_retry)
                ?.setOnClickListener {
                    clickRetry()
                }

        configFields()

        b_previvale_curp.setOnClickListener { launchCurp() }

        form {
            inputLayout(R.id.til_previvale_add_direccion) {
                isNotEmpty().description(R.string.error_campo_generico)
            }
            inputLayout(R.id.til_previvale_add_colonia) {
                isNotEmpty().description(R.string.error_campo_generico)
                length().atLeast(3).description("Ingresa una colonia válida")
            }
            inputLayout(R.id.til_previvale_add_ciudad) {
                isNotEmpty().description(R.string.error_campo_generico)
                length().atLeast(3).description("Ingresa una ciudad válida")
            }
            inputLayout(R.id.til_previvale_add_cp) {
                isNotEmpty().description(R.string.error_campo_generico)
                isNumber().description("Ingresa un código postal válido")
                length().exactly(5).description("Ingresa un código postal válido")
            }
            inputLayout(R.id.til_previvale_add_curp) {
                matches(McRegexPatterns.CURP).description(R.string.error_rfc_curp)
            }
            submitWith(R.id.b_previvale_activate) {
                launchRegistro()
            }
        }

        configEstadoSpinner()
        launchEstados()
    }

    override fun configFields() {
        label_previvale_activate.text = "Activar tarjeta"
        label_previvale_activate_pan.text = "INFORMACIÓN DE TU TARJETA MOBILECARD"
        text_previvale_activate_pan.visibility = View.GONE
        til_previvale_add_rfc.visibility = View.GONE
        b_previvale_activate.text = "Aceptar"
    }

    override fun configEstadoSpinner() {
        spinner_previvale_add_estado.adapter = adapter
    }

    override fun showRetry() {
        (activity as NegocioRegistroActivity).showRetry()
    }

    override fun hideRetry() {
        (activity as NegocioRegistroActivity).hideRetry()
    }

    override fun clickRetry() {
        launchEstados()
    }

    override fun launchEstados() {
        if (adapter.count == 0) presenter.getEstados()
    }

    override fun setEstados(estados: List<EstadoResponse.EstadoEntity>) {
        adapter.clear()
        adapter.addAll(estados)
    }

    override fun launchCurp() {
        try {
            val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(CURP_URL))
            startActivity(myIntent)
        } catch (e: ActivityNotFoundException) {
            showError(e.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.OPERATION))
            e.printStackTrace()
        }
    }

    override fun launchRegistro() {
        val direccion = StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_previvale_add_direccion.editText))
        val colonia = StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_previvale_add_colonia.editText))
        val ciudad = StringUtil.removeAcentosMultiple(AndroidUtils.getText(til_previvale_add_ciudad.editText))
        val estado = (spinner_previvale_add_estado.selectedItem as EstadoResponse.EstadoEntity).id
        val cp = AndroidUtils.getText(til_previvale_add_cp.editText)
        val curp = AndroidUtils.getText(til_previvale_add_curp.editText)
        val rfc = getRfcFromCurp(curp)

        val request = model.altaRequest.copy(
                direccion = direccion,
                colonia = colonia,
                ciudad = ciudad,
                idEstado = estado,
                cp = cp,
                curp = curp,
                rfc = rfc
        )

        presenter.createNegocio(request)
    }

    private fun getRfcFromCurp(curp: String): String {
        return if (vowels.contains(curp[1])) {
            curp.substring(0, 10)
        } else {
            curp.substring(0, 9)
        }
    }

    override fun onLoginSuccess(msg: String, withCard: Boolean) {

        val useCase = if (withCard) ResultNotificationActivity.USE_CASE_COMERCIO_REGISTRO_MC_SUCCESS
        else ResultNotificationActivity.USE_CASE_COMERCIO_REGISTRO_MC_ERROR

        ResultNotificationActivity.startForResult(activity!!, ResultNotifictionViewModel(
                useCase,
                PaisResponse.PaisEntity.MX,
                R.string.txt_mimobilecard,
                msg,
                R.drawable.t_grande_naranja, R.string.txt_continuar
        ))
    }

    override fun showSuccess(msg: String) {
        (activity as NegocioRegistroActivity).showSuccess(msg)
    }

    override fun showError(msg: String) {
        (activity as NegocioRegistroActivity).showError(msg)
    }

    override fun hideProgress() {
        (activity as NegocioRegistroActivity).hideProgress()
    }

    override fun showProgress() {
        (activity as NegocioRegistroActivity).showProgress()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ResultNotificationActivity.REQUEST_CODE) {
            if (data != null) {
                if (activity != null) {
                    val useCaseResult = data.getIntExtra(ResultNotificationActivity.DATA, -1)
                    val useCaseMenu = if (useCaseResult == ResultNotificationActivity.USE_CASE_COMERCIO_REGISTRO_MC_ERROR) NegocioCobroActivity.USE_CASE_CARD
                    else NegocioCobroActivity.USE_CASE_MENU
                    startActivity(NegocioCobroActivity.get(activity!!, NegocioCobroModel(useCaseMenu)).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                    activity!!.finish()
                }
            }
        }
    }

}