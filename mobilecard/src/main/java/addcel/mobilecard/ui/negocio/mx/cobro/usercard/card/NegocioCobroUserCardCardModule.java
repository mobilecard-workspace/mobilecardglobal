package addcel.mobilecard.ui.negocio.mx.cobro.usercard.card;

import android.app.DatePickerDialog;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Calendar;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.SPApi;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 10/09/18.
 */
@Module
public final class NegocioCobroUserCardCardModule {
    private final NegocioCobroUserCardCardFragment fragment;

    public NegocioCobroUserCardCardModule(NegocioCobroUserCardCardFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    NegocioCobroActivity provideActivity() {
        return (NegocioCobroActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }

    @PerFragment
    @Provides
    Bundle provideArguments() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    double provideMonto(Bundle args) {
        return args.getDouble("importe");
    }

    @PerFragment
    @Provides
    @Named("propina")
    double providePropina(Bundle args) {
        return args.getDouble("propina");
    }

    @PerFragment
    @Provides
    String provideConcepto(Bundle args) {
        return args.getString("concepto");
    }

    @PerFragment
    @Provides
    Calendar provideVigenciaCalendar() {
        return Calendar.getInstance();
    }

    @PerFragment
    @Provides
    DatePickerDialog datePickerDialog(NegocioCobroActivity activity,
                                      Calendar calendar) {
        DatePickerDialog datePickerDialog;
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            datePickerDialog =
                    new FixedHoloDatePickerDialog(activity, fragment, calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog =
                        new DatePickerDialog(activity, R.style.CustomDatePickerDialogTheme, fragment,
                                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                calendar.get(Calendar.DAY_OF_MONTH));
            } else {
                // R.style.CustomDatePickerDialogTheme
                datePickerDialog = new DatePickerDialog(activity, fragment, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            }
        }

        datePickerDialog.getDatePicker()
                .findViewById(Resources.getSystem().getIdentifier("day", "id", "android"))
                .setVisibility(View.GONE);
        return datePickerDialog;
    }

    @PerFragment
    @Provides
    SPApi provideService(Retrofit retrofit) {
        return SPApi.Companion.provide(retrofit);
    }

    @PerFragment
    @Provides
    TokenizerAPI provideTokenizer(Retrofit retrofit) {
        return TokenizerAPI.Companion.provideTokenizerAPI(retrofit);
    }

    @PerFragment
    @Provides
    NegocioCobroUserCardCardContract.Presenter providePresenter(SPApi api,
                                                                TokenizerAPI tokenizer, SessionOperations session, double monto,
                                                                @Named("propina") double propina, String concepto) {
        return new NegocioCobroUserCardCardPresenter(api, tokenizer, session.getNegocio(),
                session.getMinimalLocationData(), monto, propina, concepto, fragment);
    }
}
