package addcel.mobilecard.ui.negocio.mx.cobro.usercard.form;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 07/09/18.
 */
@PerFragment
@Subcomponent(modules = NegocioCobroUserCardFormModule.class)
public interface NegocioCobroUserCardFormSubcomponent {
    void inject(NegocioCobroUserCardFormFragment fragment);
}
