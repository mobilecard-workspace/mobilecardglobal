package addcel.mobilecard.ui.negocio.pe.menu

import addcel.mobilecard.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.hdodenhof.circleimageview.CircleImageView

/**
 * ADDCEL on 2019-07-12.
 */
class NegocioMenuPeAdapter(
        private val items: List<NegocioPeruMenuItems>,
        private val view: NegocioMenuPeView
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> {
                val v =
                        LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false)
                ItemViewHolder(v)
            }
            TYPE_HEADER -> {
                val v = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_header_negocio_menu, parent, false)
                HeaderViewHolder(view, v)
            }
            else -> throw RuntimeException(
                    "there is no type that matches the type $viewType + make sure your using types correctly"
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            val model = getItem(position)
            holder.icon.setImageResource(model.icon)
            holder.text.setText(model.text)
        } else if (holder is HeaderViewHolder) {
            holder.name.text = view.getNombreNegocio()
        }
    }

    override fun getItemCount(): Int {
        return items.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    fun getItem(pos: Int): NegocioPeruMenuItems {
        return items[pos - 1]
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val icon: ImageView = view.findViewById(R.id.img_menu_item_icon)
        val text: TextView = view.findViewById(R.id.text_menu_item_name)
    }

    class HeaderViewHolder(menuView: NegocioMenuPeView, view: View) :
            RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.label_negocio_peru_menu_profile)
        val icon: CircleImageView = view.findViewById(R.id.b_negocio_peru_menu_profile)
        val qr: CircleImageView = view.findViewById(R.id.img_negocio_peru_menu_profile)

        init {
            qr.setOnClickListener {
                menuView.launchProfile()
            }
        }
    }
}