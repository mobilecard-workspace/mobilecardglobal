package addcel.mobilecard.ui.negocio.mx.historial;

import java.util.List;

import addcel.mobilecard.data.net.scanpay.model.SPCuentaEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 11/14/18.
 */
public interface NegocioHistorialContract {
    interface View extends ScreenView {

        void setCuentas(List<SPCuentaEntity> cuentas);

        void notCuentas();

        void serverError();

        void clickReintentar();
    }

    interface Presenter {
        void clearDisposables();

        void getCuentas(int idPais);
    }
}
