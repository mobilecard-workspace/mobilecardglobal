package addcel.mobilecard.ui.negocio.pe.wallet

import addcel.mobilecard.data.net.tebca.CommerceAccountsAPI
import addcel.mobilecard.di.scope.PerActivity
import addcel.mobilecard.domain.peru.NegocioPeWalletInteractor
import addcel.mobilecard.domain.peru.NegocioPeWalletInteractorImpl
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-08-28.
 */

@Module
class NegocioPeWalletModule(val activity: NegocioPeWalletActivity) {

    @PerActivity
    @Provides
    fun provideModel(): NegocioPeWalletModel {
        return activity.intent.getParcelableExtra("model")!!
    }

    @PerActivity
    @Provides
    fun provideAccountApi(retrofit: Retrofit): CommerceAccountsAPI {
        return CommerceAccountsAPI.get(retrofit)
    }

    @PerActivity
    @Provides
    fun provideInteractor(
            accounts: CommerceAccountsAPI,
            model: NegocioPeWalletModel
    ): NegocioPeWalletInteractor {
        return NegocioPeWalletInteractorImpl(
                accounts,
                model.idEstablecimiento,
                CompositeDisposable()
        )
    }

    @PerActivity
    @Provides
    fun providePresenter(
            interactor: NegocioPeWalletInteractor
    ): NegocioPeWalletPresenter {
        return NegocioPeWalletPresenterImpl(interactor, activity)
    }
}

@PerActivity
@Subcomponent(modules = [NegocioPeWalletModule::class])
interface NegocioPeWalletSubcomponent {
    fun inject(activity: NegocioPeWalletActivity)
}