package addcel.mobilecard.ui.negocio.mx.cobro;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.di.scope.PerActivity;
import addcel.mobilecard.ui.custom.extension.DetachableClickListener;
import addcel.mobilecard.ui.login.LoginActivity;
import addcel.mobilecard.ui.login.LoginContract;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 10/09/18.
 */
@Module
public final class NegocioCobroModule {
    private final NegocioCobroActivity activity;

    NegocioCobroModule(NegocioCobroActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    NegocioCobroModel provideModel() {
        return activity.getIntent().getParcelableExtra("model");
    }


    @PerActivity
    @Provides
    @Named("okListener")
    DetachableClickListener provideOkListener(SessionOperations session) {
        return DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                session.logout();
                activity.startActivity(
                        LoginActivity.Companion.get(activity, LoginContract.UseCase.PAIS, null, null).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                activity.finish();
            }
        });
    }

    @PerActivity
    @Provides
    @Named("cancelListener")
    DetachableClickListener provideCancelListener() {
        return DetachableClickListener.wrap(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    @PerActivity
    @Provides
    AlertDialog provideLogout(@Named("okListener") DetachableClickListener okListener, @Named("cancelListener") DetachableClickListener cancelListener) {
        return new AlertDialog.Builder(activity).setMessage(R.string.nav_logout)
                .setPositiveButton(android.R.string.ok, okListener)
                .setNegativeButton(android.R.string.cancel, cancelListener)
                .create();
    }
}
