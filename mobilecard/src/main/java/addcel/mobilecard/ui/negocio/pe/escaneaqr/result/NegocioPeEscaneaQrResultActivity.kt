package addcel.mobilecard.ui.negocio.pe.escaneaqr.result

import addcel.mobilecard.R
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.wallet.Transaction
import addcel.mobilecard.utils.BundleBuilder
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.format.DateFormat
import android.text.style.ForegroundColorSpan
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.squareup.phrase.Phrase
import kotlinx.android.synthetic.main.activity_negocio_pe_escanea_qr_result.*
import java.text.NumberFormat

interface View {
    fun setReceiptInfo(receipt: SPReceiptEntity)

    fun setCardInfo(receipt: SPReceiptEntity)

    fun loadImage(url: String, container: ImageView)

    fun clickShare(receipt: Transaction)
}

class NegocioPeEscaneaQrResultActivity : AppCompatActivity(), View {
    override fun setCardInfo(receipt: SPReceiptEntity) {
        val maskedPan = receipt.maskedPAN

        val info = Phrase.from(this, R.string.txt_historial_receipt_card).put("name", "")
                .put("card", maskedPan).put("expiry", "**/**").put("cvv", "***").format()

        view_edocuenta_receipt_card_info.text = info
    }

    override fun setReceiptInfo(receipt: SPReceiptEntity) {

        toolbar_negocio_pe_escanea.title = getTitleSpan(receipt)

        val formatter = NumberFormat.getCurrencyInstance()

        val icon = getStatusIcon(receipt.status)
        img_historial_status.setImageResource(icon)
        label_historial_fecha.text = DateFormat.format("dd/MM/yyyy HH:mm:ss", receipt.dateTime)
        label_historial_cuenta.text = "Autorización compra: "
        view_historial_cuenta.text = receipt.authNumber
        view_historial_total.text = formatter.format(receipt.amount)

        loadImage("", img_edocuenta_receipt_card)

        setCardInfo(receipt)
    }

    override fun loadImage(url: String, container: ImageView) {
        container.setImageResource(R.drawable.full_mobilecard_h)
    }

    override fun clickShare(receipt: Transaction) {

    }

    companion object {

        fun get(context: Context, result: SPReceiptEntity): Intent {
            return Intent(context, NegocioPeEscaneaQrResultActivity::class.java).putExtras(
                    BundleBuilder().putParcelable("result", result).build()
            )
        }
    }

    lateinit var result: SPReceiptEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_pe_escanea_qr_result)

        result = intent.extras?.getParcelable("result")!!

        setReceiptInfo(result)
    }

    private fun getStatusIcon(status: Int): Int {
        return if (status == 1) R.drawable.ic_historial_success else if (status == 0) R.drawable.ic_historial_error else R.drawable.ic_historial_processing
    }

    private fun getStatusText(status: Int): String {
        return if (status == 1) "EXITOSA" else if (status == 0) "FALLIDA" else "EN PROCESO"
    }

    private fun getStatusColor(status: Int): Int {
        return if (status == 1) R.color.colorAccept else if (status == 0) R.color.colorDeny else R.color.colorProcessing
    }

    private fun getTitleSpan(receipt: SPReceiptEntity): CharSequence {
        val firstPart = SpannableString("OPERACIÓN ")
        val lastPart = SpannableString(getStatusText(receipt.status))

        firstPart.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.colorBlack)), 0,
                firstPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        lastPart.setSpan(
                ForegroundColorSpan(
                        ContextCompat.getColor(applicationContext, getStatusColor(receipt.status))
                ), 0,
                lastPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return TextUtils.concat(firstPart, lastPart)
    }
}
