package addcel.mobilecard.ui.negocio.mx.cobro.secure;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.common.base.Strings;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.scanpay.secure.ScanPaySecureInteractor;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;
import timber.log.Timber;

import static addcel.mobilecard.data.net.scanpay.ScanPayService.AUTH_FINISHED;
import static addcel.mobilecard.data.net.scanpay.ScanPayService.ERROR_PREVIO;
import static addcel.mobilecard.data.net.scanpay.ScanPayService.PROCESS_FINISHED;
import static addcel.mobilecard.ui.negocio.mx.cobro.secure.NegocioCobroSecureConstants.FORCED_USER_AGENT;
import static addcel.mobilecard.ui.negocio.mx.cobro.secure.NegocioCobroSecureConstants.FUN_JSON_PROCESS;
import static addcel.mobilecard.ui.negocio.mx.cobro.secure.NegocioCobroSecureConstants.LOG_FUN;

/**
 * ADDCEL on 08/11/17.
 */
@Module
public final class NegocioCobroSecureModule {
    private final NegocioCobroSecureFragment fragment;

    NegocioCobroSecureModule(NegocioCobroSecureFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    NegocioCobroActivity provideActivity() {
        return (NegocioCobroActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @Provides
    @PerFragment
    SPPagoEntity provideRequest() {
        return Objects.requireNonNull(fragment.getArguments()).getParcelable("data");
    }

    @Provides
    @PerFragment
    CookieManager provideManager() {
        return CookieManager.getInstance();
    }

    @PerFragment
    @Provides
    ScanPayService provideService(@Named("ScalarRetrofit") Retrofit retrofit) {
        return retrofit.create(ScanPayService.class);
    }

    @PerFragment
    @Provides
    ScanPaySecureInteractor provideInteractor(ScanPayService service,
                                              SessionOperations session) {
        return new ScanPaySecureInteractor(service, session, new CompositeDisposable());
    }

    @Provides
    @PerFragment
    NegocioCobroSecureContract.Presenter providePresenter(
            ScanPaySecureInteractor interactor) {
        return new NegocioCobroSecurePresenter(interactor, fragment) {
        };
    }

    @Provides
    @PerFragment
    WebViewClient provideWebClient() {
        return new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Timber.i("onPageStarted: %s", url);
                fragment.showProgress();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Timber.i("onPageFinished: %s", url);
                fragment.hideProgress();
                if (url.contains(ERROR_PREVIO) || url.contains(PROCESS_FINISHED) || url.contains(
                        AUTH_FINISHED)) {
                    view.evaluateJavascript(FUN_JSON_PROCESS, null);
                }
                view.evaluateJavascript(LOG_FUN, null);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request,
                                            WebResourceResponse errorResponse) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (errorResponse.getStatusCode() != 404 && errorResponse.getStatusCode() != 403) {
                        Timber.d("%s %s", errorResponse.getReasonPhrase(), request.getUrl().toString());
                        fragment.processHthResponse(new SPReceiptEntity(
                                errorResponse.getStatusCode() == 0 ? -9999 : errorResponse.getStatusCode(),
                                errorResponse.getReasonPhrase()));
                    }
                }
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                final String errorStr = Strings.nullToEmpty(TextUtils.substring(error.toString(), 0, 100));
                Timber.e("Error %d SSL al intentar cargar: %s\n%s",
                        error.getPrimaryError() == 0 ? -9999 : error.getPrimaryError(), error.getUrl(),
                        error.toString());
                if (BuildConfig.DEBUG) handler.proceed();
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request,
                                        WebResourceError error) {
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description,
                                        String failingUrl) {
                fragment.processHthResponse(new SPReceiptEntity((errorCode == 0 ? -9999 : errorCode),
                        Strings.nullToEmpty(description)));
            }
        };
    }

    @SuppressLint({"SetJavaScriptEnabled", "ClickableViewAccessibility"})
    @Provides
    @PerFragment
    WebView provideBrowser(NegocioCobroActivity activity, WebViewClient client,
                           CookieManager manager) {
        final WebView browser = new WebView(activity);
        browser.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        browser.requestFocus(View.FOCUS_DOWN);
        browser.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!v.hasFocus()) {
                        v.requestFocus();
                    }
                }
                return false;
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager.setAcceptThirdPartyCookies(browser, true);
            manager.removeAllCookies(null);
            browser.clearCache(true);
        } else {
            manager.removeAllCookie();
        }
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        browser.getSettings().setUserAgentString(FORCED_USER_AGENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        browser.addJavascriptInterface(fragment, "HTMLOUT");

        browser.setWebViewClient(client);
        return browser;
    }
}
