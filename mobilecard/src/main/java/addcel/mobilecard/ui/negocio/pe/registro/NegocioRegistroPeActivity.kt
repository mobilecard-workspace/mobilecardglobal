package addcel.mobilecard.ui.negocio.pe.registro

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.ui.ContainerActivity
import addcel.mobilecard.ui.negocio.pe.registro.menu.TipoRegistro
import addcel.mobilecard.ui.negocio.pe.registro.persona.NegocioPeRegistroPersonaFragment
import addcel.mobilecard.ui.negocio.pe.registro.persona.NegocioRegistroPersonaData
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroActivity
import addcel.mobilecard.ui.usuario.registro.sms.SmsRegistroModel
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_negocio_registro_pe.*

@Parcelize
data class NegocioRegistroPeModel(val phone: String, val email: String = "") : Parcelable

class NegocioRegistroPeActivity : ContainerActivity() {

    companion object {
        fun get(context: Context, model: NegocioRegistroPeModel): Intent {
            return Intent(context, NegocioRegistroPeActivity::class.java).putExtra("model", model)
        }
    }

    lateinit var model: NegocioRegistroPeModel

    override fun showRetry() {
        if (retry_negocio_pe.visibility == View.GONE) retry_negocio_pe.visibility = View.VISIBLE
    }

    override fun hideRetry() {
        if (retry_negocio_pe.visibility == View.VISIBLE) retry_negocio_pe.visibility = View.GONE
    }

    fun getPhone(): String {
        return model.phone
    }

    fun getEmail(): String {
        return model.email
    }

    fun getRetryView(): View {
        return retry_negocio_pe
    }

    override fun showProgress() {
        progress_pe_negocio_registro.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_pe_negocio_registro.visibility = View.GONE
    }

    override fun setAppToolbarTitle(title: Int) {
        toolbar_pe_negocio_registro.setTitle(title)
    }

    override fun setAppToolbarTitle(title: String) {
        toolbar_pe_negocio_registro.title = title
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_registro_pe)

        model = intent?.getParcelableExtra("model")!!

        setAppToolbarTitle(R.string.s_registro_negocio_title)

        fragmentManagerLazy.commit {

            val pModel = NegocioRegistroPersonaData(TipoRegistro.MANUAL)

            add(
                    R.id.frame_pe_negocio_registro,
                    NegocioPeRegistroPersonaFragment.get(pModel)
            ).setCustomAnimations(
                    android.R.anim.fade_in,
                    android.R.anim.fade_out
            )
        }
    }

    override fun getRetry(): View? {
        return null
    }

    override fun onBackPressed() {
        hideRetry()
        if (fragmentManagerLazy.backStackEntryCount == 0) {
            startActivity(
                    SmsRegistroActivity.get(
                            this, SmsRegistroModel(
                            PaisResponse.PaisEntity(4, "PE", "PERU"),
                            SmsRegistroActivity.USE_CASE_NEGOCIO)
                    )
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            finish()
        } else {
            super.onBackPressed()
        }
    }
}
