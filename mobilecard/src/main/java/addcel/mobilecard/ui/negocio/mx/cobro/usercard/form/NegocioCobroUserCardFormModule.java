package addcel.mobilecard.ui.negocio.mx.cobro.usercard.form;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.text.NumberFormat;
import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 07/09/18.
 */
@Module
public class NegocioCobroUserCardFormModule {
    private final NegocioCobroUserCardFormFragment fragment;

    NegocioCobroUserCardFormModule(NegocioCobroUserCardFormFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    NegocioCobroActivity provideActivity() {
        return (NegocioCobroActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @Provides
    @PerFragment
    Validator provideValidator(
            ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }


    @Provides
    @PerFragment
    @Named("montoCaptured")
    double provideMonto() {
        return Objects.requireNonNull(fragment.getArguments()).getDouble("monto");
    }

    @Provides
    @PerFragment
    NegocioCobroUserCardFormContract.Presenter providePresenter(
            NumberFormat format) {
        return new NegocioCobroUserCardFormPresenter(format, fragment);
    }
}
