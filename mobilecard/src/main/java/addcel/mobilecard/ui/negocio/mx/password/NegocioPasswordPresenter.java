package addcel.mobilecard.ui.negocio.mx.password;

import android.annotation.SuppressLint;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 13/09/18.
 */
public class NegocioPasswordPresenter implements NegocioPasswordContract.Presenter {
    private final NegocioAPI api;
    private final SessionOperations session;
    private final NegocioEntity newNegocio;
    private final CompositeDisposable compositeDisposable;
    private final NegocioPasswordContract.View view;

    NegocioPasswordPresenter(NegocioAPI api, SessionOperations session, NegocioEntity newNegocio,
                             NegocioPasswordContract.View view) {
        this.api = api;
        this.session = session;
        this.newNegocio = newNegocio;
        this.compositeDisposable = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        compositeDisposable.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void setPassword(String newPass) {
        view.showProgress();
        compositeDisposable.add(
                api.updatePasswordAdmin(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                        getCurrentEntity().getIdEstablecimiento(), StringUtil.sha512(newPass))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> {
                            view.hideProgress();
                            if (response.getIdError() == 0) {
                                view.showSuccess(response.getMensajeError());
                                saveUsuario(newPass, 1);
                                view.returnToMenuIfLogged();
                            } else {
                                view.showError(response.getMensajeError());
                            }
                        }, throwable -> {
                            view.hideProgress();
                            view.showError(StringUtil.getNetworkError());
                        }));
    }

    @Override
    public void saveUsuario(String newPass, int newStatus) {
        NegocioEntity temp = new NegocioEntity.Builder(getCurrentEntity()).setIdUsrStatus(1).build();
        session.setNegocio(temp);
        session.setNegocioLogged(true);
    }

    private NegocioEntity getCurrentEntity() {
        return session.getNegocio() == null ? newNegocio : session.getNegocio();
    }
}
