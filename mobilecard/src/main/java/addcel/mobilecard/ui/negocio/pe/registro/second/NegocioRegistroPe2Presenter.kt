package addcel.mobilecard.ui.negocio.pe.registro.second

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.data.net.catalogo.DeptoEntity
import addcel.mobilecard.data.net.catalogo.DistritoEntity
import addcel.mobilecard.data.net.catalogo.ProvinciaEntity
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import com.google.common.collect.Lists
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

/**
 * ADDCEL on 2019-07-12.
 */
interface NegocioRegistroPe2Presenter {
    fun getDeptos()

    fun getProvincias(depto: String, currPos: Int)

    fun getDistritos(provincia: String, currPos: Int)

    fun getInitialData(): Disposable
}

class NegocioRegistroPe2PresenterImpl(
        val catalogo: CatalogoAPI,
        val disposables: CompositeDisposable, val view: View
) : NegocioRegistroPe2Presenter {
    override fun getDeptos() {
        view.showProgress()

        disposables.add(
                catalogo.getDepartamentos(
                        BuildConfig.ADDCEL_APP_ID, 4,
                        StringUtil.getCurrentLanguage()
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r ->
                    view.hideProgress()
                    view.setDeptos(r)
                }, { t ->
                    view.hideProgress()
                    t.printStackTrace();view.showError(t.localizedMessage!!)
                    view.setDeptos(Lists.newArrayList())
                })
        )
    }

    override fun getProvincias(depto: String, currPos: Int) {
        view.showProgress()

        disposables.add(
                catalogo.getProvincias(
                        BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage(),
                        depto
                ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                        { r ->
                            view.hideProgress()
                            view.setProvincias(r, currPos)
                        }, { t ->
                    view.hideProgress()
                    t.printStackTrace();view.showError(t.localizedMessage!!)
                    view.setProvincias(Lists.newArrayList(), currPos)
                })
        )
    }

    override fun getDistritos(provincia: String, currPos: Int) {
        view.showProgress()

        disposables.add(
                catalogo.getDistritos(
                        BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage(),
                        provincia
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe({ r ->
                    view.hideProgress()
                    view.setDistritos(r, currPos)
                }, { t ->
                    view.hideProgress()
                    t.printStackTrace();view.showError(t.localizedMessage!!)
                    view.setDistritos(Lists.newArrayList(), currPos)
                })
        )
    }

    override fun getInitialData(): Disposable {

        view.hideRetry()
        view.showProgress()

        val deptos: MutableList<DeptoEntity> = ArrayList()
        val provincias: MutableList<ProvinciaEntity> = ArrayList()
        val distritos: MutableList<DistritoEntity> = ArrayList()

        return catalogo.getDepartamentos(
                BuildConfig.ADDCEL_APP_ID,
                4,
                StringUtil.getCurrentLanguage()
        )
                .subscribeOn(Schedulers.io())
                .flatMap {

                    deptos.addAll(it)

                    catalogo.getProvincias(
                            BuildConfig.ADDCEL_APP_ID,
                            4,
                            StringUtil.getCurrentLanguage(),
                            it[0].codigo
                    )
                }
                .flatMap {

                    provincias.addAll(it)

                    catalogo.getDistritos(
                            BuildConfig.ADDCEL_APP_ID,
                            4,
                            StringUtil.getCurrentLanguage(),
                            it[0].codigo
                    )
                }.observeOn(AndroidSchedulers.mainThread()).subscribe({

                    distritos.addAll(it)


                    view.hideProgress()
                    view.setInitialValues(deptos, provincias, distritos)
                }, {
                    view.hideProgress()
                    view.showRetry()
                    view.setInitialValues(
                            Collections.emptyList(),
                            Collections.emptyList(),
                            Collections.emptyList()
                    )
                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                })
    }
}