package addcel.mobilecard.ui.negocio.mx.historial;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.scanpay.model.SPCuentaEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.negocio.historial.NegocioHistorialInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 11/14/18.
 */
class NegocioHistorialPresenter implements NegocioHistorialContract.Presenter {
    private final NegocioHistorialInteractor interactor;
    private final NegocioHistorialContract.View view;

    NegocioHistorialPresenter(NegocioHistorialInteractor interactor,
                              NegocioHistorialContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }

    @Override
    public void getCuentas(int idPais) {
        view.showProgress();
        interactor.getCuentas(BuildConfig.ADDCEL_APP_ID, idPais, StringUtil.getCurrentLanguage(),
                new InteractorCallback<List<SPCuentaEntity>>() {
                    @Override
                    public void onSuccess(@NotNull List<SPCuentaEntity> result) {
                        view.hideProgress();
                        if (result.isEmpty()) {
                            view.notCuentas();
                        } else {
                            view.setCuentas(result);
                        }
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                        view.serverError();
                    }
                });
    }
}
