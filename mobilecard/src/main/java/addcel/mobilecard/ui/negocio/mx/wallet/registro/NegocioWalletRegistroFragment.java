package addcel.mobilecard.ui.negocio.mx.wallet.registro;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.util.List;

import javax.inject.Inject;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;
import addcel.mobilecard.ui.negocio.mx.wallet.NegocioWalletActivity;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.StringUtil;
import addcel.mobilecard.validation.ValidationUtils;

/**
 * ADDCEL on 12/11/18.
 */
public final class NegocioWalletRegistroFragment extends Fragment
        implements NegocioWalletRegistroContract.View {
    @Inject
    NegocioWalletActivity activity;
    @Inject
    ArrayAdapter<EstadoResponse.EstadoEntity> estadoAdapter;
    @Inject
    Validator validator;
    @Inject
    NegocioWalletRegistroContract.Presenter presenter;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout direccionTil;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout coloniaTil;

    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout ciudadTil;

    @Pattern(regex = "^[0-9]{5}$", messageResId = R.string.error_default)
    TextInputLayout cpTil;

    @Pattern(regex = "([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?)?", messageResId = R.string.error_rfc)
    TextInputLayout rfcTil;

    @Pattern(regex = "[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]", messageResId = R.string.error_rfc_curp)
    TextInputLayout curpTil;

    private Button curpButton;
    private Button activateButton;
    private Spinner estadoSpinner;

    public static synchronized NegocioWalletRegistroFragment get() {
        return new NegocioWalletRegistroFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .negocioWalletRegistroSubcomponent(new NegocioWalletRegistroModule(this))
                .inject(this);
        presenter.register();
        launchGetEstados();
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_negocio_wallet_registro, container, false);
        setUi(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        presenter.unregister();
        super.onDestroy();
    }

    @Override
    public PrevivaleUserRequest buildRequest() {
        return new PrevivaleUserRequest(0L, "", "", "",
                StringUtil.removeAcentosMultiple(AndroidUtils.getText(direccionTil.getEditText())),
                StringUtil.removeAcentosMultiple((AndroidUtils.getText(coloniaTil.getEditText()))),
                StringUtil.removeAcentosMultiple(AndroidUtils.getText(ciudadTil.getEditText())),
                AndroidUtils.getText(cpTil.getEditText()),
                ((EstadoResponse.EstadoEntity) estadoSpinner.getSelectedItem()).getId(),
                AndroidUtils.getText(rfcTil.getEditText()), AndroidUtils.getText(curpTil.getEditText()),
                BuildConfig.ADDCEL_APP_ID, "");
    }

    @Override
    public void setUi(android.view.View view) {
        view.findViewById(R.id.text_previvale_activate_pan).setVisibility(View.GONE);

        direccionTil = view.findViewById(R.id.til_previvale_add_direccion);
        coloniaTil = view.findViewById(R.id.til_previvale_add_colonia);
        ciudadTil = view.findViewById(R.id.til_previvale_add_ciudad);
        estadoSpinner = view.findViewById(R.id.spinner_previvale_add_estado);
        estadoSpinner.setAdapter(estadoAdapter);
        cpTil = view.findViewById(R.id.til_previvale_add_cp);
        rfcTil = view.findViewById(R.id.til_previvale_add_rfc);
        curpTil = view.findViewById(R.id.til_previvale_add_curp);
        curpButton = view.findViewById(R.id.b_previvale_curp);
        activateButton = view.findViewById(R.id.b_previvale_activate);

        curpButton.setOnClickListener(v -> {
            try {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(CURP_URL));
                startActivity(myIntent);
            } catch (ActivityNotFoundException e) {
                showError(e.getLocalizedMessage());
                e.printStackTrace();
            }
        });

        activateButton.setOnClickListener(v -> {
            ValidationUtils.clearViewErrors(direccionTil, coloniaTil, ciudadTil, cpTil, rfcTil,
                    curpTil);
            validator.validate();
        });
    }

    @Override
    public void setCardTextWatcher(@NonNull EditText text) {
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                onPanCaptured(s);
            }
        });
    }

    @Override
    public void onPanCaptured(CharSequence s) {

    }

    private void setCardDrawable(EditText text) {
        text.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.card_carnet, 0, 0, 0);
    }

    @Override
    public void launchGetEstados() {
        if (estadoAdapter.getCount() == 0) presenter.onGetEstadosLaunched();
    }

    @Override
    public void setEstados(List<EstadoResponse.EstadoEntity> estados) {
        estadoAdapter.addAll(estados);
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void launchRegistro() {
        presenter.onRegistroLaunched();
    }

    @Override
    public void onRegistroSuccess(@NonNull CardEntity model) {
        Intent result = new Intent();
        result.putExtra(NegocioWalletActivity.RESULT_DATA, model);
        activity.setResult(Activity.RESULT_OK, result);
        activity.finish();

    /*
    activity.getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.frame_negocio_wallet, NegocioWalletCardFragment.get(model))
            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            .commit();
     */
    }

    @Override
    public void finishActivity() {
        if (activity != null) activity.finish();
    }

    @Override
    public void onValidationSucceeded() {
        launchRegistro();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationUtils.onValidationFailed(activity, errors);
    }
}
