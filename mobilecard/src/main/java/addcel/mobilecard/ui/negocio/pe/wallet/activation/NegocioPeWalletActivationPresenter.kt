package addcel.mobilecard.ui.negocio.pe.wallet.activation

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.data.net.tebca.TebcaActivationRequest
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.peru.NegocioPeWalletActivationInteractor
import addcel.mobilecard.utils.StringUtil

/**
 * ADDCEL on 2019-08-29.
 */
interface NegocioPeWalletActivationPresenter {
    fun activate(request: TebcaActivationRequest)
}

class NegocioPeWalletActivationPresenterImpl(
        val interactor: NegocioPeWalletActivationInteractor,
        val view: NegocioPeWalletActivationView
) : NegocioPeWalletActivationPresenter {

    override fun activate(request: TebcaActivationRequest) {
        view.showProgress()

        interactor.activateCard(BuildConfig.ADDCEL_APP_ID,
                4,
                StringUtil.getCurrentLanguage(),
                request,
                object : InteractorCallback<CommerceAccountsResponse> {
                    override fun onSuccess(result: CommerceAccountsResponse) {
                        view.hideProgress()
                        view.onActivate(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }
}