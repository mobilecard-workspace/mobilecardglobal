package addcel.mobilecard.ui.negocio.mx.cobro.scan;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ADDCEL on 13/09/18.
 */
class NegocioCobroQrScanPresenter implements NegocioCobroQrScanContract.Presenter {
    private final ScanPayService service;
    private final CompositeDisposable disposable;
    private final NegocioCobroQrScanContract.View view;

    NegocioCobroQrScanPresenter(ScanPayService service, NegocioCobroQrScanContract.View view) {
        this.service = service;
        this.disposable = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearDisposable() {
        disposable.clear();
    }

    @Override
    public void validateScanResult(@NonNull String result) {
    }

    @SuppressLint("CheckResult")
    @Override
    public void getResultById() {
        view.showProgress();
        disposable.add(
                service.getResultByID(BuildConfig.ADDCEL_APP_ID, 1, StringUtil.getCurrentLanguage(),
                        view.getTransactionId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(receiptEntity -> {
                            view.hideProgress();
                            if (receiptEntity.getCode() == 0) {
                                view.onScanSuccess(receiptEntity);
                            } else {
                                view.showError(receiptEntity.getMessage());
                            }
                        }, throwable -> {
                            view.hideProgress();
                            view.showError(StringUtil.getNetworkError());
                        }));
    }
}
