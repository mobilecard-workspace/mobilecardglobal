package addcel.mobilecard.ui.negocio.pe.id

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.scanpay.QrRequest
import addcel.mobilecard.data.net.scanpay.QrType
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.domain.usuario.bottom.MenuEvent
import addcel.mobilecard.domain.usuario.bottom.UseCase
import addcel.mobilecard.ui.negocio.mx.historial.NegocioHistorialActivity
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletActivity
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletModel
import addcel.mobilecard.ui.negocio.pe.wallet.NegocioPeWalletActivity
import addcel.mobilecard.ui.negocio.pe.wallet.NegocioPeWalletModel
import addcel.mobilecard.ui.usuario.menu.MenuLayout
import addcel.mobilecard.utils.StringUtil
import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap.CompressFormat
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.squareup.otto.Bus
import com.squareup.otto.Subscribe
import com.tbruyelle.rxpermissions2.RxPermissions
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_negocio_pe_id_qr.*
import okio.ByteString.Companion.decodeBase64
import retrofit2.Retrofit
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

@Parcelize
data class NegocioPeIdQrModel(
        val idEstablecimiento: Int = 0,
        val qrBase64: String,
        val negocioName: String,
        val idPais: Int = PaisResponse.PaisEntity.PE
) : Parcelable

class NegocioPeIdQRActivity : AppCompatActivity() {

    companion object {
        fun get(context: Context, model: NegocioPeIdQrModel): Intent {
            return Intent(context, NegocioPeIdQRActivity::class.java).putExtra("model", model)
        }
    }

    @Inject
    lateinit var bus: Bus
    @Inject
    lateinit var session: SessionOperations
    @Inject
    lateinit var retrofit: Retrofit

    lateinit var api: SPApi

    lateinit var model: NegocioPeIdQrModel
    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.inject(this)

        api = retrofit.create(SPApi::class.java)

        setContentView(R.layout.activity_negocio_pe_id_qr)

        model = intent.getParcelableExtra("model")!!

        val permissions = RxPermissions(this@NegocioPeIdQRActivity)

        b_negocio_pe_id_qr_share.setOnClickListener {
            compositeDisposable.add(permissions.request(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).subscribe { accept ->
                if (accept) shareQr()
            })
        }

        setSupportActionBar(toolbar_pe_id_qr)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setName()
        configBottomLayout()

        if (model.qrBase64.isEmpty() && model.idPais == PaisResponse.PaisEntity.MX) {
            evalQr()
        } else {
            loadQr()
        }
    }

    override fun onResume() {
        super.onResume()
        bus.register(this)
    }

    override fun onPause() {
        bus.unregister(this)
        super.onPause()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun evalQr() {
        if (model.idPais == PaisResponse.PaisEntity.PE) {
            loadQr()
        } else {
            val request = QrRequest(model.idEstablecimiento, model.idPais, 0.0, QrType.ESTATICO)

            compositeDisposable.add(
                    api.generateQr(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(), request)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                if (it.idError == 0) loadQR(it.qrBase64)
                                else Toasty.error(this@NegocioPeIdQRActivity, it.mensajeError)
                                        .show()
                            }, {
                                Toasty.error(this@NegocioPeIdQRActivity, "Qr de negocio no disponible")
                                        .show()
                            })
            )
        }
    }

    private fun loadQR(base64: String) {
        val arr = base64.decodeBase64()?.toByteArray()!!
        val bmp = BitmapFactory.decodeByteArray(arr, 0, arr.size)
        img_negocio_pe_id_qr_code.setImageBitmap(bmp)
    }

    private fun loadQr() {
        val arr = model.qrBase64.decodeBase64()?.toByteArray()!!
        val bmp = BitmapFactory.decodeByteArray(arr, 0, arr.size)
        img_negocio_pe_id_qr_code.setImageBitmap(bmp)
    }

    private fun setName() {
        view_negocio_pe_id_qr_name.text = model.negocioName
    }

    private fun shareQr() {
        img_negocio_pe_id_qr_code.isDrawingCacheEnabled = true

        val bitmap = img_negocio_pe_id_qr_code.drawingCache
        val root = Environment.getExternalStorageDirectory()
        val cachePath = File(root.absolutePath + "/DCIM/Camera/image.jpg")
        try {
            cachePath.createNewFile()
            val ostream = FileOutputStream(cachePath)
            bitmap.compress(CompressFormat.JPEG, 100, ostream)
            ostream.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val share = Intent(Intent.ACTION_SEND)
        share.type = "image/*"
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(cachePath))
        startActivity(Intent.createChooser(share, "Compartir vía"))
    }

    private fun configBottomLayout() {
        val favs: ImageButton = menu_negocio_pe_id_qr.findViewById(R.id.b_menu_frecuentes)
        favs.visibility = View.GONE
    }

    @Subscribe
    fun postBottomMenuEvent(event: MenuEvent) {
        (menu_negocio_pe_id_qr as MenuLayout).unblock()

        when (event.useCase) {
            UseCase.HOME -> onBackPressed()

            UseCase.WALLET -> {
                if (model.idPais == PaisResponse.PaisEntity.PE) {
                    startActivity(
                            NegocioPeWalletActivity.get(
                                    this,
                                    NegocioPeWalletModel(
                                            session.negocio.idEstablecimiento,
                                            session.negocio.tipoPersona
                                    )
                            )
                    )
                } else if (model.idPais == PaisResponse.PaisEntity.MX) {
                    startActivity(
                            NegocioMxWalletActivity.get(
                                    this,
                                    NegocioMxWalletModel(
                                            session.negocio.idEstablecimiento,
                                            session.negocio.tipoPersona
                                    )
                            )
                    )
                }
            }

            UseCase.HISTORIAL -> startActivity(NegocioHistorialActivity.get(this, model.idPais))
            else -> Timber.d("In progress")
        }
    }
}
