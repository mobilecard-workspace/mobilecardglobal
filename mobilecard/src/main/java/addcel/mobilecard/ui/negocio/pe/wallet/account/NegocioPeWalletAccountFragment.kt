package addcel.mobilecard.ui.negocio.pe.wallet.account

import addcel.mobilecard.R
import addcel.mobilecard.ui.negocio.pe.wallet.account.form.NegocioPeWalletAccountFormFragment
import addcel.mobilecard.ui.negocio.pe.wallet.account.form.NegocioPeWalletAccountFormModel
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_wallet_account.*

/**
 * ADDCEL on 2019-08-16.
 */

@Parcelize
data class NegocioPeWalletAccountModel(val establecimientoId: Int) : Parcelable

class NegocioPeWalletAccountFragment : Fragment() {

    companion object {
        fun get(model: NegocioPeWalletAccountModel): NegocioPeWalletAccountFragment {

            val args = BundleBuilder().putParcelable("model", model).build()
            val frag = NegocioPeWalletAccountFragment()
            frag.arguments = args
            return frag
        }
    }

    lateinit var model: NegocioPeWalletAccountModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_negocio_wallet_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        b_negocio_wallet_account_add.setOnClickListener {
            fragmentManager?.transaction {
                replace(
                        R.id.frame_tebca, NegocioPeWalletAccountFormFragment.get(
                        NegocioPeWalletAccountFormModel(model.establecimientoId, null)
                )
                ).setCustomAnimations(
                        android.R.anim.fade_in, android.R.anim.slide_out_right
                )
            }
        }
    }
}