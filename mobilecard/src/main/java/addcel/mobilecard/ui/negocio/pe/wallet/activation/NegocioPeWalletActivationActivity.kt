package addcel.mobilecard.ui.negocio.pe.wallet.activation

import addcel.mobilecard.McRegexPatterns
import addcel.mobilecard.Mobilecard
import addcel.mobilecard.R
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.data.net.tebca.TebcaActivationRequest
import addcel.mobilecard.ui.ScreenView
import addcel.mobilecard.ui.custom.dialog.FixedHoloDatePickerDialog
import addcel.mobilecard.utils.AndroidUtils
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.view.View
import android.widget.DatePicker
import androidx.appcompat.app.AppCompatActivity
import com.google.common.base.Strings
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_negocio_pe_wallet_activation.*
import mx.mobilecard.crypto.AddcelCrypto
import java.util.*
import java.util.regex.Pattern
import javax.inject.Inject

@Parcelize
data class NegocioPeWalletActivationModel(val idComercio: Int) : Parcelable

interface NegocioPeWalletActivationCallback {
    fun onPeWalletActivationResult(resultCode: Int, data: Intent?)
}

interface NegocioPeWalletActivationView : ScreenView, DatePickerDialog.OnDateSetListener,
        TextWatcher {

    fun configPanTil()

    fun configExpiryDialog()

    fun configExpiryButton()

    fun showExpiryDialog()

    fun hideExpiryDialog()

    fun clickExpiryTil()

    fun getExpiry(): String

    fun evalForm(pan: String, expiry: String, cvv: String): Boolean

    fun clickActivate()

    fun onActivate(result: CommerceAccountsResponse)
}

class NegocioPeWalletActivationActivity : AppCompatActivity(), NegocioPeWalletActivationView {

    override fun afterTextChanged(p0: Editable?) {
        val icon: Int
        val pan = Strings.nullToEmpty(p0.toString())
        if (pan.matches(McRegexPatterns.TEBCA.toRegex())) {
            icon = R.drawable.logo_visa
            AndroidUtils.getEditText(til_wallet_card_info_card).setTextColor(Color.BLACK)
        } else {
            icon = android.R.color.transparent
            AndroidUtils.getEditText(til_wallet_card_info_card).setTextColor(Color.RED)
        }
        img_wallet_card_info_franquicia.setImageResource(icon)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun configPanTil() {
        text_wallet_card_info_card.addTextChangedListener(this)
    }

    override fun configExpiryButton() {
        text_wallet_card_info_exp.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            if (b) showExpiryDialog() else hideExpiryDialog()
        }

        text_wallet_card_info_exp.setOnClickListener { showExpiryDialog() }
    }

    override fun showExpiryDialog() {
        if (!::expiryDialog.isInitialized) configExpiryDialog()

        if (!expiryDialog.isShowing) expiryDialog.show()
    }

    override fun hideExpiryDialog() {
        if (!::expiryDialog.isInitialized) configExpiryDialog()

        if (expiryDialog.isShowing) expiryDialog.dismiss()
    }

    override fun clickExpiryTil() {
        showExpiryDialog()
    }

    override fun showProgress() {
        progress_negocio_peru_menu.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_negocio_peru_menu.visibility = View.GONE
    }

    override fun showError(msg: String) {
        Toasty.error(this, msg).show()
    }

    override fun showSuccess(msg: String) {
        Toasty.success(this, msg).show()
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        expiryCal.set(Calendar.YEAR, p1)
        expiryCal.set(Calendar.MONTH, p2)
        AndroidUtils.setText(
                til_wallet_card_info_exp.editText,
                DateFormat.format("MM/yy", expiryCal)
        )
    }

    companion object {
        const val REQUEST_CODE = 422
        const val DATA_CARD = "card"

        fun get(context: Context, model: NegocioPeWalletActivationModel): Intent {
            return Intent(context, NegocioPeWalletActivationActivity::class.java).putExtra(
                    "model",
                    model
            )
        }
    }

    @Inject
    lateinit var model: NegocioPeWalletActivationModel
    @Inject
    lateinit var presenter: NegocioPeWalletActivationPresenter

    lateinit var expiryDialog: DatePickerDialog
    private val expiryCal: Calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mobilecard.get().netComponent.negocioPeWalletActivationSubcomponent(
                NegocioPeWalletActivationModule(this)
        ).inject(this)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.slide_out_right)
        setContentView(R.layout.activity_negocio_pe_wallet_activation)
        configPanTil()
        configExpiryDialog()
        configExpiryButton()

        b_negocio_pe_wallet_activation_guardar.setOnClickListener {
            clickActivate()
        }
    }

    override fun configExpiryDialog() {
        expiryDialog = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            FixedHoloDatePickerDialog(
                    this, this, expiryCal.get(Calendar.YEAR),
                    expiryCal.get(Calendar.MONTH), expiryCal.get(Calendar.DAY_OF_MONTH)
            )
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                DatePickerDialog(
                        this, R.style.CustomDatePickerDialogTheme, this,
                        expiryCal.get(Calendar.YEAR), expiryCal.get(Calendar.MONTH),
                        expiryCal.get(Calendar.DAY_OF_MONTH)
                )
            } else { // R.style.CustomDatePickerDialogTheme
                DatePickerDialog(
                        this, this, expiryCal.get(Calendar.YEAR), expiryCal.get(Calendar.MONTH),
                        expiryCal.get(Calendar.DAY_OF_MONTH)
                )
            }
        }

        expiryDialog.datePicker.findViewById<View>(
                Resources.getSystem().getIdentifier("day", "id", "android")
        ).visibility = View.GONE
    }

    override fun getExpiry(): String {
        return DateFormat.format("MM/yy", expiryCal).toString()
    }

    override fun evalForm(pan: String, expiry: String, cvv: String): Boolean {
        val evalPan = Pattern.matches(McRegexPatterns.TEBCA, pan)
        val evalCvv = cvv.isNotEmpty() && cvv.length == 3
        val evalVigencia = expiry.isNotEmpty()

        til_wallet_card_info_card.error = if (!evalPan) getString(R.string.error_tarjeta) else null
        til_wallet_card_info_cvv.error = if (!evalCvv) getString(R.string.error_cvv) else null
        til_wallet_card_info_exp.error =
                if (!evalVigencia) getString(R.string.error_vigencia) else null

        return evalPan && evalCvv && evalVigencia
    }

    override fun clickActivate() {
        val pan = AndroidUtils.getText(til_wallet_card_info_card.editText)
        val cvv = AndroidUtils.getText(til_wallet_card_info_cvv.editText)
        val vigencia = getExpiry()

        if (evalForm(pan, vigencia, cvv)) {
            val request = TebcaActivationRequest(
                    AddcelCrypto.encryptHard(cvv), model.idComercio,
                    AddcelCrypto.encryptHard(pan), AddcelCrypto.encryptHard(vigencia)
            )

            presenter.activate(request)
        }
    }

    override fun onActivate(result: CommerceAccountsResponse) {
        if (result.idError != 0) {
            showError(result.mensajeError)
        } else {
            Handler().postDelayed({
                val intent = Intent().putExtra(DATA_CARD, result)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }, 200)
        }
    }
}
