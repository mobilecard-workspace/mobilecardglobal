package addcel.mobilecard.ui.negocio.mx.wallet.update.account.display

import addcel.mobilecard.R
import addcel.mobilecard.data.net.tebca.Cci
import addcel.mobilecard.ui.negocio.mx.wallet.update.account.form.NegocioMxWalletAccountFormFragment
import addcel.mobilecard.ui.negocio.mx.wallet.update.account.form.NegocioMxWalletAccountFormModel
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_wallet_account_display.*

@Parcelize
data class NegocioMxWalletAccountDisplayModel(val cci: Cci, val idNegocio: Int) :
        Parcelable

/**
 * ADDCEL on 2019-08-28.
 */
class NegocioMxWalletAccountDisplayFragment : Fragment() {
    companion object {
        fun get(model: NegocioMxWalletAccountDisplayModel): NegocioMxWalletAccountDisplayFragment {
            val frag = NegocioMxWalletAccountDisplayFragment()
            val args = BundleBuilder().putParcelable("model", model).build()
            frag.arguments = args
            return frag
        }
    }

    lateinit var model: NegocioMxWalletAccountDisplayModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = arguments?.getParcelable("model")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_negocio_wallet_account_display, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b_negocio_wallet_account.text =
                "${model.cci.nombre}\nCLABE: ${model.cci.cuenta}\n${model.cci.nombreBanco}"
        b_negocio_wallet_account.setOnClickListener {
            fragmentManager?.commit {
                replace(
                        R.id.frame_tebca, NegocioMxWalletAccountFormFragment.get(
                        NegocioMxWalletAccountFormModel(model.idNegocio, model.cci)
                )
                )
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.slide_out_right)
            }
        }
    }
}