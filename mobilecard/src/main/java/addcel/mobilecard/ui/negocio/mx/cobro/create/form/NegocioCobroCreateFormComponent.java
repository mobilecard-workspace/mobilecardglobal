package addcel.mobilecard.ui.negocio.mx.cobro.create.form;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Component;

/**
 * ADDCEL on 07/09/18.
 */
@PerFragment
@Component(modules = NegocioCobroCreateFormModule.class)
public interface NegocioCobroCreateFormComponent {
    void inject(NegocioCobroCreateFormFragment fragment);
}
