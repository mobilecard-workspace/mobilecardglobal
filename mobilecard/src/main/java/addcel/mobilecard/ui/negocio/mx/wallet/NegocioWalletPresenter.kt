package addcel.mobilecard.ui.negocio.mx.wallet

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

interface NegocioWalletPresenter {

    fun clearDisposables()

    fun onValidateLaunched(id: Int)
}

/** ADDCEL on 12/11/18.  */
class NegocioWalletPresenterImpl(private val api: NegocioAPI, private val view: NegocioWalletView) :
        NegocioWalletPresenter {
    private val disposables = CompositeDisposable()

    override fun clearDisposables() {
        disposables.clear()
    }

    override fun onValidateLaunched(id: Int) {
        disposables.add(
                api.verify(
                        BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                        id.toLong()
                ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                ).subscribe(
                        { validationEntity ->
                            if (validationEntity.idError == 0) {
                                if (validationEntity.card != null) {
                                    view.onHasCard(validationEntity.card)
                                } else {
                                    view.onNoCard()
                                }
                            } else {
                                view.onError(validationEntity.mensajeError)
                            }
                        },
                        { t ->
                            view.onError(
                                    t.localizedMessage ?: ErrorUtil.getErrorMsg(ErrorUtil.NETWORK)
                            )
                        })
        )
    }
}
