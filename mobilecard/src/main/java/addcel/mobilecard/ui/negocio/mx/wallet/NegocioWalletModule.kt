package addcel.mobilecard.ui.negocio.mx.wallet

import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.di.scope.PerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-09-04.
 */
@Module
class NegocioWalletModule(val activity: NegocioWalletActivity) {

    @PerActivity
    @Provides
    fun provideModel(): NegocioWalletModel {
        return activity.intent?.getParcelableExtra("model")!!
    }

    @PerActivity
    @Provides
    fun provideApi(r: Retrofit): NegocioAPI {
        return r.create(NegocioAPI::class.java)
    }

    @PerActivity
    @Provides
    fun providePresenter(api: NegocioAPI): NegocioWalletPresenter {
        return NegocioWalletPresenterImpl(api, activity)
    }
}

@PerActivity
@Subcomponent(modules = [NegocioWalletModule::class])
interface NegocioWalletSubcomponent {
    fun inject(activity: NegocioWalletActivity)
}