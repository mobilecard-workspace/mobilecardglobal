package addcel.mobilecard.ui.negocio.mx.cobro.usercard.card;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.inject.Inject;

import addcel.mobilecard.McConstants;
import addcel.mobilecard.McRegexPatterns;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.domain.permission.PermissionRequestConstants;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.negocio.NegocioSignatureActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroComponentCreator;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureActivity;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureModel;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketUseCase;
import addcel.mobilecard.ui.usuario.scanpay.result.ScanResultFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.DeviceUtil;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.validation.ValidationUtils;
import commons.validator.routines.CreditCardValidator;
import dagger.Lazy;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import io.reactivex.disposables.CompositeDisposable;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 10/09/18.
 */
public class NegocioCobroUserCardCardFragment extends Fragment
        implements NegocioCobroUserCardCardContract.View {

    private final CompositeDisposable permissionDisposable = new CompositeDisposable();
    @Inject
    NegocioCobroActivity activity;
    @Inject
    Validator validator;
    @Inject
    NegocioCobroUserCardCardContract.Presenter presenter;
    @Inject
    Calendar expirationCalendar;
    @Inject
    Lazy<DatePickerDialog> datePickerDialog;
    @Inject
    Picasso picasso;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout holderTil;
    @com.mobsandgeeks.saripaar.annotation.CreditCard(messageResId = R.string.error_tarjeta)
    TextInputLayout cardTil;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout cvvTil;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout vigenciaTil;
    @Email(messageResId = R.string.error_email)
    TextInputLayout emailTil;

    private TextInputLayout celularTil;
    private ImageView franquiciaImg;
    private Spinner msiSpinner;
    private ImageButton cardImgButton;
    private String signature = "";
    private ColorStateList oldColors;
    private RxPermissions permissions;

    public NegocioCobroUserCardCardFragment() {
    }

    public static synchronized NegocioCobroUserCardCardFragment get(double importe, double propina,
                                                                    String concepto) {
        Bundle args = new BundleBuilder().putDouble("importe", importe)
                .putDouble("propina", propina)
                .putString("concepto", concepto)
                .build();

        NegocioCobroUserCardCardFragment fragment = new NegocioCobroUserCardCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NegocioCobroComponentCreator.negocioCobroUserCardCardSubcomponent(Mobilecard.get(),
                new NegocioCobroUserCardCardModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_negocio_cobro_usercard_card, container, false);
        view.findViewById(R.id.b_negocio_cobro_usercard_card_continuar)
                .setOnClickListener(v -> clickContinue());
        cardImgButton = view.findViewById(R.id.b_negocio_cobro_usercard_card_scan);
        cardImgButton.setOnClickListener(v -> clickScan());
        holderTil = view.findViewById(R.id.til_negocio_cobro_usercard_card_cardholder);
        cardTil = view.findViewById(R.id.til_negocio_cobro_usercard_card_card);
        franquiciaImg = view.findViewById(R.id.img_negocio_cobro_usercard_card_franquicia);
        msiSpinner = view.findViewById(R.id.spinner_usercard_card_msi);
        emailTil = view.findViewById(R.id.til_usercard_card_email);
        celularTil = view.findViewById(R.id.til_usercard_card_celular);

        Objects.requireNonNull(cardTil.getEditText()).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                validateCardPan(s);
            }
        });
        cvvTil = view.findViewById(R.id.til_negocio_cobro_usercard_card_cvv);
        vigenciaTil = view.findViewById(R.id.til_negocio_cobro_usercard_card_exp);
        AndroidUtils.getEditText(vigenciaTil).setOnClickListener(v -> showExpirationDialog());
        vigenciaTil.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                vigenciaTil.setError(null);
                showExpirationDialog();
            } else {
                dismissExpirationDialog();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        permissions = new RxPermissions(this);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        oldColors = AndroidUtils.getEditText(cardTil).getTextColors();
        holderTil.setEnabled(false);
        cardTil.setEnabled(false);
        vigenciaTil.setEnabled(false);

        activity.hideBottomMenu();

        new Handler().postDelayed(this::clickScan, 250);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            activity.hideBottomMenu();
            activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        }
    }

    @Override
    public void onDestroyView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            datePickerDialog.get().setOnDateSetListener(null);
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 13) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                setValuesFromScan(scanResult);
                byte[] capturedCardArr = data.getByteArrayExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE);
                if (capturedCardArr != null) {
                    cardImgButton.setImageBitmap(parseImage(capturedCardArr));
                }
            }
        } else if (requestCode == NegocioSignatureActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    String capturedSignature = data.getStringExtra(NegocioSignatureActivity.DATA_SIGNATURE);
                    if (Strings.isNullOrEmpty(capturedSignature)) {
                        signature = "";
                        showError(getString(R.string.error_firma));
                    } else {
                        signature = capturedSignature;
                        presenter.checkComision();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                signature = "";
                showError("Cancelado por el usuario");
            }
        } else if (requestCode == BillPocketSecureActivity.REQUEST_CODE) {
            onBillPocketSecureResult(resultCode, data);
        }
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void clickScan() {
        permissionDisposable.add(permissions.request(Manifest.permission.CAMERA).subscribe(aBoolean -> {
            if (aBoolean) {
                launchCardScan();
            } else {
                showPermissionDialog();
            }
        }, throwable -> showPermissionDialog()));
    }

    private void showPermissionDialog() {
        new AlertDialog.Builder(activity).setTitle("Aviso")
                .setMessage(
                        "Para poder capturar los datos de la tarjeta bancaria, por favor acepta los permisos solicitados por el dispositivo")
                .setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    clickScan();
                    dialogInterface.dismiss();
                })
                .setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    @Override
    public void setValuesFromScan(CreditCard card) {
        if (getView() != null) {
            if (!Strings.isNullOrEmpty(card.cardholderName)) {
                AndroidUtils.setText(holderTil.getEditText(), card.cardholderName);
            }
            AndroidUtils.setText(cardTil.getEditText(), card.cardNumber);
            validateCardPan(card.cardNumber);
            AndroidUtils.setText(cvvTil.getEditText(), Strings.nullToEmpty(card.cvv));
            setExpiryDateFromScan(card.expiryYear, card.expiryMonth);
        }
    }

    @Override
    public String getCardHolderName() {
        return AndroidUtils.getText(holderTil.getEditText());
    }

    @Override
    public String getCard() {
        return AndroidUtils.getText(cardTil.getEditText());
    }

    @Override
    public void validateCardPan(CharSequence s) {
        int icon;
        String pan = Strings.nullToEmpty(s.toString());
        if (Pattern.matches(McRegexPatterns.PREVIVALE, pan)) {
            icon = R.drawable.card_carnet;
            AndroidUtils.getEditText(cardTil).setTextColor(oldColors);
        } else if (CreditCardValidator.VISA_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_visa;
            AndroidUtils.getEditText(cardTil).setTextColor(oldColors);
        } else if (CreditCardValidator.MASTERCARD_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_master;
            AndroidUtils.getEditText(cardTil).setTextColor(oldColors);
        } else if (CreditCardValidator.AMEX_VALIDATOR.isValid(pan)) {
            icon = R.drawable.logo_american;
            AndroidUtils.getEditText(cardTil).setTextColor(oldColors);
        } else {
            icon = android.R.color.transparent;
            AndroidUtils.getEditText(cardTil).setTextColor(Color.RED);
        }
        franquiciaImg.setImageResource(icon);
    }

    @Override
    public String getCvv() {
        EditText editText = Objects.requireNonNull(cvvTil.getEditText());
        return Strings.nullToEmpty(editText.getText().toString());
    }

    @Override
    public void clickVigencia() {
        vigenciaTil.setError(null);
        showExpirationDialog();
    }

    @Override
    public String getVigencia() {
        return AndroidUtils.getText(vigenciaTil.getEditText());
    }

    @Override
    public String getMsi() {
        return (String) msiSpinner.getSelectedItem();
    }

    @Override
    public String getEmail() {
        return AndroidUtils.getText(emailTil.getEditText());
    }

    @Override
    public String getCelular() {
        return AndroidUtils.getText(celularTil.getEditText());
    }

    @Override
    public void clickContinue() {
        clearErrors();
        validator.validate();
    }

    @Override
    public void showExpirationDialog() {
        if (isVisible() && !datePickerDialog.get().isShowing()) datePickerDialog.get().show();
    }

    @Override
    public void dismissExpirationDialog() {
        if (isVisible() && datePickerDialog.get().isShowing()) datePickerDialog.get().dismiss();
    }

    @Override
    public void onComisionReceived(SPComisionEntity comision) {

        String holder = AndroidUtils.getText(holderTil.getEditText());
        String[] holderArr = TextUtils.split(holder, "\\s+");
        SPPagoEntity request;

        if (holderArr != null && holderArr.length > 1) {
            request =
                    presenter.buildPagoEntity(getCard(), getCvv(), getVigencia(), comision, holderArr[0],
                            holderArr[1], DeviceUtil.Companion.getDeviceId(activity), getEmail(), getCelular(), signature);
        } else {
            request = presenter.buildPagoEntity(getCard(), getCvv(), getVigencia(), comision, holder, "",
                    DeviceUtil.Companion.getDeviceId(activity), getEmail(), getCelular(), signature);
        }

        presenter.getToken(request);
    }

    @Override
    public void onToken(TokenEntity token, SPPagoEntity request) {
        if (token.getSecure()) {
            launchSecure(token, request);
        } else {
            launchOpen(token, request);
        }
    }

    @Override
    public void launchOpen(TokenEntity token, SPPagoEntity request) {
        presenter.launchPago(request, token);
    }

    @Override
    public void launchSecure(TokenEntity token, SPPagoEntity request) {

        final CardEntity formaPago = CardEntity.Companion.PRESENTE(getCardHolderName(),
                AddcelCrypto.encryptHard(getCard()),
                AddcelCrypto.encryptHard(getVigencia()),
                AddcelCrypto.encryptHard(getCvv()),
                presenter.getFranquicia(getCard()),
                TipoTarjetaEntity.CREDITO);

        BillPocketSecureModel model =
                new BillPocketSecureModel(0L, formaPago, token, AddcelCrypto.encryptSensitive(JsonUtil.toJson(request)), BillPocketUseCase.NEGOCIO);

        try {
            startActivityForResult(BillPocketSecureActivity.Companion.get(activity, model), BillPocketSecureActivity.REQUEST_CODE);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void onPagoSuccess(SPReceiptEntity result) {
        activity.getFragmentManagerLazy()
                .beginTransaction()
                .addToBackStack(null)
                .hide(this)
                .add(R.id.frame_negocio_cobro, ScanResultFragment.Companion.get(result, McConstants.PAIS_ID_MX, true))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @Override
    public void onValidationSucceeded() {
        startActivityForResult(NegocioSignatureActivity.Companion.get(activity),
                NegocioSignatureActivity.REQUEST_CODE);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    private void clearErrors() {
        if (isVisible()) ValidationUtils.clearViewErrors(holderTil, cardTil, cvvTil, vigenciaTil);
    }

    private void launchCardScan() {
        Intent scanIntent = new Intent(activity, CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false);
        scanIntent.putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_INSTRUCTIONS,
                getString(R.string.txt_cardio_instruction));
        scanIntent.putExtra(CardIOActivity.EXTRA_GUIDE_COLOR,
                activity.getResources().getColor(R.color.colorAccent));

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, PermissionRequestConstants.NEGOCIO);
    }

    private void setExpiryDateFromScan(int year, int month) {
        expirationCalendar.set(Calendar.YEAR, year);
        expirationCalendar.set(Calendar.MONTH, month - 1);
        Objects.requireNonNull(vigenciaTil.getEditText())
                .setText(DateFormat.format("MM/yy", expirationCalendar));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        expirationCalendar.set(Calendar.YEAR, year);
        expirationCalendar.set(Calendar.MONTH, month);
        Objects.requireNonNull(vigenciaTil.getEditText())
                .setText(DateFormat.format("MM/yy", expirationCalendar));
    }

    private Bitmap parseImage(@NonNull byte... bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    @Override
    public void onBillPocketSecureResult(int resultCode, @org.jetbrains.annotations.Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String resultData = data.getStringExtra(BillPocketSecureActivity.RESULT_DATA);
                if (JsonUtil.isJson(resultData)) {
                    SPReceiptEntity response = JsonUtil.fromJson(resultData, SPReceiptEntity.class);
                    onPagoSuccess(response);
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            showError("Pago cancelado por el usuario.");
        }
    }
}
