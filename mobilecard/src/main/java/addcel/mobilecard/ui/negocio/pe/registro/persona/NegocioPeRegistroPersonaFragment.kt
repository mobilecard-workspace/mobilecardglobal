package addcel.mobilecard.ui.negocio.pe.registro.persona

import addcel.mobilecard.R
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeActivity
import addcel.mobilecard.ui.negocio.pe.registro.first.NegocioRegistroPE1FragmentData
import addcel.mobilecard.ui.negocio.pe.registro.first.NegocioRegistroPe1Fragment
import addcel.mobilecard.ui.negocio.pe.registro.menu.TipoRegistro
import addcel.mobilecard.utils.BundleBuilder
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.screen_negocio_registro_pe_menu.*

/**
 * ADDCEL on 2019-07-10.
 */

@Parcelize
data class NegocioRegistroPersonaData(
        val perfil: TipoRegistro,
        val idEstablecimiento: Int = 0
) : Parcelable

class NegocioPeRegistroPersonaFragment : Fragment() {
    companion object {
        fun get(configData: NegocioRegistroPersonaData): NegocioPeRegistroPersonaFragment {
            val args = BundleBuilder().putParcelable("data", configData).build()
            val fragment = NegocioPeRegistroPersonaFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var configData: NegocioRegistroPersonaData
    val adapter = NegocioPeRegistroPersonaAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configData = arguments?.getParcelable("data")!!
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_negocio_registro_pe_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_registro_peru_menu.adapter = adapter
        recycler_registro_peru_menu.layoutManager = LinearLayoutManager(view.context)
        ItemClickSupport.addTo(recycler_registro_peru_menu)
                .setOnItemClickListener { _, position, _ ->

                    val data = NegocioRegistroPE1FragmentData(
                            configData.perfil, adapter.getItem(position),
                            configData.idEstablecimiento
                    )

                    (activity as NegocioRegistroPeActivity).fragmentManagerLazy.transaction {
                        add(R.id.frame_pe_negocio_registro, NegocioRegistroPe1Fragment.get(data))
                        hide(this@NegocioPeRegistroPersonaFragment)
                        addToBackStack(null)
                        setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    }
                }
    }

    override fun onDestroyView() {
        ItemClickSupport.removeFrom(recycler_registro_peru_menu)
        super.onDestroyView()
    }
}