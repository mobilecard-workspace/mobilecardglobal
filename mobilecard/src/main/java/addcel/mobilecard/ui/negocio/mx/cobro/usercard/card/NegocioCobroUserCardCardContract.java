package addcel.mobilecard.ui.negocio.mx.cobro.usercard.card;

import android.app.DatePickerDialog;

import com.mobsandgeeks.saripaar.Validator;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.wallet.FranquiciaEntity;
import addcel.mobilecard.ui.ScreenView;
import addcel.mobilecard.ui.usuario.billpocket.BillPocketSecureCallback;
import io.card.payment.CreditCard;

/**
 * ADDCEL on 10/09/18.
 */
public interface NegocioCobroUserCardCardContract {
    interface View
            extends ScreenView, Validator.ValidationListener, DatePickerDialog.OnDateSetListener, BillPocketSecureCallback {

        void clickScan();

        void setValuesFromScan(CreditCard card);

        String getCardHolderName();

        String getCard();

        void validateCardPan(CharSequence s);

        String getCvv();

        void clickVigencia();

        String getVigencia();

        String getMsi();

        String getEmail();

        String getCelular();

        void clickContinue();

        void showExpirationDialog();

        void dismissExpirationDialog();

        void onComisionReceived(SPComisionEntity comision);

        void onToken(TokenEntity token, SPPagoEntity request);

        void launchOpen(TokenEntity token, SPPagoEntity request);

        void launchSecure(TokenEntity token, SPPagoEntity request);

        void onPagoSuccess(SPReceiptEntity result);
    }

    interface Presenter {
        void clearDisposables();

        double getMonto();

        double getPropina();

        String getConcepto();

        double calculaComision(SPComisionEntity comision);

        String getCurrentAddress();

        void checkComision();

        SPPagoEntity buildPagoEntity(String pan, String ct, String vigencia, SPComisionEntity comision,
                                     String nombre, String apellido, String imei, String email, String celular, String firma);

        void getToken(SPPagoEntity request);

        void launchPago(SPPagoEntity request, TokenEntity token);

        FranquiciaEntity getFranquicia(@NotNull String pan);
    }
}
