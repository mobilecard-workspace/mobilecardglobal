package addcel.mobilecard.ui.negocio.pe.tarjetapresente.tarjeta

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.data.net.scanpay.SPPagoEntity
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.data.net.token.TokenEntity
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.location.McLocationData
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor
import addcel.mobilecard.utils.JsonUtil
import addcel.mobilecard.utils.StringUtil
import mx.mobilecard.crypto.AddcelCrypto
import timber.log.Timber

/**
 * ADDCEL on 2019-07-15.
 */
interface NegocioPeTarjetaPresenteTarjetaPresenter {

    companion object {
        const val PCT_CREDITO = 0.049
        const val PCT_DEBITO = 0.039
        const val PCT_IMPUESTOS = 0.18
    }

    fun splitCardHolderName(cardHolderName: String): List<String>

    fun clearDisposables()

    fun formatVigencia(mes: Int, anio: Int): String

    fun getIdNegocio(): Int

    fun getIdPais(): Int

    fun getLocation(): McLocationData

    fun getToken(profile: String, pago: SPPagoEntity)

    fun launchPago(token: TokenEntity, pago: SPPagoEntity)

    fun calculaComision(monto: Double, tipoTarjetaPos: Int): Double
}

class NegocioPeTarjetaPresenteTarjetaPresenterImpl(
        val interactor: ScanPayOpenInteractor,
        val view: View
) : NegocioPeTarjetaPresenteTarjetaPresenter {
    override fun formatVigencia(mes: Int, anio: Int): String {
        Timber.d("Mes: %d, Anio: %d", mes, anio)
        val mm = if (mes < 10) "0$mes" else mes.toString()
        val yy =
                if (anio.toString().length == 2) anio.toString() else (anio.toString().substring(2))
        val str = "$mm/$yy"
        Timber.d("Vigencia: %s", str)
        return str
    }

    override fun splitCardHolderName(cardHolderName: String): List<String> {

        val strForLog = cardHolderName.replace("\\s", "+")
        Timber.d("splitCardHolderName(cardHolderName -> %s)", strForLog)
        return cardHolderName.split("\\s+")
    }

    override fun calculaComision(monto: Double, tipoTarjetaPos: Int): Double {
        val pct =
                if (tipoTarjetaPos == 0) NegocioPeTarjetaPresenteTarjetaPresenter.PCT_CREDITO else NegocioPeTarjetaPresenteTarjetaPresenter.PCT_DEBITO
        val comision = (monto * pct)
        val impuesto = comision * NegocioPeTarjetaPresenteTarjetaPresenter.PCT_IMPUESTOS
        Timber.d("FORMULA COMISION T_PRESENTE: ($monto * $pct) + ($comision * ${NegocioPeTarjetaPresenteTarjetaPresenter.PCT_IMPUESTOS})")
        return comision + impuesto
    }

    override fun getLocation(): McLocationData {
        return interactor.getLocation()
    }

    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun getIdNegocio(): Int {
        return interactor.getIdNegocio()
    }

    override fun getIdPais(): Int {
        return interactor.getIdPais(SPApi.TIPO_NEGOCIO)
    }

    override fun getToken(profile: String, pago: SPPagoEntity) {
        view.showProgress()
        interactor.getToken(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                interactor.getIdPais(SPApi.TIPO_NEGOCIO),
                AddcelCrypto.encryptSensitive(JsonUtil.toJson(pago)), profile,
                object : InteractorCallback<TokenEntity> {
                    override fun onSuccess(result: TokenEntity) {
                        view.hideProgress()
                        if (result.code == 0) {
                            launchPago(result, pago)
                        } else {
                            view.showError(result.message)
                        }
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun launchPago(token: TokenEntity, pago: SPPagoEntity) {
        view.showProgress()
        interactor.pagoBP(BuildConfig.ADDCEL_APP_ID,
                StringUtil.getCurrentLanguage(),
                token.token,
                SPApi.TIPO_NEGOCIO,
                token.accountId,
                pago,
                object : InteractorCallback<SPReceiptEntity> {
                    override fun onSuccess(result: SPReceiptEntity) {
                        view.hideProgress()
                        view.onPagoSuccess(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }
}