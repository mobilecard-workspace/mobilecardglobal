package addcel.mobilecard.ui.negocio.pe.registro.second

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.CatalogoAPI
import addcel.mobilecard.data.net.catalogo.DeptoEntity
import addcel.mobilecard.data.net.catalogo.DistritoEntity
import addcel.mobilecard.data.net.catalogo.ProvinciaEntity
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.ui.negocio.pe.registro.NegocioRegistroPeActivity
import android.widget.ArrayAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable

@PerFragment
@Subcomponent(modules = [NegocioRegistroPe2Module::class])
interface NegocioRegistroPe2Subcomponent {
    fun inject(fragment: NegocioRegistroPe2Fragment)
}

/**
 * ADDCEL on 2019-07-12.
 */
@Module
class NegocioRegistroPe2Module(val fragment: NegocioRegistroPe2Fragment) {
    @PerFragment
    @Provides
    fun provideContainer(): NegocioRegistroPeActivity {
        return fragment.activity as NegocioRegistroPeActivity
    }

    @PerFragment
    @Provides
    fun provideData(): NegocioRegistroPE2FragmentData {
        return fragment.arguments?.getParcelable("data")!!
    }

    @PerFragment
    @Provides
    fun provideDeptoAdapter(
            activity: NegocioRegistroPeActivity
    ): ArrayAdapter<DeptoEntity> {
        val adapter = ArrayAdapter<DeptoEntity>(activity, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }

    @PerFragment
    @Provides
    fun proviceProvinciaAdapter(
            activity: NegocioRegistroPeActivity
    ): ArrayAdapter<ProvinciaEntity> {
        val adapter = ArrayAdapter<ProvinciaEntity>(activity, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }

    @PerFragment
    @Provides
    fun provideDistritoAdapter(
            activity: NegocioRegistroPeActivity
    ): ArrayAdapter<DistritoEntity> {
        val adapter = ArrayAdapter<DistritoEntity>(activity, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }

    @PerFragment
    @Provides
    fun providePresenter(catalogo: CatalogoAPI): NegocioRegistroPe2Presenter {
        return NegocioRegistroPe2PresenterImpl(catalogo, CompositeDisposable(), fragment)
    }
}