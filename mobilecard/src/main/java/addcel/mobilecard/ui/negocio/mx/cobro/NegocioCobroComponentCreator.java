package addcel.mobilecard.ui.negocio.mx.cobro;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.ui.negocio.mx.cobro.create.form.DaggerNegocioCobroCreateFormComponent;
import addcel.mobilecard.ui.negocio.mx.cobro.create.form.NegocioCobroCreateFormComponent;
import addcel.mobilecard.ui.negocio.mx.cobro.create.form.NegocioCobroCreateFormModule;
import addcel.mobilecard.ui.negocio.mx.cobro.create.qr.NegocioCobroCreateQrModule;
import addcel.mobilecard.ui.negocio.mx.cobro.create.qr.NegocioCobroCreateQrSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.menu.NegocioCobroMenuModule;
import addcel.mobilecard.ui.negocio.mx.cobro.menu.NegocioCobroMenuSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.card.NegocioCobroUserCardCardModule;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.card.NegocioCobroUserCardCardSubcomponent;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.form.NegocioCobroUserCardFormModule;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.form.NegocioCobroUserCardFormSubcomponent;

/**
 * ADDCEL on 06/09/18.
 */
public final class NegocioCobroComponentCreator {
    private NegocioCobroComponentCreator() {
    }

    public static synchronized NegocioCobroSubcomponent negocioCobroSubcomponent(
            Mobilecard mobilecard, NegocioCobroModule module) {
        return mobilecard.getNetComponent().negocioCobroSubcomponent(module);
    }

    public static synchronized NegocioCobroMenuSubcomponent negocioCobroMenuSubcomponent(
            Mobilecard mobilecard, NegocioCobroMenuModule module) {
        return mobilecard.getNetComponent().negocioCobroMenuSubcomponent(module);
    }

    public static synchronized NegocioCobroCreateFormComponent negocioCobroCreateFormComponent(
            NegocioCobroCreateFormModule module) {
        return DaggerNegocioCobroCreateFormComponent.builder()
                .negocioCobroCreateFormModule(module)
                .build();
    }

    public static synchronized NegocioCobroCreateQrSubcomponent negocioCobroCreateQrSubcomponent(
            Mobilecard mobilecard, NegocioCobroCreateQrModule module) {
        return mobilecard.getNetComponent().negocioCobroCreateQrSubcomponent(module);
    }

    public static synchronized NegocioCobroUserCardFormSubcomponent negocioCobroUserCardFormSubcomponent(
            Mobilecard mobilecard, NegocioCobroUserCardFormModule module) {
        return mobilecard.getNetComponent().negocioCobroUserCardFormSubcomponent(module);
    }

    public static synchronized NegocioCobroUserCardCardSubcomponent negocioCobroUserCardCardSubcomponent(
            Mobilecard mobilecard, NegocioCobroUserCardCardModule module) {
        return mobilecard.getNetComponent().negocioCobroUserCardCardSubcomponent(module);
    }
}
