package addcel.mobilecard.ui.negocio.pe.tarjetapresente.result

import addcel.mobilecard.data.net.scanpay.SPReceiptEntity

/**
 * ADDCEL on 2019-07-15.
 */

class NegocioPeTarjetaPresenteResultPresenter(val spReceiptEntity: SPReceiptEntity, val view: View)