package addcel.mobilecard.ui.negocio.mx.cobro.secure;

import androidx.annotation.NonNull;

import com.google.common.base.Strings;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.scanpay.secure.ScanPaySecureInteractor;
import addcel.mobilecard.ui.login.tipo.LoginTipoContract;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 09/11/17.
 */
class NegocioCobroSecurePresenter implements NegocioCobroSecureContract.Presenter {

    private final ScanPaySecureInteractor interactor;
    private final NegocioCobroSecureContract.View view;

    NegocioCobroSecurePresenter(ScanPaySecureInteractor interactor,
                                NegocioCobroSecureContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public SPReceiptEntity createResponse(String msg) {
        if (!Strings.isNullOrEmpty(msg)) {
            return JsonUtil.fromJson(msg, SPReceiptEntity.class);
        } else {
            return new SPReceiptEntity(-9999, "Error");
        }
    }

    @Override
    public void pago(@NonNull SPPagoEntity data) {
        view.showProgress();
        interactor.payment(BuildConfig.ADDCEL_APP_ID, 1, StringUtil.getCurrentLanguage(),
                LoginTipoContract.Tipo.NEGOCIO, data, new InteractorCallback<String>() {
                    @Override
                    public void onSuccess(@NotNull String result) {
                        view.hideProgress();
                        view.onPagoStarted(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }
}
