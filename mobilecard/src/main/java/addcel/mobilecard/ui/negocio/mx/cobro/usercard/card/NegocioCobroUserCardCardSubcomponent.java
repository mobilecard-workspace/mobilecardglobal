package addcel.mobilecard.ui.negocio.mx.cobro.usercard.card;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 07/09/18.
 */
@PerFragment
@Subcomponent(modules = NegocioCobroUserCardCardModule.class)
public interface NegocioCobroUserCardCardSubcomponent {
    void inject(NegocioCobroUserCardCardFragment fragment);
}
