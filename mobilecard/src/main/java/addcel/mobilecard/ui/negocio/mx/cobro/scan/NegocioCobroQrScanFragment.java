package addcel.mobilecard.ui.negocio.mx.cobro.scan;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.common.base.Strings;
import com.google.gson.JsonSyntaxException;
import com.google.zxing.Result;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.McConstants;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.usuario.scanpay.result.ScanResultFragment;
import addcel.mobilecard.utils.JsonUtil;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * ADDCEL on 13/09/18.
 */
public class NegocioCobroQrScanFragment extends Fragment
        implements NegocioCobroQrScanContract.View {

    @Inject
    NegocioCobroActivity activity;
    @Inject
    NegocioCobroQrScanContract.Presenter presenter;
    private ZXingScannerView scanner;
    private EditText idText;
    private ImageButton idButton;

    public NegocioCobroQrScanFragment() {
    }

    public static synchronized NegocioCobroQrScanFragment get() {
        return new NegocioCobroQrScanFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .negocioCobroQrScanSubcomponent(new NegocioCobroQrScanModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_negocio_cobro_qr, container, false);
        scanner = view.findViewById(R.id.scanner_negocio_cobro_qr);
        idText = view.findViewById(R.id.text_negocio_cobro_qr_id);
        configIdText(Objects.requireNonNull(idText));
        idButton = view.findViewById(R.id.b_negocio_cobro_qr_id);
        configIdButton(Objects.requireNonNull(idButton));
        view.findViewById(R.id.b_negocio_cobro_qr_id).setOnClickListener(view1 -> clickId());
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
            activity.hideBottomMenu();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        activity.hideBottomMenu();
    }

    @Override
    public void onResume() {
        scanner.setResultHandler(this);
        scanner.startCamera();
        super.onResume();
    }

    @Override
    public void onPause() {
        scanner.stopCamera();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        presenter.clearDisposable();
        super.onDestroy();
    }

    @Override
    public int getTransactionId() {
        String id = Strings.nullToEmpty(idText.getText().toString());
        return TextUtils.isDigitsOnly(id) ? Integer.valueOf(id) : 0;
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NotNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void onScanSuccess(SPReceiptEntity resultEntity) {
        scanner.stopCamera();
        activity.getFragmentManagerLazy()
                .beginTransaction()
                .add(R.id.frame_negocio_cobro, ScanResultFragment.Companion.get(resultEntity, McConstants.PAIS_ID_MX, Boolean.TRUE))
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .remove(this)
                .commit();
    }

    @Override
    public void onIdReceived(@NonNull String id) {
    }

    @Override
    public void clickId() {
        presenter.getResultById();
    }

    @Override
    public void handleResult(Result result) {
        if (isVisible()) {
            if (result != null && !Strings.isNullOrEmpty(result.getText())) {
                try {
                    SPReceiptEntity resultEntity = JsonUtil.fromJson(result.getText(), SPReceiptEntity.class);
                    if (resultEntity.getCode() == 0) {
                        onScanSuccess(resultEntity);
                    } else {
                        showError(resultEntity.getMessage());
                        scanner.resumeCameraPreview(this);
                    }
                } catch (JsonSyntaxException | IllegalStateException e) {
                    showError(getString(R.string.error_default));
                    scanner.resumeCameraPreview(this);
                    e.printStackTrace();
                }
            } else {
                showError(getString(R.string.error_default));
                scanner.resumeCameraPreview(this);
            }
        }
    }

    private void configIdText(@NonNull EditText text) {
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                idButton.setEnabled(!Strings.isNullOrEmpty(s.toString()) && TextUtils.isDigitsOnly(s));
            }
        });
    }

    private void configIdButton(@NonNull ImageButton button) {
        button.setOnClickListener(v -> clickId());
        button.setEnabled(false);
    }
}
