package addcel.mobilecard.ui.negocio.mx.wallet.card;

import android.content.Context;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

import com.vinaygaba.creditcardview.CreditCardView;

import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 12/12/18.
 */
public interface NegocioWalletCardContract {

    interface Constants {
        @ColorInt
        int COLOR_GREEN = 0xFF2E7D32;
        @ColorInt
        int COLOR_RED = 0xFFC62828;
        @ColorInt
        int COLOR_GRAPHITE = 0x383428;
    }

    interface View {

        void showProgress();

        void hideProgress();

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);

        void setUI(@NonNull android.view.View view);

        void setBalance(String balance);

        void setClabe(String clabe);

        void launchCardCreation();

        void clickSave();
    }

    interface Presenter {

        CardEntity getModel();

        String getPan();

        String getHolderName();

        String getVigencia();

        String getBalance();

        int getBalanceColor();

        String buildPhoneMessage();

        CreditCardView buildCardView(Context context);
    }
}
