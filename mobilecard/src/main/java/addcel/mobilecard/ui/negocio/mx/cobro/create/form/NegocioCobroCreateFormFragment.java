package addcel.mobilecard.ui.negocio.mx.cobro.create.form;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.R;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroComponentCreator;
import addcel.mobilecard.ui.negocio.mx.cobro.create.qr.NegocioCobroCreateQrFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.FragmentUtil;
import addcel.mobilecard.utils.StringUtil;
import addcel.mobilecard.validation.ValidationUtils;

/**
 * ADDCEL on 07/09/18.
 */
public class NegocioCobroCreateFormFragment extends Fragment
        implements NegocioCobroCreateFormContract.View {
    private static final NumberFormat PESO_FORMAT =
            NumberFormat.getCurrencyInstance(new Locale("es", "MX"));
    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout conceptoTil;
    @Inject
    NegocioCobroActivity activity;
    @Inject
    Validator validator;
    @Inject
    @Named("montoCaptured")
    double montoCaptured;
    private TextView importeView;
    private TextInputLayout propinaTil;

    public NegocioCobroCreateFormFragment() {
    }

    public static synchronized NegocioCobroCreateFormFragment get(double monto) {
        NegocioCobroCreateFormFragment fragment = new NegocioCobroCreateFormFragment();
        fragment.setArguments(new BundleBuilder().putDouble("monto", monto).build());
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NegocioCobroComponentCreator.negocioCobroCreateFormComponent(
                new NegocioCobroCreateFormModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_negocio_cobro_create_form, container, false);
        importeView = view.findViewById(R.id.til_negocio_cobro_create_form_monto);
        propinaTil = view.findViewById(R.id.til_negocio_cobro_create_form_propina);
        conceptoTil = view.findViewById(R.id.til_negocio_cobro_create_form_concepto);
        view.findViewById(R.id.b_negocio_cobro_create_form_continuar)
                .setOnClickListener(v -> clickContinuar());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        AndroidUtils.setText(importeView, PESO_FORMAT.format(montoCaptured));
        activity.hideBottomMenu();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
            activity.hideBottomMenu();
        }
    }

    @Override
    public double getImporte() {
        return montoCaptured;
    }

    @Override
    public double getPropina() {
        EditText editText = Objects.requireNonNull(propinaTil.getEditText());
        String s = Strings.nullToEmpty(editText.getText().toString());
        return StringUtil.isDecimalAmount(s) ? Double.parseDouble(s) : 0.0;
    }

    @Override
    public String getConcepto() {
        EditText editText = Objects.requireNonNull(conceptoTil.getEditText());
        return Strings.nullToEmpty(editText.getText().toString());
    }

    @Override
    public void clickContinuar() {
        clearErrors();
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        FragmentUtil.addFragmentHidingCurrent(activity.getFragmentManagerLazy(),
                R.id.frame_negocio_cobro, this,
                NegocioCobroCreateQrFragment.get(getImporte(), getPropina(), getConcepto()));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    private void clearErrors() {
        if (isVisible()) ValidationUtils.clearViewErrors(importeView, propinaTil, conceptoTil);
    }
}
