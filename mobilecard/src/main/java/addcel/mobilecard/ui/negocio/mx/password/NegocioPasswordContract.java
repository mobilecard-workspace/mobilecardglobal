package addcel.mobilecard.ui.negocio.mx.password;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 13/09/18.
 */
public interface NegocioPasswordContract {
    interface View extends ScreenView, Validator.ValidationListener {

        void returnToMenuIfLogged();

        void clickActualizar();
    }

    interface Presenter {

        void clearDisposables();

        void setPassword(String newPass);

        void saveUsuario(String newPass, int newStatus);
    }
}
