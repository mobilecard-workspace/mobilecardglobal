package addcel.mobilecard.ui.negocio.mx.wallet.card;

import java.text.NumberFormat;
import java.util.Objects;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.negocio.wallet.card.NegocioWalletCardInteractor;
import addcel.mobilecard.domain.negocio.wallet.card.NegocioWalletCardInteractorImpl;
import addcel.mobilecard.ui.negocio.mx.wallet.NegocioWalletActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 12/12/18.
 */
@Module
public final class NegocioWalletCardModule {
    private final NegocioWalletCardFragment fragment;

    public NegocioWalletCardModule(NegocioWalletCardFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    NegocioWalletActivity provideActivity() {
        return (NegocioWalletActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    NegocioAPI provideApi(Retrofit retrofit) {
        return retrofit.create(NegocioAPI.class);
    }

    @PerFragment
    @Provides
    CardEntity provideCard() {
        return Objects.requireNonNull(fragment.getArguments()).getParcelable("card");
    }

    @PerFragment
    @Provides
    NegocioWalletCardInteractor provideInteractor(NegocioAPI api,
                                                  SessionOperations session, CardEntity card) {
        return new NegocioWalletCardInteractorImpl(api, session, card);
    }

    @PerFragment
    @Provides
    NegocioWalletCardContract.Presenter providePresenter(
            NegocioWalletCardInteractor interactor, NumberFormat format) {
        return new NegocioWalletCardPresenter(interactor, format);
    }
}
