package addcel.mobilecard.ui.negocio.mx.cobro.secure;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 08/11/17.
 */
@PerFragment
@Subcomponent(modules = NegocioCobroSecureModule.class)
public interface NegocioCobroSecureSubcomponent {
    void inject(NegocioCobroSecureFragment fragment);
}
