package addcel.mobilecard.ui.negocio.mx.cobro.secure;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.common.base.Charsets;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.McConstants;
import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.usuario.scanpay.result.ScanResultFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * ADDCEL on 17/01/18.
 */
public class NegocioCobroSecureFragment extends Fragment
        implements NegocioCobroSecureContract.View {
    @Inject
    NegocioCobroActivity activity;
    @Inject
    NegocioCobroSecureContract.Presenter presenter;
    @Inject
    WebView browser;
    @Inject
    SPPagoEntity data;

    @BindView(R.id.screen_scanpay_secure)
    FrameLayout container;

    private Unbinder unbinder;

    public NegocioCobroSecureFragment() {
    }

    public static synchronized NegocioCobroSecureFragment get(SPPagoEntity data) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        NegocioCobroSecureFragment fragment = new NegocioCobroSecureFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .negocioCobroSecureSubcomponent(new NegocioCobroSecureModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_scanpay_secure, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_FINISHED);
        container.addView(browser);
        if (savedInstanceState == null) {
            presenter.pago(data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (browser != null) {
            browser.onResume();
            browser.resumeTimers();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (browser != null) {
            browser.stopLoading();
            browser.onPause();
            browser.pauseTimers();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        container.removeAllViews();
        browser.destroy();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NotNull String msg) {
        activity.showError(msg);
    }

    @JavascriptInterface
    @Override
    public void showSuccess(@NotNull String msg) {
        if (isVisible()) {
            SPReceiptEntity response = presenter.createResponse(msg);
            processHthResponse(response);
        }
    }

    @JavascriptInterface
    @Override
    public void log(String msg) {
        Timber.d(msg);
    }

    @Override
    public void onPagoStarted(@NonNull String html) {
        browser.post(() -> {
            browser.clearCache(true);
            browser.loadDataWithBaseURL(BuildConfig.BASE_URL, html, "text/html",
                    Charsets.UTF_8.displayName(), null);
        });
    }

    @Override
    public void processHthResponse(SPReceiptEntity response) {
        String title;
        String msg;
        if (response.getCode() == 0) {
            FragmentManager fragmentManager = activity.getFragmentManagerLazy();
            fragmentManager.beginTransaction()
                    .add(R.id.frame_negocio_cobro,
                            ScanResultFragment.Companion.get(new SPReceiptEntity(response.getCode(), McConstants.PAIS_ID_MX, response.getMessage()),
                                    Boolean.TRUE))
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .remove(this)
                    .commit();
        } else {
            title = "Error: " + response.getCode();
            new AlertDialog.Builder(activity).setTitle(title)
                    .setMessage(response.getMessage())
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> activity.finish())
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss())
                    .show();
        }
    }

    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        Timber.d("onSaveInstanceState");
        try {
            super.onSaveInstanceState(outState);
            browser.saveState(outState);
        } catch (Throwable t) {
            Timber.e(t);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Timber.d("onRestoreInstanceState");
        try {
            browser.restoreState(savedInstanceState);
        } catch (Throwable t) {
            Timber.e(t);
        }
    }
}
