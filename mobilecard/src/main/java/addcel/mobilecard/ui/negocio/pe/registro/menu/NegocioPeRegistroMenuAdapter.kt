package addcel.mobilecard.ui.negocio.pe.registro.menu

/**
 * ADDCEL on 2019-07-09.
 */
import addcel.mobilecard.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

enum class TipoRegistro(val icon: Int, val titleRes: Int, val subRes: Int) {
    QR(R.drawable.qr, R.string.s_registro_negocio_qr, R.string.s_registro_negocio_qr_sub), MANUAL(
            R.drawable.manual,
            R.string.s_registro_negocio_manual,
            R.string.s_registro_negocio_manual_sub
    )
}

class NegocioPeRegistroMenuAdapter :
        RecyclerView.Adapter<NegocioPeRegistroMenuAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.item_peru_registro_menu, parent,
                        false
                )
        )
    }

    override fun getItemCount(): Int {
        return TipoRegistro.values().size
    }

    fun getItem(pos: Int): TipoRegistro {
        return TipoRegistro.values()[pos]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val tipo = TipoRegistro.values()[position]

        holder.icon.setCompoundDrawablesRelativeWithIntrinsicBounds(tipo.icon, 0, 0, 0)
        holder.icon.setText(tipo.titleRes)
        holder.subTitle.setText(tipo.subRes)
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val subTitle: TextView = v.findViewById(R.id.subtitle_negocio_pe_registro_menu)
        val icon: TextView = v.findViewById(R.id.b_negocio_pe_registro_menu)
    }
}