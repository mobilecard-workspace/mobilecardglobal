package addcel.mobilecard.ui.negocio.mx.cobro.create.qr;

import android.os.Bundle;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 07/09/18.
 */
@Module
public final class NegocioCobroCreateQrModule {
    private final NegocioCobroCreateQrFragment fragment;

    public NegocioCobroCreateQrModule(NegocioCobroCreateQrFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    NegocioCobroActivity provideActivity() {
        return (NegocioCobroActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Bundle provideArguments() {
        return Objects.requireNonNull(fragment.getArguments());
    }

    @PerFragment
    @Provides
    double provideMonto(Bundle arguments) {
        return arguments.getDouble("monto");
    }

    @PerFragment
    @Provides
    @Named("propina")
    double providePropina(Bundle arguments) {
        return arguments.getDouble("propina");
    }

    @PerFragment
    @Provides
    String provideConcepto(Bundle arguments) {
        return arguments.getString("concepto");
    }

    @PerFragment
    @Provides
    ScanPayService provideService(Retrofit retrofit) {
        return retrofit.create(ScanPayService.class);
    }

    @PerFragment
    @Provides
    NegocioCobroCreateQrContract.Presenter providePresenter(
            ScanPayService service, SessionOperations session, double monto,
            @Named("propina") double propina, String concepto) {
        return new NegocioCobroCreateQrPresenter(service, session.getNegocio(), monto, propina,
                concepto, fragment);
    }
}
