package addcel.mobilecard.ui.negocio.mx.cobro.usercard.card;

import android.annotation.SuppressLint;
import android.os.Build;

import com.google.common.base.Strings;

import org.jetbrains.annotations.NotNull;

import java.util.regex.Pattern;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.McRegexPatterns;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.scanpay.SPApi;
import addcel.mobilecard.data.net.scanpay.SPPagoEntity;
import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity;
import addcel.mobilecard.data.net.token.TokenEntity;
import addcel.mobilecard.data.net.token.TokenizerAPI;
import addcel.mobilecard.data.net.wallet.FranquiciaEntity;
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity;
import addcel.mobilecard.domain.location.McLocationData;
import addcel.mobilecard.utils.ErrorUtil;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;
import commons.validator.routines.CreditCardValidator;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 10/09/18.
 */
public class NegocioCobroUserCardCardPresenter
        implements NegocioCobroUserCardCardContract.Presenter {
    private final SPApi service;
    private final TokenizerAPI tokenAPI;
    private final NegocioEntity negocio;
    private final McLocationData location;
    private final double monto;
    private final double propina;
    private final String concepto;
    private final CompositeDisposable compositeDisposable;
    private final NegocioCobroUserCardCardContract.View view;

    NegocioCobroUserCardCardPresenter(SPApi service, TokenizerAPI tokenAPI, NegocioEntity negocio,
                                      McLocationData location, double monto, double propina, String concepto,
                                      NegocioCobroUserCardCardContract.View view) {
        this.service = service;
        this.tokenAPI = tokenAPI;
        this.negocio = negocio;
        this.location = location;
        this.monto = monto;
        this.propina = propina;
        this.concepto = concepto;
        this.compositeDisposable = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public void clearDisposables() {
        compositeDisposable.clear();
    }

    @Override
    public double getMonto() {
        return monto;
    }

    @Override
    public double getPropina() {
        return propina;
    }

    @Override
    public String getConcepto() {
        return concepto;
    }

    @Override
    public double calculaComision(SPComisionEntity comision) {
        return comision.getComisionFija() + ((monto + comision.getComisionFija())
                * comision.getComisionPorcentaje());
    }

    @Override
    public String getCurrentAddress() {
        return Strings.nullToEmpty(location.getCurrAddress());
    }

    @SuppressLint("CheckResult")
    @Override
    public void checkComision() {
        view.showProgress();
        compositeDisposable.add(
                service.checkComision(BuildConfig.ADDCEL_APP_ID, 1, StringUtil.getCurrentLanguage(),
                        negocio.getIdEstablecimiento())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(comisionEntity -> {
                            view.hideProgress();
                            if (comisionEntity.getIdError() == 0) {
                                view.onComisionReceived(comisionEntity);
                            } else {
                                view.showError(comisionEntity.getMensajeError());
                            }
                        }, throwable -> {
                            view.hideProgress();
                            view.showError(StringUtil.getNetworkError());
                        }));
    }

    @Override
    public SPPagoEntity buildPagoEntity(String pan, String ct, String vigencia,
                                        SPComisionEntity comision, String nombre, String apellido, String imei, String email,
                                        String celular, String firma) {

        return new SPPagoEntity(monto, calculaComision(comision),
                StringUtil.removeAcentosMultiple(concepto), AddcelCrypto.encryptHard(ct),
                negocio.getIdEstablecimiento(), firma, 0, 0, 0, imei, location.getLat(), location.getLon(),
                Build.MODEL, Integer.parseInt(view.getMsi()), propina,
                StringUtil.removeAcentosMultiple(concepto), String.valueOf(Build.VERSION.SDK_INT),
                AddcelCrypto.encryptHard(pan), TipoTarjetaEntity.CREDITO.name(),
                AddcelCrypto.encryptHard(vigencia), "", nombre, apellido, email, celular);
    }

    @Override
    public void getToken(SPPagoEntity request) {
        view.showProgress();
        compositeDisposable.add(
                tokenAPI.getToken(AddcelCrypto.encryptSensitive(JsonUtil.toJson(request)),
                        request.getFirma().substring(0, 10), BuildConfig.ADDCEL_APP_ID, 1,
                        StringUtil.getCurrentLanguage())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(tokenEntity -> {
                            view.hideProgress();
                            if (tokenEntity.getCode() == 0) {
                                view.onToken(tokenEntity, request);
                            } else {
                                view.showError(tokenEntity.getMessage());
                            }
                        }, throwable -> {
                            view.hideProgress();
                            view.showError(
                                    throwable.getLocalizedMessage() != null ? throwable.getLocalizedMessage()
                                            : ErrorUtil.Companion.getErrorMsg(ErrorUtil.NETWORK));
                        }));
    }

    @Override
    public void launchPago(SPPagoEntity request, TokenEntity token) {
        view.showProgress();
        compositeDisposable.add(service.pagoBP(token.getToken(), BuildConfig.ADDCEL_APP_ID, 1,
                StringUtil.getCurrentLanguage(), SPApi.TIPO_NEGOCIO, token.getAccountId(), request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(spReceiptEntity -> {
                    view.hideProgress();
                    view.onPagoSuccess(spReceiptEntity);
                }, throwable -> {
                    view.hideProgress();
                    view.showError(throwable.getLocalizedMessage() == null ? ErrorUtil.Companion.getErrorMsg(
                            ErrorUtil.NETWORK) : throwable.getLocalizedMessage());
                }));
    }

    @Override
    public FranquiciaEntity getFranquicia(@NotNull String pan) {
        if (Pattern.matches(McRegexPatterns.PREVIVALE, pan)) {
            return FranquiciaEntity.Carnet;
        }
        if (CreditCardValidator.VISA_VALIDATOR.isValid(pan)) {
            return FranquiciaEntity.VISA;
        }
        if (CreditCardValidator.MASTERCARD_VALIDATOR.isValid(pan)) {
            return FranquiciaEntity.MasterCard;
        }
        if (CreditCardValidator.AMEX_VALIDATOR.isValid(pan)) {
            return FranquiciaEntity.AmEx;
        }
        return FranquiciaEntity.VISA;
    }
}
