package addcel.mobilecard.ui.negocio.pe.tarjetapresente.monto

import addcel.mobilecard.R
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.ui.listener.NumberKeyboardListener
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.NegocioPeTarjetaPresenteActivity
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.concepto.NegocioPeTarjetaPresenteConceptoData
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.concepto.NegocioPeTarjetaPresenteConceptoFragment
import addcel.mobilecard.utils.StringUtil
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.view_keyboard_full.*

/**
 * ADDCEL on 2019-07-12.
 */
class NegocioPeTarjetaPresenteKeyboardFragment : Fragment(), NumberKeyboardListener {
    companion object {
        fun get(): NegocioPeTarjetaPresenteKeyboardFragment {
            return NegocioPeTarjetaPresenteKeyboardFragment()
        }
    }

    private var montoString = ""

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_keyboard_full, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        text_keyboard_monto.isFocusable = false
        text_keyboard_monto.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                b_keyboard_total_delete.isEnabled = editable.isNotEmpty()
                b_keyboard_recargar.isEnabled = editable.isNotEmpty()
            }
        })
        b_keyboard_total_delete.isEnabled = false
        b_keyboard_recargar.isEnabled = false

        b_keyboard_total_one.setOnClickListener { onKeyStroke("1") }
        b_keyboard_total_two.setOnClickListener { onKeyStroke("2") }
        b_keyboard_total_three.setOnClickListener { onKeyStroke("3") }
        b_keyboard_total_four.setOnClickListener { onKeyStroke("4") }
        b_keyboard_total_five.setOnClickListener { onKeyStroke("5") }
        b_keyboard_total_six.setOnClickListener { onKeyStroke("6") }
        b_keyboard_total_seven.setOnClickListener { onKeyStroke("7") }
        b_keyboard_total_eight.setOnClickListener { onKeyStroke("8") }
        b_keyboard_total_nine.setOnClickListener { onKeyStroke("9") }
        b_keyboard_total_delete.setOnClickListener { onDeleteStroke() }
        b_keyboard_total_zero.setOnClickListener { onKeyStroke("0") }
        b_keyboard_recargar.setOnClickListener { launchConcepto() }
    }

    private fun launchConcepto() {
        if (montoString.isNotEmpty()) {

            val conceptoData = NegocioPeTarjetaPresenteConceptoData(montoString)

            (activity as NegocioPeTarjetaPresenteActivity).fragmentManagerLazy.commit {
                add(
                        R.id.frame_negocio_negocio_pe_tarjeta_presente,
                        NegocioPeTarjetaPresenteConceptoFragment.get(conceptoData)
                )
                hide(this@NegocioPeTarjetaPresenteKeyboardFragment)
                addToBackStack(null)
                setCustomAnimations(
                        android.R.anim.slide_in_left,
                        android.R.anim.slide_out_right
                )
            }
        } else {
            (activity as NegocioPeTarjetaPresenteActivity).showError(getString(R.string.error_cantidad))
        }
    }

    override fun onKeyStroke(captured: String) {
        if (captured.isNotEmpty() && StringUtil.isDecimalAmount(captured)) {
            montoString += captured
            text_keyboard_monto.text =
                    NumberKeyboardListener.getFormattedMonto(montoString, PaisResponse.PaisEntity.PE)
        } else {
            montoString = ""
            text_keyboard_monto.text = ""
        }
    }

    override fun onDeleteStroke() {
        montoString = StringUtil.removeLast(montoString)
        text_keyboard_monto.text =
                NumberKeyboardListener.getFormattedMonto(montoString, PaisResponse.PaisEntity.PE)
    }
}