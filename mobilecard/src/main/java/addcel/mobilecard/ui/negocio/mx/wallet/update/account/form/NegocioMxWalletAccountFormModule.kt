package addcel.mobilecard.ui.negocio.mx.wallet.update.account.form

import addcel.mobilecard.R
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.negocios.entity.BankEntity
import addcel.mobilecard.data.net.tebca.CommerceAccountsAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.domain.peru.NegocioPeWalletAccountInteractor
import addcel.mobilecard.domain.peru.NegocioPeWalletAccountInteractorImpl
import addcel.mobilecard.ui.negocio.mx.wallet.update.NegocioMxWalletActivity
import android.widget.ArrayAdapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-08-26.
 */

@Module
class NegocioMxWalletAccountFormModule(val fragment: NegocioMxWalletAccountFormFragment) {

    @Provides
    @PerFragment
    fun provideActivity(): NegocioMxWalletActivity {
        return fragment.activity as NegocioMxWalletActivity
    }

    @Provides
    @PerFragment
    fun provideModel(): NegocioMxWalletAccountFormModel {
        return fragment.arguments?.getParcelable("model")!!
    }

    @Provides
    @PerFragment
    fun provideAdapter(
            activity: NegocioMxWalletActivity
    ): ArrayAdapter<BankEntity> {
        val adapter = ArrayAdapter<BankEntity>(activity, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        adapter.setNotifyOnChange(true)
        return adapter
    }

    @Provides
    @PerFragment
    fun provideApi(r: Retrofit): NegocioAPI {
        return r.create(NegocioAPI::class.java)
    }

    @Provides
    @PerFragment
    fun provideAccountsApi(r: Retrofit): CommerceAccountsAPI {
        return CommerceAccountsAPI.get(r)
    }

    @Provides
    @PerFragment
    fun provideInteractor(
            api: NegocioAPI,
            accounts: CommerceAccountsAPI
    ): NegocioPeWalletAccountInteractor {
        return NegocioPeWalletAccountInteractorImpl(api, accounts, CompositeDisposable())
    }

    @Provides
    @PerFragment
    fun providePresenter(
            interactor: NegocioPeWalletAccountInteractor
    ): NegocioMxWalletAccountFormPresenter {
        return NegocioMxWalletAccountFormPresenterImpl(interactor, fragment)
    }
}

@PerFragment
@Subcomponent(modules = [NegocioMxWalletAccountFormModule::class])
interface NegocioMxWalletAccountFormSubcomponent {
    fun inject(fragment: NegocioMxWalletAccountFormFragment)
}