package addcel.mobilecard.ui.negocio.mx.cobro.usercard.form;

import androidx.annotation.NonNull;

import com.mobsandgeeks.saripaar.Validator;

import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 07/09/18.
 */
public interface NegocioCobroUserCardFormContract {
    interface View extends ScreenView, Validator.ValidationListener {

        double getImporte();

        double getPropina();

        String getConcepto();

        void onMontoCaptured(@NonNull CharSequence monto);

        void onPropinaCaptured(@NonNull CharSequence propina);

        void setTotalValue(double value);

        void clickContinuar();
    }

    interface Presenter {
        double calculateTotal(@NonNull CharSequence monto, @NonNull CharSequence propina);

        String formatAmount(double cantidad);
    }
}
