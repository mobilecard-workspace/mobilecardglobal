package addcel.mobilecard.ui.negocio.pe.wallet

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.peru.NegocioPeWalletInteractor
import addcel.mobilecard.utils.StringUtil
import java.text.NumberFormat
import java.util.*

/**
 * ADDCEL on 2019-08-28.
 */
interface NegocioPeWalletPresenter {
    fun clearDisposables()
    fun getAccountInfo()
    fun blockCard(isBlocked: Int)
    fun requestCard()
    fun replaceCard()
    fun favCard(type: Int)
    fun formatSaldo(saldo: Double): String
    fun getBlockMessage(estatus: Int): String
    fun evalIfBlocked(estatus: Int): Boolean
    fun processResult(result: CommerceAccountsResponse)
}

class NegocioPeWalletPresenterImpl(
        val interactor: NegocioPeWalletInteractor,
        val view: NegocioPeWalletView
) : NegocioPeWalletPresenter {

    companion object {
        private const val BLOQUEAR = 0
        private const val DESBLOQUEAR = 1
        private val solFormatter = NumberFormat.getCurrencyInstance(Locale("es", "PE"))
    }

    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun processResult(result: CommerceAccountsResponse) {
        if (result.idError == 0) {
            view.showSuccess(result.mensajeError)
            if (result.tebca != null) view.onTebcaAccount(result.tebca) else view.onTebcaAccountNull()
            if (result.cci != null) view.onAccount(result.cci) else view.onAccountNull()
        } else {
            view.showError(result.mensajeError)
        }
    }

    override fun getAccountInfo() {
        view.showProgress()
        interactor.getAccountInfo(BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage(),
                object : InteractorCallback<CommerceAccountsResponse> {
                    override fun onSuccess(result: CommerceAccountsResponse) {
                        view.hideRetry()
                        view.hideProgress()
                        processResult(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showRetry()
                        view.showError(error)
                    }
                })
    }

    override fun blockCard(isBlocked: Int) {
        view.showProgress()
        val block = if (isBlocked == DESBLOQUEAR) BLOQUEAR else DESBLOQUEAR

        interactor.blockCard(BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage(), block,
                object : InteractorCallback<CommerceAccountsResponse> {
                    override fun onSuccess(result: CommerceAccountsResponse) {
                        view.hideProgress()
                        processResult(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun requestCard() {
        view.showProgress()
        interactor.requestCard(BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage(),
                object : InteractorCallback<CommerceAccountsResponse> {
                    override fun onSuccess(result: CommerceAccountsResponse) {
                        view.hideProgress()
                        processResult(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun replaceCard() {
        view.showProgress()
        interactor.replaceCard(BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage(),
                object : InteractorCallback<CommerceAccountsResponse> {
                    override fun onSuccess(result: CommerceAccountsResponse) {
                        view.hideProgress()
                        processResult(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun favCard(type: Int) {
        view.showProgress()
        interactor.favCard(BuildConfig.ADDCEL_APP_ID, 4, StringUtil.getCurrentLanguage(), type,
                object : InteractorCallback<CommerceAccountsResponse> {
                    override fun onSuccess(result: CommerceAccountsResponse) {
                        view.hideProgress()
                        processResult(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun formatSaldo(saldo: Double): String {
        return solFormatter.format(saldo)
    }

    override fun getBlockMessage(estatus: Int): String {
        val action = if (estatus == BLOQUEAR) "desbloquear" else "bloquear"
        return "¿Deseas $action tu cuenta?"
    }

    override fun evalIfBlocked(estatus: Int): Boolean {
        return estatus == BLOQUEAR
    }
}