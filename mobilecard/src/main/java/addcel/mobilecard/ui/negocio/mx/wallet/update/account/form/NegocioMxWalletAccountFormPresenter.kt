package addcel.mobilecard.ui.negocio.mx.wallet.update.account.form

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.net.negocios.entity.BankEntity
import addcel.mobilecard.data.net.negocios.entity.BankResponse
import addcel.mobilecard.data.net.tebca.CciRequest
import addcel.mobilecard.data.net.tebca.CommerceAccountsResponse
import addcel.mobilecard.domain.InteractorCallback
import addcel.mobilecard.domain.peru.NegocioPeWalletAccountInteractor
import addcel.mobilecard.utils.StringUtil
import com.google.common.collect.Lists

/**
 * ADDCEL on 2019-08-26.
 */
interface NegocioMxWalletAccountFormPresenter {
    fun clearDisposables()
    fun getBanks()
    fun setSelectedBankPos(bankName: String?, banks: List<BankEntity>): Int
    fun updateAccount(cciRequest: CciRequest)
    fun addAccount(cciRequest: CciRequest)
}

class NegocioMxWalletAccountFormPresenterImpl(
        private val interactor: NegocioPeWalletAccountInteractor,
        private val view: NegocioWalletAccountFormView
) : NegocioMxWalletAccountFormPresenter {
    override fun clearDisposables() {
        interactor.clearDisposables()
    }

    override fun getBanks() {
        view.showProgress()
        interactor.getBanks(BuildConfig.ADDCEL_APP_ID, 1, StringUtil.getCurrentLanguage(),
                object : InteractorCallback<BankResponse> {
                    override fun onSuccess(result: BankResponse) {
                        view.hideProgress()
                        if (result.idError == 0L) view.setBanks(result.banks) else view.setBanks(
                                Lists.newArrayList()
                        )
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.setBanks(Lists.newArrayList())
                    }
                })
    }

    override fun setSelectedBankPos(bankName: String?, banks: List<BankEntity>): Int {
        if (bankName == null) return 0
        else {
            for (item in banks.indices) {
                if (bankName.equals(banks[item].nombreCorto, true) || bankName.equals(
                                banks[item].nombreRazonSocial, true
                        )
                ) return item
            }
        }
        return 0
    }

    override fun updateAccount(cciRequest: CciRequest) {
        view.showProgress()
        interactor.addAccount(BuildConfig.ADDCEL_APP_ID,
                1,
                StringUtil.getCurrentLanguage(),
                cciRequest,
                object : InteractorCallback<CommerceAccountsResponse> {
                    override fun onSuccess(result: CommerceAccountsResponse) {
                        view.hideProgress()
                        view.onAccountAdded(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }

    override fun addAccount(cciRequest: CciRequest) {
        view.showProgress()
        interactor.addAccount(BuildConfig.ADDCEL_APP_ID,
                1,
                StringUtil.getCurrentLanguage(),
                cciRequest,
                object : InteractorCallback<CommerceAccountsResponse> {
                    override fun onSuccess(result: CommerceAccountsResponse) {
                        view.hideProgress()
                        view.onAccountAdded(result)
                    }

                    override fun onError(error: String) {
                        view.hideProgress()
                        view.showError(error)
                    }
                })
    }
}