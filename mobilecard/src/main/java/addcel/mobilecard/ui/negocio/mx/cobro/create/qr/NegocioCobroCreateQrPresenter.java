package addcel.mobilecard.ui.negocio.mx.cobro.create.qr;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.View;

import net.glxn.qrgen.android.QRCode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.scanpay.ScanPayService;
import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity;
import addcel.mobilecard.data.net.scanpay.model.SPQrEntity;
import addcel.mobilecard.ui.login.tipo.LoginTipoContract;
import addcel.mobilecard.utils.JsonUtil;
import addcel.mobilecard.utils.StringUtil;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import mx.mobilecard.crypto.AddcelCrypto;

/**
 * ADDCEL on 07/09/18.
 */
public class NegocioCobroCreateQrPresenter implements NegocioCobroCreateQrContract.Presenter {
    private final ScanPayService service;
    private final NegocioEntity entity;
    private final double monto;
    private final double propina;
    private final String concepto;
    private final NegocioCobroCreateQrContract.View view;
    private final CompositeDisposable compositeDisposable;
    private int idBitacoraTransaccion;

    NegocioCobroCreateQrPresenter(ScanPayService service, NegocioEntity entity, double monto,
                                  double propina, String concepto, NegocioCobroCreateQrContract.View view) {
        this.service = service;
        this.entity = entity;
        this.monto = monto;
        this.propina = propina;
        this.concepto = concepto;
        this.compositeDisposable = new CompositeDisposable();
        this.view = view;
    }

    @Override
    public SPQrEntity buildEntity(SPComisionEntity comision) {

        return new SPQrEntity(0.0,
                monto,
                1.0,
                calculaComision(comision),
                StringUtil.removeAcentosMultiple(concepto),
                entity.getIdEstablecimiento(),
                0,
                0,
                StringUtil.removeAcentosMultiple(concepto));
    }

    @Override
    public boolean isIdSet() {
        return idBitacoraTransaccion != 0;
    }

    @SuppressLint("CheckResult")
    @Override
    public void createQrCode(int sideLen, int fgColor, int bgColor, SPComisionEntity comision) {
        view.showProgress();

        final String cleanQr = JsonUtil.toJson(buildEntity(comision));
        final String encryptedQR = AddcelCrypto.encryptHard(cleanQr);

        Single<Bitmap> single = Single.fromCallable(
                () -> QRCode.from(encryptedQR)
                        .withCharset("UTF-8")
                        .withColor(fgColor, bgColor)
                        .withSize(sideLen, sideLen)
                        .bitmap())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

        compositeDisposable.add(single.subscribe(bitmap -> {
            view.hideProgress();
            view.setQrCode(bitmap, comision);
        }, throwable -> {
            view.showProgress();
            view.showError(StringUtil.getNetworkError());
        }));
    }

    @Override
    public Bitmap getBitmapOfRootView(View view) {
        View rootview = view.getRootView();
        rootview.setDrawingCacheEnabled(true);
        return rootview.getDrawingCache();
    }

    @Override
    public String buildScreenShotName() {
        String fecha = DateFormat.format("yyyyMMddhhmmss", Calendar.getInstance()).toString();
        return "scanpay_" + fecha + ".png";
    }

    @Override
    public void createImage(Bitmap bmp) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        File file = new File(Environment.getExternalStorageDirectory() + "/sc_transfer.jpg");
        try {
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(bytes.toByteArray());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void clearCompositeDisposable() {
        compositeDisposable.clear();
    }

    @SuppressLint("CheckResult")
    @Override
    public void checkComision() {
        view.showProgress();
        Observable<SPComisionEntity> cObservable =
                service.checkComision(BuildConfig.ADDCEL_APP_ID, entity.getIdPais(), StringUtil.getCurrentLanguage(),
                        entity.getIdEstablecimiento())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        compositeDisposable.add(cObservable.subscribe(comisionEntity -> {
            view.hideProgress();
            if (comisionEntity.getIdError() == 0) {
                view.onComisionReceived(comisionEntity);
            } else {
                view.showError(comisionEntity.getMensajeError());
            }
        }, throwable -> {
            view.hideProgress();
            view.showError(StringUtil.getNetworkError());
        }));
    }

    @SuppressLint("CheckResult")
    @Override
    public void getId(SPComisionEntity comision) {
        view.showProgress();
        compositeDisposable.add(
                service.getId(BuildConfig.ADDCEL_APP_ID, entity.getIdPais(), StringUtil.getCurrentLanguage(),
                        LoginTipoContract.Tipo.NEGOCIO.name().toLowerCase(), concepto,
                        entity.getIdEstablecimiento(), monto, calculaComision(comision),
                        entity.getNombreEstablecimiento(), propina)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(spIdEntity -> {
                            view.hideProgress();
                            if (spIdEntity.getIdError() == 0) {
                                view.setId(spIdEntity.getIdBitacora());
                                idBitacoraTransaccion = spIdEntity.getIdBitacora();
                            } else {
                                view.showError(spIdEntity.getMensajeError());
                            }
                        }, throwable -> {
                            view.hideProgress();
                            view.showError(StringUtil.getNetworkError());
                        }));
    }

    @SuppressLint("CheckResult")
    @Override
    public void validateId() {
        view.showProgress();
        compositeDisposable.add(
                service.getResultByID(BuildConfig.ADDCEL_APP_ID, entity.getIdPais(), StringUtil.getCurrentLanguage(),
                        idBitacoraTransaccion)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(receiptEntity -> {
                            view.hideProgress();
                            if (receiptEntity.getCode() == 0) {
                                view.onConfirmId(receiptEntity);
                            } else {
                                view.showError(receiptEntity.getMessage());
                            }
                        }, throwable -> {
                            view.hideProgress();
                            view.showError(StringUtil.getNetworkError());
                        }));
    }

    private double calculaComision(SPComisionEntity comision) {
        return comision.getComisionFija() + ((monto + comision.getComisionFija())
                * comision.getComisionPorcentaje());
    }
}
