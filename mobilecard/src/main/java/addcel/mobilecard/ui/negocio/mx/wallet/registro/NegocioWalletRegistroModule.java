package addcel.mobilecard.ui.negocio.mx.wallet.registro;

import android.widget.ArrayAdapter;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.squareup.otto.Bus;

import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.local.shared.usuario.SessionOperations;
import addcel.mobilecard.data.net.catalogo.CatalogoService;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.negocios.NegocioAPI;
import addcel.mobilecard.data.net.wallet.WalletAPI;
import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.domain.negocio.wallet.NegocioWalletInteractor;
import addcel.mobilecard.domain.negocio.wallet.NegocioWalletInteractorImpl;
import addcel.mobilecard.ui.negocio.mx.wallet.NegocioWalletActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * ADDCEL on 12/11/18.
 */
@Module
public final class NegocioWalletRegistroModule {
    private final NegocioWalletRegistroFragment fragment;

    NegocioWalletRegistroModule(NegocioWalletRegistroFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    NegocioWalletActivity provideActivity() {
        return (NegocioWalletActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    Validator provideValidator() {
        ViewDataAdapter<TextInputLayout, String> adapter =
                new ViewDataAdapter<TextInputLayout, String>() {
                    @Override
                    public String getData(TextInputLayout view) {
                        return Strings.nullToEmpty(
                                Objects.requireNonNull(view.getEditText()).getText().toString());
                    }
                };
        Validator v = new Validator(fragment);
        v.setValidationListener(fragment);
        v.registerAdapter(TextInputLayout.class, adapter);
        return v;
    }

    @PerFragment
    @Provides
    ArrayAdapter<EstadoResponse.EstadoEntity> provideEstadoAdapter(
            NegocioWalletActivity activity) {
        ArrayAdapter<EstadoResponse.EstadoEntity> adapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter.setNotifyOnChange(Boolean.TRUE);
        return adapter;
    }

    @PerFragment
    @Provides
    NegocioAPI provideApi(Retrofit retrofit) {
        return retrofit.create(NegocioAPI.class);
    }

    @PerFragment
    @Provides
    NegocioWalletInteractor provideInteractor(WalletAPI api,
                                              CatalogoService catalogo, Bus bus, SessionOperations session) {
        return new NegocioWalletInteractorImpl(api, catalogo, bus, session);
    }

    @PerFragment
    @Provides
    NegocioWalletRegistroContract.Presenter providePresenter(
            NegocioWalletInteractor interactor) {
        return new NegocioWalletRegistroPresenter(interactor, fragment);
    }
}
