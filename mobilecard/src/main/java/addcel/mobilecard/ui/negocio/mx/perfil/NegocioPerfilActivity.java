package addcel.mobilecard.ui.negocio.mx.perfil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import addcel.mobilecard.R;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.utils.BundleBuilder;

public class NegocioPerfilActivity extends AppCompatActivity {

    private NegocioEntity negocioEntity;

    private TextInputLayout stablecimientoTil;
    private TextInputLayout nombreTil;
    private TextInputLayout emailTil;
    private TextInputLayout direccionTil;

    public static Intent get(Context context, NegocioEntity negocioEntity) {
        BundleBuilder bundleBuilder = new BundleBuilder().putParcelable("negocio", negocioEntity);
        return new Intent(context, NegocioPerfilActivity.class).putExtras(bundleBuilder.build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNegocio(Objects.requireNonNull(getIntent().getExtras()));
        setContentView(R.layout.activity_negocio_perfil);
        setSupportActionBar(findViewById(R.id.toolbar_negocio_perfil));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        stablecimientoTil = findViewById(R.id.til_profile_establecimiento);
        nombreTil = findViewById(R.id.til_profile_nombre);
        emailTil = findViewById(R.id.til_profile_email);
        direccionTil = findViewById(R.id.til_profile_direccion);
        findViewById(R.id.b_profile_ok).setOnClickListener(v -> NegocioPerfilActivity.this.finish());
        setInfo(negocioEntity);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setNegocio(Bundle bundle) {
        negocioEntity = bundle.getParcelable("negocio");
    }

    private void setInfo(NegocioEntity negocio) {
        Objects.requireNonNull(stablecimientoTil.getEditText())
                .setText(negocio.getNombreEstablecimiento());
        stablecimientoTil.setEnabled(false);
        Objects.requireNonNull(nombreTil.getEditText()).setText(negocio.getRepresentanteLegal());
        nombreTil.setEnabled(false);
        Objects.requireNonNull(emailTil.getEditText()).setText(negocio.getEmail());
        emailTil.setEnabled(false);
        Objects.requireNonNull(direccionTil.getEditText())
                .setText(negocio.getDireccionEstablecimiento());
        direccionTil.setEnabled(false);
    }
}
