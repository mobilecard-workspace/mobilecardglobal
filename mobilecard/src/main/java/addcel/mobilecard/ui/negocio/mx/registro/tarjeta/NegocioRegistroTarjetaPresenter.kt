package addcel.mobilecard.ui.negocio.mx.registro.tarjeta

import addcel.mobilecard.BuildConfig
import addcel.mobilecard.data.local.shared.state.StateSession
import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.catalogo.CatalogoService
import addcel.mobilecard.data.net.catalogo.model.PaisResponse
import addcel.mobilecard.data.net.negocios.NegocioAPI
import addcel.mobilecard.data.net.negocios.entity.NegocioCreateRequest
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.StringUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ADDCEL on 2019-12-16.
 */
interface NegocioRegistroTarjetaPresenter {

    fun addToDisposables(d: Disposable)

    fun clearDisposables()

    fun getEstados()

    fun getTerminos()

    fun getPrivacidad()

    fun createNegocio(createRequest: NegocioCreateRequest)

    fun loginNegocio(negocioEntity: NegocioEntity)

    var terminos: String

    var privacidad: String
}

class NegocioRegistroTarjetaPresenterImpl(
        val api: NegocioAPI,
        val catalogoService: CatalogoService,
        val session: SessionOperations,
        val state: StateSession,
        val compositeDisposable: CompositeDisposable = CompositeDisposable(),
        val view: NegocioRegistroTarjetaView
) : NegocioRegistroTarjetaPresenter {

    override var terminos: String = ""

    override var privacidad: String = ""

    override fun addToDisposables(d: Disposable) {
        compositeDisposable.add(d)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
    }

    override fun getEstados() {
        view.showProgress()
        val bDisposable =
                catalogoService.getEstados(BuildConfig.ADDCEL_APP_ID, 1, StringUtil.getCurrentLanguage())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    view.hideProgress()
                                    if (it.idError == 0) {
                                        view.hideRetry()
                                        view.setEstados(it.estados)
                                    } else {
                                        view.showRetry()
                                        view.showError(it.mensajeError)
                                    }
                                },
                                {
                                    view.showRetry()
                                    view.hideProgress()
                                    view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                                })

        addToDisposables(bDisposable)

    }

    override fun getTerminos() {
        if (terminos.isNotEmpty()) {
            //view.showTerminos(terminos)
        } else {
            view.showProgress()
            val tDisposable = api.getTerminos(
                    BuildConfig.ADDCEL_APP_ID,
                    StringUtil.sha256("mobilecardandroid"), 1, StringUtil.getCurrentLanguage()
            )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                view.hideProgress()
                                if (it.idError == 0) {
                                    terminos = it.terminos
                                    // view.showTerminos(terminos)
                                } else
                                    view.showError(it.mensajeError)
                            },
                            {
                                view.hideProgress()
                                view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                            })
            addToDisposables(tDisposable)
        }
    }

    override fun getPrivacidad() {
        if (privacidad.isNotEmpty()) {
            //view.showPrivacidad(privacidad)
        } else {
            view.showProgress()
            val pDisposable = api.getAvisoPrivacidad(
                    BuildConfig.ADDCEL_APP_ID,
                    StringUtil.sha256("mobilecardandroid"), 1, StringUtil.getCurrentLanguage()
            )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                view.hideProgress()
                                if (it.idError == 0) {
                                    privacidad = it.terminos
                                    //view.showPrivacidad(privacidad)
                                } else
                                    view.showError(it.mensajeError)
                            },
                            {
                                view.hideProgress()
                                view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                            })
            addToDisposables(pDisposable)
        }
    }

    override fun createNegocio(createRequest: NegocioCreateRequest) {
        view.showProgress()
        val cDisposable = api.create(
                BuildConfig.ADDCEL_APP_ID,
                PaisResponse.PaisEntity.MX,
                StringUtil.getCurrentLanguage(), createRequest
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.hideProgress()
                            if (it.idError == 0) {
                                loginNegocio(it)
                            } else {
                                view.showError(it.mensajeError)
                            }
                        },
                        {
                            view.hideProgress()
                            view.showError(ErrorUtil.getFormattedHttpErrorMsg(it))
                        }
                )
        addToDisposables(cDisposable)
    }

    override fun loginNegocio(negocioEntity: NegocioEntity) {
        session.logout()

        session.negocio = negocioEntity
        session.isNegocioLogged = java.lang.Boolean.TRUE

        state.setCapturedEmail("")
        state.setCapturedPhone("")
        state.setMenuLaunched(false)
        view.onLoginSuccess(negocioEntity.mensajeError, negocioEntity.isCardPrevivale)
    }
}