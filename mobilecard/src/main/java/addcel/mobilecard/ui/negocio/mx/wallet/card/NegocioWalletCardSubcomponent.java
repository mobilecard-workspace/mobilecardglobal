package addcel.mobilecard.ui.negocio.mx.wallet.card;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 12/12/18.
 */
@PerFragment
@Subcomponent(modules = NegocioWalletCardModule.class)
public interface NegocioWalletCardSubcomponent {
    void inject(NegocioWalletCardFragment fragment);
}
