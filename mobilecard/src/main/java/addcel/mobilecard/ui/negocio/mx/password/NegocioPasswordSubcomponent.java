package addcel.mobilecard.ui.negocio.mx.password;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 13/09/18.
 */
@PerActivity
@Subcomponent(modules = NegocioPasswordModule.class)
public interface NegocioPasswordSubcomponent {
    void inject(NegocioPasswordActivity activity);
}
