package addcel.mobilecard.ui.negocio.mx.password;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroModel;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.validation.ValidationUtils;
import es.dmoral.toasty.Toasty;

public class NegocioPasswordActivity extends AppCompatActivity
        implements NegocioPasswordContract.View {

    @Inject
    NegocioPasswordContract.Presenter presenter;
    @Inject
    Validator validator;
    @Inject
    boolean loggedIn;

    @Password(messageResId = R.string.error_password, scheme = Password.Scheme.ANY, min = 8)
    private TextInputLayout newPassTil;

    @ConfirmPassword(messageResId = R.string.error_password)
    private TextInputLayout confPassTil;

    private ProgressBar progressBar;

    public static Intent get(Context context, NegocioEntity negocio, boolean loggedIn) {
        return new Intent(context, NegocioPasswordActivity.class).putExtras(
                new BundleBuilder().putParcelable("negocio", negocio)
                        .putBoolean("loggedIn", loggedIn)
                        .build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mobilecard.get()
                .getNetComponent()
                .negocioPasswordSubcomponent(new NegocioPasswordModule(this))
                .inject(this);
        setContentView(R.layout.activity_password);

        setUI();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        presenter.clearDisposables();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (getProgressBar().getVisibility() == View.GONE)
            getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (getProgressBar().getVisibility() == View.VISIBLE)
            getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        Toasty.error(this, msg).show();
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        Toasty.success(this, msg).show();
    }

    @Override
    public void returnToMenuIfLogged() {
        if (!loggedIn)
            startActivity(NegocioCobroActivity.Companion.get(this, new NegocioCobroModel(NegocioCobroActivity.USE_CASE_MENU)));
        finish();
    }

    @Override
    public void clickActualizar() {
        ValidationUtils.clearViewErrors(newPassTil, confPassTil);
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        presenter.setPassword(
                Strings.nullToEmpty(Objects.requireNonNull(newPassTil.getEditText()).getText().toString()));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ValidationUtils.onValidationFailed(this, errors);
    }

    private void setUI() {
        findViewById(R.id.til_password_old).setVisibility(View.GONE);
        newPassTil = findViewById(R.id.til_password_new);
        confPassTil = findViewById(R.id.til_password_conf);
        findViewById(R.id.b_password_actualizar).setOnClickListener(v -> clickActualizar());

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private ProgressBar getProgressBar() {
        if (progressBar == null) progressBar = findViewById(R.id.progress_password);
        return progressBar;
    }
}
