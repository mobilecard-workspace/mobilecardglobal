package addcel.mobilecard.ui.negocio

import addcel.mobilecard.R
import addcel.mobilecard.ui.custom.view.SignatureView
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Base64
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.screen_negocio_cobro_signature.*
import java.io.ByteArrayOutputStream

class NegocioSignatureActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE = 401
        const val DATA_SIGNATURE = "firma"

        fun get(context: Context): Intent {
            return Intent(context, NegocioSignatureActivity::class.java)
        }
    }

    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.screen_negocio_cobro_signature)
        signature_negocio_cobro_signature.isDrawingCacheEnabled = true


        b_negocio_cobro_signature_borrar.setOnClickListener {
            clearFirma(signature_negocio_cobro_signature)
        }

        b_negocio_cobro_signature_continuar.setOnClickListener {
            if (signature_negocio_cobro_signature.isEmpty) Toasty.error(
                    this@NegocioSignatureActivity,
                    getString(R.string.error_firma)
            ).show()
            else getFirma(signature_negocio_cobro_signature.drawingCache)
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, Intent())
        finish()
    }

    private fun getFirma(bitmap: Bitmap) {
        val single = Single.just(bitmap).map { bmp ->
            val baos = ByteArrayOutputStream()
            bmp.compress(Bitmap.CompressFormat.PNG, 10, baos)
            val imageBytes = baos.toByteArray()
            Base64.encodeToString(imageBytes, Base64.DEFAULT)
        }

        compositeDisposable.add(single.subscribeOn(Schedulers.computation()).observeOn(
                AndroidSchedulers.mainThread()
        ).subscribe({ onSuccess(it) }, {
            onError()
        })
        )
    }

    private fun onSuccess(firma: String) {
        val intent = Intent().putExtra(DATA_SIGNATURE, firma)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun onError() {
        setResult(-200, Intent())
        finish()
    }

    private fun clearFirma(view: SignatureView) {
        view.clear()
    }
}
