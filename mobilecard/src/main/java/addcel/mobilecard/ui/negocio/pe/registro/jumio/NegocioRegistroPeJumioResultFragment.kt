package addcel.mobilecard.ui.negocio.pe.registro.jumio

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * ADDCEL on 2019-07-14.
 */

@Parcelize
data class NegocioRegistroPeJumioResultFragmentData(
        val login: String,
        val reference: String, val message: String, val buttonText: String, val idPais: Int = 4
) :
        Parcelable

