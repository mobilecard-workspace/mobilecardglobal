package addcel.mobilecard.ui.negocio.mx.wallet.registro;

import android.widget.EditText;

import androidx.annotation.NonNull;

import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.data.net.wallet.PrevivaleUserRequest;
import addcel.mobilecard.domain.negocio.wallet.NegocioWalletEstadoEvent;

/**
 * ADDCEL on 12/11/18.
 */
public interface NegocioWalletRegistroContract {
    interface View extends Validator.ValidationListener {

        String CURP_URL = "https://www.gob.mx/curp/";

        PrevivaleUserRequest buildRequest();

        void setUi(android.view.View view);

        void setCardTextWatcher(@NonNull EditText text);

        void onPanCaptured(CharSequence s);

        void launchGetEstados();

        void setEstados(List<EstadoResponse.EstadoEntity> estados);

        void showProgress();

        void hideProgress();

        void showError(@NonNull String msg);

        void showSuccess(@NonNull String msg);

        void launchRegistro();

        void onRegistroSuccess(@NonNull CardEntity model);

        void finishActivity();
    }

    interface Presenter {
        NegocioEntity getNegocioInfo();

        void register();

        void unregister();

        void onGetEstadosLaunched();

        void onEstadosReceived(NegocioWalletEstadoEvent event);

        void onRegistroLaunched();

        void clearDisposables();
    }
}
