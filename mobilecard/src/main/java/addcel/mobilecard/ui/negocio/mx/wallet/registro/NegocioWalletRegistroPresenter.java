package addcel.mobilecard.ui.negocio.mx.wallet.registro;

import com.squareup.otto.Subscribe;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.BuildConfig;
import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.wallet.CardEntity;
import addcel.mobilecard.domain.InteractorCallback;
import addcel.mobilecard.domain.negocio.wallet.NegocioWalletEstadoEvent;
import addcel.mobilecard.domain.negocio.wallet.NegocioWalletInteractor;
import addcel.mobilecard.utils.StringUtil;

/**
 * ADDCEL on 12/11/18.
 */
class NegocioWalletRegistroPresenter implements NegocioWalletRegistroContract.Presenter {
    private final NegocioWalletInteractor interactor;
    private final NegocioWalletRegistroContract.View view;

    NegocioWalletRegistroPresenter(NegocioWalletInteractor interactor,
                                   NegocioWalletRegistroContract.View view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public NegocioEntity getNegocioInfo() {
        return interactor.getNegocioInfo();
    }

    @Override
    public void register() {
        interactor.getBus().register(this);
    }

    @Override
    public void unregister() {
        interactor.clearDisposables();
        interactor.getBus().unregister(this);
    }

    @Override
    public void onGetEstadosLaunched() {
        view.showProgress();
        interactor.getEstados(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage());
    }

    @Subscribe
    @Override
    public void onEstadosReceived(NegocioWalletEstadoEvent event) {
        view.hideProgress();
        if (event.getData().getIdError() == 0) {
            view.setEstados(event.getData().getEstados());
        } else {
            view.showError(event.getData().getMensajeError());
            view.finishActivity();
        }
    }

    @Override
    public void onRegistroLaunched() {
        view.showProgress();
        interactor.registrar(BuildConfig.ADDCEL_APP_ID, StringUtil.getCurrentLanguage(),
                view.buildRequest(), new InteractorCallback<CardEntity>() {
                    @Override
                    public void onSuccess(@NotNull CardEntity result) {
                        view.hideProgress();
                        view.onRegistroSuccess(result);
                    }

                    @Override
                    public void onError(@NotNull String error) {
                        view.hideProgress();
                        view.showError(error);
                    }
                });
    }

    @Override
    public void clearDisposables() {
        interactor.clearDisposables();
    }
}
