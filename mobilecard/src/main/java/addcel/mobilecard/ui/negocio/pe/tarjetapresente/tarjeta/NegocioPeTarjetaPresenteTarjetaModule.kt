package addcel.mobilecard.ui.negocio.pe.tarjetapresente.tarjeta

import addcel.mobilecard.data.local.shared.usuario.SessionOperations
import addcel.mobilecard.data.net.scanpay.SPApi
import addcel.mobilecard.data.net.token.TokenizerAPI
import addcel.mobilecard.di.scope.PerFragment
import addcel.mobilecard.domain.scanpay.open.ScanPayOpenInteractor
import addcel.mobilecard.ui.negocio.pe.tarjetapresente.NegocioPeTarjetaPresenteActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

/**
 * ADDCEL on 2019-07-15.
 */
@Module
class NegocioPeTarjetaPresenteTarjetaModule(
        val fragment: NegocioPeTarjetaPresenteTarjetaFragment
) {

    @PerFragment
    @Provides
    fun provideContainerAct(): NegocioPeTarjetaPresenteActivity {
        return fragment.activity as NegocioPeTarjetaPresenteActivity
    }

    @PerFragment
    @Provides
    fun provideData(): NegocioPeTarjetaPresenteTarjetaData {
        return fragment.arguments?.getParcelable("data")!!
    }

    @PerFragment
    @Provides
    fun provideSPApi(r: Retrofit): SPApi {
        return SPApi.provide(r)
    }

    @PerFragment
    @Provides
    fun provideTokenizer(r: Retrofit): TokenizerAPI {
        return TokenizerAPI.provideTokenizerAPI(r)
    }

    @PerFragment
    @Provides
    fun provideInteractor(
            spApi: SPApi, tokenizerAPI: TokenizerAPI,
            session: SessionOperations
    ): ScanPayOpenInteractor {
        return ScanPayOpenInteractor(spApi, tokenizerAPI, session, CompositeDisposable())
    }

    @PerFragment
    @Provides
    fun providePresenter(
            interactor: ScanPayOpenInteractor
    ): NegocioPeTarjetaPresenteTarjetaPresenter {
        return NegocioPeTarjetaPresenteTarjetaPresenterImpl(interactor, fragment)
    }
}

@PerFragment
@Subcomponent(modules = [NegocioPeTarjetaPresenteTarjetaModule::class])
interface NegocioPeTarjetaPresenteTarjetaSubcomponent {
    fun inject(fragment: NegocioPeTarjetaPresenteTarjetaFragment)
}