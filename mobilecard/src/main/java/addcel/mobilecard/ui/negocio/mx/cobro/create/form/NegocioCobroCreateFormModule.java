package addcel.mobilecard.ui.negocio.mx.cobro.create.form;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;

import java.util.Objects;

import javax.inject.Named;

import addcel.mobilecard.di.scope.PerFragment;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import dagger.Module;
import dagger.Provides;

/**
 * ADDCEL on 07/09/18.
 */
@Module
public final class NegocioCobroCreateFormModule {
    private final NegocioCobroCreateFormFragment fragment;

    NegocioCobroCreateFormModule(NegocioCobroCreateFormFragment fragment) {
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    NegocioCobroActivity provideActivity() {
        return (NegocioCobroActivity) Objects.requireNonNull(fragment.getActivity());
    }

    @PerFragment
    @Provides
    @Named("montoCaptured")
    double providesMonto() {
        return Objects.requireNonNull(fragment.getArguments()).getDouble("monto");
    }

    @PerFragment
    @Provides
    @Named("tilAdapter")
    ViewDataAdapter<TextInputLayout, String> provideAdapter() {
        return new ViewDataAdapter<TextInputLayout, String>() {
            @Override
            public String getData(TextInputLayout view) {
                return Objects.requireNonNull(view.getEditText()).getText().toString();
            }
        };
    }

    @PerFragment
    @Provides
    Validator provideValidator(
            @Named("tilAdapter") ViewDataAdapter<TextInputLayout, String> adapter) {
        Validator validator = new Validator(fragment);
        validator.registerAdapter(TextInputLayout.class, adapter);
        validator.setValidationListener(fragment);
        return validator;
    }
}
