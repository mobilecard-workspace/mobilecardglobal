package addcel.mobilecard.ui.negocio.mx.cobro.menu;

import addcel.mobilecard.di.scope.PerFragment;
import dagger.Subcomponent;

/**
 * ADDCEL on 07/09/18.
 */
@PerFragment
@Subcomponent(modules = NegocioCobroMenuModule.class)
public interface NegocioCobroMenuSubcomponent {
    void inject(NegocioCobroMenuFragment fragment);
}
