package addcel.mobilecard.ui.negocio.mx.cobro.usercard.form;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import addcel.mobilecard.Mobilecard;
import addcel.mobilecard.R;
import addcel.mobilecard.ui.ContainerScreenView;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroActivity;
import addcel.mobilecard.ui.negocio.mx.cobro.NegocioCobroComponentCreator;
import addcel.mobilecard.ui.negocio.mx.cobro.usercard.card.NegocioCobroUserCardCardFragment;
import addcel.mobilecard.utils.AndroidUtils;
import addcel.mobilecard.utils.BundleBuilder;
import addcel.mobilecard.utils.FragmentUtil;
import addcel.mobilecard.validation.ValidationUtils;

/**
 * ADDCEL on 07/09/18.
 */
public class NegocioCobroUserCardFormFragment extends Fragment
        implements NegocioCobroUserCardFormContract.View {
    private static final NumberFormat PESO_FORMAT =
            NumberFormat.getCurrencyInstance(new Locale("es", "MX"));
    @Inject
    NegocioCobroActivity activity;
    @Inject
    Validator validator;
    @Inject
    @Named("montoCaptured")
    double montoCaptured;
    @Inject
    NegocioCobroUserCardFormContract.Presenter presenter;
    @NotEmpty(messageResId = R.string.error_campo_generico)
    TextInputLayout conceptoTil;
    private TextView importeView;
    private TextInputLayout propinaTil;
    private TextView totalView;

    public NegocioCobroUserCardFormFragment() {
    }

    public static synchronized NegocioCobroUserCardFormFragment get(double monto) {
        NegocioCobroUserCardFormFragment fragment = new NegocioCobroUserCardFormFragment();
        fragment.setArguments(new BundleBuilder().putDouble("monto", monto).build());
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NegocioCobroComponentCreator.negocioCobroUserCardFormSubcomponent(Mobilecard.get(),
                new NegocioCobroUserCardFormModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_negocio_cobro_usercard_form, container, false);
        importeView = view.findViewById(R.id.til_negocio_cobro_usercard_form_monto);
        propinaTil = view.findViewById(R.id.til_negocio_cobro_usercard_form_propina);
        conceptoTil = view.findViewById(R.id.til_negocio_cobro_usercard_form_concepto);
        totalView = view.findViewById(R.id.view_negocio_cobro_usercard_form_total);
        view.findViewById(R.id.b_negocio_cobro_usercard_form_continuar)
                .setOnClickListener(v -> clickContinuar());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
        activity.hideBottomMenu();
        AndroidUtils.setText(importeView, PESO_FORMAT.format(getImporte()));

        Objects.requireNonNull(propinaTil.getEditText()).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                onPropinaCaptured(s);
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            activity.setStreenStatus(ContainerScreenView.STATUS_NORMAL);
            activity.hideBottomMenu();
        }
    }

    @Override
    public double getImporte() {
        return montoCaptured;
    }

    @Override
    public double getPropina() {
        EditText editText = Objects.requireNonNull(propinaTil.getEditText());
        String s = Strings.nullToEmpty(editText.getText().toString());
        return !Strings.isNullOrEmpty(s) ? Double.parseDouble(s) : 0.0;
    }

    @Override
    public String getConcepto() {
        EditText editText = Objects.requireNonNull(conceptoTil.getEditText());
        return Strings.nullToEmpty(editText.getText().toString());
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void showError(@NotNull @NonNull String msg) {
        activity.showError(msg);
    }

    @Override
    public void showSuccess(@NotNull @NonNull String msg) {
        activity.showSuccess(msg);
    }

    @Override
    public void onMontoCaptured(@NonNull CharSequence monto) {
        double total =
                presenter.calculateTotal(monto, Objects.requireNonNull(propinaTil.getEditText()).getText());
        setTotalValue(total);
    }

    @Override
    public void onPropinaCaptured(@NonNull CharSequence propina) {
        double total = presenter.calculateTotal(Objects.requireNonNull(importeView).getText(), propina);
        setTotalValue(total);
    }

    @Override
    public void setTotalValue(double value) {
        totalView.setText(presenter.formatAmount(value));
    }

    @Override
    public void clickContinuar() {
        clearErrors();
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        FragmentUtil.addFragmentHidingCurrent(activity.getFragmentManagerLazy(),
                R.id.frame_negocio_cobro, this,
                NegocioCobroUserCardCardFragment.get(getImporte(), getPropina(), getConcepto()));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isVisible()) ValidationUtils.onValidationFailed(activity, errors);
    }

    private void clearErrors() {
        if (isVisible()) ValidationUtils.clearViewErrors(importeView, propinaTil, conceptoTil);
    }
}
