package addcel.mobilecard.ui.negocio.mx.cobro.create.qr;

import android.graphics.Bitmap;

import addcel.mobilecard.data.net.scanpay.SPReceiptEntity;
import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity;
import addcel.mobilecard.data.net.scanpay.model.SPQrEntity;
import addcel.mobilecard.ui.ScreenView;

/**
 * ADDCEL on 07/09/18.
 */
public interface NegocioCobroCreateQrContract {
    interface View extends ScreenView {

        void onComisionReceived(SPComisionEntity comision);

        void setQrCode(Bitmap bitmap, SPComisionEntity comision);

        void setId(int id);

        void clickConfirm();

        void onConfirmId(SPReceiptEntity result);
    }

    interface Presenter {

        void clearCompositeDisposable();

        void checkComision();

        SPQrEntity buildEntity(SPComisionEntity comision);

        boolean isIdSet();

        void createQrCode(int sideLen, int fgColor, int bgColor, SPComisionEntity comision);

        Bitmap getBitmapOfRootView(android.view.View view);

        String buildScreenShotName();

        void createImage(Bitmap bm);

        // TODO checkComision();

        void getId(SPComisionEntity comision);

        void validateId();
    }
}
