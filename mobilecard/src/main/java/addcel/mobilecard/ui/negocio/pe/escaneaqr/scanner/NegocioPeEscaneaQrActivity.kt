package addcel.mobilecard.ui.negocio.pe.escaneaqr.scanner

import addcel.mobilecard.R
import addcel.mobilecard.data.net.scanpay.SPReceiptEntity
import addcel.mobilecard.utils.ErrorUtil
import addcel.mobilecard.utils.JsonUtil
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonSyntaxException
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_negocio_registro_pe_qr.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import timber.log.Timber

class NegocioPeEscaneaQrActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    override fun handleResult(p0: Result?) {
        if (p0 != null) {
            val code = p0.text
            if (code.isNullOrEmpty()) {
                Toast.makeText(this, ErrorUtil.getErrorMsg(ErrorUtil.OPERATION), Toast.LENGTH_SHORT)
                        .show()
                scanner.resumeCameraPreview(this)
            } else {
                try {
                    val resultEntity = JsonUtil.fromJson(code, SPReceiptEntity::class.java)
                    if (resultEntity.code == 0) {
                        Timber.d("Scan result: %s", code)
                        val intent = Intent().putExtra("result", code)
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    } else {
                        Toast.makeText(this, resultEntity.message, Toast.LENGTH_SHORT).show()
                        scanner.resumeCameraPreview(this)
                    }
                } catch (e: JsonSyntaxException) {
                    Toast.makeText(
                            this,
                            ErrorUtil.getErrorMsg(ErrorUtil.OPERATION),
                            Toast.LENGTH_SHORT
                    )
                            .show()
                    scanner.resumeCameraPreview(this)
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    Toast.makeText(
                            this,
                            ErrorUtil.getErrorMsg(ErrorUtil.OPERATION),
                            Toast.LENGTH_SHORT
                    )
                            .show()
                    scanner.resumeCameraPreview(this)
                    e.printStackTrace()
                }
            }
        } else {
            Toast.makeText(this, ErrorUtil.getErrorMsg(ErrorUtil.OPERATION), Toast.LENGTH_SHORT)
                    .show()
            scanner.resumeCameraPreview(this)
        }
    }

    companion object {
        const val REQUEST_CODE = 407

        fun get(context: Context): Intent {
            return Intent(context, NegocioPeEscaneaQrActivity::class.java)
        }
    }

    private lateinit var scanner: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_negocio_registro_pe_qr)
        scanner = scanner_pe_negocio_registro
    }

    override fun onResume() {
        super.onResume()
        scanner.setResultHandler(this)
        scanner.startCamera()
    }

    public override fun onPause() {
        super.onPause()
        scanner.stopCamera()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, Intent())
        finish()
    }
}
