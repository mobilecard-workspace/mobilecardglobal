package addcel.mobilecard.ui.negocio.mx.historial;

import addcel.mobilecard.di.scope.PerActivity;
import dagger.Subcomponent;

/**
 * ADDCEL on 11/14/18.
 */
@PerActivity
@Subcomponent(modules = NegocioHistorialModule.class)
public interface NegocioHistorialSubcomponent {
    void inject(NegocioHistorialActivity activity);
}
