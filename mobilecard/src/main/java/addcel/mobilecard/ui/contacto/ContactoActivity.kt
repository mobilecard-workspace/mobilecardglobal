package addcel.mobilecard.ui.contacto

import addcel.mobilecard.McConstants
import addcel.mobilecard.R
import addcel.mobilecard.ui.custom.extension.ItemClickSupport
import addcel.mobilecard.ui.usuario.main.contacto.ContactoAdapter
import addcel.mobilecard.utils.AndroidUtils
import addcel.mobilecard.utils.MapUtils
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.dmoral.toasty.Toasty
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_contacto.*
import java.util.*

@Parcelize
data class ContactoModel(val idUsuario: Long, val email: String, val idPais: Int) :
        Parcelable

interface ContactoView {
    fun setUI()

    fun configRecyclerView(view: RecyclerView)

    fun setClicks()

    fun clickWsp()

    fun clickFb()

    fun clickTw()

    fun clickLanzaAccion(pos: Int)

    fun llamanos()

    fun escribenos()

    fun llamame()

    fun launchWhatsapp()

    fun lanzaMobilecard()

    fun lanzaFacebook()

    fun lanzaTwitter()

    fun lanzaGPlus()

    fun lanzaYoutube()

    fun lanzaLinkedIn()

    fun launchIntent(intent: Intent)

    fun lanzaAccion(pos: Int)

    fun buildEmailSubject(): String
}

class ContactoActivity : AppCompatActivity(), ContactoView {

    override fun setUI() {
        configRecyclerView(recycler_contacto)
        setClicks()
    }

    override fun configRecyclerView(view: RecyclerView) {
        view.adapter = ContactoAdapter()
        view.layoutManager = LinearLayoutManager(view.context)
        ItemClickSupport.addTo(view)
                .setOnItemClickListener { _, position, _ -> clickLanzaAccion(position) }
    }

    override fun setClicks() {
        // b_contacto_wsp.setOnClickListener { clickWsp() }
        b_contacto_fb.setOnClickListener { clickFb() }
        b_contacto_tw.setOnClickListener { clickTw() }
    }

    override fun clickWsp() {
        launchWhatsapp()
    }

    override fun clickFb() {
        lanzaFacebook()
    }

    override fun clickTw() {
        lanzaTwitter()
    }

    override fun clickLanzaAccion(pos: Int) {
        lanzaAccion(pos)
    }

    override fun llamanos() {
        val tel = getPhoneAfterCountry(model.idPais)
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$tel"))
        launchIntent(intent)
    }

    override fun escribenos() {

        val destinatario = arrayOf(getEmailAfterCountry(model.idPais))
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, destinatario)
        i.putExtra(Intent.EXTRA_SUBJECT, buildEmailSubject())
        i.putExtra(Intent.EXTRA_TEXT, getString(R.string.txt_contacto_escribenos_email_footer))
        launchIntent(i)
    }

    override fun llamame() {
    }

    override fun launchWhatsapp() {
        if (AndroidUtils.appInstalledOrNot(this, McConstants.WHATSAPP_PACKAGE)) {
            val link = when (model.idPais) {
                McConstants.PAIS_ID_MX -> McConstants.WHATSAPP_MX
                McConstants.PAIS_ID_CO -> McConstants.WHATSAPP_CO
                McConstants.PAIS_ID_USA -> McConstants.WHATSAPP_USA
                McConstants.PAIS_ID_PE -> McConstants.WHATSAPP_PE
                else -> McConstants.WHATSAPP_MX
            }

            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
            launchIntent(browserIntent)
        } else {
            Toasty.error(this, "Instala WhatsApp en tu dispositivo para comunicarte con nosotros").show()
        }
    }

    override fun lanzaMobilecard() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(WEB))
        launchIntent(browserIntent)
    }

    override fun lanzaFacebook() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK))
        launchIntent(browserIntent)
    }

    override fun lanzaTwitter() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(TWITTER))
        launchIntent(browserIntent)
    }

    override fun lanzaGPlus() {
    }

    override fun lanzaYoutube() {
    }

    override fun lanzaLinkedIn() {
    }

    override fun launchIntent(intent: Intent) {
        startActivity(intent)
    }

    override fun lanzaAccion(pos: Int) {
        when (pos) {
            0 -> llamanos()
            1 -> escribenos()
            2 -> lanzaMobilecard()
            3 -> launchWhatsapp()
            4 -> lanzaFacebook()
            5 -> lanzaTwitter()
            6 -> lanzaYoutube()
            7 -> lanzaLinkedIn()
            else -> {
            }
        }
    }

    override fun buildEmailSubject(): String {
        return String.format(
                Locale.getDefault(), "%s: %s - %s", "ANDROID", model.email,
                model.idUsuario
        )
    }

    companion object {

        private const val TEL_MX = "018009255001"
        private const val TEL_CO = "+5715800822"
        //private const val TEL_CO = "018005184868"
        private const val TEL_US = "+12134230411"
        private const val TEL_PE = "080080102"

        private const val EMAIL_MX = "soporte@mobilecardmx.com"
        private const val EMAIL_CO = "soporte@mobilecardco.com"
        private const val EMAIL_US = "support@mobilecardusa.com"
        private const val EMAIL_PE = "soporte@mobilecardpe.com"

        private const val TWITTER = "https://www.twitter.com/mobilecardmx"
        private const val FACEBOOK = "https://www.facebook.com/mobilecardmx/"
        private const val WEB = "http://www.mobilecardcorp.com/"

        private val MAP_PHONE = mapOf(1 to TEL_MX, 2 to TEL_CO, 3 to TEL_US, 4 to TEL_PE)
        private val MAP_EMAIL =
                mapOf(1 to EMAIL_MX, 2 to EMAIL_CO, 3 to EMAIL_US, 4 to EMAIL_PE)

        fun get(context: Context, model: ContactoModel): Intent {
            return Intent(context, ContactoActivity::class.java).putExtra("model", model)
        }

        fun getPhoneAfterCountry(idPais: Int): String {
            return MapUtils.getOrDefault(MAP_PHONE, idPais, TEL_MX)
        }

        fun getEmailAfterCountry(idPais: Int): String {
            return MapUtils.getOrDefault(MAP_EMAIL, idPais, EMAIL_MX)
        }
    }

    lateinit var model: ContactoModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacto)


        setSupportActionBar(toolbar_contacto)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        model = intent?.getParcelableExtra("model")!!
        setUI()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
