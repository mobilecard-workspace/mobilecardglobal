package addcel.mobilecard.ui.cvv

import addcel.mobilecard.R
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Parcelable
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_mobilecard_cvv_timer.*
import mx.mobilecard.crypto.AddcelCrypto
import java.util.concurrent.TimeUnit

@Parcelize
data class MobilecardCvvTimerModel(val code: String) : Parcelable

class MobilecardCvvTimerActivity : AppCompatActivity() {

    companion object {
        fun get(context: Context, model: MobilecardCvvTimerModel): Intent {
            return Intent(context, MobilecardCvvTimerActivity::class.java).putExtra("model", model)
        }
    }

    lateinit var model: MobilecardCvvTimerModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobilecard_cvv_timer)

        model = intent?.getParcelableExtra("model")!!

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        val cvv = AddcelCrypto.decryptHard(model.code)

        b_cvv_cerrar.setOnClickListener { finishAfterMillis(250) }

        if (cvv.isNotBlank()) {
            cvv_text.text = cvv

            object : CountDownTimer(TimeUnit.SECONDS.toMillis(10), 1000) {

                override fun onTick(millisUntilFinished: Long) {

                    val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                    val str = if (seconds < 10) "0$seconds" else seconds.toString()
                    cvv_timer_text.text = str
                }

                override fun onFinish() {
                    cvv_timer_text.text = "00"
                    finishAfterMillis(500)
                }
            }.start()
        } else {
            AlertDialog.Builder(this).setTitle("Información")
                    .setMessage("No se ha podido mostrar el CVV de la tarjeta MobileCard.\nComúnicate a soporte para cualquier aclaración.")
                    .setPositiveButton("Aceptar") { _, _ -> this@MobilecardCvvTimerActivity.finish() }
                    .setOnCancelListener {
                        this@MobilecardCvvTimerActivity.finish()
                    }
                    .show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun finishAfterMillis(wait: Long) {
        Handler().postDelayed({ this@MobilecardCvvTimerActivity.finish() }, wait)
    }
}
