package addcel.mobilecard.validation;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.ValidationError;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * ADDCEL on 11/27/18.
 */
public final class ValidationUtils {
    private ValidationUtils() {
    }

    public static void onValidationFailed(Context context, List<ValidationError> errors) {
        String error;
        View view;
        for (int i = 0, size = errors.size(); i < size; i++) {
            ValidationError e = errors.get(i);
            view = e.getView();
            error = e.getCollatedErrorMessage(view.getContext());
            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setError(error);
            } else if (view instanceof EditText) {
                ((EditText) view).setError(error);
            } else {
                Toasty.error(context, error).show();
            }
        }
    }

    public static void clearViewErrors(View... views) {
        for (View v : views) {
            if (v instanceof TextInputLayout) ((TextInputLayout) v).setError(null);
            if (v instanceof EditText) {
                ((EditText) v).setError(null);
                v.clearFocus();
            }
            if (v instanceof TextView) {
                ((TextView) v).setError(null);
                v.clearFocus();
            }
        }
    }
}
