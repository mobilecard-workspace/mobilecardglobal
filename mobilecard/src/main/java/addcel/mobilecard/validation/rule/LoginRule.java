package addcel.mobilecard.validation.rule;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.lang.annotation.Annotation;

import addcel.mobilecard.validation.annotation.Login;
import commons.validator.routines.EmailValidator;

/**
 * ADDCEL on 14/07/17.
 */

public final class LoginRule extends AnnotationRule<Login, String> {

    private static final String LOGIN_PATTERN = "[a-zA-Z0-9 \\-#.()/%&]{4,16}";

    /**
     * Constructor. It is mandatory that all subclasses MUST have a constructor with the same
     * signature.
     *
     * @param login The rule {@link Annotation} instance to which
     *              this rule is paired.
     */
    protected LoginRule(Login login) {
        super(login);
    }

    @Override
    public boolean isValid(String s) {
        return s.matches(LOGIN_PATTERN) || EmailValidator.getInstance().isValid(s);
    }
}
