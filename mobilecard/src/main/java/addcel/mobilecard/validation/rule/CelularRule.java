package addcel.mobilecard.validation.rule;

import android.text.TextUtils;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.lang.annotation.Annotation;

import addcel.mobilecard.validation.annotation.Celular;

/**
 * ADDCEL on 09/09/16.
 */
public final class CelularRule extends AnnotationRule<Celular, String> {

    /**
     * Constructor. It is mandatory that all subclasses MUST have a constructor with the same
     * signature.
     *
     * @param celular The rule {@link Annotation} instance to which
     *                this rule is paired.
     */
    protected CelularRule(Celular celular) {
        super(celular);
    }

    @Override
    public boolean isValid(String s) {
        return !TextUtils.isEmpty(s)
                && TextUtils.isDigitsOnly(s)
                && s.length() >= 6
                && s.length() <= 12;
    }
}
