package addcel.mobilecard.validation.rule;

import android.text.TextUtils;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.lang.annotation.Annotation;

import addcel.mobilecard.validation.annotation.PeajeTag;

/**
 * ADDCEL on 14/12/16.
 */

public class PeajeTagRule extends AnnotationRule<PeajeTag, String> {
    /**
     * Constructor. It is mandatory that all subclasses MUST have a constructor with the same
     * signature.
     *
     * @param peajeTag The rule {@link Annotation} instance to which
     *                 this rule is paired.
     */
    protected PeajeTagRule(PeajeTag peajeTag) {
        super(peajeTag);
    }

    @Override
    public boolean isValid(String s) {
        if (TextUtils.isDigitsOnly(s) && s.length() == 11) {
            return s.indexOf("005") == 0 || s.indexOf("006") == 0;
        } else {
            return TextUtils.isDigitsOnly(s) && s.length() == 8;
        }
    }
}
