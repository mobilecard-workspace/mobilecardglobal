package addcel.mobilecard.validation.annotation;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import addcel.mobilecard.validation.rule.CelularRule;

/**
 * ADDCEL on 09/09/16.
 */
@ValidateUsing(CelularRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Celular {
    int messageResId() default -1;                     // Mandatory attribute

    String message() default "Número de teléfono no válido";   // Mandatory attribute

    int sequence() default -1;
}
