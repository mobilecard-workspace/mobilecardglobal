package addcel.mobilecard.validation.annotation;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import addcel.mobilecard.R;
import addcel.mobilecard.validation.rule.LoginRule;

/**
 * ADDCEL on 14/07/17.
 */
@ValidateUsing(LoginRule.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface Login {
    int messageResId() default R.string.error_username;                     // Mandatory attribute

    String message() default "Nombre de usuario o email no válido";   // Mandatory attribute

    int sequence() default -1;
}
