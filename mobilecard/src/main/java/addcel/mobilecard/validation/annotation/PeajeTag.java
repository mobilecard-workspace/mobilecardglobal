package addcel.mobilecard.validation.annotation;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import addcel.mobilecard.validation.rule.PeajeTagRule;

/**
 * ADDCEL on 14/12/16.
 */

@ValidateUsing(PeajeTagRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PeajeTag {
    int messageResId() default -1;                     // Mandatory attribute

    String message() default "Tag de telepeaje no válido";   // Mandatory attribute

    int sequence() default -1;
}
