package addcel.mobilecard;

/**
 * ADDCEL on 17/05/17.
 */

public interface UIEventView {
    void showProgress();

    void hideProgress();

    void onError(String msg);
}
