package addcel.mobilecard;

import android.os.Bundle;

/**
 * ADDCEL on 24/11/16.
 */

public interface MobilecardBaseView {
    void showError(int resId, boolean destroy);

    void showError(String charSequence, boolean destroy);

    void showSuccess(String charSequence);

    void startProgress(int StringId);

    void stopProgress();

    void startActivity(Class<?> cl, Bundle bundle);

    void startActivity(Class<?> cl);
}
