package addcel.mobilecard.data.interfaces;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * ADDCEL on 1/11/19.
 */
public interface Mappable {

    Map<String, String> asMap(@NotNull Map<String, String> map);
}
