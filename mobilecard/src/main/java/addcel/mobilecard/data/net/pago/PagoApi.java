package addcel.mobilecard.data.net.pago;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PagoApi {

    String TRANSACTO_PATH = "Transacto/{idApp}/{idioma}/{accountId}/pagoProsa3DS";
    String HTH_PATH = "";

    @FormUrlEncoded
    @POST(TRANSACTO_PATH)
    Observable<String> loadInitialModelFromJson(
            @Header("Authorization") String auth, @Path("idApp") int idApp, @Path("idioma") String idioma,
            @Path("accountId") String accountId, @Field("json") String json);

    //concept=%s&idUser=%d&idCard=%d&accountId=%d&amount=%f&comision=%f&idioma=%s&lat=%f&lon=%f
    @FormUrlEncoded
    @POST()
    Observable<String> loadInitialModelFromForm(
            @Header("Authorization") String auth, @Path("initialUrl") String initialUrl,
            @Field("concept") String concept, @Field("idUser") long idUser, @Field("idCard") int idCard,
            @Field("accountId") int accountId, @Field("amount") double amount,
            @Field("comision") double comision, @Field("idioma") String idioma, @Field("lat") double lat,
            @Field("lon") double lon);
}
