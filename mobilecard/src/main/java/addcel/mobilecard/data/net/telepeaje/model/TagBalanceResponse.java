package addcel.mobilecard.data.net.telepeaje.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TagBalanceResponse {

    @SerializedName("balance")
    private double Balance;

    @SerializedName("idError")
    private long IdError;

    @SerializedName("mensajeError")
    private String MensajeError;

    public double getBalance() {
        return Balance;
    }

    public void setBalance(double balance) {
        Balance = balance;
    }

    public long getIdError() {
        return IdError;
    }

    public void setIdError(long idError) {
        IdError = idError;
    }

    public String getMensajeError() {
        return MensajeError;
    }

    public void setMensajeError(String mensajeError) {
        MensajeError = mensajeError;
    }
}
