package addcel.mobilecard.data.net.colombia;

import java.util.List;

import addcel.mobilecard.data.net.colombia.entity.ColombiaBonoInfoEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaPagoRequestEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaPagoResponseEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaRecargaCarrierEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaRequestEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaServicioConsultaRequestEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaServicioConsultaResponseEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaTiendaEntity;
import addcel.mobilecard.data.net.colombia.entity.ColombiaTokenEntity;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ColombiaApi {
    String PATH = "Colombia/{idApp}/{idPais}/{idioma}/";

    @POST(PATH + "getDatosServicios")
    Observable<ColombiaTiendaEntity> loadTiendaData(
            @Header("Authorization") String auth, @Path("idApp") int idApp, @Path("idPais") int idPais,
            @Path("idioma") String idioma, @Body ColombiaRequestEntity request);

    @POST(PATH + "getDatosServicios")
    Observable<List<ColombiaRecargaCarrierEntity>> loadCarriers(
            @Header("Authorization") String auth, @Path("idApp") int idApp, @Path("idPais") int idPais,
            @Path("idioma") String idioma, @Body ColombiaRequestEntity request);

    @POST(PATH + "getDatosServicios")
    Observable<ColombiaBonoInfoEntity> loadBonos(
            @Header("Authorization") String auth, @Path("idApp") int idApp, @Path("idPais") int idPais,
            @Path("idioma") String idioma, @Body ColombiaRequestEntity request);

    @POST(PATH + "getToken")
    Observable<ColombiaTokenEntity> getToken(
            @Header("Authorization") String auth, @Path("idApp") int idApp, @Path("idPais") int idPais,
            @Path("idioma") String idioma, @Body ColombiaRequestEntity request);

    @POST(PATH + "enviarPago")
    Observable<ColombiaPagoResponseEntity> enviarPago(
            @Header("Authorization") String auth, @Path("idApp") int idApp, @Path("idPais") int idPais,
            @Path("idioma") String idioma, @Body ColombiaPagoRequestEntity request);

    @POST(PATH + "consultaReferencia")
    Observable<ColombiaServicioConsultaResponseEntity> consultaReferencia(
            @Header("Authorization") String auth, @Path("idApp") int idApp, @Path("idPais") int idPais,
            @Path("idioma") String idioma, @Body ColombiaServicioConsultaRequestEntity request);
}
