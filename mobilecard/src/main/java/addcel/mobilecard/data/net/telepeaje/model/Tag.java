package addcel.mobilecard.data.net.telepeaje.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Tag implements Parcelable {
    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };

    @SerializedName("dv")
    private int mDv;

    @SerializedName("etiqueta")
    private String mEtiqueta;

    @SerializedName("tag")
    private String mTag;

    @SerializedName("tiporecargatag")
    private int mTiporecargatag;

    public Tag(int mDv, String mEtiqueta, String mTag, int mTiporecargatag) {
        this.mDv = mDv;
        this.mEtiqueta = mEtiqueta;
        this.mTag = mTag;
        this.mTiporecargatag = mTiporecargatag;
    }

    protected Tag(Parcel in) {
        mDv = in.readInt();
        mEtiqueta = in.readString();
        mTag = in.readString();
        mTiporecargatag = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mDv);
        dest.writeString(mEtiqueta);
        dest.writeString(mTag);
        dest.writeInt(mTiporecargatag);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getDv() {
        return mDv;
    }

    public void setDv(int dv) {
        mDv = dv;
    }

    public String getEtiqueta() {
        return mEtiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        mEtiqueta = etiqueta;
    }

    public String getTag() {
        return mTag;
    }

    public void setTag(String tag) {
        mTag = tag;
    }

    public int getTiporecargatag() {
        return mTiporecargatag;
    }

    public void setTiporecargatag(int tiporecargatag) {
        mTiporecargatag = tiporecargatag;
    }
}
