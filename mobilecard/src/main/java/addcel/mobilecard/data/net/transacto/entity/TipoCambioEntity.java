package addcel.mobilecard.data.net.transacto.entity;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TipoCambioEntity {

    @SerializedName("valorDolar")
    private Double ValorDolar;

    public Double getValorDolar() {
        return ValorDolar;
    }

    public void setValorDolar(Double valorDolar) {
        ValorDolar = valorDolar;
    }
}
