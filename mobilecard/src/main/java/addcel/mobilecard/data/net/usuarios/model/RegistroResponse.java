package addcel.mobilecard.data.net.usuarios.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 08/03/17.
 */
public final class RegistroResponse extends McResponse implements Parcelable {

    public static final Creator<RegistroResponse> CREATOR = new Creator<RegistroResponse>() {
        @Override
        public RegistroResponse createFromParcel(Parcel in) {
            return new RegistroResponse(in);
        }

        @Override
        public RegistroResponse[] newArray(int size) {
            return new RegistroResponse[size];
        }
    };

    @SerializedName("ideUsuario")
    private long idUsuario;

    private int idPais;
    private String debugPass;

    public RegistroResponse() {
    }

    public RegistroResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    private RegistroResponse(Parcel in) {
        super(in);
        idUsuario = in.readLong();
        idPais = in.readInt();
        debugPass = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(idUsuario);
        dest.writeInt(idPais);
        dest.writeString(debugPass);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public int getIdPais() {
        return idPais;
    }

    public String getDebugPassword() {
        return debugPass;
    }
}
