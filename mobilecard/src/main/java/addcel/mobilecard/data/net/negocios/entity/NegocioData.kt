package addcel.mobilecard.data.net.negocios.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import javax.inject.Named

/**
 * ADDCEL on 2019-12-05.
 */


@Parcelize
data class NegocioCreateRequest(
        @Named("celular")
        val celular: String,
        @Named("nombre_establecimiento")
        val nombreEstablecimiento: String,
        @Named("email")
        val email: String,
        @Named("password")
        val password: String,
        @Named("direccion")
        val direccion: String = "",
        @Named("colonia")
        val colonia: String = "",
        @Named("ciudad")
        val ciudad: String = "",
        @Named("idEstado")
        val idEstado: Int = 0,
        @Named("cp")
        val cp: String = "",
        @Named("nombre")
        val nombre: String,
        @Named("apellidoPaterno")
        val paterno: String,
        @Named("apellidoMaterno")
        val materno: String,
        @Named("clabe")
        val clabe: String,
        @Named("idbanco")
        val idBanco: Int,
        @Named("curp")
        val curp: String = "",
        @Named("rfc")
        val rfc: String = ""
) : Parcelable