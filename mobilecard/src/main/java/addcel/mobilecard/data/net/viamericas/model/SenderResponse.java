package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

@SuppressWarnings("unused")
public class SenderResponse extends McResponse implements Parcelable {

    public static final Creator<SenderResponse> CREATOR = new Creator<SenderResponse>() {
        @Override
        public SenderResponse createFromParcel(Parcel in) {
            return new SenderResponse(in);
        }

        @Override
        public SenderResponse[] newArray(int size) {
            return new SenderResponse[size];
        }
    };
    @SerializedName("address")
    private String mAddress;
    @SerializedName("FName")
    private String mFName;
    @SerializedName("idCity")
    private String mIdCity;
    @SerializedName("idCountry")
    private String mIdCountry;
    @SerializedName("idSender")
    private String mIdSender;
    @SerializedName("idState")
    private String mIdState;
    @SerializedName("LName")
    private String mLName;
    @SerializedName("phone1")
    private String mPhone1;
    @SerializedName("ZIP")
    private String mZIP;

    public SenderResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    protected SenderResponse(Parcel in) {
        super(in);
        mAddress = in.readString();
        mFName = in.readString();
        mIdCity = in.readString();
        mIdCountry = in.readString();
        mIdSender = in.readString();
        mIdState = in.readString();
        mLName = in.readString();
        mPhone1 = in.readString();
        mZIP = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mAddress);
        dest.writeString(mFName);
        dest.writeString(mIdCity);
        dest.writeString(mIdCountry);
        dest.writeString(mIdSender);
        dest.writeString(mIdState);
        dest.writeString(mLName);
        dest.writeString(mPhone1);
        dest.writeString(mZIP);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getFName() {
        return mFName;
    }

    public void setFName(String FName) {
        mFName = FName;
    }

    public String getIdCity() {
        return mIdCity;
    }

    public void setIdCity(String idCity) {
        mIdCity = idCity;
    }

    public String getIdCountry() {
        return mIdCountry;
    }

    public void setIdCountry(String idCountry) {
        mIdCountry = idCountry;
    }

    public String getIdSender() {
        return mIdSender;
    }

    public void setIdSender(String idSender) {
        mIdSender = idSender;
    }

    public String getIdState() {
        return mIdState;
    }

    public void setIdState(String idState) {
        mIdState = idState;
    }

    public String getLName() {
        return mLName;
    }

    public void setLName(String LName) {
        mLName = LName;
    }

    public String getPhone1() {
        return mPhone1;
    }

    public void setPhone1(String phone1) {
        mPhone1 = phone1;
    }

    public String getZIP() {
        return mZIP;
    }

    public void setZIP(String ZIP) {
        mZIP = ZIP;
    }
}
