package addcel.mobilecard.data.net.negocios

import addcel.mobilecard.data.net.negocios.entity.*
import addcel.mobilecard.data.net.usuarios.model.LoginRequest
import addcel.mobilecard.data.net.usuarios.model.PushResponse
import addcel.mobilecard.data.net.usuarios.model.PushTokenRequest
import addcel.mobilecard.data.net.usuarios.model.TerminosResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.*

/** ADDCEL on 06/09/18.  */
interface NegocioAPI {

    @GET("Usuarios/{idApp}/{idPais}/{idioma}/bankCodes")
    fun getBanks(
            @Path("idApp") idApp: Int,
            @Path("idPais") idPais: Int, @Path("idioma") idioma: String
    ): Observable<BankResponse>

    @POST(API + "user/insertv4")
    fun create(
            @Path("idApp") idApp: Int, @Path("idioma") idioma: String,
            @Body body: NegocioAltaRequest
    ): Observable<NegocioEntity>

    @POST("Usuarios/{idApp}/{idPais}/{idioma}/comercio/alta")
    fun create(
            @Path("idApp") idApp: Int, @Path("idPais") idPais: Int,
            @Path("idioma") idioma: String, @Body request: NegocioCreateRequest
    ): Observable<NegocioEntity>

    @POST("Usuarios/{idApp}/{idPais}/{idioma}/user/login")
    fun login(
            @Path("idApp") idApp: Int,
            @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Body
            json: LoginRequest
    ): Observable<NegocioEntity>

    @FormUrlEncoded
    @POST(API + "commerce/passUpdate")
    fun updatePasswordAdmin(
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("userAdmin") id: Long, @Field("newPass")
            newPass: String
    ): Observable<DefaultResponse>

    @FormUrlEncoded
    @POST(API + "commerce/passReset")
    fun resetPasswordAdmin(
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("userAdmin")
            userAdmin: String
    ): Observable<DefaultResponse>

    @GET("Usuarios/{idApp}/terminos/{idPais}/{idioma}")
    fun getTerminos(
            @Path("idApp") idApp: Int,
            @Header("client") client: String, @Path("idPais") idPais: Int, @Path("idioma")
            idioma: String
    ): Observable<TerminosResponse>

    @GET("Usuarios/{idApp}/privacidad/{idPais}/{idioma}")
    fun getAvisoPrivacidad(
            @Path("idApp")
            idApp: Int, @Header("client") client: String, @Path("idPais") idPais: Int, @Path("idioma")
            idioma: String
    ): Observable<TerminosResponse>

    @GET("Usuarios/{idApp}/{method}/{idPais}/{idioma}")
    fun getLegalInfo(
            @Path("idApp") idApp: Int,
            @Path("method") method: String, @Header("client") client: String, @Path("idPais") idPais: Int,
            @Path("idioma") idioma: String
    ): Observable<TerminosResponse>

    @FormUrlEncoded
    @POST("Usuarios/{idApp}/{idioma}/commerce/verify")
    fun verify(
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("id") id: Long
    ): Observable<ValidationEntity>

    @FormUrlEncoded
    @POST(API + "commerce/email/resend")
    fun resendActivationLink(
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("id") id: Long
    ): Observable<DefaultResponse>


    @FormUrlEncoded
    @POST("Usuarios/{idApp}/{idPais}/{idioma}/commerce/update/reference")
    fun updateReference(
            @Path("idApp") idApp: Int, @Path("idPais") idPais: Int, @Path("idioma")
            idioma: String, @Field("login") login: String, @Field("scanReference")
            scanReference: String
    ): Observable<DefaultResponse>

    @POST("PushNotifications/saveToken")
    fun saveToken(@Header("authorization") authorization: String, @Body request: PushTokenRequest): Observable<PushResponse>

    companion object {

        const val API = "Usuarios/{idApp}/{idioma}/"
        const val METHOD_TERMS = "terminos"
        const val METHOD_PRIVACY = "privacidad"
        const val PUSH_AUTH = "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"

        fun create(r: Retrofit): NegocioAPI {
            return r.create(NegocioAPI::class.java)
        }
    }
}
