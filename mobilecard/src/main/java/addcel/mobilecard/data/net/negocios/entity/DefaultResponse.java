package addcel.mobilecard.data.net.negocios.entity;

import org.jetbrains.annotations.NotNull;

/**
 * ADDCEL on 06/09/18.
 */
public final class DefaultResponse {
    private int idError;
    private String mensajeError;

    public DefaultResponse() {
    }

    public DefaultResponse(int idError, String mensajeError) {
        this.idError = idError;
        this.mensajeError = mensajeError;
    }

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    @NotNull
    @Override
    public String toString() {
        return mensajeError;
    }
}
