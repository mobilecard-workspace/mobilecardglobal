package addcel.mobilecard.data.net.hth

import android.os.Parcelable
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 2019-05-13.
 */


data class AccountRequest(
        val actNumber: String, val actType: String, val alias: String,
        val bankCode: String, val contact: String, val email: String, val holderName: String,
        val idUsuario: Long, val idioma: String, val phone: String, val rfc: String
)

@Parcelize
data class AccountEntity(
        val alias: String, val id: Long, val comision_fija: Double,
        val comision_porcentaje: Double, val telefono: String, val correo: String
) : Parcelable {
    override fun toString(): String {
        return alias
    }
}

data class AccountResponse(
        val idError: Int, val mensajeError: String,
        val accounts: List<AccountEntity>
)

data class AccountUpdateRequest(
        val idUsuario: Long, val idAccount: Long, val telefono: String,
        val alias: String, val email: String, val idioma: String
)

data class BankEntity(
        val clave: String, val id: Long, val nombre_corto: String,
        val nombre_razon_social: String, val comision_fija: Double, val comision_porcentaje: Double
) {
    override fun toString(): String {
        return nombre_corto
    }
}

data class BankResponse(val idError: Int, val mensajeError: String, val banks: List<BankEntity>)

/*
public long idUser;
public long idCard;
public long accountId;
public double amount;
public String concept;
public double comision;
public int idProvider;
public int idProduct;
public String imei;
public String software;
public String model;
public double lat;
public double lon;
public String referencia;
public String emisor;
 */
data class PaymentEntity(
        val idUser: Long,
        val idCard: Long,
        val accountId: Long,
        val amount: Double,
        val concept: String = CONCEPTO,
        val comision: Double,
        val idProvider: Int = ID_PROVIDER,
        val idProduct: Int = ID_PROVIDER,
        val imei: String,
        val software: String,
        val model: String,
        val lat: Double,
        val lon: Double,
        val referencia: String,
        val emisor: String
) {
    companion object {
        const val ID_PROVIDER = 48
        const val CONCEPTO = "Transferencia electronica"
    }
}

@Parcelize
data class ReceiptEntity(
        val amount: Double, val authNumber: String, val code: Int,
        val dateTime: Long, val idTransaccion: Int, val maskedPAN: String, val message: String,
        val opId: String, val status: Int, val ticketUrl: String, val txnISOCode: String
) : Parcelable

interface H2HApi {
    companion object {
        const val PROCESS_FINISHED = "/payworks/payworks2RecRespuesta"

        fun get(retrofit: Retrofit): H2HApi {
            return retrofit.create(H2HApi::class.java)
        }
    }

    @FormUrlEncoded
    @POST("H2HPayment/{idApp}/toAccount")
    fun toAccount(
            @Path("idApp") idApp: Int,
            @Field("idUsuario") idUsuario: Long, @Field("idioma")
            idioma: String
    ): Observable<AccountResponse>

    @FormUrlEncoded
    @POST("H2HPayment/{idApp}/deleteAccount")
    fun deleteAccount(
            @Path("idApp")
            idApp: Int, @Field("idUsuario") idUsuario: Long, @Field("idioma") idioma: String,
            @Field("idAccount") id: Int
    ): Observable<AccountResponse>

    @POST("H2HPayment/{idApp}/updateAccount")
    fun updateAccount(
            @Path("idApp") idApp: Int, @Body
            request: AccountUpdateRequest
    ): Observable<AccountResponse>

    @POST("H2HPayment/{idApp}/enqueueAccountSignUp")
    fun enqueueAccountSignUp(
            @Path("idApp")
            idApp: Int, @Body request: AccountRequest
    ): Observable<AccountResponse>

    @GET("H2HPayment/{idApp}/bankCodes")
    fun bankCodes(
            @Path("idApp")
            idApp: Int
    ): Observable<BankResponse>

    @POST("H2HPayment/{idApp}/{idPais}/{idioma}/{accountId}/pagoBP")
    fun enqueuePayment(
            @Header("Authorization") auth: String, @Path("idApp") idApp: Int, @Path("idPais") idPais: Int,
            @Path("idioma") idioma: String, @Path("accountId") accountId: String, @Body
            request: PaymentEntity
    ): Observable<ReceiptEntity>
}
