package addcel.mobilecard.data.net.qr

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * ADDCEL on 2019-07-15.
 */

data class QrRequest(
        @SerializedName("amount") val amount: Double, @SerializedName("comision")
        val comision: Double, @SerializedName("description") val description: String,
        @SerializedName("expirationDate") val expirationDate: String
)

data class QREntity(
        @SerializedName("code") val code: Int, @SerializedName("message")
        val message: String, @SerializedName("qrBase64") val qrBase64: String, @SerializedName("qrTag")
        val qrTag: String
)

data class QrHexaRequest(@SerializedName("qrHexa") val qrHexa: String)

@Parcelize
data class QrHexaEntity(
        @SerializedName("code") val code: Int, @SerializedName("message")
        val message: String, @SerializedName("countryCode") val countryCode: String, @SerializedName("city")
        val city: String, @SerializedName("businessName") val businessName: String,
        @SerializedName("currencyCode") val currencyCode: String, @SerializedName("amount")
        val amount: Double
) : Parcelable

interface VisaQrWalletAPI {
    companion object {
        const val PATH = "/VisaQRWalletService/{idApp}/{idPais}/{idioma}/"

        fun get(r: Retrofit): VisaQrWalletAPI {
            return r.create(VisaQrWalletAPI::class.java)
        }
    }

    @POST("$PATH{idUsuario}/{bussinessId}/payment/generate/qr")
    fun generateDynamicQr(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Path("idUsuario")
            id: Long, @Path("bussinessId") idNegocio: Int, @Body body: QrRequest
    ): Observable<QREntity>

    @POST("/VisaQRWalletService/qr/decode/hexadecimal")
    fun qrDecodeHexadecimal(
            @Body
            request: QrHexaRequest
    ): Observable<QrHexaEntity>
}