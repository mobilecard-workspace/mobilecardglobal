package addcel.mobilecard.data.net.colombia.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * ADDCEL on 10/17/18.
 */
public final class ColombiaPagoRequestEntity {

    /**
     * comision : 500 cvv2 : 123 cx : 0 cy : 0 idAplicacion : 1 idProducto : 1008 idRecarga : 85
     * idUsuario : 215 imei : 352689081588323 key : 1 login : carlosgs modelo : Pixel monto : 10000
     * password : 12345678 referencias : [{"len":10,"nombre":"Numero
     * Celular","tipo":"number","valor":"5518307722","variable":"numeroCelular"}] software : 28 token
     * : +A/bmwSjNm2ZVR/81Ywdx3QSTS/cO+7mj+1SK32I1Lg= valorImpuesto : 0.0
     */
    private final double comision;

    private final int idTarjeta;
    private final double cx;
    private final double cy;
    private final int idAplicacion;
    private final String idProducto;
    private final String idRecarga;
    private final long idUsuario;
    private final String imei;
    private final int key;
    private final String login;
    private final String modelo;
    private final double monto;
    private final String password;
    private final String software;
    private final String token;
    private final String valorImpuesto;
    private final List<ReferenciasEntity> referencias;

    private ColombiaPagoRequestEntity(Builder builder) {
        comision = builder.comision;
        idTarjeta = builder.idTarjeta;
        cx = builder.cx;
        cy = builder.cy;
        idAplicacion = builder.idAplicacion;
        idProducto = builder.idProducto;
        idRecarga = builder.idRecarga;
        idUsuario = builder.idUsuario;
        imei = builder.imei;
        key = builder.key;
        login = builder.login;
        modelo = builder.modelo;
        monto = builder.monto;
        password = builder.password;
        software = builder.software;
        token = builder.token;
        valorImpuesto = builder.valorImpuesto;
        referencias = builder.referencias;
    }

    private double getComision() {
        return comision;
    }

    private int getIdTarjeta() {
        return idTarjeta;
    }

    private double getCx() {
        return cx;
    }

    private double getCy() {
        return cy;
    }

    private int getIdAplicacion() {
        return idAplicacion;
    }

    private String getIdProducto() {
        return idProducto;
    }

    private String getIdRecarga() {
        return idRecarga;
    }

    private long getIdUsuario() {
        return idUsuario;
    }

    private String getImei() {
        return imei;
    }

    private int getKey() {
        return key;
    }

    private String getLogin() {
        return login;
    }

    private String getModelo() {
        return modelo;
    }

    private double getMonto() {
        return monto;
    }

    private String getPassword() {
        return password;
    }

    private String getSoftware() {
        return software;
    }

    private String getToken() {
        return token;
    }

    private String getValorImpuesto() {
        return valorImpuesto;
    }

    private List<ReferenciasEntity> getReferencias() {
        return referencias;
    }

    public static class ReferenciasEntity implements Parcelable {
        public static final Creator<ReferenciasEntity> CREATOR = new Creator<ReferenciasEntity>() {
            @Override
            public ReferenciasEntity createFromParcel(Parcel in) {
                return new ReferenciasEntity(in);
            }

            @Override
            public ReferenciasEntity[] newArray(int size) {
                return new ReferenciasEntity[size];
            }
        };
        /**
         * len : 10 nombre : Numero Celular tipo : number valor : 5518307722 variable : numeroCelular
         */
        private final int len;

        private final String nombre;
        private final String tipo;
        private final String valor;
        private final String variable;

        private ReferenciasEntity(Builder builder) {
            len = builder.len;
            nombre = builder.nombre;
            tipo = builder.tipo;
            valor = builder.valor;
            variable = builder.variable;
        }

        ReferenciasEntity(Parcel in) {
            len = in.readInt();
            nombre = in.readString();
            tipo = in.readString();
            valor = in.readString();
            variable = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(len);
            dest.writeString(nombre);
            dest.writeString(tipo);
            dest.writeString(valor);
            dest.writeString(variable);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        int getLen() {
            return len;
        }

        String getNombre() {
            return nombre;
        }

        String getTipo() {
            return tipo;
        }

        String getValor() {
            return valor;
        }

        String getVariable() {
            return variable;
        }

        public static final class Builder {
            private int len;
            private String nombre;
            private String tipo;
            private String valor;
            private String variable;

            public Builder() {
            }

            public Builder(ReferenciasEntity copy) {
                this.len = copy.getLen();
                this.nombre = copy.getNombre();
                this.tipo = copy.getTipo();
                this.valor = copy.getValor();
                this.variable = copy.getVariable();
            }

            public Builder setLen(int len) {
                this.len = len;
                return this;
            }

            public Builder setNombre(String nombre) {
                this.nombre = nombre;
                return this;
            }

            public Builder setTipo(String tipo) {
                this.tipo = tipo;
                return this;
            }

            public Builder setValor(String valor) {
                this.valor = valor;
                return this;
            }

            public Builder setVariable(String variable) {
                this.variable = variable;
                return this;
            }

            public ReferenciasEntity build() {
                return new ReferenciasEntity(this);
            }
        }
    }

    public static final class Builder {
        private double comision;
        private int idTarjeta;
        private double cx;
        private double cy;
        private int idAplicacion;
        private String idProducto;
        private String idRecarga;
        private long idUsuario;
        private String imei;
        private int key;
        private String login;
        private String modelo;
        private double monto;
        private String password;
        private String software;
        private String token;
        private String valorImpuesto;
        private List<ReferenciasEntity> referencias;

        public Builder() {
        }

        public Builder(ColombiaPagoRequestEntity copy) {
            this.comision = copy.getComision();
            this.idTarjeta = copy.getIdTarjeta();
            this.cx = copy.getCx();
            this.cy = copy.getCy();
            this.idAplicacion = copy.getIdAplicacion();
            this.idProducto = copy.getIdProducto();
            this.idRecarga = copy.getIdRecarga();
            this.idUsuario = copy.getIdUsuario();
            this.imei = copy.getImei();
            this.key = copy.getKey();
            this.login = copy.getLogin();
            this.modelo = copy.getModelo();
            this.monto = copy.getMonto();
            this.password = copy.getPassword();
            this.software = copy.getSoftware();
            this.token = copy.getToken();
            this.valorImpuesto = copy.getValorImpuesto();
            this.referencias = copy.getReferencias();
        }

        public Builder setComision(double comision) {
            this.comision = comision;
            return this;
        }

        public Builder setCvv2(int idTarjeta) {
            this.idTarjeta = idTarjeta;
            return this;
        }

        public Builder setCx(double cx) {
            this.cx = cx;
            return this;
        }

        public Builder setCy(double cy) {
            this.cy = cy;
            return this;
        }

        public Builder setIdAplicacion(int idAplicacion) {
            this.idAplicacion = idAplicacion;
            return this;
        }

        public Builder setIdProducto(String idProducto) {
            this.idProducto = idProducto;
            return this;
        }

        public Builder setIdRecarga(String idRecarga) {
            this.idRecarga = idRecarga;
            return this;
        }

        public Builder setIdUsuario(long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public Builder setImei(String imei) {
            this.imei = imei;
            return this;
        }

        public Builder setKey(int key) {
            this.key = key;
            return this;
        }

        public Builder setLogin(String login) {
            this.login = login;
            return this;
        }

        public Builder setModelo(String modelo) {
            this.modelo = modelo;
            return this;
        }

        public Builder setMonto(double monto) {
            this.monto = monto;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setSoftware(String software) {
            this.software = software;
            return this;
        }

        public Builder setToken(String token) {
            this.token = token;
            return this;
        }

        public Builder setValorImpuesto(String valorImpuesto) {
            this.valorImpuesto = valorImpuesto;
            return this;
        }

        public Builder setReferencias(List<ReferenciasEntity> referencias) {
            this.referencias = referencias;
            return this;
        }

        public ColombiaPagoRequestEntity build() {
            return new ColombiaPagoRequestEntity(this);
        }
    }
}
