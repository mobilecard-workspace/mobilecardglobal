package addcel.mobilecard.data.net.viamericas

import addcel.mobilecard.data.net.viamericas.model.*
import io.reactivex.Observable
import okhttp3.Credentials
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 25/03/17.
 */

interface ViamericasService {

    @FormUrlEncoded
    @POST(PATH + "get/costPaymentLocationNetwork")
    fun getLocations(
            @Header("Authorization") auth: String,
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("json")
            json: String
    ): Observable<PaymentLocationNetworkResponse>


    @FormUrlEncoded
    @POST(PATH + "get/countrys")
    fun getCountries(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<CountriesResponse>


    @FormUrlEncoded
    @POST(PATH + "get/ExchangeRate")
    fun getExchangeRate(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<ExchangeRateResponse>


    @FormUrlEncoded
    @POST(PATH + "get/countrysStates")
    fun getStates(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<StatesResponse>


    @FormUrlEncoded
    @POST(PATH + "get/Cities")
    fun getZipCode(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<VmZipResponse>

    ///get/orderFees

    @FormUrlEncoded
    @POST(PATH + "get/orderFees")
    fun getOrderFees(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<OrderFeeResponse>


    @FormUrlEncoded
    @POST(PATH + "get/statesCitys")
    fun getCitiesByState(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<CitiesResponse>


    @FormUrlEncoded
    @POST(PATH + "get/BeneficiariesBySender")
    fun getRecipients(
            @Header("Authorization") auth: String,
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("json")
            json: String
    ): Observable<RecipientsResponse>


    @FormUrlEncoded
    @POST(PATH + "get/payment/locations")
    fun getBranches(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<LocationResponse>


    @FormUrlEncoded
    @POST(PATH + "get/payment/modes")
    fun getPaymentModes(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<PaymentModeResponse>

    @FormUrlEncoded
    @POST(PATH + "get/currency/country")
    fun getCurrency(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<CurrencyResponse>


    @FormUrlEncoded
    @POST(PATH + "validate/senderProfile")
    fun validateRecipient(
            @Header("Authorization") auth: String,
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("json")
            json: String
    ): Observable<SenderValidationResponse>

    @FormUrlEncoded
    @POST(PATH + "transfers/createRecipient")
    fun createRecipient(
            @Header("Authorization") auth: String,
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("json")
            json: String
    ): Observable<RecipientResponse>

    @FormUrlEncoded
    @POST(PATH + "transfers/createSender")
    fun createSender(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int,
            @Path("idioma") idioma: String, @Field("json") json: String
    ): Observable<SenderResponse>


    @FormUrlEncoded
    @POST(PATH + "transfers/createNewOrder")
    fun createNewOrder(
            @Header("Authorization") auth: String,
            @Path("idApp")
            idApp: Int, @Path("idioma") idioma: String, @Field("json")
            json: String
    ): Observable<OrderResponse>

    @FormUrlEncoded
    @POST(PATH + "transfers/confirmTransactionCredit")
    fun confirmTransactionCredit(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int, @Path("idioma") idioma: String, @Field("json")
            json: String
    ): Observable<CreditTransactionConfirmationResponse>

    @FormUrlEncoded
    @POST(PATH + "transfers/confirmTransactionCreditACH")
    fun confirmTransactionCreditACH(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int, @Path("idioma") idioma: String,
            @Field("json") json: String
    ): Observable<CreditTransactionConfirmationResponse>

    @FormUrlEncoded
    @POST(PATH + "transfers/createSenderProfileCard")
    fun createSenderProfileCard(
            @Header("Authorization") auth: String,
            @Path("idApp") idApp: Int, @Path("idioma") idioma: String, @Field("json")
            json: String
    ): Observable<SenderProfileResponse>

    companion object {

        const val PATH = "TransfersBoot/{idApp}/{idioma}/"

        //User: mobilecardmx
        //Pass: 82007eb4238205c75ebcdefc06a01311

        val AUTH = Credentials.basic("mobilecardmx", "82007eb4238205c75ebcdefc06a01311")

        fun get(r: Retrofit): ViamericasService {
            return r.create(ViamericasService::class.java)
        }
    }
}
