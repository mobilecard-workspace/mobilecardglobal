package addcel.mobilecard.data.net.colombia.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ColombiaRecargaCarrierEntity implements Parcelable {

    /**
     * label : TIGO montos : [{"comision":"500.00","description":"Recarga TIGO
     * 5000","idOperador":"1","idProducto":"1008","idRecarga":"60","monto":"5000.0","operador":"TIGO","productMovilRed":"3","productTypeMovilRed":"7702138005065"},{"comision":"500.00","description":"Recarga
     * TIGO
     * 10000","idOperador":"1","idProducto":"1008","idRecarga":"61","monto":"10000.0","operador":"TIGO","productMovilRed":"3","productTypeMovilRed":"7702138005065"},{"comision":"500.00","description":"Recarga
     * TIGO
     * 15000","idOperador":"1","idProducto":"1008","idRecarga":"62","monto":"15000.0","operador":"TIGO","productMovilRed":"3","productTypeMovilRed":"7702138005065"},{"comision":"500.00","description":"Recarga
     * TIGO
     * 20000","idOperador":"1","idProducto":"1008","idRecarga":"63","monto":"20000.0","operador":"TIGO","productMovilRed":"3","productTypeMovilRed":"7702138005065"},{"comision":"500.00","description":"Recarga
     * TIGO
     * 25000","idOperador":"1","idProducto":"1008","idRecarga":"64","monto":"25000.0","operador":"TIGO","productMovilRed":"3","productTypeMovilRed":"7702138005065"},{"comision":"500.00","description":"Recarga
     * TIGO
     * 30000","idOperador":"1","idProducto":"1008","idRecarga":"65","monto":"30000.0","operador":"TIGO","productMovilRed":"3","productTypeMovilRed":"7702138005065"}]
     * value : 1
     */
    @SerializedName("label")
    private String label;

    @SerializedName("value")
    private String value;

    @SerializedName("montos")
    private List<MontoEntity> montos;

    private ColombiaRecargaCarrierEntity(Parcel in) {
        label = in.readString();
        value = in.readString();
        montos = in.createTypedArrayList(MontoEntity.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(value);
        dest.writeTypedList(montos);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ColombiaRecargaCarrierEntity> CREATOR =
            new Creator<ColombiaRecargaCarrierEntity>() {
                @Override
                public ColombiaRecargaCarrierEntity createFromParcel(Parcel in) {
                    return new ColombiaRecargaCarrierEntity(in);
                }

                @Override
                public ColombiaRecargaCarrierEntity[] newArray(int size) {
                    return new ColombiaRecargaCarrierEntity[size];
                }
            };

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<MontoEntity> getMontos() {
        return montos;
    }

    public void setMontos(List<MontoEntity> montos) {
        this.montos = montos;
    }

    public static class MontoEntity implements Parcelable {
        /**
         * comision : 500.00 description : Recarga TIGO 5000 idOperador : 1 idProducto : 1008 idRecarga
         * : 60 monto : 5000.0 operador : TIGO productMovilRed : 3 productTypeMovilRed : 7702138005065
         */
        @SerializedName("comision")
        private String comision;

        @SerializedName("description")
        private String description;

        @SerializedName("idOperador")
        private String idOperador;

        @SerializedName("idProducto")
        private String idProducto;

        @SerializedName("idRecarga")
        private String idRecarga;

        @SerializedName("monto")
        private String monto;

        @SerializedName("operador")
        private String operador;

        @SerializedName("productMovilRed")
        private String productMovilRed;

        @SerializedName("productTypeMovilRed")
        private String productTypeMovilRed;

        MontoEntity(Parcel in) {
            comision = in.readString();
            description = in.readString();
            idOperador = in.readString();
            idProducto = in.readString();
            idRecarga = in.readString();
            monto = in.readString();
            operador = in.readString();
            productMovilRed = in.readString();
            productTypeMovilRed = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(comision);
            dest.writeString(description);
            dest.writeString(idOperador);
            dest.writeString(idProducto);
            dest.writeString(idRecarga);
            dest.writeString(monto);
            dest.writeString(operador);
            dest.writeString(productMovilRed);
            dest.writeString(productTypeMovilRed);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<MontoEntity> CREATOR = new Creator<MontoEntity>() {
            @Override
            public MontoEntity createFromParcel(Parcel in) {
                return new MontoEntity(in);
            }

            @Override
            public MontoEntity[] newArray(int size) {
                return new MontoEntity[size];
            }
        };

        public String getComision() {
            return comision;
        }

        public void setComision(String comision) {
            this.comision = comision;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIdOperador() {
            return idOperador;
        }

        public void setIdOperador(String idOperador) {
            this.idOperador = idOperador;
        }

        public String getIdProducto() {
            return idProducto;
        }

        public void setIdProducto(String idProducto) {
            this.idProducto = idProducto;
        }

        public String getIdRecarga() {
            return idRecarga;
        }

        public void setIdRecarga(String idRecarga) {
            this.idRecarga = idRecarga;
        }

        public String getMonto() {
            return monto;
        }

        public void setMonto(String monto) {
            this.monto = monto;
        }

        public String getOperador() {
            return operador;
        }

        public void setOperador(String operador) {
            this.operador = operador;
        }

        public String getProductMovilRed() {
            return productMovilRed;
        }

        public void setProductMovilRed(String productMovilRed) {
            this.productMovilRed = productMovilRed;
        }

        public String getProductTypeMovilRed() {
            return productTypeMovilRed;
        }

        public void setProductTypeMovilRed(String productTypeMovilRed) {
            this.productTypeMovilRed = productTypeMovilRed;
        }
    }
}
