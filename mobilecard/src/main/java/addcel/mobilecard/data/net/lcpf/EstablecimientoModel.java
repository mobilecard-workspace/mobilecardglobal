package addcel.mobilecard.data.net.lcpf;

import android.os.Parcel;
import android.os.Parcelable;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.data.net.hth.model.HthAccountResponse;

/**
 * ADDCEL on 30/08/17.
 */

public final class EstablecimientoModel implements Parcelable {
    public static final Creator<EstablecimientoModel> CREATOR = new Creator<EstablecimientoModel>() {
        @Override
        public EstablecimientoModel createFromParcel(Parcel in) {
            return new EstablecimientoModel(in);
        }

        @Override
        public EstablecimientoModel[] newArray(int size) {
            return new EstablecimientoModel[size];
        }
    };
    private int id;
    private String descripcion;
    private int logotipo;
    private HthAccountResponse.Account account;

    public EstablecimientoModel() {
    }

    public EstablecimientoModel(int id, String descripcion, int logotipo) {
        this.id = id;
        this.descripcion = descripcion;
        this.logotipo = logotipo;
    }

    public EstablecimientoModel(int id, String descripcion, int logotipo,
                                HthAccountResponse.Account account) {
        this.id = id;
        this.descripcion = descripcion;
        this.logotipo = logotipo;
        this.account = account;
    }

    private EstablecimientoModel(Parcel in) {
        id = in.readInt();
        descripcion = in.readString();
        logotipo = in.readInt();
        account = in.readParcelable(HthAccountResponse.Account.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(descripcion);
        dest.writeInt(logotipo);
        dest.writeParcelable(account, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getLogotipo() {
        return logotipo;
    }

    public HthAccountResponse.Account getAccount() {
        return account;
    }

    @NotNull
    @Override
    public String toString() {
        return descripcion;
    }
}
