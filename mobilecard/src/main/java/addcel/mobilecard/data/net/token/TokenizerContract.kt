package addcel.mobilecard.data.net.token

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 4/11/19.
 */

@Parcelize
data class SecurePaths(
        val start: String, val form: String, val authError: String,
        val error: String, val success: String
) : Parcelable

@Parcelize
data class TokenEntity(
        val accountId: String, val code: Int, val idUsuario: Long,
        val json: String, val message: String, val secure: Boolean, val token: String,
        @SerializedName("paths") val paths: SecurePaths?
) : Parcelable

data class TokenBaseResponse(
        val code: Int, val message: String
)

interface TokenizerAPI {

    @POST("$PATH{idApp}/{idPais}/{idioma}/getToken")
    fun getToken(
            @Header("Authorization")
            auth: String, @Header("Profile") profile: String, @Path("idApp") idApp: Int, @Path("idPais")
            idPais: Int, @Path("idioma") idioma: String
    ): Observable<TokenEntity>


    @GET("$PATH{idApp}/{idPais}/{idioma}/whiteList")
    fun isOnWhiteList(
            @Path("idApp") idApp: Int, @Path("idPais")
            idPais: Int, @Path("idioma") idioma: String, @Query("idUsuario") idUsuario: Long
    ): Observable<TokenBaseResponse>

    companion object {

        const val PATH = "Tokenizer/" //const val PATH = "Tokenizer/"

        fun provideTokenizerAPI(retrofit: Retrofit): TokenizerAPI {
            return retrofit.create(TokenizerAPI::class.java)
        }
    }
}