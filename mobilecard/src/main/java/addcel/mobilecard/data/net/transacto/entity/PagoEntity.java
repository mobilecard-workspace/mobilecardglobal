package addcel.mobilecard.data.net.transacto.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class PagoEntity implements Parcelable {

    public static final Creator<PagoEntity> CREATOR = new Creator<PagoEntity>() {
        @Override
        public PagoEntity createFromParcel(Parcel in) {
            return new PagoEntity(in);
        }

        @Override
        public PagoEntity[] newArray(int size) {
            return new PagoEntity[size];
        }
    };
    @SerializedName("comision")
    private double mComision;
    @SerializedName("concepto")
    private String mConcepto;
    @SerializedName("emisor")
    private int mEmisor;
    @SerializedName("idAplicacion")
    private int mIdAplicacion;
    @SerializedName("idPais")
    private int mIdPais;
    @SerializedName("idProveedor")
    private String mIdProveedor;
    @SerializedName("idTarjeta")
    private int mIdTarjeta;
    @SerializedName("idUsuario")
    private String mIdUsuario;
    @SerializedName("monto")
    private double mMonto;
    @SerializedName("operacion")
    private String mOperacion;
    @SerializedName("referencia")
    private String mReferencia;
    @SerializedName("software")
    private String mSoftware;
    @SerializedName("tipo")
    private String mTipo;
    @SerializedName("token")
    private String mToken;
    @SerializedName("idioma")
    private String mIdioma;
    @SerializedName("codigoPais")
    private String mPaisDestino;
    @SerializedName("debug")
    private boolean mDebug;
    @SerializedName("lat")
    private double mLat;
    @SerializedName("lon")
    private double mLon;
    @SerializedName("imei")
    private String mImei;

    private PagoEntity() {
    }

    private PagoEntity(Parcel in) {
        mComision = in.readDouble();
        mConcepto = in.readString();
        mEmisor = in.readInt();
        mIdAplicacion = in.readInt();
        mIdPais = in.readInt();
        mIdProveedor = in.readString();
        mIdTarjeta = in.readInt();
        mIdUsuario = in.readString();
        mMonto = in.readDouble();
        mOperacion = in.readString();
        mReferencia = in.readString();
        mSoftware = in.readString();
        mTipo = in.readString();
        mToken = in.readString();
        mIdioma = in.readString();
        mPaisDestino = in.readString();
        mDebug = in.readByte() != 0;
        mLat = in.readDouble();
        mLon = in.readDouble();
        mImei = in.readString();
    }

    @NotNull
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("mComision", mComision)
                .add("mConcepto", mConcepto)
                .add("mEmisor", mEmisor)
                .add("mIdAplicacion", mIdAplicacion)
                .add("mIdPais", mIdPais)
                .add("mIdProveedor", mIdProveedor)
                .add("mIdTarjeta", mIdTarjeta)
                .add("mIdUsuario", mIdUsuario)
                .add("mMonto", mMonto)
                .add("mOperacion", mOperacion)
                .add("mReferencia", mReferencia)
                .add("mSoftware", mSoftware)
                .add("mTipo", mTipo)
                .add("mToken", mToken)
                .add("mIdioma", mIdioma)
                .add("mPaisDestino", mPaisDestino)
                .add("mDebug", mDebug)
                .add("mLat", mLat)
                .add("mLon", mLon)
                .add("mImei", mImei)
                .toString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mComision);
        dest.writeString(mConcepto);
        dest.writeInt(mEmisor);
        dest.writeInt(mIdAplicacion);
        dest.writeInt(mIdPais);
        dest.writeString(mIdProveedor);
        dest.writeInt(mIdTarjeta);
        dest.writeString(mIdUsuario);
        dest.writeDouble(mMonto);
        dest.writeString(mOperacion);
        dest.writeString(mReferencia);
        dest.writeString(mSoftware);
        dest.writeString(mTipo);
        dest.writeString(mToken);
        dest.writeString(mIdioma);
        dest.writeString(mPaisDestino);
        dest.writeByte((byte) (mDebug ? 1 : 0));
        dest.writeDouble(mLat);
        dest.writeDouble(mLon);
        dest.writeString(mImei);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public double getComision() {
        return mComision;
    }

    public String getConcepto() {
        return mConcepto;
    }

    public int getEmisor() {
        return mEmisor;
    }

    public int getIdAplicacion() {
        return mIdAplicacion;
    }

    public int getIdPais() {
        return mIdPais;
    }

    public String getIdProveedor() {
        return mIdProveedor;
    }

    public int getIdTarjeta() {
        return mIdTarjeta;
    }

    public String getIdUsuario() {
        return mIdUsuario;
    }

    public double getMonto() {
        return mMonto;
    }

    public String getOperacion() {
        return mOperacion;
    }

    public String getReferencia() {
        return mReferencia;
    }

    public String getSoftware() {
        return mSoftware;
    }

    public String getTipo() {
        return mTipo;
    }

    public String getToken() {
        return mToken;
    }

    public String getPaisDestino() {
        return mPaisDestino;
    }

    public boolean idDebug() {
        return mDebug;
    }

    public double getLat() {
        return mLat;
    }

    public double getLon() {
        return mLon;
    }

    public String getImei() {
        return mImei;
    }

    public static class Builder {

        private double mComision;
        private String mConcepto;
        private int mEmisor;
        private int mIdAplicacion;
        private int mIdPais;
        private String mIdProveedor;
        private int mIdTarjeta;
        private String mIdUsuario;
        private double mMonto;
        private String mOperacion;
        private String mReferencia;
        private String mSoftware;
        private String mTipo;
        private String mToken;
        private String mPaisDestino;
        private String mIdioma;
        private boolean mDebug;
        private double mLat;
        private double mLon;
        private String mImei;

        public PagoEntity.Builder withComision(double comision) {
            mComision = comision;
            return this;
        }

        public PagoEntity.Builder withConcepto(String concepto) {
            mConcepto = concepto;
            return this;
        }

        public PagoEntity.Builder withEmisor(int emisor) {
            mEmisor = emisor;
            return this;
        }

        public PagoEntity.Builder withIdAplicacion(int idAplicacion) {
            mIdAplicacion = idAplicacion;
            return this;
        }

        public PagoEntity.Builder withIdPais(int idPais) {
            mIdPais = idPais;
            return this;
        }

        public PagoEntity.Builder withIdProveedor(String idProveedor) {
            mIdProveedor = idProveedor;
            return this;
        }

        public PagoEntity.Builder withIdTarjeta(int idTarjeta) {
            mIdTarjeta = idTarjeta;
            return this;
        }

        public PagoEntity.Builder withIdUsuario(String idUsuario) {
            mIdUsuario = idUsuario;
            return this;
        }

        public PagoEntity.Builder withMonto(double monto) {
            mMonto = monto;
            return this;
        }

        public PagoEntity.Builder withOperacion(String operacion) {
            mOperacion = operacion;
            return this;
        }

        public PagoEntity.Builder withReferencia(String referencia) {
            mReferencia = referencia;
            return this;
        }

        public PagoEntity.Builder withSoftware(String software) {
            mSoftware = software;
            return this;
        }

        public PagoEntity.Builder withTipo(String tipo) {
            mTipo = tipo;
            return this;
        }

        public PagoEntity.Builder withToken(String token) {
            mToken = token;
            return this;
        }

        public PagoEntity.Builder withIdioma(String idioma) {
            mIdioma = idioma;
            return this;
        }

        public PagoEntity.Builder withPaisDestino(String paisDestino) {
            mPaisDestino = paisDestino;
            return this;
        }

        public PagoEntity.Builder withDebug(boolean mDebug) {
            this.mDebug = mDebug;
            return this;
        }

        public PagoEntity.Builder withLat(double mLat) {
            this.mLat = mLat;
            return this;
        }

        public PagoEntity.Builder withLon(double mLon) {
            this.mLon = mLon;
            return this;
        }

        public Builder withImei(String mImei) {
            this.mImei = mImei;
            return this;
        }

        public PagoEntity build() {
            PagoEntity PagoModel = new PagoEntity();
            PagoModel.mComision = mComision;
            PagoModel.mConcepto = mConcepto;
            PagoModel.mEmisor = mEmisor;
            PagoModel.mIdAplicacion = mIdAplicacion;
            PagoModel.mIdPais = mIdPais;
            PagoModel.mIdProveedor = mIdProveedor;
            PagoModel.mIdTarjeta = mIdTarjeta;
            PagoModel.mIdUsuario = mIdUsuario;
            PagoModel.mMonto = mMonto;
            PagoModel.mOperacion = mOperacion;
            PagoModel.mReferencia = mReferencia;
            PagoModel.mSoftware = mSoftware;
            PagoModel.mTipo = mTipo;
            PagoModel.mToken = mToken;
            PagoModel.mIdioma = mIdioma;
            PagoModel.mPaisDestino = mPaisDestino;
            PagoModel.mDebug = mDebug;
            PagoModel.mLat = mLat;
            PagoModel.mLon = mLon;
            PagoModel.mImei = mImei;
            return PagoModel;
        }
    }
}
