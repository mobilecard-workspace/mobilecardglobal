package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PaymentLocationNetworkRequest {

    @SerializedName("amount")
    private String mAmount;
    @SerializedName("idCountry")
    private String mIdCountry;
    @SerializedName("idModePayCurrency")
    private String mIdModePayCurrency;
    @SerializedName("idPaymentMode")
    private String mIdPaymentMode;
    @SerializedName("onlyNetwork")
    private String mOnlyNetwork;
    @SerializedName("withCost")
    private String mWithCost;

    public PaymentLocationNetworkRequest(String mAmount, String mIdCountry,
                                         String mIdModePayCurrency) {
        this.mAmount = mAmount;
        this.mIdCountry = mIdCountry;
        this.mIdModePayCurrency = mIdModePayCurrency;
        this.mIdPaymentMode = "0";
        this.mOnlyNetwork = "C";
        this.mWithCost = "true";
    }

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public String getIdCountry() {
        return mIdCountry;
    }

    public void setIdCountry(String idCountry) {
        mIdCountry = idCountry;
    }

    public String getIdModePayCurrency() {
        return mIdModePayCurrency;
    }

    public void setIdModePayCurrency(String idModePayCurrency) {
        mIdModePayCurrency = idModePayCurrency;
    }

    public String getIdPaymentMode() {
        return mIdPaymentMode;
    }

    public void setIdPaymentMode(String idPaymentMode) {
        mIdPaymentMode = idPaymentMode;
    }

    public String getOnlyNetwork() {
        return mOnlyNetwork;
    }

    public void setOnlyNetwork(String onlyNetwork) {
        mOnlyNetwork = onlyNetwork;
    }

    public String getWithCost() {
        return mWithCost;
    }

    public void setWithCost(String withCost) {
        mWithCost = withCost;
    }
}
