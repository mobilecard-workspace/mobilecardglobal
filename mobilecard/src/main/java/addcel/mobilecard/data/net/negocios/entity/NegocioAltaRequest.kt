package addcel.mobilecard.data.net.negocios.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NegocioAltaRequest(
        @SerializedName("usuario")
        val usuario: String = "",
        @SerializedName("pass")
        val pass: String = "",
        @SerializedName("nombre_establecimiento")
        val nombreEstablecimiento: String = "",
        @SerializedName("rfc")
        val rfc: String = "",
        @SerializedName("razon_social")
        val razonSocial: String = "",
        @SerializedName("representante_legal")
        val representanteLegal: String = "",
        @SerializedName("direccion_establecimiento")
        val direccionEstablecimiento: String = "",
        @SerializedName("email_contacto")
        val emailContacto: String = "",
        @SerializedName("telefono_contacto")
        val telefonoContacto: String = "",
        @SerializedName("id_banco")
        val idBanco: String = "",
        @SerializedName("cuenta_clabe")
        val cuentaClabe: String = "",
        @SerializedName("nombre_prop_cuenta")
        val nombrePropCuenta: String = "",
        @SerializedName("id_account")
        val idAccount: Int = 0,
        @SerializedName("estatus")
        val estatus: Int = 0,
        @SerializedName("comision_fija")
        val comisionFija: Double = 0.toDouble(),
        @SerializedName("comision_porcentaje")
        val comisionPorcentaje: Double = 0.toDouble(),
        @SerializedName("urlLogo")
        val urlLogo: String = "",
        @SerializedName("cuenta_tipo")
        val cuentaTipo: String = "",
        @SerializedName("codigo_banco")
        val codigoBanco: String = "",
        @SerializedName("id_aplicacion")
        val idAplicacion: Int = 1,
        @SerializedName("negocio")
        val negocio: Boolean = true,
        @SerializedName("img_ine")
        val imgIne: String = "",
        @SerializedName("img_domicilio")
        val imgDomicilio: String = "",
        @SerializedName("t_previvale")
        val tPrevivale: String = "",
        @SerializedName("nombre")
        val nombre: String = "",
        @SerializedName("paterno")
        val paterno: String = "",
        @SerializedName("materno")
        val materno: String = "",
        @SerializedName("colonia")
        val colonia: String = "",
        @SerializedName("ciudad")
        val ciudad: String = "",
        @SerializedName("cp")
        val cp: String = "",
        @SerializedName("id_estado")
        val idEstado: Int = 0,
        @SerializedName("curp")
        val curp: String = "",
        @SerializedName("smsCode")
        val smsCode: String = "",
        @SerializedName("idPais")
        val idPais: Int = 1,
        @SerializedName("tipoDocumento")
        val tipoDocumento: String = "",
        @SerializedName("numeroDocumento")
        val numeroDocumento: String = "",
        @SerializedName("fechaNacimiento")
        val fechaNacimiento: String = "",
        @SerializedName("nacionalidad")
        val nacionalidad: String = "",
        @SerializedName("distrito")
        val distrito: String = "",
        @SerializedName("provincia")
        val provincia: String = "",
        @SerializedName("departamento")
        val departamento: String = "",
        @SerializedName("centroLaboral")
        val centroLaboral: String = "",
        @SerializedName("ocupacion")
        val ocupacion: String = "",
        @SerializedName("cargo")
        val cargo: String = "",
        @SerializedName("institucion")
        val institucion: String = "",
        @SerializedName("cci")
        val cci: String = "",
        @SerializedName("id_establecimiento")
        val idEstablecimiento: Int = 0,
        @SerializedName("digito_verificador")
        val digitoVerificador: String = "",
        @SerializedName("direccion")
        val direccion: String = "",
        @SerializedName("tipo_persona")
        val tipoPersona: String = ""
) : Parcelable