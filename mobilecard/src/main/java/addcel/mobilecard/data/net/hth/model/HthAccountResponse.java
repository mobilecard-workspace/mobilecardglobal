package addcel.mobilecard.data.net.hth.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.List;

@SuppressWarnings("unused")
public class HthAccountResponse {

    @SerializedName("accounts")
    private List<Account> Accounts;

    @SerializedName("idError")
    private Long IdError;

    @SerializedName("mensajeError")
    private String MensajeError;

    public HthAccountResponse(Long idError, String mensajeError) {
        IdError = idError;
        MensajeError = mensajeError;
    }

    public List<Account> getAccounts() {
        return Accounts;
    }

    public void setAccounts(List<Account> accounts) {
        Accounts = accounts;
    }

    public Long getIdError() {
        return IdError;
    }

    public void setIdError(Long idError) {
        IdError = idError;
    }

    public String getMensajeError() {
        return MensajeError;
    }

    public void setMensajeError(String mensajeError) {
        MensajeError = mensajeError;
    }

    @SuppressWarnings("unused")
    public static class Account implements Parcelable {

        public static final Creator<Account> CREATOR = new Creator<Account>() {
            @Override
            public Account createFromParcel(Parcel in) {
                return new Account(in);
            }

            @Override
            public Account[] newArray(int size) {
                return new Account[size];
            }
        };

        @SerializedName("alias")
        private String Alias;

        @SerializedName("id")
        private long Id;

        @SerializedName("comision_fija")
        private double ComisionFija;

        @SerializedName("comision_porcentaje")
        private double ComisionPorcentaje;

        @SerializedName("telefono")
        private String Telefono;

        @SerializedName("correo")
        private String Correo;

        public Account(String alias, long id, double comisionFija, double comisionPorcentaje,
                       String telefono, String correo) {
            Alias = alias;
            Id = id;
            ComisionFija = comisionFija;
            ComisionPorcentaje = comisionPorcentaje;
            Telefono = telefono;
            Correo = correo;
        }

        protected Account(Parcel in) {
            Alias = in.readString();
            Id = in.readLong();
            ComisionFija = in.readDouble();
            ComisionPorcentaje = in.readDouble();
            Telefono = in.readString();
            Correo = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(Alias);
            dest.writeLong(Id);
            dest.writeDouble(ComisionFija);
            dest.writeDouble(ComisionPorcentaje);
            dest.writeString(Telefono);
            dest.writeString(Correo);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getAlias() {
            return Alias;
        }

        public void setAlias(String alias) {
            Alias = alias;
        }

        public long getId() {
            return Id;
        }

        public void setId(long id) {
            Id = id;
        }

        public double getComisionFija() {
            return ComisionFija;
        }

        public Account setComisionFija(double comisionFija) {
            ComisionFija = comisionFija;
            return this;
        }

        public double getComisionPorcentaje() {
            return ComisionPorcentaje;
        }

        public Account setComisionPorcentaje(double comisionPorcentaje) {
            ComisionPorcentaje = comisionPorcentaje;
            return this;
        }

        public String getTelefono() {
            return Telefono;
        }

        public Account setTelefono(String telefono) {
            Telefono = telefono;
            return this;
        }

        public String getCorreo() {
            return Correo;
        }

        public Account setCorreo(String correo) {
            Correo = correo;
            return this;
        }

        @NotNull
        @Override
        public String toString() {
            return Alias;
        }
    }
}
