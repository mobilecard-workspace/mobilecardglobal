package addcel.mobilecard.data.net.negocios.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.gson.annotations.SerializedName;

import addcel.mobilecard.data.net.wallet.CardEntity;

public class NegocioEntity implements Parcelable {

    public final static int STATUS_RESET = 98;
    public final static int STATUS_ACTIVE = 1;
    public final static int STATUS_PWD = 99;
    public final static int STATUS_SMS = 100;
    public static final Creator<NegocioEntity> CREATOR = new Creator<NegocioEntity>() {
        @Override
        public NegocioEntity createFromParcel(Parcel in) {
            return new NegocioEntity(in);
        }

        @Override
        public NegocioEntity[] newArray(int size) {
            return new NegocioEntity[size];
        }
    };
    /**
     * card : {"balance":0,"codigo":"","cpAmex":"","determinada":false,"domAmex":null,"idTarjeta":0,"idUsuario":0,"isMobilecard":false,"mobilecard":false,"nombre":"","pan":"","representanteLegal":"","tipo":0,"tipoTarjeta":"","vigencia":""}
     * comisionFija : 0
     * comisionPorcentaje : 0.5
     * cuentaClabe : 07261656465623653133
     * direccionEstablecimiento : Calle Alce Blanco 16
     * email : elmans8+pe@gmail.com
     * idAccount : 0
     * idBanco : 4005
     * idError : 0
     * idEstablecimiento : 140
     * idUsrStatus : 100
     * jumioStatus :
     * mensajeError : login exitoso
     * negocio : true
     * nombreEstablecimiento : CARLOS VINOS
     * nombrePropCuenta :
     * razonSocial : GASC SA DE CV
     * representanteLegal : CARLOS GARCÍA SANOJA
     * scanReference :
     * telefono : 5518307722
     * urlLogo : http://199.231.160.203:80/LCPFServices/logo_default.png
     */

    @SerializedName("card")
    private CardEntity card;
    @SerializedName("comisionFija")
    private int comisionFija;
    @SerializedName("comisionPorcentaje")
    private double comisionPorcentaje;
    @SerializedName("cuentaClabe")
    private String cuentaClabe;
    @SerializedName("direccionEstablecimiento")
    private String direccionEstablecimiento;
    @SerializedName("email")
    private String email;
    @SerializedName("idAccount")
    private int idAccount;
    @SerializedName("idBanco")
    private String idBanco;
    @SerializedName("idError")
    private int idError;
    @SerializedName("idEstablecimiento")
    private int idEstablecimiento;
    @SerializedName("idUsrStatus")
    private int idUsrStatus;
    @SerializedName("jumioStatus")
    private String jumioStatus;
    @SerializedName("mensajeError")
    private String mensajeError;
    @SerializedName("negocio")
    private boolean negocio;
    @SerializedName("nombreEstablecimiento")
    private String nombreEstablecimiento;
    @SerializedName("nombrePropCuenta")
    private String nombrePropCuenta;
    @SerializedName("razonSocial")
    private String razonSocial;
    @SerializedName("representanteLegal")
    private String representanteLegal;
    @SerializedName("scanReference")
    private String scanReference;
    @SerializedName("telefono")
    private String telefono;
    @SerializedName("urlLogo")
    private String urlLogo;
    @SerializedName("idPais")
    private int idPais;
    @SerializedName("qrTag")
    private String qrTag;
    @SerializedName("qrBase64")
    private String qrBase64;
    @SerializedName("tipo_persona")
    private String tipoPersona;
    @SerializedName("digito_verificador")
    private String digitoVerificador;
    @SerializedName("cardPrevivale")
    private boolean cardPrevivale;

    private NegocioEntity(Builder builder) {
        setCard(builder.card);
        setComisionFija(builder.comisionFija);
        setComisionPorcentaje(builder.comisionPorcentaje);
        setCuentaClabe(builder.cuentaClabe);
        setDireccionEstablecimiento(builder.direccionEstablecimiento);
        setEmail(builder.email);
        setIdAccount(builder.idAccount);
        setIdBanco(builder.idBanco);
        setIdError(builder.idError);
        setIdEstablecimiento(builder.idEstablecimiento);
        setIdUsrStatus(builder.idUsrStatus);
        setJumioStatus(builder.jumioStatus);
        setMensajeError(builder.mensajeError);
        setNegocio(builder.negocio);
        setNombreEstablecimiento(builder.nombreEstablecimiento);
        setNombrePropCuenta(builder.nombrePropCuenta);
        setRazonSocial(builder.razonSocial);
        setRepresentanteLegal(builder.representanteLegal);
        setScanReference(builder.scanReference);
        setTelefono(builder.telefono);
        setUrlLogo(builder.urlLogo);
        setIdPais(builder.idPais);
        setQrTag(builder.qrTag);
        setQrBase64(builder.qrBase64);
        setTipoPersona(builder.tipoPersona);
        setDigitoVerificador(builder.digitoVerificador);
        setCardPrevivale(builder.cardPrevivale);
    }

    protected NegocioEntity(Parcel in) {
        card = in.readParcelable(CardEntity.class.getClassLoader());
        comisionFija = in.readInt();
        comisionPorcentaje = in.readDouble();
        cuentaClabe = in.readString();
        direccionEstablecimiento = in.readString();
        email = in.readString();
        idAccount = in.readInt();
        idBanco = in.readString();
        idError = in.readInt();
        idEstablecimiento = in.readInt();
        idUsrStatus = in.readInt();
        jumioStatus = in.readString();
        mensajeError = in.readString();
        negocio = in.readByte() != 0;
        nombreEstablecimiento = in.readString();
        nombrePropCuenta = in.readString();
        razonSocial = in.readString();
        representanteLegal = in.readString();
        scanReference = in.readString();
        telefono = in.readString();
        urlLogo = in.readString();
        idPais = in.readInt();
        qrTag = in.readString();
        qrBase64 = in.readString();
        tipoPersona = in.readString();
        digitoVerificador = in.readString();
        cardPrevivale = in.readByte() != 0;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(NegocioEntity copy) {
        Builder builder = new Builder();
        builder.card = copy.getCard();
        builder.comisionFija = copy.getComisionFija();
        builder.comisionPorcentaje = copy.getComisionPorcentaje();
        builder.cuentaClabe = copy.getCuentaClabe();
        builder.direccionEstablecimiento = copy.getDireccionEstablecimiento();
        builder.email = copy.getEmail();
        builder.idAccount = copy.getIdAccount();
        builder.idBanco = copy.getIdBanco();
        builder.idError = copy.getIdError();
        builder.idEstablecimiento = copy.getIdEstablecimiento();
        builder.idUsrStatus = copy.getIdUsrStatus();
        builder.jumioStatus = copy.getJumioStatus();
        builder.mensajeError = copy.getMensajeError();
        builder.negocio = copy.isNegocio();
        builder.nombreEstablecimiento = copy.getNombreEstablecimiento();
        builder.nombrePropCuenta = copy.getNombrePropCuenta();
        builder.razonSocial = copy.getRazonSocial();
        builder.representanteLegal = copy.getRepresentanteLegal();
        builder.scanReference = copy.getScanReference();
        builder.telefono = copy.getTelefono();
        builder.urlLogo = copy.getUrlLogo();
        builder.idPais = copy.getIdPais();
        builder.qrTag = copy.getQrTag();
        builder.qrBase64 = copy.getQrBase64();
        builder.tipoPersona = copy.getTipoPersona();
        builder.digitoVerificador = copy.getDigitoVerificador();
        return builder;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(card, flags);
        dest.writeInt(comisionFija);
        dest.writeDouble(comisionPorcentaje);
        dest.writeString(cuentaClabe);
        dest.writeString(direccionEstablecimiento);
        dest.writeString(email);
        dest.writeInt(idAccount);
        dest.writeString(idBanco);
        dest.writeInt(idError);
        dest.writeInt(idEstablecimiento);
        dest.writeInt(idUsrStatus);
        dest.writeString(jumioStatus);
        dest.writeString(mensajeError);
        dest.writeByte((byte) (negocio ? 1 : 0));
        dest.writeString(nombreEstablecimiento);
        dest.writeString(nombrePropCuenta);
        dest.writeString(razonSocial);
        dest.writeString(representanteLegal);
        dest.writeString(scanReference);
        dest.writeString(telefono);
        dest.writeString(urlLogo);
        dest.writeInt(idPais);
        dest.writeString(qrTag);
        dest.writeString(qrBase64);
        dest.writeString(tipoPersona);
        dest.writeString(digitoVerificador);
        dest.writeByte((byte) (cardPrevivale ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public CardEntity getCard() {
        return card;
    }

    public void setCard(CardEntity card) {
        this.card = card;
    }

    public int getComisionFija() {
        return comisionFija;
    }

    public void setComisionFija(int comisionFija) {
        this.comisionFija = comisionFija;
    }

    public double getComisionPorcentaje() {
        return comisionPorcentaje;
    }

    public void setComisionPorcentaje(double comisionPorcentaje) {
        this.comisionPorcentaje = comisionPorcentaje;
    }

    public String getCuentaClabe() {
        return cuentaClabe;
    }

    public void setCuentaClabe(String cuentaClabe) {
        this.cuentaClabe = cuentaClabe;
    }

    public String getDireccionEstablecimiento() {
        return direccionEstablecimiento;
    }

    public void setDireccionEstablecimiento(String direccionEstablecimiento) {
        this.direccionEstablecimiento = direccionEstablecimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public String getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(String idBanco) {
        this.idBanco = idBanco;
    }

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }

    public int getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(int idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }

    public int getIdUsrStatus() {
        return idUsrStatus;
    }

    public void setIdUsrStatus(int idUsrStatus) {
        this.idUsrStatus = idUsrStatus;
    }

    public String getJumioStatus() {
        return Strings.nullToEmpty(jumioStatus);
    }

    public void setJumioStatus(String jumioStatus) {
        this.jumioStatus = jumioStatus;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public boolean isNegocio() {
        return negocio;
    }

    public void setNegocio(boolean negocio) {
        this.negocio = negocio;
    }

    public String getNombreEstablecimiento() {
        return nombreEstablecimiento;
    }

    public void setNombreEstablecimiento(String nombreEstablecimiento) {
        this.nombreEstablecimiento = nombreEstablecimiento;
    }

    public String getNombrePropCuenta() {
        return nombrePropCuenta;
    }

    public void setNombrePropCuenta(String nombrePropCuenta) {
        this.nombrePropCuenta = nombrePropCuenta;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(String representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    public String getScanReference() {
        return Strings.nullToEmpty(scanReference);
    }

    public void setScanReference(String scanReference) {
        this.scanReference = scanReference;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrlLogo() {
        return urlLogo;
    }

    public void setUrlLogo(String urlLogo) {
        this.urlLogo = urlLogo;
    }

    public String getQrTag() {
        return Strings.nullToEmpty(qrTag);
    }

    public void setQrTag(String qrTag) {
        this.qrTag = qrTag;
    }

    public String getQrBase64() {
        return Strings.nullToEmpty(qrBase64);
    }

    public void setQrBase64(String qrBase64) {
        this.qrBase64 = qrBase64;
    }

    public String getTipoPersona() {
        return Strings.nullToEmpty(tipoPersona);
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getDigitoVerificador() {
        return Strings.nullToEmpty(digitoVerificador);
    }

    public void setDigitoVerificador(String digitoVerificador) {
        this.digitoVerificador = digitoVerificador;
    }

    public boolean isCardPrevivale() {
        return cardPrevivale;
    }

    public NegocioEntity setCardPrevivale(boolean cardPrevivale) {
        this.cardPrevivale = cardPrevivale;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("card", card)
                .add("comisionFija", comisionFija)
                .add("comisionPorcentaje", comisionPorcentaje)
                .add("cuentaClabe", cuentaClabe)
                .add("direccionEstablecimiento", direccionEstablecimiento)
                .add("email", email)
                .add("idAccount", idAccount)
                .add("idBanco", idBanco)
                .add("idError", idError)
                .add("idEstablecimiento", idEstablecimiento)
                .add("idUsrStatus", idUsrStatus)
                .add("jumioStatus", jumioStatus)
                .add("mensajeError", mensajeError)
                .add("negocio", negocio)
                .add("nombreEstablecimiento", nombreEstablecimiento)
                .add("nombrePropCuenta", nombrePropCuenta)
                .add("razonSocial", razonSocial)
                .add("representanteLegal", representanteLegal)
                .add("scanReference", scanReference)
                .add("telefono", telefono)
                .add("urlLogo", urlLogo)
                .add("idPais", idPais)
                .add("digitoVerificador", digitoVerificador)
                .add("cardPrevivale", cardPrevivale)
                .toString();
    }

    public static final class Builder {
        private CardEntity card;
        private int comisionFija;
        private double comisionPorcentaje;
        private String cuentaClabe;
        private String direccionEstablecimiento;
        private String email;
        private int idAccount;
        private String idBanco;
        private int idError;
        private int idEstablecimiento;
        private int idUsrStatus;
        private String jumioStatus;
        private String mensajeError;
        private boolean negocio;
        private String nombreEstablecimiento;
        private String nombrePropCuenta;
        private String razonSocial;
        private String representanteLegal;
        private String scanReference;
        private String telefono;
        private String urlLogo;
        private int idPais;
        private String qrTag;
        private String qrBase64;
        private String tipoPersona;
        private String digitoVerificador;
        private boolean cardPrevivale;

        public Builder() {
        }

        public Builder(NegocioEntity copy) {
            this.comisionFija = copy.getComisionFija();
            this.comisionPorcentaje = copy.getComisionPorcentaje();
            this.cuentaClabe = copy.getCuentaClabe();
            this.direccionEstablecimiento = copy.getDireccionEstablecimiento();
            this.email = copy.getEmail();
            this.idAccount = copy.getIdAccount();
            this.idBanco = copy.getIdBanco();
            this.idError = copy.getIdError();
            this.idEstablecimiento = copy.getIdEstablecimiento();
            this.idUsrStatus = copy.getIdUsrStatus();
            this.mensajeError = copy.getMensajeError();
            this.negocio = copy.isNegocio();
            this.nombreEstablecimiento = copy.getNombreEstablecimiento();
            this.nombrePropCuenta = copy.getNombrePropCuenta();
            this.razonSocial = copy.getRazonSocial();
            this.representanteLegal = copy.getRepresentanteLegal();
            this.telefono = copy.getTelefono();
            this.urlLogo = copy.getUrlLogo();
            this.card = copy.getCard();
            this.idPais = copy.getIdPais();
            this.qrTag = copy.getQrTag();
            this.qrBase64 = copy.getQrBase64();
            this.tipoPersona = copy.getTipoPersona();
            this.digitoVerificador = copy.getDigitoVerificador();
            this.cardPrevivale = copy.isCardPrevivale();
        }

        public Builder setCard(CardEntity card) {
            this.card = card;
            return this;
        }

        public Builder setComisionFija(int comisionFija) {
            this.comisionFija = comisionFija;
            return this;
        }

        public Builder setComisionPorcentaje(double comisionPorcentaje) {
            this.comisionPorcentaje = comisionPorcentaje;
            return this;
        }

        public Builder setCuentaClabe(String cuentaClabe) {
            this.cuentaClabe = cuentaClabe;
            return this;
        }

        public Builder setDireccionEstablecimiento(String direccionEstablecimiento) {
            this.direccionEstablecimiento = direccionEstablecimiento;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setIdAccount(int idAccount) {
            this.idAccount = idAccount;
            return this;
        }

        public Builder setIdBanco(String idBanco) {
            this.idBanco = idBanco;
            return this;
        }

        public Builder setIdError(int idError) {
            this.idError = idError;
            return this;
        }

        public Builder setIdEstablecimiento(int idEstablecimiento) {
            this.idEstablecimiento = idEstablecimiento;
            return this;
        }

        public Builder setIdUsrStatus(int idUsrStatus) {
            this.idUsrStatus = idUsrStatus;
            return this;
        }

        public Builder setJumioStatus(String jumioStatus) {
            this.jumioStatus = jumioStatus;
            return this;
        }

        public Builder setMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }

        public Builder setNegocio(boolean negocio) {
            this.negocio = negocio;
            return this;
        }

        public Builder setCardPrevivale(boolean cardPrevivale) {
            this.cardPrevivale = cardPrevivale;
            return this;
        }

        public Builder setNombreEstablecimiento(String nombreEstablecimiento) {
            this.nombreEstablecimiento = nombreEstablecimiento;
            return this;
        }

        public Builder setNombrePropCuenta(String nombrePropCuenta) {
            this.nombrePropCuenta = nombrePropCuenta;
            return this;
        }

        public Builder setRazonSocial(String razonSocial) {
            this.razonSocial = razonSocial;
            return this;
        }

        public Builder setRepresentanteLegal(String representanteLegal) {
            this.representanteLegal = representanteLegal;
            return this;
        }

        public Builder setScanReference(String scanReference) {
            this.scanReference = scanReference;
            return this;
        }

        public Builder setTelefono(String telefono) {
            this.telefono = telefono;
            return this;
        }

        public Builder setUrlLogo(String urlLogo) {
            this.urlLogo = urlLogo;
            return this;
        }

        public Builder setIdPais(int idPais) {
            this.idPais = idPais;
            return this;
        }

        public Builder setQrTag(String qrTag) {
            this.qrTag = qrTag;
            return this;
        }

        public Builder setQrBase64(String qrBase64) {
            this.qrBase64 = qrBase64;
            return this;
        }

        public Builder setTipoPersona(String tipoPersona) {
            this.tipoPersona = tipoPersona;
            return this;
        }

        public NegocioEntity build() {
            return new NegocioEntity(this);
        }
    }
    /**
     * comisionFija : 6.38 comisionPorcentaje : 0.05 cuentaClabe : 012256564656646643
     * direccionEstablecimiento : Andres De Urdaneta 48 Naucalpan Mexico email :
     * carlos.garcia+biz1@addcel.com idAccount : 42 idBanco : 6 idError : 0 idEstablecimiento : 18
     * idUsrStatus : 1 mensajeError : login exitoso negocio : true nombreEstablecimiento : Fotos
     * nombrePropCuenta : Carlos Garcia Sanoja razonSocial : FOTOS Y RECUERDOS LOL DE CV
     * representanteLegal : Carlos Garcia Sanoja telefono : urlLogo :
     * http://199.231.160.203:80/LCPFServices/logo_default.png
     */

}
