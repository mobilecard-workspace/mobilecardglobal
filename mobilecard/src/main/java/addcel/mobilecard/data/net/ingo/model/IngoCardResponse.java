package addcel.mobilecard.data.net.ingo.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class IngoCardResponse {

    @Expose
    private Long errorCode;
    @Expose
    private String errorMessage;

    public Long getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public static class Builder {

        private Long errorCode;
        private String errorMessage;

        public IngoCardResponse.Builder withErrorCode(Long errorCode) {
            this.errorCode = errorCode;
            return this;
        }

        public IngoCardResponse.Builder withErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public IngoCardResponse build() {
            IngoCardResponse ingoCardResponse = new IngoCardResponse();
            ingoCardResponse.errorCode = errorCode;
            ingoCardResponse.errorMessage = errorMessage;
            return ingoCardResponse;
        }
    }
}
