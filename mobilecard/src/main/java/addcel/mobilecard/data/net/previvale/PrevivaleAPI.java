package addcel.mobilecard.data.net.previvale;

import addcel.mobilecard.data.net.previvale.entity.PrActivateRequestEntity;
import addcel.mobilecard.data.net.previvale.entity.PrActivateResponseEntity;
import addcel.mobilecard.data.net.previvale.entity.PrCardRequest;
import addcel.mobilecard.data.net.previvale.entity.PrCardResponse;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * ADDCEL on 11/14/18.
 */
public interface PrevivaleAPI {
    String PATH = "PreviVale/api/";

    @POST(PATH + "registrarTarjeta")
    Observable<PrActivateResponseEntity> activaTarjeta(
            @Body PrActivateRequestEntity request);

    @POST(PATH + "{idApp}/{idPais}/{idioma}/registrarNegocioConTarjeta")
    Observable<PrActivateResponseEntity> activarTarjeta(@Header("Authorization") String auth,
                                                        @Path("idApp") int idApp, @Path("idPais") int idPais, @Path("idioma") String idioma,
                                                        @Body PrActivateRequestEntity request);

    /*
    String credentials = Credentials.basic("mobilecardmx", "82007eb4238205c75ebcdefc06a01311");
    */
    @POST(PATH + "{idApp}/{idPais}/{idioma}/estatusTarjetaNegocio")
    Observable<PrCardResponse> getNegocioCard(@Header("Authorization") String auth,
                                              @Path("idApp") int idApp, @Path("idPais") int idPais, @Path("idioma") String idioma,
                                              @Body PrCardRequest idEstablecimiento);
}
