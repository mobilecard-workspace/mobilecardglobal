package addcel.mobilecard.data.net.colombia.entity;

public class ColombiaRequestEntity {

    private final int idAplicacion;
    private final int idUsuario;
    private final String usuario;
    private final String password;
    private final String referencia;
    private final int idServicio;
    private final String referenciaServicio;
    private final int idProveedor;
    private final int idProducto;
    private final int idRecarga;

    private ColombiaRequestEntity(Builder builder) {
        idAplicacion = builder.idAplicacion;
        idUsuario = builder.idUsuario;
        usuario = builder.usuario;
        password = builder.password;
        referencia = builder.referencia;
        idServicio = builder.idServicio;
        referenciaServicio = builder.referenciaServicio;
        idProveedor = builder.idProveedor;
        idProducto = builder.idProducto;
        idRecarga = builder.idRecarga;
    }

    public int getIdAplicacion() {
        return idAplicacion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getPassword() {
        return password;
    }

    public String getReferencia() {
        return referencia;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public String getReferenciaServicio() {
        return referenciaServicio;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public int getIdRecarga() {
        return idRecarga;
    }

    public static final class Builder {
        private int idAplicacion;
        private int idUsuario;
        private String usuario;
        private String password;
        private String referencia;
        private int idServicio;
        private String referenciaServicio;
        private int idProveedor;
        private int idProducto;
        private int idRecarga;

        public Builder() {
        }

        public Builder setIdAplicacion(int val) {
            idAplicacion = val;
            return this;
        }

        public Builder setIdUsuario(int val) {
            idUsuario = val;
            return this;
        }

        public Builder setUsuario(String val) {
            usuario = val;
            return this;
        }

        public Builder setPassword(String val) {
            password = val;
            return this;
        }

        public Builder setReferencia(String val) {
            referencia = val;
            return this;
        }

        public Builder setIdServicio(int val) {
            idServicio = val;
            return this;
        }

        public Builder setReferenciaServicio(String val) {
            referenciaServicio = val;
            return this;
        }

        public Builder setIdProveedor(int val) {
            idProveedor = val;
            return this;
        }

        public Builder setIdProducto(int val) {
            idProducto = val;
            return this;
        }

        public Builder setIdRecarga(int val) {
            idRecarga = val;
            return this;
        }

        public ColombiaRequestEntity build() {
            return new ColombiaRequestEntity(this);
        }
    }
}
