package addcel.mobilecard.data.net.scanpay.model;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 11/14/18.
 */
public final class SPCuentaEntity {

    @SerializedName("codigo_aut")
    private String CodigoAut;

    @SerializedName("comision")
    private double Comision;

    @SerializedName("fecha")
    private String Fecha;

    @SerializedName("id_bitacora")
    private int IdBitacora;

    @SerializedName("importe")
    private double Importe;

    @SerializedName("propina")
    private double Propina;

    @SerializedName("referencia_negocio")
    private String ReferenciaNegocio;

    @SerializedName("total")
    private double Total;

    public SPCuentaEntity() {
    }

    private SPCuentaEntity(Builder builder) {
        CodigoAut = builder.CodigoAut;
        Comision = builder.Comision;
        Fecha = builder.Fecha;
        IdBitacora = builder.IdBitacora;
        Importe = builder.Importe;
        Propina = builder.Propina;
        ReferenciaNegocio = builder.ReferenciaNegocio;
        Total = builder.Total;
    }

    public String getCodigoAut() {
        return CodigoAut;
    }

    public double getComision() {
        return Comision;
    }

    public String getFecha() {
        return Fecha;
    }

    private int getIdBitacora() {
        return IdBitacora;
    }

    public double getImporte() {
        return Importe;
    }

    public double getPropina() {
        return Propina;
    }

    public String getReferenciaNegocio() {
        return ReferenciaNegocio;
    }

    public double getTotal() {
        return Total;
    }

    public static final class Builder {
        private String CodigoAut;
        private double Comision;
        private String Fecha;
        private int IdBitacora;
        private double Importe;
        private double Propina;
        private String ReferenciaNegocio;
        private double Total;

        public Builder() {
        }

        public Builder(SPCuentaEntity copy) {
            this.CodigoAut = copy.getCodigoAut();
            this.Comision = copy.getComision();
            this.Fecha = copy.getFecha();
            this.IdBitacora = copy.getIdBitacora();
            this.Importe = copy.getImporte();
            this.Propina = copy.getPropina();
            this.ReferenciaNegocio = copy.getReferenciaNegocio();
            this.Total = copy.getTotal();
        }

        public Builder setCodigoAut(String CodigoAut) {
            this.CodigoAut = CodigoAut;
            return this;
        }

        public Builder setComision(double Comision) {
            this.Comision = Comision;
            return this;
        }

        public Builder setFecha(String Fecha) {
            this.Fecha = Fecha;
            return this;
        }

        public Builder setIdBitacora(int IdBitacora) {
            this.IdBitacora = IdBitacora;
            return this;
        }

        public Builder setImporte(double Importe) {
            this.Importe = Importe;
            return this;
        }

        public Builder setPropina(double Propina) {
            this.Propina = Propina;
            return this;
        }

        public Builder setReferenciaNegocio(String ReferenciaNegocio) {
            this.ReferenciaNegocio = ReferenciaNegocio;
            return this;
        }

        public Builder setTotal(double Total) {
            this.Total = Total;
            return this;
        }

        public SPCuentaEntity build() {
            return new SPCuentaEntity(this);
        }
    }
}
