package addcel.mobilecard.data.net.apto.accounts

import addcel.mobilecard.BuildConfig
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import timber.log.Timber
import java.util.concurrent.TimeUnit


/**
 * ADDCEL on 2019-08-01.
 */

data class CardIssuer(val issuer: String = "shift")

data class AptoError(val code: Int, val message: String)

data class AptoPinRequest(
        @SerializedName("pin")
        val pin: String
)

data class AptoCardDetails(
        @SerializedName("card_id")
        val cardId: String,
        @SerializedName("cvv")
        val cvv: String,
        @SerializedName("expiration")
        val expiration: String,
        @SerializedName("pan")
        val pan: String,
        @SerializedName("type")
        val type: String
)

interface AccountsAPI {

    companion object {
        /*
        Api-Key	Bearer <Api key>
      Authorization	Bearer <User Token>
    
          "user_id": "crdhldr_84376e78da145c38f135f104",
        "user_token": "RHPFn1pLBa5VYB6RpImqlHZ0TVvk+IbWI0ouOhU4x3bbmJMYqKwxhjCfffBf8G1/b2RETBTjL3vevgEvpWbOBlHPV6JDRm8VhsHHXOxSv1A="
    
    
         */

        private const val VERSION = "v1"

        private const val API_KEY =
                "Bearer KCR+DBqSu3M4a1+IZMCz9ffADr3EzwaBhF0WwcAQ0nVqtQlX28xZQdfoMAIcGEXI"

        fun get(baseUrl: String): AccountsAPI {
            val interceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Timber.d(message)
                }
            })

            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val builder = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS).writeTimeout(40, TimeUnit.SECONDS)

            if (BuildConfig.DEBUG) builder.addInterceptor(interceptor)

            val r = Retrofit.Builder().client(builder.build())
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()

            return r.create(AccountsAPI::class.java)
        }
    }

    @Headers("Api-Key: $API_KEY")
    @POST("$VERSION/user/accounts/issuecard")
    fun issueCard(
            @Header("Authorization") auth: String, @Body issuer: CardIssuer
    ): Observable<AptoCardData>

    @Headers("Api-Key: $API_KEY")
    @GET("$VERSION/user/accounts/{account_id}")
    fun getAccount(@Header("Authorization") auth: String, @Path("account_id") accountId: String): Observable<AptoCardData>

    @Headers("Api-Key: $API_KEY")
    @POST("$VERSION/user/accounts/{account_id}/activate")
    fun activate(
            @Header("Authorization") auth: String,
            @Path("account_id")
            accountId: String
    ): Observable<AptoCardData>

    /*$VERSION/user/accounts/\(DataUser.APTOACCOUNTID!)/transactions*/
    @Headers("Api-Key: $API_KEY")
    @GET("$VERSION/user/accounts/{account_id}/transactions")
    fun transactions(
            @Header("Authorization") auth: String,
            @Path("account_id")
            accountId: String
    ): Observable<AptoTransactionResponse>

    /*
    /$VERSION/user/accounts/\(DataUser.APTOACCOUNTID!)/balance
     */
    /*$VERSION/user/accounts/\(DataUser.APTOACCOUNTID!)/transactions*/
    @Headers("Api-Key: $API_KEY")
    @GET("$VERSION/user/accounts/{account_id}/balance")
    fun getBalance(
            @Header("Authorization") auth: String,
            @Path("account_id")
            accountId: String
    ): Observable<AptoCardBalanceEntity>

    @Headers("Api-Key: $API_KEY")
    @POST("$VERSION/user/accounts/{account_id}")
    fun enable(@Header("Authorization") auth: String, @Path("account_id") accountId: String): Observable<AptoCardData>

    @Headers("Api-Key: $API_KEY")
    @POST("$VERSION/user/accounts/{account_id}")
    fun disable(@Header("Authorization") auth: String, @Path("account_id") accountId: String): Observable<AptoCardData>

    @Headers("Api-Key: $API_KEY")
    @POST("$VERSION/user/accounts/{account_id}/pin")
    fun pin(@Header("Authorization") auth: String, @Path("account_id") accountId: String, @Body request: AptoPinRequest): Observable<AptoCardData>

    @Headers("Api-Key: $API_KEY")
    @POST("$VERSION/user/accounts/{account_id}/details")
    fun details(@Header("Authorization") auth: String, @Path("account_id") accountId: String): Observable<AptoCardDetails>

}