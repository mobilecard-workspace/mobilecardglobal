package addcel.mobilecard.data.net.telepeaje.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PagoModel {

    @SerializedName("lon")
    private double mLon;

    @SerializedName("lat")
    private double mLat;

    @SerializedName("idTarjeta")
    private int mIdTarjeta;

    @SerializedName("imei")
    private String mImei;

    @SerializedName("key")
    private String mKey;

    @SerializedName("login")
    private String mLogin;

    @SerializedName("modelo")
    private String mModelo;

    @SerializedName("pin")
    private String mPin;

    @SerializedName("producto")
    private String mProducto;

    @SerializedName("software")
    private String mSoftware;

    @SerializedName("tarjeta")
    private String mTarjeta;

    @SerializedName("tipo")
    private String mTipo;

    @SerializedName("idUsuario")
    private long mIdUsuario;

    @SerializedName("debug")
    private boolean mDebug;

    public double getLon() {
        return mLon;
    }

    public double getLat() {
        return mLat;
    }

    public int getIdTarjeta() {
        return mIdTarjeta;
    }

    public String getImei() {
        return mImei;
    }

    public String getKey() {
        return mKey;
    }

    public String getLogin() {
        return mLogin;
    }

    public String getModelo() {
        return mModelo;
    }

    public String getPin() {
        return mPin;
    }

    public String getProducto() {
        return mProducto;
    }

    public String getSoftware() {
        return mSoftware;
    }

    public String getTarjeta() {
        return mTarjeta;
    }

    public String getTipo() {
        return mTipo;
    }

    public long getIdUsuario() {
        return mIdUsuario;
    }

    public boolean isDebug() {
        return mDebug;
    }

    public static class Builder {

        private double mLon;
        private double mLat;
        private int mIdTarjeta;
        private String mImei;
        private String mKey;
        private String mLogin;
        private String mModelo;
        private String mPin;
        private String mProducto;
        private String mSoftware;
        private String mTarjeta;
        private String mTipo;
        private long mIdUsuario;
        private boolean mDebug;

        public PagoModel.Builder withLon(double lon) {
            mLon = lon;
            return this;
        }

        public PagoModel.Builder withLat(double lat) {
            mLat = lat;
            return this;
        }

        public PagoModel.Builder withIdTarjeta(int idTarjeta) {
            mIdTarjeta = idTarjeta;
            return this;
        }

        public PagoModel.Builder withImei(String imei) {
            mImei = imei;
            return this;
        }

        public PagoModel.Builder withKey(String key) {
            mKey = key;
            return this;
        }

        public PagoModel.Builder withLogin(String login) {
            mLogin = login;
            return this;
        }

        public PagoModel.Builder withModelo(String modelo) {
            mModelo = modelo;
            return this;
        }

        public PagoModel.Builder withPin(String pin) {
            mPin = pin;
            return this;
        }

        public PagoModel.Builder withProducto(String producto) {
            mProducto = producto;
            return this;
        }

        public PagoModel.Builder withSoftware(String software) {
            mSoftware = software;
            return this;
        }

        public PagoModel.Builder withTarjeta(String tarjeta) {
            mTarjeta = tarjeta;
            return this;
        }

        public PagoModel.Builder withTipo(String tipo) {
            mTipo = tipo;
            return this;
        }

        public PagoModel.Builder withIdUsuario(long idUsuario) {
            mIdUsuario = idUsuario;
            return this;
        }

        public PagoModel.Builder withDebug(boolean mDebug) {
            this.mDebug = mDebug;
            return this;
        }

        public PagoModel build() {
            PagoModel PagoModel = new PagoModel();
            PagoModel.mLon = mLon;
            PagoModel.mLat = mLat;
            PagoModel.mIdTarjeta = mIdTarjeta;
            PagoModel.mImei = mImei;
            PagoModel.mKey = mKey;
            PagoModel.mLogin = mLogin;
            PagoModel.mModelo = mModelo;
            PagoModel.mPin = mPin;
            PagoModel.mProducto = mProducto;
            PagoModel.mSoftware = mSoftware;
            PagoModel.mTarjeta = mTarjeta;
            PagoModel.mTipo = mTipo;
            PagoModel.mIdUsuario = mIdUsuario;
            PagoModel.mDebug = mDebug;
            return PagoModel;
        }
    }
}
