package addcel.mobilecard.data.net.catalogo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.List;

@SuppressWarnings("unused")
public final class ServicioResponse {

    @SerializedName("idError")
    private int mIdError;

    @SerializedName("mensajeError")
    private String mMensajeError;

    @SerializedName("servicios")
    private List<ServicioEntity> mServicios;

    public ServicioResponse(int mIdError, String mMensajeError) {
        this.mIdError = mIdError;
        this.mMensajeError = mMensajeError;
    }

    public ServicioResponse(int mIdError, String mMensajeError, List<ServicioEntity> mServicios) {
        this(mIdError, mMensajeError);
        this.mServicios = mServicios;
    }

    public int getIdError() {
        return mIdError;
    }

    public String getMensajeError() {
        return mMensajeError;
    }

    public List<ServicioEntity> getServicios() {
        return mServicios;
    }

    public static final class ServicioEntity implements Parcelable {

        public static final Creator<ServicioEntity> CREATOR = new Creator<ServicioEntity>() {
            @Override
            public ServicioEntity createFromParcel(Parcel in) {
                return new ServicioEntity(in);
            }

            @Override
            public ServicioEntity[] newArray(int size) {
                return new ServicioEntity[size];
            }
        };

        @SerializedName("categoria")
        private String mCategoria;

        @SerializedName("comision")
        private double mComision;

        @SerializedName("consultaSaldo")
        private boolean mConsultaSaldo;

        @SerializedName("emisor")
        private String mEmisor;

        @SerializedName("empresa")
        private String mEmpresa;

        @SerializedName("id")
        private int mId;

        @SerializedName("idCategoria")
        private int mIdCategoria;

        @SerializedName("importe")
        private double mImporte;

        @SerializedName("isAntad")
        private int mIsAntad;

        @SerializedName("nombreReferencia")
        private String mNombreReferencia;

        @SerializedName("operacion")
        private String mOperacion;

        @SerializedName("porcentajeComision")
        private double mPorcentajeComision;

        @SerializedName("referencia")
        private String mReferencia;

        @SerializedName("ticket")
        private String mTicket;

        @SerializedName("tipoTransaccion")
        private String mTipoTransaccion;

        protected ServicioEntity(Parcel in) {
            mCategoria = in.readString();
            mComision = in.readDouble();
            mConsultaSaldo = in.readByte() != 0;
            mEmisor = in.readString();
            mEmpresa = in.readString();
            mId = in.readInt();
            mIdCategoria = in.readInt();
            mImporte = in.readDouble();
            mIsAntad = in.readInt();
            mNombreReferencia = in.readString();
            mOperacion = in.readString();
            mPorcentajeComision = in.readDouble();
            mReferencia = in.readString();
            mTicket = in.readString();
            mTipoTransaccion = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mCategoria);
            dest.writeDouble(mComision);
            dest.writeByte((byte) (mConsultaSaldo ? 1 : 0));
            dest.writeString(mEmisor);
            dest.writeString(mEmpresa);
            dest.writeInt(mId);
            dest.writeInt(mIdCategoria);
            dest.writeDouble(mImporte);
            dest.writeInt(mIsAntad);
            dest.writeString(mNombreReferencia);
            dest.writeString(mOperacion);
            dest.writeDouble(mPorcentajeComision);
            dest.writeString(mReferencia);
            dest.writeString(mTicket);
            dest.writeString(mTipoTransaccion);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getCategoria() {
            return mCategoria;
        }

        public void setCategoria(String categoria) {
            mCategoria = categoria;
        }

        public double getComision() {
            return mComision;
        }

        public void setComision(double comision) {
            mComision = comision;
        }

        public boolean getConsultaSaldo() {
            return mConsultaSaldo;
        }

        public void setConsultaSaldo(boolean consultaSaldo) {
            mConsultaSaldo = consultaSaldo;
        }

        public String getEmisor() {
            return mEmisor;
        }

        public void setEmisor(String emisor) {
            mEmisor = emisor;
        }

        public String getEmpresa() {
            return mEmpresa;
        }

        public void setEmpresa(String empresa) {
            mEmpresa = empresa;
        }

        public int getId() {
            return mId;
        }

        public void setId(int id) {
            mId = id;
        }

        public int getIdCategoria() {
            return mIdCategoria;
        }

        public void setIdCategoria(int idCategoria) {
            mIdCategoria = idCategoria;
        }

        public double getImporte() {
            return mImporte;
        }

        public void setImporte(double importe) {
            mImporte = importe;
        }

        public int getIsAntad() {
            return mIsAntad;
        }

        public void setIsAntad(int isAntad) {
            mIsAntad = isAntad;
        }

        public String getNombreReferencia() {
            return mNombreReferencia;
        }

        public void setNombreReferencia(String nombreReferencia) {
            mNombreReferencia = nombreReferencia;
        }

        public String getOperacion() {
            return mOperacion;
        }

        public void setOperacion(String operacion) {
            mOperacion = operacion;
        }

        public double getPorcentajeComision() {
            return mPorcentajeComision;
        }

        public void setPorcentajeComision(double porcentajeComision) {
            mPorcentajeComision = porcentajeComision;
        }

        public String getReferencia() {
            return mReferencia;
        }

        public void setReferencia(String referencia) {
            mReferencia = referencia;
        }

        public String getTicket() {
            return mTicket;
        }

        public void setTicket(String ticket) {
            mTicket = ticket;
        }

        public String getTipoTransaccion() {
            return mTipoTransaccion;
        }

        public void setTipoTransaccion(String tipoTransaccion) {
            mTipoTransaccion = tipoTransaccion;
        }

        @NotNull
        @Override
        public String toString() {
            return mEmpresa;
        }
    }
}
