package addcel.mobilecard.data.net.negocios.entity;

import com.google.gson.annotations.Expose;

import java.util.List;

@SuppressWarnings("unused")
public class BankResponse {

    @Expose
    private List<BankEntity> banks;
    @Expose
    private Long idError;
    @Expose
    private String mensajeError;

    public List<BankEntity> getBanks() {
        return banks;
    }

    public Long getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public static class Builder {

        private List<BankEntity> banks;
        private Long idError;
        private String mensajeError;

        public BankResponse.Builder withBanks(List<BankEntity> banks) {
            this.banks = banks;
            return this;
        }

        public BankResponse.Builder withIdError(Long idError) {
            this.idError = idError;
            return this;
        }

        public BankResponse.Builder withMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }

        public BankResponse build() {
            BankResponse bankResponse = new BankResponse();
            bankResponse.banks = banks;
            bankResponse.idError = idError;
            bankResponse.mensajeError = mensajeError;
            return bankResponse;
        }
    }
}
