package addcel.mobilecard.data.net.catalogo;

import java.util.List;

import addcel.mobilecard.data.net.catalogo.model.CatalogoResponse;
import addcel.mobilecard.data.net.catalogo.model.EstadoResponse;
import addcel.mobilecard.data.net.catalogo.model.PaisResponse;
import addcel.mobilecard.data.net.catalogo.model.ServicioResponse;
import addcel.mobilecard.data.net.catalogo.model.ServiciosRequest;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * ADDCEL on 13/06/18.
 */
public interface CatalogoService {

    String PATH = "Catalogos/{idApp}/{idPais}/{idioma}/";

    @POST(PATH + "getServicios")
    Observable<ServicioResponse> getServicios(@Path("idApp") int idApp,
                                              @Path("idPais") int idPais, @Path("idioma") String idioma, @Body ServiciosRequest request);

    @POST(PATH + "getRecargas")
    Observable<CatalogoResponse> getRecargas(@Path("idApp") int idApp,
                                             @Path("idPais") int idPais, @Path("idioma") String idioma, @Body ServiciosRequest request);

    @POST(PATH + "getRecargaServicios")
    Observable<CatalogoResponse> getRecargaServicios(
            @Path("idApp") int idApp, @Path("idPais") int idPais, @Path("idioma") String idioma,
            @Body ServiciosRequest request);

    @POST(PATH + "getRecargaMontos")
    Observable<CatalogoResponse> getRecargaMontos(
            @Path("idApp") int idApp, @Path("idPais") int idPais, @Path("idioma") String idioma,
            @Body ServiciosRequest request);

    @POST(PATH + "getServicioCategorias")
    Observable<CatalogoResponse> getServiciosCategorias(
            @Path("idApp") int idApp, @Path("idPais") int idPais, @Path("idioma") String idioma,
            @Body ServiciosRequest request);

    @GET(PATH + "TelefonicaPaises")
    Observable<PaisResponse> getPaisesTelefonica(
            @Path("idApp") int idApp, @Path("idPais") int idPais, @Path("idioma") String idioma);

    @GET("Catalogos/{idApp}/{idioma}/getPaises")
    Observable<List<PaisResponse.PaisEntity>> getPaises(
            @Path("idApp") int idApp, @Path("idioma") String idioma);

    @GET(PATH + "estados")
    Observable<EstadoResponse> getEstados(@Path("idApp") int idApp,
                                          @Path("idPais") int idPais, @Path("idioma") String idioma);
}
