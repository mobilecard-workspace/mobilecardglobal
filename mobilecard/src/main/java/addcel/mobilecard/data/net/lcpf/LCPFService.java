package addcel.mobilecard.data.net.lcpf;

import addcel.mobilecard.data.net.lcpf.model.HthAccountRequest;
import addcel.mobilecard.data.net.lcpf.model.HthAccountResponse;
import addcel.mobilecard.data.net.lcpf.model.HthAccountUpdateRequest;
import addcel.mobilecard.data.net.lcpf.model.HthBankResponse;
import addcel.mobilecard.data.net.lcpf.model.LcpfEstablecimientoRequest;
import addcel.mobilecard.data.net.lcpf.model.LcpfEstablecimientoResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * ADDCEL on 08/11/17.
 */

public interface LCPFService {

    String ENQUEUE_PAYMENT = "LCPFServices/payworks/enqueuePayment";
    String ERROR_PREVIO = "LCPFServices/payworks/error_previo_pago";
    String AUTH_FINISHED = "LCPFServices/payworks/payworksRec3DRespuesta";
    String PROCESS_FINISHED = "LCPFServices/payworks/payworks2RecRespuesta";

    @FormUrlEncoded
    @POST("LCPFServices/toAccount")
    Call<HthAccountResponse> toAccount(
            @Field("idUsuario") long idUsuario, @Field("idioma") String idioma);

    @FormUrlEncoded
    @POST("LCPFServices/deleteAccount")
    Call<HthAccountResponse> deleteAccount(
            @Field("idUsuario") long idUsuario, @Field("idioma") String idioma,
            @Field("idAccount") int id);

    @POST("LCPFServices/{idioma}/getEstablecimientos")
    Call<LcpfEstablecimientoResponse> getEstablecimientos(@Path("idioma") String idioma,
                                                          @Body LcpfEstablecimientoRequest body);

    @POST("LCPFServices/updateAccount")
    Call<HthAccountResponse> updateAccount(
            @Body HthAccountUpdateRequest request);

    @POST("LCPFServices/enqueueAccountSignUp")
    Call<HthAccountResponse> enqueueAccountSignUp(
            @Body HthAccountRequest request);

    @GET("LCPFServices/bankCodes")
    Call<HthBankResponse> bankCodes();
}
