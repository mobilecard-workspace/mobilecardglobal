package addcel.mobilecard.data.net.catalogo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * ADDCEL on 2019-07-12.
 */

data class DeptoEntity(
        @SerializedName("codigo") val codigo: String, @SerializedName("departamento")
        val departamento: String, @SerializedName("id") val id: Int, @SerializedName("idPais")
        val idPais: Int, @SerializedName("status") val status: Int
) {
    override fun toString(): String {
        return departamento
    }
}

data class ProvinciaEntity(
        @SerializedName("codigo") val codigo: String,
        @SerializedName("codigoDepartamento") val codigoDepartamento: String, @SerializedName("id")
        val id: Int, @SerializedName("idPais") val idPais: Int, @SerializedName("provincia")
        val provincia: String, @SerializedName("status") val status: Int
) {
    override fun toString(): String {
        return provincia
    }
}

data class DistritoEntity(
        @SerializedName("codigo") val codigo: String,
        @SerializedName("codigoProvincia") val codigoProvincia: String, @SerializedName("distrito")
        val distrito: String, @SerializedName("id") val id: Int, @SerializedName("idPais")
        val idPais: Int, @SerializedName("status") val status: Int
) {
    override fun toString(): String {
        return distrito
    }
}

data class ColombiaOperadorRecargaResponse(
        @SerializedName("idError") val idError: Int,
        @SerializedName("mensajeError") val mensajeError: String, @SerializedName("operadores")
        val operadores: List<OperadorRecarga>
)

@Parcelize
data class OperadorRecarga(
        @SerializedName("codigo") val codigo: String,
        @SerializedName("id") val id: Int, @SerializedName("nombre") val nombre: String,
        @SerializedName("imgcolor") val imgColor: String, @SerializedName("imgwild")
        val imgWhite: String
) : Parcelable

data class ColombiaPaqueteDatosResponse(
        @SerializedName("categorias")
        val categorias: List<Categoria>, @SerializedName("idError") val idError: Int,
        @SerializedName("mensajeError") val mensajeError: String
)

@Parcelize
data class Categoria(
        @SerializedName("codigo") val codigo: String,
        @SerializedName("descripcion") val descripcion: String, @SerializedName("id") val id: Int,
        @SerializedName("imgcolor") val imgColor: String, @SerializedName("imgwild")
        val imgWhite: String
) : Parcelable

data class ColombiaPaqueteDatosMontoResponse(
        @SerializedName("idError") val idError: Int,
        @SerializedName("mensajeError") val mensajeError: String, @SerializedName("paquetes")
        val paquetes: List<Paquete>
)

@Parcelize
data class Paquete(
        @SerializedName("codigo") val codigo: String,
        @SerializedName("descripcion") val descripcion: String, @SerializedName("id") val id: Int,
        @SerializedName("imgcolor") val imgcolor: String, @SerializedName("imgwild")
        val imgwild: String, @SerializedName("sku") val sku: String, @SerializedName("valor")
        val valor: String
) : Parcelable

data class ColombiaTaeMontoResponse(
        @SerializedName("idError") val idError: Int,
        @SerializedName("mensajeError") val mensajeError: String, @SerializedName("montos")
        val montos: List<Double>
)

interface CatalogoAPI {

    companion object {

        const val OPERADORES_RECARGA = "getOperadoresRecarga"
        const val OPERADORES_PAQUETES = "getOperadoresPaquetes"

        fun get(retrofit: Retrofit): CatalogoAPI {
            return retrofit.create(CatalogoAPI::class.java)
        }
    }

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/getDepartamentos")
    fun getDepartamentos(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma")
            idioma: String
    ): Observable<List<DeptoEntity>>

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/getProvincias")
    fun getProvincias(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Query("codigo")
            codigo: String
    ): Observable<List<ProvinciaEntity>>

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/getDistritos")
    fun getDistritos(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Query("codigo")
            codigo: String
    ): Observable<List<DistritoEntity>>

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/puntored/{method}")
    fun getOperadoresPuntored(
            @Path("idApp") idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String,
            @Path("method") method: String
    ): Observable<ColombiaOperadorRecargaResponse>

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/puntored/getCategorias/{idOperador}")
    fun getCategoriasPuntored(
            @Path("idApp") idApp: Int, @Path("idPais") idPais: Int, @Path("idioma")
            idioma: String, @Path("idOperador") idOperador: Int
    ): Observable<ColombiaPaqueteDatosResponse>

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/puntored/getPaquetes/{idCategoria}")
    fun getPaquetesDatosMontoPuntored(
            @Path("idApp") idApp: Int, @Path("idPais") idPais: Int,
            @Path("idioma") idioma: String, @Path("idCategoria")
            idCategoria: Int
    ): Observable<ColombiaPaqueteDatosMontoResponse>

    @GET("Catalogos/{idApp}/{idPais}/{idioma}/puntored/getMontos")
    fun getTaeMontoPuntored(
            @Path("idApp") idApp: Int, @Path("idPais") idPais: Int, @Path("idioma")
            idioma: String
    ): Observable<ColombiaTaeMontoResponse>
}