package addcel.mobilecard.data.net.aci.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.MoreObjects;

import org.jetbrains.annotations.NotNull;

public class ServiceModel implements Parcelable {
    public static final Creator<ServiceModel> CREATOR = new Creator<ServiceModel>() {
        @Override
        public ServiceModel createFromParcel(Parcel in) {
            return new ServiceModel(in);
        }

        @Override
        public ServiceModel[] newArray(int size) {
            return new ServiceModel[size];
        }
    };
    private long id;
    private String name;
    private double pctComision;

    public ServiceModel() {
    }

    protected ServiceModel(Parcel in) {
        id = in.readLong();
        name = in.readString();
        pctComision = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeDouble(pctComision);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPctComision() {
        return pctComision;
    }

    public void setPctComision(double pctComision) {
        this.pctComision = pctComision;
    }

    @NotNull
    @Override
    public String toString() {
        return name;
    }

    public String toStringLog() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("pctComision", pctComision)
                .toString();
    }
}
