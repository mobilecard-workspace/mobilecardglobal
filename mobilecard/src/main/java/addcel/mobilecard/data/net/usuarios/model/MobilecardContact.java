package addcel.mobilecard.data.net.usuarios.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;

/**
 * ADDCEL on 16/11/16.
 */
public final class MobilecardContact implements Parcelable {
    public static final Creator<MobilecardContact> CREATOR = new Creator<MobilecardContact>() {
        @Override
        public MobilecardContact createFromParcel(Parcel in) {
            return new MobilecardContact(in);
        }

        @Override
        public MobilecardContact[] newArray(int size) {
            return new MobilecardContact[size];
        }
    };
    private final String nombre;
    private final String telefono;

    public MobilecardContact(String nombre, String telefono) {
        this.nombre = nombre;
        this.telefono = telefono;
    }

    protected MobilecardContact(Parcel in) {
        nombre = in.readString();
        telefono = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeString(telefono);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getTrimmedTelefono() {
        return telefono.replace("-", "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MobilecardContact that = (MobilecardContact) o;
        return Objects.equal(nombre, that.nombre) && Objects.equal(telefono, that.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(nombre, telefono);
    }

    @Override
    public String toString() {
        return nombre + " - " + telefono;
    }
}
