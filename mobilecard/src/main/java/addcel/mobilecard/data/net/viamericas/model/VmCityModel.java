package addcel.mobilecard.data.net.viamericas.model;

import org.jetbrains.annotations.NotNull;

/**
 * ADDCEL on 29/03/17.
 */

public class VmCityModel {
    /*
    {"idCity":"MT216","idState":"MEX30","nameCity":"ABASOLO","nameState":"TAMAULIPAS","__hashCodeCalc":false
     */
    private String idCity;
    private String idState;
    private String nameCity;
    private String nameState;
    private String zipCode;

    public VmCityModel() {
    }

    public VmCityModel(String idCity, String nameCity) {
        this.idCity = idCity;
        this.nameCity = nameCity;
    }

    public String getIdCity() {
        return idCity;
    }

    public String getIdState() {
        return idState;
    }

    public String getNameCity() {
        return nameCity;
    }

    public String getNameState() {
        return nameState;
    }

    public String getZipCode() {
        return zipCode;
    }

    @NotNull
    @Override
    public String toString() {
        return nameCity;
    }
}
