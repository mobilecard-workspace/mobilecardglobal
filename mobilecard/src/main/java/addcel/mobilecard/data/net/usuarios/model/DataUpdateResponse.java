package addcel.mobilecard.data.net.usuarios.model;

/**
 * ADDCEL on 27/07/17.
 */
public class DataUpdateResponse extends McResponse {
    private String value;

    public DataUpdateResponse() {
    }

    public DataUpdateResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public DataUpdateResponse(int idError, String mensajeError, String value) {
        super(idError, mensajeError);
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
