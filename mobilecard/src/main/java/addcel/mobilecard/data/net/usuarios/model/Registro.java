package addcel.mobilecard.data.net.usuarios.model;

import com.google.gson.annotations.SerializedName;

class Registro extends BaseRequest {

    @SerializedName("cedula")
    private String mCedula;
    @SerializedName("eMail")
    private String mEMail;
    @SerializedName("gemalto")
    private long mGemalto;
    @SerializedName("idBanco")
    private long mIdBanco;
    @SerializedName("idError")
    private long mIdError;
    @SerializedName("idIngo")
    private String mIdIngo;
    @SerializedName("idPais")
    private long mIdPais;
    @SerializedName("idProveedor")
    private long mIdProveedor;
    @SerializedName("idTipoTarjeta")
    private long mIdTipoTarjeta;
    @SerializedName("idUsrStatus")
    private long mIdUsrStatus;
    @SerializedName("imei")
    private String mImei;
    @SerializedName("modelo")
    private String mModelo;
    @SerializedName("newPassword")
    private String mNewPassword;
    @SerializedName("numExtStr")
    private String mNumExtStr;
    @SerializedName("operador")
    private long mOperador;
    @SerializedName("recibirSMS")
    private long mRecibirSMS;
    @SerializedName("software")
    private String mSoftware;
    @SerializedName("telefonoOriginal")
    private String mTelefonoOriginal;
    @SerializedName("tipo")
    private String mTipo;
    @SerializedName("tipoCedula")
    private long mTipoCedula;
    @SerializedName("usrApellido")
    private String mUsrApellido;
    @SerializedName("usrCalle")
    private String mUsrCalle;
    @SerializedName("usrCiudad")
    private String mUsrCiudad;
    @SerializedName("usrColonia")
    private String mUsrColonia;
    @SerializedName("usrCp")
    private String mUsrCp;
    @SerializedName("usrDireccion")
    private String mUsrDireccion;
    @SerializedName("usrDomAmex")
    private String mUsrDomAmex;
    @SerializedName("usrFechaNac")
    private String mUsrFechaNac;
    @SerializedName("usrIdEstado")
    private long mUsrIdEstado;
    @SerializedName("usrLogin")
    private String mUsrLogin;
    @SerializedName("usrMaterno")
    private String mUsrMaterno;
    @SerializedName("usrNombre")
    private String mUsrNombre;
    @SerializedName("usrNss")
    private String mUsrNss;
    @SerializedName("usrNumExt")
    private long mUsrNumExt;
    @SerializedName("usrNumInterior")
    private String mUsrNumInterior;
    @SerializedName("usrPwd")
    private String mUsrPwd;
    @SerializedName("usrSexo")
    private String mUsrSexo;
    @SerializedName("usrTdcNumero")
    private String mUsrTdcNumero;
    @SerializedName("usrTdcVigencia")
    private String mUsrTdcVigencia;
    @SerializedName("usrTdcCodigo")
    private String mUsrTdcCodigo;
    @SerializedName("usrTelCasa")
    private String mUsrTelCasa;
    @SerializedName("usrTelOficina")
    private String mUsrTelOficina;
    @SerializedName("usrTelefono")
    private String mUsrTelefono;
    @SerializedName("usrTerminos")
    private String mUsrTerminos;
    @SerializedName("wkey")
    private String mWkey;
    @SerializedName("debug")
    private boolean mDebug;

    public Registro() {
        super();
    }

    public Registro(String idioma) {
        super(idioma);
    }

    public String getCedula() {
        return mCedula;
    }

    public void setCedula(String cedula) {
        mCedula = cedula;
    }

    public String getEMail() {
        return mEMail;
    }

    public void setEMail(String eMail) {
        mEMail = eMail;
    }

    public long getGemalto() {
        return mGemalto;
    }

    public void setGemalto(long gemalto) {
        mGemalto = gemalto;
    }

    public long getIdBanco() {
        return mIdBanco;
    }

    public void setIdBanco(long idBanco) {
        mIdBanco = idBanco;
    }

    public long getIdError() {
        return mIdError;
    }

    public void setIdError(long idError) {
        mIdError = idError;
    }

    public String getIdIngo() {
        return mIdIngo;
    }

    public void setIdIngo(String idIngo) {
        mIdIngo = idIngo;
    }

    public long getIdPais() {
        return mIdPais;
    }

    public void setIdPais(long idPais) {
        mIdPais = idPais;
    }

    public long getIdProveedor() {
        return mIdProveedor;
    }

    public void setIdProveedor(long idProveedor) {
        mIdProveedor = idProveedor;
    }

    public long getIdTipoTarjeta() {
        return mIdTipoTarjeta;
    }

    public void setIdTipoTarjeta(long idTipoTarjeta) {
        mIdTipoTarjeta = idTipoTarjeta;
    }

    public long getIdUsrStatus() {
        return mIdUsrStatus;
    }

    public void setIdUsrStatus(long idUsrStatus) {
        mIdUsrStatus = idUsrStatus;
    }

    public String getImei() {
        return mImei;
    }

    public void setImei(String imei) {
        mImei = imei;
    }

    public String getModelo() {
        return mModelo;
    }

    public void setModelo(String modelo) {
        mModelo = modelo;
    }

    public String getNewPassword() {
        return mNewPassword;
    }

    public void setNewPassword(String newPassword) {
        mNewPassword = newPassword;
    }

    public String getNumExtStr() {
        return mNumExtStr;
    }

    public void setNumExtStr(String numExtStr) {
        mNumExtStr = numExtStr;
    }

    public long getOperador() {
        return mOperador;
    }

    public void setOperador(long operador) {
        mOperador = operador;
    }

    public long getRecibirSMS() {
        return mRecibirSMS;
    }

    public void setRecibirSMS(long recibirSMS) {
        mRecibirSMS = recibirSMS;
    }

    public String getSoftware() {
        return mSoftware;
    }

    public void setSoftware(String software) {
        mSoftware = software;
    }

    public String getTelefonoOriginal() {
        return mTelefonoOriginal;
    }

    public void setTelefonoOriginal(String telefonoOriginal) {
        mTelefonoOriginal = telefonoOriginal;
    }

    public String getTipo() {
        return mTipo;
    }

    public void setTipo(String tipo) {
        mTipo = tipo;
    }

    public long getTipoCedula() {
        return mTipoCedula;
    }

    public void setTipoCedula(long tipoCedula) {
        mTipoCedula = tipoCedula;
    }

    public String getUsrApellido() {
        return mUsrApellido;
    }

    public void setUsrApellido(String usrApellido) {
        mUsrApellido = usrApellido;
    }

    public String getUsrCalle() {
        return mUsrCalle;
    }

    public void setUsrCalle(String usrCalle) {
        mUsrCalle = usrCalle;
    }

    public String getUsrCiudad() {
        return mUsrCiudad;
    }

    public void setUsrCiudad(String usrCiudad) {
        mUsrCiudad = usrCiudad;
    }

    public String getUsrColonia() {
        return mUsrColonia;
    }

    public void setUsrColonia(String usrColonia) {
        mUsrColonia = usrColonia;
    }

    public String getUsrCp() {
        return mUsrCp;
    }

    public void setUsrCp(String usrCp) {
        mUsrCp = usrCp;
    }

    public String getUsrDireccion() {
        return mUsrDireccion;
    }

    public void setUsrDireccion(String usrDireccion) {
        mUsrDireccion = usrDireccion;
    }

    public String getUsrDomAmex() {
        return mUsrDomAmex;
    }

    public void setUsrDomAmex(String usrDomAmex) {
        mUsrDomAmex = usrDomAmex;
    }

    public String getUsrFechaNac() {
        return mUsrFechaNac;
    }

    public void setUsrFechaNac(String usrFechaNac) {
        mUsrFechaNac = usrFechaNac;
    }

    public long getUsrIdEstado() {
        return mUsrIdEstado;
    }

    public void setUsrIdEstado(long usrIdEstado) {
        mUsrIdEstado = usrIdEstado;
    }

    public String getUsrLogin() {
        return mUsrLogin;
    }

    public void setUsrLogin(String usrLogin) {
        mUsrLogin = usrLogin;
    }

    public String getUsrMaterno() {
        return mUsrMaterno;
    }

    public void setUsrMaterno(String usrMaterno) {
        mUsrMaterno = usrMaterno;
    }

    public String getUsrNombre() {
        return mUsrNombre;
    }

    public void setUsrNombre(String usrNombre) {
        mUsrNombre = usrNombre;
    }

    public String getUsrNss() {
        return mUsrNss;
    }

    public void setUsrNss(String usrNss) {
        mUsrNss = usrNss;
    }

    public long getUsrNumExt() {
        return mUsrNumExt;
    }

    public void setUsrNumExt(long usrNumExt) {
        mUsrNumExt = usrNumExt;
    }

    public String getUsrNumInterior() {
        return mUsrNumInterior;
    }

    public void setUsrNumInterior(String usrNumInterior) {
        mUsrNumInterior = usrNumInterior;
    }

    public String getUsrPwd() {
        return mUsrPwd;
    }

    public void setUsrPwd(String usrPwd) {
        mUsrPwd = usrPwd;
    }

    public String getUsrSexo() {
        return mUsrSexo;
    }

    public void setUsrSexo(String usrSexo) {
        mUsrSexo = usrSexo;
    }

    public String getUsrTdcNumero() {
        return mUsrTdcNumero;
    }

    public void setUsrTdcNumero(String usrTdcNumero) {
        mUsrTdcNumero = usrTdcNumero;
    }

    public String getUsrTdcVigencia() {
        return mUsrTdcVigencia;
    }

    public void setUsrTdcVigencia(String usrTdcVigencia) {
        mUsrTdcVigencia = usrTdcVigencia;
    }

    public String getUsrTelCasa() {
        return mUsrTelCasa;
    }

    public void setUsrTelCasa(String usrTelCasa) {
        mUsrTelCasa = usrTelCasa;
    }

    public String getUsrTelOficina() {
        return mUsrTelOficina;
    }

    public void setUsrTelOficina(String usrTelOficina) {
        mUsrTelOficina = usrTelOficina;
    }

    public String getUsrTelefono() {
        return mUsrTelefono;
    }

    public void setUsrTelefono(String usrTelefono) {
        mUsrTelefono = usrTelefono;
    }

    public String getUsrTerminos() {
        return mUsrTerminos;
    }

    public void setUsrTerminos(String usrTerminos) {
        mUsrTerminos = usrTerminos;
    }

    public String getWkey() {
        return mWkey;
    }

    public void setWkey(String wkey) {
        mWkey = wkey;
    }

    public String getUsrTdcCodigo() {
        return mUsrTdcCodigo;
    }

    public void setUsrTdcCodigo(String usrTdcCodigo) {
        this.mUsrTdcCodigo = usrTdcCodigo;
    }

    public boolean ismDebug() {
        return mDebug;
    }

    public void setmDebug(boolean mDebug) {
        this.mDebug = mDebug;
    }
}
