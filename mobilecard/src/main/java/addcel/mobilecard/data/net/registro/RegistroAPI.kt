package addcel.mobilecard.data.net.registro

import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 2019-07-24.
 */

enum class TipoVerificacion {
    USUARIO, COMERCIO
}

data class SmsActivationRequest(
        val phoneNumber: String, val imei: String,
        val tipo: TipoVerificacion
)

data class SmsActivationEntity(
        @SerializedName("data") val `data`: Data?, @SerializedName("code")
        val code: Int, @SerializedName("message") val message: String
)

data class Data(
        @SerializedName("code") val code: String, @SerializedName("date") val date: Long,
        @SerializedName("idSmsActivationCode") val idSmsActivationCode: Int, @SerializedName("imei")
        val imei: String, @SerializedName("phoneNumber") val phoneNumber: String,
        @SerializedName("status") val status: String
)

data class SmsVerificationRequest(
        val code: String, val phoneNumber: String, val imei: String,
        val tipo: TipoVerificacion
)

data class EmailStatusEntity(
        @SerializedName("id") val id: Long, @SerializedName("idError")
        val idError: Int, @SerializedName("mensajeError") val mensajeError: String,
        @SerializedName("status") val status: Int
)

interface RegistroAPI {
    companion object {
        const val BASE_URL = "Usuarios/{idApp}/{idPais}/{idioma}/"

        fun get(r: Retrofit): RegistroAPI {
            return r.create(RegistroAPI::class.java)
        }
    }

    @POST("sms/{idApp}/{idPais}/{idioma}/activation/code")
    fun getSmsCode(
            @Path("idApp") idApp: Int,
            @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Body
            body: SmsActivationRequest
    ): Observable<SmsActivationEntity>

    @POST("sms/{idApp}/{idPais}/{idioma}/activate/code")
    fun validateSmsCode(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Body
            body: SmsVerificationRequest
    ): Observable<SmsActivationEntity>

    @FormUrlEncoded
    @POST("$BASE_URL{tipoUsuario}/emailVerification")
    fun sendVerificationLink(
            @Path("idApp") idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String,
            @Path("tipoUsuario") tipo: TipoVerificacion, @Field("email") email: String, @Field("imei")
            imei: String, @Field("telefono") telefono: String
    ): Observable<EmailStatusEntity>

    @FormUrlEncoded
    @POST("$BASE_URL{tipoUsuario}/verify")
    fun checkIfEmailVerified(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Path("tipoUsuario")
            tipoUsuario: TipoVerificacion, @Field("id") id: Long
    ): Observable<EmailStatusEntity>
}