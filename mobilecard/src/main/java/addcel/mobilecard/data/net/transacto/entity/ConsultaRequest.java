package addcel.mobilecard.data.net.transacto.entity;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class ConsultaRequest {
    private final int idAplicacion;
    private final int idPais;
    private final String operacion;
    private final String referencia;
    private final String emisor;
    private final String idioma;

    private ConsultaRequest(Builder builder) {
        idAplicacion = builder.idAplicacion;
        idPais = builder.idPais;
        operacion = builder.operacion;
        referencia = builder.referencia;
        emisor = builder.emisor;
        idioma = builder.idioma;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ConsultaRequest copy) {
        Builder builder = new Builder();
        builder.idAplicacion = copy.getIdAplicacion();
        builder.idPais = copy.getIdPais();
        builder.operacion = copy.getOperacion();
        builder.referencia = copy.getReferencia();
        builder.emisor = copy.getEmisor();
        builder.idioma = copy.getIdioma();
        return builder;
    }

    public int getIdAplicacion() {
        return idAplicacion;
    }

    public int getIdPais() {
        return idPais;
    }

    public String getOperacion() {
        return operacion;
    }

    public String getReferencia() {
        return referencia;
    }

    public String getEmisor() {
        return emisor;
    }

    public String getIdioma() {
        return idioma;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConsultaRequest request = (ConsultaRequest) o;
        return idAplicacion == request.idAplicacion
                && idPais == request.idPais
                && Objects.equal(operacion, request.operacion)
                && Objects.equal(referencia, request.referencia)
                && Objects.equal(emisor, request.emisor)
                && Objects.equal(idioma, request.idioma);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(idAplicacion, idPais, operacion, referencia, emisor, idioma);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("idAplicacion", idAplicacion)
                .add("idPais", idPais)
                .add("operacion", operacion)
                .add("referencia", referencia)
                .add("emisor", emisor)
                .add("idioma", idioma)
                .toString();
    }

    public static final class Builder {
        private int idAplicacion;
        private int idPais;
        private String operacion;
        private String referencia;
        private String emisor;
        private String idioma;

        private Builder() {
        }

        public Builder setIdAplicacion(int idAplicacion) {
            this.idAplicacion = idAplicacion;
            return this;
        }

        public Builder setIdPais(int idPais) {
            this.idPais = idPais;
            return this;
        }

        public Builder setOperacion(String operacion) {
            this.operacion = operacion;
            return this;
        }

        public Builder setReferencia(String referencia) {
            this.referencia = referencia;
            return this;
        }

        public Builder setEmisor(String emisor) {
            this.emisor = emisor;
            return this;
        }

        public Builder setIdioma(String idioma) {
            this.idioma = idioma;
            return this;
        }

        public ConsultaRequest build() {
            return new ConsultaRequest(this);
        }
    }
}
