package addcel.mobilecard.data.net.ingo.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class IngoLoginRequest {

    @Expose
    private String deviceId;
    @Expose
    private Long idUsuario;
    @Expose
    private String plataforma;

    public String getDeviceId() {
        return deviceId;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public static class Builder {

        private final String plataforma;
        private String deviceId;
        private Long idUsuario;

        public Builder() {
            this.plataforma = "ANDROID";
        }

        public IngoLoginRequest.Builder withDeviceId(String deviceId) {
            this.deviceId = deviceId;
            return this;
        }

        public IngoLoginRequest.Builder withIdUsuario(Long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public IngoLoginRequest build() {
            IngoLoginRequest ingoLoginRequest = new IngoLoginRequest();
            ingoLoginRequest.deviceId = deviceId;
            ingoLoginRequest.idUsuario = idUsuario;
            ingoLoginRequest.plataforma = plataforma;
            return ingoLoginRequest;
        }
    }
}
