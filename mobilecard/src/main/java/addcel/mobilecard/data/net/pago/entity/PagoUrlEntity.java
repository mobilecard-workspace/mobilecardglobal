package addcel.mobilecard.data.net.pago.entity;

import android.os.Parcel;
import android.os.Parcelable;

public final class PagoUrlEntity implements Parcelable {
    private final String inicioUrl;
    private final String formUrl;
    private final String securityErrorUrl;
    private final String successUrl;

    private PagoUrlEntity(Builder builder) {
        inicioUrl = builder.inicioUrl;
        formUrl = builder.formUrl;
        securityErrorUrl = builder.securityErrorUrl;
        successUrl = builder.successUrl;
    }

    public String getInicioUrl() {
        return inicioUrl;
    }

    public String getFormUrl() {
        return formUrl;
    }

    public String getSecurityErrorUrl() {
        return securityErrorUrl;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public static final class Builder {
        private String inicioUrl;
        private String formUrl;
        private String securityErrorUrl;
        private String successUrl;

        public Builder() {
        }

        public Builder setInicioUrl(String val) {
            inicioUrl = val;
            return this;
        }

        public Builder setFormUrl(String val) {
            formUrl = val;
            return this;
        }

        public Builder setSecurityErrorUrl(String val) {
            securityErrorUrl = val;
            return this;
        }

        public Builder setSuccessUrl(String val) {
            successUrl = val;
            return this;
        }

        public PagoUrlEntity build() {
            return new PagoUrlEntity(this);
        }
    }

    private PagoUrlEntity(Parcel in) {
        inicioUrl = in.readString();
        formUrl = in.readString();
        securityErrorUrl = in.readString();
        successUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(inicioUrl);
        dest.writeString(formUrl);
        dest.writeString(securityErrorUrl);
        dest.writeString(successUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PagoUrlEntity> CREATOR = new Creator<PagoUrlEntity>() {
        @Override
        public PagoUrlEntity createFromParcel(Parcel in) {
            return new PagoUrlEntity(in);
        }

        @Override
        public PagoUrlEntity[] newArray(int size) {
            return new PagoUrlEntity[size];
        }
    };
}
