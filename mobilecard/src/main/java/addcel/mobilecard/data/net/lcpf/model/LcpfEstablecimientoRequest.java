package addcel.mobilecard.data.net.lcpf.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LcpfEstablecimientoRequest {

    @SerializedName("lat")
    private double Lat;
    @SerializedName("lon")
    private double Lon;
    @SerializedName("ubicacion_actual")
    private String UbicacionActual;

    public double getLat() {
        return Lat;
    }

    public double getLon() {
        return Lon;
    }

    public String getUbicacionActual() {
        return UbicacionActual;
    }

    public static class Builder {

        private double Lat;
        private double Lon;
        private String UbicacionActual;

        public LcpfEstablecimientoRequest.Builder withLat(double lat) {
            Lat = lat;
            return this;
        }

        public LcpfEstablecimientoRequest.Builder withLon(double lon) {
            Lon = lon;
            return this;
        }

        public LcpfEstablecimientoRequest.Builder withUbicacionActual(String ubicacionActual) {
            UbicacionActual = ubicacionActual;
            return this;
        }

        public LcpfEstablecimientoRequest build() {
            LcpfEstablecimientoRequest LcpfEstablecimientoRequest = new LcpfEstablecimientoRequest();
            LcpfEstablecimientoRequest.Lat = Lat;
            LcpfEstablecimientoRequest.Lon = Lon;
            LcpfEstablecimientoRequest.UbicacionActual = UbicacionActual;
            return LcpfEstablecimientoRequest;
        }
    }
}
