package addcel.mobilecard.data.net.aci;

import java.util.List;

import addcel.mobilecard.data.net.aci.model.Address;
import addcel.mobilecard.data.net.aci.model.CheckDataRequest;
import addcel.mobilecard.data.net.aci.model.NoDataPagoRequest;
import addcel.mobilecard.data.net.aci.model.PagoRequest;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

/**
 * ADDCEL on 22/03/17.
 */
public interface AciService {

    @Streaming
    @GET("AciBridgeWS/getServices")
    Observable<ResponseBody> get();

    @GET("AciBridgeWS/{id_service}/addresses")
    Observable<List<Address>> getAddresses(
            @Path("id_service") int id);

    @POST("AciBridgeWS/CheckUserData")
    Observable<McResponse> checkUser(
            @Body CheckDataRequest request);

    @POST("AciBridgeWS/paymentNotData")
    Observable<McResponse> paymentNotData(
            @Body NoDataPagoRequest request);

    @POST("AciBridgeWS/payment")
    Observable<McResponse> payment(@Body PagoRequest request);
}
