package addcel.mobilecard.data.net.viamericas.model;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 05/04/17.
 */

public final class LocationResponse extends McResponse {
    private List<LocationModel> locations;

    public LocationResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public List<LocationModel> getLocations() {
        return locations;
    }
}
