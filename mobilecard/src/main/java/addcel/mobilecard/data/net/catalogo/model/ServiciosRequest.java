package addcel.mobilecard.data.net.catalogo.model;

/**
 * ADDCEL on 13/06/18.
 */
public final class ServiciosRequest {
    private static ServiciosRequest emptyInstance;
    private final int idAplicacion;
    private final int tipoConsulta;
    private final int idServicio;
    private final int idCategoria;
    private final String nombreReferencia;
    private final int idRecarga;
    private final String idioma;
    private final int idPais;

    private ServiciosRequest(Builder builder) {
        idAplicacion = builder.idAplicacion;
        tipoConsulta = builder.tipoConsulta;
        idServicio = builder.idServicio;
        idCategoria = builder.idCategoria;
        nombreReferencia = builder.nombreReferencia;
        idRecarga = builder.idRecarga;
        idioma = builder.idioma;
        idPais = builder.idPais;
    }

    public static ServiciosRequest empty() {
        if (emptyInstance == null) {
            emptyInstance = new ServiciosRequest.Builder().build();
        }
        return emptyInstance;
    }

    public int getIdAplicacion() {
        return idAplicacion;
    }

    public int getTipoConsulta() {
        return tipoConsulta;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public String getNombreReferencia() {
        return nombreReferencia;
    }

    public int getIdRecarga() {
        return idRecarga;
    }

    public String getIdioma() {
        return idioma;
    }

    public static class Builder {
        private int idAplicacion;
        private int tipoConsulta;
        private int idServicio;
        private int idCategoria;
        private String nombreReferencia;
        private int idRecarga;
        private String idioma;
        private int idPais;

        public Builder setIdAplicacion(int idAplicacion) {
            this.idAplicacion = idAplicacion;
            return this;
        }

        public Builder setTipoConsulta(int tipoConsulta) {
            this.tipoConsulta = tipoConsulta;
            return this;
        }

        public Builder setIdServicio(int idServicio) {
            this.idServicio = idServicio;
            return this;
        }

        public Builder setIdCategoria(int idCategoria) {
            this.idCategoria = idCategoria;
            return this;
        }

        public Builder setNombreReferencia(String nombreReferencia) {
            this.nombreReferencia = nombreReferencia;
            return this;
        }

        public Builder setIdRecarga(int idRecarga) {
            this.idRecarga = idRecarga;
            return this;
        }

        public Builder setIdioma(String idioma) {
            this.idioma = idioma;
            return this;
        }

        public Builder setIdPais(int idPais) {
            this.idPais = idPais;
            return this;
        }

        public ServiciosRequest build() {
            return new ServiciosRequest(this);
        }
    }
}
