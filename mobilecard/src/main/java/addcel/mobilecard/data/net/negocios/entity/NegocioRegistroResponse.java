package addcel.mobilecard.data.net.negocios.entity;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 12/4/18.
 */
public final class NegocioRegistroResponse {

    /**
     * idError : 0
     * mensajeError : Se ha registrado con éxito, se envió un correo electrónico para confirmar
     * registro.
     * id : 0
     */

    @SerializedName("idError")
    private final int idError;
    @SerializedName("mensajeError")
    private final String mensajeError;
    @SerializedName("id")
    private final int id;

    private NegocioRegistroResponse(Builder builder) {
        idError = builder.idError;
        mensajeError = builder.mensajeError;
        id = builder.id;
    }

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public int getId() {
        return id;
    }

    public static final class Builder {
        private int idError;
        private String mensajeError;
        private int id;

        public Builder() {
        }

        public Builder(NegocioRegistroResponse copy) {
            this.idError = copy.getIdError();
            this.mensajeError = copy.getMensajeError();
            this.id = copy.getId();
        }

        public Builder setIdError(int idError) {
            this.idError = idError;
            return this;
        }

        public Builder setMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public NegocioRegistroResponse build() {
            return new NegocioRegistroResponse(this);
        }
    }
}
