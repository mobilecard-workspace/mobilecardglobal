package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * ADDCEL on 25/03/17.
 */

public final class RecipientModel implements Parcelable {
    public static final Creator<RecipientModel> CREATOR = new Creator<RecipientModel>() {
        @Override
        public RecipientModel createFromParcel(Parcel in) {
            return new RecipientModel(in);
        }

        @Override
        public RecipientModel[] newArray(int size) {
            return new RecipientModel[size];
        }
    };
    //{address, email, idCity, idCountry, lastName, idSender, idState, firstName}
    private final String address;
    private final String email;
    private final String idCity;
    private final String idCountry;
    private final String lastName;
    private final String idSender;
    private final String idState;
    private final String firstName;
    private final String MName;
    private final String LName;
    private final String SLName;
    private final String address2;
    private final String phone1;
    private final String ZIP;
    private final String birthDate;

    private RecipientModel(Builder builder) {
        this.address = builder.address;
        this.email = builder.email;
        this.idCity = builder.idCity;
        this.idCountry = builder.idCountry;
        this.lastName = builder.lastName;
        this.idSender = builder.idSender;
        this.idState = builder.idState;
        this.firstName = builder.firstName;
        this.MName = builder.MName;
        this.LName = builder.LName;
        this.SLName = builder.SLName;
        this.address2 = builder.address2;
        this.phone1 = builder.phone1;
        this.ZIP = builder.ZIP;
        this.birthDate = builder.birthDate;
    }

    private RecipientModel(Parcel in) {
        address = in.readString();
        email = in.readString();
        idCity = in.readString();
        idCountry = in.readString();
        lastName = in.readString();
        idSender = in.readString();
        idState = in.readString();
        firstName = in.readString();
        MName = in.readString();
        LName = in.readString();
        SLName = in.readString();
        address2 = in.readString();
        phone1 = in.readString();
        ZIP = in.readString();
        birthDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeString(email);
        dest.writeString(idCity);
        dest.writeString(idCountry);
        dest.writeString(lastName);
        dest.writeString(idSender);
        dest.writeString(idState);
        dest.writeString(firstName);
        dest.writeString(MName);
        dest.writeString(LName);
        dest.writeString(SLName);
        dest.writeString(address2);
        dest.writeString(phone1);
        dest.writeString(ZIP);
        dest.writeString(birthDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static class Builder {
        private String address;
        private String email;
        private String idCity;
        private String idCountry;
        private String lastName;
        private String idSender;
        private String idState;
        private String firstName;
        private String MName;
        private String LName;
        private String SLName;
        private String address2;
        private String phone1;
        private String ZIP;
        private String birthDate;

        public Builder() {
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setIdCity(String idCity) {
            this.idCity = idCity;
            return this;
        }

        public Builder setIdCountry(String idCountry) {
            this.idCountry = idCountry;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setIdSender(String idSender) {
            this.idSender = idSender;
            return this;
        }

        public Builder setIdState(String idState) {
            this.idState = idState;
            return this;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setMName(String MName) {
            this.MName = MName;
            return this;
        }

        public Builder setLName(String LName) {
            this.LName = LName;
            return this;
        }

        public Builder setSLName(String SLName) {
            this.SLName = SLName;
            return this;
        }

        public Builder setAddress2(String address2) {
            this.address2 = address2;
            return this;
        }

        public Builder setPhone1(String phone1) {
            this.phone1 = phone1;
            return this;
        }

        public Builder setZIP(String ZIP) {
            this.ZIP = ZIP;
            return this;
        }

        public Builder setBirthDate(String birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public RecipientModel build() {
            return new RecipientModel(this);
        }
    }
}
