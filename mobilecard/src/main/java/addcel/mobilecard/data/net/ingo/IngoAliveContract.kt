package addcel.mobilecard.data.net.ingo

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.GET

/**
 * ADDCEL on 2019-05-02.
 */
data class IngoAliveResponse(val message: String, val serviceAvailable: Boolean)

/**
 * Cliente para enviar peticiones desde el movil directo al servidor de INGO
 */
interface IngoAliveAPI {

    /**
     *
     */
    @GET("Ingo.Server.Status.Service/api/SystemAvailability")
    fun systemAvailability(): Observable<IngoAliveResponse>

    companion object {
        fun get(retrofit: Retrofit): IngoAliveAPI {
            return retrofit.create(IngoAliveAPI::class.java)
        }
    }
}