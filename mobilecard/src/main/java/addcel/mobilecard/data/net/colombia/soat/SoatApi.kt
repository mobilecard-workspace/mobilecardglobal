package addcel.mobilecard.data.net.colombia.soat

import com.google.gson.annotations.SerializedName

/**
 * ADDCEL on 10/1/19.
 */

data class SoatInfoRequest(
        @SerializedName("idApp") val idApp: Int, @SerializedName("idPais")
        val idPais: Int, @SerializedName("idUsuario") val idUsuario: Long, @SerializedName("idioma")
        val idioma: String, @SerializedName("placa") val placa: String
)

data class SoatInfoEntity(
        @SerializedName("aaaa_modelo") val aaaaModelo: Int,
        @SerializedName("claseVehiculo") val claseVehiculo: String, @SerializedName("cnt_cc")
        val cntCc: Int, @SerializedName("cnt_ocupantes") val cntOcupantes: Int,
        @SerializedName("cnt_toneladas") val cntToneladas: Double, @SerializedName("codClaseSise")
        val codClaseSise: Int, @SerializedName("cod_destino") val codDestino: Int,
        @SerializedName("codLineaSise") val codLineaSise: Int, @SerializedName("codMarcaSise")
        val codMarcaSise: String, @SerializedName("codigo") val codigo: Int, @SerializedName("color")
        val color: String, @SerializedName("divipola") val divipola: String,
        @SerializedName("estadoDelVehiculo") val estadoDelVehiculo: String,
        @SerializedName("homologaciones") val homologaciones: Homologaciones,
        @SerializedName("idClaseVehiculo") val idClaseVehiculo: Int, @SerializedName("idColor")
        val idColor: Int, @SerializedName("idConsulta") val idConsulta: String,
        @SerializedName("idLinea") val idLinea: Int, @SerializedName("idMarca") val idMarca: Int,
        @SerializedName("idTipoCarroceria") val idTipoCarroceria: Int,
        @SerializedName("idTipoCombustible") val idTipoCombustible: Int,
        @SerializedName("idTipoServicio") val idTipoServicio: Int, @SerializedName("linea")
        val linea: String, @SerializedName("marca") val marca: String, @SerializedName("mensaje")
        val mensaje: String, @SerializedName("noChasis") val noChasis: String,
        @SerializedName("noMotor") val noMotor: Int, @SerializedName("noVin") val noVin: String,
        @SerializedName("observacionesServicio") val observacionesServicio: String,
        @SerializedName("organismoTransito") val organismoTransito: String,
        @SerializedName("pesoBrutoVehicular") val pesoBrutoVehicular: Int,
        @SerializedName("snConsultaRuntExitosa") val snConsultaRuntExitosa: String,
        @SerializedName("tipoCarroceria") val tipoCarroceria: String, @SerializedName("tipoCombustible")
        val tipoCombustible: String, @SerializedName("tipoServicio") val tipoServicio: String
)

data class Homologaciones(
        @SerializedName("clase") val clase: Clase,
        @SerializedName("codDestinoSise") val codDestinoSise: Int, @SerializedName("codLineaSise")
        val codLineaSise: Int, @SerializedName("codMarcaSise") val codMarcaSise: String
)

data class Clase(
        @SerializedName("codClase") val codClase: Int,
        @SerializedName("codTipoVehMinTrans") val codTipoVehMinTrans: Int, @SerializedName("txtDesc")
        val txtDesc: String
)

