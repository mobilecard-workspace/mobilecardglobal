package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 05/04/17.
 */

public final class RecipientsResponse extends McResponse implements Parcelable {
    public static final Creator<RecipientsResponse> CREATOR = new Creator<RecipientsResponse>() {
        @Override
        public RecipientsResponse createFromParcel(Parcel in) {
            return new RecipientsResponse(in);
        }

        @Override
        public RecipientsResponse[] newArray(int size) {
            return new RecipientsResponse[size];
        }
    };
    private List<RecipientResponse> recipients;

    public RecipientsResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    private RecipientsResponse(Parcel in) {
        super(in);
        recipients = in.createTypedArrayList(RecipientResponse.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(recipients);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<RecipientResponse> getSenders() {
        return recipients;
    }

    public List<RecipientResponse> getRecipients() {
        return recipients;
    }
}
