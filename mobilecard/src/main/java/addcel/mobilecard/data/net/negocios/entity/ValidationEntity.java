package addcel.mobilecard.data.net.negocios.entity;

import com.google.gson.annotations.SerializedName;

import addcel.mobilecard.data.net.wallet.CardEntity;

/**
 * ADDCEL on 12/3/18.
 */
public final class ValidationEntity {

    /**
     * idError : -1000
     * mensajeError : Error inesperado, vuelva a intentarlo
     * id : 140
     * estatus : 99
     * email_contacto : elmans8+pe@gmail.com
     * telefono_contacto : 5518307722
     * nombre :
     * paterno :
     * materno :
     * accountId :
     * card : null
     * jumioStatus : 0
     * jumioReference :
     */

    @SerializedName("idError")
    private int idError;
    @SerializedName("mensajeError")
    private String mensajeError;
    @SerializedName("id")
    private int id;
    @SerializedName("estatus")
    private int estatus;
    @SerializedName("email_contacto")
    private String emailContacto;
    @SerializedName("telefono_contacto")
    private String telefonoContacto;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("paterno")
    private String paterno;
    @SerializedName("materno")
    private String materno;
    @SerializedName("accountId")
    private String accountId;
    @SerializedName("card")
    private CardEntity card;
    @SerializedName("jumioStatus")
    private String jumioStatus;
    @SerializedName("jumioReference")
    private String jumioReference;
    @SerializedName("idPais")
    private int idPais;

    private ValidationEntity(Builder builder) {
        setIdError(builder.idError);
        setMensajeError(builder.mensajeError);
        setId(builder.id);
        setEstatus(builder.estatus);
        setEmailContacto(builder.emailContacto);
        setTelefonoContacto(builder.telefonoContacto);
        setNombre(builder.nombre);
        setPaterno(builder.paterno);
        setMaterno(builder.materno);
        setAccountId(builder.accountId);
        setCard(builder.card);
        setJumioStatus(builder.jumioStatus);
        setJumioReference(builder.jumioReference);
        setIdPais(builder.idPais);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ValidationEntity copy) {
        Builder builder = new Builder();
        builder.idError = copy.getIdError();
        builder.mensajeError = copy.getMensajeError();
        builder.id = copy.getId();
        builder.estatus = copy.getEstatus();
        builder.emailContacto = copy.getEmailContacto();
        builder.telefonoContacto = copy.getTelefonoContacto();
        builder.nombre = copy.getNombre();
        builder.paterno = copy.getPaterno();
        builder.materno = copy.getMaterno();
        builder.accountId = copy.getAccountId();
        builder.card = copy.getCard();
        builder.jumioStatus = copy.getJumioStatus();
        builder.jumioReference = copy.getJumioReference();
        builder.idPais = copy.getIdPais();
        return builder;
    }

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public CardEntity getCard() {
        return card;
    }

    public void setCard(CardEntity card) {
        this.card = card;
    }

    public String getJumioStatus() {
        return jumioStatus;
    }

    public void setJumioStatus(String jumioStatus) {
        this.jumioStatus = jumioStatus;
    }

    public String getJumioReference() {
        return jumioReference;
    }

    public void setJumioReference(String jumioReference) {
        this.jumioReference = jumioReference;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public static final class Builder {
        private int idError;
        private String mensajeError;
        private int id;
        private int estatus;
        private String emailContacto;
        private String telefonoContacto;
        private String nombre;
        private String paterno;
        private String materno;
        private String accountId;
        private CardEntity card;
        private String jumioStatus;
        private String jumioReference;
        private int idPais;

        public Builder() {
        }

        public Builder setIdError(int idError) {
            this.idError = idError;
            return this;
        }

        public Builder setMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setEstatus(int estatus) {
            this.estatus = estatus;
            return this;
        }

        public Builder setEmailContacto(String emailContacto) {
            this.emailContacto = emailContacto;
            return this;
        }

        public Builder setTelefonoContacto(String telefonoContacto) {
            this.telefonoContacto = telefonoContacto;
            return this;
        }

        public Builder setNombre(String nombre) {
            this.nombre = nombre;
            return this;
        }

        public Builder setPaterno(String paterno) {
            this.paterno = paterno;
            return this;
        }

        public Builder setMaterno(String materno) {
            this.materno = materno;
            return this;
        }

        public Builder setAccountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder setCard(CardEntity card) {
            this.card = card;
            return this;
        }

        public Builder setJumioStatus(String jumioStatus) {
            this.jumioStatus = jumioStatus;
            return this;
        }

        public Builder setJumioReference(String jumioReference) {
            this.jumioReference = jumioReference;
            return this;
        }

        public Builder setIdPais(int idPais) {
            this.idPais = idPais;
            return this;
        }

        public ValidationEntity build() {
            return new ValidationEntity(this);
        }
    }
}
