package addcel.mobilecard.data.net.hth.model;

/**
 * ADDCEL on 10/01/18.
 */
public final class HthAccountUpdateRequest {
    private final long idUsuario;
    private final long idAccount;
    private final String telefono;
    private final String alias;
    private final String email;
    private final String idioma;

    private HthAccountUpdateRequest(Builder builder) {
        idUsuario = builder.idUsuario;
        idAccount = builder.idAccount;
        telefono = builder.telefono;
        alias = builder.alias;
        email = builder.email;
        idioma = builder.idioma;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public long getIdAccount() {
        return idAccount;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getAlias() {
        return alias;
    }

    public String getEmail() {
        return email;
    }

    public String getIdioma() {
        return idioma;
    }

    public static class Builder {
        private long idUsuario;
        private long idAccount;
        private String telefono;
        private String alias;
        private String email;
        private String idioma;

        public Builder setIdUsuario(long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public Builder setIdAccount(long idAccount) {
            this.idAccount = idAccount;
            return this;
        }

        public Builder setTelefono(String telefono) {
            this.telefono = telefono;
            return this;
        }

        public Builder setAlias(String alias) {
            this.alias = alias;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setIdioma(String idioma) {
            this.idioma = idioma;
            return this;
        }

        public HthAccountUpdateRequest build() {
            return new HthAccountUpdateRequest(this);
        }
    }
}
