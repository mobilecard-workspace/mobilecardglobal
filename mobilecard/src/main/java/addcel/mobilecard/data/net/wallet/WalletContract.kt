package addcel.mobilecard.data.net.wallet

import addcel.mobilecard.data.net.usuarios.model.McResponse
import android.os.Parcelable
import android.text.TextUtils
import com.google.common.collect.Lists
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 2019-05-22.
 */

/**
 *  "balance": 0.0,
"clabe": null,
"codigo": "4TKnncjkrG3QYs63wn4GlQ==",
"cpAmex": "62270",
"date": null,
"determinada": true,
"domAmex": "El Reloj 69 ",
"idTarjeta": 626,
"img_full": "http://199.231.160.203/images/cards/full/Citibanamex.png",
"img_short": "http://199.231.160.203/images/cards/short/Citibanamex.png",
"mobilecard": false,
"movements": [],
"nombre": "Carlos Garcia Sanoja",
"pan": "zrISZawICBbk8nGMXsd6Aw==",
"phoneNumberActivation": "",
"previvale": false,
"tipo": "MasterCard",
"tipoTarjeta": "CREDITO",
"vigencia": "9WUiKlCber8VGgM3gW9LYg=="
 */
@Parcelize
data class CardEntity(
        val balance: Double, val clabe: String? = "",
        val codigo: String? = "", val cpAmex: String? = "", val date: String? = "",
        val determinada: Boolean, val domAmex: String? = "", val idTarjeta: Int,
        val mobilecard: Boolean, val movements: List<MovementEntity>? = ArrayList(),
        val nombre: String? = "", val pan: String? = "", val phoneNumberActivation: String? = "",
        val previvale: Boolean, val tipo: FranquiciaEntity? = FranquiciaEntity.VISA,
        val tipoTarjeta: TipoTarjetaEntity? = TipoTarjetaEntity.CREDITO, val vigencia: String? = "",
        @SerializedName("img_short") val imgShort: String? = "", @SerializedName("img_full")
        val imgFull: String? = "", @SerializedName("account_id") val accountId: String? = ""
) :
        Parcelable {
    companion object {

        fun EMPTY(): CardEntity {
            return CardEntity(
                    0.0, "", "", "", "", false, "", 0, false, Lists.newArrayList(), "", "", "",
                    false, FranquiciaEntity.VISA, TipoTarjetaEntity.CREDITO, "", "", ""
            )
        }

        fun PRESENTE(
                name: String,
                pan: String,
                vigencia: String,
                codigo: String,
                franquicia: FranquiciaEntity,
                tipoTarjeta: TipoTarjetaEntity
        ): CardEntity {
            return CardEntity(
                    0.0, "", codigo, "", "", false, "", 0, false, Lists.newArrayList(), name, pan, "",
                    false, franquicia, tipoTarjeta, vigencia, "", ""
            )
        }

        fun getVigenciaAsArray(cleanVigencia: String): Array<String> {
            return TextUtils.split(cleanVigencia, "/")
        }
    }
}

data class TebcaAltaRequest(
        @SerializedName("cedula") val cedula: String, @SerializedName("codigo")
        val codigo: String, @SerializedName("digito_verificador") val digitoVerificador: String,
        @SerializedName("eMail") val eMail: String, @SerializedName("id_usuario") val idUsuario: Long,
        @SerializedName("pan") val pan: String, @SerializedName("tipocedula") val tipocedula: String,
        @SerializedName("usr_apellido") val usrApellido: String, @SerializedName("usr_cargo_publico")
        val usrCargoPublico: String, @SerializedName("usr_centro_laboral") val usrCentroLaboral: String,
        @SerializedName("usr_departamento") val usrDepartamento: String,
        @SerializedName("usr_direccion") val usrDireccion: String, @SerializedName("usr_distrito")
        val usrDistrito: String, @SerializedName("usr_fecha_nac") val usrFechaNac: String,
        @SerializedName("usr_institucion") val usrInstitucion: String,
        @SerializedName("usr_nacionalidad") val usrNacionalidad: String, @SerializedName("usr_nombre")
        val usrNombre: String, @SerializedName("usr_ocupacion") val usrOcupacion: String,
        @SerializedName("usr_provincia") val usrProvincia: String, @SerializedName("usr_sexo")
        val usrSexo: String, @SerializedName("usr_telefono") val usrTelefono: String,
        @SerializedName("vigencia") val vigencia: String
)

@Parcelize
data class TransactionCard(
        @SerializedName("balance") val balance: Double,
        @SerializedName("clabe") val clabe: String, @SerializedName("codigo") val codigo: String,
        @SerializedName("cpAmex") val cpAmex: String, @SerializedName("date") val date: String,
        @SerializedName("determinada") val determinada: Boolean, @SerializedName("domAmex")
        val domAmex: String, @SerializedName("idTarjeta") val idTarjeta: Int,
        @SerializedName("img_full") val imgFull: String, @SerializedName("img_short")
        val imgShort: String, @SerializedName("mobilecard") val mobilecard: Boolean,
        @SerializedName("nombre") val nombre: String, @SerializedName("pan") val pan: String,
        @SerializedName("phoneNumberActivation") val phoneNumberActivation: String,
        @SerializedName("previvale") val previvale: Boolean, @SerializedName("tipo") val tipo: String,
        @SerializedName("tipoTarjeta") val tipoTarjeta: String, @SerializedName("vigencia")
        val vigencia: String
) : Parcelable

data class CardRequest(
        val idTarjeta: Int,
        val idUsuario: Long,
        val pan: String,
        val vigencia: String,
        val codigo: String,
        val nombre: String,
        val determinada: Boolean,
        val domAmex: String,
        val cpAmex: String,
        val isMobilecard: Boolean,
        val tipoTarjeta: TipoTarjetaEntity,
        val idioma: String,
        val addressUsa: String,
        val cityUsa: String,
        val stateUsa: Int,
        val zipCodeUsa: String
)

data class DefaultRequest(val idUser: Long, val idCard: Int)

data class CardResponse(
        val idError: Int, val mensajeError: String, val hasMobilecard: Boolean,
        val tarjetas: List<CardEntity>
)

enum class FranquiciaEntity(val description: String) {
    VISA("Visa"), MasterCard("Mastercard"), AmEx("American Express"),
    Amex("American express"), Carnet("Carnet")
}

data class MobileCardRequest(
        val idUsuario: Long, val ssn: String, val direccion: String,
        val estado: String, val ciudad: String, val cp: String,
        val fechaNac: String, //(09261983)MMDDYYYY
        val govtId: String, val govtIdIssueDate: String, val govtIdExpirationDate: String,
        val govtIdIssueState: String, val idioma: String
)

data class MobileCardResponse(val idError: Int, val mensajeError: String, val card: CardEntity)

@Parcelize
data class MovementEntity(
        val id: Int, val total: Double, val ticket: String,
        val date: String
) : Parcelable

data class MovementsResponse(
        val idError: Int, val mensajeError: String, @SerializedName("today")
        val fromToday: List<MovementEntity>, @SerializedName("week") val fromWeek: List<MovementEntity>,
        @SerializedName("month") val fromMonth: List<MovementEntity>, @SerializedName("previous_month")
        val fromPreviousMonth: List<MovementEntity>
)

data class TransactionResponse(
        @SerializedName("data") val `data`: List<TransactionsPerMonth>,
        @SerializedName("idError") val idError: Int, @SerializedName("mensajeError")
        val mensajeError: String
)

data class TransactionsPerMonth(
        @SerializedName("mes") val mes: String,
        @SerializedName("transacciones") val transacciones: List<Transaction>
)

@Parcelize
data class Transaction(
        @SerializedName("comision") val comision: Double,
        @SerializedName("date") val date: String, @SerializedName("id") val id: String,
        @SerializedName("importe") val importe: Double, @SerializedName("status") val status: Int,
        @SerializedName("ticket") val ticket: String, @SerializedName("total") val total: Double,
        @SerializedName("card") val card: TransactionCard, @SerializedName("idPais") val idPais: Int,
        @SerializedName("currency") val currency: String) : Parcelable

data class PrevivaleUserRequest(
        @SerializedName("id_usuario") val idUsuario: Long,
        @SerializedName("usr_nombre") val nombre: String, val paterno: String, val materno: String,
        @SerializedName("usr_direccion") val usrDireccion: String, @SerializedName("usr_colonia")
        val colonia: String, @SerializedName("usr_ciudad") val ciudad: String, @SerializedName("usr_cp")
        val usrCp: String, @SerializedName("usr_id_estado") val usrIdEstado: Int, @SerializedName("rfc")
        val rfc: String, @SerializedName("curp") val curp: String, @SerializedName("id_aplicacion")
        val idAplicacion: Int, @SerializedName("pan") val pan: String
)

enum class TipoTarjetaEntity(val symbol: String) {
    CREDITO("K"), DEBITO("D"), CUENTA_BANCARIA("D")
}


/*
{
    "code": 0,
    "id_user": 8923233104182,
    "id_usuario_apto": "crdhldr_0c42006b04c99a8039c9420f",
    "id_verification_1": "entity_d0cc5c75a7aeee78",
    "id_verification_2": "entity_63f64a945dbcf366",
    "mensaje": "Consulta exitosa",
    "token": "Rd72kC0DsBTrHdntJVhj8ep+/jT5I/7wrS97dyHN5U66UxidWu/gP2Z3IpFcJV4Agi2n2eJxfs9LjpPe31BcTg/VvZMHlGJ+3gzZl1HgrZk="
}
 */
@Parcelize
data class AptoResponse(
        @SerializedName("code") val code: Int, @SerializedName("id_user")
        val idUser: Long, @SerializedName("id_usuario_apto") val idUsuarioApto: String,
        @SerializedName("id_verification_1") val idVerification1: String,
        @SerializedName("id_verification_2") val idVerification2: String, @SerializedName("mensaje")
        val mensaje: String, @SerializedName("token") val token: String
) : Parcelable

/*
["id_user":DataUser.USERIDD!,
"id_verification_1":DataUser.VERIFICATIONID_SMS!,
"id_verification_2":DataUser.VERIFICATIONID_MAIL!,
"token":DataUser.APTOTOKEN!,"id_usuario_apto":DataUser.IDUSERAPTO!]
 */
data class AptoUserRequest(
        @SerializedName("id_user")
        val idUser: Long,
        @SerializedName("id_usuario_apto")
        val idUsuarioApto: String,
        @SerializedName("id_verification_1")
        val idVerification1: String,
        @SerializedName("id_verification_2")
        val idVerification2: String,
        @SerializedName("token")
        val token: String
)

data class AptoCardRequest(
        @SerializedName("account_id")
        val accountId: String,
        @SerializedName("id_user")
        val idUser: Long,
        @SerializedName("token")
        val token: String
)

data class AptoCardDataRequest(
        @SerializedName("account_id")
        val accountId: String,
        @SerializedName("codigo")
        val codigo: String,
        @SerializedName("idUsuario")
        val idUsuario: Long,
        @SerializedName("nombre")
        val nombre: String,
        @SerializedName("pan")
        val pan: String,
        @SerializedName("vigencia")
        val vigencia: String
)


interface WalletAPI {

    companion object {
        const val CONTEXT = "Wallet/"
        const val DEBIT = "2"
        const val USUARIO = "usuario"
        const val NEGOCIO = "negocio"

        fun get(retrofit: Retrofit): WalletAPI {
            return retrofit.create(WalletAPI::class.java)
        }
    }

    @FormUrlEncoded
    @POST("$CONTEXT{idApp}/getCards")
    fun getTarjetas(
            @Path("idApp") idApp: Int,
            @Field("idUsuario") idUsuario: Long, @Field("idioma")
            idioma: String
    ): Observable<CardResponse>

    @FormUrlEncoded
    @POST("$CONTEXT{idApp}/getCards/{filter}")
    fun getTarjetasWithFilter(
            @Path("idApp") idApp: Int, @Path("filter") filter: String, @Field("idUsuario")
            idUsuario: Long, @Field("idioma") idioma: String
    ): Observable<CardResponse>

    @FormUrlEncoded
    @POST("$CONTEXT{idApp}/delete")
    fun deleteTarjeta(
            @Path("idApp") idApp: Int,
            @Field("idTarjeta") idTarjeta: Int, @Field("idUsuario") idUsuario: Long, @Field("idioma")
            idioma: String
    ): Observable<CardResponse>

    @POST("$CONTEXT{idApp}/update")
    fun updateTarjeta(
            @Path("idApp") idApp: Int, @Body
            request: CardRequest
    ): Observable<CardResponse>

    @POST("$CONTEXT{idApp}/add")
    fun addTarjeta(
            @Path("idApp") idApp: Int, @Body
            request: CardRequest
    ): Observable<CardResponse>

    @FormUrlEncoded
    @POST("$CONTEXT{idApp}/getCustomCard")
    fun getMobilecardCard(
            @Path("idApp")
            idApp: Int, @Field("idUsuario") idUsuario: Long, @Field("idioma") idioma: String, @Field("pais")
            pais: Int
    ): Observable<MobileCardResponse>

    @FormUrlEncoded
    @POST("$CONTEXT{idApp}/movements")
    fun getMovementsByUser(
            @Path("idApp")
            idApp: Int, @Field("idUsuario") idUsuario: Long, @Field("idioma")
            idioma: String
    ): Observable<MovementsResponse>

    @FormUrlEncoded
    @POST("$CONTEXT{idApp}/{idPais}/{idioma}/transactions")
    fun transactions(
            @Path("idApp") idApp: Int, @Path("idPais") pais: Int, @Path("idioma") idioma: String,
            @Field("idUsuario") idUsuario: Long
    ): Observable<TransactionResponse>

    @POST("$CONTEXT{idApp}/{idPais}/{idioma}/{tipo}/{id}/registrartarjetaprevivale")
    fun getPrevivaleCard(
            @Path("idApp") idApp: Int, @Path("idPais") pais: Int, @Path("idioma")
            idioma: String, @Path("tipo") tipo: String, @Path("id") id: Long, @Body
            userRequest: PrevivaleUserRequest
    ): Observable<MobileCardResponse>

    @POST("$CONTEXT{idApp}/{idPais}/{idioma}/defaultcard")
    fun setDefaultCard(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Body
            request: DefaultRequest
    ): Observable<CardResponse>

    @POST("$CONTEXT{idApp}/{idPais}/{idioma}/saveTebcaCard")
    fun saveTebcaCard(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Body
            request: TebcaAltaRequest
    ): Observable<CardResponse>

    //APTO
    @GET("$CONTEXT{idApp}/{idPais}/{idioma}/{idUsuario}/getInfoApto")
    fun getInfoApto(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Path("idUsuario")
            idUsuario: Long
    ): Observable<AptoResponse>

    @POST("$CONTEXT{idApp}/{idPais}/{idioma}/saveAptoInfo")
    fun saveInfoApto(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Body request: AptoUserRequest
    ): Observable<McResponse>

    @POST("$CONTEXT{idApp}/{idPais}/{idioma}/saveAptoInfo")
    fun saveInfoApto(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Body request: AptoCardRequest
    ): Observable<McResponse>

    @POST("$CONTEXT{idApp}/addAptoCard")
    fun addAptoCard(
            @Path("idApp")
            idApp: Int, @Body request: AptoCardDataRequest
    ): Observable<McResponse>

}