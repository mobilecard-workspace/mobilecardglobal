package addcel.mobilecard.data.net.viamericas.model;

/**
 * ADDCEL on 30/03/17.
 */

final class VmBranchRequest {
    //{"idCountry":"MEX", "idCity": "MEX30", "currencyMode":"N", "paymentMode":"C"}
    private final String idCountry;
    private final String idCity;
    private final String currencyMode;
    private final String payMode;

    public VmBranchRequest(String idCountry, String idCity, String currencyMode, String payMode) {
        this.idCountry = idCountry;
        this.idCity = idCity;
        this.currencyMode = currencyMode;
        this.payMode = payMode;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public String getIdCity() {
        return idCity;
    }

    public String getCurrencyMode() {
        return currencyMode;
    }

    public String getPayMode() {
        return payMode;
    }
}
