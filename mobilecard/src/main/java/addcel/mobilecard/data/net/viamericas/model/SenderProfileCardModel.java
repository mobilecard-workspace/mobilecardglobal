package addcel.mobilecard.data.net.viamericas.model;

/**
 * ADDCEL on 25/03/17.
 */
//{cvv, cardFirstname, cardLastName, cardNumber, expDate, idSender, nickName, paymentType}
public final class SenderProfileCardModel {
    private final String cvv;
    private final String cardFirstname;
    private final String cardLastName;
    private final String cardNumber;
    private final String expDate;
    private final String idSender;
    private final String nickName;
    private final String paymentType;

    private SenderProfileCardModel(Builder builder) {
        cvv = builder.cvv;
        cardFirstname = builder.cardFirstname;
        cardLastName = builder.cardLastName;
        cardNumber = builder.cardNumber;
        expDate = builder.expDate;
        idSender = builder.idSender;
        nickName = builder.nickName;
        paymentType = builder.paymentType;
    }

    public String getCvv() {
        return cvv;
    }

    public String getCardFirstname() {
        return cardFirstname;
    }

    public String getCardLastName() {
        return cardLastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpDate() {
        return expDate;
    }

    public String getIdSender() {
        return idSender;
    }

    public String getNickName() {
        return nickName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public static class Builder {
        private String cvv;
        private String cardFirstname;
        private String cardLastName;
        private String cardNumber;
        private String expDate;
        private String idSender;
        private String nickName;
        private String paymentType;

        public Builder setCvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public Builder setCardFirstname(String cardFirstname) {
            this.cardFirstname = cardFirstname;
            return this;
        }

        public Builder setCardLastName(String cardLastName) {
            this.cardLastName = cardLastName;
            return this;
        }

        public Builder setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder setExpDate(String expDate) {
            this.expDate = expDate;
            return this;
        }

        public Builder setIdSender(String idSender) {
            this.idSender = idSender;
            return this;
        }

        public Builder setNickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        public Builder setPaymentType(String paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public SenderProfileCardModel build() {
            return new SenderProfileCardModel(this);
        }
    }
}
