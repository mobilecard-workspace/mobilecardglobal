package addcel.mobilecard.data.net.previvale.entity;

/**
 * ADDCEL on 12/11/18.
 */
public final class PrCardRequest {
    private final long idEstablecimiento;

    public PrCardRequest(long idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }

    public long getIdEstablecimiento() {
        return idEstablecimiento;
    }
}
