package addcel.mobilecard.data.net.aci

import addcel.mobilecard.data.net.aci.model.NoDataPagoRequest
import android.os.Parcelable
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 2019-05-21.
 */
@Parcelize
data class AciAddressEntity(val city: String, val street: String) : Parcelable

@Parcelize
data class AciServiceEntity(val id: Int, val name: String, val pctComision: Double) :
        Parcelable

data class AciCheckDataEntity(val idUsuario: Long, val idTarjeta: Int)

data class AciResponse(val idError: Int, val mensajeError: String)

data class AciPagoEntity(
        val ciudadServicio: String, val direccionServicio: String,
        val idServicio: String, val idTarjeta: Int, val idUsuario: Long, val idioma: String,
        val imei: String, val lat: Double, val lon: Double, val modelo: String, val monto: Double,
        val nombreServicio: String, val referencia: String, val software: String, val wkey: String
)

interface AciAPI {

    companion object {
        fun get(retrofit: Retrofit): AciAPI {
            return retrofit.create(AciAPI::class.java)
        }
    }

    @Streaming
    @GET("AciBridgeWS/getServices")
    fun get(): Observable<ResponseBody>

    @GET("AciBridgeWS/{id_service}/addresses")
    fun getAddresses(
            @Path("id_service")
            id: Int
    ): Observable<List<AciAddressEntity>>

    @POST("AciBridgeWS/CheckUserData")
    fun checkUser(
            @Body
            request: AciCheckDataEntity
    ): Observable<AciResponse>

    @POST("AciBridgeWS/paymentNotData")
    fun paymentNotData(
            @Body
            request: NoDataPagoRequest
    ): Observable<AciResponse>

    @POST("AciBridgeWS/payment")
    fun payment(@Body request: AciPagoEntity): Observable<AciResponse>
}