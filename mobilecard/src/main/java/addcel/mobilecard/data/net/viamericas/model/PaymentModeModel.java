package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public class PaymentModeModel implements Parcelable {

    public static final Creator<PaymentModeModel> CREATOR = new Creator<PaymentModeModel>() {
        @Override
        public PaymentModeModel createFromParcel(Parcel in) {
            return new PaymentModeModel(in);
        }

        @Override
        public PaymentModeModel[] newArray(int size) {
            return new PaymentModeModel[size];
        }
    };
    @SerializedName("paymentModeId")
    private String mPaymentModeId;
    @SerializedName("paymentModeName")
    private String mPaymentModeName;

    public PaymentModeModel() {
    }

    public PaymentModeModel(String mPaymentModeId, String mPaymentModeName) {
        this.mPaymentModeId = mPaymentModeId;
        this.mPaymentModeName = mPaymentModeName;
    }

    protected PaymentModeModel(Parcel in) {
        mPaymentModeId = in.readString();
        mPaymentModeName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPaymentModeId);
        dest.writeString(mPaymentModeName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getPaymentModeId() {
        return mPaymentModeId;
    }

    public void setPaymentModeId(String paymentModeId) {
        mPaymentModeId = paymentModeId;
    }

    public String getPaymentModeName() {
        return mPaymentModeName;
    }

    public void setPaymentModeName(String paymentModeName) {
        mPaymentModeName = paymentModeName;
    }

    @NotNull
    @Override
    public String toString() {
        return mPaymentModeName;
    }
}
