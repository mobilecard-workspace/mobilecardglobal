package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class HoldMotiveModel {

    @SerializedName("descripcion")
    private String mDescripcion;
    @SerializedName("tipo_hold")
    private String mTipoHold;

    public String getDescripcion() {
        return mDescripcion;
    }

    public void setDescripcion(String descripcion) {
        mDescripcion = descripcion;
    }

    public String getTipoHold() {
        return mTipoHold;
    }

    public void setTipoHold(String tipoHold) {
        mTipoHold = tipoHold;
    }
}
