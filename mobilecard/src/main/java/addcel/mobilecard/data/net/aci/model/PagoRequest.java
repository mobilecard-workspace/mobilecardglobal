package addcel.mobilecard.data.net.aci.model;

/**
 * ADDCEL on 29/03/17.
 */
public final class PagoRequest extends BaseRequest {

    private final long idUsuario;
    private final int idTarjeta;
    private final double monto;
    private final String referencia;
    private final String nombreServicio;
    private final String direccionServicio;
    private final String ciudadServicio;
    private final String idServicio;
    private final String software;
    private final String wkey;
    private final String imei;
    private final String modelo;
    private final double lat;
    private final double lon;

    private PagoRequest(PagoRequest.Builder builder) {
        setIdioma(builder.idioma);
        idUsuario = builder.idUsuario;
        idTarjeta = builder.idTarjeta;
        monto = builder.monto;
        referencia = builder.referencia;
        nombreServicio = builder.nombreServicio;
        direccionServicio = builder.direccionServicio;
        ciudadServicio = builder.ciudadServicio;
        idServicio = builder.idServicio;
        software = builder.software;
        wkey = builder.wkey;
        imei = builder.imei;
        modelo = builder.modelo;
        lat = builder.lat;
        lon = builder.lon;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    public double getMonto() {
        return monto;
    }

    public String getReferencia() {
        return referencia;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public String getDireccionServicio() {
        return direccionServicio;
    }

    public String getCiudadServicio() {
        return ciudadServicio;
    }

    public String getIdServicio() {
        return idServicio;
    }

    public String getSoftware() {
        return software;
    }

    public String getWkey() {
        return wkey;
    }

    public String getImei() {
        return imei;
    }

    public String getModelo() {
        return modelo;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public static class Builder {
        private final String idioma;
        private final long idUsuario;
        private final int idTarjeta;
        private double monto;
        private String referencia;
        private String nombreServicio;
        private String direccionServicio;
        private String ciudadServicio;
        private String idServicio;
        private String software;
        private String wkey;
        private String imei;
        private String modelo;
        private double lat;
        private double lon;

        public Builder(String idioma, long idUsuario, int idTarjeta) {
            this.idioma = idioma;
            this.idUsuario = idUsuario;
            this.idTarjeta = idTarjeta;
        }

        public PagoRequest.Builder setMonto(double monto) {
            this.monto = monto;
            return this;
        }

        public PagoRequest.Builder setReferencia(String referencia) {
            this.referencia = referencia;
            return this;
        }

        public PagoRequest.Builder setNombreServicio(String nombreServicio) {
            this.nombreServicio = nombreServicio;
            return this;
        }

        public PagoRequest.Builder setDireccionServicio(String direccionServicio) {
            this.direccionServicio = direccionServicio;
            return this;
        }

        public PagoRequest.Builder setCiudadServicio(String ciudadServicio) {
            this.ciudadServicio = ciudadServicio;
            return this;
        }

        public PagoRequest.Builder setIdServicio(String idServicio) {
            this.idServicio = idServicio;
            return this;
        }

        public PagoRequest.Builder setSoftware(String software) {
            this.software = software;
            return this;
        }

        public PagoRequest.Builder setWkey(String wkey) {
            this.wkey = wkey;
            return this;
        }

        public PagoRequest.Builder setImei(String imei) {
            this.imei = imei;
            return this;
        }

        public PagoRequest.Builder setModelo(String modelo) {
            this.modelo = modelo;
            return this;
        }

        public Builder setLat(double lat) {
            this.lat = lat;
            return this;
        }

        public Builder setLon(double lon) {
            this.lon = lon;
            return this;
        }

        public PagoRequest build() {
            return new PagoRequest(this);
        }
    }
}
