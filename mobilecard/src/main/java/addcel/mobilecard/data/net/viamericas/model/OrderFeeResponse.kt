package addcel.mobilecard.data.net.viamericas.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class OrderFeeResponse(
        @SerializedName("idError")
        val idError: Int,
        @SerializedName("orders")
        val orders: List<OrderFeeModel>,
        @SerializedName("totalFee")
        val totalFee: String
)

@Parcelize
data class OrderFeeModel(
        @SerializedName("customerFixFee")
        val customerFixFee: String,
        @SerializedName("customerPercentageFee")
        val customerPercentageFee: String,
        @SerializedName("fundingFee")
        val fundingFee: String,
        @SerializedName("stateTax")
        val stateTax: String
) : Parcelable