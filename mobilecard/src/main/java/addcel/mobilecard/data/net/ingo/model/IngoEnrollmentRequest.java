package addcel.mobilecard.data.net.ingo.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class IngoEnrollmentRequest {

    @Expose
    private String addressLine1;
    @Expose
    private String addressLine2;
    @Expose
    private Boolean allowTexts;
    @Expose
    private String city;
    @Expose
    private String countryOfOrigin;
    @Expose
    private String dateOfBirth;
    @Expose
    private String deviceId;
    @Expose
    private String email;
    @Expose
    private String firstName;
    @Expose
    private String gender;
    @Expose
    private String homeNumber;
    @Expose
    private Long idUsuario;
    @Expose
    private String lastName;
    @Expose
    private String middleInitial;
    @Expose
    private String mobileNumber;
    @Expose
    private String plataforma;
    @Expose
    private String ssn;
    @Expose
    private String state;
    @Expose
    private String suffix;
    @Expose
    private String title;
    @Expose
    private String zip;

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public Boolean getAllowTexts() {
        return allowTexts;
    }

    public String getCity() {
        return city;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getGender() {
        return gender;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public String getSsn() {
        return ssn;
    }

    public String getState() {
        return state;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getTitle() {
        return title;
    }

    public String getZip() {
        return zip;
    }

    /*
      .withAddressLine2("")

    .withCountryOfOrigin("US")
    .withMiddleInitial("")
     .withPlataforma("ANDROID")
    .withSuffix("")
    .withTitle("")
       */
    public static class Builder {
        private final String addressLine2;
        private final String middleInitial;
        private final String plataforma;
        private final String suffix;
        private final String title;
        private final Boolean allowTexts;
        private String addressLine1;
        private String city;
        private String countryOfOrigin;
        private String dateOfBirth;
        private String deviceId;
        private String email;
        private String firstName;
        private String gender;
        private String homeNumber;
        private Long idUsuario;
        private String lastName;
        private String mobileNumber;
        private String ssn;
        private String state;
        private String zip;

        /**
         * Inicializamos con valores default asignados
         */
        public Builder() {
            this.addressLine2 = "";
            this.middleInitial = "";
            this.plataforma = "ANDROID";
            this.suffix = "";
            this.title = "";
            this.allowTexts = Boolean.FALSE;
        }

        public IngoEnrollmentRequest.Builder withAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
            return this;
        }

        public IngoEnrollmentRequest.Builder withCity(String city) {
            this.city = city;
            return this;
        }

        public IngoEnrollmentRequest.Builder withCountryOfOrigin(String countryOfOrigin) {
            this.countryOfOrigin = countryOfOrigin;
            return this;
        }

        public IngoEnrollmentRequest.Builder withDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public IngoEnrollmentRequest.Builder withDeviceId(String deviceId) {
            this.deviceId = deviceId;
            return this;
        }

        public IngoEnrollmentRequest.Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public IngoEnrollmentRequest.Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public IngoEnrollmentRequest.Builder withGender(String gender) {
            this.gender = gender;
            return this;
        }

        public IngoEnrollmentRequest.Builder withHomeNumber(String homeNumber) {
            this.homeNumber = homeNumber;
            return this;
        }

        public IngoEnrollmentRequest.Builder withIdUsuario(Long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public IngoEnrollmentRequest.Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public IngoEnrollmentRequest.Builder withMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
            return this;
        }

        public IngoEnrollmentRequest.Builder withSsn(String ssn) {
            this.ssn = ssn;
            return this;
        }

        public IngoEnrollmentRequest.Builder withState(String state) {
            this.state = state;
            return this;
        }

        public IngoEnrollmentRequest.Builder withZip(String zip) {
            this.zip = zip;
            return this;
        }

        public IngoEnrollmentRequest build() {
            IngoEnrollmentRequest ingoEnrollmentRequest = new IngoEnrollmentRequest();
            ingoEnrollmentRequest.addressLine1 = addressLine1;
            ingoEnrollmentRequest.addressLine2 = addressLine2;
            ingoEnrollmentRequest.allowTexts = allowTexts;
            ingoEnrollmentRequest.city = city;
            ingoEnrollmentRequest.countryOfOrigin = countryOfOrigin;
            ingoEnrollmentRequest.dateOfBirth = dateOfBirth;
            ingoEnrollmentRequest.deviceId = deviceId;
            ingoEnrollmentRequest.email = email;
            ingoEnrollmentRequest.firstName = firstName;
            ingoEnrollmentRequest.gender = gender;
            ingoEnrollmentRequest.homeNumber = homeNumber;
            ingoEnrollmentRequest.idUsuario = idUsuario;
            ingoEnrollmentRequest.lastName = lastName;
            ingoEnrollmentRequest.middleInitial = middleInitial;
            ingoEnrollmentRequest.mobileNumber = mobileNumber;
            ingoEnrollmentRequest.plataforma = plataforma;
            ingoEnrollmentRequest.ssn = ssn;
            ingoEnrollmentRequest.state = state;
            ingoEnrollmentRequest.suffix = suffix;
            ingoEnrollmentRequest.title = title;
            ingoEnrollmentRequest.zip = zip;
            return ingoEnrollmentRequest;
        }
    }
}
