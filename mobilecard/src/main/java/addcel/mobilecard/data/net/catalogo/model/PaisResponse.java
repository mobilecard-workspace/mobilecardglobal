package addcel.mobilecard.data.net.catalogo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * ADDCEL on 11/12/17.
 */
public final class PaisResponse {
    private final int idError;
    private final String mensajeError;
    private List<PaisEntity> paises;

    public PaisResponse(int idError, String mensajeError) {
        this.idError = idError;
        this.mensajeError = mensajeError;
    }

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public List<PaisEntity> getPaises() {
        return paises;
    }

    @NotNull
    @Override
    public String toString() {
        return mensajeError;
    }

    /**
     * ADDCEL on 11/12/17.
     */

  /*
  {"id":7,"codigoPais":"000052","nombrePais":"Mexico"}
   */
    public static final class PaisEntity implements Parcelable {
        public static final int MX = 1;
        public static final int CO = 2;
        public static final int US = 3;
        public static final int PE = 4;
        public static final PaisEntity DEFAULT_VALUE = new PaisEntity(-1, "", "");
        public static final Creator<PaisEntity> CREATOR = new Creator<PaisEntity>() {
            @Override
            public PaisEntity createFromParcel(Parcel in) {
                return new PaisEntity(in);
            }

            @Override
            public PaisEntity[] newArray(int size) {
                return new PaisEntity[size];
            }
        };
        private int id;

        @SerializedName("codigoPais")
        private String codigoPais;
        private String nombrePais;
        private String nombre;
        private String url;

        public PaisEntity() {
        }

        public PaisEntity(int id, String codigoPais, String nombrePais) {
            this.id = id;
            this.codigoPais = codigoPais;
            this.nombrePais = nombrePais;
        }

        public PaisEntity(int id, String codigoPais, String nombrePais, String nombre) {
            this(id, codigoPais, nombrePais);
            this.nombre = nombre;
        }

        protected PaisEntity(Parcel in) {
            id = in.readInt();
            codigoPais = in.readString();
            nombrePais = in.readString();
            nombre = in.readString();
            url = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(codigoPais);
            dest.writeString(nombrePais);
            dest.writeString(nombre);
            dest.writeString(url);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public int getId() {
            return id;
        }

        public String getCodigoPais() {
            return codigoPais;
        }

        public String getNombrePais() {
            return nombrePais;
        }

        public String getNombre() {
            return nombre;
        }

        public String getUrl() {
            return url;
        }

        @NotNull
        @Override
        public String toString() {
            return !Strings.isNullOrEmpty(nombre) ? nombre : nombrePais;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PaisEntity that = (PaisEntity) o;
            return id == that.id && Objects.equal(codigoPais, that.codigoPais) && Objects.equal(
                    nombrePais, that.nombrePais) && Objects.equal(nombre, that.nombre) && Objects.equal(url,
                    that.url);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(id, codigoPais, nombrePais, nombre, url);
        }
    }
}
