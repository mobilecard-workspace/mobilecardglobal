package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 05/04/17.
 */

public final class StatesResponse extends McResponse {
    @SerializedName("countryStates")
    private List<VmStateModel> states;

    public StatesResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public List<VmStateModel> getStates() {
        return states;
    }
}
