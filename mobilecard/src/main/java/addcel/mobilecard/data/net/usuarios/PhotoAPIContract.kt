package addcel.mobilecard.data.net.usuarios

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * ADDCEL on 2019-07-01.
 */

data class PhotoRequest(val idUser: Long, val photo: String)

data class PhotoResponse(val idError: Int, val mensajeError: String)

data class PhotoResponseEvent(val data: PhotoResponse)

data class PhotoNotificationEvent(val photo64: String?)

data class NameNotificationEvent(val name: String?)

data class CountryNotificationEnvent(val id: Int)

interface PhotoAPI {

    companion object {
        const val ADD = "add"
        const val UPDATE = "update"

        fun get(retrofit: Retrofit): PhotoAPI {
            return retrofit.create(PhotoAPI::class.java)
        }
    }

    @POST("Usuarios/{idApp}/{idPais}/{idioma}/user/photo/{method}")
    fun loadPhoto(
            @Path("idApp")
            idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Path("method")
            method: String, @Body body: PhotoRequest
    ): Observable<PhotoResponse>
}