package addcel.mobilecard.data.net.pago

import android.os.Parcelable
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

data class PagoEntity(
        val codigoPais: String = "",
        val comision: Double = 0.0,
        val concepto: String = "",
        val debug: Boolean = false,
        val emisor: Int = 0,
        val idAplicacion: Int = 1,
        val idPais: Int = 1,
        val idProveedor: String = "",
        val idCard: Int = 0,
        val idUser: Long = 0L,
        val imei: String = "",
        val lat: Double = 0.0,
        val lon: Double = 0.0,
        val cargo: Double = 0.0,
        val operacion: String = "",
        val referencia: String = "",
        val software: String = "",
        val modelo: String = ""
)

// {"code":0,"message":null,"status":null,"opId":null,"txnISOCode":null,"authNumber":null,"ticketUrl":null,"amount":0.0,"maskedPAN":null,"dateTime":0,"idTransaccion":1628}
@Parcelize
data class PagoResponse(
        val amount: Double, val code: Int, val dateTime: Long,
        val idTransaccion: Int, val maskedPAN: String? = "", val message: String?, val opId: Int,
        val status: Int, val txnISOCode: String? = "", val authNumber: String? = "",
        val ticketUrl: String? = ""
) : Parcelable

interface BPApi {
    @POST("Transacto/{idApp}/{idPais}/{idioma}/{accountId}/pagoBP")
    fun executePagoBP(
            @Header("Authorization") auth: String, @Path("idApp") idApp: Int, @Path("idPais") idPais: Int,
            @Path("idioma") idioma: String, @Path("accountId") accountId: String, @Body
            pago: PagoEntity
    ): Observable<PagoResponse>

    companion object {
        fun get(retrofit: Retrofit): BPApi {
            return retrofit.create(BPApi::class.java)
        }
    }
}
