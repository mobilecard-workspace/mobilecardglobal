package addcel.mobilecard.data.net.ingo;

import addcel.mobilecard.data.net.ingo.model.IngoCardRequest;
import addcel.mobilecard.data.net.ingo.model.IngoCardResponse;
import addcel.mobilecard.data.net.ingo.model.IngoEnrollmentRequest;
import addcel.mobilecard.data.net.ingo.model.IngoEstadoResponse;
import addcel.mobilecard.data.net.ingo.model.IngoLoginRequest;
import addcel.mobilecard.data.net.ingo.model.IngoUserData;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * ADDCEL on 24/11/16.
 */
public interface IngoService {
    String PATH = "MCIngo/{idApp}/{idioma}/";

    @POST(PATH + "getSessionData")
    Observable<IngoUserData> getSessionData(
            @Header("Authorization") String auth, @Path("idApp") int idApp, @Path("idioma") String idioma,
            @Body IngoLoginRequest request);

    @GET(PATH + "{idPais}/estados")
    Observable<IngoEstadoResponse> getEstados(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idPais") int idPais);

    @POST(PATH + "EnrollCustomer")
    Observable<IngoUserData> enrollCustomer(@Path("idApp") int idApp,
                                            @Path("idioma") String idioma, @Body IngoEnrollmentRequest request);

    @POST(PATH + "AddOrUpdateCard")
    Observable<IngoCardResponse> addOrUpdateCard(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Body IngoCardRequest request);
}
