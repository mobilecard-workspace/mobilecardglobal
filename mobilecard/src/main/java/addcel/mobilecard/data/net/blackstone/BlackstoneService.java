package addcel.mobilecard.data.net.blackstone;

import java.util.List;

import addcel.mobilecard.data.net.blackstone.model.BSCarrierEntity;
import addcel.mobilecard.data.net.blackstone.model.BSPagoRequestEntity;
import addcel.mobilecard.data.net.blackstone.model.BSPagoResponseEntity;
import addcel.mobilecard.data.net.blackstone.model.BSProductEntity;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * ADDCEL on 06/07/18.
 */
public interface BlackstoneService {
    String PATH = "Blackstone/{idApp}/{idioma}";

    /**
     * https://192.168.75.53/Blackstone/1/es/getCarriers?countryCode=USA Variable Path
     * {idApp}/{idioma}/getCarriers Response: [{"carrierName":"ADMA TELECOM
     * INC"},{"carrierName":"BTS"}]
     */
    @GET(PATH + "/getCarriers")
    Observable<List<BSCarrierEntity>> getCarriers(
            @Path("idApp") int idApp, @Path("idioma") String idioma,
            @Query("countryCode") String countryCode);

    /**
     * get('http://199.231.160.203/Blackstone/1/es/getProductsByCarrier?carrierName=TIGO')
     *
     * @param idApp       - Identificador de la aplicacion
     * @param idioma      - Idioma del dispositivo
     * @param carrierName - Identificador del carrier seleccionado
     * @return Lista de productos asociados al carrier seleccionado
     */
    @GET(PATH + "/getProductsByCarrier")
    Observable<List<BSProductEntity>> getProductsByCarrier(
            @Path("idApp") int idApp, @Path("idioma") String idioma,
            @Query("carrierName") String carrierName);

    @POST(PATH + "/doTopUp")
    Observable<BSPagoResponseEntity> doTopUp(@Path("idApp") int idApp,
                                             @Path("idioma") String idioma, @Body BSPagoRequestEntity request);
}
