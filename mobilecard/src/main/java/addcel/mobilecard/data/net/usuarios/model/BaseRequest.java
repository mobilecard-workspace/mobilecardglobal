package addcel.mobilecard.data.net.usuarios.model;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 08/12/16.
 */

public class BaseRequest {
    @SerializedName("idioma")
    private String mIdioma;

    protected BaseRequest() {
    }

    protected BaseRequest(String idioma) {
        this.mIdioma = idioma;
    }

    public String getIdioma() {
        return mIdioma;
    }

    protected void setIdioma(String idioma) {
        this.mIdioma = idioma;
    }
}
