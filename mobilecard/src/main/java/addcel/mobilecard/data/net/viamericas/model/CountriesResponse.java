package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 05/04/17.
 */

public final class CountriesResponse extends McResponse {
    @SerializedName("countrys")
    private List<VmCountryModel> countries;

    public CountriesResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public List<VmCountryModel> getCountries() {
        return countries;
    }
}
