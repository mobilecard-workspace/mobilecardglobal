package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public class LocationModel implements Parcelable {

    public static final Creator<LocationModel> CREATOR = new Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };
    @SerializedName("addressBranch")
    private String mAddressBranch;
    @SerializedName("businessHours")
    private String mBusinessHours;
    @SerializedName("customerfixfee")
    private String mCustomerfixfee;
    @SerializedName("customerpercentageapplied")
    private String mCustomerpercentageapplied;
    @SerializedName("customerpercentagefee")
    private String mCustomerpercentagefee;
    @SerializedName("idCity")
    private String mIdCity;
    @SerializedName("idCountry")
    private String mIdCountry;
    @SerializedName("idModePay")
    private String mIdModePay;
    @SerializedName("idState")
    private String mIdState;
    @SerializedName("idbranchpaymentlocation")
    private String mIdbranchpaymentlocation;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("nameGroup")
    private String mNameGroup;
    @SerializedName("outputAmount")
    private Double mOutputAmount;
    @SerializedName("outputRate")
    private Double mOutputRate;
    @SerializedName("payerNetworkLocation")
    private String mPayerNetworkLocation;
    @SerializedName("phoneBranch")
    private String mPhoneBranch;
    @SerializedName("routingRequired")
    private Boolean mRoutingRequired;
    @SerializedName("viamericasfixfee")
    private String mViamericasfixfee;
    @SerializedName("viamericaspercentagefee")
    private String mViamericaspercentagefee;
    @SerializedName("__hashCodeCalc")
    private Boolean m_HashCodeCalc;

    protected LocationModel(Parcel in) {
        mAddressBranch = in.readString();
        mBusinessHours = in.readString();
        mCustomerfixfee = in.readString();
        mCustomerpercentageapplied = in.readString();
        mCustomerpercentagefee = in.readString();
        mIdCity = in.readString();
        mIdCountry = in.readString();
        mIdModePay = in.readString();
        mIdState = in.readString();
        mIdbranchpaymentlocation = in.readString();
        mLatitude = in.readString();
        mLongitude = in.readString();
        mNameGroup = in.readString();
        mPayerNetworkLocation = in.readString();
        mPhoneBranch = in.readString();
        mViamericasfixfee = in.readString();
        mViamericaspercentagefee = in.readString();
    }

    public LocationModel() {
    }

    public LocationModel(String addressBranch, String nameGroup) {
        this.mAddressBranch = addressBranch;
        this.mNameGroup = nameGroup;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAddressBranch);
        dest.writeString(mBusinessHours);
        dest.writeString(mCustomerfixfee);
        dest.writeString(mCustomerpercentageapplied);
        dest.writeString(mCustomerpercentagefee);
        dest.writeString(mIdCity);
        dest.writeString(mIdCountry);
        dest.writeString(mIdModePay);
        dest.writeString(mIdState);
        dest.writeString(mIdbranchpaymentlocation);
        dest.writeString(mLatitude);
        dest.writeString(mLongitude);
        dest.writeString(mNameGroup);
        dest.writeString(mPayerNetworkLocation);
        dest.writeString(mPhoneBranch);
        dest.writeString(mViamericasfixfee);
        dest.writeString(mViamericaspercentagefee);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAddressBranch() {
        return mAddressBranch;
    }

    public void setAddressBranch(String addressBranch) {
        mAddressBranch = addressBranch;
    }

    public String getBusinessHours() {
        return mBusinessHours;
    }

    public void setBusinessHours(String businessHours) {
        mBusinessHours = businessHours;
    }

    public String getCustomerfixfee() {
        return mCustomerfixfee;
    }

    public void setCustomerfixfee(String customerfixfee) {
        mCustomerfixfee = customerfixfee;
    }

    public String getCustomerpercentageapplied() {
        return mCustomerpercentageapplied;
    }

    public void setCustomerpercentageapplied(String customerpercentageapplied) {
        mCustomerpercentageapplied = customerpercentageapplied;
    }

    public String getCustomerpercentagefee() {
        return mCustomerpercentagefee;
    }

    public void setCustomerpercentagefee(String customerpercentagefee) {
        mCustomerpercentagefee = customerpercentagefee;
    }

    public String getIdCity() {
        return mIdCity;
    }

    public void setIdCity(String idCity) {
        mIdCity = idCity;
    }

    public String getIdCountry() {
        return mIdCountry;
    }

    public void setIdCountry(String idCountry) {
        mIdCountry = idCountry;
    }

    public String getIdModePay() {
        return mIdModePay;
    }

    public void setIdModePay(String idModePay) {
        mIdModePay = idModePay;
    }

    public String getIdState() {
        return mIdState;
    }

    public void setIdState(String idState) {
        mIdState = idState;
    }

    public String getIdbranchpaymentlocation() {
        return mIdbranchpaymentlocation;
    }

    public void setIdbranchpaymentlocation(String idbranchpaymentlocation) {
        mIdbranchpaymentlocation = idbranchpaymentlocation;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getNameGroup() {
        return mNameGroup;
    }

    public void setNameGroup(String nameGroup) {
        mNameGroup = nameGroup;
    }

    public Double getOutputAmount() {
        return mOutputAmount;
    }

    public void setOutputAmount(Double outputAmount) {
        mOutputAmount = outputAmount;
    }

    public Double getOutputRate() {
        return mOutputRate;
    }

    public void setOutputRate(Double outputRate) {
        mOutputRate = outputRate;
    }

    public String getPayerNetworkLocation() {
        return mPayerNetworkLocation;
    }

    public void setPayerNetworkLocation(String payerNetworkLocation) {
        mPayerNetworkLocation = payerNetworkLocation;
    }

    public String getPhoneBranch() {
        return mPhoneBranch;
    }

    public void setPhoneBranch(String phoneBranch) {
        mPhoneBranch = phoneBranch;
    }

    public Boolean getRoutingRequired() {
        return mRoutingRequired;
    }

    public void setRoutingRequired(Boolean routingRequired) {
        mRoutingRequired = routingRequired;
    }

    public String getViamericasfixfee() {
        return mViamericasfixfee;
    }

    public void setViamericasfixfee(String viamericasfixfee) {
        mViamericasfixfee = viamericasfixfee;
    }

    public String getViamericaspercentagefee() {
        return mViamericaspercentagefee;
    }

    public void setViamericaspercentagefee(String viamericaspercentagefee) {
        mViamericaspercentagefee = viamericaspercentagefee;
    }

    public Boolean get_HashCodeCalc() {
        return m_HashCodeCalc;
    }

    public void set_HashCodeCalc(Boolean _HashCodeCalc) {
        m_HashCodeCalc = _HashCodeCalc;
    }

    @NotNull
    @Override
    public String toString() {
        return mNameGroup;
    }
}
