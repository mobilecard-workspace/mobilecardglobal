package addcel.mobilecard.data.net.colombia.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * ADDCEL on 10/18/18.
 */
public final class ColombiaPagoResponseEntity implements Parcelable {
    private final int idError;
    private final String mensajeError;
    private final String codRespuesta;
    private final double costoTransaccion;
    private final String descRespuesta;
    private final String estado;
    private final String fechaPosteo;
    private final String fechaTransaccion;
    private final int idIntegrador;
    private final int idTransaccion;
    private final int idTransaccionAutorizador;
    private final String numAprobacion;
    private final double montoTotal;
    private final double comision;

    private ColombiaPagoResponseEntity(Builder builder) {
        idError = builder.idError;
        mensajeError = builder.mensajeError;
        codRespuesta = builder.codRespuesta;
        costoTransaccion = builder.costoTransaccion;
        descRespuesta = builder.descRespuesta;
        estado = builder.estado;
        fechaPosteo = builder.fechaPosteo;
        fechaTransaccion = builder.fechaTransaccion;
        idIntegrador = builder.idIntegrador;
        idTransaccion = builder.idTransaccion;
        idTransaccionAutorizador = builder.idTransaccionAutorizador;
        numAprobacion = builder.numAprobacion;
        montoTotal = builder.montoTotal;
        comision = builder.comision;
    }

    private ColombiaPagoResponseEntity(Parcel in) {
        idError = in.readInt();
        mensajeError = in.readString();
        codRespuesta = in.readString();
        costoTransaccion = in.readDouble();
        descRespuesta = in.readString();
        estado = in.readString();
        fechaPosteo = in.readString();
        fechaTransaccion = in.readString();
        idIntegrador = in.readInt();
        idTransaccion = in.readInt();
        idTransaccionAutorizador = in.readInt();
        numAprobacion = in.readString();
        montoTotal = in.readDouble();
        comision = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idError);
        dest.writeString(mensajeError);
        dest.writeString(codRespuesta);
        dest.writeDouble(costoTransaccion);
        dest.writeString(descRespuesta);
        dest.writeString(estado);
        dest.writeString(fechaPosteo);
        dest.writeString(fechaTransaccion);
        dest.writeInt(idIntegrador);
        dest.writeInt(idTransaccion);
        dest.writeInt(idTransaccionAutorizador);
        dest.writeString(numAprobacion);
        dest.writeDouble(montoTotal);
        dest.writeDouble(comision);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ColombiaPagoResponseEntity> CREATOR =
            new Creator<ColombiaPagoResponseEntity>() {
                @Override
                public ColombiaPagoResponseEntity createFromParcel(Parcel in) {
                    return new ColombiaPagoResponseEntity(in);
                }

                @Override
                public ColombiaPagoResponseEntity[] newArray(int size) {
                    return new ColombiaPagoResponseEntity[size];
                }
            };

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    private String getCodRespuesta() {
        return codRespuesta;
    }

    private double getCostoTransaccion() {
        return costoTransaccion;
    }

    private String getDescRespuesta() {
        return descRespuesta;
    }

    private String getEstado() {
        return estado;
    }

    private String getFechaPosteo() {
        return fechaPosteo;
    }

    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    public int getIdIntegrador() {
        return idIntegrador;
    }

    public int getIdTransaccion() {
        return idTransaccion;
    }

    public int getIdTransaccionAutorizador() {
        return idTransaccionAutorizador;
    }

    public String getNumAprobacion() {
        return numAprobacion;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public double getComision() {
        return comision;
    }

    public static final class Builder {
        private String codRespuesta;
        private double costoTransaccion;
        private String descRespuesta;
        private String estado;
        private String fechaPosteo;
        private String fechaTransaccion;
        private int idIntegrador;
        private int idTransaccion;
        private int idTransaccionAutorizador;
        private String numAprobacion;
        private double montoTotal;
        private double comision;
        private int idError;
        private String mensajeError;

        public Builder() {
        }

        public Builder(ColombiaPagoResponseEntity copy) {
            this.idError = copy.getIdError();
            this.mensajeError = copy.getMensajeError();
            this.codRespuesta = copy.getCodRespuesta();
            this.costoTransaccion = copy.getCostoTransaccion();
            this.descRespuesta = copy.getDescRespuesta();
            this.estado = copy.getEstado();
            this.fechaPosteo = copy.getFechaPosteo();
            this.fechaTransaccion = copy.getFechaTransaccion();
            this.idIntegrador = copy.getIdIntegrador();
            this.idTransaccion = copy.getIdTransaccion();
            this.idTransaccionAutorizador = copy.getIdTransaccionAutorizador();
            this.numAprobacion = copy.getNumAprobacion();
            this.montoTotal = copy.getMontoTotal();
            this.comision = copy.getComision();
        }

        public Builder setCodRespuesta(String codRespuesta) {
            this.codRespuesta = codRespuesta;
            return this;
        }

        public Builder setCostoTransaccion(double costoTransaccion) {
            this.costoTransaccion = costoTransaccion;
            return this;
        }

        public Builder setDescRespuesta(String descRespuesta) {
            this.descRespuesta = descRespuesta;
            return this;
        }

        public Builder setEstado(String estado) {
            this.estado = estado;
            return this;
        }

        public Builder setFechaPosteo(String fechaPosteo) {
            this.fechaPosteo = fechaPosteo;
            return this;
        }

        public Builder setFechaTransaccion(String fechaTransaccion) {
            this.fechaTransaccion = fechaTransaccion;
            return this;
        }

        public Builder setIdIntegrador(int idIntegrador) {
            this.idIntegrador = idIntegrador;
            return this;
        }

        public Builder setIdTransaccion(int idTransaccion) {
            this.idTransaccion = idTransaccion;
            return this;
        }

        public Builder setIdTransaccionAutorizador(int idTransaccionAutorizador) {
            this.idTransaccionAutorizador = idTransaccionAutorizador;
            return this;
        }

        public Builder setNumAprobacion(String numAprobacion) {
            this.numAprobacion = numAprobacion;
            return this;
        }

        public Builder setMontoTotal(double montoTotal) {
            this.montoTotal = montoTotal;
            return this;
        }

        public Builder setComision(double comision) {
            this.comision = comision;
            return this;
        }

        public ColombiaPagoResponseEntity build() {
            return new ColombiaPagoResponseEntity(this);
        }

        public Builder setIdError(int idError) {
            this.idError = idError;
            return this;
        }

        public Builder setMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }
    }
}
