package addcel.mobilecard.data.net.apto.accounts

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


/**
 * ADDCEL on 2019-10-24.
 */
@Parcelize
data class AptoCardData(
        @SerializedName("account_id")
        val accountId: String,
        @SerializedName("card_brand")
        val cardBrand: String?,
        @SerializedName("card_issuer")
        val cardIssuer: String,
        @SerializedName("card_network")
        val cardNetwork: String,
        @SerializedName("card_product_id")
        val cardProductId: String,
        @SerializedName("card_style")
        val cardStyle: CardStyle,
        @SerializedName("cardholder_first_name")
        val cardholderFirstName: String,
        @SerializedName("cardholder_last_name")
        val cardholderLastName: String,
        @SerializedName("cvv")
        val cvv: String,
        @SerializedName("expiration")
        val expiration: String,
        @SerializedName("features")
        val features: Features,
        @SerializedName("issued_at")
        val issuedAt: String?,
        @SerializedName("kyc_reason")
        val kycReason: String?,
        @SerializedName("kyc_status")
        val kycStatus: String,
        @SerializedName("last_four")
        val lastFour: String,
        @SerializedName("name_on_card")
        val nameOnCard: String,
        @SerializedName("native_spendable_today")
        val nativeSpendableToday: NativeSpendableToday,
        @SerializedName("native_total_balance")
        val nativeTotalBalance: NativeTotalBalance,
        @SerializedName("ordered_status")
        val orderedStatus: String,
        @SerializedName("pan")
        val pan: String,
        @SerializedName("spendable_today")
        val spendableToday: SpendableToday,
        @SerializedName("state")
        val state: String,
        @SerializedName("total_balance")
        val totalBalance: TotalBalance,
        @SerializedName("type")
        val type: String,
        @SerializedName("wait_list")
        val waitList: Boolean
) : Parcelable

@Parcelize
data class CardStyle(
        @SerializedName("background")
        val background: Background,
        @SerializedName("balance_selector_asset")
        val balanceSelectorAsset: String?,
        @SerializedName("text_color")
        val textColor: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Background(
        @SerializedName("background_color")
        val backgroundColor: String,
        @SerializedName("background_image")
        val backgroundImage: String?,
        @SerializedName("background_type")
        val backgroundType: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Features(
        @SerializedName("activation")
        val activation: Activation,
        @SerializedName("get_pin")
        val getPin: GetPin,
        @SerializedName("select_balance_store")
        val selectBalanceStore: SelectBalanceStore,
        @SerializedName("set_pin")
        val setPin: SetPin,
        @SerializedName("support")
        val support: Support
) : Parcelable

@Parcelize
data class Activation(
        @SerializedName("status")
        val status: String
) : Parcelable

@Parcelize
data class GetPin(
        @SerializedName("ivr_phone")
        val ivrPhone: IvrPhone,
        @SerializedName("status")
        val status: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class IvrPhone(
        @SerializedName("country_code")
        val countryCode: String,
        @SerializedName("phone_number")
        val phoneNumber: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class SelectBalanceStore(
        @SerializedName("allowed_balance_types")
        val allowedBalanceTypes: String?
) : Parcelable

@Parcelize
data class SetPin(
        @SerializedName("status")
        val status: String,
        @SerializedName("type")
        val type: String?
) : Parcelable

@Parcelize
data class Support(
        @SerializedName("ivr_phone")
        val ivrPhone: IvrPhoneX,
        @SerializedName("status")
        val status: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class IvrPhoneX(
        @SerializedName("country_code")
        val countryCode: String,
        @SerializedName("phone_number")
        val phoneNumber: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class NativeSpendableToday(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String
) : Parcelable

@Parcelize
data class NativeTotalBalance(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String
) : Parcelable

@Parcelize
data class SpendableToday(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String
) : Parcelable

@Parcelize
data class TotalBalance(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String
) : Parcelable