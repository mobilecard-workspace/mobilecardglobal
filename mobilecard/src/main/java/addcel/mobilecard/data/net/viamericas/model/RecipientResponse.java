package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

@SuppressWarnings("unused")
public class RecipientResponse extends McResponse
        implements Parcelable {

    public static final Creator<RecipientResponse> CREATOR = new Creator<RecipientResponse>() {
        @Override
        public RecipientResponse createFromParcel(Parcel in) {
            return new RecipientResponse(in);
        }

        @Override
        public RecipientResponse[] newArray(int size) {
            return new RecipientResponse[size];
        }
    };
    @SerializedName("address")
    private String mAddress;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("FName")
    private String mFName;
    @SerializedName("idCity")
    private String mIdCity;
    @SerializedName("idCountry")
    private String mIdCountry;
    @SerializedName("idRecipient")
    private String mIdRecipient;
    @SerializedName("idSender")
    private String mIdSender;
    @SerializedName("idState")
    private String mIdState;
    @SerializedName("LName")
    private String mLName;

    public RecipientResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    protected RecipientResponse(Parcel in) {
        super(in);
        mAddress = in.readString();
        mEmail = in.readString();
        mFName = in.readString();
        mIdCity = in.readString();
        mIdCountry = in.readString();
        mIdRecipient = in.readString();
        mIdSender = in.readString();
        mIdState = in.readString();
        mLName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mAddress);
        dest.writeString(mEmail);
        dest.writeString(mFName);
        dest.writeString(mIdCity);
        dest.writeString(mIdCountry);
        dest.writeString(mIdRecipient);
        dest.writeString(mIdSender);
        dest.writeString(mIdState);
        dest.writeString(mLName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFName() {
        return mFName;
    }

    public void setFName(String FName) {
        mFName = FName;
    }

    public String getIdCity() {
        return mIdCity;
    }

    public void setIdCity(String idCity) {
        mIdCity = idCity;
    }

    public String getIdCountry() {
        return mIdCountry;
    }

    public void setIdCountry(String idCountry) {
        mIdCountry = idCountry;
    }

    public String getIdRecipient() {
        return mIdRecipient;
    }

    public void setIdRecipient(String idRecipient) {
        mIdRecipient = idRecipient;
    }

    public String getIdSender() {
        return mIdSender;
    }

    public void setIdSender(String idSender) {
        mIdSender = idSender;
    }

    public String getIdState() {
        return mIdState;
    }

    public void setIdState(String idState) {
        mIdState = idState;
    }

    public String getLName() {
        return mLName;
    }

    public void setLName(String LName) {
        mLName = LName;
    }

    @NotNull
    @Override
    public String toString() {
        return mLName + ", " + mFName;
    }
}
