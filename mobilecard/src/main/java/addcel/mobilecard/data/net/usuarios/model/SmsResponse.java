package addcel.mobilecard.data.net.usuarios.model;

/**
 * ADDCEL on 11/29/18.
 */
public final class SmsResponse {
    private final int idError;
    private final String mensajeError;
    private final int idUsrStatus;

    private SmsResponse(Builder builder) {
        idError = builder.idError;
        mensajeError = builder.mensajeError;
        idUsrStatus = builder.idUsrStatus;
    }

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public int getIdUsrStatus() {
        return idUsrStatus;
    }

    public static final class Builder {
        private int idError;
        private String mensajeError;
        private int idUsrStatus;

        public Builder() {
        }

        public Builder(SmsResponse copy) {
            this.idError = copy.getIdError();
            this.mensajeError = copy.getMensajeError();
            this.idUsrStatus = copy.getIdUsrStatus();
        }

        public Builder setIdError(int idError) {
            this.idError = idError;
            return this;
        }

        public Builder setMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }

        public Builder setIdUsrStatus(int idUsrStatus) {
            this.idUsrStatus = idUsrStatus;
            return this;
        }

        public SmsResponse build() {
            return new SmsResponse(this);
        }
    }
}
