package addcel.mobilecard.data.net.viamericas.model;

/**
 * ADDCEL on 25/03/17.
 */
//{addressReceiver, recipientFirstName, recipientLastName, idBranchReciever, idCityRecivier,
// idStateReceiver, idCountryReciever, idRecipient, idSender, modePayReciever  accountReceiver,
// modPayCurrencyReceiver, amount}
public final class OrderModel {
    private final String addressReceiver;
    private final String recipientFirstName;
    private final String recipientLastName;
    private final String idBranchReciever;
    private final String idCityRecivier;
    private final String idStateReceiver;
    private final String idCountryReciever;
    private final String idRecipient;
    private final String idSender;
    private final String modePayReciever;
    private final String accountReceiver;
    private final String modPayCurrencyReceiver;
    private final double amount;
    private final long idUsuario;
    private final String latitude;
    private final String longitude;
    private final String placeName;
    private final String phoneType;
    private final String software;
    private final String appVersion;

    private OrderModel(Builder builder) {
        addressReceiver = builder.addressReceiver;
        recipientFirstName = builder.recipientFirstName;
        recipientLastName = builder.recipientLastName;
        idBranchReciever = builder.idBranchReciever;
        idCityRecivier = builder.idCityRecivier;
        idStateReceiver = builder.idStateReceiver;
        idCountryReciever = builder.idCountryReciever;
        idRecipient = builder.idRecipient;
        idSender = builder.idSender;
        modePayReciever = builder.modePayReciever;
        accountReceiver = builder.accountReceiver;
        modPayCurrencyReceiver = builder.modPayCurrencyReceiver;
        amount = builder.amount;
        idUsuario = builder.idUsuario;
        latitude = builder.latitude;
        longitude = builder.longitude;
        placeName = builder.placeName;
        phoneType = builder.phoneType;
        software = builder.software;
        appVersion = builder.appVersion;
    }

    public static class Builder {
        private String addressReceiver;
        private String recipientFirstName;
        private String recipientLastName;
        private String idBranchReciever;
        private String idCityRecivier;
        private String idStateReceiver;
        private String idCountryReciever;
        private String idRecipient;
        private String idSender;
        private String modePayReciever;
        private String accountReceiver;
        private String modPayCurrencyReceiver;
        private double amount;
        private long idUsuario;
        private String latitude;
        private String longitude;
        private String placeName;
        private String phoneType;
        private String software;
        private String appVersion;

        public Builder setAddressReceiver(String addressReceiver) {
            this.addressReceiver = addressReceiver;
            return this;
        }

        public Builder setRecipientFirstName(String recipientFirstName) {
            this.recipientFirstName = recipientFirstName;
            return this;
        }

        public Builder setRecipientLastName(String recipientLastName) {
            this.recipientLastName = recipientLastName;
            return this;
        }

        public Builder setIdBranchReciever(String idBranchReciever) {
            this.idBranchReciever = idBranchReciever;
            return this;
        }

        public Builder setIdCityRecivier(String idCityRecivier) {
            this.idCityRecivier = idCityRecivier;
            return this;
        }

        public Builder setIdStateReceiver(String idStateReceiver) {
            this.idStateReceiver = idStateReceiver;
            return this;
        }

        public Builder setIdCountryReciever(String idCountryReciever) {
            this.idCountryReciever = idCountryReciever;
            return this;
        }

        public Builder setIdRecipient(String idRecipient) {
            this.idRecipient = idRecipient;
            return this;
        }

        public Builder setIdSender(String idSender) {
            this.idSender = idSender;
            return this;
        }

        public Builder setModePayReciever(String modePayReciever) {
            this.modePayReciever = modePayReciever;
            return this;
        }

        public Builder setAccountReceiver(String accountReceiver) {
            this.accountReceiver = accountReceiver;
            return this;
        }

        public Builder setModPayCurrencyReceiver(String modPayCurrencyReceiver) {
            this.modPayCurrencyReceiver = modPayCurrencyReceiver;
            return this;
        }

        public Builder setAmount(double amount) {
            this.amount = amount;
            return this;
        }

        public Builder setIdUsuario(long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public Builder setLatitude(String latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder setLongitude(String longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder setPlaceName(String placeName) {
            this.placeName = placeName;
            return this;
        }

        public Builder setPhoneType(String phoneType) {
            this.phoneType = phoneType;
            return this;
        }

        public Builder setSoftware(String software) {
            this.software = software;
            return this;
        }

        public Builder setAppVersion(String appVersion) {
            this.appVersion = appVersion;
            return this;
        }

        public OrderModel build() {
            return new OrderModel(this);
        }
    }
}
