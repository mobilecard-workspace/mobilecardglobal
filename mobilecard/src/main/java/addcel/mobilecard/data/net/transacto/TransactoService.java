package addcel.mobilecard.data.net.transacto;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * ADDCEL on 29/06/17.
 */
public interface TransactoService {
    String PATH = "Transacto/{idApp}/{idioma}/{method}";

    String SALDO = "procesaConsultaSaldos";
    String DIVISA = "getDivisa";
    String PAGO = "Transacto/{idApp}/{idioma}/{accountId}/pagoProsa3DS";

    @FormUrlEncoded
    @POST(PATH)
    Observable<String> call(@Path("idApp") int idApp,
                            @Path("idioma") String idioma, @Path("method") String method, @Field("json") String json);
}
