package addcel.mobilecard.data.net.apto.verification

import addcel.mobilecard.BuildConfig
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * ADDCEL on 2019-07-30.
 */

enum class DataPointType {
    phone, email, birthdate
}

data class Verification(
        @SerializedName("datapoint_type") val type: DataPointType,
        @SerializedName("datapoint") val datapoint: Map<String, Any>
)

data class Secret(val secret: String)

@Parcelize
data class VerificationResponse(
        @SerializedName("status") val status: String,
        @SerializedName("type") val type: String, @SerializedName("verification_id")
        val verificationId: String
) : Parcelable

interface VerificationAPI {

    companion object {

        private const val API_KEY =
                "Bearer KCR+DBqSu3M4a1+IZMCz9ffADr3EzwaBhF0WwcAQ0nVqtQlX28xZQdfoMAIcGEXI"

        private const val VERSION = "v1"

        const val STATUS_PENDING = "pending"
        const val STATUS_PASSED = "passed"

        fun get(): VerificationAPI {
            val interceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Timber.d(message)
                }
            })

            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val builder = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS).writeTimeout(40, TimeUnit.SECONDS)

            if (BuildConfig.DEBUG) builder.addInterceptor(interceptor)

            val r = Retrofit.Builder().client(builder.build())
                    .baseUrl(BuildConfig.APTO_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()

            return r.create(VerificationAPI::class.java)
        }
    }

    @Headers("Api-Key: $API_KEY")
    @POST("v1/verifications/start")
    fun start(
            @Body
            body: Verification
    ): Observable<VerificationResponse>

    @Headers("Api-Key: $API_KEY")
    @POST("$VERSION/verifications/{verification_id}/finish")
    fun finish(
            @Path("verification_id") verificationId: String, @Body
            secret: Secret
    ): Observable<VerificationResponse>
}