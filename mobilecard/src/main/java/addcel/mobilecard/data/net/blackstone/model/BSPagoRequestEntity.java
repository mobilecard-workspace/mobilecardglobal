package addcel.mobilecard.data.net.blackstone.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class BSPagoRequestEntity {
    @Expose
    private final double amount;
    @Expose
    private final String countryCode;
    @Expose
    private final String mainCode;
    @Expose
    private final String phoneNumber;
    @Expose
    private final int idTarjeta;
    @Expose
    private final long idUsuario;
    @Expose
    private final String carrierName;
    @Expose
    private final double lat;
    @Expose
    private final double lon;

    private BSPagoRequestEntity(Builder builder) {
        amount = builder.amount;
        countryCode = builder.countryCode;
        mainCode = builder.mainCode;
        phoneNumber = builder.phoneNumber;
        idTarjeta = builder.idTarjeta;
        idUsuario = builder.idUsuario;
        carrierName = builder.carrierName;
        lat = builder.lat;
        lon = builder.lon;
    }

    public double getAmount() {
        return amount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getMainCode() {
        return mainCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public static class Builder {

        private double amount;
        private String countryCode;
        private String mainCode;
        private String phoneNumber;
        private int idTarjeta;
        private long idUsuario;
        private String carrierName;
        private double lat;
        private double lon;

        public BSPagoRequestEntity.Builder withAmount(double amount) {
            this.amount = amount;
            return this;
        }

        public BSPagoRequestEntity.Builder withCountryCode(String countryCode) {
            this.countryCode = countryCode;
            return this;
        }

        public BSPagoRequestEntity.Builder withMainCode(String mainCode) {
            this.mainCode = mainCode;
            return this;
        }

        public BSPagoRequestEntity.Builder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public BSPagoRequestEntity.Builder withIdTarjeta(int idTarjeta) {
            this.idTarjeta = idTarjeta;
            return this;
        }

        public BSPagoRequestEntity.Builder withIdUsuario(long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public BSPagoRequestEntity.Builder withCarrierName(String carrierName) {
            this.carrierName = carrierName;
            return this;
        }

        public BSPagoRequestEntity.Builder withtLat(double lat) {
            this.lat = lat;
            return this;
        }

        public BSPagoRequestEntity.Builder withLon(double lon) {
            this.lon = lon;
            return this;
        }

        public BSPagoRequestEntity build() {
            return new BSPagoRequestEntity(this);
        }
    }
}
