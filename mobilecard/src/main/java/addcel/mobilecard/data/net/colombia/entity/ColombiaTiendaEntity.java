package addcel.mobilecard.data.net.colombia.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ColombiaTiendaEntity {

    @SerializedName("BONOS")
    private List<BonoEntity> BONOS;

    @SerializedName("RECARGAS")
    private List<RecargaEntity> RECARGAS;

    @SerializedName("SERVICIOS")
    private List<ServicioEntity> SERVICIOS;

    public List<BonoEntity> getBONOS() {
        return BONOS;
    }

    public void setBONOS(List<BonoEntity> BONOS) {
        this.BONOS = BONOS;
    }

    public List<RecargaEntity> getRECARGAS() {
        return RECARGAS;
    }

    public void setRECARGAS(List<RecargaEntity> RECARGAS) {
        this.RECARGAS = RECARGAS;
    }

    public List<ServicioEntity> getSERVICIOS() {
        return SERVICIOS;
    }

    public void setSERVICIOS(List<ServicioEntity> SERVICIOS) {
        this.SERVICIOS = SERVICIOS;
    }

    public static class BonoEntity implements Parcelable {
        /**
         * descripcion_servicio : BONOS descripcion : BONOS PEAJES idproducto : 1019 tipo_servicio : 1
         * campos : [{"nombre":"Placa
         * Vehiculo","valor":"","tipo":"string","len":6,"variable":"placaVehiculo"},{"nombre":"Nombre
         * Destinatario","valor":"","tipo":"string","len":50,"variable":"nombreDestinatario"},{"nombre":"Email
         * Destinatario","valor":"","tipo":"email","len":50,"variable":"emailDestinatario"}]
         */
        @SerializedName("descripcion_servicio")
        private String descripcionServicio;

        @SerializedName("descripcion")
        private String descripcion;

        @SerializedName("idproducto")
        private String idproducto;

        @SerializedName("tipo_servicio")
        private String tipoServicio;

        @SerializedName("campos")
        private List<CampoBonoEntity> campos;

        BonoEntity(Parcel in) {
            descripcionServicio = in.readString();
            descripcion = in.readString();
            idproducto = in.readString();
            tipoServicio = in.readString();
            campos = in.createTypedArrayList(CampoBonoEntity.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(descripcionServicio);
            dest.writeString(descripcion);
            dest.writeString(idproducto);
            dest.writeString(tipoServicio);
            dest.writeTypedList(campos);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<BonoEntity> CREATOR = new Creator<BonoEntity>() {
            @Override
            public BonoEntity createFromParcel(Parcel in) {
                return new BonoEntity(in);
            }

            @Override
            public BonoEntity[] newArray(int size) {
                return new BonoEntity[size];
            }
        };

        public String getDescripcionServicio() {
            return descripcionServicio;
        }

        public void setDescripcionServicio(String descripcionServicio) {
            this.descripcionServicio = descripcionServicio;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getIdproducto() {
            return idproducto;
        }

        public void setIdproducto(String idproducto) {
            this.idproducto = idproducto;
        }

        public String getTipoServicio() {
            return tipoServicio;
        }

        public void setTipoServicio(String tipoServicio) {
            this.tipoServicio = tipoServicio;
        }

        public List<CampoBonoEntity> getCampos() {
            return campos;
        }

        public void setCampos(List<CampoBonoEntity> campos) {
            this.campos = campos;
        }

        public static class CampoBonoEntity implements Parcelable {
            /**
             * nombre : Placa Vehiculo valor : tipo : string len : 6 variable : placaVehiculo
             */
            @SerializedName("nombre")
            private String nombre;

            @SerializedName("valor")
            private String valor;

            @SerializedName("tipo")
            private String tipo;

            @SerializedName("len")
            private int len;

            @SerializedName("variable")
            private String variable;

            CampoBonoEntity(Parcel in) {
                nombre = in.readString();
                valor = in.readString();
                tipo = in.readString();
                len = in.readInt();
                variable = in.readString();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(nombre);
                dest.writeString(valor);
                dest.writeString(tipo);
                dest.writeInt(len);
                dest.writeString(variable);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<CampoBonoEntity> CREATOR = new Creator<CampoBonoEntity>() {
                @Override
                public CampoBonoEntity createFromParcel(Parcel in) {
                    return new CampoBonoEntity(in);
                }

                @Override
                public CampoBonoEntity[] newArray(int size) {
                    return new CampoBonoEntity[size];
                }
            };

            public String getNombre() {
                return nombre;
            }

            public void setNombre(String nombre) {
                this.nombre = nombre;
            }

            public String getValor() {
                return valor;
            }

            public void setValor(String valor) {
                this.valor = valor;
            }

            public String getTipo() {
                return tipo;
            }

            public void setTipo(String tipo) {
                this.tipo = tipo;
            }

            public int getLen() {
                return len;
            }

            public void setLen(int len) {
                this.len = len;
            }

            public String getVariable() {
                return variable;
            }

            public void setVariable(String variable) {
                this.variable = variable;
            }
        }
    }

    public static class RecargaEntity implements Parcelable {
        /**
         * descripcion_servicio : RECARGAS descripcion : Tiempo Aire idproducto : 1008 tipo_servicio : 1
         * campos : [{"nombre":"Numero
         * Celular","valor":"","tipo":"number","len":10,"variable":"numeroCelular"}]
         */
        @SerializedName("descripcion_servicio")
        private String descripcionServicio;

        @SerializedName("descripcion")
        private String descripcion;

        @SerializedName("idproducto")
        private String idproducto;

        @SerializedName("tipo_servicio")
        private String tipoServicio;

        @SerializedName("campos")
        private List<CampoRecargaEntity> campos;

        RecargaEntity(Parcel in) {
            descripcionServicio = in.readString();
            descripcion = in.readString();
            idproducto = in.readString();
            tipoServicio = in.readString();
            campos = in.createTypedArrayList(CampoRecargaEntity.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(descripcionServicio);
            dest.writeString(descripcion);
            dest.writeString(idproducto);
            dest.writeString(tipoServicio);
            dest.writeTypedList(campos);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<RecargaEntity> CREATOR = new Creator<RecargaEntity>() {
            @Override
            public RecargaEntity createFromParcel(Parcel in) {
                return new RecargaEntity(in);
            }

            @Override
            public RecargaEntity[] newArray(int size) {
                return new RecargaEntity[size];
            }
        };

        public String getDescripcionServicio() {
            return descripcionServicio;
        }

        public void setDescripcionServicio(String descripcionServicio) {
            this.descripcionServicio = descripcionServicio;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getIdproducto() {
            return idproducto;
        }

        public void setIdproducto(String idproducto) {
            this.idproducto = idproducto;
        }

        public String getTipoServicio() {
            return tipoServicio;
        }

        public void setTipoServicio(String tipoServicio) {
            this.tipoServicio = tipoServicio;
        }

        public List<CampoRecargaEntity> getCampos() {
            return campos;
        }

        public void setCampos(List<CampoRecargaEntity> campos) {
            this.campos = campos;
        }

        public static class CampoRecargaEntity implements Parcelable {
            /**
             * nombre : Numero Celular valor : tipo : number len : 10 variable : numeroCelular
             */
            @SerializedName("nombre")
            private String nombre;

            @SerializedName("valor")
            private String valor;

            @SerializedName("tipo")
            private String tipo;

            @SerializedName("len")
            private int len;

            @SerializedName("variable")
            private String variable;

            CampoRecargaEntity(Parcel in) {
                nombre = in.readString();
                valor = in.readString();
                tipo = in.readString();
                len = in.readInt();
                variable = in.readString();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(nombre);
                dest.writeString(valor);
                dest.writeString(tipo);
                dest.writeInt(len);
                dest.writeString(variable);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<CampoRecargaEntity> CREATOR = new Creator<CampoRecargaEntity>() {
                @Override
                public CampoRecargaEntity createFromParcel(Parcel in) {
                    return new CampoRecargaEntity(in);
                }

                @Override
                public CampoRecargaEntity[] newArray(int size) {
                    return new CampoRecargaEntity[size];
                }
            };

            public String getNombre() {
                return nombre;
            }

            public void setNombre(String nombre) {
                this.nombre = nombre;
            }

            public String getValor() {
                return valor;
            }

            public void setValor(String valor) {
                this.valor = valor;
            }

            public String getTipo() {
                return tipo;
            }

            public void setTipo(String tipo) {
                this.tipo = tipo;
            }

            public int getLen() {
                return len;
            }

            public void setLen(int len) {
                this.len = len;
            }

            public String getVariable() {
                return variable;
            }

            public void setVariable(String variable) {
                this.variable = variable;
            }
        }
    }

    public static class ServicioEntity implements Parcelable {
        /**
         * descripcion_servicio : SERVICIOS descripcion : Pago LuqueOspina idproducto : 1005
         * tipo_servicio : 0 campos : [{"nombre":"Cedula o
         * Nit","valor":"","tipo":"number","len":15,"variable":"documento"}]
         */
        @SerializedName("descripcion_servicio")
        private String descripcionServicio;

        @SerializedName("descripcion")
        private String descripcion;

        @SerializedName("idproducto")
        private String idproducto;

        @SerializedName("tipo_servicio")
        private String tipoServicio;

        @SerializedName("campos")
        private List<CampoServicioEntity> campos;

        ServicioEntity(Parcel in) {
            descripcionServicio = in.readString();
            descripcion = in.readString();
            idproducto = in.readString();
            tipoServicio = in.readString();
            campos = in.createTypedArrayList(CampoServicioEntity.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(descripcionServicio);
            dest.writeString(descripcion);
            dest.writeString(idproducto);
            dest.writeString(tipoServicio);
            dest.writeTypedList(campos);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ServicioEntity> CREATOR = new Creator<ServicioEntity>() {
            @Override
            public ServicioEntity createFromParcel(Parcel in) {
                return new ServicioEntity(in);
            }

            @Override
            public ServicioEntity[] newArray(int size) {
                return new ServicioEntity[size];
            }
        };

        public String getDescripcionServicio() {
            return descripcionServicio;
        }

        public void setDescripcionServicio(String descripcionServicio) {
            this.descripcionServicio = descripcionServicio;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getIdproducto() {
            return idproducto;
        }

        public void setIdproducto(String idproducto) {
            this.idproducto = idproducto;
        }

        public String getTipoServicio() {
            return tipoServicio;
        }

        public void setTipoServicio(String tipoServicio) {
            this.tipoServicio = tipoServicio;
        }

        public List<CampoServicioEntity> getCampos() {
            return campos;
        }

        public void setCampos(List<CampoServicioEntity> campos) {
            this.campos = campos;
        }

        public static class CampoServicioEntity implements Parcelable {
            /**
             * nombre : Cedula o Nit valor : tipo : number len : 15 variable : documento
             */
            @SerializedName("nombre")
            private String nombre;

            @SerializedName("valor")
            private String valor;

            @SerializedName("tipo")
            private String tipo;

            @SerializedName("len")
            private int len;

            @SerializedName("variable")
            private String variable;

            CampoServicioEntity(Parcel in) {
                nombre = in.readString();
                valor = in.readString();
                tipo = in.readString();
                len = in.readInt();
                variable = in.readString();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(nombre);
                dest.writeString(valor);
                dest.writeString(tipo);
                dest.writeInt(len);
                dest.writeString(variable);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<CampoServicioEntity> CREATOR =
                    new Creator<CampoServicioEntity>() {
                        @Override
                        public CampoServicioEntity createFromParcel(Parcel in) {
                            return new CampoServicioEntity(in);
                        }

                        @Override
                        public CampoServicioEntity[] newArray(int size) {
                            return new CampoServicioEntity[size];
                        }
                    };

            public String getNombre() {
                return nombre;
            }

            public void setNombre(String nombre) {
                this.nombre = nombre;
            }

            public String getValor() {
                return valor;
            }

            public void setValor(String valor) {
                this.valor = valor;
            }

            public String getTipo() {
                return tipo;
            }

            public void setTipo(String tipo) {
                this.tipo = tipo;
            }

            public int getLen() {
                return len;
            }

            public void setLen(int len) {
                this.len = len;
            }

            public String getVariable() {
                return variable;
            }

            public void setVariable(String variable) {
                this.variable = variable;
            }
        }
    }
}
