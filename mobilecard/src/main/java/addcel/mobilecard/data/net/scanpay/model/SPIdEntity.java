package addcel.mobilecard.data.net.scanpay.model;

/**
 * ADDCEL on 12/09/18.
 */
public final class SPIdEntity {
    private final int idError;
    private final String mensajeError;
    private final int idBitacora;

    private SPIdEntity(Builder builder) {
        idError = builder.idError;
        mensajeError = builder.mensajeError;
        idBitacora = builder.idBitacora;
    }

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public int getIdBitacora() {
        return idBitacora;
    }

    public static final class Builder {
        private int idError;
        private String mensajeError;
        private int idBitacora;

        public Builder() {
        }

        public Builder(SPIdEntity copy) {
            this.idError = copy.getIdError();
            this.mensajeError = copy.getMensajeError();
            this.idBitacora = copy.getIdBitacora();
        }

        public Builder setIdError(int idError) {
            this.idError = idError;
            return this;
        }

        public Builder setMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }

        public Builder setIdBitacora(int idBitacora) {
            this.idBitacora = idBitacora;
            return this;
        }

        public SPIdEntity build() {
            return new SPIdEntity(this);
        }
    }
}
