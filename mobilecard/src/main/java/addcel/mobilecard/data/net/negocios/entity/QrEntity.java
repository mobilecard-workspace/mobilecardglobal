package addcel.mobilecard.data.net.negocios.entity;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class QrEntity {

    @Expose
    private Long accountId;
    @Expose
    private Double amount;
    @Expose
    private String cardNumber;
    @Expose
    private Double comision;
    @Expose
    private String concept;
    @Expose
    private String cvv;
    @Expose
    private String expDate;
    @Expose
    private Long idCard;
    @Expose
    private Long idEstablecimiento;
    @Expose
    private Long idUser;
    @Expose
    private Double propina;
    @Expose
    private String referenciaNeg;
    @Expose
    private Long tipoTarjeta;

    public Long getAccountId() {
        return accountId;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public Double getComision() {
        return comision;
    }

    public String getConcept() {
        return concept;
    }

    public String getCvv() {
        return cvv;
    }

    public String getExpDate() {
        return expDate;
    }

    public Long getIdCard() {
        return idCard;
    }

    public Long getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public Long getIdUser() {
        return idUser;
    }

    public Double getPropina() {
        return propina;
    }

    public String getReferenciaNeg() {
        return referenciaNeg;
    }

    public Long getTipoTarjeta() {
        return tipoTarjeta;
    }

    public static class Builder {

        private Long accountId;
        private Double amount;
        private String cardNumber;
        private Double comision;
        private String concept;
        private String cvv;
        private String expDate;
        private Long idCard;
        private Long idEstablecimiento;
        private Long idUser;
        private Double propina;
        private String referenciaNeg;
        private Long tipoTarjeta;

        public QrEntity.Builder withAccountId(Long accountId) {
            this.accountId = accountId;
            return this;
        }

        public QrEntity.Builder withAmount(Double amount) {
            this.amount = amount;
            return this;
        }

        public QrEntity.Builder withCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public QrEntity.Builder withComision(Double comision) {
            this.comision = comision;
            return this;
        }

        public QrEntity.Builder withConcept(String concept) {
            this.concept = concept;
            return this;
        }

        public QrEntity.Builder withCvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public QrEntity.Builder withExpDate(String expDate) {
            this.expDate = expDate;
            return this;
        }

        public QrEntity.Builder withIdCard(Long idCard) {
            this.idCard = idCard;
            return this;
        }

        public QrEntity.Builder withIdEstablecimiento(Long idEstablecimiento) {
            this.idEstablecimiento = idEstablecimiento;
            return this;
        }

        public QrEntity.Builder withIdUser(Long idUser) {
            this.idUser = idUser;
            return this;
        }

        public QrEntity.Builder withPropina(Double propina) {
            this.propina = propina;
            return this;
        }

        public QrEntity.Builder withReferenciaNeg(String referenciaNeg) {
            this.referenciaNeg = referenciaNeg;
            return this;
        }

        public QrEntity.Builder withTipoTarjeta(Long tipoTarjeta) {
            this.tipoTarjeta = tipoTarjeta;
            return this;
        }

        public QrEntity build() {
            QrEntity qrEntity = new QrEntity();
            qrEntity.accountId = accountId;
            qrEntity.amount = amount;
            qrEntity.cardNumber = cardNumber;
            qrEntity.comision = comision;
            qrEntity.concept = concept;
            qrEntity.cvv = cvv;
            qrEntity.expDate = expDate;
            qrEntity.idCard = idCard;
            qrEntity.idEstablecimiento = idEstablecimiento;
            qrEntity.idUser = idUser;
            qrEntity.propina = propina;
            qrEntity.referenciaNeg = referenciaNeg;
            qrEntity.tipoTarjeta = tipoTarjeta;
            return qrEntity;
        }
    }
}
