package addcel.mobilecard.data.net.colombia.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * ADDCEL on 11/1/18.
 */
public final class ColombiaServicioConsultaResponseEntity implements Parcelable {

    /**
     * comision : 0 idAplicacion : 1 idError : 0 idProducto : 1027 mensajeError : String monto :
     * 10000
     * referencias : [{"len":50,"nombre":"Opcion de
     * Pago","tipo":"string","valor":"Directo","variable":"OpcionPago"},{"len":40,"nombre":"Referencia
     * Producto","tipo":"number","valor":"334563","variable":"ReferenciaProducto"},{"len":40,"nombre":"Descripcion","tipo":"string","valor":"","variable":"Descripcion"},{"len":10,"nombre":"Valor
     * a Pagar","tipo":"string","valor":"10000","variable":"valueToPay"}] valorImpuesto : 0
     */
    @SerializedName("comision")
    private double comision;

    @SerializedName("idAplicacion")
    private int idAplicacion;

    @SerializedName("idError")
    private int idError;

    @SerializedName("idProducto")
    private String idProducto;

    @SerializedName("mensajeError")
    private String mensajeError;

    @SerializedName("monto")
    private double monto;

    @SerializedName("valorImpuesto")
    private double valorImpuesto;

    @SerializedName("referencias")
    private List<ReferenciasEntity> referencias;

    private ColombiaServicioConsultaResponseEntity(Parcel in) {
        comision = in.readDouble();
        idAplicacion = in.readInt();
        idError = in.readInt();
        idProducto = in.readString();
        mensajeError = in.readString();
        monto = in.readDouble();
        valorImpuesto = in.readDouble();
        referencias = in.createTypedArrayList(ReferenciasEntity.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(comision);
        dest.writeInt(idAplicacion);
        dest.writeInt(idError);
        dest.writeString(idProducto);
        dest.writeString(mensajeError);
        dest.writeDouble(monto);
        dest.writeDouble(valorImpuesto);
        dest.writeTypedList(referencias);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ColombiaServicioConsultaResponseEntity> CREATOR =
            new Creator<ColombiaServicioConsultaResponseEntity>() {
                @Override
                public ColombiaServicioConsultaResponseEntity createFromParcel(Parcel in) {
                    return new ColombiaServicioConsultaResponseEntity(in);
                }

                @Override
                public ColombiaServicioConsultaResponseEntity[] newArray(int size) {
                    return new ColombiaServicioConsultaResponseEntity[size];
                }
            };

    public double getComision() {
        return comision;
    }

    public void setComision(double comision) {
        this.comision = comision;
    }

    public int getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(int idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double getValorImpuesto() {
        return valorImpuesto;
    }

    public void setValorImpuesto(double valorImpuesto) {
        this.valorImpuesto = valorImpuesto;
    }

    public List<ReferenciasEntity> getReferencias() {
        return referencias;
    }

    public void setReferencias(List<ReferenciasEntity> referencias) {
        this.referencias = referencias;
    }

    public static class ReferenciasEntity implements Parcelable {
        /**
         * len : 50 nombre : Opcion de Pago tipo : string valor : Directo variable : OpcionPago
         */
        @SerializedName("len")
        private int len;

        @SerializedName("nombre")
        private String nombre;

        @SerializedName("tipo")
        private String tipo;

        @SerializedName("valor")
        private String valor;

        @SerializedName("variable")
        private String variable;

        ReferenciasEntity(Parcel in) {
            len = in.readInt();
            nombre = in.readString();
            tipo = in.readString();
            valor = in.readString();
            variable = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(len);
            dest.writeString(nombre);
            dest.writeString(tipo);
            dest.writeString(valor);
            dest.writeString(variable);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ReferenciasEntity> CREATOR = new Creator<ReferenciasEntity>() {
            @Override
            public ReferenciasEntity createFromParcel(Parcel in) {
                return new ReferenciasEntity(in);
            }

            @Override
            public ReferenciasEntity[] newArray(int size) {
                return new ReferenciasEntity[size];
            }
        };

        public int getLen() {
            return len;
        }

        public void setLen(int len) {
            this.len = len;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getTipo() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public String getValor() {
            return valor;
        }

        public void setValor(String valor) {
            this.valor = valor;
        }

        public String getVariable() {
            return variable;
        }

        public void setVariable(String variable) {
            this.variable = variable;
        }
    }
}
