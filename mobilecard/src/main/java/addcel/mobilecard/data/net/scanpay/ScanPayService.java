package addcel.mobilecard.data.net.scanpay;

import java.util.List;

import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity;
import addcel.mobilecard.data.net.scanpay.model.SPCuentaEntity;
import addcel.mobilecard.data.net.scanpay.model.SPIdEntity;
import io.reactivex.Observable;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

/**
 * ADDCEL on 21/08/18.
 */
public interface ScanPayService {

    String ENQUEUE_PAYMENT = "CuantoTeDebo/{idApp}/{idPais}/{idioma}/{tipoUsuario}/payment";
    String ENQUEUE_PAYMENT_BY_ID = "CuantoTeDebo/{idApp}/{idPais}/{idioma}/paymentByIdBP";

    String ERROR_PREVIO = "CuantoTeDebo/payworks/error_previo_pago";
    String AUTH_FINISHED = "CuantoTeDebo/3d/secure/response";
    String PROCESS_FINISHED = "CuantoTeDebo/payworks/response";

    @GET("CuantoTeDebo/{idApp}/{idPais}/{idioma}/{tipoUsuario}/generatePaymentIdBP")
    Observable<SPIdEntity> getId(@Path("idApp") int idApp, @Path("idPais") int idPais,
                                 @Path("idioma") String idioma, @Path("tipoUsuario") String tipoUsuario,
                                 @Query("concept") String concept, @Query("establecimientoId") long establecimientoId,
                                 @Query("amount") double amount, @Query("comision") double comision,
                                 @Query("referenciaNeg") String referenciaNeg, @Query("propina") double propina);

    @Streaming
    @Headers("Content-Type: application/json")
    @POST("CuantoTeDebo/{idApp}/{idPais}/{idioma}/getEstablecimientos")
    Observable<ResponseBody> getEstablecimientos(@Path("idApp") int idApp, @Path("idPais") int idPais,
                                                 @Path("idioma") String idioma, @Body RequestBody body);

    @FormUrlEncoded
    @POST("CuantoTeDebo/{idApp}/{idPais}/{idioma}/getComision")
    Observable<SPComisionEntity> checkComision(@Path("idApp") int idApp, @Path("idPais") int idPais,
                                               @Path("idioma") String idioma, @Field("idCommerce") long idEstablecimiento);

    @GET("CuantoTeDebo/{idApp}/{idPais}/{idioma}/resultByID")
    Observable<SPReceiptEntity> getResultByID(@Path("idApp") int idApp, @Path("idPais") int idPais,
                                              @Path("idioma") String idioma, @Query("idBitacora") long idBitacora);

    @GET("CuantoTeDebo/{idApp}/{idPais}/{idioma}/{id}/cuentas")
    Observable<List<SPCuentaEntity>> getBills(@Path("idApp") int idApp, @Path("idioma") String idioma,
                                              @Path("idPais") int idPais, @Path("id") long id);

    @FormUrlEncoded
    @POST(ENQUEUE_PAYMENT)
    Observable<String> payment(@Path("idApp") int idApp,
                               @Path("idPais") int idPais, @Path("idioma") String idioma,
                               @Path("tipoUsuario") String tipoUsuario, @Field("vigencia") String vigencia,
                               @Field("amount") double amount, @Field("msi") int msi,
                               @Field("tipoTarjetaEntity") String tipoTarjeta, @Field("idBitacora") int idBitacora,
                               @Field("idCard") int idCard, @Field("concept") String concept,
                               @Field("propina") double propina, @Field("lon") double lon, @Field("firma") String firma,
                               @Field("idUser") long idUser, @Field("ct") String ct,
                               @Field("referenciaNeg") String referenciaNeg, @Field("comision") double comision,
                               @Field("establecimientoId") int establecimientoId, @Field("tarjeta") String tarjeta,
                               @Field("lat") double lat);

    @FormUrlEncoded
    @POST(ENQUEUE_PAYMENT_BY_ID)
    Observable<String> paymentById(
            @Path("idApp") int idApp, @Path("idPais") int idPais, @Path("idioma") String idioma,
            @Field("vigencia") String vigencia, @Field("amount") double amount, @Field("msi") int msi,
            @Field("tipoTarjetaEntity") String tipoTarjeta, @Field("idBitacora") int idBitacora,
            @Field("idCard") int idCard, @Field("concept") String concept,
            @Field("propina") double propina, @Field("lon") double lon, @Field("firma") String firma,
            @Field("idUser") long idUser, @Field("ct") String ct,
            @Field("referenciaNeg") String referenciaNeg, @Field("comision") double comision,
            @Field("establecimientoId") int establecimientoId, @Field("tarjeta") String tarjeta,
            @Field("lat") double lat);
}
