package addcel.mobilecard.data.net.scanpay

import addcel.mobilecard.data.net.scanpay.model.SPComisionEntity
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 4/2/19.
 */

@Parcelize
data class SpEstablecimiento(
        val alias: String, val comision_fija: Double,
        val comision_porcentaje: Double, val correo: String, val id: Int, val telefono: String,
        val urlLogo: String
) : Parcelable

@Parcelize
data class SPPagoEntity(
        val amount: Double = 0.0,
        val comision: Double = 0.0,
        val concept: String = "",
        val ct: String = "",
        val establecimientoId: Int = 0,
        val firma: String = "",
        val idBitacora: Int = 0,
        val idCard: Int = 0,
        val idUser: Long = 0L,
        val imei: String = "",
        val lat: Double = 0.0,
        val lon: Double = 0.0,
        val modelo: String = "",
        val msi: Int = 0,
        val propina: Double = 0.0,
        val referenciaNeg: String = "",
        val software: String = "",
        val tarjeta: String = "",
        val tipoTarjeta: String = "",
        val vigencia: String = "",
        val qrBase64: String = "",
        val nombre: String? = "",
        val apellido: String? = "",
        val email: String? = "",
        val celular: String? = ""
) : Parcelable {

    override fun toString(): String {
        return "SPPagoEntity(amount=$amount, comision=$comision, concept='$concept', ct='$ct', establecimientoId=$establecimientoId, idBitacora=$idBitacora, idCard=$idCard, idUser=$idUser, imei='$imei', lat=$lat, lon=$lon, modelo='$modelo', msi=$msi, propina=$propina, referenciaNeg='$referenciaNeg', software='$software', tarjeta='$tarjeta', tipoTarjeta='$tipoTarjeta', vigencia='$vigencia', qrBase64='$qrBase64', nombre='$nombre', apellido='$apellido')"
    }
}

@Parcelize
data class SPReceiptEntity(
        val code: Int, val amount: Double,
        val authNumber: String? = "", val dateTime: Long, val idTransaccion: Int,
        val maskedPAN: String? = "", val message: String? = "", val opId: Int, val status: Int,
        val ticketUrl: String? = "", val txnISOCode: String? = ""
) : Parcelable {
    constructor(status: Int, message: String) : this(
            status, 0.0, "", 0L, 0, "", message, 0, status,
            "", ""
    )

    constructor(code: Int, status: Int, message: String) : this(
            code, 0.0, "", 0L, 0, "", message, 0, status,
            "", ""
    )
}

enum class QrType {
    ESTATICO,
}

data class QrRequest(
        @SerializedName("idEstablecimiento")
        val idEstablecimiento: Int,
        @SerializedName("idPais")
        val idPais: Int,
        @SerializedName("monto")
        val monto: Double,
        @SerializedName("typeQR")
        val typeQR: QrType
)

data class QrResponse(
        @SerializedName("idError")
        val idError: Int,
        @SerializedName("mensajeError")
        val mensajeError: String,
        @SerializedName("qrBase64")
        val qrBase64: String
)

interface SPApi {

    @FormUrlEncoded
    @POST("CuantoTeDebo/{idApp}/{idPais}/{idioma}/getComision")
    fun checkComision(
            @Path("idApp") idApp: Int, @Path("idPais") idPais: Int, @Path("idioma") idioma: String,
            @Field("idCommerce") idEstablecimiento: Long
    ): Observable<SPComisionEntity>

    @POST("CuantoTeDebo/{idApp}/{idPais}/{idioma}/{tipoUsuario}/{accountId}/pagoBP")
    fun pagoBP(
            @Header("Authorization") auth: String, @Path("idApp") idApp: Int, @Path("idPais") idPais: Int,
            @Path("idioma") idioma: String, @Path("tipoUsuario") tipoUsuario: String, @Path("accountId")
            accountId: String, @Body body: SPPagoEntity
    ): Observable<SPReceiptEntity>

    @POST("CuantoTeDebo/{idApp}/{idioma}/generateQR")
    fun generateQr(@Path("idApp") idApp: Int, @Path("idioma") idioma: String, @Body qrRequest: QrRequest): Observable<QrResponse>

    @FormUrlEncoded
    @POST("CuantoTeDebo/{idPais}/{idApp}/{idioma}/{accountId}/paymentByIdBP")
    fun pagoBPById(
            @Header("Authorization") auth: String, @Path("idApp") idApp: Int, @Path("idPais")
            idPais: Int, @Path("idioma") idioma: String, @Path("accountId") accountId: String,
            @Field("idUser") idUser: Long, @Field("idCard") idCard: Int, @Field("idBitacora")
            idBitacora: Int
    ): Observable<SPReceiptEntity>

    companion object {
        const val TIPO_USUARIO = "USUARIO"
        const val TIPO_NEGOCIO = "NEGOCIO"

        fun provide(retrofit: Retrofit): SPApi {
            return retrofit.create(SPApi::class.java)
        }
    }
}
