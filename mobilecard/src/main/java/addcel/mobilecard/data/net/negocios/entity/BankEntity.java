package addcel.mobilecard.data.net.negocios.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public class BankEntity {

    @Expose
    private String clave;
    @SerializedName("comision_fija")
    private Double comisionFija;
    @SerializedName("comision_porcentaje")
    private Double comisionPorcentaje;
    @Expose
    private int id;
    @SerializedName("nombre_corto")
    private String nombreCorto;
    @SerializedName("nombre_razon_social")
    private String nombreRazonSocial;

    public String getClave() {
        return clave;
    }

    public Double getComisionFija() {
        return comisionFija;
    }

    public Double getComisionPorcentaje() {
        return comisionPorcentaje;
    }

    public int getId() {
        return id;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public String getNombreRazonSocial() {
        return nombreRazonSocial;
    }

    @NotNull
    @Override
    public String toString() {
        return nombreCorto;
    }

    public static class Builder {

        private String clave;
        private Double comisionFija;
        private Double comisionPorcentaje;
        private int id;
        private String nombreCorto;
        private String nombreRazonSocial;

        public BankEntity.Builder withClave(String clave) {
            this.clave = clave;
            return this;
        }

        public BankEntity.Builder withComisionFija(Double comisionFija) {
            this.comisionFija = comisionFija;
            return this;
        }

        public BankEntity.Builder withComisionPorcentaje(Double comisionPorcentaje) {
            this.comisionPorcentaje = comisionPorcentaje;
            return this;
        }

        public BankEntity.Builder withId(int id) {
            this.id = id;
            return this;
        }

        public BankEntity.Builder withNombreCorto(String nombreCorto) {
            this.nombreCorto = nombreCorto;
            return this;
        }

        public BankEntity.Builder withNombreRazonSocial(String nombreRazonSocial) {
            this.nombreRazonSocial = nombreRazonSocial;
            return this;
        }

        public BankEntity build() {
            BankEntity bank = new BankEntity();
            bank.clave = clave;
            bank.comisionFija = comisionFija;
            bank.comisionPorcentaje = comisionPorcentaje;
            bank.id = id;
            bank.nombreCorto = nombreCorto;
            bank.nombreRazonSocial = nombreRazonSocial;
            return bank;
        }
    }
}
