package addcel.mobilecard.data.net.apto.accounts

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


/**
 * ADDCEL on 2019-10-23.
 */
data class AptoTransactionResponse(
        @SerializedName("data")
        val tData: List<Data>,
        @SerializedName("has_more")
        val hasMore: Boolean,
        @SerializedName("page")
        val page: Int,
        @SerializedName("rows")
        val rows: Int,
        @SerializedName("total_count")
        val totalCount: Int,
        @SerializedName("type")
        val type: String
)

@Parcelize
data class Data(
        @SerializedName("adjustments")
        val adjustments: Adjustments,
        @SerializedName("billing_amount")
        val billingAmount: BillingAmountX,
        @SerializedName("card_present")
        val cardPresent: Boolean,
        @SerializedName("cashback_amount")
        val cashbackAmount: CashbackAmount,
        @SerializedName("created_at")
        val createdAt: Double,
        @SerializedName("decline_code")
        val declineCode: String?,
        @SerializedName("decline_reason")
        val declineReason: String?,
        @SerializedName("description")
        val description: String,
        @SerializedName("ecommerce")
        val ecommerce: Boolean,
        @SerializedName("emv")
        val emv: Boolean,
        @SerializedName("fee_amount")
        val feeAmount: FeeAmount,
        @SerializedName("funding_source_name")
        val fundingSourceName: String,
        @SerializedName("hold_amount")
        val holdAmount: HoldAmount,
        @SerializedName("id")
        val id: String,
        @SerializedName("international")
        val international: Boolean,
        @SerializedName("iso_created_at")
        val isoCreatedAt: String,
        @SerializedName("last_message")
        val lastMessage: String?,
        @SerializedName("local_amount")
        val localAmount: LocalAmountX,
        @SerializedName("merchant")
        val merchant: Merchant,
        @SerializedName("native_balance")
        val nativeBalance: NativeBalance,
        @SerializedName("network")
        val network: String,
        @SerializedName("settlement")
        val settlement: Settlement,
        @SerializedName("state")
        val state: String,
        @SerializedName("store")
        val store: Store,
        @SerializedName("transaction_type")
        val transactionType: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Adjustments(
        @SerializedName("data")
        val `data`: List<DataX>,
        @SerializedName("has_more")
        val hasMore: Boolean,
        @SerializedName("page")
        val page: Int,
        @SerializedName("rows")
        val rows: Int,
        @SerializedName("total_count")
        val totalCount: Int,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class DataX(
        @SerializedName("adjustment_type")
        val adjustmentType: String,
        @SerializedName("billing_amount")
        val billingAmount: BillingAmount,
        @SerializedName("created_at")
        val createdAt: Double,
        @SerializedName("exchange_rate")
        val exchangeRate: Double,
        @SerializedName("external_id")
        val externalId: String,
        @SerializedName("fee")
        val fee: Fee,
        @SerializedName("funding_source_name")
        val fundingSourceName: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("iso_created_at")
        val isoCreatedAt: String,
        @SerializedName("local_amount")
        val localAmount: LocalAmount,
        @SerializedName("native_amount")
        val nativeAmount: NativeAmount,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class BillingAmount(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Fee(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class LocalAmount(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class NativeAmount(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class BillingAmountX(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class CashbackAmount(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class FeeAmount(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class HoldAmount(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class LocalAmountX(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Merchant(
        @SerializedName("id")
        val id: String?,
        @SerializedName("key")
        val key: String?,
        @SerializedName("mcc")
        val mcc: Mcc,
        @SerializedName("name")
        val name: String?,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Mcc(
        @SerializedName("code")
        val code: Int,
        @SerializedName("icon")
        val icon: String,
        @SerializedName("name")
        val name: String?,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class NativeBalance(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Settlement(
        @SerializedName("amount")
        val amount: String?,
        @SerializedName("date")
        val date: Int,
        @SerializedName("iso_date")
        val isoDate: String?,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Store(
        @SerializedName("address")
        val address: Address,
        @SerializedName("id")
        val id: String?,
        @SerializedName("key")
        val key: String?,
        @SerializedName("location")
        val location: Location,
        @SerializedName("name")
        val name: String,
        @SerializedName("type")
        val type: String
) : Parcelable

@Parcelize
data class Address(
        @SerializedName("apt")
        val apt: String?,
        @SerializedName("city")
        val city: String,
        @SerializedName("country")
        val country: String,
        @SerializedName("state")
        val state: String,
        @SerializedName("street")
        val street: String?,
        @SerializedName("type")
        val type: String,
        @SerializedName("zip_code")
        val zipCode: String?
) : Parcelable

@Parcelize
data class Location(
        @SerializedName("latitude")
        val latitude: Double?,
        @SerializedName("longitude")
        val longitude: Double?,
        @SerializedName("type")
        val type: String
) : Parcelable