package addcel.mobilecard.data.net.catalogo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.List;

@SuppressWarnings("unused")
public class CatalogoResponse {

    @SerializedName("categorias")
    private List<CategoriaEntity> mCategorias;

    @SerializedName("idError")
    private int mIdError;

    @SerializedName("mensajeError")
    private String mMensajeError;

    @SerializedName("montos")
    private List<MontoEntity> mMontos;

    @SerializedName("recargas")
    private List<RecargaEntity> mRecargas;

    @SerializedName("servicios")
    private List<RecargaEntity> mServicios;

    public CatalogoResponse() {
    }

    private CatalogoResponse(int mIdError, String mMensajeError) {
        this.mIdError = mIdError;
        this.mMensajeError = mMensajeError;
    }

    public CatalogoResponse(List<CategoriaEntity> mCategorias, int mIdError, String mMensajeError,
                            List<MontoEntity> mMontos, List<RecargaEntity> mRecargas, List<RecargaEntity> mServicios) {
        this(mIdError, mMensajeError);
        this.mCategorias = mCategorias;
        this.mMontos = mMontos;
        this.mRecargas = mRecargas;
        this.mServicios = mServicios;
    }

    public List<CategoriaEntity> getCategorias() {
        return mCategorias;
    }

    public int getIdError() {
        return mIdError;
    }

    public String getMensajeError() {
        return mMensajeError;
    }

    public List<MontoEntity> getMontos() {
        return mMontos;
    }

    public List<RecargaEntity> getRecargas() {
        return mRecargas;
    }

    public void setmRecargas(List<RecargaEntity> mRecargas) {
        this.mRecargas = mRecargas;
    }

    public List<RecargaEntity> getServicios() {
        return mServicios;
    }

    public static final class CategoriaEntity implements Parcelable {

        public static final Creator<CategoriaEntity> CREATOR = new Creator<CategoriaEntity>() {
            @Override
            public CategoriaEntity createFromParcel(Parcel in) {
                return new CategoriaEntity(in);
            }

            @Override
            public CategoriaEntity[] newArray(int size) {
                return new CategoriaEntity[size];
            }
        };

        @SerializedName("descripcion")
        private String mDescripcion;

        @SerializedName("id")
        private int mId;

        protected CategoriaEntity(Parcel in) {
            mDescripcion = in.readString();
            mId = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mDescripcion);
            dest.writeInt(mId);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getDescripcion() {
            return mDescripcion;
        }

        public void setDescripcion(String descripcion) {
            mDescripcion = descripcion;
        }

        public int getId() {
            return mId;
        }

        public void setId(int id) {
            mId = id;
        }

        @NotNull
        @Override
        public String toString() {
            return mDescripcion;
        }
    }

    @SuppressWarnings("unused")
    public static class MontoEntity implements Parcelable {

        public static final Creator<MontoEntity> CREATOR = new Creator<MontoEntity>() {
            @Override
            public MontoEntity createFromParcel(Parcel in) {
                return new MontoEntity(in);
            }

            @Override
            public MontoEntity[] newArray(int size) {
                return new MontoEntity[size];
            }
        };

        @SerializedName("comision")
        private double mComision;

        @SerializedName("concepto")
        private String mConcepto;

        @SerializedName("descripcion")
        private String mDescripcion;

        @SerializedName("emisor")
        private String mEmisor;

        @SerializedName("id")
        private int mId;

        @SerializedName("monto")
        private double mMonto;

        @SerializedName("operacion")
        private String mOperacion;

        protected MontoEntity(Parcel in) {
            mComision = in.readDouble();
            mConcepto = in.readString();
            mDescripcion = in.readString();
            mEmisor = in.readString();
            mId = in.readInt();
            mMonto = in.readDouble();
            mOperacion = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(mComision);
            dest.writeString(mConcepto);
            dest.writeString(mDescripcion);
            dest.writeString(mEmisor);
            dest.writeInt(mId);
            dest.writeDouble(mMonto);
            dest.writeString(mOperacion);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public double getComision() {
            return mComision;
        }

        public void setComision(double comision) {
            mComision = comision;
        }

        public String getConcepto() {
            return mConcepto;
        }

        public void setConcepto(String concepto) {
            mConcepto = concepto;
        }

        public String getDescripcion() {
            return mDescripcion;
        }

        public void setDescripcion(String descripcion) {
            mDescripcion = descripcion;
        }

        public String getEmisor() {
            return mEmisor;
        }

        public void setEmisor(String emisor) {
            mEmisor = emisor;
        }

        public int getId() {
            return mId;
        }

        public void setId(int id) {
            mId = id;
        }

        public double getMonto() {
            return mMonto;
        }

        public void setMonto(double monto) {
            mMonto = monto;
        }

        public String getOperacion() {
            return mOperacion;
        }

        public void setOperacion(String operacion) {
            mOperacion = operacion;
        }

        @NotNull
        @Override
        public String toString() {
            return mDescripcion;
        }
    }

    @SuppressWarnings("unused")
    public static final class RecargaEntity implements Parcelable {

        public static final Creator<RecargaEntity> CREATOR = new Creator<RecargaEntity>() {
            @Override
            public RecargaEntity createFromParcel(Parcel in) {
                return new RecargaEntity(in);
            }

            @Override
            public RecargaEntity[] newArray(int size) {
                return new RecargaEntity[size];
            }
        };

        @SerializedName("descripcion")
        private String mDescripcion;

        @SerializedName("esAntad")
        private boolean mEsAntad;

        @SerializedName("id")
        private int mId;

        public RecargaEntity(String mDescripcion, boolean mEsAntad, int mId) {
            this.mDescripcion = mDescripcion;
            this.mEsAntad = mEsAntad;
            this.mId = mId;
        }

        protected RecargaEntity(Parcel in) {
            mDescripcion = in.readString();
            mEsAntad = in.readByte() != 0;
            mId = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mDescripcion);
            dest.writeByte((byte) (mEsAntad ? 1 : 0));
            dest.writeInt(mId);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getDescripcion() {
            return mDescripcion;
        }

        public void setDescripcion(String descripcion) {
            mDescripcion = descripcion;
        }

        public boolean getEsAntad() {
            return mEsAntad;
        }

        public void setEsAntad(boolean esAntad) {
            mEsAntad = esAntad;
        }

        public int getId() {
            return mId;
        }

        public void setId(int id) {
            mId = id;
        }

        @NotNull
        @Override
        public String toString() {
            return mDescripcion;
        }
    }
}
