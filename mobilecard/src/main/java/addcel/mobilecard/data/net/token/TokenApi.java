package addcel.mobilecard.data.net.token;

import addcel.mobilecard.data.net.transacto.entity.PagoBpResponse;
import addcel.mobilecard.data.net.transacto.entity.PagoEntity;
import addcel.mobilecard.data.net.transacto.entity.TokenEntity;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Transacto/{idApp}/{idioma}/{method}";
 * <p>
 * String SALDO = "procesaConsultaSaldos";
 * String TOKEN = "getToken";
 */
public interface TokenApi {
    @POST("Transacto/{idApp}/{idioma}/getToken")
    Observable<TokenEntity> getToken(
            @Header("Authorization") String auth, @Header("Profile") String profile,
            @Path("idApp") int idApp, @Path("idioma") String idioma);

    @POST("Transacto/{idApp}/{idPais}/{idioma}/{accountId}/pagoBP")
    Observable<PagoBpResponse> executePagoBP(@Header("Authorization") String auth,
                                             @Path("idApp") int idApp, @Path("idPais") int idPais, @Path("idioma") String idioma,
                                             @Path("accountId") String accountId, @Body PagoEntity pago);
}

