package addcel.mobilecard.data.net.usuarios.model;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 16/08/17.
 */
public final class UserValidation extends McResponse {
    @SerializedName("id_usuario")
    private long idUSuario;

    @SerializedName("usr_telefono")
    private String usrTelefono;

    @SerializedName("usr_nombre")
    private String usrNombre;

    @SerializedName("usr_apellido")
    private String usrApellido;

    @SerializedName("id_usr_status")
    private Integer idUsrStatus;

    @SerializedName("idpais")
    private Integer idPais;

    @SerializedName("usr_jumio")
    private String usrJumio;

    @SerializedName("scanReference")
    private String scanReference;

    public UserValidation() {
    }

    public UserValidation(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    private UserValidation(Builder builder) {
        setIdError(builder.idError);
        setMensajeError(builder.mensajeError);
        idUSuario = builder.idUSuario;
        usrTelefono = builder.usrTelefono;
        usrNombre = builder.usrNombre;
        usrApellido = builder.usrApellido;
        idUsrStatus = builder.idUsrStatus;
        idPais = builder.idPais;
        usrJumio = builder.usrJumio;
        scanReference = builder.scanReference;
    }

    public long getIdUSuario() {
        return idUSuario;
    }

    public String getUsrTelefono() {
        return usrTelefono;
    }

    public String getUsrNombre() {
        return usrNombre;
    }

    public String getUsrApellido() {
        return usrApellido;
    }

    public Integer getIdUsrStatus() {
        return idUsrStatus;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public String getUsrJumio() {
        return usrJumio;
    }

    public String getScanReference() {
        return scanReference;
    }

    public static final class Builder {
        private int idError;
        private String mensajeError;
        private long idUSuario;
        private String usrTelefono;
        private String usrNombre;
        private String usrApellido;
        private Integer idUsrStatus;
        private Integer idPais;
        private String usrJumio;
        private String scanReference;

        public Builder() {
        }

        public Builder(UserValidation copy) {
            this.idError = copy.getIdError();
            this.mensajeError = copy.getMensajeError();
            this.idUSuario = copy.getIdUSuario();
            this.usrTelefono = copy.getUsrTelefono();
            this.usrNombre = copy.getUsrNombre();
            this.usrApellido = copy.getUsrApellido();
            this.idUsrStatus = copy.getIdUsrStatus();
            this.idPais = copy.getIdPais();
            this.usrJumio = copy.getUsrJumio();
            this.scanReference = copy.getScanReference();
        }

        public Builder setIdError(int idError) {
            this.idError = idError;
            return this;
        }

        public Builder setMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }

        public Builder setIdUSuario(long idUSuario) {
            this.idUSuario = idUSuario;
            return this;
        }

        public Builder setUsrTelefono(String usrTelefono) {
            this.usrTelefono = usrTelefono;
            return this;
        }

        public Builder setUsrNombre(String usrNombre) {
            this.usrNombre = usrNombre;
            return this;
        }

        public Builder setUsrApellido(String usrApellido) {
            this.usrApellido = usrApellido;
            return this;
        }

        public Builder setIdUsrStatus(Integer idUsrStatus) {
            this.idUsrStatus = idUsrStatus;
            return this;
        }

        public Builder setIdPais(Integer idPais) {
            this.idPais = idPais;
            return this;
        }

        public Builder setUsrJumio(String usrJumio) {
            this.usrJumio = usrJumio;
            return this;
        }

        public Builder setScanReference(String scanReference) {
            this.scanReference = scanReference;
            return this;
        }

        public UserValidation build() {
            return new UserValidation(this);
        }
    }
}
