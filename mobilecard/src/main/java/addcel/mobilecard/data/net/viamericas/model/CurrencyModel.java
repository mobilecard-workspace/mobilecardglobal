package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public class CurrencyModel implements Parcelable {

    public static final Creator<CurrencyModel> CREATOR = new Creator<CurrencyModel>() {
        @Override
        public CurrencyModel createFromParcel(Parcel in) {
            return new CurrencyModel(in);
        }

        @Override
        public CurrencyModel[] newArray(int size) {
            return new CurrencyModel[size];
        }
    };
    @SerializedName("ID")
    private String mID;
    @SerializedName("ISO_CURRENCY")
    private String mISOCURRENCY;
    @SerializedName("NAME")
    private String mNAME;

    public CurrencyModel() {
    }

    public CurrencyModel(String mID, String mNAME) {
        this.mID = mID;
        this.mNAME = mNAME;
    }

    protected CurrencyModel(Parcel in) {
        mID = in.readString();
        mISOCURRENCY = in.readString();
        mNAME = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mID);
        dest.writeString(mISOCURRENCY);
        dest.writeString(mNAME);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getID() {
        return mID;
    }

    public void setID(String ID) {
        mID = ID;
    }

    public String getISOCURRENCY() {
        return mISOCURRENCY;
    }

    public void setISOCURRENCY(String ISOCURRENCY) {
        mISOCURRENCY = ISOCURRENCY;
    }

    public String getNAME() {
        return mNAME;
    }

    public void setNAME(String NAME) {
        mNAME = NAME;
    }

    @NotNull
    @Override
    public String toString() {
        return mNAME;
    }
}
