package addcel.mobilecard.data.net.transacto.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public final class TokenEntity implements Parcelable {

    public static final Creator<TokenEntity> CREATOR = new Creator<TokenEntity>() {
        @Override
        public TokenEntity createFromParcel(Parcel in) {
            return new TokenEntity(in);
        }

        @Override
        public TokenEntity[] newArray(int size) {
            return new TokenEntity[size];
        }
    };
    @SerializedName("idError")
    private int mIdError;
    @SerializedName("mensajeError")
    private String mMensajeError;
    @SerializedName("token")
    private String mToken;
    @SerializedName("accountId")
    private String mAccountId;
    @SerializedName("secure")
    private boolean mSecure;

    protected TokenEntity(Parcel in) {
        mIdError = in.readInt();
        mMensajeError = in.readString();
        mToken = in.readString();
        mAccountId = in.readString();
        mSecure = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mIdError);
        dest.writeString(mMensajeError);
        dest.writeString(mToken);
        dest.writeString(mAccountId);
        dest.writeByte((byte) (mSecure ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getIdError() {
        return mIdError;
    }

    public void setIdError(int idError) {
        mIdError = idError;
    }

    public String getMensajeError() {
        return mMensajeError;
    }

    public void setMensajeError(String mensajeError) {
        mMensajeError = mensajeError;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public String getAccountId() {
        return mAccountId;
    }

    public void setAccountId(String accountId) {
        this.mAccountId = accountId;
    }

    public boolean isSecure() {
        return mSecure;
    }

    public void setSecure(boolean secure) {
        this.mSecure = secure;
    }

    @NotNull
    @Override
    public String toString() {
        return mMensajeError;
    }
}
