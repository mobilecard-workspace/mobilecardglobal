package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * ADDCEL on 25/03/17.
 */

public final class SenderModel implements Parcelable {
    public static final Creator<SenderModel> CREATOR = new Creator<SenderModel>() {
        @Override
        public SenderModel createFromParcel(Parcel in) {
            return new SenderModel(in);
        }

        @Override
        public SenderModel[] newArray(int size) {
            return new SenderModel[size];
        }
    };
    //{firstName, lastName, address, phone, idCountry, idState, idCity, zipCode}
    private final long idUsuario;
    private final String firstName;
    private final String lastName;
    private final String address;
    private final String phone;
    private final String idCountry;
    private final String idState;
    private final String idCity;
    private final String zipCode;
    private final String email;
    private final String LName;
    private final String SLName;
    private final String phone1;
    private final String ZIP;
    private final String birthDate;
    private final String address2;
    private final String MName;

    private SenderModel(Builder builder) {
        idUsuario = builder.idUsuario;
        firstName = builder.firstName;
        lastName = builder.lastName;
        address = builder.address;
        phone = builder.phone;
        idCountry = builder.idCountry;
        idState = builder.idState;
        idCity = builder.idCity;
        zipCode = builder.zipCode;
        email = builder.email;
        LName = builder.LName;
        SLName = builder.SLName;
        phone1 = builder.phone1;
        ZIP = builder.ZIP;
        birthDate = builder.birthDate;
        address2 = builder.address2;
        MName = builder.MName;
    }

    private SenderModel(Parcel in) {
        idUsuario = in.readLong();
        firstName = in.readString();
        lastName = in.readString();
        address = in.readString();
        phone = in.readString();
        idCountry = in.readString();
        idState = in.readString();
        idCity = in.readString();
        zipCode = in.readString();
        email = in.readString();
        LName = in.readString();
        SLName = in.readString();
        phone1 = in.readString();
        ZIP = in.readString();
        birthDate = in.readString();
        address2 = in.readString();
        MName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(idUsuario);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(idCountry);
        dest.writeString(idState);
        dest.writeString(idCity);
        dest.writeString(zipCode);
        dest.writeString(email);
        dest.writeString(LName);
        dest.writeString(SLName);
        dest.writeString(phone1);
        dest.writeString(ZIP);
        dest.writeString(birthDate);
        dest.writeString(address2);
        dest.writeString(MName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static class Builder {
        private long idUsuario;
        private String firstName;
        private String lastName;
        private String address;
        private String phone;
        private String idCountry;
        private String idState;
        private String idCity;
        private String zipCode;
        private String email;
        private String LName;
        private String SLName;
        private String phone1;
        private String ZIP;
        private String birthDate;
        private String address2;
        private String MName;

        public Builder setIdUsuario(long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setIdCountry(String idCountry) {
            this.idCountry = idCountry;
            return this;
        }

        public Builder setIdState(String idState) {
            this.idState = idState;
            return this;
        }

        public Builder setIdCity(String idCity) {
            this.idCity = idCity;
            return this;
        }

        public Builder setZipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setLName(String LName) {
            this.LName = LName;
            return this;
        }

        public Builder setSLName(String SLName) {
            this.SLName = SLName;
            return this;
        }

        public Builder setPhone1(String phone1) {
            this.phone1 = phone1;
            return this;
        }

        public Builder setZIP(String ZIP) {
            this.ZIP = ZIP;
            return this;
        }

        public Builder setBirthDate(String birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Builder setAddress2(String address2) {
            this.address2 = address2;
            return this;
        }

        public Builder setMName(String MName) {
            this.MName = MName;
            return this;
        }

        public SenderModel build() {
            return new SenderModel(this);
        }
    }
}
