package addcel.mobilecard.data.net.telepeaje;

import addcel.mobilecard.data.net.telepeaje.model.Tag;
import addcel.mobilecard.data.net.telepeaje.model.TagBalanceResponse;
import addcel.mobilecard.data.net.telepeaje.model.TagsResponse;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * ADDCEL on 16/08/17.
 */
public interface TagsApi {
    String API = "Usuarios/{idApp}/";

    String PAGO = "Peaje/{idApp}/{idioma}/iave/3dsecure/procesaPago";

    @GET(API + "{idioma}/{idUsuario}/tags/")
    Observable<TagsResponse> list(@Path("idApp") int idApp,
                                  @Path("idioma") String idioma, @Path("idUsuario") long idUsuario);

    @GET(API + "{idioma}/{idUsuario}/tags/{provider}")
    Observable<TagsResponse> list(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario,
            @Path("provider") int provider);

    @GET(API + "{idioma}/{idUsuario}/tags/{idTag}/delete")
    Observable<TagsResponse> delete(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario,
            @Path("idTag") String idTag);

    @GET(API + "{idioma}/{idUsuario}/tags/{provider}/{idTag}/delete")
    Observable<TagsResponse> delete(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario,
            @Path("provider") int provider, @Path("idTag") String idTag);

    @POST(API + "{idioma}/{idUsuario}/tags/add")
    Observable<TagsResponse> insert(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario,
            @Body Tag tag);

    @POST(API + "{idioma}/{idUsuario}/tags/{provider}/add")
    Observable<TagsResponse> insert(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario,
            @Path("provider") int provider, @Body Tag tag);

    @POST(API + "{idioma}/{idUsuario}/tags/update")
    Observable<TagsResponse> update(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario,
            @Body Tag tag);

    @POST(API + "{idioma}/{idUsuario}/tags/{provider}/update")
    Observable<TagsResponse> update(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario,
            @Path("provider") int provider, @Body Tag tag);

    @POST(API + "{idioma}/{idUsuario}/tags/checkBalance")
    Observable<TagBalanceResponse> checkBalance(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario,
            @Body Tag tag);
}
