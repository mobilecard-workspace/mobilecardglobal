package addcel.mobilecard.data.net.hth.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.common.base.Strings;

import java.util.Map;

import addcel.mobilecard.data.interfaces.Mappable;

public final class HthPaymentRequest implements Parcelable, Mappable {

    public static final Creator<HthPaymentRequest> CREATOR = new Creator<HthPaymentRequest>() {
        @Override
        public HthPaymentRequest createFromParcel(Parcel in) {
            return new HthPaymentRequest(in);
        }

        @Override
        public HthPaymentRequest[] newArray(int size) {
            return new HthPaymentRequest[size];
        }
    };
    // concept=%s&idUser=%d&idCard=%d&accountId=%d&amount=%f&comision=%f&idioma=%s&lat=%f&lon=%f
    private final String concept;
    private final long idUser;
    private final int idCard;
    private final int accountId;
    private final double amount;
    private final double comision;
    private final String idioma;
    private final double lat;
    private final double lon;

    private HthPaymentRequest(Builder builder) {
        concept = builder.concept;
        idUser = builder.idUser;
        idCard = builder.idCard;
        accountId = builder.accountId;
        amount = builder.amount;
        comision = builder.comision;
        idioma = builder.idioma;
        lat = builder.lat;
        lon = builder.lon;
    }

    private HthPaymentRequest(Parcel in) {
        concept = in.readString();
        idUser = in.readLong();
        idCard = in.readInt();
        accountId = in.readInt();
        amount = in.readDouble();
        comision = in.readDouble();
        idioma = in.readString();
        lat = in.readDouble();
        lon = in.readDouble();
    }

    @Override
    public Map<String, String> asMap(@NonNull Map<String, String> map) {
        map.put("concept", Strings.nullToEmpty(concept));
        map.put("idUser", String.valueOf(idUser));
        map.put("idCard", String.valueOf(idCard));
        map.put("accountId", String.valueOf(accountId));
        map.put("amount", String.valueOf(amount));
        map.put("comision", String.valueOf(comision));
        map.put("idioma", Strings.nullToEmpty(idioma));
        map.put("lat", String.valueOf(lat));
        map.put("lon", String.valueOf(lon));
        return map;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(concept);
        dest.writeLong(idUser);
        dest.writeInt(idCard);
        dest.writeInt(accountId);
        dest.writeDouble(amount);
        dest.writeDouble(comision);
        dest.writeString(idioma);
        dest.writeDouble(lat);
        dest.writeDouble(lon);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final class Builder {
        private String concept;
        private long idUser;
        private int idCard;
        private int accountId;
        private double amount;
        private double comision;
        private String idioma;
        private double lat;
        private double lon;

        public Builder() {
        }

        public Builder setConcept(String val) {
            concept = val;
            return this;
        }

        public Builder setIdUser(long val) {
            idUser = val;
            return this;
        }

        public Builder setIdCard(int val) {
            idCard = val;
            return this;
        }

        public Builder setAccountId(int val) {
            accountId = val;
            return this;
        }

        public Builder setAmount(double val) {
            amount = val;
            return this;
        }

        public Builder setComision(double val) {
            comision = val;
            return this;
        }

        public Builder setIdioma(String val) {
            idioma = val;
            return this;
        }

        public Builder setLat(double val) {
            lat = val;
            return this;
        }

        public Builder setLon(double val) {
            lon = val;
            return this;
        }

        public HthPaymentRequest build() {
            return new HthPaymentRequest(this);
        }
    }
}
