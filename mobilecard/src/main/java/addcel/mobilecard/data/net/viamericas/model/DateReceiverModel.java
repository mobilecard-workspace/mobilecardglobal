package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DateReceiverModel implements Parcelable {

    public static final Creator<DateReceiverModel> CREATOR = new Creator<DateReceiverModel>() {
        @Override
        public DateReceiverModel createFromParcel(Parcel in) {
            return new DateReceiverModel(in);
        }

        @Override
        public DateReceiverModel[] newArray(int size) {
            return new DateReceiverModel[size];
        }
    };
    @SerializedName("dayOfMonth")
    private int mDayOfMonth;
    @SerializedName("hourOfDay")
    private int mHourOfDay;
    @SerializedName("minute")
    private int mMinute;
    @SerializedName("month")
    private int mMonth;
    @SerializedName("second")
    private int mSecond;
    @SerializedName("year")
    private int mYear;

    protected DateReceiverModel(Parcel in) {
        mDayOfMonth = in.readInt();
        mHourOfDay = in.readInt();
        mMinute = in.readInt();
        mMonth = in.readInt();
        mSecond = in.readInt();
        mYear = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mDayOfMonth);
        dest.writeInt(mHourOfDay);
        dest.writeInt(mMinute);
        dest.writeInt(mMonth);
        dest.writeInt(mSecond);
        dest.writeInt(mYear);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getDayOfMonth() {
        return mDayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        mDayOfMonth = dayOfMonth;
    }

    public int getHourOfDay() {
        return mHourOfDay;
    }

    public void setHourOfDay(int hourOfDay) {
        mHourOfDay = hourOfDay;
    }

    public int getMinute() {
        return mMinute;
    }

    public void setMinute(int minute) {
        mMinute = minute;
    }

    public int getMonth() {
        return mMonth;
    }

    public void setMonth(int month) {
        mMonth = month;
    }

    public int getSecond() {
        return mSecond;
    }

    public void setSecond(int second) {
        mSecond = second;
    }

    public int getYear() {
        return mYear;
    }

    public void setYear(int year) {
        mYear = year;
    }
}
