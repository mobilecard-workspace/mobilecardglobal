package addcel.mobilecard.data.net.transacto.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 3/5/19.
 */
public final class PagoBpResponse implements Parcelable {

    public static final Creator<PagoBpResponse> CREATOR = new Creator<PagoBpResponse>() {
        @Override
        public PagoBpResponse createFromParcel(Parcel in) {
            return new PagoBpResponse(in);
        }

        @Override
        public PagoBpResponse[] newArray(int size) {
            return new PagoBpResponse[size];
        }
    };
    /**
     * amount : 0
     * code : 0
     * dateTime : 1551117995220
     * idTransaccion : 1231
     * maskedPAN : 404645******2939
     * message : APROBADA
     * opId : 35441
     * status : 1
     * txnISOCode : 00
     */

    @SerializedName("amount")
    private int amount;
    @SerializedName("code")
    private int code;
    @SerializedName("dateTime")
    private long dateTime;
    @SerializedName("idTransaccion")
    private int idTransaccion;
    @SerializedName("maskedPAN")
    private String maskedPAN;
    @SerializedName("message")
    private String message;
    @SerializedName("opId")
    private int opId;
    @SerializedName("status")
    private int status;
    @SerializedName("txnISOCode")
    private String txnISOCode;
    /**
     * authNumber : BP8519
     * ticketUrl : 310079ed28ae0df2bf9230b464f7f3bc1edada7e
     */

    @SerializedName("authNumber")
    private String authNumber;
    @SerializedName("ticketUrl")
    private String ticketUrl;

    protected PagoBpResponse(Parcel in) {
        amount = in.readInt();
        code = in.readInt();
        dateTime = in.readLong();
        idTransaccion = in.readInt();
        maskedPAN = in.readString();
        message = in.readString();
        opId = in.readInt();
        status = in.readInt();
        txnISOCode = in.readString();
        authNumber = in.readString();
        ticketUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(amount);
        dest.writeInt(code);
        dest.writeLong(dateTime);
        dest.writeInt(idTransaccion);
        dest.writeString(maskedPAN);
        dest.writeString(message);
        dest.writeInt(opId);
        dest.writeInt(status);
        dest.writeString(txnISOCode);
        dest.writeString(authNumber);
        dest.writeString(ticketUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public int getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(int idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getMaskedPAN() {
        return maskedPAN;
    }

    public void setMaskedPAN(String maskedPAN) {
        this.maskedPAN = maskedPAN;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOpId() {
        return opId;
    }

    public void setOpId(int opId) {
        this.opId = opId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTxnISOCode() {
        return txnISOCode;
    }

    public void setTxnISOCode(String txnISOCode) {
        this.txnISOCode = txnISOCode;
    }

    public String getAuthNumber() {
        return authNumber;
    }

    public void setAuthNumber(String authNumber) {
        this.authNumber = authNumber;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("amount", amount)
                .add("code", code)
                .add("dateTime", dateTime)
                .add("idTransaccion", idTransaccion)
                .add("maskedPAN", maskedPAN)
                .add("message", message)
                .add("opId", opId)
                .add("status", status)
                .add("txnISOCode", txnISOCode)
                .toString();
    }
}
