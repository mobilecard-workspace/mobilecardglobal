package addcel.mobilecard.data.net.hth.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.List;

@SuppressWarnings("unused")
public class HthBankResponse {

    @SerializedName("banks")
    private List<Bank> Banks;

    @SerializedName("idError")
    private Long IdError;

    @SerializedName("mensajeError")
    private String MensajeError;

    public List<Bank> getBanks() {
        return Banks;
    }

    public void setBanks(List<Bank> banks) {
        Banks = banks;
    }

    public Long getIdError() {
        return IdError;
    }

    public void setIdError(Long idError) {
        IdError = idError;
    }

    public String getMensajeError() {
        return MensajeError;
    }

    public void setMensajeError(String mensajeError) {
        MensajeError = mensajeError;
    }

    @SuppressWarnings("unused")
    public static class Bank {

        @SerializedName("clave")
        private String Clave;

        @SerializedName("id")
        private Long Id;

        @SerializedName("nombre_corto")
        private String NombreCorto;

        @SerializedName("nombre_razon_social")
        private String NombreRazonSocial;

        @SerializedName("comision_fija")
        private double ComisionFija;

        @SerializedName("comision_porcentaje")
        private double ComisionPorcentaje;

        public String getClave() {
            return Clave;
        }

        public void setClave(String clave) {
            Clave = clave;
        }

        public Long getId() {
            return Id;
        }

        public void setId(Long id) {
            Id = id;
        }

        public String getNombreCorto() {
            return NombreCorto;
        }

        public void setNombreCorto(String nombreCorto) {
            NombreCorto = nombreCorto;
        }

        public String getNombreRazonSocial() {
            return NombreRazonSocial;
        }

        public void setNombreRazonSocial(String nombreRazonSocial) {
            NombreRazonSocial = nombreRazonSocial;
        }

        public double getComisionFija() {
            return ComisionFija;
        }

        public Bank setComisionFija(double comisionFija) {
            ComisionFija = comisionFija;
            return this;
        }

        public double getComisionPorcentaje() {
            return ComisionPorcentaje;
        }

        public Bank setComisionPorcentaje(double comisionPorcentaje) {
            ComisionPorcentaje = comisionPorcentaje;
            return this;
        }

        @NotNull
        @Override
        public String toString() {
            return NombreCorto;
        }
    }
}
