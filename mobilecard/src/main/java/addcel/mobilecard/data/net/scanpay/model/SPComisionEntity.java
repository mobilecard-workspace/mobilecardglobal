package addcel.mobilecard.data.net.scanpay.model;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 10/22/18.
 */
public final class SPComisionEntity {

    /**
     * idError : 500 mensajeError : Error al obtener comision comision_fija : 0 comision_porcentaje :
     * 0
     */
    @SerializedName("idError")
    private int idError;

    @SerializedName("mensajeError")
    private String mensajeError;

    @SerializedName("comision_fija")
    private double comisionFija;

    @SerializedName("comision_porcentaje")
    private double comisionPorcentaje;

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public double getComisionFija() {
        return comisionFija;
    }

    public void setComisionFija(double comisionFija) {
        this.comisionFija = comisionFija;
    }

    public double getComisionPorcentaje() {
        return comisionPorcentaje;
    }

    public void setComisionPorcentaje(double comisionPorcentaje) {
        this.comisionPorcentaje = comisionPorcentaje;
    }
}
