package addcel.mobilecard.data.net.usuarios.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;

import org.jetbrains.annotations.NotNull;

public class McResponse implements Parcelable {

    public static final Creator<McResponse> CREATOR = new Creator<McResponse>() {
        @Override
        public McResponse createFromParcel(Parcel in) {
            return new McResponse(in);
        }

        @Override
        public McResponse[] newArray(int size) {
            return new McResponse[size];
        }
    };
    private int idError;
    private String mensajeError;

    protected McResponse() {
    }

    public McResponse(int idError, String mensajeError) {
        this.idError = idError;
        this.mensajeError = mensajeError;
    }

    protected McResponse(Parcel in) {
        idError = in.readInt();
        mensajeError = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idError);
        dest.writeString(mensajeError);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getIdError() {
        return idError;
    }

    void setIdError(int idError) {
        this.idError = idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    @NotNull
    @Override
    public String toString() {
        return mensajeError;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        McResponse that = (McResponse) o;
        return idError == that.idError && Objects.equal(mensajeError, that.mensajeError);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(idError, mensajeError);
    }
}
