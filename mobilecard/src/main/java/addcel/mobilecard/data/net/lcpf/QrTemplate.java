package addcel.mobilecard.data.net.lcpf;

import addcel.mobilecard.data.net.hth.model.HthPaymentResponse;

/**
 * ADDCEL on 30/08/17.
 */

final class QrTemplate {
    private final HthPaymentResponse response;

    public QrTemplate(HthPaymentResponse response) {
        this.response = response;
    }

    public HthPaymentResponse getResponse() {
        return response;
    }
}
