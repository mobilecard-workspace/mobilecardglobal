package addcel.mobilecard.data.net.lcpf.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LcpfEstablecimiento implements Parcelable {

    public static final Creator<LcpfEstablecimiento> CREATOR = new Creator<LcpfEstablecimiento>() {
        @Override
        public LcpfEstablecimiento createFromParcel(Parcel in) {
            return new LcpfEstablecimiento(in);
        }

        @Override
        public LcpfEstablecimiento[] newArray(int size) {
            return new LcpfEstablecimiento[size];
        }
    };
    @SerializedName("alias")
    private String Alias;
    @SerializedName("comision_fija")
    private double ComisionFija;
    @SerializedName("comision_porcentaje")
    private double ComisionPorcentaje;
    @SerializedName("correo")
    private String Correo;
    @SerializedName("id")
    private int Id;
    @SerializedName("telefono")
    private String Telefono;
    @SerializedName("urlLogo")
    private String UrlLogo;

    public LcpfEstablecimiento() {
    }

    protected LcpfEstablecimiento(Parcel in) {
        Alias = in.readString();
        ComisionFija = in.readDouble();
        ComisionPorcentaje = in.readDouble();
        Correo = in.readString();
        Id = in.readInt();
        Telefono = in.readString();
        UrlLogo = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Alias);
        dest.writeDouble(ComisionFija);
        dest.writeDouble(ComisionPorcentaje);
        dest.writeString(Correo);
        dest.writeInt(Id);
        dest.writeString(Telefono);
        dest.writeString(UrlLogo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAlias() {
        return Alias;
    }

    public double getComisionFija() {
        return ComisionFija;
    }

    public double getComisionPorcentaje() {
        return ComisionPorcentaje;
    }

    public String getCorreo() {
        return Correo;
    }

    public int getId() {
        return Id;
    }

    public String getTelefono() {
        return Telefono;
    }

    public String getUrlLogo() {
        return UrlLogo;
    }

    public static class Builder {

        private String Alias;
        private double ComisionFija;
        private double ComisionPorcentaje;
        private String Correo;
        private int Id;
        private String Telefono;
        private String Logo;

        public LcpfEstablecimiento.Builder withAlias(String alias) {
            Alias = alias;
            return this;
        }

        public LcpfEstablecimiento.Builder withComisionFija(double comisionFija) {
            ComisionFija = comisionFija;
            return this;
        }

        public LcpfEstablecimiento.Builder withComisionPorcentaje(double comisionPorcentaje) {
            ComisionPorcentaje = comisionPorcentaje;
            return this;
        }

        public LcpfEstablecimiento.Builder withCorreo(String correo) {
            Correo = correo;
            return this;
        }

        public LcpfEstablecimiento.Builder withId(int id) {
            Id = id;
            return this;
        }

        public LcpfEstablecimiento.Builder withTelefono(String telefono) {
            Telefono = telefono;
            return this;
        }

        public LcpfEstablecimiento.Builder withLogo(String logo) {
            Logo = logo;
            return this;
        }

        public LcpfEstablecimiento build() {
            LcpfEstablecimiento Account = new LcpfEstablecimiento();
            Account.Alias = Alias;
            Account.ComisionFija = ComisionFija;
            Account.ComisionPorcentaje = ComisionPorcentaje;
            Account.Correo = Correo;
            Account.Id = Id;
            Account.Telefono = Telefono;
            Account.UrlLogo = Logo;
            return Account;
        }
    }
}
