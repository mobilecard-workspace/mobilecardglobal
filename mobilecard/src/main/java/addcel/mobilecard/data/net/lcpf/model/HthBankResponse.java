package addcel.mobilecard.data.net.lcpf.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class HthBankResponse {

    @SerializedName("banks")
    private List<Bank> Banks;
    @SerializedName("idError")
    private Long IdError;
    @SerializedName("mensajeError")
    private String MensajeError;

    public List<Bank> getBanks() {
        return Banks;
    }

    public void setBanks(List<Bank> banks) {
        Banks = banks;
    }

    public Long getIdError() {
        return IdError;
    }

    public void setIdError(Long idError) {
        IdError = idError;
    }

    public String getMensajeError() {
        return MensajeError;
    }

    public void setMensajeError(String mensajeError) {
        MensajeError = mensajeError;
    }
}
