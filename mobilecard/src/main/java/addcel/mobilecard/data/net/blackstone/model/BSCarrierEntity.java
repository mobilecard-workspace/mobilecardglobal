package addcel.mobilecard.data.net.blackstone.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class BSCarrierEntity implements Parcelable {

    public static final Creator<BSCarrierEntity> CREATOR = new Creator<BSCarrierEntity>() {
        @Override
        public BSCarrierEntity createFromParcel(Parcel in) {
            return new BSCarrierEntity(in);
        }

        @Override
        public BSCarrierEntity[] newArray(int size) {
            return new BSCarrierEntity[size];
        }
    };
    @Expose
    private String carrierName;

    BSCarrierEntity() {
    }

    private BSCarrierEntity(Parcel in) {
        carrierName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(carrierName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public static class Builder {

        private String carrierName;

        public BSCarrierEntity.Builder withCarrierName(String carrierName) {
            this.carrierName = carrierName;
            return this;
        }

        public BSCarrierEntity build() {
            BSCarrierEntity bSCarrierEntity = new BSCarrierEntity();
            bSCarrierEntity.carrierName = carrierName;
            return bSCarrierEntity;
        }
    }
}
