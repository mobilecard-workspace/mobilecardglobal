package addcel.mobilecard.data.net.apto.accounts

import com.google.gson.annotations.SerializedName


/**
 * ADDCEL on 2019-10-24.
 */
data class AptoCardBalanceEntity(
        @SerializedName("amount_held")
        val amountHeld: AmountHeld,
        @SerializedName("amount_spendable")
        val amountSpendable: AmountSpendable,
        @SerializedName("balance")
        val balance: Balance,
        @SerializedName("details")
        val details: Details,
        @SerializedName("funding_source_type")
        val fundingSourceType: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("state")
        val state: String,
        @SerializedName("type")
        val type: String
)

data class AmountHeld(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
)

data class AmountSpendable(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
)

data class Balance(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
)

data class Details(
        @SerializedName("balance")
        val balance: BalanceX,
        @SerializedName("custodian")
        val custodian: Custodian,
        @SerializedName("external_id")
        val externalId: String,
        @SerializedName("type")
        val type: String
)

data class BalanceX(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("currency")
        val currency: String,
        @SerializedName("type")
        val type: String
)

data class Custodian(
        @SerializedName("custodian_type")
        val custodianType: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("type")
        val type: String
)