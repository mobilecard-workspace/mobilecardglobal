package addcel.mobilecard.data.net.aci.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public class _class implements Parcelable {

    public static final Creator<_class> CREATOR = new Creator<_class>() {
        @Override
        public _class createFromParcel(Parcel in) {
            return new _class(in);
        }

        @Override
        public _class[] newArray(int size) {
            return new _class[size];
        }
    };

    @SerializedName("clase_name")
    private String mClaseName;

    @SerializedName("class_type")
    private String mClassType;

    protected _class(Parcel in) {
        mClaseName = in.readString();
        mClassType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mClaseName);
        dest.writeString(mClassType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getClaseName() {
        return mClaseName;
    }

    public void setClaseName(String claseName) {
        mClaseName = claseName;
    }

    public String getClassType() {
        return mClassType;
    }

    public void setClassType(String classType) {
        mClassType = classType;
    }

    @NotNull
    @Override
    public String toString() {
        return mClaseName + "-" + mClassType;
    }
}
