package addcel.mobilecard.data.net.colombia.multimarket

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


/**
 * ADDCEL on 2019-12-18.
 */
data class MmCategoriasResponse(
        @SerializedName("categorias")
        val categorias: List<Categoria>,
        @SerializedName("codigo")
        val codigo: Int,
        @SerializedName("mensaje")
        val mensaje: String
)

@Parcelize
data class Categoria(
        @SerializedName("nombre")
        val nombre: String
) : Parcelable

data class MmServiciosResponse(
        @SerializedName("codigo")
        val codigo: Int,
        @SerializedName("mensaje")
        val mensaje: String,
        @SerializedName("searchable")
        val searchable: Boolean,
        @SerializedName("servicios")
        val servicios: List<Servicio>
)

@Parcelize
data class Servicio(
        @SerializedName("name")
        val name: String,
        @SerializedName("productId")
        val productId: Int
) : Parcelable

data class MmProductosResponse(
        @SerializedName("codigo")
        val codigo: Int,
        @SerializedName("mensaje")
        val mensaje: String,
        @SerializedName("productos")
        val productos: List<Producto>
)

@Parcelize
data class Producto(
        @SerializedName("nombre")
        val nombre: String,
        @SerializedName("valor")
        val valor: List<Valor>
) : Parcelable

@Parcelize
data class Valor(
        @SerializedName("productId")
        val productId: Int,
        @SerializedName("valor")
        val valor: String
) : Parcelable


data class MmConsultaEntity(
        @SerializedName("barcode")
        val barcode: Boolean,
        @SerializedName("idConvenio")
        val idConvenio: Int,
        @SerializedName("idUsuario")
        val idUsuario: Long,
        @SerializedName("reference")
        val reference: String
)

@Parcelize
data class MmConsultaResponse(
        @SerializedName("codigo")
        val codigo: Int = -1,
        @SerializedName("comision")
        val comision: Double = 0.0,
        @SerializedName("idReferencia")
        val idReferencia: String = "",
        @SerializedName("mensaje")
        val mensaje: String = "Error",
        @SerializedName("monto")
        val monto: Double = 0.0,
        @SerializedName("barcode")
        val barcode: Boolean = false,
        @SerializedName("reference")
        val reference: String = "",
        @SerializedName("amountEditable")
        val amountEditable: Boolean = false
) : Parcelable

data class MmCompraProductoEntity(
        @SerializedName("amount")
        val amount: Double,
        @SerializedName("idApp")
        val idApp: Int,
        @SerializedName("idPais")
        val idPais: Int,
        @SerializedName("idProducto")
        val idProducto: Int,
        @SerializedName("idTarjeta")
        val idTarjeta: Int,
        @SerializedName("idUsuario")
        val idUsuario: Long,
        @SerializedName("idioma")
        val idioma: String,
        @SerializedName("comision")
        val comision: Double
)

data class MmPagoServicioEntity(
        @SerializedName("barcode")
        val barcode: Boolean = false,
        @SerializedName("idApp")
        val idApp: Int = 1,
        @SerializedName("idConvenio")
        val idConvenio: Int = 0,
        @SerializedName("idPais")
        val idPais: Int = 2,
        @SerializedName("idTarjeta")
        val idTarjeta: Int = 0,
        @SerializedName("idUsuario")
        val idUsuario: Long = 0L,
        @SerializedName("idioma")
        val idioma: String = "es",
        @SerializedName("reference")
        val reference: String = "",
        @SerializedName("comision")
        val comision: Double = 0.0,
        @SerializedName("idReferencia")
        val idReferencia: String = "",
        @SerializedName("idReferencia")
        val monto: Double = 0.0
)

@Parcelize
data class MmPagoResponse(
        @SerializedName("codigo")
        val codigo: Int,
        @SerializedName("idTransaccion")
        val idTransaccion: String?,
        @SerializedName("mensaje")
        val mensaje: String?
) : Parcelable

