package addcel.mobilecard.data.net.catalogo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * ADDCEL on 2/1/19.
 */
public final class EstadoResponse {

    /**
     * estados :
     * [{"abreviatura":"","id":1,"nombre":"Aguascalientes","pais":1},{"abreviatura":"","id":2,"nombre":"Baja
     * California","pais":1},{"abreviatura":"","id":3,"nombre":"Baja California
     * Sur","pais":1},{"abreviatura":"","id":4,"nombre":"Campeche","pais":1},{"abreviatura":"","id":5,"nombre":"Coahuila","pais":1},{"abreviatura":"","id":6,"nombre":"Colima","pais":1},{"abreviatura":"","id":7,"nombre":"Chiapas","pais":1},{"abreviatura":"","id":8,"nombre":"Chihuahua","pais":1},{"abreviatura":"","id":9,"nombre":"Distrito
     * Federal","pais":1},{"abreviatura":"","id":10,"nombre":"Durango","pais":1},{"abreviatura":"","id":11,"nombre":"Guanajuato","pais":1},{"abreviatura":"","id":12,"nombre":"Guerrero","pais":1},{"abreviatura":"","id":13,"nombre":"Hidalgo","pais":1},{"abreviatura":"","id":14,"nombre":"Jalisco","pais":1},{"abreviatura":"","id":15,"nombre":"Estado
     * de
     * Mexico","pais":1},{"abreviatura":"","id":16,"nombre":"Michoacan","pais":1},{"abreviatura":"","id":17,"nombre":"Morelos","pais":1},{"abreviatura":"","id":18,"nombre":"Nayarit","pais":1},{"abreviatura":"","id":19,"nombre":"Nuevo
     * Leon","pais":1},{"abreviatura":"","id":20,"nombre":"Oaxaca","pais":1},{"abreviatura":"","id":21,"nombre":"Puebla","pais":1},{"abreviatura":"","id":22,"nombre":"Queretaro","pais":1},{"abreviatura":"","id":23,"nombre":"Quintana
     * Roo","pais":1},{"abreviatura":"","id":24,"nombre":"San Luis
     * Potosi","pais":1},{"abreviatura":"","id":25,"nombre":"Sinaloa","pais":1},{"abreviatura":"","id":26,"nombre":"Sonora","pais":1},{"abreviatura":"","id":27,"nombre":"Tabasco","pais":1},{"abreviatura":"","id":28,"nombre":"Tamaulipas","pais":1},{"abreviatura":"","id":29,"nombre":"Tlaxcala","pais":1},{"abreviatura":"","id":30,"nombre":"Veracruz","pais":1},{"abreviatura":"","id":31,"nombre":"Yucatan","pais":1},{"abreviatura":"","id":32,"nombre":"Zacatecas","pais":1}]
     * idError : 0 mensajeError :
     */
    @SerializedName("idError")
    private int idError;

    @SerializedName("mensajeError")
    private String mensajeError;

    @SerializedName("estados")
    private List<EstadoEntity> estados;

    public EstadoResponse(int idError, String mensajeError) {
        this.idError = idError;
        this.mensajeError = mensajeError;
    }

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public List<EstadoEntity> getEstados() {
        return estados;
    }

    public void setEstados(List<EstadoEntity> estados) {
        this.estados = estados;
    }

    public static class EstadoEntity implements Parcelable {
        public static final Creator<EstadoEntity> CREATOR = new Creator<EstadoEntity>() {
            @Override
            public EstadoEntity createFromParcel(Parcel in) {
                return new EstadoEntity(in);
            }

            @Override
            public EstadoEntity[] newArray(int size) {
                return new EstadoEntity[size];
            }
        };
        /**
         * abreviatura : id : 1 nombre : Aguascalientes pais : 1
         */
        @SerializedName("abreviatura")
        private String abreviatura;

        @SerializedName("id")
        private int id;

        @SerializedName("nombre")
        private String nombre;

        @SerializedName("pais")
        private int pais;

        protected EstadoEntity(Parcel in) {
            abreviatura = in.readString();
            id = in.readInt();
            nombre = in.readString();
            pais = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(abreviatura);
            dest.writeInt(id);
            dest.writeString(nombre);
            dest.writeInt(pais);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getAbreviatura() {
            return abreviatura;
        }

        public void setAbreviatura(String abreviatura) {
            this.abreviatura = abreviatura;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public int getPais() {
            return pais;
        }

        public void setPais(int pais) {
            this.pais = pais;
        }

        @NotNull
        @Override
        public String toString() {
            return nombre;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            EstadoEntity that = (EstadoEntity) o;
            return id == that.id
                    && pais == that.pais
                    && Objects.equal(abreviatura, that.abreviatura)
                    && Objects.equal(nombre, that.nombre);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(abreviatura, id, nombre, pais);
        }
    }
}
