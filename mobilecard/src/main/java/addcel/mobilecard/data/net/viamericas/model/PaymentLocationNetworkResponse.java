package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

@SuppressWarnings("unused")
public final class PaymentLocationNetworkResponse extends McResponse
        implements Parcelable {

    public static final Creator<PaymentLocationNetworkResponse> CREATOR =
            new Creator<PaymentLocationNetworkResponse>() {
                @Override
                public PaymentLocationNetworkResponse createFromParcel(Parcel in) {
                    return new PaymentLocationNetworkResponse(in);
                }

                @Override
                public PaymentLocationNetworkResponse[] newArray(int size) {
                    return new PaymentLocationNetworkResponse[size];
                }
            };
    @SerializedName("locations")
    private List<LocationModel> mLocations;

    public PaymentLocationNetworkResponse() {
    }

    public PaymentLocationNetworkResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    protected PaymentLocationNetworkResponse(Parcel in) {
        super(in);
        mLocations = in.createTypedArrayList(LocationModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(mLocations);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<LocationModel> getLocations() {
        return mLocations;
    }

    public void setLocations(List<LocationModel> locations) {
        mLocations = locations;
    }
}
