package addcel.mobilecard.data.net.ingo.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class IngoCardRequest {

    @Expose
    private String addressLine1;
    @Expose
    private String addressLine2;
    @Expose
    private String cardNickname;
    @Expose
    private String cardNumber;
    @Expose
    private String city;
    @Expose
    private String customerId;
    @Expose
    private String diviceId;
    @Expose
    private String expirationMonthYear;
    @Expose
    private String nameOnCard;
    @Expose
    private String state;
    @Expose
    private String zip;

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getCardNickname() {
        return cardNickname;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCity() {
        return city;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getDiviceId() {
        return diviceId;
    }

    public String getExpirationMonthYear() {
        return expirationMonthYear;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public static class Builder {

        private String addressLine1;
        private String addressLine2;
        private String cardNickname;
        private String cardNumber;
        private String city;
        private String customerId;
        private String diviceId;
        private String expirationMonthYear;
        private String nameOnCard;
        private String state;
        private String zip;

        public IngoCardRequest.Builder withAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
            return this;
        }

        public IngoCardRequest.Builder withAddressLine2(String addressLine2) {
            this.addressLine2 = addressLine2;
            return this;
        }

        public IngoCardRequest.Builder withCardNickname(String cardNickname) {
            this.cardNickname = cardNickname;
            return this;
        }

        public IngoCardRequest.Builder withCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public IngoCardRequest.Builder withCity(String city) {
            this.city = city;
            return this;
        }

        public IngoCardRequest.Builder withCustomerId(String customerId) {
            this.customerId = customerId;
            return this;
        }

        public IngoCardRequest.Builder withDiviceId(String diviceId) {
            this.diviceId = diviceId;
            return this;
        }

        public IngoCardRequest.Builder withExpirationMonthYear(String expirationMonthYear) {
            this.expirationMonthYear = expirationMonthYear;
            return this;
        }

        public IngoCardRequest.Builder withNameOnCard(String nameOnCard) {
            this.nameOnCard = nameOnCard;
            return this;
        }

        public IngoCardRequest.Builder withState(String state) {
            this.state = state;
            return this;
        }

        public IngoCardRequest.Builder withZip(String zip) {
            this.zip = zip;
            return this;
        }

        public IngoCardRequest build() {
            IngoCardRequest ingoCardRequest = new IngoCardRequest();
            ingoCardRequest.addressLine1 = addressLine1;
            ingoCardRequest.addressLine2 = addressLine2;
            ingoCardRequest.cardNickname = cardNickname;
            ingoCardRequest.cardNumber = cardNumber;
            ingoCardRequest.city = city;
            ingoCardRequest.customerId = customerId;
            ingoCardRequest.diviceId = diviceId;
            ingoCardRequest.expirationMonthYear = expirationMonthYear;
            ingoCardRequest.nameOnCard = nameOnCard;
            ingoCardRequest.state = state;
            ingoCardRequest.zip = zip;
            return ingoCardRequest;
        }
    }
}
