package addcel.mobilecard.data.net.viamericas.model;

import org.jetbrains.annotations.NotNull;

/**
 * ADDCEL on 29/03/17.
 */

public final class VmStateModel {
    //{"idState":"MEX1","nameState":"AGUASCALIENTES","__hashCodeCalc":false}
    private String idState;
    private String nameState;

    public VmStateModel() {
    }

    public VmStateModel(String idState, String nameState) {
        this.idState = idState;
        this.nameState = nameState;
    }

    public String getIdState() {
        return idState;
    }

    public String getNameState() {
        return nameState;
    }

    @NotNull
    @Override
    public String toString() {
        return nameState;
    }
}
