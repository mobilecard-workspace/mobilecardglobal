package addcel.mobilecard.data.net.previvale.historial

import com.google.gson.annotations.SerializedName


/**
 * ADDCEL on 2020-01-27.
 */


data class PrevivaleHistorialRequest(
        @SerializedName("fechaFin")
        val fechaFin: String,
        @SerializedName("fechaIni")
        val fechaIni: String,
        @SerializedName("idUsuario")
        val idUsuario: Long,
        @SerializedName("tarjeta")
        val tarjeta: String
)

data class PrevivaleHistorialResponse(
        @SerializedName("code")
        val code: Int,
        @SerializedName("data")
        val `data`: List<PrevivaleTransaction>,
        @SerializedName("message")
        val message: String
)

data class PrevivaleTransaction(
        @SerializedName("cantidad")
        val cantidad: String,
        @SerializedName("establecimiento")
        val establecimiento: String,
        @SerializedName("fecha")
        val fecha: String,
        @SerializedName("status")
        val status: String
)