package addcel.mobilecard.data.net.telepeaje.model;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * ADDCEL on 16/08/17.
 */
public class TagsResponse {
    private int idError;
    private String mensajeError;
    private List<Tag> tags;

    public TagsResponse() {
    }

    public TagsResponse(int idError, String mensajeError) {
        this.idError = idError;
        this.mensajeError = mensajeError;
    }

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public List<Tag> getTags() {
        return tags;
    }

    @NotNull
    @Override
    public String toString() {
        return mensajeError;
    }
}
