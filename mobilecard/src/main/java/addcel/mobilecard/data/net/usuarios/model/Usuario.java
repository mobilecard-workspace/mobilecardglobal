package addcel.mobilecard.data.net.usuarios.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.NonNull;

public final class Usuario implements Parcelable {

    public static final Creator<Usuario> CREATOR = new Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };
    /**
     * idError : 0 mensajeError : login exitoso ideUsuario : 8923233104032 usrLogin :
     * elmans8+biz@gmail.com usrPwd : usrFechaNac : 2018-11-29 usrTelefono : 5518307722 operador : 0
     * usrFechaRegistro : 2018-11-29 12:38:16 usrNombre : Carlos usrApellido : Garcia usrDireccion :
     * Calle Falsa 123 usrTdcNumero : usrTdcCodigo : usrTdcVigencia : idBanco : 0 idTipoTarjeta : 0
     * idProveedor : 0 idUsrStatus : 1 cedula : tipoCedula : 1 recibirSMS : 0 idPais : 3 gemalto : 0
     * eMail : elmans8+biz@gmail.com imei : 352689081588323 tipo : Google software : 9 modelo :
     * ANDROID wkey : telefonoOriginal : usrMaterno : Sanoja usrSexo : usrTelCasa : usrTelOficina :
     * usrIdEstado : 15 usrCiudad : Naucalpan usrCalle : usrNumExt : 0 usrNumInterior : usrColonia :
     * Ciudad Satelite usrCp : 53100 usrDomAmex : usrTerminos : numExtStr : idIngo :
     * 768bd236-e8c2-41f7-b454-c37fa3735bf5 usrNss : 555448888 newPassword : idioma : debug : false
     * proveedor : idApp : tipoUsuario : id : 0 negocio : false smsCode : null
     */
    @SerializedName("idError")
    private int idError;
    @SerializedName("mensajeError")
    private String mensajeError;
    @SerializedName("ideUsuario")
    private long ideUsuario;
    @SerializedName("usrLogin")
    private String usrLogin;
    @SerializedName("usrPwd")
    private String usrPwd;
    @SerializedName("usrFechaNac")
    private String usrFechaNac;
    @SerializedName("usrTelefono")
    private String usrTelefono;
    @SerializedName("operador")
    private int operador;
    @SerializedName("usrFechaRegistro")
    private String usrFechaRegistro;
    @SerializedName("usrNombre")
    private String usrNombre;
    @SerializedName("usrApellido")
    private String usrApellido;
    @SerializedName("usrDireccion")
    private String usrDireccion;
    @SerializedName("usrTdcNumero")
    private String usrTdcNumero;
    @SerializedName("usrTdcCodigo")
    private String usrTdcCodigo;
    @SerializedName("usrTdcVigencia")
    private String usrTdcVigencia;
    @SerializedName("idBanco")
    private int idBanco;
    @SerializedName("idTipoTarjeta")
    private int idTipoTarjeta;
    @SerializedName("idProveedor")
    private int idProveedor;
    @SerializedName("idUsrStatus")
    private int idUsrStatus;
    @SerializedName("cedula")
    private String cedula;
    @SerializedName("tipoCedula")
    private int tipoCedula;
    @SerializedName("recibirSMS")
    private int recibirSMS;
    @SerializedName("idPais")
    private int idPais;
    @SerializedName("gemalto")
    private int gemalto;
    @SerializedName("eMail")
    private String eMail;
    @SerializedName("imei")
    private String imei;
    @SerializedName("tipo")
    private String tipo;
    @SerializedName("software")
    private String software;
    @SerializedName("modelo")
    private String modelo;
    @SerializedName("wkey")
    private String wkey;
    @SerializedName("telefonoOriginal")
    private String telefonoOriginal;
    @SerializedName("usrMaterno")
    private String usrMaterno;
    @SerializedName("usrSexo")
    private String usrSexo;
    @SerializedName("usrTelCasa")
    private String usrTelCasa;
    @SerializedName("usrTelOficina")
    private String usrTelOficina;
    @SerializedName("usrIdEstado")
    private int usrIdEstado;
    @SerializedName("usrCiudad")
    private String usrCiudad;
    @SerializedName("usrCalle")
    private String usrCalle;
    @SerializedName("usrNumExt")
    private int usrNumExt;
    @SerializedName("usrNumInterior")
    private String usrNumInterior;
    @SerializedName("usrColonia")
    private String usrColonia;
    @SerializedName("usrCp")
    private String usrCp;
    @SerializedName("usrDomAmex")
    private String usrDomAmex;
    @SerializedName("usrTerminos")
    private String usrTerminos;
    @SerializedName("numExtStr")
    private String numExtStr;
    @SerializedName("idIngo")
    private String idIngo;
    @SerializedName("usrNss")
    private String usrNss;
    @SerializedName("newPassword")
    private String newPassword;
    @SerializedName("idioma")
    private String idioma;
    @SerializedName("debug")
    private boolean debug;
    @SerializedName("proveedor")
    private String proveedor;
    @SerializedName("idApp")
    private String idApp;
    @SerializedName("tipoUsuario")
    private String tipoUsuario;
    @SerializedName("id")
    private int id;
    @SerializedName("negocio")
    private boolean negocio;
    @SerializedName("smsCode")
    private Object smsCode;
    @SerializedName("usr_email")
    private String usrEmail;
    @SerializedName("usr_sms")
    private String usrSms;
    @SerializedName("usr_jumio")
    private String usrJumio;
    @SerializedName("scanReference")
    private String scanReference;
    @SerializedName("img")
    private String img;

    public Usuario() {
    }

    protected Usuario(Parcel in) {
        idError = in.readInt();
        mensajeError = in.readString();
        ideUsuario = in.readLong();
        usrLogin = in.readString();
        usrPwd = in.readString();
        usrFechaNac = in.readString();
        usrTelefono = in.readString();
        operador = in.readInt();
        usrFechaRegistro = in.readString();
        usrNombre = in.readString();
        usrApellido = in.readString();
        usrDireccion = in.readString();
        usrTdcNumero = in.readString();
        usrTdcCodigo = in.readString();
        usrTdcVigencia = in.readString();
        idBanco = in.readInt();
        idTipoTarjeta = in.readInt();
        idProveedor = in.readInt();
        idUsrStatus = in.readInt();
        cedula = in.readString();
        tipoCedula = in.readInt();
        recibirSMS = in.readInt();
        idPais = in.readInt();
        gemalto = in.readInt();
        eMail = in.readString();
        imei = in.readString();
        tipo = in.readString();
        software = in.readString();
        modelo = in.readString();
        wkey = in.readString();
        telefonoOriginal = in.readString();
        usrMaterno = in.readString();
        usrSexo = in.readString();
        usrTelCasa = in.readString();
        usrTelOficina = in.readString();
        usrIdEstado = in.readInt();
        usrCiudad = in.readString();
        usrCalle = in.readString();
        usrNumExt = in.readInt();
        usrNumInterior = in.readString();
        usrColonia = in.readString();
        usrCp = in.readString();
        usrDomAmex = in.readString();
        usrTerminos = in.readString();
        numExtStr = in.readString();
        idIngo = in.readString();
        usrNss = in.readString();
        newPassword = in.readString();
        idioma = in.readString();
        debug = in.readByte() != 0;
        proveedor = in.readString();
        idApp = in.readString();
        tipoUsuario = in.readString();
        id = in.readInt();
        negocio = in.readByte() != 0;
        usrEmail = in.readString();
        usrSms = in.readString();
        usrJumio = in.readString();
        scanReference = in.readString();
        img = in.readString();
    }


    public static Usuario sanitize(@NonNull Usuario usuario) {
        usuario.mensajeError = Strings.nullToEmpty(usuario.mensajeError);
        usuario.usrLogin = Strings.nullToEmpty(usuario.usrLogin);
        usuario.usrPwd = Strings.nullToEmpty(usuario.usrPwd);
        usuario.usrFechaNac = Strings.nullToEmpty(usuario.usrFechaNac);
        usuario.usrTelefono = Strings.nullToEmpty(usuario.usrTelefono);
        usuario.usrFechaRegistro = Strings.nullToEmpty(usuario.usrFechaRegistro);
        usuario.usrNombre = Strings.nullToEmpty(usuario.usrNombre);
        usuario.usrApellido = Strings.nullToEmpty(usuario.usrApellido);
        usuario.usrDireccion = Strings.nullToEmpty(usuario.usrDireccion);
        usuario.usrTdcNumero = Strings.nullToEmpty(usuario.usrTdcNumero);
        usuario.usrTdcVigencia = Strings.nullToEmpty(usuario.usrTdcVigencia);
        usuario.cedula = Strings.nullToEmpty(usuario.cedula);
        usuario.eMail = Strings.nullToEmpty(usuario.eMail);
        usuario.imei = Strings.nullToEmpty(usuario.imei);
        usuario.tipo = Strings.nullToEmpty(usuario.tipo);
        usuario.software = Strings.nullToEmpty(usuario.software);
        usuario.modelo = Strings.nullToEmpty(usuario.modelo);
        usuario.wkey = Strings.nullToEmpty(usuario.wkey);
        usuario.telefonoOriginal = Strings.nullToEmpty(usuario.telefonoOriginal);
        usuario.usrMaterno = Strings.nullToEmpty(usuario.usrMaterno);
        usuario.usrSexo = Strings.nullToEmpty(usuario.usrSexo);
        usuario.usrTelCasa = Strings.nullToEmpty(usuario.usrTelCasa);
        usuario.usrTelOficina = Strings.nullToEmpty(usuario.usrTelOficina);
        usuario.usrCiudad = Strings.nullToEmpty(usuario.usrCiudad);
        usuario.usrCalle = Strings.nullToEmpty(usuario.usrCalle);
        usuario.usrNumInterior = Strings.nullToEmpty(usuario.usrNumInterior);
        usuario.usrColonia = Strings.nullToEmpty(usuario.usrColonia);
        usuario.usrCp = Strings.nullToEmpty(usuario.usrCp);
        usuario.usrDomAmex = Strings.nullToEmpty(usuario.usrDomAmex);
        usuario.usrTerminos = Strings.nullToEmpty(usuario.usrTerminos);
        usuario.numExtStr = Strings.nullToEmpty(usuario.numExtStr);
        usuario.idIngo = Strings.nullToEmpty(usuario.idIngo);
        usuario.usrNss = Strings.nullToEmpty(usuario.usrNss);
        usuario.newPassword = Strings.nullToEmpty(usuario.newPassword);
        usuario.idioma = Strings.nullToEmpty(usuario.idioma);
        usuario.proveedor = Strings.nullToEmpty(usuario.proveedor);
        usuario.idApp = Strings.nullToEmpty(usuario.idApp);
        usuario.tipoUsuario = Strings.nullToEmpty(usuario.tipoUsuario);
        usuario.usrEmail = Strings.nullToEmpty(usuario.usrEmail);
        usuario.usrSms = Strings.nullToEmpty(usuario.usrSms);
        usuario.usrJumio = Strings.nullToEmpty(usuario.usrJumio);
        usuario.scanReference = Strings.nullToEmpty(usuario.scanReference);
        usuario.img = Strings.nullToEmpty(usuario.img);
        return usuario;
    }

    public static Usuario fromRegistroResponse(RegistroResponse registroResponse) {
        Usuario user = new Usuario();
        user.setIdeUsuario(registroResponse.getIdUsuario());
        user.setIdPais(registroResponse.getIdPais());
        return user;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idError);
        dest.writeString(mensajeError);
        dest.writeLong(ideUsuario);
        dest.writeString(usrLogin);
        dest.writeString(usrPwd);
        dest.writeString(usrFechaNac);
        dest.writeString(usrTelefono);
        dest.writeInt(operador);
        dest.writeString(usrFechaRegistro);
        dest.writeString(usrNombre);
        dest.writeString(usrApellido);
        dest.writeString(usrDireccion);
        dest.writeString(usrTdcNumero);
        dest.writeString(usrTdcCodigo);
        dest.writeString(usrTdcVigencia);
        dest.writeInt(idBanco);
        dest.writeInt(idTipoTarjeta);
        dest.writeInt(idProveedor);
        dest.writeInt(idUsrStatus);
        dest.writeString(cedula);
        dest.writeInt(tipoCedula);
        dest.writeInt(recibirSMS);
        dest.writeInt(idPais);
        dest.writeInt(gemalto);
        dest.writeString(eMail);
        dest.writeString(imei);
        dest.writeString(tipo);
        dest.writeString(software);
        dest.writeString(modelo);
        dest.writeString(wkey);
        dest.writeString(telefonoOriginal);
        dest.writeString(usrMaterno);
        dest.writeString(usrSexo);
        dest.writeString(usrTelCasa);
        dest.writeString(usrTelOficina);
        dest.writeInt(usrIdEstado);
        dest.writeString(usrCiudad);
        dest.writeString(usrCalle);
        dest.writeInt(usrNumExt);
        dest.writeString(usrNumInterior);
        dest.writeString(usrColonia);
        dest.writeString(usrCp);
        dest.writeString(usrDomAmex);
        dest.writeString(usrTerminos);
        dest.writeString(numExtStr);
        dest.writeString(idIngo);
        dest.writeString(usrNss);
        dest.writeString(newPassword);
        dest.writeString(idioma);
        dest.writeByte((byte) (debug ? 1 : 0));
        dest.writeString(proveedor);
        dest.writeString(idApp);
        dest.writeString(tipoUsuario);
        dest.writeInt(id);
        dest.writeByte((byte) (negocio ? 1 : 0));
        dest.writeString(usrEmail);
        dest.writeString(usrSms);
        dest.writeString(usrJumio);
        dest.writeString(scanReference);
        dest.writeString(img);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getIdError() {
        return idError;
    }

    public void setIdError(int idError) {
        this.idError = idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public long getIdeUsuario() {
        return ideUsuario;
    }

    public void setIdeUsuario(long ideUsuario) {
        this.ideUsuario = ideUsuario;
    }

    public String getUsrLogin() {
        return usrLogin;
    }

    public void setUsrLogin(String usrLogin) {
        this.usrLogin = usrLogin;
    }

    public String getUsrPwd() {
        return usrPwd;
    }

    public void setUsrPwd(String usrPwd) {
        this.usrPwd = usrPwd;
    }

    public String getUsrFechaNac() {
        return usrFechaNac;
    }

    public void setUsrFechaNac(String usrFechaNac) {
        this.usrFechaNac = usrFechaNac;
    }

    public String getUsrTelefono() {
        return usrTelefono;
    }

    public void setUsrTelefono(String usrTelefono) {
        this.usrTelefono = usrTelefono;
    }

    public int getOperador() {
        return operador;
    }

    public void setOperador(int operador) {
        this.operador = operador;
    }

    public String getUsrFechaRegistro() {
        return usrFechaRegistro;
    }

    public void setUsrFechaRegistro(String usrFechaRegistro) {
        this.usrFechaRegistro = usrFechaRegistro;
    }

    public String getUsrNombre() {
        return usrNombre;
    }

    public void setUsrNombre(String usrNombre) {
        this.usrNombre = usrNombre;
    }

    public String getUsrApellido() {
        return usrApellido;
    }

    public void setUsrApellido(String usrApellido) {
        this.usrApellido = usrApellido;
    }

    public String getUsrDireccion() {
        return usrDireccion;
    }

    public void setUsrDireccion(String usrDireccion) {
        this.usrDireccion = usrDireccion;
    }

    public String getUsrTdcNumero() {
        return usrTdcNumero;
    }

    public void setUsrTdcNumero(String usrTdcNumero) {
        this.usrTdcNumero = usrTdcNumero;
    }

    public String getUsrTdcCodigo() {
        return usrTdcCodigo;
    }

    public void setUsrTdcCodigo(String usrTdcCodigo) {
        this.usrTdcCodigo = usrTdcCodigo;
    }

    public String getUsrTdcVigencia() {
        return usrTdcVigencia;
    }

    public void setUsrTdcVigencia(String usrTdcVigencia) {
        this.usrTdcVigencia = usrTdcVigencia;
    }

    public int getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(int idBanco) {
        this.idBanco = idBanco;
    }

    public int getIdTipoTarjeta() {
        return idTipoTarjeta;
    }

    public void setIdTipoTarjeta(int idTipoTarjeta) {
        this.idTipoTarjeta = idTipoTarjeta;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdUsrStatus() {
        return idUsrStatus;
    }

    public void setIdUsrStatus(int idUsrStatus) {
        this.idUsrStatus = idUsrStatus;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getTipoCedula() {
        return tipoCedula;
    }

    public void setTipoCedula(int tipoCedula) {
        this.tipoCedula = tipoCedula;
    }

    public int getRecibirSMS() {
        return recibirSMS;
    }

    public void setRecibirSMS(int recibirSMS) {
        this.recibirSMS = recibirSMS;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public int getGemalto() {
        return gemalto;
    }

    public void setGemalto(int gemalto) {
        this.gemalto = gemalto;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getWkey() {
        return wkey;
    }

    public void setWkey(String wkey) {
        this.wkey = wkey;
    }

    public String getTelefonoOriginal() {
        return telefonoOriginal;
    }

    public void setTelefonoOriginal(String telefonoOriginal) {
        this.telefonoOriginal = telefonoOriginal;
    }

    public String getUsrMaterno() {
        return usrMaterno;
    }

    public void setUsrMaterno(String usrMaterno) {
        this.usrMaterno = usrMaterno;
    }

    public String getUsrSexo() {
        return usrSexo;
    }

    public void setUsrSexo(String usrSexo) {
        this.usrSexo = usrSexo;
    }

    public String getUsrTelCasa() {
        return usrTelCasa;
    }

    public void setUsrTelCasa(String usrTelCasa) {
        this.usrTelCasa = usrTelCasa;
    }

    public String getUsrTelOficina() {
        return usrTelOficina;
    }

    public void setUsrTelOficina(String usrTelOficina) {
        this.usrTelOficina = usrTelOficina;
    }

    public int getUsrIdEstado() {
        return usrIdEstado;
    }

    public void setUsrIdEstado(int usrIdEstado) {
        this.usrIdEstado = usrIdEstado;
    }

    public String getUsrCiudad() {
        return usrCiudad;
    }

    public void setUsrCiudad(String usrCiudad) {
        this.usrCiudad = usrCiudad;
    }

    public String getUsrCalle() {
        return usrCalle;
    }

    public void setUsrCalle(String usrCalle) {
        this.usrCalle = usrCalle;
    }

    public int getUsrNumExt() {
        return usrNumExt;
    }

    public void setUsrNumExt(int usrNumExt) {
        this.usrNumExt = usrNumExt;
    }

    public String getUsrNumInterior() {
        return usrNumInterior;
    }

    public void setUsrNumInterior(String usrNumInterior) {
        this.usrNumInterior = usrNumInterior;
    }

    public String getUsrColonia() {
        return usrColonia;
    }

    public void setUsrColonia(String usrColonia) {
        this.usrColonia = usrColonia;
    }

    public String getUsrCp() {
        return usrCp;
    }

    public void setUsrCp(String usrCp) {
        this.usrCp = usrCp;
    }

    public String getUsrDomAmex() {
        return usrDomAmex;
    }

    public void setUsrDomAmex(String usrDomAmex) {
        this.usrDomAmex = usrDomAmex;
    }

    public String getUsrTerminos() {
        return usrTerminos;
    }

    public void setUsrTerminos(String usrTerminos) {
        this.usrTerminos = usrTerminos;
    }

    public String getNumExtStr() {
        return numExtStr;
    }

    public void setNumExtStr(String numExtStr) {
        this.numExtStr = numExtStr;
    }

    public String getIdIngo() {
        return idIngo;
    }

    public void setIdIngo(String idIngo) {
        this.idIngo = idIngo;
    }

    public String getUsrNss() {
        return usrNss;
    }

    public void setUsrNss(String usrNss) {
        this.usrNss = usrNss;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getIdApp() {
        return idApp;
    }

    public void setIdApp(String idApp) {
        this.idApp = idApp;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isNegocio() {
        return negocio;
    }

    public void setNegocio(boolean negocio) {
        this.negocio = negocio;
    }

    public Object getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(Object smsCode) {
        this.smsCode = smsCode;
    }

    public String getUsrEmail() {
        return usrEmail;
    }

    public void setUsrEmail(String usrEmail) {
        this.usrEmail = usrEmail;
    }

    public String getUsrSms() {
        return usrSms;
    }

    public void setUsrSms(String usrSms) {
        this.usrSms = usrSms;
    }

    public String getUsrJumio() {
        return Strings.nullToEmpty(usrJumio);
    }

    public void setUsrJumio(String usrJumio) {
        this.usrJumio = usrJumio;
    }

    public String getScanReference() {
        return Strings.nullToEmpty(scanReference);
    }

    public void setScanReference(String scanReference) {
        this.scanReference = scanReference;
    }

    public String getImg() {
        return Strings.nullToEmpty(img);
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("ideUsuario", ideUsuario)
                .add("usrLogin", usrLogin)
                .add("usrTelefono", usrTelefono)
                .add("usrNombre", usrNombre)
                .add("usrApellido", usrApellido)
                .add("idUsrStatus", idUsrStatus)
                .add("idPais", idPais)
                .add("eMail", eMail)
                .add("imei", imei)
                .add("usrMaterno", usrMaterno)
                .add("usrEmail", usrEmail)
                .add("usrSms", usrSms)
                .add("usrJumio", usrJumio)
                .add("scanReference", scanReference)
                .toString();
    }
}
