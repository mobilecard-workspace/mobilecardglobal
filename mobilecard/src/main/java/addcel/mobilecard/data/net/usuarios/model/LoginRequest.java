package addcel.mobilecard.data.net.usuarios.model;

import addcel.mobilecard.ui.login.tipo.LoginTipoContract;

/**
 * ADDCEL on 14/07/17.
 */
public final class LoginRequest {

    private final String usrLoginOrEmail;
    private final String usrPwd;
    private final String imei;
    private final String platform;
    private final String os;
    private final String manufacturer;
    private final LoginTipoContract.Tipo tipoUsuario;

    private LoginRequest(Builder builder) {
        usrLoginOrEmail = builder.usrLoginOrEmail;
        usrPwd = builder.usrPwd;
        imei = builder.imei;
        platform = builder.platform;
        os = builder.os;
        manufacturer = builder.manufacturer;
        tipoUsuario = builder.tipoUsuario;
    }

    public String getUsrLoginOrEmail() {
        return usrLoginOrEmail;
    }

    public String getUsrPwd() {
        return usrPwd;
    }

    public String getImei() {
        return imei;
    }

    public String getPlatform() {
        return platform;
    }

    public String getOs() {
        return os;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public LoginTipoContract.Tipo getTipoUsuario() {
        return tipoUsuario;
    }

    public static class Builder {
        private String usrLoginOrEmail;
        private String usrPwd;
        private String imei;
        private String platform;
        private String os;
        private String manufacturer;
        private LoginTipoContract.Tipo tipoUsuario;

        public Builder() {
        }

        public Builder setUsrLoginOrEmail(String usrLoginOrEmail) {
            this.usrLoginOrEmail = usrLoginOrEmail;
            return this;
        }

        public Builder setUsrPwd(String usrPwd) {
            this.usrPwd = usrPwd;
            return this;
        }

        public Builder setImei(String imei) {
            this.imei = imei;
            return this;
        }

        public Builder setPlatform(String platform) {
            this.platform = platform;
            return this;
        }

        public Builder setOs(String os) {
            this.os = os;
            return this;
        }

        public Builder setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }

        public Builder setTipoUsuario(LoginTipoContract.Tipo tipoUsuario) {
            this.tipoUsuario = tipoUsuario;
            return this;
        }

        public LoginRequest build() {
            return new LoginRequest(this);
        }
    }
}
