package addcel.mobilecard.data.net.usuarios.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public class Ciudad {

    @SerializedName("county_name")
    private String mCountyName;
    @SerializedName("description")
    private Object mDescription;
    @SerializedName("feat_class")
    private String mFeatClass;
    @SerializedName("feature_id")
    private String mFeatureId;
    @SerializedName("fips_class")
    private String mFipsClass;
    @SerializedName("fips_county_cd")
    private String mFipsCountyCd;
    @SerializedName("full_county_name")
    private String mFullCountyName;
    @SerializedName("link_title")
    private Object mLinkTitle;
    @SerializedName("name")
    private String mName;
    @SerializedName("primary_latitude")
    private String mPrimaryLatitude;
    @SerializedName("primary_longitude")
    private String mPrimaryLongitude;
    @SerializedName("state_abbreviation")
    private String mStateAbbreviation;
    @SerializedName("state_name")
    private String mStateName;
    @SerializedName("url")
    private String mUrl;

    public String getCountyName() {
        return mCountyName;
    }

    public void setCountyName(String countyName) {
        mCountyName = countyName;
    }

    public Object getDescription() {
        return mDescription;
    }

    public void setDescription(Object description) {
        mDescription = description;
    }

    public String getFeatClass() {
        return mFeatClass;
    }

    public void setFeatClass(String featClass) {
        mFeatClass = featClass;
    }

    public String getFeatureId() {
        return mFeatureId;
    }

    public void setFeatureId(String featureId) {
        mFeatureId = featureId;
    }

    public String getFipsClass() {
        return mFipsClass;
    }

    public void setFipsClass(String fipsClass) {
        mFipsClass = fipsClass;
    }

    public String getFipsCountyCd() {
        return mFipsCountyCd;
    }

    public void setFipsCountyCd(String fipsCountyCd) {
        mFipsCountyCd = fipsCountyCd;
    }

    public String getFullCountyName() {
        return mFullCountyName;
    }

    public void setFullCountyName(String fullCountyName) {
        mFullCountyName = fullCountyName;
    }

    public Object getLinkTitle() {
        return mLinkTitle;
    }

    public void setLinkTitle(Object linkTitle) {
        mLinkTitle = linkTitle;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPrimaryLatitude() {
        return mPrimaryLatitude;
    }

    public void setPrimaryLatitude(String primaryLatitude) {
        mPrimaryLatitude = primaryLatitude;
    }

    public String getPrimaryLongitude() {
        return mPrimaryLongitude;
    }

    public void setPrimaryLongitude(String primaryLongitude) {
        mPrimaryLongitude = primaryLongitude;
    }

    public String getStateAbbreviation() {
        return mStateAbbreviation;
    }

    public void setStateAbbreviation(String stateAbbreviation) {
        mStateAbbreviation = stateAbbreviation;
    }

    public String getStateName() {
        return mStateName;
    }

    public void setStateName(String stateName) {
        mStateName = stateName;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @NotNull
    @Override
    public String toString() {
        return mName;
    }
}
