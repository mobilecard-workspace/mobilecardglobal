package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ObjReceiptModel implements Parcelable {

    public static final Creator<ObjReceiptModel> CREATOR = new Creator<ObjReceiptModel>() {
        @Override
        public ObjReceiptModel createFromParcel(Parcel in) {
            return new ObjReceiptModel(in);
        }

        @Override
        public ObjReceiptModel[] newArray(int size) {
            return new ObjReceiptModel[size];
        }
    };
    @SerializedName("accReceiver")
    private String mAccReceiver;
    @SerializedName("achAccount")
    private String mAchAccount;
    @SerializedName("addressPayer")
    private String mAddressPayer;
    @SerializedName("addressReceiver")
    private String mAddressReceiver;
    @SerializedName("addressRpt")
    private String mAddressRpt;
    @SerializedName("addressSender")
    private String mAddressSender;
    @SerializedName("businessHours")
    private String mBusinessHours;
    @SerializedName("cardTranId")
    private String mCardTranId;
    @SerializedName("citySender")
    private String mCitySender;
    @SerializedName("companyPayer")
    private String mCompanyPayer;
    @SerializedName("currencyPayer")
    private String mCurrencyPayer;
    @SerializedName("currencySrc")
    private String mCurrencySrc;
    @SerializedName("dateReceiver")
    private DateReceiverModel mDateReceiver;
    @SerializedName("discountValue")
    private String mDiscountValue;
    @SerializedName("fxRateCustomer")
    private String mFxRateCustomer;
    @SerializedName("idBranch")
    private String mIdBranch;
    @SerializedName("idFlagReceiver")
    private String mIdFlagReceiver;
    @SerializedName("idPayment")
    private String mIdPayment;
    @SerializedName("idReceiver")
    private String mIdReceiver;
    @SerializedName("idSender")
    private String mIdSender;
    @SerializedName("mexPhone")
    private String mMexPhone;
    @SerializedName("modePayReceiver")
    private String mModePayReceiver;
    @SerializedName("nameCityReceiver")
    private String mNameCityReceiver;
    @SerializedName("nameCountryReceiver")
    private String mNameCountryReceiver;
    @SerializedName("nameCountrySender")
    private String mNameCountrySender;
    @SerializedName("namePayer")
    private String mNamePayer;
    @SerializedName("nameReceiver")
    private String mNameReceiver;
    @SerializedName("nameSender")
    private String mNameSender;
    @SerializedName("nameStateReceiver")
    private String mNameStateReceiver;
    @SerializedName("netAmountReceiver")
    private Double mNetAmountReceiver;
    @SerializedName("notesReceiver")
    private String mNotesReceiver;
    @SerializedName("originalFreeValue")
    private String mOriginalFreeValue;
    @SerializedName("passwordReceiver")
    private String mPasswordReceiver;
    @SerializedName("paymentName")
    private String mPaymentName;
    @SerializedName("phone1Payer")
    private String mPhone1Payer;
    @SerializedName("phone1Receiver")
    private String mPhone1Receiver;
    @SerializedName("phone1Sender")
    private String mPhone1Sender;
    @SerializedName("phone2Receiver")
    private String mPhone2Receiver;
    @SerializedName("phone2Sender")
    private String mPhone2Sender;
    @SerializedName("rateChangeReceiver")
    private Double mRateChangeReceiver;
    @SerializedName("receiptDisclaimer")
    private String mReceiptDisclaimer;
    @SerializedName("receiverDateAvailable")
    private String mReceiverDateAvailable;
    @SerializedName("stateSender")
    private String mStateSender;
    @SerializedName("taxPercentage")
    private Double mTaxPercentage;
    @SerializedName("telexReceiver")
    private Double mTelexReceiver;
    @SerializedName("tollFreeRpt")
    private String mTollFreeRpt;
    @SerializedName("totalPayReceiver")
    private Double mTotalPayReceiver;
    @SerializedName("totalReceiver")
    private Double mTotalReceiver;
    @SerializedName("transferTaxes")
    private Double mTransferTaxes;
    @SerializedName("zipSender")
    private String mZipSender;

    protected ObjReceiptModel(Parcel in) {
        mAccReceiver = in.readString();
        mAchAccount = in.readString();
        mAddressPayer = in.readString();
        mAddressReceiver = in.readString();
        mAddressRpt = in.readString();
        mAddressSender = in.readString();
        mBusinessHours = in.readString();
        mCardTranId = in.readString();
        mCitySender = in.readString();
        mCompanyPayer = in.readString();
        mCurrencyPayer = in.readString();
        mCurrencySrc = in.readString();
        mDateReceiver = in.readParcelable(DateReceiverModel.class.getClassLoader());
        mDiscountValue = in.readString();
        mFxRateCustomer = in.readString();
        mIdBranch = in.readString();
        mIdFlagReceiver = in.readString();
        mIdPayment = in.readString();
        mIdReceiver = in.readString();
        mIdSender = in.readString();
        mMexPhone = in.readString();
        mModePayReceiver = in.readString();
        mNameCityReceiver = in.readString();
        mNameCountryReceiver = in.readString();
        mNameCountrySender = in.readString();
        mNamePayer = in.readString();
        mNameReceiver = in.readString();
        mNameSender = in.readString();
        mNameStateReceiver = in.readString();
        mNotesReceiver = in.readString();
        mOriginalFreeValue = in.readString();
        mPasswordReceiver = in.readString();
        mPaymentName = in.readString();
        mPhone1Payer = in.readString();
        mPhone1Receiver = in.readString();
        mPhone1Sender = in.readString();
        mPhone2Receiver = in.readString();
        mPhone2Sender = in.readString();
        mReceiptDisclaimer = in.readString();
        mReceiverDateAvailable = in.readString();
        mStateSender = in.readString();
        mTollFreeRpt = in.readString();
        mZipSender = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAccReceiver);
        dest.writeString(mAchAccount);
        dest.writeString(mAddressPayer);
        dest.writeString(mAddressReceiver);
        dest.writeString(mAddressRpt);
        dest.writeString(mAddressSender);
        dest.writeString(mBusinessHours);
        dest.writeString(mCardTranId);
        dest.writeString(mCitySender);
        dest.writeString(mCompanyPayer);
        dest.writeString(mCurrencyPayer);
        dest.writeString(mCurrencySrc);
        dest.writeParcelable(mDateReceiver, flags);
        dest.writeString(mDiscountValue);
        dest.writeString(mFxRateCustomer);
        dest.writeString(mIdBranch);
        dest.writeString(mIdFlagReceiver);
        dest.writeString(mIdPayment);
        dest.writeString(mIdReceiver);
        dest.writeString(mIdSender);
        dest.writeString(mMexPhone);
        dest.writeString(mModePayReceiver);
        dest.writeString(mNameCityReceiver);
        dest.writeString(mNameCountryReceiver);
        dest.writeString(mNameCountrySender);
        dest.writeString(mNamePayer);
        dest.writeString(mNameReceiver);
        dest.writeString(mNameSender);
        dest.writeString(mNameStateReceiver);
        dest.writeString(mNotesReceiver);
        dest.writeString(mOriginalFreeValue);
        dest.writeString(mPasswordReceiver);
        dest.writeString(mPaymentName);
        dest.writeString(mPhone1Payer);
        dest.writeString(mPhone1Receiver);
        dest.writeString(mPhone1Sender);
        dest.writeString(mPhone2Receiver);
        dest.writeString(mPhone2Sender);
        dest.writeString(mReceiptDisclaimer);
        dest.writeString(mReceiverDateAvailable);
        dest.writeString(mStateSender);
        dest.writeString(mTollFreeRpt);
        dest.writeString(mZipSender);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAccReceiver() {
        return mAccReceiver;
    }

    public void setAccReceiver(String accReceiver) {
        mAccReceiver = accReceiver;
    }

    public String getAchAccount() {
        return mAchAccount;
    }

    public void setAchAccount(String achAccount) {
        mAchAccount = achAccount;
    }

    public String getAddressPayer() {
        return mAddressPayer;
    }

    public void setAddressPayer(String addressPayer) {
        mAddressPayer = addressPayer;
    }

    public String getAddressReceiver() {
        return mAddressReceiver;
    }

    public void setAddressReceiver(String addressReceiver) {
        mAddressReceiver = addressReceiver;
    }

    public String getAddressRpt() {
        return mAddressRpt;
    }

    public void setAddressRpt(String addressRpt) {
        mAddressRpt = addressRpt;
    }

    public String getAddressSender() {
        return mAddressSender;
    }

    public void setAddressSender(String addressSender) {
        mAddressSender = addressSender;
    }

    public String getBusinessHours() {
        return mBusinessHours;
    }

    public void setBusinessHours(String businessHours) {
        mBusinessHours = businessHours;
    }

    public String getCardTranId() {
        return mCardTranId;
    }

    public void setCardTranId(String cardTranId) {
        mCardTranId = cardTranId;
    }

    public String getCitySender() {
        return mCitySender;
    }

    public void setCitySender(String citySender) {
        mCitySender = citySender;
    }

    public String getCompanyPayer() {
        return mCompanyPayer;
    }

    public void setCompanyPayer(String companyPayer) {
        mCompanyPayer = companyPayer;
    }

    public String getCurrencyPayer() {
        return mCurrencyPayer;
    }

    public void setCurrencyPayer(String currencyPayer) {
        mCurrencyPayer = currencyPayer;
    }

    public String getCurrencySrc() {
        return mCurrencySrc;
    }

    public void setCurrencySrc(String currencySrc) {
        mCurrencySrc = currencySrc;
    }

    public DateReceiverModel getDateReceiver() {
        return mDateReceiver;
    }

    public void setDateReceiver(DateReceiverModel dateReceiver) {
        mDateReceiver = dateReceiver;
    }

    public String getDiscountValue() {
        return mDiscountValue;
    }

    public void setDiscountValue(String discountValue) {
        mDiscountValue = discountValue;
    }

    public String getFxRateCustomer() {
        return mFxRateCustomer;
    }

    public void setFxRateCustomer(String fxRateCustomer) {
        mFxRateCustomer = fxRateCustomer;
    }

    public String getIdBranch() {
        return mIdBranch;
    }

    public void setIdBranch(String idBranch) {
        mIdBranch = idBranch;
    }

    public String getIdFlagReceiver() {
        return mIdFlagReceiver;
    }

    public void setIdFlagReceiver(String idFlagReceiver) {
        mIdFlagReceiver = idFlagReceiver;
    }

    public String getIdPayment() {
        return mIdPayment;
    }

    public void setIdPayment(String idPayment) {
        mIdPayment = idPayment;
    }

    public String getIdReceiver() {
        return mIdReceiver;
    }

    public void setIdReceiver(String idReceiver) {
        mIdReceiver = idReceiver;
    }

    public String getIdSender() {
        return mIdSender;
    }

    public void setIdSender(String idSender) {
        mIdSender = idSender;
    }

    public String getMexPhone() {
        return mMexPhone;
    }

    public void setMexPhone(String mexPhone) {
        mMexPhone = mexPhone;
    }

    public String getModePayReceiver() {
        return mModePayReceiver;
    }

    public void setModePayReceiver(String modePayReceiver) {
        mModePayReceiver = modePayReceiver;
    }

    public String getNameCityReceiver() {
        return mNameCityReceiver;
    }

    public void setNameCityReceiver(String nameCityReceiver) {
        mNameCityReceiver = nameCityReceiver;
    }

    public String getNameCountryReceiver() {
        return mNameCountryReceiver;
    }

    public void setNameCountryReceiver(String nameCountryReceiver) {
        mNameCountryReceiver = nameCountryReceiver;
    }

    public String getNameCountrySender() {
        return mNameCountrySender;
    }

    public void setNameCountrySender(String nameCountrySender) {
        mNameCountrySender = nameCountrySender;
    }

    public String getNamePayer() {
        return mNamePayer;
    }

    public void setNamePayer(String namePayer) {
        mNamePayer = namePayer;
    }

    public String getNameReceiver() {
        return mNameReceiver;
    }

    public void setNameReceiver(String nameReceiver) {
        mNameReceiver = nameReceiver;
    }

    public String getNameSender() {
        return mNameSender;
    }

    public void setNameSender(String nameSender) {
        mNameSender = nameSender;
    }

    public String getNameStateReceiver() {
        return mNameStateReceiver;
    }

    public void setNameStateReceiver(String nameStateReceiver) {
        mNameStateReceiver = nameStateReceiver;
    }

    public Double getNetAmountReceiver() {
        return mNetAmountReceiver;
    }

    public void setNetAmountReceiver(Double netAmountReceiver) {
        mNetAmountReceiver = netAmountReceiver;
    }

    public String getNotesReceiver() {
        return mNotesReceiver;
    }

    public void setNotesReceiver(String notesReceiver) {
        mNotesReceiver = notesReceiver;
    }

    public String getOriginalFreeValue() {
        return mOriginalFreeValue;
    }

    public void setOriginalFreeValue(String originalFreeValue) {
        mOriginalFreeValue = originalFreeValue;
    }

    public String getPasswordReceiver() {
        return mPasswordReceiver;
    }

    public void setPasswordReceiver(String passwordReceiver) {
        mPasswordReceiver = passwordReceiver;
    }

    public String getPaymentName() {
        return mPaymentName;
    }

    public void setPaymentName(String paymentName) {
        mPaymentName = paymentName;
    }

    public String getPhone1Payer() {
        return mPhone1Payer;
    }

    public void setPhone1Payer(String phone1Payer) {
        mPhone1Payer = phone1Payer;
    }

    public String getPhone1Receiver() {
        return mPhone1Receiver;
    }

    public void setPhone1Receiver(String phone1Receiver) {
        mPhone1Receiver = phone1Receiver;
    }

    public String getPhone1Sender() {
        return mPhone1Sender;
    }

    public void setPhone1Sender(String phone1Sender) {
        mPhone1Sender = phone1Sender;
    }

    public String getPhone2Receiver() {
        return mPhone2Receiver;
    }

    public void setPhone2Receiver(String phone2Receiver) {
        mPhone2Receiver = phone2Receiver;
    }

    public String getPhone2Sender() {
        return mPhone2Sender;
    }

    public void setPhone2Sender(String phone2Sender) {
        mPhone2Sender = phone2Sender;
    }

    public Double getRateChangeReceiver() {
        return mRateChangeReceiver;
    }

    public void setRateChangeReceiver(Double rateChangeReceiver) {
        mRateChangeReceiver = rateChangeReceiver;
    }

    public String getReceiptDisclaimer() {
        return mReceiptDisclaimer;
    }

    public void setReceiptDisclaimer(String receiptDisclaimer) {
        mReceiptDisclaimer = receiptDisclaimer;
    }

    public String getReceiverDateAvailable() {
        return mReceiverDateAvailable;
    }

    public void setReceiverDateAvailable(String receiverDateAvailable) {
        mReceiverDateAvailable = receiverDateAvailable;
    }

    public String getStateSender() {
        return mStateSender;
    }

    public void setStateSender(String stateSender) {
        mStateSender = stateSender;
    }

    public Double getTaxPercentage() {
        return mTaxPercentage;
    }

    public void setTaxPercentage(Double taxPercentage) {
        mTaxPercentage = taxPercentage;
    }

    public Double getTelexReceiver() {
        return mTelexReceiver;
    }

    public void setTelexReceiver(Double telexReceiver) {
        mTelexReceiver = telexReceiver;
    }

    public String getTollFreeRpt() {
        return mTollFreeRpt;
    }

    public void setTollFreeRpt(String tollFreeRpt) {
        mTollFreeRpt = tollFreeRpt;
    }

    public Double getTotalPayReceiver() {
        return mTotalPayReceiver;
    }

    public void setTotalPayReceiver(Double totalPayReceiver) {
        mTotalPayReceiver = totalPayReceiver;
    }

    public Double getTotalReceiver() {
        return mTotalReceiver;
    }

    public void setTotalReceiver(Double totalReceiver) {
        mTotalReceiver = totalReceiver;
    }

    public Double getTransferTaxes() {
        return mTransferTaxes;
    }

    public void setTransferTaxes(Double transferTaxes) {
        mTransferTaxes = transferTaxes;
    }

    public String getZipSender() {
        return mZipSender;
    }

    public void setZipSender(String zipSender) {
        mZipSender = zipSender;
    }
}
