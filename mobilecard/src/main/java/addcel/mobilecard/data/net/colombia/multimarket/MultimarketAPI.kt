package addcel.mobilecard.data.net.colombia.multimarket

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 2019-12-18.
 */
interface MultimarketAPI {

    /*
        FLUJO COMPRA PRODUCTOS
     */

    @Headers(
            "Authorization: Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"
    )
    @GET("ColombiaMiddleware/{idApp}/{idPais}/{idioma}/multimarket/api/catalogoProductos")
    fun getProductos(@Path("idApp") idApp: Int,
                     @Path("idPais") idPais: Int,
                     @Path("idioma") idioma: String): Observable<MmProductosResponse>

    @Headers(
            "Authorization: Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"
    )
    @POST("ColombiaMiddleware/multimarket/api/compraProducto")
    fun compraProducto(@Body body: MmCompraProductoEntity): Observable<MmPagoResponse>

    /*
        FLUJO PAGO SERVICIOS
    */

    @Headers(
            "Authorization: Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"
    )
    @GET("ColombiaMiddleware/{idApp}/{idPais}/{idioma}/multimarket/api/catalogoCategorias")
    fun getCategoriasServicio(@Path("idApp") idApp: Int,
                              @Path("idPais") idPais: Int,
                              @Path("idioma") idioma: String): Observable<MmCategoriasResponse>

    @Headers(
            "Authorization: Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"
    )
    @GET("ColombiaMiddleware/{idApp}/{idPais}/{idioma}/multimarket/api/catalogoServicios")
    fun getServicios(@Path("idApp") idApp: Int,
                     @Path("idPais") idPais: Int,
                     @Path("idioma") idioma: String,
                     @Query("categoria") categoria: String): Observable<MmServiciosResponse>


    @Headers(
            "Authorization: Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"
    )
    @POST("ColombiaMiddleware/multimarket/api/escaneaFactura")
    fun escaneaFactura(@Body body: MmConsultaEntity): Observable<MmConsultaResponse>

    @Headers(
            "Authorization: Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"
    )
    @POST("ColombiaMiddleware/multimarket/api/pagoServicio")
    fun pagoServicio(@Body body: MmPagoServicioEntity): Observable<MmPagoResponse>

    companion object {
        fun get(r: Retrofit): MultimarketAPI {
            return r.create(MultimarketAPI::class.java)
        }
    }
}