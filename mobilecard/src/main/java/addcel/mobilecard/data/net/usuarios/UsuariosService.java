package addcel.mobilecard.data.net.usuarios;

import addcel.mobilecard.data.net.negocios.entity.DefaultResponse;
import addcel.mobilecard.data.net.usuarios.model.DataUpdateResponse;
import addcel.mobilecard.data.net.usuarios.model.LoginRequest;
import addcel.mobilecard.data.net.usuarios.model.McResponse;
import addcel.mobilecard.data.net.usuarios.model.PushResponse;
import addcel.mobilecard.data.net.usuarios.model.PushTokenRequest;
import addcel.mobilecard.data.net.usuarios.model.RegistroRequest;
import addcel.mobilecard.data.net.usuarios.model.RemovePushTokenRequest;
import addcel.mobilecard.data.net.usuarios.model.SmsResponse;
import addcel.mobilecard.data.net.usuarios.model.TerminosResponse;
import addcel.mobilecard.data.net.usuarios.model.UserValidation;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.ui.login.tipo.LoginTipoContract;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * ADDCEL on 11/11/16.
 */
public interface UsuariosService {

    String API = "Usuarios/{idApp}/";

    String PUSH_AUTH = "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx";

    /**
     * @param idioma  - El idioma actual del dispositivo
     * @param request - { "card": { "codigo": "T5n5l1gcaZoE+aGD1jLCcw==", //<b>HARD</b> "cpAmex":
     *                "69", "determinada": true, "domAmex": "Juan Escutia", "idTarjeta": 0, "idUsuario": 0,
     *                "isMobilecard": false, "nombre": "Carlos Garcia", "pan": "4mX9TBRwUR/U7YsJJDzSvQ==",
     *                //<b>HARD</b> "tipoTarjetaEntity": "DEBITO", "vigencia": "AypGZez27YHoVsRuTSsPiw=="
     *                //<b>HARD</b> }, "country": 1, "email": "elmans8@gmail.com", "firstName": "Carlos",
     *                "gender": "M", "imei": "354116077382634", "lastName": "García", "manufacturer": "motorola",
     *                "os": "7.0", "password": "7aO1Rfve99pVnLYBO9cduA==", //<b>HARD</b> "phone": "5518307722",
     *                "platform": "ANDROID" } Donde password se cifra con metodo <b>HARD</b> y si se agrega una
     *                tarjeta se envia como elemento "card" la misma estructura que se usa en el api de Wallet.
     *                En caso de no agregar tarjeta, no se incluye el elemento 'card'
     * @return El mismo objeto que la version anterior del API
     */
    @POST(API + "{idioma}/user/insertv3")
    Observable<Usuario> create(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Body RegistroRequest request);

    /**
     * @param idioma - El idioma actual del dispositivo
     * @param json   - { "imei": "354116077382634", "manufacturer": "motorola", "os": "24", "platform":
     *               "ANDROID", "usrLoginOrEmail": "elmansss", "usrPwd": "7aO1Rfve99pVnLYBO9cduA=="
     *               //<b>HARD</b> }
     * @return El mismo objeto que la version anterior del API
     */
    @POST(API + "{idPais}/{idioma}/user/login")
    Observable<Usuario> login(@Path("idApp") int idApp,
                              @Path("idPais") int idPais, @Path("idioma") String idioma, @Body LoginRequest json);

    /**
     * @param idioma    - El idioma actual del dispositivo
     * @param idUsuario - Id numerica del usuario logueado
     * @param oldPass   - Password actual del usuario //<b>HARD</b>
     * @param newPass   - Password nuevo capturado //<b>HARD</b>
     * @return El mismo objeto que la version anterior del API
     */
    @FormUrlEncoded
    @POST(API + "{idioma}/{idUsuario}/password/update")
    Observable<McResponse> updatePassword(@Path("idApp") int idApp, @Path("idioma") String idioma,
                                          @Path("idUsuario") long idUsuario, @Field("old") String oldPass,
                                          @Field("new") String newPass);

    /**
     * @param idioma      -El idioma actual del dispositivo
     * @param userOrEmail - Login (version anterior del API) o correo electronico (dato que se usara
     *                    como login en esta version)
     * @return El mismo objeto que la version anterior del API
     */
    @FormUrlEncoded
    @POST(API + "{idioma}/password/reset")
    Observable<McResponse> resetPassword(
            @Path("idApp") int idApp, @Path("idioma") String idioma,
            @Field("userOrEmail") String userOrEmail);

    @FormUrlEncoded
    @POST(API + "{idioma}/{idUsuario}/{name}/update")
    Observable<DataUpdateResponse> updateRx(@Path("idApp") int idApp, @Path("idioma") String idioma,
                                            @Path("idUsuario") long idUsuario, @Path("name") String name, @Field("value") String value);

    /**
     * @param idioma    - El idioma actual del dispositivo
     * @param idUsuario - Id numerica del usuario logueado
     * @return El mismo objeto que la version anterior del API
     */
    @GET(API + "{idioma}/{idUsuario}/activate")
    Observable<McResponse> activateUser(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Path("idUsuario") long idUsuario);

    @FormUrlEncoded
    @POST(API + "{idioma}/user/verify")
    Observable<UserValidation> verifyUser(
            @Path("idApp") int idApp, @Path("idioma") String idioma, @Field("idUsuario") long idUsuario);

    @GET(API + "terminos/{idPais}/{idioma}")
    Observable<TerminosResponse> getTerminos(
            @Path("idApp") int idApp, @Header("client") String client, @Path("idPais") int idPais,
            @Path("idioma") String idioma);

    @GET(API + "privacidad/{idPais}/{idioma}")
    Observable<TerminosResponse> getAvisoPrivacidad(
            @Path("idApp") int idApp, @Header("client") String client, @Path("idPais") int idPais,
            @Path("idioma") String idioma);

    @GET(API + "{tipoTerm}/{idPais}/{idioma}")
    Observable<TerminosResponse> getTermsParam(
            @Path("idApp") int idApp, @Header("client") String client, @Path("tipoTerm") String tipoTerm,
            @Path("idPais") int idPais, @Path("idioma") String idioma);

    @FormUrlEncoded
    @POST(API + "{idPais}/{idioma}/{tipoUsuario}/{id}/sms/validate")
    Observable<SmsResponse> validateSMS(@Path("idApp") int idApp, @Path("idPais") int idPais,
                                        @Path("idioma") String idioma, @Path("tipoUsuario") LoginTipoContract.Tipo tipoUsuario,
                                        @Path("id") long id, @Field("codigo") String codigo);

    @POST(API + "{idPais}/{idioma}/{tipoUsuario}/{id}/sms/resend")
    Observable<DefaultResponse> resendSMS(@Path("idApp") int idApp, @Path("idPais") int idPais,
                                          @Path("idioma") String idioma, @Path("tipoUsuario") LoginTipoContract.Tipo tipoUsuario,
                                          @Path("id") long id);

    @FormUrlEncoded
    @POST(API + "{idioma}/{idPais}/user/updatereference")
    Observable<McResponse> updateReference(@Path("idApp") int idApp, @Path("idPais") int idPais,
                                           @Path("idioma") String idioma, @Field("login") String login,
                                           @Field("scanReference") String reference);


    @POST("PushNotifications/saveToken")
    Observable<PushResponse> saveToken(@Header("authorization") String authorization, @Body PushTokenRequest request);


    @HTTP(method = "DELETE", path = "PushNotifications/removeToken", hasBody = true)
    Observable<PushResponse> deleteToken(@Header("authorization") String authorization, @Body RemovePushTokenRequest request);
}
