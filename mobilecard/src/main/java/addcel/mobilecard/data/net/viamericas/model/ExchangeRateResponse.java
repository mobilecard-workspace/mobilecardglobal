package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

@SuppressWarnings("unused")
public class ExchangeRateResponse extends McResponse {

    @SerializedName("exchangeRateRes")
    private List<ExchangeRateModel> mExchangeRateRes;

    public ExchangeRateResponse() {
    }

    public ExchangeRateResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public List<ExchangeRateModel> getExchangeRateRes() {
        return mExchangeRateRes;
    }

    public void setExchangeRateRes(List<ExchangeRateModel> exchangeRateRes) {
        mExchangeRateRes = exchangeRateRes;
    }
}
