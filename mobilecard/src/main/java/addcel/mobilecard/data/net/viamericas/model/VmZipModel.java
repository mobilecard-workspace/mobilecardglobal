package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

@SuppressWarnings("unused")
public class VmZipModel extends McResponse {

    @SerializedName("cityName")
    private String mCityName;
    @SerializedName("idCity")
    private String mIdCity;
    @SerializedName("idState")
    private String mIdState;
    @SerializedName("zipCode")
    private String mZipCode;

    public VmZipModel(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public VmZipModel(String idState, String idCity, String zipCode) {
        super(0, "");
        mIdState = idState;
        mIdCity = idCity;
        mZipCode = zipCode;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setCityName(String cityName) {
        mCityName = cityName;
    }

    public String getIdCity() {
        return mIdCity;
    }

    public void setIdCity(String idCity) {
        mIdCity = idCity;
    }

    public String getIdState() {
        return mIdState;
    }

    public void setIdState(String idState) {
        mIdState = idState;
    }

    public String getZipCode() {
        return mZipCode;
    }

    public void setZipCode(String zipCode) {
        mZipCode = zipCode;
    }

    @NotNull
    @Override
    public String toString() {
        return mZipCode;
    }
}
