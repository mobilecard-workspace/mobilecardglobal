package addcel.mobilecard.data.net.colombia.entity;

/**
 * ADDCEL on 10/18/18.
 */
public final class ColombiaTokenEntity {
    private int idError;
    private String mensajeError;
    private String token;

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public String getToken() {
        return token;
    }
}
