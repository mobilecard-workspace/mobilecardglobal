package addcel.mobilecard.data.net.aci.model;

/**
 * ADDCEL on 28/03/17.
 */
public final class NoDataPagoRequest extends BaseRequest {

    private final long idUsuario;
    private final int idTarjeta;
    // cvv2
    private final String codigo;
    // direccion
    private final String direccionUsuario;
    private final String ciudadUsuario;
    private final int estadoUsuario;
    private final String cpUsuario;
    private final double monto;
    private final String referencia;
    private final String nombreServicio;
    private final String direccionServicio;
    private final String ciudadServicio;
    private final String idServicio;
    private final String software;
    private final String wkey;
    private final String imei;
    private final String modelo;

    private NoDataPagoRequest(Builder builder) {
        setIdioma(builder.idioma);
        idUsuario = builder.idUsuario;
        idTarjeta = builder.idTarjeta;
        // cvv2
        codigo = builder.codigo;
        // direccion
        direccionUsuario = builder.direccionUsuario;
        ciudadUsuario = builder.ciudadUsuario;
        estadoUsuario = builder.estadoUsuario;
        cpUsuario = builder.cpUsuario;
        monto = builder.monto;
        referencia = builder.referencia;
        nombreServicio = builder.nombreServicio;
        direccionServicio = builder.direccionServicio;
        ciudadServicio = builder.ciudadServicio;
        idServicio = builder.idServicio;
        software = builder.software;
        wkey = builder.wkey;
        imei = builder.imei;
        modelo = builder.modelo;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDireccionUsuario() {
        return direccionUsuario;
    }

    public String getCiudadUsuario() {
        return ciudadUsuario;
    }

    public int getEstadoUsuario() {
        return estadoUsuario;
    }

    public String getCpUsuario() {
        return cpUsuario;
    }

    public double getMonto() {
        return monto;
    }

    public String getReferencia() {
        return referencia;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public String getDireccionServicio() {
        return direccionServicio;
    }

    public String getCiudadServicio() {
        return ciudadServicio;
    }

    public String getIdServicio() {
        return idServicio;
    }

    public String getSoftware() {
        return software;
    }

    public String getWkey() {
        return wkey;
    }

    public String getImei() {
        return imei;
    }

    public String getModelo() {
        return modelo;
    }

    public static class Builder {
        private final String idioma;
        private final long idUsuario;
        private final int idTarjeta;
        private String codigo; // cvv2
        private String direccionUsuario; // direccion
        private String ciudadUsuario;
        private int estadoUsuario;
        private String cpUsuario;
        private double monto;
        private String referencia;

        private String nombreServicio;
        private String direccionServicio;
        private String ciudadServicio;
        private String idServicio;

        private String software;
        private String wkey;
        private String imei;
        private String modelo;

        public Builder(String idioma, long idUsuario, int idTarjeta) {
            this.idioma = idioma;
            this.idUsuario = idUsuario;
            this.idTarjeta = idTarjeta;
        }

        public Builder setCodigo(String codigo) {
            this.codigo = codigo;
            return this;
        }

        public Builder setDireccionUsuario(String direccionUsuario) {
            this.direccionUsuario = direccionUsuario;
            return this;
        }

        public Builder setCiudadUsuario(String ciudadUsuario) {
            this.ciudadUsuario = ciudadUsuario;
            return this;
        }

        public Builder setEstadoUsuario(int estadoUsuario) {
            this.estadoUsuario = estadoUsuario;
            return this;
        }

        public Builder setCpUsuario(String cpUsuario) {
            this.cpUsuario = cpUsuario;
            return this;
        }

        public Builder setMonto(double monto) {
            this.monto = monto;
            return this;
        }

        public Builder setReferencia(String referencia) {
            this.referencia = referencia;
            return this;
        }

        public Builder setNombreServicio(String nombreServicio) {
            this.nombreServicio = nombreServicio;
            return this;
        }

        public Builder setDireccionServicio(String direccionServicio) {
            this.direccionServicio = direccionServicio;
            return this;
        }

        public Builder setCiudadServicio(String ciudadServicio) {
            this.ciudadServicio = ciudadServicio;
            return this;
        }

        public Builder setIdServicio(String idServicio) {
            this.idServicio = idServicio;
            return this;
        }

        public Builder setSoftware(String software) {
            this.software = software;
            return this;
        }

        public Builder setWkey(String wkey) {
            this.wkey = wkey;
            return this;
        }

        public Builder setImei(String imei) {
            this.imei = imei;
            return this;
        }

        public Builder setModelo(String modelo) {
            this.modelo = modelo;
            return this;
        }

        public NoDataPagoRequest build() {
            return new NoDataPagoRequest(this);
        }
    }
}
