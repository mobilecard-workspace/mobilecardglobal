package addcel.mobilecard.data.net.lcpf.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class HthAccountResponse {

    @SerializedName("accounts")
    private List<LcpfEstablecimiento> Accounts;
    @SerializedName("idError")
    private Long IdError;
    @SerializedName("mensajeError")
    private String MensajeError;

    public HthAccountResponse(Long idError, String mensajeError) {
        IdError = idError;
        MensajeError = mensajeError;
    }

    public List<LcpfEstablecimiento> getAccounts() {
        return Accounts;
    }

    public void setAccounts(List<LcpfEstablecimiento> accounts) {
        Accounts = accounts;
    }

    public Long getIdError() {
        return IdError;
    }

    public void setIdError(Long idError) {
        IdError = idError;
    }

    public String getMensajeError() {
        return MensajeError;
    }

    public void setMensajeError(String mensajeError) {
        MensajeError = mensajeError;
    }
}
