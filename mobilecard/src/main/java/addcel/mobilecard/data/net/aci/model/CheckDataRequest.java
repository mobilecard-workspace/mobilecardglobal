package addcel.mobilecard.data.net.aci.model;

import com.google.common.base.MoreObjects;

/**
 * ADDCEL on 29/03/17.
 */
public final class CheckDataRequest {
    private final long idUsuario;
    private final int idTarjeta;

    public CheckDataRequest(long idUsuario, int idTarjeta) {
        this.idUsuario = idUsuario;
        this.idTarjeta = idTarjeta;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("idUsuario", idUsuario)
                .add("idTarjeta", idTarjeta)
                .toString();
    }
}
