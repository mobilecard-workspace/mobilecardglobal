package addcel.mobilecard.data.net.colombia.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * ADDCEL on 10/18/18.
 */
public final class ColombiaServicioConsultaRequestEntity implements Parcelable {

    public static final Creator<ColombiaServicioConsultaRequestEntity> CREATOR =
            new Creator<ColombiaServicioConsultaRequestEntity>() {
                @Override
                public ColombiaServicioConsultaRequestEntity createFromParcel(Parcel in) {
                    return new ColombiaServicioConsultaRequestEntity(in);
                }

                @Override
                public ColombiaServicioConsultaRequestEntity[] newArray(int size) {
                    return new ColombiaServicioConsultaRequestEntity[size];
                }
            };
    /**
     * idAplicacion : 1 idProducto : 1005 idUsuario : 215 referencias : [{"len":15,"nombre":"Cedula o
     * Nit","tipo":"number","valor":"646531684668343","variable":"documento"}]
     */
    private final int idAplicacion;

    private final String idProducto;
    private final long idUsuario;
    private final List<ReferenciasEntity> referencias;

    private ColombiaServicioConsultaRequestEntity(Builder builder) {
        idAplicacion = builder.idAplicacion;
        idProducto = builder.idProducto;
        idUsuario = builder.idUsuario;
        referencias = builder.referencias;
    }

    private ColombiaServicioConsultaRequestEntity(Parcel in) {
        idAplicacion = in.readInt();
        idProducto = in.readString();
        idUsuario = in.readLong();
        referencias = in.createTypedArrayList(ReferenciasEntity.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idAplicacion);
        dest.writeString(idProducto);
        dest.writeLong(idUsuario);
        dest.writeTypedList(referencias);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    private int getIdAplicacion() {
        return idAplicacion;
    }

    private String getIdProducto() {
        return idProducto;
    }

    private long getIdUsuario() {
        return idUsuario;
    }

    private List<ReferenciasEntity> getReferencias() {
        return referencias;
    }

    public static class ReferenciasEntity implements Parcelable {
        public static final Creator<ReferenciasEntity> CREATOR = new Creator<ReferenciasEntity>() {
            @Override
            public ReferenciasEntity createFromParcel(Parcel in) {
                return new ReferenciasEntity(in);
            }

            @Override
            public ReferenciasEntity[] newArray(int size) {
                return new ReferenciasEntity[size];
            }
        };
        /**
         * len : 15 nombre : Cedula o Nit tipo : number valor : 646531684668343 variable : documento
         */
        private final int len;

        private final String nombre;
        private final String tipo;
        private final String valor;
        private final String variable;

        private ReferenciasEntity(Builder builder) {
            len = builder.len;
            nombre = builder.nombre;
            tipo = builder.tipo;
            valor = builder.valor;
            variable = builder.variable;
        }

        ReferenciasEntity(Parcel in) {
            len = in.readInt();
            nombre = in.readString();
            tipo = in.readString();
            valor = in.readString();
            variable = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(len);
            dest.writeString(nombre);
            dest.writeString(tipo);
            dest.writeString(valor);
            dest.writeString(variable);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        int getLen() {
            return len;
        }

        String getNombre() {
            return nombre;
        }

        String getTipo() {
            return tipo;
        }

        String getValor() {
            return valor;
        }

        String getVariable() {
            return variable;
        }

        public static final class Builder {
            private int len;
            private String nombre;
            private String tipo;
            private String valor;
            private String variable;

            public Builder() {
            }

            public Builder(ReferenciasEntity copy) {
                this.len = copy.getLen();
                this.nombre = copy.getNombre();
                this.tipo = copy.getTipo();
                this.valor = copy.getValor();
                this.variable = copy.getVariable();
            }

            public Builder setLen(int len) {
                this.len = len;
                return this;
            }

            public Builder setNombre(String nombre) {
                this.nombre = nombre;
                return this;
            }

            public Builder setTipo(String tipo) {
                this.tipo = tipo;
                return this;
            }

            public Builder setValor(String valor) {
                this.valor = valor;
                return this;
            }

            public Builder setVariable(String variable) {
                this.variable = variable;
                return this;
            }

            public ReferenciasEntity build() {
                return new ReferenciasEntity(this);
            }
        }
    }

    public static final class Builder {
        private int idAplicacion;
        private String idProducto;
        private long idUsuario;
        private List<ReferenciasEntity> referencias;

        public Builder() {
        }

        public Builder(ColombiaServicioConsultaRequestEntity copy) {
            this.idAplicacion = copy.getIdAplicacion();
            this.idProducto = copy.getIdProducto();
            this.idUsuario = copy.getIdUsuario();
            this.referencias = copy.getReferencias();
        }

        public Builder setIdAplicacion(int idAplicacion) {
            this.idAplicacion = idAplicacion;
            return this;
        }

        public Builder setIdProducto(String idProducto) {
            this.idProducto = idProducto;
            return this;
        }

        public Builder setIdUsuario(long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public Builder setReferencias(List<ReferenciasEntity> referencias) {
            this.referencias = referencias;
            return this;
        }

        public ColombiaServicioConsultaRequestEntity build() {
            return new ColombiaServicioConsultaRequestEntity(this);
        }
    }
}
