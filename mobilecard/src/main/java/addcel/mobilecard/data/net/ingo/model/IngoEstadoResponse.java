package addcel.mobilecard.data.net.ingo.model;

import com.google.gson.annotations.Expose;

import org.jetbrains.annotations.NotNull;

import java.util.List;

@SuppressWarnings("unused")
public class IngoEstadoResponse {

    @Expose
    private List<EstadoEntity> estados;
    @Expose
    private Long idError;
    @Expose
    private String mensajeError;

    public List<EstadoEntity> getEstados() {
        return estados;
    }

    public Long getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public static class Builder {

        private List<EstadoEntity> estados;
        private Long idError;
        private String mensajeError;

        public IngoEstadoResponse.Builder withEstados(List<EstadoEntity> estados) {
            this.estados = estados;
            return this;
        }

        public IngoEstadoResponse.Builder withIdError(Long idError) {
            this.idError = idError;
            return this;
        }

        public IngoEstadoResponse.Builder withMensajeError(String mensajeError) {
            this.mensajeError = mensajeError;
            return this;
        }

        public IngoEstadoResponse build() {
            IngoEstadoResponse ingoEstadoResponse = new IngoEstadoResponse();
            ingoEstadoResponse.estados = estados;
            ingoEstadoResponse.idError = idError;
            ingoEstadoResponse.mensajeError = mensajeError;
            return ingoEstadoResponse;
        }
    }

    @SuppressWarnings("unused")
    public static class EstadoEntity {

        @Expose
        private String abreviatura;
        @Expose
        private String id;
        @Expose
        private String nombre;
        @Expose
        private Long pais;

        public String getAbreviatura() {
            return abreviatura;
        }

        public String getId() {
            return id;
        }

        public String getNombre() {
            return nombre;
        }

        public Long getPais() {
            return pais;
        }

        @NotNull
        @Override
        public String toString() {
            return nombre;
        }

        public static class Builder {

            private String abreviatura;
            private String id;
            private String nombre;
            private Long pais;

            public Builder withAbreviatura(String abreviatura) {
                this.abreviatura = abreviatura;
                return this;
            }

            public Builder withId(String id) {
                this.id = id;
                return this;
            }

            public Builder withNombre(String nombre) {
                this.nombre = nombre;
                return this;
            }

            public Builder withPais(Long pais) {
                this.pais = pais;
                return this;
            }

            public EstadoEntity build() {
                EstadoEntity estado = new EstadoEntity();
                estado.abreviatura = abreviatura;
                estado.id = id;
                estado.nombre = nombre;
                estado.pais = pais;
                return estado;
            }
        }
    }
}
