package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 05/04/17.
 */

public final class CurrencyResponse extends McResponse {
    @SerializedName("currency")
    private List<CurrencyModel> currencies;

    public CurrencyResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public List<CurrencyModel> getCurrencies() {
        return currencies;
    }
}
