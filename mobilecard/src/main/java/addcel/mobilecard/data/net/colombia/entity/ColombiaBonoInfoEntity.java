package addcel.mobilecard.data.net.colombia.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * ADDCEL on 10/17/18.
 */
public final class ColombiaBonoInfoEntity implements Parcelable {
    public static final Creator<ColombiaBonoInfoEntity> CREATOR =
            new Creator<ColombiaBonoInfoEntity>() {
                @Override
                public ColombiaBonoInfoEntity createFromParcel(Parcel in) {
                    return new ColombiaBonoInfoEntity(in);
                }

                @Override
                public ColombiaBonoInfoEntity[] newArray(int size) {
                    return new ColombiaBonoInfoEntity[size];
                }
            };
    /**
     * value : 1 label : CREPES&WAFFLES montos :
     * [{"idRecarga":"1","idProducto":"1018","monto":"10000.0","description":"Bono Regalo
     * Crepes&waffles
     * 10000","comision":"0.00"},{"idRecarga":"2","idProducto":"1018","monto":"20000.0","description":"Bono
     * Regalo Crepes&waffles
     * 20000","comision":"0.00"},{"idRecarga":"3","idProducto":"1018","monto":"50000.0","description":"Bono
     * Regalo Crepes&waffles
     * 50000","comision":"0.00"},{"idRecarga":"4","idProducto":"1018","monto":"100000.0","description":"Bono
     * Regalo Crepes&waffles 100000","comision":"0.00"}]
     */
    private String value;

    private String label;
    private List<MontoEntity> montos;

    private ColombiaBonoInfoEntity(Parcel in) {
        value = in.readString();
        label = in.readString();
        montos = in.createTypedArrayList(MontoEntity.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
        dest.writeString(label);
        dest.writeTypedList(montos);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<MontoEntity> getMontos() {
        return montos;
    }

    public void setMontos(List<MontoEntity> montos) {
        this.montos = montos;
    }

    public static class MontoEntity implements Parcelable {
        public static final Creator<MontoEntity> CREATOR = new Creator<MontoEntity>() {
            @Override
            public MontoEntity createFromParcel(Parcel in) {
                return new MontoEntity(in);
            }

            @Override
            public MontoEntity[] newArray(int size) {
                return new MontoEntity[size];
            }
        };
        /**
         * idRecarga : 1 idProducto : 1018 monto : 10000.0 description : Bono Regalo Crepes&waffles
         * 10000 comision : 0.00
         */
        private String idRecarga;

        private String idProducto;
        private String monto;
        private String description;
        private String comision;

        MontoEntity(Parcel in) {
            idRecarga = in.readString();
            idProducto = in.readString();
            monto = in.readString();
            description = in.readString();
            comision = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(idRecarga);
            dest.writeString(idProducto);
            dest.writeString(monto);
            dest.writeString(description);
            dest.writeString(comision);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getIdRecarga() {
            return idRecarga;
        }

        public void setIdRecarga(String idRecarga) {
            this.idRecarga = idRecarga;
        }

        public String getIdProducto() {
            return idProducto;
        }

        public void setIdProducto(String idProducto) {
            this.idProducto = idProducto;
        }

        public String getMonto() {
            return monto;
        }

        public void setMonto(String monto) {
            this.monto = monto;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getComision() {
            return comision;
        }

        public void setComision(String comision) {
            this.comision = comision;
        }
    }
}
