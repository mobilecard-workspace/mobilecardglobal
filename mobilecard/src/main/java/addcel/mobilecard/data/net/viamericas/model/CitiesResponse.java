package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 05/04/17.
 */

public class CitiesResponse extends McResponse {
    //@SerializedName("stateCities")
    @SerializedName("city")
    private List<VmCityModel> cities;

    public CitiesResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public List<VmCityModel> getCities() {
        return cities;
    }
}
