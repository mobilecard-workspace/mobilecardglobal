package addcel.mobilecard.data.net.previvale.entity;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 11/14/18.
 */
public final class PrActivateResponseEntity {

    /**
     * code : 1000 message : Se ha registrado correctamente el usuario y la tarjeta data :
     * {"authorization":"SUCCESS","previvaleCode":0}
     */
    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private DataEntity data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        /**
         * authorization : SUCCESS previvaleCode : 0
         */
        @SerializedName("authorization")
        private String authorization;

        @SerializedName("previvaleCode")
        private int previvaleCode;

        public String getAuthorization() {
            return authorization;
        }

        public void setAuthorization(String authorization) {
            this.authorization = authorization;
        }

        public int getPrevivaleCode() {
            return previvaleCode;
        }

        public void setPrevivaleCode(int previvaleCode) {
            this.previvaleCode = previvaleCode;
        }
    }
}
