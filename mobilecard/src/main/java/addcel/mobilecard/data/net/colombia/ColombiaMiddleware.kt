package addcel.mobilecard.data.net.colombia

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import okhttp3.Credentials
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 9/24/19.
 */

data class RecargaRequest(
        @SerializedName("codigoProveedor") val codigoProveedor: String,
        @SerializedName("concepto") val concepto: String, @SerializedName("idApp") val idApp: Int,
        @SerializedName("idPais") val idPais: Int, @SerializedName("idTarjeta") val idTarjeta: Int,
        @SerializedName("idUsuario") val idUsuario: Long, @SerializedName("idioma") val idioma: String,
        @SerializedName("imei") val imei: String, @SerializedName("monto") val monto: Double,
        @SerializedName("numeroCelular") val numeroCelular: String
)

data class PaqueteRequest(
        @SerializedName("concepto") val concepto: String, @SerializedName("idApp")
        val idApp: Int, @SerializedName("idPais") val idPais: Int, @SerializedName("idPaquete")
        val idPaquete: String, @SerializedName("idTarjeta") val idTarjeta: Int, @SerializedName("idUsuario")
        val idUsuario: Long, @SerializedName("idioma") val idioma: String, @SerializedName("imei")
        val imei: String, @SerializedName("numeroCelular") val numeroCelular: String
)

data class PuntoRedResponse(
        @SerializedName("codigo") val codigo: Int, @SerializedName("mensaje")
        val mensaje: String
)

//VIARAPIDA
data class VrTagRequest(
        @SerializedName("alias") val alias: String, @SerializedName("idApp")
        val idApp: Int, @SerializedName("idPais") val idPais: Int, @SerializedName("idUsuario")
        val idUsuario: Long, @SerializedName("idioma") val idioma: String, @SerializedName("placa")
        val placa: String, @SerializedName("tagId") val tagId: String, @SerializedName("idTagBd")
        val idTagBd: Int = 0
)

data class VrTagResponse(
        @SerializedName("codigo") val codigo: Int, @SerializedName("mensaje")
        val mensaje: String, @SerializedName("tag") val tag: VrTag
)

data class VrTagListResponse(
        @SerializedName("codigo") val codigo: Int, @SerializedName("mensaje")
        val mensaje: String, @SerializedName("tags") val tags: List<VrTag>
)

@Parcelize
data class VrTag(
        @SerializedName("alias") val alias: String, @SerializedName("cuenta")
        val cuenta: String, @SerializedName("idViaTag") val idViaTag: Int, @SerializedName("placa")
        val placa: String, @SerializedName("tagId") val tagId: String
) : Parcelable {
    override fun toString(): String {
        return "$alias - $placa"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VrTag

        if (alias != other.alias) return false
        if (cuenta != other.cuenta) return false
        if (idViaTag != other.idViaTag) return false
        if (placa != other.placa) return false
        if (tagId != other.tagId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = alias.hashCode()
        result = 31 * result + cuenta.hashCode()
        result = 31 * result + idViaTag
        result = 31 * result + placa.hashCode()
        result = 31 * result + tagId.hashCode()
        return result
    }
}

@Parcelize
data class VrTagDetalle(
        @SerializedName("codigo") val codigo: Int,
        @SerializedName("cuenta") val cuenta: String, @SerializedName("mensaje") val mensaje: String,
        @SerializedName("montos") val montos: List<Double>, @SerializedName("placa") val placa: String,
        @SerializedName("saldo") val saldo: Double
) : Parcelable

data class VrRecargaRequest(
        @SerializedName("idApp") val idApp: Int, @SerializedName("idPais")
        val idPais: Int, @SerializedName("idTarjeta") val idTarjeta: Int, @SerializedName("idUsuario")
        val idUsuario: Long, @SerializedName("idioma") val idioma: String, @SerializedName("imei")
        val imei: String, @SerializedName("monto") val monto: Double, @SerializedName("placa")
        val placa: String, @SerializedName("tagId") val tagId: String
)

data class VrResponse(
        @SerializedName("codigo") val codigo: Int, @SerializedName("mensaje")
        val mensaje: String
)

data class SoatConsultaRequest(
        @SerializedName("idApp") val idApp: Int, @SerializedName("idPais")
        val idPais: Int, @SerializedName("idUsuario") val idUsuario: Long, @SerializedName("idioma")
        val idioma: String, @SerializedName("placa") val placa: String
)

@Parcelize
data class SoatConsultaResponse(
        @SerializedName("categoria") val categoria: String?,
        @SerializedName("cc") val cc: String?,
        @SerializedName("codigo") val codigo: Int,
        @SerializedName("email") val email: String?,
        @SerializedName("linea") val linea: String?,
        @SerializedName("marca") val marca: String?,
        @SerializedName("mensaje") val mensaje: String?,
        @SerializedName("modelo") val modelo: String?,
        @SerializedName("nombre") val nombre: String?,
        @SerializedName("placa") val placa: String?,
        @SerializedName("seguroSoat") val seguroSoat: Double,
        @SerializedName("telefono") val telefono: String?,
        @SerializedName("total") val total: Double,
        @SerializedName("vigenciaFin") val vigenciaFin: String?,
        @SerializedName("vigenciaIni") val vigenciaIni: String?,
        @SerializedName("idTarjeta") val idCard: Int = 0,
        @SerializedName("idUsuario") val idUSuario: Long = 0L,
        @SerializedName("idApp") val idApp: Int = 1,
        @SerializedName("idPais") val idPais: Int = 1,
        @SerializedName("idioma") val idioma: String = "es"
) : Parcelable

data class TerminosEntity(
        @SerializedName("idError") val mIdError: Int = 0,
        @SerializedName("mensajeError") val mMensajeError: String, @SerializedName("termino")
        val mTerminos: String = ""
)

interface ColombiaMiddleware {

    companion object {

        private const val user = "mobilecardmx"
        private const val pass = "82007eb4238205c75ebcdefc06a01311"

        fun getAuth(): String {
            return Credentials.basic(user, pass)
        }

        fun get(r: Retrofit): ColombiaMiddleware {
            return r.create(ColombiaMiddleware::class.java)
        }
    }

    //RECARGAS
    @POST("ColombiaMiddleware/puntored/api/recarga")
    fun recarga(
            @Header("Authorization")
            auth: String, @Body request: RecargaRequest
    ): Observable<PuntoRedResponse>

    @POST("ColombiaMiddleware/puntored/api/paquete")
    fun paquete(
            @Header("Authorization")
            auth: String, @Body request: PaqueteRequest
    ): Observable<PuntoRedResponse>

    //VIARAPIDA
    @POST("ColombiaMiddleware/viarapida/api/saveTag")
    fun saveTag(
            @Header("Authorization")
            auth: String, @Body request: VrTagRequest
    ): Observable<VrTagResponse>

    @HTTP(method = "DELETE", path = "ColombiaMiddleware/viarapida/api/deleteTag/", hasBody = true)
    fun deleteTag(
            @Header("Authorization") auth: String, @Body
            request: VrTagRequest
    ): Observable<VrTagListResponse>

    @GET("ColombiaMiddleware/{idApp}/{idPais}/{idioma}/viarapida/api/listTags/{idUsuario}")
    fun listTags(
            @Header("Authorization") auth: String, @Path("idApp") idApp: Int, @Path("idPais")
            idPais: Int, @Path("idioma") idioma: String, @Path("idUsuario")
            idUsuario: Long
    ): Observable<VrTagListResponse>

    /*
    ColombiaMiddleware/1/2/es/viarapida/api/detalleTag/567267/4847845
     */
    @GET("ColombiaMiddleware/{idApp}/{idPais}/{idioma}/viarapida/api/detalleTag/{idTag}/{idUsuario}")
    fun detalleTag(
            @Header("Authorization") auth: String, @Path("idApp") idApp: Int, @Path("idPais")
            idPais: Int, @Path("idioma") idioma: String, @Path("idTag") idTag: Int, @Path("idUsuario")
            idUsuario: Long
    ): Observable<VrTagDetalle>

    @POST("ColombiaMiddleware/viarapida/api/recarga")
    fun recargaTag(
            @Header("Authorization")
            auth: String, @Body request: VrRecargaRequest
    ): Observable<VrResponse>

    @POST("ColombiaMiddleware/api/soat/calculaPoliza")
    fun consultaPoliza(
            @Header("Authorization")
            auth: String, @Body request: SoatConsultaRequest
    ): Observable<SoatConsultaResponse>

    @POST("ColombiaMiddleware/api/soat/expedirPoliza")
    fun expedirPoliza(
            @Header("Authorization")
            auth: String, @Body request: SoatConsultaResponse
    ): Observable<VrResponse>

    @GET("Usuarios/{idApp}/{tipoTerm}/{idPais}/{idioma}")
    fun getTerminos(
            @Path("idApp") idApp: Int,
            @Header("client") client: String, @Path("tipoTerm") tipoTerm: String, @Path("idPais")
            idPais: Int, @Path("idioma") idioma: String
    ): Observable<TerminosEntity>
}