package addcel.mobilecard.data.net.usuarios.model

import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.HTTP
import retrofit2.http.Header
import retrofit2.http.POST


/**
 * ADDCEL on 18/10/19.
 */

data class PushTokenRequest(
        @SerializedName("idApp")
        val idApp: Int,
        @SerializedName("idPais")
        val idPais: Int,
        @SerializedName("id_usuario")
        val idUsuario: Long,
        @SerializedName("idioma")
        val idioma: String,
        @SerializedName("tipoUsuario")
        val tipoUsuario: String,
        @SerializedName("token")
        val token: String
)

data class RemovePushTokenRequest(
        @SerializedName("idUsuario")
        val idUsuario: Long,
        @SerializedName("tipoUsuario")
        val tipoUsuario: String,
        @SerializedName("token")
        val token: String
)

data class PushResponse(val code: Int, val message: String, val idToken: Int, val currentDate: Long)


interface PushApi {

    companion object {
        const val PUSH_AUTH = "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"
        fun get(r: Retrofit): PushApi {
            return r.create(PushApi::class.java)
        }
    }

    @POST("PushNotifications/saveToken")
    fun saveToken(@Header("authorization") authorization: String, @Body request: PushTokenRequest): Observable<PushResponse>

    @HTTP(method = "DELETE", path = "PushNotifications/removeToken", hasBody = true)
    fun deleteToken(@Header("authorization") authorization: String, @Body request: RemovePushTokenRequest): Observable<PushResponse>
}