package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 10/04/17.
 */

public final class VmZipResponse extends McResponse {
    @SerializedName("stateCities")
    private List<VmZipModel> zipCodeList;

    public VmZipResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public List<VmZipModel> getZipCodeList() {
        return zipCodeList;
    }
}
