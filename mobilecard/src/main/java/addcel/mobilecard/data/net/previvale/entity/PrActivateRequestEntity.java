package addcel.mobilecard.data.net.previvale.entity;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 11/14/18.
 */
public final class PrActivateRequestEntity {

    /**
     * idUsuario : 1319500901163 tarjeta : 5064500104030953
     */
    @SerializedName("idUsuario")
    private final long idUsuario;

    @SerializedName("tarjeta")
    private final String tarjeta;

    private PrActivateRequestEntity(Builder builder) {
        idUsuario = builder.idUsuario;
        tarjeta = builder.tarjeta;
    }

    private long getIdUsuario() {
        return idUsuario;
    }

    private String getTarjeta() {
        return tarjeta;
    }

    public static final class Builder {
        private long idUsuario;
        private String tarjeta;

        public Builder() {
        }

        public Builder(PrActivateRequestEntity copy) {
            this.idUsuario = copy.getIdUsuario();
            this.tarjeta = copy.getTarjeta();
        }

        public Builder setIdUsuario(long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public Builder setTarjeta(String tarjeta) {
            this.tarjeta = tarjeta;
            return this;
        }

        public PrActivateRequestEntity build() {
            return new PrActivateRequestEntity(this);
        }
    }
}
