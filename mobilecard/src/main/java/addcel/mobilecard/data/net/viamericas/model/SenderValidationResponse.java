package addcel.mobilecard.data.net.viamericas.model;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 27/03/17.
 */

public final class SenderValidationResponse extends McResponse {
    private long idUsuario;
    private String idSender;

    public SenderValidationResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public String getIdSender() {
        return idSender;
    }
}
