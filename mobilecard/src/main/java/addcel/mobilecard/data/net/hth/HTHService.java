package addcel.mobilecard.data.net.hth;

import addcel.mobilecard.data.net.hth.model.HthAccountRequest;
import addcel.mobilecard.data.net.hth.model.HthAccountResponse;
import addcel.mobilecard.data.net.hth.model.HthAccountUpdateRequest;
import addcel.mobilecard.data.net.hth.model.HthBankResponse;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * ADDCEL on 08/11/17.
 */
public interface HTHService {

    String ENQUEUE_PAYMENT = "H2HPayment/{idApp}/payworks/enqueuePayment";
    String PROCESS_SECURED = "/payworks/payworksRec3DRespuesta";
    String PROCESS_FINISHED = "/payworks/payworks2RecRespuesta";

    @FormUrlEncoded
    @POST("H2HPayment/{idApp}/toAccount")
    Observable<HthAccountResponse> toAccount(
            @Path("idApp") int idApp, @Field("idUsuario") long idUsuario, @Field("idioma") String idioma);

    @FormUrlEncoded
    @POST("H2HPayment/{idApp}/deleteAccount")
    Observable<HthAccountResponse> deleteAccount(@Path("idApp") int idApp,
                                                 @Field("idUsuario") long idUsuario, @Field("idioma") String idioma,
                                                 @Field("idAccount") int id);

    @POST("H2HPayment/{idApp}/updateAccount")
    Observable<HthAccountResponse> updateAccount(
            @Path("idApp") int idApp, @Body HthAccountUpdateRequest request);

    @POST("H2HPayment/{idApp}/enqueueAccountSignUp")
    Observable<HthAccountResponse> enqueueAccountSignUp(@Path("idApp") int idApp,
                                                        @Body HthAccountRequest request);

    @GET("H2HPayment/{idApp}/bankCodes")
    Observable<HthBankResponse> bankCodes(
            @Path("idApp") int idApp);
}
