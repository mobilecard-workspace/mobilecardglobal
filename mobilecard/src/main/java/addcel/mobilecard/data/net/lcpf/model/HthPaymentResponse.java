package addcel.mobilecard.data.net.lcpf.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.jetbrains.annotations.NotNull;

/**
 * ADDCEL on 09/11/17.
 */

public final class HthPaymentResponse implements Parcelable {
    private int idError;
    private String mensajeError;
    private long idBitacora;
    private String referenciaNeg;
    private double montoTransfer;
    private double comision;
    private String authProcom;
    private String refProcom;
    private String referenciaBanorte;
    private String tarjeta;
    private String fecha;

    public HthPaymentResponse() {
    }

    public HthPaymentResponse(int idError, String mensajeError) {
        this.idError = idError;
        this.mensajeError = mensajeError;
    }

    protected HthPaymentResponse(Parcel in) {
        idError = in.readInt();
        mensajeError = in.readString();
        idBitacora = in.readLong();
        referenciaNeg = in.readString();
        montoTransfer = in.readDouble();
        comision = in.readDouble();
        authProcom = in.readString();
        refProcom = in.readString();
        referenciaBanorte = in.readString();
        tarjeta = in.readString();
        fecha = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idError);
        dest.writeString(mensajeError);
        dest.writeLong(idBitacora);
        dest.writeString(referenciaNeg);
        dest.writeDouble(montoTransfer);
        dest.writeDouble(comision);
        dest.writeString(authProcom);
        dest.writeString(refProcom);
        dest.writeString(referenciaBanorte);
        dest.writeString(tarjeta);
        dest.writeString(fecha);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HthPaymentResponse> CREATOR = new Creator<HthPaymentResponse>() {
        @Override
        public HthPaymentResponse createFromParcel(Parcel in) {
            return new HthPaymentResponse(in);
        }

        @Override
        public HthPaymentResponse[] newArray(int size) {
            return new HthPaymentResponse[size];
        }
    };

    public int getIdError() {
        return idError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public String getReferenciaNeg() {
        return referenciaNeg;
    }

    public long getIdBitacora() {
        return idBitacora;
    }

    public double getMontoTransfer() {
        return montoTransfer;
    }

    public double getComision() {
        return comision;
    }

    public String getAuthProcom() {
        return authProcom;
    }

    public String getRefProcom() {
        return refProcom;
    }

    public String getReferenciaBanorte() {
        return referenciaBanorte;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public String getFecha() {
        return fecha;
    }

    @NotNull
    @Override
    public String toString() {
        return mensajeError;
    }
}
