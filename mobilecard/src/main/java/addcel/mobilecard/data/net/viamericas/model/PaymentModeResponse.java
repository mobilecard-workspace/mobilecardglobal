package addcel.mobilecard.data.net.viamericas.model;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

/**
 * ADDCEL on 05/04/17.
 */

public final class PaymentModeResponse extends McResponse {
    private List<PaymentModeModel> paymentModes;

    public PaymentModeResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public List<PaymentModeModel> getPaymentModes() {
        return paymentModes;
    }
}
