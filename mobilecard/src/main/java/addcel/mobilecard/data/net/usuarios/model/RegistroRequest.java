package addcel.mobilecard.data.net.usuarios.model;

import addcel.mobilecard.data.net.wallet.CardRequest;

/**
 * ADDCEL on 14/07/17.
 */
public final class RegistroRequest {
    private final String email;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final String phone;
    private final String gender;
    private final int country;
    private final CardRequest card;
    private final String imei;
    private final String platform;
    private final String os;
    private final String manufacturer;
    private final String cedula;
    private final int tipoCedula;

    private RegistroRequest(Builder builder) {
        email = builder.email;
        password = builder.password;
        firstName = builder.firstName;
        lastName = builder.lastName;
        phone = builder.phone;
        gender = builder.gender;
        country = builder.country;
        card = builder.card;
        imei = builder.imei;
        platform = builder.platform;
        os = builder.os;
        manufacturer = builder.manufacturer;
        cedula = builder.cedula;
        tipoCedula = builder.tipoCedula;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getGender() {
        return gender;
    }

    public int getCountry() {
        return country;
    }

    public CardRequest getCard() {
        return card;
    }

    public String getImei() {
        return imei;
    }

    public String getPlatform() {
        return platform;
    }

    public String getOs() {
        return os;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getCedula() {
        return cedula;
    }

    public int getTipoCedula() {
        return tipoCedula;
    }

    public static class Builder {
        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private String phone;
        private String gender;
        private int country;
        private CardRequest card;
        private String imei;
        private String platform;
        private String os;
        private String manufacturer;
        private String cedula;
        private int tipoCedula;

        public Builder() {
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setGender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder setCountry(int country) {
            this.country = country;
            return this;
        }

        public Builder setCard(CardRequest card) {
            this.card = card;
            return this;
        }

        public Builder setImei(String imei) {
            this.imei = imei;
            return this;
        }

        public Builder setPlatform() {
            this.platform = "ANDROID";
            return this;
        }

        public Builder setOs(String os) {
            this.os = os;
            return this;
        }

        public Builder setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }

        public Builder setCedula(String cedula) {
            this.cedula = cedula;
            return this;
        }

        public Builder setTipoCedula(int tipoCedula) {
            this.tipoCedula = tipoCedula;
            return this;
        }

        public RegistroRequest build() {
            return new RegistroRequest(this);
        }
    }
}
