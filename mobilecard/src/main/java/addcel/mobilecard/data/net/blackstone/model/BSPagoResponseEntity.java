package addcel.mobilecard.data.net.blackstone.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class BSPagoResponseEntity implements Parcelable {

    public static final Creator<BSPagoResponseEntity> CREATOR = new Creator<BSPagoResponseEntity>() {
        @Override
        public BSPagoResponseEntity createFromParcel(Parcel in) {
            return new BSPagoResponseEntity(in);
        }

        @Override
        public BSPagoResponseEntity[] newArray(int size) {
            return new BSPagoResponseEntity[size];
        }
    };
    @Expose
    private String authorizationCode;
    @Expose
    private double balance;
    @Expose
    private String carrierName;
    @Expose
    private double comision;
    @Expose
    private String concepto;
    @Expose
    private String errorCode;
    @Expose
    private String errorMessage;
    @Expose
    private String fecha;
    @Expose
    private String loginOrMail;
    @Expose
    private String maskCard;
    @Expose
    private double monto;
    @Expose
    private String nombre;
    @Expose
    private String referenceNumber;
    @Expose
    private String toppedUpNumber;
    @Expose
    private double total;
    @Expose
    private String transactionID;

    public BSPagoResponseEntity() {
    }

    protected BSPagoResponseEntity(Parcel in) {
        authorizationCode = in.readString();
        balance = in.readDouble();
        carrierName = in.readString();
        comision = in.readDouble();
        concepto = in.readString();
        errorCode = in.readString();
        errorMessage = in.readString();
        fecha = in.readString();
        loginOrMail = in.readString();
        maskCard = in.readString();
        monto = in.readDouble();
        nombre = in.readString();
        referenceNumber = in.readString();
        toppedUpNumber = in.readString();
        total = in.readDouble();
        transactionID = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(authorizationCode);
        dest.writeDouble(balance);
        dest.writeString(carrierName);
        dest.writeDouble(comision);
        dest.writeString(concepto);
        dest.writeString(errorCode);
        dest.writeString(errorMessage);
        dest.writeString(fecha);
        dest.writeString(loginOrMail);
        dest.writeString(maskCard);
        dest.writeDouble(monto);
        dest.writeString(nombre);
        dest.writeString(referenceNumber);
        dest.writeString(toppedUpNumber);
        dest.writeDouble(total);
        dest.writeString(transactionID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public double getBalance() {
        return balance;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public double getComision() {
        return comision;
    }

    public String getConcepto() {
        return concepto;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getFecha() {
        return fecha;
    }

    public String getLoginOrMail() {
        return loginOrMail;
    }

    public String getMaskCard() {
        return maskCard;
    }

    public double getMonto() {
        return monto;
    }

    public String getNombre() {
        return nombre;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getToppedUpNumber() {
        return toppedUpNumber;
    }

    public double getTotal() {
        return total;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public static class Builder {

        private String authorizationCode;
        private double balance;
        private String carrierName;
        private double comision;
        private String concepto;
        private String errorCode;
        private String errorMessage;
        private String fecha;
        private String loginOrMail;
        private String maskCard;
        private double monto;
        private String nombre;
        private String referenceNumber;
        private String toppedUpNumber;
        private double total;
        private String transactionID;

        public BSPagoResponseEntity.Builder withAuthorizationCode(String authorizationCode) {
            this.authorizationCode = authorizationCode;
            return this;
        }

        public BSPagoResponseEntity.Builder withBalance(double balance) {
            this.balance = balance;
            return this;
        }

        public BSPagoResponseEntity.Builder withCarrierName(String carrierName) {
            this.carrierName = carrierName;
            return this;
        }

        public BSPagoResponseEntity.Builder withComision(double comision) {
            this.comision = comision;
            return this;
        }

        public BSPagoResponseEntity.Builder withConcepto(String concepto) {
            this.concepto = concepto;
            return this;
        }

        public BSPagoResponseEntity.Builder withErrorCode(String errorCode) {
            this.errorCode = errorCode;
            return this;
        }

        public BSPagoResponseEntity.Builder withErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public BSPagoResponseEntity.Builder withFecha(String fecha) {
            this.fecha = fecha;
            return this;
        }

        public BSPagoResponseEntity.Builder withLoginOrMail(String loginOrMail) {
            this.loginOrMail = loginOrMail;
            return this;
        }

        public BSPagoResponseEntity.Builder withMaskCard(String maskCard) {
            this.maskCard = maskCard;
            return this;
        }

        public BSPagoResponseEntity.Builder withMonto(double monto) {
            this.monto = monto;
            return this;
        }

        public BSPagoResponseEntity.Builder withNombre(String nombre) {
            this.nombre = nombre;
            return this;
        }

        public BSPagoResponseEntity.Builder withReferenceNumber(String referenceNumber) {
            this.referenceNumber = referenceNumber;
            return this;
        }

        public BSPagoResponseEntity.Builder withToppedUpNumber(String toppedUpNumber) {
            this.toppedUpNumber = toppedUpNumber;
            return this;
        }

        public BSPagoResponseEntity.Builder withTotal(double total) {
            this.total = total;
            return this;
        }

        public BSPagoResponseEntity.Builder withTransactionID(String transactionID) {
            this.transactionID = transactionID;
            return this;
        }

        public BSPagoResponseEntity build() {
            BSPagoResponseEntity bSPagoResponseEntity = new BSPagoResponseEntity();
            bSPagoResponseEntity.authorizationCode = authorizationCode;
            bSPagoResponseEntity.balance = balance;
            bSPagoResponseEntity.carrierName = carrierName;
            bSPagoResponseEntity.comision = comision;
            bSPagoResponseEntity.concepto = concepto;
            bSPagoResponseEntity.errorCode = errorCode;
            bSPagoResponseEntity.errorMessage = errorMessage;
            bSPagoResponseEntity.fecha = fecha;
            bSPagoResponseEntity.loginOrMail = loginOrMail;
            bSPagoResponseEntity.maskCard = maskCard;
            bSPagoResponseEntity.monto = monto;
            bSPagoResponseEntity.nombre = nombre;
            bSPagoResponseEntity.referenceNumber = referenceNumber;
            bSPagoResponseEntity.toppedUpNumber = toppedUpNumber;
            bSPagoResponseEntity.total = total;
            bSPagoResponseEntity.transactionID = transactionID;
            return bSPagoResponseEntity;
        }
    }
}
