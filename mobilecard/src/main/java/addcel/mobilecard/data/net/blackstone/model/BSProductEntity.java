package addcel.mobilecard.data.net.blackstone.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class BSProductEntity implements Parcelable {

    public static final Creator<BSProductEntity> CREATOR = new Creator<BSProductEntity>() {
        @Override
        public BSProductEntity createFromParcel(Parcel in) {
            return new BSProductEntity(in);
        }

        @Override
        public BSProductEntity[] newArray(int size) {
            return new BSProductEntity[size];
        }
    };
    @Expose
    private String carrierName;
    @Expose
    private String code;
    @Expose
    private Double maxDenominations;
    @Expose
    private Double minDenominations;
    @Expose
    private String name;
    @Expose
    private String type;

    BSProductEntity() {
    }

    private BSProductEntity(Parcel in) {
        carrierName = in.readString();
        code = in.readString();
        if (in.readByte() == 0) {
            maxDenominations = null;
        } else {
            maxDenominations = in.readDouble();
        }
        if (in.readByte() == 0) {
            minDenominations = null;
        } else {
            minDenominations = in.readDouble();
        }
        name = in.readString();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(carrierName);
        dest.writeString(code);
        if (maxDenominations == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(maxDenominations);
        }
        if (minDenominations == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(minDenominations);
        }
        dest.writeString(name);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public String getCode() {
        return code;
    }

    public Double getMaxDenominations() {
        return maxDenominations;
    }

    public Double getMinDenominations() {
        return minDenominations;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public static class Builder {

        private String carrierName;
        private String code;
        private Double maxDenominations;
        private Double minDenominations;
        private String name;
        private String type;

        public BSProductEntity.Builder withCarrierName(String carrierName) {
            this.carrierName = carrierName;
            return this;
        }

        public BSProductEntity.Builder withCode(String code) {
            this.code = code;
            return this;
        }

        public BSProductEntity.Builder withMaxDenominations(Double maxDenominations) {
            this.maxDenominations = maxDenominations;
            return this;
        }

        public BSProductEntity.Builder withMinDenominations(Double minDenominations) {
            this.minDenominations = minDenominations;
            return this;
        }

        public BSProductEntity.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public BSProductEntity.Builder withType(String type) {
            this.type = type;
            return this;
        }

        public BSProductEntity build() {
            BSProductEntity bSProductEntity = new BSProductEntity();
            bSProductEntity.carrierName = carrierName;
            bSProductEntity.code = code;
            bSProductEntity.maxDenominations = maxDenominations;
            bSProductEntity.minDenominations = minDenominations;
            bSProductEntity.name = name;
            bSProductEntity.type = type;
            return bSProductEntity;
        }
    }
}
