package addcel.mobilecard.data.net.apto.user

import addcel.mobilecard.BuildConfig
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * ADDCEL on 2019-07-31.
 */

data class UserVerification(@SerializedName("verification_id") val verificationId: String)

data class UserDataPoints(val type: String, val data: List<Map<String, Any>>)

data class UserInsertRequest(@SerializedName("data_points") val dataPoints: UserDataPoints)

data class UserInsertResponse(
        @SerializedName("type") val type: String, @SerializedName("user_id")
        val userId: String, @SerializedName("user_token") val userToken: String
)

interface UserAPI {

    companion object {

        private const val VERSION = "v1"

        const val DATA_TYPE_PHONE = "phone"
        const val DATA_TYPE_EMAIL = "email"
        const val DATA_TYPE_NAME = "name"
        const val DATA_TYPE_ADDRESS = "address"
        const val DATA_TYPE_APT = "apt"
        const val DATA_TYPE_CITY = "city"
        const val DATA_TYPE_STATE = "state"
        const val DATA_TYPE_ZIP = "zip"
        const val DATA_TYPE_DOCUMENT = "id_document"
        const val DATA_TYPE_BIRTHDATE = "birthdate"

        private const val API_KEY =
                "Bearer KCR+DBqSu3M4a1+IZMCz9ffADr3EzwaBhF0WwcAQ0nVqtQlX28xZQdfoMAIcGEXI"

        fun get(): UserAPI {
            val interceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Timber.d(message)
                }
            })

            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val builder = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS).writeTimeout(40, TimeUnit.SECONDS)

            if (BuildConfig.DEBUG) builder.addInterceptor(interceptor)

            val r = Retrofit.Builder().client(builder.build())
                    .baseUrl(BuildConfig.APTO_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()

            return r.create(UserAPI::class.java)
        }
    }

    @Headers("Api-Key: $API_KEY")
    @POST("$VERSION/user")
    fun user(
            @Body
            body: UserInsertRequest
    ): Observable<UserInsertResponse>
}