package addcel.mobilecard.data.net.scanpay.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public class LcpfEstablecimiento implements Parcelable {

    public static final Creator<LcpfEstablecimiento> CREATOR = new Creator<LcpfEstablecimiento>() {
        @Override
        public LcpfEstablecimiento createFromParcel(Parcel in) {
            return new LcpfEstablecimiento(in);
        }

        @Override
        public LcpfEstablecimiento[] newArray(int size) {
            return new LcpfEstablecimiento[size];
        }
    };

    @SerializedName("alias")
    private String Alias;

    @SerializedName("comision_fija")
    private double ComisionFija;

    @SerializedName("comision_porcentaje")
    private double ComisionPorcentaje;

    @SerializedName("correo")
    private String Correo;

    @SerializedName("id")
    private int Id;

    @SerializedName("telefono")
    private String Telefono;

    @SerializedName("urlLogo")
    private String UrlLogo;

    public LcpfEstablecimiento() {
    }

    protected LcpfEstablecimiento(Parcel in) {
        Alias = in.readString();
        ComisionFija = in.readDouble();
        ComisionPorcentaje = in.readDouble();
        Correo = in.readString();
        Id = in.readInt();
        Telefono = in.readString();
        UrlLogo = in.readString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LcpfEstablecimiento that = (LcpfEstablecimiento) o;
        return Double.compare(that.ComisionFija, ComisionFija) == 0
                && Double.compare(that.ComisionPorcentaje, ComisionPorcentaje) == 0
                && Id == that.Id
                && Objects.equal(Alias, that.Alias)
                && Objects.equal(Correo, that.Correo)
                && Objects.equal(Telefono, that.Telefono)
                && Objects.equal(UrlLogo, that.UrlLogo);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(Alias, ComisionFija, ComisionPorcentaje, Correo, Id, Telefono, UrlLogo);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Alias);
        dest.writeDouble(ComisionFija);
        dest.writeDouble(ComisionPorcentaje);
        dest.writeString(Correo);
        dest.writeInt(Id);
        dest.writeString(Telefono);
        dest.writeString(UrlLogo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAlias() {
        return Alias;
    }

    public double getComisionFija() {
        return ComisionFija;
    }

    public double getComisionPorcentaje() {
        return ComisionPorcentaje;
    }

    public String getCorreo() {
        return Correo;
    }

    public int getId() {
        return Id;
    }

    public String getTelefono() {
        return Telefono;
    }

    public String getUrlLogo() {
        return UrlLogo;
    }

    @NotNull
    @Override
    public String toString() {
        return Strings.nullToEmpty(Alias);
    }

    public static class Builder {

        private String Alias;
        private double ComisionFija;
        private double ComisionPorcentaje;
        private String Correo;
        private int Id;
        private String Telefono;
        private String Logo;

        public LcpfEstablecimiento.Builder withAlias(String alias) {
            Alias = alias;
            return this;
        }

        public LcpfEstablecimiento.Builder withComisionFija(double comisionFija) {
            ComisionFija = comisionFija;
            return this;
        }

        public LcpfEstablecimiento.Builder withComisionPorcentaje(double comisionPorcentaje) {
            ComisionPorcentaje = comisionPorcentaje;
            return this;
        }

        public LcpfEstablecimiento.Builder withCorreo(String correo) {
            Correo = correo;
            return this;
        }

        public LcpfEstablecimiento.Builder withId(int id) {
            Id = id;
            return this;
        }

        public LcpfEstablecimiento.Builder withTelefono(String telefono) {
            Telefono = telefono;
            return this;
        }

        public LcpfEstablecimiento.Builder withLogo(String logo) {
            Logo = logo;
            return this;
        }

        public LcpfEstablecimiento build() {
            LcpfEstablecimiento Account = new LcpfEstablecimiento();
            Account.Alias = Alias;
            Account.ComisionFija = ComisionFija;
            Account.ComisionPorcentaje = ComisionPorcentaje;
            Account.Correo = Correo;
            Account.Id = Id;
            Account.Telefono = Telefono;
            Account.UrlLogo = Logo;
            return Account;
        }
    }
}
