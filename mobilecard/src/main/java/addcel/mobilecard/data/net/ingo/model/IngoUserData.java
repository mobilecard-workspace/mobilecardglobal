package addcel.mobilecard.data.net.ingo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class IngoUserData implements Parcelable {

    public static final Creator<IngoUserData> CREATOR = new Creator<IngoUserData>() {
        @Override
        public IngoUserData createFromParcel(Parcel in) {
            return new IngoUserData(in);
        }

        @Override
        public IngoUserData[] newArray(int size) {
            return new IngoUserData[size];
        }
    };
    @Expose
    private String customerId;
    @Expose
    private int errorCode;
    @Expose
    private String errorMessage;
    @Expose
    private String sessionId;
    @Expose
    private String ssoToken;

    public IngoUserData() {
    }

    protected IngoUserData(Parcel in) {
        customerId = in.readString();
        errorCode = in.readInt();
        errorMessage = in.readString();
        sessionId = in.readString();
        ssoToken = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerId);
        dest.writeInt(errorCode);
        dest.writeString(errorMessage);
        dest.writeString(sessionId);
        dest.writeString(ssoToken);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCustomerId() {
        return customerId;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getSsoToken() {
        return ssoToken;
    }

    public static class Builder {

        private String customerId;
        private int errorCode;
        private String errorMessage;
        private String sessionId;
        private String ssoToken;

        public IngoUserData.Builder withCustomerId(String customerId) {
            this.customerId = customerId;
            return this;
        }

        public IngoUserData.Builder withErrorCode(int errorCode) {
            this.errorCode = errorCode;
            return this;
        }

        public IngoUserData.Builder withErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public IngoUserData.Builder withSessionId(String sessionId) {
            this.sessionId = sessionId;
            return this;
        }

        public IngoUserData.Builder withSsoToken(String ssoToken) {
            this.ssoToken = ssoToken;
            return this;
        }

        public IngoUserData build() {
            IngoUserData ingoUserData = new IngoUserData();
            ingoUserData.customerId = customerId;
            ingoUserData.errorCode = errorCode;
            ingoUserData.errorMessage = errorMessage;
            ingoUserData.sessionId = sessionId;
            ingoUserData.ssoToken = ssoToken;
            return ingoUserData;
        }
    }
}
