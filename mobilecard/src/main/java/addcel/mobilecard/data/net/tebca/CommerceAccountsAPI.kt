package addcel.mobilecard.data.net.tebca

import addcel.mobilecard.data.net.wallet.FranquiciaEntity
import addcel.mobilecard.data.net.wallet.MovementEntity
import addcel.mobilecard.data.net.wallet.TipoTarjetaEntity
import android.os.Parcelable
import com.google.common.collect.Lists
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import kotlinx.android.parcel.Parcelize
import retrofit2.Retrofit
import retrofit2.http.*

/**
 * ADDCEL on 2019-08-29.
 */

@Parcelize
data class CommerceAccountsResponse(
        @SerializedName("cci") val cci: Cci?,
        @SerializedName("idError") val idError: Int, @SerializedName("mensajeError")
        val mensajeError: String, @SerializedName("tebca") val tebca: TebcaCard?
) : Parcelable

@Parcelize
data class TebcaCard(
        val balance: Double, val clabe: String? = "",
        val codigo: String? = "", val cpAmex: String? = "", val date: String? = "",
        val determinada: Boolean, val domAmex: String? = "", val idTarjeta: Int,
        val mobilecard: Boolean, val movements: List<MovementEntity>? = Lists.newArrayList(),
        val nombre: String? = "", val pan: String? = "", val phoneNumberActivation: String? = "",
        val previvale: Boolean, val tipo: FranquiciaEntity? = FranquiciaEntity.VISA,
        val tipoTarjeta: TipoTarjetaEntity? = TipoTarjetaEntity.CREDITO, val vigencia: String? = "",
        @SerializedName("img_short") val imgShort: String? = "", @SerializedName("img_full")
        val imgFull: String? = "", val estatus: Int, val favorito: Boolean
) : Parcelable

data class CciRequest(
        @SerializedName("cuenta") val cuenta: String, @SerializedName("id")
        val id: Int, @SerializedName("idBanco") val idBanco: Int
)

@Parcelize
data class Cci(
        @SerializedName("cuenta") val cuenta: String, @SerializedName("favorito")
        val favorito: Boolean, @SerializedName("idBanco") val idBanco: Int, @SerializedName("nombre")
        val nombre: String, @SerializedName("nombreBanco") val nombreBanco: String
) : Parcelable

data class TebcaActivationRequest(
        @SerializedName("codigo") val codigo: String,
        @SerializedName("id") val id: Int, @SerializedName("pan") val pan: String,
        @SerializedName("vigencia") val vigencia: String
)

interface CommerceAccountsAPI {
    companion object {
        private const val BASE = "TradeAccount/{idApp}/{idPais}/{idioma}"
        const val TYPE_CCI = 1
        const val TYPE_TEBCA = 2

        fun get(r: Retrofit): CommerceAccountsAPI {
            return r.create(CommerceAccountsAPI::class.java)
        }
    }

    @FormUrlEncoded
    @POST("$BASE/CommerceCard")
    fun getCommerceAccounts(
            @Path("idApp") idApp: Int,
            @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Field("id")
            id: Int
    ): Observable<CommerceAccountsResponse>

    @FormUrlEncoded
    @POST("$BASE/Applyforcard")
    fun applyForDigitalCard(
            @Path("idApp") idApp: Int,
            @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Field("id")
            id: Int
    ): Observable<CommerceAccountsResponse>

    @POST("$BASE/Activate")
    fun activatePhysicalCard(
            @Path("idApp") idApp: Int, @Path("idPais")
            idPais: Int, @Path("idioma") idioma: String, @Body
            body: TebcaActivationRequest
    ): Observable<CommerceAccountsResponse>

    @FormUrlEncoded
    @POST("$BASE/chgblockstatus/{blocked}")
    fun blockCard(
            @Path("idApp") idApp: Int,
            @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Path("blocked") blocked: Int,
            @Field("id") id: Int
    ): Observable<CommerceAccountsResponse>

    @FormUrlEncoded
    @POST("$BASE/replacement")
    fun replaceCard(
            @Path("idApp") idApp: Int,
            @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Field("id")
            id: Int
    ): Observable<CommerceAccountsResponse>

    @FormUrlEncoded
    @POST("$BASE/account/favorite")
    fun setFavoriteAccount(
            @Path("idApp") idApp: Int,
            @Path("idPais") idPais: Int, @Path("idioma") idioma: String, @Field("id") id: Int,
            @Field("type") tipoCuenta: Int
    ): Observable<CommerceAccountsResponse>

    @POST("$BASE/account/update")
    fun updatePersonalAccount(
            @Path("idApp") idApp: Int, @Path("idPais")
            idPais: Int, @Path("idioma") idioma: String, @Body
            body: CciRequest
    ): Observable<CommerceAccountsResponse>

    /*
    http://192.168.75.53/TradeAccount/1/1/es/account/update
     */
}