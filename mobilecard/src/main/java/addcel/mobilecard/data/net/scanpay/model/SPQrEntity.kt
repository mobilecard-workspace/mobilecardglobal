package addcel.mobilecard.data.net.scanpay.model

import com.google.gson.annotations.SerializedName


/**
 * ADDCEL on 17/10/19.
 */
data class SPQrEntity(
        @SerializedName("amount")
        val amount: Double = 0.0,
        @SerializedName("amount_mxn")
        val amountMxn: Double,
        @SerializedName("amount_usd")
        val amountUsd: Double = 1.0,
        @SerializedName("comision")
        val comision: Double = 0.0,
        @SerializedName("concept")
        val concept: String,
        @SerializedName("establecimientoId")
        val establecimientoId: Int,
        @SerializedName("idBitacora")
        val idBitacora: Int,
        @SerializedName("propina")
        val propina: Double,
        @SerializedName("referenciaNeg")
        val referenciaNeg: String
)