package addcel.mobilecard.data.net.viamericas.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreditTransactionConfirmationResponse(
        @SerializedName("amount")
        val amount: Int,
        @SerializedName("approvalCode")
        val approvalCode: String,
        @SerializedName("cardNumber")
        val cardNumber: String,
        @SerializedName("idError")
        val idError: Int,
        @SerializedName("invoiceNumber")
        val invoiceNumber: String,
        @SerializedName("objReceipt")
        val objReceipt: VmReceiptModel,
        @SerializedName("responseCode")
        val responseCode: Int,
        @SerializedName("responseMessage")
        val responseMessage: String,
        @SerializedName("transactionCurrency")
        val transactionCurrency: String,
        @SerializedName("transactionDate")
        val transactionDate: String,
        @SerializedName("transactionId")
        val transactionId: String
) : Parcelable

@Parcelize
class VmReceiptModel(
        @SerializedName("currencyPayer")
        val currencyPayer: String,
        @SerializedName("currencySrc")
        val currencySrc: String,
        @SerializedName("totalPayReceiver")
        val totalPayReceiver: Int,
        @SerializedName("totalReceiver")
        val totalReceiver: Int
) : Parcelable