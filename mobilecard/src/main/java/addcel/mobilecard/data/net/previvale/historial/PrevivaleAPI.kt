package addcel.mobilecard.data.net.previvale.historial

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * ADDCEL on 2020-01-27.
 */
interface PrevivaleAPI {

    companion object {
        private const val PATH = "PreviVale/api/{idApp}/{idPais}/{idioma}"
        private const val AUTH = "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"

        fun create(retrofit: Retrofit): PrevivaleAPI {
            return retrofit.create(PrevivaleAPI::class.java)
        }
    }


    @Headers("Authorization: $AUTH")
    @POST("$PATH/movimientosTarjeta")
    fun movimientosTarjeta(@Path("idApp") idApp: Int,
                           @Path("idPais") idPais: Int,
                           @Path("idioma") idioma: String,
                           @Body body: PrevivaleHistorialRequest): Observable<PrevivaleHistorialResponse>

}