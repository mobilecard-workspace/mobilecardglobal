package addcel.mobilecard.data.net.scanpay.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class LcpfEstablecimientoResponse {

    @SerializedName("accounts")
    private List<LcpfEstablecimiento> Accounts;

    @SerializedName("idError")
    private Long IdError;

    @SerializedName("mensajeError")
    private String MensajeError;

    public List<LcpfEstablecimiento> getAccounts() {
        return Accounts;
    }

    public Long getIdError() {
        return IdError;
    }

    public String getMensajeError() {
        return MensajeError;
    }

    public static class Builder {

        private List<LcpfEstablecimiento> Accounts;
        private Long IdError;
        private String MensajeError;

        public LcpfEstablecimientoResponse.Builder withAccounts(List<LcpfEstablecimiento> accounts) {
            Accounts = accounts;
            return this;
        }

        public LcpfEstablecimientoResponse.Builder withIdError(Long idError) {
            IdError = idError;
            return this;
        }

        public LcpfEstablecimientoResponse.Builder withMensajeError(String mensajeError) {
            MensajeError = mensajeError;
            return this;
        }

        public LcpfEstablecimientoResponse build() {
            LcpfEstablecimientoResponse LcpfEstablecimientoResponse = new LcpfEstablecimientoResponse();
            LcpfEstablecimientoResponse.Accounts = Accounts;
            LcpfEstablecimientoResponse.IdError = IdError;
            LcpfEstablecimientoResponse.MensajeError = MensajeError;
            return LcpfEstablecimientoResponse;
        }
    }
}
