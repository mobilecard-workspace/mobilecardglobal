package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ExchangeRateModel implements Parcelable {

    public static final Creator<ExchangeRateModel> CREATOR = new Creator<ExchangeRateModel>() {
        @Override
        public ExchangeRateModel createFromParcel(Parcel in) {
            return new ExchangeRateModel(in);
        }

        @Override
        public ExchangeRateModel[] newArray(int size) {
            return new ExchangeRateModel[size];
        }
    };
    @SerializedName("dateRequested")
    private String mDateRequested;
    @SerializedName("exchangeRate")
    private double mExchangeRate;
    @SerializedName("identificationLimit")
    private double mIdentificationLimit;
    @SerializedName("maximumToSend")
    private double mMaximumToSend;
    @SerializedName("minimumToSend")
    private double mMinimumToSend;
    @SerializedName("__hashCodeCalc")
    private Boolean m_HashCodeCalc;

    protected ExchangeRateModel(Parcel in) {
        mDateRequested = in.readString();
        mExchangeRate = in.readDouble();
        mIdentificationLimit = in.readDouble();
        mMaximumToSend = in.readDouble();
        mMinimumToSend = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDateRequested);
        dest.writeDouble(mExchangeRate);
        dest.writeDouble(mIdentificationLimit);
        dest.writeDouble(mMaximumToSend);
        dest.writeDouble(mMinimumToSend);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getDateRequested() {
        return mDateRequested;
    }

    public void setDateRequested(String dateRequested) {
        mDateRequested = dateRequested;
    }

    public double getExchangeRate() {
        return mExchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        mExchangeRate = exchangeRate;
    }

    public double getIdentificationLimit() {
        return mIdentificationLimit;
    }

    public void setIdentificationLimit(double identificationLimit) {
        mIdentificationLimit = identificationLimit;
    }

    public double getMaximumToSend() {
        return mMaximumToSend;
    }

    public void setMaximumToSend(double maximumToSend) {
        mMaximumToSend = maximumToSend;
    }

    public double getMinimumToSend() {
        return mMinimumToSend;
    }

    public void setMinimumToSend(double minimumToSend) {
        mMinimumToSend = minimumToSend;
    }

    public Boolean get_HashCodeCalc() {
        return m_HashCodeCalc;
    }

    public void set_HashCodeCalc(Boolean _HashCodeCalc) {
        m_HashCodeCalc = _HashCodeCalc;
    }
}
