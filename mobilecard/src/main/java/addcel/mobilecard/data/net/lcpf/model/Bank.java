package addcel.mobilecard.data.net.lcpf.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public class Bank {

    @SerializedName("clave")
    private String Clave;
    @SerializedName("id")
    private Long Id;
    @SerializedName("nombre_corto")
    private String NombreCorto;
    @SerializedName("nombre_razon_social")
    private String NombreRazonSocial;
    @SerializedName("comision_fija")
    private double ComisionFija;
    @SerializedName("comision_porcentaje")
    private double ComisionPorcentaje;

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        Clave = clave;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNombreCorto() {
        return NombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        NombreCorto = nombreCorto;
    }

    public String getNombreRazonSocial() {
        return NombreRazonSocial;
    }

    public void setNombreRazonSocial(String nombreRazonSocial) {
        NombreRazonSocial = nombreRazonSocial;
    }

    public double getComisionFija() {
        return ComisionFija;
    }

    public Bank setComisionFija(double comisionFija) {
        ComisionFija = comisionFija;
        return this;
    }

    public double getComisionPorcentaje() {
        return ComisionPorcentaje;
    }

    public Bank setComisionPorcentaje(double comisionPorcentaje) {
        ComisionPorcentaje = comisionPorcentaje;
        return this;
    }

    @NotNull
    @Override
    public String toString() {
        return NombreCorto;
    }
}
