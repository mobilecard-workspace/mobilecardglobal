package addcel.mobilecard.data.net.hth.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class HthAccountRequest {

    @SerializedName("actNumber")
    private String ActNumber;

    @SerializedName("actType")
    private String ActType;

    @SerializedName("alias")
    private String Alias;

    @SerializedName("bankCode")
    private String BankCode;

    @SerializedName("contact")
    private String Contact;

    @SerializedName("email")
    private String Email;

    @SerializedName("holderName")
    private String HolderName;

    @SerializedName("idUsuario")
    private Long IdUsuario;

    @SerializedName("idioma")
    private String Idioma;

    @SerializedName("phone")
    private String Phone;

    @SerializedName("rfc")
    private String Rfc;

    public String getActNumber() {
        return ActNumber;
    }

    public String getActType() {
        return ActType;
    }

    public String getAlias() {
        return Alias;
    }

    public String getBankCode() {
        return BankCode;
    }

    public String getContact() {
        return Contact;
    }

    public String getEmail() {
        return Email;
    }

    public String getHolderName() {
        return HolderName;
    }

    public Long getIdUsuario() {
        return IdUsuario;
    }

    public String getIdioma() {
        return Idioma;
    }

    public String getPhone() {
        return Phone;
    }

    public String getRfc() {
        return Rfc;
    }

    public static class Builder {

        private String ActNumber;
        private String ActType;
        private String Alias;
        private String BankCode;
        private String Contact;
        private String Email;
        private String HolderName;
        private Long IdUsuario;
        private String Idioma;
        private String Phone;
        private String Rfc;

        public Builder withActNumber(String actNumber) {
            ActNumber = actNumber;
            return this;
        }

        public Builder withActType(String actType) {
            ActType = actType;
            return this;
        }

        public Builder withAlias(String alias) {
            Alias = alias;
            return this;
        }

        public Builder withBankCode(String bankCode) {
            BankCode = bankCode;
            return this;
        }

        public Builder withContact(String contact) {
            Contact = contact;
            return this;
        }

        public Builder withEmail(String email) {
            Email = email;
            return this;
        }

        public Builder withHolderName(String holderName) {
            HolderName = holderName;
            return this;
        }

        public Builder withIdUsuario(Long idUsuario) {
            IdUsuario = idUsuario;
            return this;
        }

        public Builder withIdioma(String idioma) {
            Idioma = idioma;
            return this;
        }

        public Builder withPhone(String phone) {
            Phone = phone;
            return this;
        }

        public Builder withRfc(String rfc) {
            Rfc = rfc;
            return this;
        }

        public HthAccountRequest build() {
            HthAccountRequest HthAccountRequest = new HthAccountRequest();
            HthAccountRequest.ActNumber = ActNumber;
            HthAccountRequest.ActType = ActType;
            HthAccountRequest.Alias = Alias;
            HthAccountRequest.BankCode = BankCode;
            HthAccountRequest.Contact = Contact;
            HthAccountRequest.Email = Email;
            HthAccountRequest.HolderName = HolderName;
            HthAccountRequest.IdUsuario = IdUsuario;
            HthAccountRequest.Idioma = Idioma;
            HthAccountRequest.Phone = Phone;
            HthAccountRequest.Rfc = Rfc;
            return HthAccountRequest;
        }
    }
}
