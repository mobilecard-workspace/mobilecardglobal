package addcel.mobilecard.data.net.viamericas.model;

/**
 * ADDCEL on 25/03/17.
 */

//{idSender, idReciever, paymentType, cardNumber, expDate, cvv, cardFirstName, cardLastName, nickName}
public final class CreditTransactionConfirmationModel {

    private final long idBitacora;
    private final String idSender;
    private final String idReciever;
    private final String paymentType;
    private final String cardNumber;
    private final String expDate;
    private final String cvv;
    private final String cardFirstName;
    private final String cardLastName;
    private final String nickName;
    private final int idTarjeta;
    private final long idUsuario;

    private CreditTransactionConfirmationModel(Builder builder) {
        idBitacora = builder.idBitacora;
        idSender = builder.idSender;
        idReciever = builder.idReciever;
        paymentType = builder.paymentType;
        cardNumber = builder.cardNumber;
        expDate = builder.expDate;
        cvv = builder.cvv;
        cardFirstName = builder.cardFirstName;
        cardLastName = builder.cardLastName;
        nickName = builder.nickName;
        idTarjeta = builder.idTarjeta;
        idUsuario = builder.idUsuario;
    }

    public long getIdBitacora() {
        return idBitacora;
    }

    public String getIdSender() {
        return idSender;
    }

    public String getIdReciever() {
        return idReciever;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpDate() {
        return expDate;
    }

    public String getCvv() {
        return cvv;
    }

    public String getCardFirstName() {
        return cardFirstName;
    }

    public String getCardLastName() {
        return cardLastName;
    }

    public String getNickName() {
        return nickName;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public static class Builder {
        private long idBitacora;
        private String idSender;
        private String idReciever;
        private String paymentType;
        private String cardNumber;
        private String expDate;
        private String cvv;
        private String cardFirstName;
        private String cardLastName;
        private String nickName;
        private int idTarjeta;
        private long idUsuario;

        public Builder setIdBitacora(long idBitacora) {
            this.idBitacora = idBitacora;
            return this;
        }

        public Builder setIdSender(String idSender) {
            this.idSender = idSender;
            return this;
        }

        public Builder setIdReciever(String idReciever) {
            this.idReciever = idReciever;
            return this;
        }

        public Builder setPaymentType(String paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public Builder setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder setExpDate(String expDate) {
            this.expDate = expDate;
            return this;
        }

        public Builder setCvv(String cvv) {
            this.cvv = cvv;
            return this;
        }

        public Builder setCardFirstName(String cardFirstName) {
            this.cardFirstName = cardFirstName;
            return this;
        }

        public Builder setCardLastName(String cardLastName) {
            this.cardLastName = cardLastName;
            return this;
        }

        public Builder setNickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        public Builder setIdTarjeta(int idTarjeta) {
            this.idTarjeta = idTarjeta;
            return this;
        }

        public Builder setIdUsuario(long idUsuario) {
            this.idUsuario = idUsuario;
            return this;
        }

        public CreditTransactionConfirmationModel build() {
            return new CreditTransactionConfirmationModel(this);
        }
    }
}
