package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

@SuppressWarnings("unused")
public class SenderProfileResponse extends McResponse {

    @SerializedName("cardType")
    private String mCardType;
    @SerializedName("company")
    private String mCompany;
    @SerializedName("expDate")
    private String mExpDate;
    @SerializedName("idSender")
    private String mIdSender;
    @SerializedName("idSenderPayment")
    private String mIdSenderPayment;
    @SerializedName("maskedCardNumber")
    private String mMaskedCardNumber;
    @SerializedName("nickName")
    private String mNickName;
    @SerializedName("paymentType")
    private String mPaymentType;

    public SenderProfileResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public String getCardType() {
        return mCardType;
    }

    public void setCardType(String cardType) {
        mCardType = cardType;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getExpDate() {
        return mExpDate;
    }

    public void setExpDate(String expDate) {
        mExpDate = expDate;
    }

    public String getIdSender() {
        return mIdSender;
    }

    public void setIdSender(String idSender) {
        mIdSender = idSender;
    }

    public String getIdSenderPayment() {
        return mIdSenderPayment;
    }

    public void setIdSenderPayment(String idSenderPayment) {
        mIdSenderPayment = idSenderPayment;
    }

    public String getMaskedCardNumber() {
        return mMaskedCardNumber;
    }

    public void setMaskedCardNumber(String maskedCardNumber) {
        mMaskedCardNumber = maskedCardNumber;
    }

    public String getNickName() {
        return mNickName;
    }

    public void setNickName(String nickName) {
        mNickName = nickName;
    }

    public String getPaymentType() {
        return mPaymentType;
    }

    public void setPaymentType(String paymentType) {
        mPaymentType = paymentType;
    }
}
