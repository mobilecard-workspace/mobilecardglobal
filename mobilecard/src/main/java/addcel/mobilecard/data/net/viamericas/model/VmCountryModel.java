package addcel.mobilecard.data.net.viamericas.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

/**
 * ADDCEL on 29/03/17.
 */

public final class VmCountryModel implements Parcelable, Comparable<VmCountryModel> {
    public static final Creator<VmCountryModel> CREATOR = new Creator<VmCountryModel>() {
        @Override
        public VmCountryModel createFromParcel(Parcel in) {
            return new VmCountryModel(in);
        }

        @Override
        public VmCountryModel[] newArray(int size) {
            return new VmCountryModel[size];
        }
    };
    private String idCountry;
    private String nameCountry;
    private int weight;

    public VmCountryModel() {
    }

    public VmCountryModel(String idCountry, String nameCountry) {
        this.idCountry = idCountry;
        this.nameCountry = nameCountry;
        this.weight = 0;
    }

    private VmCountryModel(Parcel in) {
        idCountry = in.readString();
        nameCountry = in.readString();
        weight = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idCountry);
        dest.writeString(nameCountry);
        dest.writeInt(weight);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @NotNull
    @Override
    public String toString() {
        return nameCountry;
    }

    @Override
    public int compareTo(@NonNull VmCountryModel country) {

        return Integer.compare(weight, country.weight);
    }
}
