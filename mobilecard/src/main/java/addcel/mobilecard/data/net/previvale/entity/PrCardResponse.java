package addcel.mobilecard.data.net.previvale.entity;

import com.google.gson.annotations.SerializedName;

/**
 * ADDCEL on 12/11/18.
 */
public final class PrCardResponse {

    /**
     * code : 1000 data :
     * {"ct":"1234567890","determinada":true,"fechaRegistro":"01/01/01","pan":"123456789","tipo":"Mastercard","tipoTarjetaEntity":0,"vigencia":"1234567890"}
     * message : SUCCESS
     */
    @SerializedName("code")
    private int code;

    @SerializedName("data")
    private DataEntity data;

    @SerializedName("message")
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * ct : 1234567890 determinada : true fechaRegistro : 01/01/01 pan : 123456789 tipo : Mastercard
         * tipoTarjetaEntity : 0 vigencia : 1234567890
         */
        @SerializedName("ct")
        private String ct;

        @SerializedName("determinada")
        private boolean determinada;

        @SerializedName("fechaRegistro")
        private String fechaRegistro;

        @SerializedName("pan")
        private String pan;

        @SerializedName("tipo")
        private String tipo;

        @SerializedName("tipoTarjetaEntity")
        private int tipoTarjeta;

        @SerializedName("vigencia")
        private String vigencia;

        public String getCt() {
            return ct;
        }

        public void setCt(String ct) {
            this.ct = ct;
        }

        public boolean isDeterminada() {
            return determinada;
        }

        public void setDeterminada(boolean determinada) {
            this.determinada = determinada;
        }

        public String getFechaRegistro() {
            return fechaRegistro;
        }

        public void setFechaRegistro(String fechaRegistro) {
            this.fechaRegistro = fechaRegistro;
        }

        public String getPan() {
            return pan;
        }

        public void setPan(String pan) {
            this.pan = pan;
        }

        public String getTipo() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public int getTipoTarjeta() {
            return tipoTarjeta;
        }

        public void setTipoTarjeta(int tipoTarjeta) {
            this.tipoTarjeta = tipoTarjeta;
        }

        public String getVigencia() {
            return vigencia;
        }

        public void setVigencia(String vigencia) {
            this.vigencia = vigencia;
        }
    }
}
