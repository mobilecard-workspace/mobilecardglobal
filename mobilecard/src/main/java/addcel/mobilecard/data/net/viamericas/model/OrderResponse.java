package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import addcel.mobilecard.data.net.usuarios.model.McResponse;

@SuppressWarnings("unused")
public class OrderResponse extends McResponse {

    @SerializedName("idBitacora")
    private long mIdBitacora;
    @SerializedName("holdMotives")
    private List<HoldMotiveModel> mHoldMotives;
    @SerializedName("idBranch")
    private String mIdBranch;
    @SerializedName("idReceiver")
    private String mIdReceiver;
    @SerializedName("idSender")
    private String mIdSender;
    @SerializedName("passwordReceiver")
    private String mPasswordReceiver;
    @SerializedName("requireAdditionalInformation")
    private Boolean mRequireAdditionalInformation;

    public OrderResponse(int idError, String mensajeError) {
        super(idError, mensajeError);
    }

    public long getIdBitacora() {
        return mIdBitacora;
    }

    public void setIdBitacora(long idBitacora) {
        this.mIdBitacora = idBitacora;
    }

    public List<HoldMotiveModel> getHoldMotives() {
        return mHoldMotives;
    }

    public void setHoldMotives(List<HoldMotiveModel> holdMotives) {
        mHoldMotives = holdMotives;
    }

    public String getIdBranch() {
        return mIdBranch;
    }

    public void setIdBranch(String idBranch) {
        mIdBranch = idBranch;
    }

    public String getIdReceiver() {
        return mIdReceiver;
    }

    public void setIdReceiver(String idReceiver) {
        mIdReceiver = idReceiver;
    }

    public String getIdSender() {
        return mIdSender;
    }

    public void setIdSender(String idSender) {
        mIdSender = idSender;
    }

    public String getPasswordReceiver() {
        return mPasswordReceiver;
    }

    public void setPasswordReceiver(String passwordReceiver) {
        mPasswordReceiver = passwordReceiver;
    }

    public Boolean getRequireAdditionalInformation() {
        return mRequireAdditionalInformation;
    }

    public void setRequireAdditionalInformation(Boolean requireAdditionalInformation) {
        mRequireAdditionalInformation = requireAdditionalInformation;
    }
}
