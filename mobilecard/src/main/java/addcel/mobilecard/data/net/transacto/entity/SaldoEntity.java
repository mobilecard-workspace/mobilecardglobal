package addcel.mobilecard.data.net.transacto.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

public final class SaldoEntity implements Parcelable {

    public static final Creator<SaldoEntity> CREATOR = new Creator<SaldoEntity>() {
        @Override
        public SaldoEntity createFromParcel(Parcel in) {
            return new SaldoEntity(in);
        }

        @Override
        public SaldoEntity[] newArray(int size) {
            return new SaldoEntity[size];
        }
    };

    @SerializedName("comision")
    private double mComision;

    @SerializedName("comisionMxn")
    private double mComisionMxn;

    @SerializedName("comisionUsd")
    private double mComisionUsd;

    @SerializedName("concepto")
    private String mConcepto;

    @SerializedName("datosAdicionales")
    private String mDatosAdicionales;

    @SerializedName("descripcion")
    private String mDescripcion;

    @SerializedName("eMail")
    private String mEMail;

    @SerializedName("folioComercio")
    private String mFolioComercio;

    @SerializedName("folioTransaccion")
    private String mFolioTransaccion;

    @SerializedName("idError")
    private int mIdError;

    @SerializedName("idTransaccion")
    private int mIdTransaccion;

    @SerializedName("mensajeAntad")
    private String mMensajeAntad;

    @SerializedName("mensajeCajero")
    private String mMensajeCajero;

    @SerializedName("mensajeError")
    private String mMensajeError;

    @SerializedName("mensajeTicket")
    private String mMensajeTicket;

    @SerializedName("monto")
    private double mMonto;

    @SerializedName("montoMxn")
    private double mMontoMxn;

    @SerializedName("montoUsd")
    private double mMontoUsd;

    @SerializedName("numAuth")
    private String mNumAuth;

    @SerializedName("numAutorizacion")
    private String mNumAutorizacion;

    @SerializedName("referencia")
    private String mReferencia;

    @SerializedName("respCode")
    private String mRespCode;

    @SerializedName("tipoCambio")
    private double mTipoCambio;

    @SerializedName("totalMxn")
    private double mTotalMxn;

    @SerializedName("totalUsd")
    private double mTotalUsd;

    public SaldoEntity(int mIdError, String mMensajeError) {
        this.mIdError = mIdError;
        this.mMensajeError = mMensajeError;
    }

    public SaldoEntity(Builder b) {
        this.mIdError = 0;
        this.mMensajeError = "";
        this.mComision = b.mComision;
        this.mComisionMxn = b.mComisionMxn;
        this.mComisionUsd = b.mComisionUsd;
        this.mConcepto = b.mConcepto;
        this.mMonto = b.mMonto;
        this.mMontoMxn = b.mMontoMxn;
        this.mMontoUsd = b.mMontoUsd;
        this.mTipoCambio = b.mTipoCambio;
        this.mTotalMxn = b.mTotalMxn;
        this.mTotalUsd = b.mTotalUsd;
    }

    protected SaldoEntity(Parcel in) {
        mComision = in.readDouble();
        mComisionMxn = in.readDouble();
        mComisionUsd = in.readDouble();
        mConcepto = in.readString();
        mDatosAdicionales = in.readString();
        mDescripcion = in.readString();
        mEMail = in.readString();
        mFolioComercio = in.readString();
        mFolioTransaccion = in.readString();
        mIdError = in.readInt();
        mIdTransaccion = in.readInt();
        mMensajeAntad = in.readString();
        mMensajeCajero = in.readString();
        mMensajeError = in.readString();
        mMensajeTicket = in.readString();
        mMonto = in.readDouble();
        mMontoMxn = in.readDouble();
        mMontoUsd = in.readDouble();
        mNumAuth = in.readString();
        mNumAutorizacion = in.readString();
        mReferencia = in.readString();
        mRespCode = in.readString();
        mTipoCambio = in.readDouble();
        mTotalMxn = in.readDouble();
        mTotalUsd = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mComision);
        dest.writeDouble(mComisionMxn);
        dest.writeDouble(mComisionUsd);
        dest.writeString(mConcepto);
        dest.writeString(mDatosAdicionales);
        dest.writeString(mDescripcion);
        dest.writeString(mEMail);
        dest.writeString(mFolioComercio);
        dest.writeString(mFolioTransaccion);
        dest.writeInt(mIdError);
        dest.writeInt(mIdTransaccion);
        dest.writeString(mMensajeAntad);
        dest.writeString(mMensajeCajero);
        dest.writeString(mMensajeError);
        dest.writeString(mMensajeTicket);
        dest.writeDouble(mMonto);
        dest.writeDouble(mMontoMxn);
        dest.writeDouble(mMontoUsd);
        dest.writeString(mNumAuth);
        dest.writeString(mNumAutorizacion);
        dest.writeString(mReferencia);
        dest.writeString(mRespCode);
        dest.writeDouble(mTipoCambio);
        dest.writeDouble(mTotalMxn);
        dest.writeDouble(mTotalUsd);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public double getComision() {
        return mComision;
    }

    public void setComision(double comision) {
        mComision = comision;
    }

    public double getComisionMxn() {
        return mComisionMxn;
    }

    public void setComisionMxn(double comisionMxn) {
        mComisionMxn = comisionMxn;
    }

    public double getComisionUsd() {
        return mComisionUsd;
    }

    public void setComisionUsd(double comisionUsd) {
        mComisionUsd = comisionUsd;
    }

    public String getConcepto() {
        return mConcepto;
    }

    public void setConcepto(String concepto) {
        mConcepto = concepto;
    }

    public String getDatosAdicionales() {
        return mDatosAdicionales;
    }

    public void setDatosAdicionales(String datosAdicionales) {
        mDatosAdicionales = datosAdicionales;
    }

    public String getDescripcion() {
        return mDescripcion;
    }

    public void setDescripcion(String descripcion) {
        mDescripcion = descripcion;
    }

    public String getEMail() {
        return mEMail;
    }

    public void setEMail(String eMail) {
        mEMail = eMail;
    }

    public String getFolioComercio() {
        return mFolioComercio;
    }

    public void setFolioComercio(String folioComercio) {
        mFolioComercio = folioComercio;
    }

    public String getFolioTransaccion() {
        return mFolioTransaccion;
    }

    public void setFolioTransaccion(String folioTransaccion) {
        mFolioTransaccion = folioTransaccion;
    }

    public int getIdError() {
        return mIdError;
    }

    public void setIdError(int idError) {
        mIdError = idError;
    }

    public int getIdTransaccion() {
        return mIdTransaccion;
    }

    public void setIdTransaccion(int idTransaccion) {
        mIdTransaccion = idTransaccion;
    }

    public String getMensajeAntad() {
        return mMensajeAntad;
    }

    public void setMensajeAntad(String mensajeAntad) {
        mMensajeAntad = mensajeAntad;
    }

    public String getMensajeCajero() {
        return mMensajeCajero;
    }

    public void setMensajeCajero(String mensajeCajero) {
        mMensajeCajero = mensajeCajero;
    }

    public String getMensajeError() {
        return mMensajeError;
    }

    public void setMensajeError(String mensajeError) {
        mMensajeError = mensajeError;
    }

    public String getMensajeTicket() {
        return mMensajeTicket;
    }

    public void setMensajeTicket(String mensajeTicket) {
        mMensajeTicket = mensajeTicket;
    }

    public double getMonto() {
        return mMonto;
    }

    public void setMonto(double monto) {
        mMonto = monto;
    }

    public double getMontoMxn() {
        return mMontoMxn;
    }

    public void setMontoMxn(double montoMxn) {
        mMontoMxn = montoMxn;
    }

    public double getMontoUsd() {
        return mMontoUsd;
    }

    public void setMontoUsd(double montoUsd) {
        mMontoUsd = montoUsd;
    }

    public String getNumAuth() {
        return mNumAuth;
    }

    public void setNumAuth(String numAuth) {
        mNumAuth = numAuth;
    }

    public String getNumAutorizacion() {
        return mNumAutorizacion;
    }

    public void setNumAutorizacion(String numAutorizacion) {
        mNumAutorizacion = numAutorizacion;
    }

    public String getReferencia() {
        return mReferencia;
    }

    public void setReferencia(String referencia) {
        mReferencia = referencia;
    }

    public String getRespCode() {
        return mRespCode;
    }

    public void setRespCode(String respCode) {
        mRespCode = respCode;
    }

    public double getTipoCambio() {
        return mTipoCambio;
    }

    public void setTipoCambio(double tipoCambio) {
        mTipoCambio = tipoCambio;
    }

    public double getTotalMxn() {
        return mTotalMxn;
    }

    public void setTotalMxn(double totalMxn) {
        mTotalMxn = totalMxn;
    }

    public double getTotalUsd() {
        return mTotalUsd;
    }

    public void setTotalUsd(double totalUsd) {
        mTotalUsd = totalUsd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaldoEntity that = (SaldoEntity) o;
        return Double.compare(that.mComision, mComision) == 0
                && Double.compare(that.mComisionMxn, mComisionMxn) == 0
                && Double.compare(that.mComisionUsd, mComisionUsd) == 0
                && mIdError == that.mIdError
                && mIdTransaccion == that.mIdTransaccion
                && Double.compare(that.mMonto, mMonto) == 0
                && Double.compare(that.mMontoMxn, mMontoMxn) == 0
                && Double.compare(that.mMontoUsd, mMontoUsd) == 0
                && Double.compare(that.mTipoCambio, mTipoCambio) == 0
                && Double.compare(that.mTotalMxn, mTotalMxn) == 0
                && Double.compare(that.mTotalUsd, mTotalUsd) == 0
                && Objects.equal(mConcepto, that.mConcepto)
                && Objects.equal(mDatosAdicionales, that.mDatosAdicionales)
                && Objects.equal(mDescripcion, that.mDescripcion)
                && Objects.equal(mEMail, that.mEMail)
                && Objects.equal(mFolioComercio, that.mFolioComercio)
                && Objects.equal(mFolioTransaccion, that.mFolioTransaccion)
                && Objects.equal(mMensajeAntad, that.mMensajeAntad)
                && Objects.equal(mMensajeCajero, that.mMensajeCajero)
                && Objects.equal(mMensajeError, that.mMensajeError)
                && Objects.equal(mMensajeTicket, that.mMensajeTicket)
                && Objects.equal(mNumAuth, that.mNumAuth)
                && Objects.equal(mNumAutorizacion, that.mNumAutorizacion)
                && Objects.equal(mReferencia, that.mReferencia)
                && Objects.equal(mRespCode, that.mRespCode);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mComision, mComisionMxn, mComisionUsd, mConcepto, mDatosAdicionales,
                mDescripcion, mEMail, mFolioComercio, mFolioTransaccion, mIdError, mIdTransaccion,
                mMensajeAntad, mMensajeCajero, mMensajeError, mMensajeTicket, mMonto, mMontoMxn, mMontoUsd,
                mNumAuth, mNumAutorizacion, mReferencia, mRespCode, mTipoCambio, mTotalMxn, mTotalUsd);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("mComision", mComision)
                .add("mComisionMxn", mComisionMxn)
                .add("mComisionUsd", mComisionUsd)
                .add("mConcepto", mConcepto)
                .add("mDatosAdicionales", mDatosAdicionales)
                .add("mDescripcion", mDescripcion)
                .add("mEMail", mEMail)
                .add("mFolioComercio", mFolioComercio)
                .add("mFolioTransaccion", mFolioTransaccion)
                .add("mIdError", mIdError)
                .add("mIdTransaccion", mIdTransaccion)
                .add("mMensajeAntad", mMensajeAntad)
                .add("mMensajeCajero", mMensajeCajero)
                .add("mMensajeError", mMensajeError)
                .add("mMensajeTicket", mMensajeTicket)
                .add("mMonto", mMonto)
                .add("mMontoMxn", mMontoMxn)
                .add("mMontoUsd", mMontoUsd)
                .add("mNumAuth", mNumAuth)
                .add("mNumAutorizacion", mNumAutorizacion)
                .add("mReferencia", mReferencia)
                .add("mRespCode", mRespCode)
                .add("mTipoCambio", mTipoCambio)
                .add("mTotalMxn", mTotalMxn)
                .add("mTotalUsd", mTotalUsd)
                .toString();
    }

    public static class Builder {
        private double mComision;
        private double mComisionMxn;
        private double mComisionUsd;
        private String mConcepto;
        private double mMonto;
        private double mMontoMxn;
        private double mMontoUsd;
        private double mTipoCambio;
        private double mTotalMxn;
        private double mTotalUsd;

        public Builder() {
        }

        public Builder setmComision(double mComision) {
            this.mComision = mComision;
            return this;
        }

        public Builder setmComisionMxn(double mComisionMxn) {
            this.mComisionMxn = mComisionMxn;
            return this;
        }

        public Builder setmComisionUsd(double mComisionUsd) {
            this.mComisionUsd = mComisionUsd;
            return this;
        }

        public Builder setmConcepto(String mConcepto) {
            this.mConcepto = mConcepto;
            return this;
        }

        public Builder setmMonto(double mMonto) {
            this.mMonto = mMonto;
            return this;
        }

        public Builder setmMontoMxn(double mMontoMxn) {
            this.mMontoMxn = mMontoMxn;
            return this;
        }

        public Builder setmMontoUsd(double mMontoUsd) {
            this.mMontoUsd = mMontoUsd;
            return this;
        }

        public Builder setmTipoCambio(double mTipoCambio) {
            this.mTipoCambio = mTipoCambio;
            return this;
        }

        public Builder setmTotalMxn(double mTotalMxn) {
            this.mTotalMxn = mTotalMxn;
            return this;
        }

        public Builder setmTotalUsd(double mTotalUsd) {
            this.mTotalUsd = mTotalUsd;
            return this;
        }

        public SaldoEntity build() {
            return new SaldoEntity(this);
        }
    }
}
