package addcel.mobilecard.data.net.viamericas.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderFeeRequest {
    /**
     * idModePay : c
     * idBranchPay : bc0001
     * netAmount : 2
     */

    @SerializedName("idModePay")
    private String idModePay;
    @SerializedName("idBranchPay")
    private String idBranchPay;
    @SerializedName("netAmount")
    private String netAmount;

    private OrderFeeRequest(Builder builder) {
        setIdModePay(builder.idModePay);
        setIdBranchPay(builder.idBranchPay);
        setNetAmount(builder.netAmount);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(OrderFeeRequest copy) {
        Builder builder = new Builder();
        builder.idModePay = copy.getIdModePay();
        builder.idBranchPay = copy.getIdBranchPay();
        builder.netAmount = copy.getNetAmount();
        return builder;
    }

    public String getIdModePay() {
        return idModePay;
    }

    public void setIdModePay(String idModePay) {
        this.idModePay = idModePay;
    }

    public String getIdBranchPay() {
        return idBranchPay;
    }

    public void setIdBranchPay(String idBranchPay) {
        this.idBranchPay = idBranchPay;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }


    public static final class Builder {
        private String idModePay;
        private String idBranchPay;
        private String netAmount;

        private Builder() {
        }

        public Builder setIdModePay(String idModePay) {
            this.idModePay = idModePay;
            return this;
        }

        public Builder setIdBranchPay(String idBranchPay) {
            this.idBranchPay = idBranchPay;
            return this;
        }

        public Builder setNetAmount(String netAmount) {
            this.netAmount = netAmount;
            return this;
        }

        public OrderFeeRequest build() {
            return new OrderFeeRequest(this);
        }
    }
}
