package addcel.mobilecard.data.net.usuarios.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public final class TerminosResponse {

    @SerializedName("idError")
    private int mIdError;

    @SerializedName("mensajeError")
    private String mMensajeError;

    @SerializedName("termino")
    private String mTerminos;

    public TerminosResponse() {
    }

    public TerminosResponse(int mIdError, @NotNull String mMensajeError) {
        this.mIdError = mIdError;
        this.mMensajeError = mMensajeError;
    }

    public int getIdError() {
        return mIdError;
    }

    public void setIdError(int idError) {
        mIdError = idError;
    }

    public String getMensajeError() {
        return mMensajeError;
    }

    public void setMensajeError(String mensajeError) {
        mMensajeError = mensajeError;
    }

    public String getTerminos() {
        return mTerminos;
    }

    public void setTerminos(String terminos) {
        mTerminos = terminos;
    }

    @NotNull
    @Override
    public String toString() {
        return mMensajeError;
    }
}
