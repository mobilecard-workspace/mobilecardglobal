package addcel.mobilecard.data.local.db.favoritos.database;

/*
 ADDCEL on 22/06/18.
*/

import androidx.room.Database;
import androidx.room.RoomDatabase;

import addcel.mobilecard.data.local.db.favoritos.dao.FavoritoDaoRx;
import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;

@Database(entities = {Favorito.class}, version = 3, exportSchema = false)
public abstract class McDatabaseRx extends RoomDatabase {

    public abstract FavoritoDaoRx favoritoDao();
}
