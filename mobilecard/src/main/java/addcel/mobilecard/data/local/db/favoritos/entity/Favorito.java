package addcel.mobilecard.data.local.db.favoritos.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.common.base.MoreObjects;

/**
 * ADDCEL on 02/10/17.
 */
@Entity(tableName = "favorito")
public class Favorito {
    public static final int CAT_RECARGA = 1;
    public static final int CAT_PEAJE = 2;
    public static final int CAT_SERVICIO = 3;
    public static final int CAT_SERVICIO_US = 4;

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "descripcion")
    private String descripcion;

    @ColumnInfo(name = "id_pais")
    private int pais;

    @ColumnInfo(name = "nombre")
    private String nombre;

    @ColumnInfo(name = "categoria")
    private int categoria;

    @ColumnInfo(name = "logotipo")
    private int logotipo;

    @ColumnInfo(name = "servicio")
    private String servicio;

    @ColumnInfo(name = "montos")
    private String montos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPais() {
        return pais;
    }

    public void setPais(int pais) {
        this.pais = pais;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getLogotipo() {
        return logotipo;
    }

    public void setLogotipo(int logotipo) {
        this.logotipo = logotipo;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getMontos() {
        return montos;
    }

    public void setMontos(String montos) {
        this.montos = montos;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("descripcion", descripcion)
                .add("pais", pais)
                .add("nombre", nombre)
                .add("categoria", categoria)
                .add("logotipo", logotipo)
                .add("servicio", servicio)
                .add("montos", montos)
                .toString();
    }
}
