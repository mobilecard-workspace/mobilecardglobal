package addcel.mobilecard.data.local.shared.usuario;

import androidx.annotation.Nullable;

import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.location.McLocationData;

/**
 * ADDCEL on 14/03/17.
 */
public interface SessionOperations {

    String USUARIO = "usuario";
    String MIN_LOCATION = "min_location";
    String LOGGED = "logged";
    String NEGOCIO = "negocio";
    String NEGOCIO_LOGGED = "negocio_logged";

    Usuario getUsuario();

    void setUsuario(Usuario usuario);

    McLocationData getMinimalLocationData();

    void setMinimalLocationData(@Nullable McLocationData locationData);

    boolean isUsuarioLogged();

    void setUsuarioLogged(boolean logged);

    NegocioEntity getNegocio();

    void setNegocio(NegocioEntity negocio);

    boolean isNegocioLogged();

    void setNegocioLogged(boolean logged);

    void logout();
}
