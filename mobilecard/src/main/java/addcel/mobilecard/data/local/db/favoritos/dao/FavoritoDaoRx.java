package addcel.mobilecard.data.local.db.favoritos.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import addcel.mobilecard.data.local.db.favoritos.entity.Favorito;
import io.reactivex.Flowable;

/**
 * ADDCEL on 22/06/18.
 */
@Dao
public interface FavoritoDaoRx {
    @Query("select * from favorito where id_pais = :pais")
    Flowable<List<Favorito>> getAll(int pais);

    @Delete
    int delete(Favorito favorito);

    @Insert
    long insert(Favorito favorito);
}
