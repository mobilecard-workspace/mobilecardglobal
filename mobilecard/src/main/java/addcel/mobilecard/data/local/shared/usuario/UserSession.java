package addcel.mobilecard.data.local.shared.usuario;

import android.content.SharedPreferences;

import androidx.annotation.Nullable;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import addcel.mobilecard.data.net.negocios.entity.NegocioEntity;
import addcel.mobilecard.data.net.usuarios.model.Usuario;
import addcel.mobilecard.domain.location.McLocationData;

/**
 * ADDCEL on 14/03/17.
 */
public final class UserSession implements SessionOperations {

    private final SharedPreferences preferences;
    private final Gson gson;

    public UserSession(SharedPreferences preferences, Gson gson) {
        this.preferences = preferences;
        this.gson = gson;
    }

    @Override
    public Usuario getUsuario() {
        final String sUsuario = preferences.getString(USUARIO, "");
        return gson.fromJson(sUsuario, Usuario.class);
    }

    @Override
    public void setUsuario(Usuario usuario) {
        final SharedPreferences.Editor editor = preferences.edit();
        final String sUsuario = gson.toJson(usuario);
        editor.putString(USUARIO, sUsuario);
        editor.apply();
    }

    @Override
    public McLocationData getMinimalLocationData() {
        final String sAddress = preferences.getString(MIN_LOCATION, "");
        if (Strings.isNullOrEmpty(sAddress)) {
            return new McLocationData(0, 0, "");
        }
        return gson.fromJson(sAddress, McLocationData.class);
    }

    @Override
    public void setMinimalLocationData(@Nullable McLocationData locationData) {
        final SharedPreferences.Editor editor = preferences.edit();
        String sLocation;
        if (locationData == null) {
            sLocation = gson.toJson(new McLocationData(0, 0, ""));
        } else {
            sLocation = gson.toJson(locationData);
        }
        editor.putString(MIN_LOCATION, sLocation);
        editor.apply();
    }

    public boolean isUsuarioLogged() {
        return getNegocio() == null && getUsuario() != null && preferences.getBoolean(LOGGED, false);
    }

    @Override
    public void setUsuarioLogged(boolean logged) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(NEGOCIO_LOGGED, Boolean.FALSE);
        editor.putBoolean(LOGGED, logged);
        editor.apply();
    }

    //NEGOCIO
    @Override
    public NegocioEntity getNegocio() {
        final String sUsuario = preferences.getString(NEGOCIO, "");
        return gson.fromJson(sUsuario, NegocioEntity.class);
    }

    @Override
    public void setNegocio(NegocioEntity negocio) {
        final SharedPreferences.Editor editor = preferences.edit();
        final String sNegocio = gson.toJson(negocio);
        editor.putString(NEGOCIO, sNegocio);
        editor.apply();
    }

    @Override
    public boolean isNegocioLogged() {
        return getUsuario() == null && getNegocio() != null && preferences.getBoolean(NEGOCIO_LOGGED,
                false);
    }

    @Override
    public void setNegocioLogged(boolean logged) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(NEGOCIO_LOGGED, logged);
        editor.putBoolean(LOGGED, Boolean.FALSE);
        editor.apply();
    }

    @Override
    public void logout() {
        setUsuario(null);
        setUsuarioLogged(false);
        setNegocio(null);
        setNegocioLogged(false);
    }
}
