package addcel.mobilecard.data.local.shared.state

import android.content.SharedPreferences
import androidx.core.content.edit

/**
 * ADDCEL on 2019-06-07.
 */
class StateSession(private val prefs: SharedPreferences) {

    companion object {
        const val MENU_LAUNCHED = "menu_launched"
        const val WALLET_LAUNCHED = "wallet_launched"
        const val CAPTURED_PHONE = "captured_phone"
        const val CAPTURED_EMAIL = "captured_email"

        fun build(prefs: SharedPreferences): StateSession {
            return StateSession(prefs)
        }
    }

    fun isMenuLaunched(): Boolean {
        return prefs.getBoolean(MENU_LAUNCHED, true)
    }

    fun setMenuLaunched(menuLaunched: Boolean) {
        prefs.edit {
            putBoolean(MENU_LAUNCHED, menuLaunched)
        }
    }

    fun isWalletLaunched(): Boolean {
        return prefs.getBoolean(WALLET_LAUNCHED, true)
    }

    fun setWalletLaunched(walletLaunched: Boolean) {
        prefs.edit { putBoolean(WALLET_LAUNCHED, walletLaunched) }
    }

    fun getCapturedPhone(): String {
        return prefs.getString(CAPTURED_PHONE, "") ?: ""
    }

    fun setCapturedPhone(capturedPhone: String) {
        prefs.edit { putString(CAPTURED_PHONE, capturedPhone) }
    }

    fun getCapturedEmail(): String {
        return prefs.getString(CAPTURED_EMAIL, "") ?: ""
    }

    fun setCapturedEmail(capturedEmail: String) {
        prefs.edit { putString(CAPTURED_EMAIL, capturedEmail) }
    }
}